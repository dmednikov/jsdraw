module.exports = function (grunt) {

    grunt.initConfig({
        qunit: {
            all: ['test/**/*.html']
        },
        jshint: {
            files: ['Gruntfile.js', 'drawing_game/**/*.js', 'test/**/*.js'],
            options: {
                esnext: true,
                node: true,
                globals: {
                    jQuery: true,
                    console: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-qunit');

    grunt.registerTask('default', ['jshint']);
    // A convenient task alias.
    grunt.registerTask('test', ['qunit']);
};