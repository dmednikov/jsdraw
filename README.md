#### To run the project from Docker image ####

1. Docker image lives at [https://hub.docker.com/r/dmednikov/jsdraw/](https://hub.docker.com/r/dmednikov/jsdraw/)
2. Follow standard Docker instructions to pull the image
3. Then start the container
    ```docker run -p 8000:8000 dmednikov/jsdraw```
4. Get IP_ADDRESS of the docker machine
    ```docker-machine env```
5. Open a browser window http://IP_ADDRESS:8000

The docker image is generated, however it is not deployed anywhere. It is created just for fun.
Docker image is created via direct integration of DockerHub and Bitbucket server.
At this time I no longer use DOCKER_HUB_TRIGGER

#### To run the project, after the clone ####

1. From "js_draw" folder: 
    ```
    npm install 
    ```
(it will install all required modules)
2. And then from "drawing_game" folder: 
    ```node app.js``` (to start up the node .js)
3. In the browser:```http://localhost:8000/```

#### Heroku deployment

The app deployed to Heroku at:

https://jsdraw.herokuapp.com 

Heroku allows you do git like app deployment. However, one needs API KEY for that.
API key can be found at https://dashboard.heroku.com/account

####Basic usage:####

- Right click in the canvas to draw shapes. 
- Open a new tab and they will sync with each other

####Useful to install for development####

 ```npm install -g browserify ```

 ```npm install -g watchify ```

 ```npm install -g grunt-cli ```

Commands to browserify and watchify (from "drawing_game" folder):

 ```browserify assets/js/script.js -o assets/js/auto_generated/bundle.js -d ```

 ```watchify assets/js/script.js -o assets/js/auto_generated/bundle.js -v ```

 ```npm test ``` (to run tests)

Instead of using watchify to run browserify, should use it to run babelify
which is "browserify transform for babel". Produces transpiled ECMA2015 code.

  ```watchify assets/js/script.js -o assets/js/auto_generated/bundle.js -v -d -t [ babelify --presets [ @babel/preset-env ] ] ```

To start the app itself:

  ```npm start ```

To start the nodemon rather than the node (for development):

  ```npm run start-dev ```