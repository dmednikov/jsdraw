/*jshint expr: true*/
var chai = require('chai'),
    mocha = require('mocha'),
    should = chai.should(),
    expect = chai.expect;

var io = require('socket.io-client');
//
describe("UserEventManager", function() {
    var server,
        options ={
            transports: ['websocket'],
            'force new connection': true
        };
    beforeEach(function (done) {
        // start the server
        server = require('../../../../../drawing_game/app').server;
        done();
    });
    //
    it("check that a user gets created on.join()", function(done) {
        var client = io.connect("http://localhost:8000", options);

        client.once("connect", function () {

            client.once("all-users", function (users) {
                expect(users).to.not.be.null;
                expect(users).to.have.lengthOf(1);
                client.disconnect();
                done();
            });

            var data = {
                name: 'anonymous',
                room_name: 'default',
                uuid:'123111'
            };

            client.emit("join", data);
        });

    });
    //
    it("check that 3 users get created in 'default' room", function(done) {
        const client1 = io.connect("http://localhost:8000", options);

        client1.once("connect", function () {
            
            const client2 = io.connect("http://localhost:8000", options);

            client2.once("connect", function () {
            
                const client3 = io.connect("http://localhost:8000", options);

                client3.once("connect", function () {

                    client3.once("all-users", function (users) {
                        expect(users).to.not.be.null;// jshint ignore:line
                        expect(users).to.have.lengthOf(3);
                        client1.disconnect();
                        client2.disconnect();
                        client3.disconnect();
                        done();
                    });
            
                    //const client3 = io.connect("http://localhost:8000", options);

                    const data = {
                        name: 'anonymous',
                        room_name: 'default',
                        uuid:'123113'
                    };

                    client3.emit("join", data);
                });


                const data = {
                    name: 'anonymous',
                    room_name: 'default',
                    uuid:'123112'
                };

                client2.emit("join", data);
            });

            const data = {
                name: 'anonymous',
                room_name: 'default',
                uuid:'123111'
            };

            client1.emit("join", data);
        });

    });
    //utility function ...
    function connectClient(room_name, uuid){
        const client = io.connect("http://localhost:8000", options);
        client.once("connect", function () {
            const data = {
                name: 'anonymous',
                room_name: room_name,
                uuid: uuid
            };
            client.emit("join", data);
        });
        return client;
    }
    function getConnectedClient(){
        const client = io.connect("http://localhost:8000", options);
        return client;
    }
    function clientJoinRoom(client, room_name, uuid){

        client.once("connect", function () {
            const data = {
                name: 'anonymous',
                room_name: room_name,
                uuid: uuid
            };
            client.emit("join", data);
        });
    }
    //
    it("check that 3 users get created in 'default' room repeated but done in a different way :) ", function(done) {
        //connect the first client
        const client1 = getConnectedClient();
        clientJoinRoom(client1, 'default', '223111');
        //connect the second client
        //const client2 = connectClient('default', '123112');
        const client2 = getConnectedClient();
        clientJoinRoom(client2, 'default', '223112');
        //connect the third client
        //const client3 = connectClient('default', '123113');
        const client3 = getConnectedClient();
        clientJoinRoom(client3, 'default', '223113');
        //check that all three clients are in the same ('default') room
        client3.on("all-users", function (users) {
            //because order of uysers joining is not guaranteed need to wait until we hit 3 users
            // it is "safe" to wait because there is a timeout on each test (2000 ms)
            if(users.length !== 3){
                return;
            }
            expect(users).to.not.be.null;
            expect(users).to.have.lengthOf(3); // i guess it is redundant but looks rnicely readable
            client1.disconnect();
            client2.disconnect();
            client3.disconnect();

            done();
        });

        
    });
    //
    it("check room user counts with disconnect", function(done) {
        //connect the first client
        const client1 = connectClient('default', '123111');
        //connect the second client
        const client2 = connectClient('default', '123112');
        //connect the third client
        const client3 = connectClient('default', '123113');
        //connect the forth client
        const client4 = connectClient('default', '123114');
        //disconnect one client
        client1.disconnect();
        //check that three clients are in the same ('default') room
        client4.on("all-users", function (users) {
            //because order of uysers joining is not guaranteed need to wait until we hit 3 users
            // it is "safe" to wait because there is a timeout on each test (2000 ms)
            if(users.length !== 3){
                return;
            }
            expect(users).to.not.be.null;
            expect(users).to.have.lengthOf(3);
            client2.disconnect();
            client3.disconnect();
            client4.disconnect();
            done();
        });

    });
    
    //
    it("check two different room user counts", function(done) {
        //connect the first client to an existing/valid room
        const client1 = connectClient('Mumbai', '123111');
        //connect the second client  to an existing/valid room
        const client2 = connectClient('Mumbai', '123112');
        //connect the third client
        const client3 = connectClient('default', '123113');
        //connect the forth client
        const client4 = connectClient('default', '123114');
        //check counts in the first room
        client2.on("all-users", function (users) {
            //because order of uysers joining is not guaranteed need to wait until we hit 3 users
            // it is "safe" to wait because there is a timeout on each test (2000 ms)
            if(users.length !== 2){
                return;
            }
            expect(users).to.not.be.null;
            expect(users).to.have.lengthOf(2);
        });
        //check counts in the second room
        client4.on("all-users", function (users) {
            //because order of uysers joining is not guaranteed need to wait until we hit 3 users
            // it is "safe" to wait because there is a timeout on each test (2000 ms)
            if(users.length !== 2){
                return;
            }
            expect(users).to.not.be.null;
            expect(users).to.have.lengthOf(2);
            client1.disconnect();
            client2.disconnect();
            client3.disconnect();
            client4.disconnect();
            done();
        });
    });
});