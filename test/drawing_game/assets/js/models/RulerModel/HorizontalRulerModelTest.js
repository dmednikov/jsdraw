var assert = require('assert');

var chai = require('chai'),
    mocha = require('mocha'),
    expect = chai.expect;

var jsdom = require('jsdom');

jsdom.defaultDocumentFeatures={
    FetchExternalResources      : ['script'],
    ProcessExternalResources    : ['script'],
    MutationEvents              : '2.0',
    QuerySelector               : false
};

var htmlDoc = '<html lang="en-US"></html>';

var tstDocument=jsdom.jsdom(htmlDoc);
var document = tstDocument.defaultView.document;

var window = tstDocument.defaultView,
  $ = global.jQuery = require('jquery')(window);

var sinon = require('sinon');
let item = {};
item.moveTo = function(){};
item.lineTo = function(){};
item.oneOf = function(){};
// Replace class with a stub.(optionally return an item.
global.Path2D = sinon.stub(item, 'constructor').returns(item);

/*
require('jsdom-global')(); // gives me document -> https://github.com/rstacruz/jsdom-global

var testCanvasElement = document.createElement('canvas');
testCanvasElement.width = 500;
testCanvasElement.height = 300;
var canvas = $(testCanvasElement);
*/
//console.info(testCanvasElement.getContext('2d'));

var proxyquire =  require('proxyquire'), // allows to mock what's in require
   LayersAPI   =  { };

//http://stackoverflow.com/questions/30627490/how-can-i-stub-window-width-using-sinon
var originalWidthFunction = $.prototype.width;
sinon.stub($.prototype, 'width').returns(900);

var originalHeightFunction = $.prototype.height;
sinon.stub($.prototype, 'height').returns(900);

////////
//var Transform = require('../../../../../../drawing_game/assets/js/matrix/Transform');
//const transform = sinon.stub(Transform);

var transform = {};
transform.getScaleX = function(){};
transform.save = function(){};
transform.zoomOnPoint = function(){};
transform.restore = function(){};

var context = {};
context.transform = transform;

context.clearRect = function() {};
context.beginPath = function() {};
context.moveTo = function() {};
context.lineTo = function() {};
context.save = function() {};
context.restore = function() {};
context.setTransform = function() {};
context.strokeText = function() {};
context.fillText = function() {};
context.stroke = function() {};

var canvas = {};
canvas.getContext = function(){return context;};
canvas.width = function(){return 100;};
canvas.height = function(){return 100;};



//LayersAPI.oneOf = function () { return canvas };


//var RulerModel = proxyquire('../../../../../../drawing_game/assets/js/models/RulerModel/RulerModel', { '../../layers/LayerAPI': LayersAPI });
var RulerModel = require('../../../../../../drawing_game/assets/js/models/RulerModel/RulerModel');

//var HorizontalRulerModel = proxyquire('../../../../../../drawing_game/assets/js/models/RulerModel/HorizontalRulerModel', { './RulerModel': RulerModel });
var HorizontalRulerModel = require('../../../../../../drawing_game/assets/js/models/RulerModel/HorizontalRulerModel');
//stub the function of the client to create a mocked client
//var Line = require('../../../../../../drawing_game/assets/js/shapes/Line');
//sinon.stub(Line.prototype.constructor, 'draw').returns([foo, foo]);
//var draw = sinon.stub(Line, 'draw');
//draw();
//let whatever = {}
//whatever.draw = function(){};
//global.Line = sinon.stub(whatever, 'draw').returns(whatever);

var BaseModel = require('../../../../../../drawing_game/assets/js/models/BaseModel');
/*
global.CanvasRenderingContext2D = () => {};
global.CanvasRenderingContext2D.clearRect = () => {}; 
global.CanvasRenderingContext2D.offset = () => {};
global.CanvasRenderingContext2D.top = () => {};

*/
/*
const CanvasRenderingContext2D = () => {};
CanvasRenderingContext2D.clearRect = () => {}; 
CanvasRenderingContext2D.offset = () => {};
CanvasRenderingContext2D.top = () => {};
CanvasRenderingContext2D.transform = transform;
*/
global.jQuery.subscribe = () => {};
//global.Path2D = () => {};
//global.moveTo = () => {};
//global.Path2D = () => new Object();

//var HorizontalRulerModel = require('../../../../../../drawing_game/assets/js/models/RulerModel/HorizontalRulerModel');
//ar spy = sinon.stub(HorizontalRulerModel, 'clearCanvas')
//global.CanvasRenderingContext2D

const options ={};
options.socket = null;
options.canvas = canvas;
options.pubsub = null;
options.context = context;
var horizontalRulerModel = new HorizontalRulerModel(options);


describe('HorizontalRulerModel tests', function() {
  describe('Instance is HorizontalRulerModel', function() {
    it('check that it is HorizontalRulerModel', function() {
      expect(horizontalRulerModel).to.be.an.instanceof(HorizontalRulerModel);
    });
  });
  describe('Instance is RulerModel', function() {
    it('check that it is RulerModel', function() {
      expect(horizontalRulerModel).to.be.an.instanceof(RulerModel);
    });
  });
  describe('Instance is BaseModel', function() {
    it('check that it is BaseModel', function() {
      expect(horizontalRulerModel).to.be.an.instanceof(BaseModel);
    });
  });



});
//////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// restoring original function
$.prototype.width = originalWidthFunction;
$.prototype.height = originalHeightFunction;