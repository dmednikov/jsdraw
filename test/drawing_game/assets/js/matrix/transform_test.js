var assert = require('assert');
var Transform = require('../../../../../drawing_game/assets/js/matrix/Transform');
var jsdom = require('jsdom');
//console.log(__filename);

	jsdom.defaultDocumentFeatures={
        FetchExternalResources      : ['script'],
        ProcessExternalResources    : ['script'],
        MutationEvents              : '2.0',
        QuerySelector               : false
    };

    var htmlDoc = '<html lang="en-US">' +
        '</html>';

    var tstDocument=jsdom.jsdom(htmlDoc);
    var canvas = tstDocument.defaultView.document.createElement('canvas');
	canvas.width = 500;
	canvas.height = 400;
	var ctx = canvas.getContext('2d');
	var transform = new Transform(ctx);


describe('Transform', function() {
	describe('zoomOnPoint', function() {
  		it('should stay in the position after zomming in and out', function() {
	      	//zoom in
	    	transform.zoomOnPoint(1.25, 1.25, 100, 100);

			//zoom out on the same point
			transform.zoomOnPoint(0.8, 0.8, 100, 100);

			//the matrix should not have changed
			assert.equal(0, transform.getXTranslation());
			assert.equal(0, transform.getYTranslation());
			
    	});

  	});
  	describe('zoomOnPoint multiple times', function() {
  		it('should stay in the position after zomming in and out', function() {
	      	//zoom in
	    	transform.zoomOnPoint(1.25, 1.25, 100, 100);
	    	transform.zoomOnPoint(1.25, 1.25, 150, 100);
	    	transform.zoomOnPoint(1.25, 1.25, 200, 300);
	    	transform.zoomOnPoint(1.25, 1.25, 100, 100);
	    	transform.zoomOnPoint(1.25, 1.25, 123, 123);

			//reverse order of points when zooming out
			transform.zoomOnPoint(0.8, 0.8, 123, 123);
			transform.zoomOnPoint(0.8, 0.8, 100, 100);
			transform.zoomOnPoint(0.8, 0.8, 200, 300);
			transform.zoomOnPoint(0.8, 0.8, 150, 100);
			transform.zoomOnPoint(0.8, 0.8, 100, 100);

			//the matrix should not have changed
			assert.equal(0, transform.getXTranslation());
			assert.equal(0, transform.getYTranslation());
			
    	});

  	});

});