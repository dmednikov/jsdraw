// Including libraries
var bodyParser = require('body-parser');
var express = require("express");
var http = require('http');
//var app = require('http').createServer(handler),
var app = express();
var server = http.createServer(app);
var io = require('socket.io')(server);

var ejs = require('ejs'); //tempaltes
var fs = require('fs'); //file system

var staticVars = require('./assets/js/utils/static_vars.js');
//initializing user related information
var UserEventManager = require('./assets/js/users/UserEventManager.js');
var UsersModel = require('./assets/js/users/UsersModel.js');
var usersModel = new UsersModel();
var UsersService = require('./assets/js/users/UsersService.js');
var usersService = new UsersService(usersModel);
// refactor Canvas events from thsi file
var CanvasEventManager = require('./assets/js/events/CanvasEventManager.js');


// create application/json parser 
var jsonParser = bodyParser.json()
 
// create application/x-www-form-urlencoded parser 
var urlencodedParser = bodyParser.urlencoded({ extended: false })

var port = process.env.PORT || 8000;


server.listen(port, function () {
    console.log('listening on *:' + port);
});


function serveHomePage(req, res, fParams){

    //since we are in a request handler function
    //we're using readFile instead of readFileSync
    fs.readFile('./drawing_game/index.html', 'utf-8', function(err, content) {

        res.writeHead(200, {'Content-Type': 'text/html'});

        if (err) {
            console.info(err);
            res.end('error occurred');
            return;
        }

        const username = "Anonymous";

        const number_of_room_visitors = 0;

        const room_name = (fParams && fParams.room_name) || "default";

        const temp = 'drawing2';  //

        const renderedHtml = ejs.render(content, {
            username: username,
            room: room_name, 
            number_of_room_visitors: number_of_room_visitors
        });  //get redered HTML code
        res.end(renderedHtml);

    });
    
}

app.get('/', function(req, res) {

    serveHomePage(req, res);

});
app.get('/index.html', function(req, res) {

    serveHomePage(req, res);

});

app.get('/rooms/:room_name', urlencodedParser, function (req, res, next) {
    if (req.originalUrl === '/rooms/choose_room.html') {
        return next();
    }

    const largestCities = staticVars.getLargestCities();

    const needle = decodeURIComponent(req.params.room_name);

    var index = 0, len = largestCities.length;

    for( ; index < len; index++ ) {
        if( largestCities[index][0] === needle ) {
            break;
        }
    }

    if(index !== len){
        serveHomePage(req, res, {room_name: req.params.room_name});
    } else {
        serveHomePage(req, res);
    }
    
    
})

// POST /login gets urlencoded bodies 
app.post('/rooms/choose_room.html', urlencodedParser, function (req, res) {
    if (!req.body) return res.sendStatus(400)
    //res.send('welcome, ' + req.body.username + req.body.state_id);
    //res.redirect('/index.html?username='+ req.body.username + '&state_id=' + req.body.state_id );
    const room_name = encodeURIComponent(req.body.state_id);
    res.redirect('/rooms/'+room_name);
    //res.render('/index.html?username='+ req.body.username + '&state_id=' + req.body.state_id, {"whatever":"qweqweqwe"});
})

app.use(express.static("./drawing_game"));
// If the URL of the socket server is opened in a browser
/*function handler(request, response) {
    request.addListener('end', function () {
        fileServer.serve(request, response);Even
    }).resume();
}*/

// Delete this row if you want to see debug messages
//io.set('log level', 1);

// managing users and rooms
let users = []; // update it in disconnect

// Listen for incoming connections from clients
io.on('connection', function (socket) {

    console.log('a user connected');
    const userEventManagerParams = {
        "io" : io,
        "socket" : socket,
        "usersModel" : usersModel,
        "usersService" : usersService
    };

    var userEventManager = new UserEventManager(userEventManagerParams);

    var canvasEventManager = new CanvasEventManager(userEventManagerParams);

   
    
    // purely testing message
    socket.on("echo", function (msg, callback) {
        callback = callback || function () {};
        //here i want to send back to themessage emitter too
        socket.emit("echo", msg);
        
        callback(null, "Done.");
    });
    
});