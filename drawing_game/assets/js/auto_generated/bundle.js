(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

function EventEmitter() {
  this._events = this._events || {};
  this._maxListeners = this._maxListeners || undefined;
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!isNumber(n) || n < 0 || isNaN(n))
    throw TypeError('n must be a positive number');
  this._maxListeners = n;
  return this;
};

EventEmitter.prototype.emit = function(type) {
  var er, handler, len, args, i, listeners;

  if (!this._events)
    this._events = {};

  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events.error ||
        (isObject(this._events.error) && !this._events.error.length)) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      } else {
        // At least give some kind of context to the user
        var err = new Error('Uncaught, unspecified "error" event. (' + er + ')');
        err.context = er;
        throw err;
      }
    }
  }

  handler = this._events[type];

  if (isUndefined(handler))
    return false;

  if (isFunction(handler)) {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        args = Array.prototype.slice.call(arguments, 1);
        handler.apply(this, args);
    }
  } else if (isObject(handler)) {
    args = Array.prototype.slice.call(arguments, 1);
    listeners = handler.slice();
    len = listeners.length;
    for (i = 0; i < len; i++)
      listeners[i].apply(this, args);
  }

  return true;
};

EventEmitter.prototype.addListener = function(type, listener) {
  var m;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events)
    this._events = {};

  // To avoid recursion in the case that type === "newListener"! Before
  // adding it to the listeners, first emit "newListener".
  if (this._events.newListener)
    this.emit('newListener', type,
              isFunction(listener.listener) ?
              listener.listener : listener);

  if (!this._events[type])
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  else if (isObject(this._events[type]))
    // If we've already got an array, just append.
    this._events[type].push(listener);
  else
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];

  // Check for listener leak
  if (isObject(this._events[type]) && !this._events[type].warned) {
    if (!isUndefined(this._maxListeners)) {
      m = this._maxListeners;
    } else {
      m = EventEmitter.defaultMaxListeners;
    }

    if (m && m > 0 && this._events[type].length > m) {
      this._events[type].warned = true;
      console.error('(node) warning: possible EventEmitter memory ' +
                    'leak detected. %d listeners added. ' +
                    'Use emitter.setMaxListeners() to increase limit.',
                    this._events[type].length);
      if (typeof console.trace === 'function') {
        // not supported in IE 10
        console.trace();
      }
    }
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  var fired = false;

  function g() {
    this.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  g.listener = listener;
  this.on(type, g);

  return this;
};

// emits a 'removeListener' event iff the listener was removed
EventEmitter.prototype.removeListener = function(type, listener) {
  var list, position, length, i;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events || !this._events[type])
    return this;

  list = this._events[type];
  length = list.length;
  position = -1;

  if (list === listener ||
      (isFunction(list.listener) && list.listener === listener)) {
    delete this._events[type];
    if (this._events.removeListener)
      this.emit('removeListener', type, listener);

  } else if (isObject(list)) {
    for (i = length; i-- > 0;) {
      if (list[i] === listener ||
          (list[i].listener && list[i].listener === listener)) {
        position = i;
        break;
      }
    }

    if (position < 0)
      return this;

    if (list.length === 1) {
      list.length = 0;
      delete this._events[type];
    } else {
      list.splice(position, 1);
    }

    if (this._events.removeListener)
      this.emit('removeListener', type, listener);
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  var key, listeners;

  if (!this._events)
    return this;

  // not listening for removeListener, no need to emit
  if (!this._events.removeListener) {
    if (arguments.length === 0)
      this._events = {};
    else if (this._events[type])
      delete this._events[type];
    return this;
  }

  // emit removeListener for all listeners on all events
  if (arguments.length === 0) {
    for (key in this._events) {
      if (key === 'removeListener') continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners('removeListener');
    this._events = {};
    return this;
  }

  listeners = this._events[type];

  if (isFunction(listeners)) {
    this.removeListener(type, listeners);
  } else if (listeners) {
    // LIFO order
    while (listeners.length)
      this.removeListener(type, listeners[listeners.length - 1]);
  }
  delete this._events[type];

  return this;
};

EventEmitter.prototype.listeners = function(type) {
  var ret;
  if (!this._events || !this._events[type])
    ret = [];
  else if (isFunction(this._events[type]))
    ret = [this._events[type]];
  else
    ret = this._events[type].slice();
  return ret;
};

EventEmitter.prototype.listenerCount = function(type) {
  if (this._events) {
    var evlistener = this._events[type];

    if (isFunction(evlistener))
      return 1;
    else if (evlistener)
      return evlistener.length;
  }
  return 0;
};

EventEmitter.listenerCount = function(emitter, type) {
  return emitter.listenerCount(type);
};

function isFunction(arg) {
  return typeof arg === 'function';
}

function isNumber(arg) {
  return typeof arg === 'number';
}

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

function isUndefined(arg) {
  return arg === void 0;
}

},{}],2:[function(require,module,exports){
"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function MouseHandler(canvas) {
  /*
   * 
   * @param {type} canvas
   * @returns {undefined}canvas - jQuery object
   */
  var that = this;

  this.init = function (canvas) {
    this.canvas = canvas;
    this.context = this.canvas[0].getContext('2d'); // set up context for render function
  };

  this.init(canvas);

  this.convertToCanvasCoords = function (pageX, pageY) {
    //in case canvas get moved during the page lifecycle
    var offset = canvas.offset();
    var x = pageX - offset.left;
    var y = pageY - offset.top;
    return {
      "x": x,
      "y": y
    };
  };
}

module.exports = MouseHandler;

},{}],3:[function(require,module,exports){
'use strict';
/*
 * A central factory which produces the data structures for all events
 * 
 * Rather than creating different events all over the code a central factory
 * should allow for better control and change management. In other words
 * in orde to fire an event a structure for the event must be taken from
 * this factory and its fields filled as required.
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var EventFactory =
/*#__PURE__*/
function () {
  function EventFactory() {
    _classCallCheck(this, EventFactory);
  }

  _createClass(EventFactory, null, [{
    key: "getMouseMoveEventObject",

    /*
     * Some consideration for mouseMove event
     *
     * Rather than creating a drawing event with changing mouse coordinates
     * for this iteration decided that creatin a generic event with properties such as
     * isDrawing' is preferred. Will see if it goes out of control with a number of required
     * properties, in this case might revisit the approach.
     */
    value: function getMouseMoveEventObject() {
      return {
        mouseCoordsOverCanvas: {
          //mouse coordinates over canvas (not adjusted to the current pan and zoom)
          x: "",
          y: ""
        },
        mouseCoordsOverCanvasAdjusted: {
          //mouse coordinates over canvas adjusted to the current pan and zoom
          x: "",
          y: ""
        },
        isDrawing: false,
        isPanning: false,
        panDisplacement: 0,
        zoomLevel: 1,
        origin: {},
        // where the move event started
        currentDrawingMode: 0 // this is for cases when isDrawing is set to true one needs to know what Shape to create in onMouseDown

      };
    }
    /*
     * Some consideration for mouseDown event
     *
     * A couple of options here:
     *  - pass origin already adjusted to pan and zoom
     *  - pass raw coordinates and adjust on the client
     *
     */

  }, {
    key: "getMouseDownEventObject",
    value: function getMouseDownEventObject() {
      return {
        mouseCoordsOverCanvasAdjusted: {
          //mouse coordinates over canvas adjusted to the current pan and zoom
          x: "",
          y: ""
        },
        button: "left",
        isDrawing: false
      };
    }
    /*
     * Event for when client zooms in or out
     *
     */

  }, {
    key: "getZoomEventObject",
    value: function getZoomEventObject() {
      return {
        direction: 0
      };
    }
    /*
     * Event for when client zooms in or out, pans ...
     * Any time transform matrix needs to be adjusted on any layer
     */

  }, {
    key: "getChangeTransformMatrixObject",
    value: function getChangeTransformMatrixObject() {
      return {
        zoomLevel: 1,
        origin: {},
        panDisplacement: {
          //pan displacement values
          x: "",
          y: ""
        },
        mouseCoordsOverCanvas: {
          ////mouse coordinates over canvas (not adjusted to the current pan and zoom), temporary for backwards copmatibility
          x: "",
          y: ""
        }
      };
    }
  }]);

  return EventFactory;
}();

module.exports = EventFactory;

},{}],4:[function(require,module,exports){
"use strict";

var ShapeFactory = require('../shapes/ShapeFactory');
/* 
 * This is a model supports information (block) display
 * for a currently selected shape
 */


function InfoBlock(options) {
  /*
   * @param {type} canvas
   * @returns {undefined}canvas - jQuery object
   */
  var that = this;

  this.init = function (options) {
    this.basicModel = options.basicModel;
    this.pubsub = options.pubsub; //$.subscribe('client_zooming', function( event, obj ) {
    //that.handleZoomEvent.call(that, obj);
    //});

    if (this.pubsub) {
      this.pubsub.on('client_zooming', function (obj) {
        that.handleZoomEvent.call(that, obj);
      });
    }

    if (this.pubsub) {
      this.pubsub.on('users_updated', function (obj) {
        that.handleUsersUpdated.call(that, obj);
      });
    }
    /*$.subscribe('socket_zooming', function( event, obj ) {
        that.handleZoomEvent.call(that, obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('socket_zooming', function (obj) {
        that.handleZoomEvent.call(that, obj);
      });
    }
    /*$.subscribe('client_panning', function( event, obj ) {
        that.handlePanEvent.call(that, obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('client_panning', function (obj) {
        that.handlePanEvent.call(that, obj);
      });
    }
    /*$.subscribe('socket_panning', function( event, obj ) {
        that.handlePanEvent.call(that, obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('socket_panning', function (obj) {
        that.handlePanEvent.call(that, obj);
      });
    }
    /*$.subscribe('mouse_move', function( event, obj ) {
        that.handleMouseMoveEvent.call(that, obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('mouse_move', function (obj) {
        that.handleMouseMoveEvent.call(that, obj);
      });
    }

    $.subscribe('shape_selected', function (event, obj) {
      that.handleShapeSelectedEvent.call(that, obj);
    });
    $.subscribe('debug_show_shapes', function (event) {
      console.info(that.basicModel.getShapesRawForDebugging());
    });
    $.subscribe('properties_form_submission', function (event, obj) {
      //jQuery("#propertiesForm").hide();
      jQuery("#myModal").remove();
      var form_data = obj.split('&');
      var input = {};
      $.each(form_data, function (key, value) {
        var data = value.split('=');

        if (data[0] === "points") {
          input[data[0]] = JSON.parse(decodeURIComponent(data[1]));
          return true; // it is not 'continue' because we are in 'each' loop not regular 'for loop'
        }

        input[data[0]] = decodeURIComponent(data[1]);
      });
      var newShape = null;

      try {
        newShape = ShapeFactory.createFromJSON(input);
      } catch (ex) {//should may be popup user message about an attempt to create invalid shape ...
      }

      if (newShape !== null && newShape.isValid()) {
        that.basicModel.updateShape(input.selectedIndex, newShape);
        that.basicModel.emitShapeUpdated(input.selectedIndex);
      }
    });
    $.subscribe('show_properties_dialog', function (event, index) {
      var shape = that.basicModel.getShape(index); //default, if not hsape is selected popup informative message

      var html = '<!-- Modal --> ' + '<div id="myModal" class="modal fade" role="dialog">' + '<div class="modal-dialog">' + '<!-- Modal content-->' + '<div class="modal-content">' + '<div class="modal-header">' + '<button type="button" class="close" data-dismiss="modal">&times;</button>' + '<h4 class="modal-title">No shape selected!</h4>' + '</div>' + '<div class="modal-body">' + '<form id="propertiesForm">' + '<span class="input-group-addon" id="sizing-addon3">Please select a shape</span>' + '</form>' + '</div>' + '<div class="modal-footer">' + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' + '</div>' + '</div>' + '</div>' + '</div>'; //if there is a selected shape, overwrite default then

      if (shape != null) {
        html = '<!-- Modal --> ' + '<div id="myModal" class="modal fade" role="dialog">' + '<div class="modal-dialog">' + '<!-- Modal content-->' + '<div class="modal-content">' + '<div class="modal-header">' + '<button type="button" class="close" data-dismiss="modal">&times;</button>' + '<h4 class="modal-title">' + shape.constructor.name + '</h4>' + '</div>' + '<div class="modal-body">' + '<form id="propertiesForm">' + shape.toHTMLString() + '<input name="type" type="hidden" value="' + shape.constructor.name + '"/>' + '<input name="selectedIndex" type="hidden" value="' + index + '"/>' + '</form>' + '</div>' + '<div class="modal-footer">' + '<button type="button" class="btn btn-default" onClick="$.publish(\'properties_form_submission\', $(\'#propertiesForm\').serialize())">Submit</button>' + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' + '</div>' + '</div>' + '</div>' + '</div>';
      } //$("#myModal").modal();


      var modal = $(html);
      modal.modal({
        backdrop: false
      });
    });
  };

  this.init(options);

  this.handleUsersUpdated = function (obj) {
    var users = obj.users.filter(function (item) {
      return item.uuid !== obj.selfUUID;
    });
    $("#number_of_room_visitors").html(users.length + 1);
  };

  this.handleZoomEvent = function (obj) {
    $("#zoom_value").html(obj.zoomLevel);
  };

  this.handlePanEvent = function (obj) {
    $("#pan_x").html(obj.panDisplacement.x);
    $("#pan_y").html(obj.panDisplacement.y);
  };

  this.handleMouseMoveEvent = function (obj) {
    $("#mouse_x").html(obj.mouseCoordsOverCanvas.x);
    $("#mouse_y").html(obj.mouseCoordsOverCanvas.y);
    $("#mouse_adjusted_x").html(obj.mouseCoordsOverCanvasAdjusted.x);
    $("#mouse_adjusted_y").html(obj.mouseCoordsOverCanvasAdjusted.y);
  };
  /*
   * param obj : 
   */


  this.handleShapeSelectedEvent = function (obj) {
    var inputGroupStart = '<div class="panel panel-default" style="margin:4px;">';
    var inputGroupEnd = '</div>'; //const button = '<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Properties</button>';

    var button = '<button type="button" class="btn btn-primary btn-sm" onClick="$.publish(\'show_properties_dialog\', ' + obj.selectedShapeIndex + ')" >Properties</button>';
    $("#shape_properties").html(inputGroupStart + that.basicModel.getShape(obj.selectedShapeIndex).toString() + "<br />" + button + inputGroupEnd);
  };
}

module.exports = InfoBlock;

},{"../shapes/ShapeFactory":30}],5:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//Should it be a Singleton perhaps?
// interesting discussion:
// http://stackoverflow.com/questions/3680429/click-through-a-div-to-underlying-elements
// there is an option there 

/*
     $('#elementontop).click(function (e) {
        $('#elementontop).hide();
        $(document.elementFromPoint(e.clientX, e.clientY)).trigger("click");
        $('#elementontop').show();
    });
 */
var LayerAPI =
/*#__PURE__*/
function () {
  function LayerAPI() {
    _classCallCheck(this, LayerAPI);
  }

  _createClass(LayerAPI, null, [{
    key: "create",
    value: function create(originalCanvas, id, zIndex) {
      var originalCanvasOffset = originalCanvas.offset();
      var canvas = document.createElement('canvas');
      canvas.id = id;
      canvas.style.top = originalCanvasOffset.top + "px";
      canvas.style.left = originalCanvasOffset.left + "px";
      canvas.style.pointerEvents = "none";
      canvas.width = originalCanvas.width();
      canvas.height = originalCanvas.height();
      canvas.style.zIndex = 8;

      if (zIndex) {
        canvas.style.zIndex = zIndex;
      }

      canvas.style.position = "absolute";
      canvas.style.border = "none";
      document.body.appendChild(canvas);
      return canvas;
    }
  }, {
    key: "oneOf",
    value: function oneOf(originalCanvas, id) {
      if (originalCanvas) {
        var offset = 20;
        var originalCanvasOffset = originalCanvas.offset();
        var canvas = document.createElement('canvas');
        canvas.id = id;
        canvas.customOffset = offset; // used when the cnavas is re-drawn TODO: look into why this is needed.

        canvas.style.top = originalCanvasOffset.top - offset + "px";
        canvas.style.left = originalCanvasOffset.left - offset + "px";
        canvas.style.pointerEvents = "none";
        canvas.width = originalCanvas.width() + offset;
        canvas.height = originalCanvas.height() + offset;
        canvas.style.zIndex = 9;
        canvas.style.position = "absolute";
        canvas.style.border = "none";
        document.body.appendChild(canvas);
        return canvas;
      }

      return null;
    }
  }]);

  return LayerAPI;
}();

module.exports = LayerAPI;

},{}],6:[function(require,module,exports){
"use strict";

/*
 * Transform tracker
 *
 * @author Kevin Moot <kevin.moot@gmail.com>
 * Based on a class created by Simon Sarris - www.simonsarris.com - sarris@acm.org
 * 
 * https://github.com/simonsarris/Canvas-tutorials/blob/master/transform.js
 * 
 * It uses setTransform() HTML Canvas function to reset the current transformation to the identety matrix
 * After every translate, scale ... setTransform is called which means that all consequtive transformation
 * will only affect drawings made after the setTransform method is called. Which allows for zoom to simply
 * go up. 1.1 ... 1.2 ... 1.3 and then ... 1.2 willa ctually scale down. In usual context (without this transform 
 * library) going to ... 1.2 will actually still scale up  by a factor of 1.2 instead of scaling down by 0.1 
 */
var Decimal = require('decimal.js');

function Transform(context) {
  // Set the precision and rounding of the default Decimal constructor
  Decimal.set({
    precision: 10,
    rounding: 6
  });
  this.context = context;
  this.matrix = [new Decimal(1), new Decimal(0), new Decimal(0), new Decimal(1), new Decimal(0), new Decimal(0)]; //initialize with the identity matrix

  this.stack = []; //==========================================
  // Constructor, getter/setter
  //==========================================    

  this.setContext = function (context) {
    this.context = context;
  };

  this.getMatrix = function () {
    var clone = [];

    for (var i = 0; i < this.matrix.length; i++) {
      clone.push(this.matrix[i].toNumber());
    }

    return clone;
  };

  this.getXTranslation = function () {
    return this.matrix[4].toNumber();
  };

  this.getYTranslation = function () {
    return this.matrix[5].toNumber();
  };

  this.getScaleX = function () {
    return this.matrix[0].toNumber();
  };

  this.getScaleY = function () {
    return this.matrix[3].toNumber();
  };

  this.setMatrix = function (m) {
    //this.matrix = [m[0],m[1],m[2],m[3],m[4],m[5]];
    this.matrix = [new Decimal(m[0]), new Decimal(m[1]), new Decimal(m[2]), new Decimal(m[3]), new Decimal(m[4]), new Decimal(m[5])];
    this.setTransform();
  };

  this.cloneMatrix = function (m) {
    return [m[0], m[1], m[2], m[3], m[4], m[5]];
  }; //==========================================
  // Stack
  //==========================================


  this.save = function () {
    var matrix = this.cloneMatrix(this.getMatrix());
    this.stack.push(matrix);
    if (this.context) this.context.save();
  };

  this.restore = function () {
    if (this.stack.length > 0) {
      var matrix = this.stack.pop();
      this.setMatrix(matrix);
    }

    if (this.context) this.context.restore();
  }; //==========================================
  // Matrix
  //==========================================


  this.setTransform = function () {
    if (this.context) {
      this.context.setTransform(this.matrix[0], //Horizontal scaling
      this.matrix[1], //Horizontal skewing
      this.matrix[2], //Vertical skewing
      this.matrix[3], //Vertical scaling
      this.matrix[4], //Horizontal moving
      this.matrix[5] //Vertical moving
      );
    }
  };
  /*
  this.translate = function(x, y) {
      this.matrix[4] += this.matrix[0] * x + this.matrix[2] * y;
      this.matrix[5] += this.matrix[1] * x + this.matrix[3] * y;
      
      this.setTransform();
  };
  */


  this.translateDelta = function (x, y) {
    this.matrix[4] = this.matrix[4].plus(this.matrix[0].times(x).plus(this.matrix[2].times(y)));
    this.matrix[5] = this.matrix[5].plus(this.matrix[1].times(x).plus(this.matrix[3].times(y)));
    this.setTransform();
  };

  this.translateDeltaLight = function (x, y) {
    //console.info(                              this.matrix[0].toNumber()  );
    //console.info(                              this.matrix[2].times(y).toNumber()  );
    //console.info( this.matrix[0].times(x).plus(this.matrix[2].times(y)).toNumber() );
    this.matrix[4] = this.matrix[4].plus(this.matrix[0].times(x).plus(this.matrix[2].times(y)));
    this.matrix[5] = this.matrix[5].plus(this.matrix[1].times(x).plus(this.matrix[3].times(y)));
  };

  this.translate = function (x, y) {
    this.matrix[4] = new Decimal(x);
    this.matrix[5] = new Decimal(y);
    this.setTransform();
  };

  this.translateLight = function (x, y) {
    this.matrix[4] = new Decimal(x);
    this.matrix[5] = new Decimal(y);
  };

  this.rotate = function (rad) {
    var c = Math.cos(rad);
    var s = Math.sin(rad);
    var m11 = this.matrix[0] * c + this.matrix[2] * s;
    var m12 = this.matrix[1] * c + this.matrix[3] * s;
    var m21 = this.matrix[0] * -s + this.matrix[2] * c;
    var m22 = this.matrix[1] * -s + this.matrix[3] * c;
    this.matrix[0] = m11;
    this.matrix[1] = m12;
    this.matrix[2] = m21;
    this.matrix[3] = m22;
    this.setTransform();
  };

  this.scale = function (sx, sy) {
    this.matrix[0] = this.matrix[0].times(sx);
    this.matrix[1] = this.matrix[1].times(sx);
    this.matrix[2] = this.matrix[2].times(sy);
    this.matrix[3] = this.matrix[3].times(sy);
    this.setTransform();
  };

  this.setScale = function (x, y) {
    this.matrix[0] = new Decimal(x);
    this.matrix[1] = new Decimal(x);
    this.matrix[2] = new Decimal(y);
    this.matrix[3] = new Decimal(y);
    this.setTransform();
  };

  this.scaleLight = function (sx, sy) {
    this.matrix[0] = this.matrix[0].times(sx);
    this.matrix[1] = this.matrix[1].times(sx);
    this.matrix[2] = this.matrix[2].times(sy);
    this.matrix[3] = this.matrix[3].times(sy); //this.setTransform();
  };
  /* Assuming uniform scaling 
   *
   */


  this.getScaleFactor = function () {
    return this.matrix[0];
  };
  /*
  this.scale = function(sx, sy) {
      this.matrix[0] = sx;
      this.matrix[3] = sy;
      
      this.setTransform();
      //this.context.translate(x, y);
  };*/


  this.zoomOnPoint = function (sx, sy, x, y) {
    //console.info(sx + " :: " + sy);
    //console.info(x + " :: " + y);
    sx = new Decimal(sx);
    sy = new Decimal(sy);
    x = new Decimal(x);
    y = new Decimal(y); //this.matrix[4] = new Decimal(x);
    //this.matrix[5] = new Decimal(y);
    //move coordinate system origin to the x,y (mouse position over the canvas)
    //console.info(this.matrix[4].toNumber() + " <-4 0 5-> " + this.matrix[5].toNumber());
    //console.info(x + " - " + y);

    this.translateDeltaLight(x, y); //console.info(this.matrix[4].toNumber() + " <-4 1 5-> " + this.matrix[5].toNumber());
    //scale 

    this.scaleLight(sx, sy); //const scaleFactor = this.getScaleFactor();
    //
    //console.info("scale :" +scaleFactor);
    //console.info("Back  X:" + x.minus(x.times(scaleFactor)).toNumber());
    // move the coordinate system back to where it was
    //this.translateDeltaLight(x.minus(x.times(scaleFactor)), y.minus(y.times(scaleFactor)));
    //originx -= mousex/(scale*zoom) - mousex/scale;
    //originx = originx - originx/zoom -originx

    this.translateDeltaLight(-x, -y); //console.info(this.matrix[4].toNumber() + " <-4 2 5-> " + this.matrix[5].toNumber());
    //this.restore();

    this.setTransform(); //console.info(this.getMatrix());
  }; //==========================================
  // Matrix extensions
  //==========================================


  this.rotateDegrees = function (deg) {
    var rad = deg * Math.PI / 180;
    this.rotate(rad);
  };

  this.rotateAbout = function (rad, x, y) {
    this.translate(x, y);
    this.rotate(rad);
    this.translate(-x, -y);
    this.setTransform();
  };

  this.rotateDegreesAbout = function (deg, x, y) {
    this.translate(x, y);
    this.rotateDegrees(deg);
    this.translate(-x, -y);
    this.setTransform();
  };

  this.identity = function () {
    this.m = [1, 0, 0, 1, 0, 0];
    this.setTransform();
  };

  this.multiply = function (matrix) {
    var m11 = this.matrix[0] * matrix.m[0] + this.matrix[2] * matrix.m[1];
    var m12 = this.matrix[1] * matrix.m[0] + this.matrix[3] * matrix.m[1];
    var m21 = this.matrix[0] * matrix.m[2] + this.matrix[2] * matrix.m[3];
    var m22 = this.matrix[1] * matrix.m[2] + this.matrix[3] * matrix.m[3];
    var dx = this.matrix[0] * matrix.m[4] + this.matrix[2] * matrix.m[5] + this.matrix[4];
    var dy = this.matrix[1] * matrix.m[4] + this.matrix[3] * matrix.m[5] + this.matrix[5];
    this.matrix[0] = m11;
    this.matrix[1] = m12;
    this.matrix[2] = m21;
    this.matrix[3] = m22;
    this.matrix[4] = dx;
    this.matrix[5] = dy;
    this.setTransform();
  };

  this.invert = function () {
    var d = 1 / (this.matrix[0] * this.matrix[3] - this.matrix[1] * this.matrix[2]);
    var m0 = this.matrix[3] * d;
    var m1 = -this.matrix[1] * d;
    var m2 = -this.matrix[2] * d;
    var m3 = this.matrix[0] * d;
    var m4 = d * (this.matrix[2] * this.matrix[5] - this.matrix[3] * this.matrix[4]);
    var m5 = d * (this.matrix[1] * this.matrix[4] - this.matrix[0] * this.matrix[5]);
    this.matrix[0] = m0;
    this.matrix[1] = m1;
    this.matrix[2] = m2;
    this.matrix[3] = m3;
    this.matrix[4] = m4;
    this.matrix[5] = m5;
    this.setTransform();
  }; //==========================================
  // Helpers
  //==========================================


  this.transformPoint = function (x, y) {
    return {
      x: x * this.matrix[0] + y * this.matrix[2] + this.matrix[4],
      y: x * this.matrix[1] + y * this.matrix[3] + this.matrix[5]
    };
  };
}

module.exports = Transform;

},{"decimal.js":34}],7:[function(require,module,exports){
"use strict";

/* 
 * This is BASE model from which many other models should inherit
 * It provides common functionality for:
 * - zooming
 * - panning
 * - clearing canvas

 * 
 */
var Transform = require('../matrix/Transform'); //var ShapeStoreArray = require('../storage/shapes/ShapeStoreArray');


function BaseModel(options) {
  var that;

  this.init = function (options) {
    if (options.socket) {
      this.sockett = options.socket;
    }

    this.zoomLevel = 1;
    this.panDisplacement = {};
    this.panDisplacement.x = 0;
    this.panDisplacement.y = 0;
    this.mouseDownOrigin = {}; //mouseDownOrigin - start for pan

    this.mouseDownOrigin.x = 0;
    this.mouseDownOrigin.y = 0;
    this.pubsub = options.pubsub;

    if (options.canvas) {
      this.canvas = options.canvas;
    }

    this.context = options.context;
    that = this;
    /*
     * Zoom on the client
     */

    /*jQuery.subscribe('client_zooming', function( event, obj ) {
        that.handleZoomEvent.call(that, obj);
    });*/

    if (this.pubsub) {
      this.pubsub.on('client_zooming', function (obj) {
        that.handleZoomEvent.call(that, obj);
      });
    }
    /*
     * 
     */

    /*jQuery.subscribe('socket_zooming', function( event, obj ) {
        that.handleZoomEvent.call(that, obj);
      });*/


    if (this.pubsub) {
      this.pubsub.on('socket_zooming', function (obj) {
        that.handleZoomEvent.call(that, obj);
      });
    }
    /*jQuery.subscribe('client_panning', function( event, obj ) {
        that.handlePanEvent.call(that, obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('client_panning', function (obj) {
        that.handlePanEvent.call(that, obj);
      });
    }
    /*jQuery.subscribe('socket_panning', function( event, obj ) {
        that.handleSocketPanEvent.call(that, obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('socket_panning', function (obj) {
        that.handleSocketPanEvent.call(that, obj);
      });
    }
    /*
     * Set the origin for pan start position
     */

    /*jQuery.subscribe('mouse_left_button_down', function( event, obj ) {
      that.handleMouseLeftButtonDownEvent.call(that, obj);
    });*/
    //pubsub is not guaranteed at all, some models do not need it


    if (this.pubsub) {
      this.pubsub.on('mouse_left_button_down', function (obj) {
        that.handleMouseLeftButtonDownEvent.call(that, obj);
      });
    }

    if (this.pubsub) {
      this.pubsub.on('window_resized', function (obj) {
        that.handleWindowResizedEvent.call(that, obj);
      });
    }
  };

  this.init(options);
}

BaseModel.prototype.getScale = function (obj) {
  return this.context.transform.getScaleX();
};

BaseModel.prototype.getScaleX = function (obj) {
  return this.context.transform.getScaleX();
};

BaseModel.prototype.getScaleY = function (obj) {
  return this.context.transform.getScaleY();
};
/*
 * Need to set the origin when left mouse button is pressed down
 * as it is going to be a new shape origin or start of pan
 * 
 * @param {type} obj
 * @param {type} bool indicates if this function should call render function
 * @returns {none}
 */


BaseModel.prototype.handleWindowResizedEvent = function (obj, doNotRender) {
  var offset = 0;

  if (this.canvas[0].customOffset !== undefined) {
    offset = this.canvas[0].customOffset;
  } //


  this.canvas.width(obj.canvas.width() + offset);
  this.canvas.height(obj.canvas.height() + offset); //

  this.context.canvas.width = obj.canvas.width() + offset;
  this.context.canvas.height = obj.canvas.height() + offset; //

  this.context.transform.translate(this.panDisplacement.x, this.panDisplacement.y);
  /*if (typeof this.reactToCanvasResizeEvent === "function") { 
      this.reactToCanvasResizeEvent();
  }*/
  //

  if (doNotRender) {
    return;
  }

  this.render.call(this, obj);
};
/*
 * Need to set the origin when left mouse button is pressed down
 * as it is going to be a new shape origin or start of pan
 * 
 * @param {type} obj
 * @returns {none}
 */


BaseModel.prototype.handleMouseLeftButtonDownEvent = function (obj) {
  if (obj === null) {
    return;
  } //this call makes a huge difference to panning and zooming


  this.mouseDownOrigin = obj.mouseCoordsOverCanvasAdjusted;
};

BaseModel.prototype.handleZoomEvent = function (obj) {
  this.zoomLevel *= obj.zoomL;
  this.context.transform.zoomOnPoint(obj.zoomL, obj.zoomL, obj.origin.x, obj.origin.y); //const matrix = this.context.transform.getMatrix();

  this.panDisplacement.x = this.context.transform.getXTranslation();
  this.panDisplacement.y = this.context.transform.getYTranslation();
  this.render(this.context, obj);
};

BaseModel.prototype.handlePanEvent = function (obj) {
  //console.info(this);
  //console.info(this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x));
  //console.info(obj.mouseCoordsOverCanvas.x + " - basemodel - " + this.mouseDownOrigin.x + " - basemodel - " + this.panDisplacement.x);
  //console.info(this.panDisplacement);
  this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
  this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);
  this.context.transform.translate(this.panDisplacement.x, this.panDisplacement.y);
  this.render(this.context, obj);
};
/*
 * There is some specific to socket panning because
 * there was no mouse_down event to set the origin
 * And this is nothing to calculate here, just values to the ones passed in the event
 */


BaseModel.prototype.handleSocketPanEvent = function (obj) {
  this.mouseDownOrigin = obj.origin;
  this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
  this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);
  this.context.transform.translate(this.panDisplacement.x, this.panDisplacement.y);
  this.render(this.context, obj);
};
/*
 * Calculates dispalcement on one axis based on the mouseCoordsOverCanvas (passed in)
 * and previously svaed MouseDownOrigin
 *
 * @param mouseCoordsOverCanvasOnXorY is current mouse position over the canvas either over X or Y axis
 * @param panDisplacementOnXorY current pan displacement
 * @param mouseDownOriginOnXorY mouse position when pan started
 */


BaseModel.prototype.calculateDeltaDisplacement = function (mouseCoordsOverCanvasOnXorY, panDisplacementOnXorY, mouseDownOriginOnXorY) {
  // obj.mouseCoordsOverCanvas is current mouse position over the canvas; It doesn't account 
  // for zoom or previous dicplacement
  //console.info("calculateDeltaDisplacement parameters = " + mouseCoordsOverCanvasOnXorY + " : " + panDisplacementOnXorY + " : " + mouseDownOriginOnXorY + " : " + this.zoomLevel);
  var current = (mouseCoordsOverCanvasOnXorY - panDisplacementOnXorY) / this.zoomLevel; //console.info("calculateDeltaDisplacement current = " + current);
  //need to multiply by this.zoomLevel to convert displacement back to screen coords

  var diffX = (mouseDownOriginOnXorY - current) * this.zoomLevel; //console.info("calculateDeltaDisplacement diffX = " + diffX);

  var directionX = diffX < 0 ? 1 : -1;
  diffX = Math.abs(diffX); //console.info("calculateDeltaDisplacement diffX * directionX = " + (diffX * directionX));

  return diffX * directionX;
};

module.exports = BaseModel;

},{"../matrix/Transform":6}],8:[function(require,module,exports){
"use strict";

/* 
 * This model maintains an array of all shapes drawn (for redrawing on canvas
 * and any manipulation)
 * It also draws these shapes on the canvas (clears canvas as well). However, it only
 * "draws" the shapes from "shapes" array. There is a difference between
 * those and a shape being drawn (this one happens in its own model) and later
 * added to "shapes" array.
 * It also handles "pan" event and "init"s a grid
 * Makes shapes selected
 * 
 * TODO: Too many responsibilities and it should be refactored
 */
var Line = require('../shapes/Line');

var ShapeFactory = require('../shapes/ShapeFactory');

var Transform = require('../matrix/Transform');

var BaseModel = require('./BaseModel'); //var ShapeStoreArray = require('../storage/shapes/ShapeStoreArray');


function BasicModel(options) {
  //const options = {};
  //options.canvas = canvas;
  //options.socket = socket;
  BaseModel.call(this, options); //call the super constructor

  var that;

  this.init = function (options) {
    this.sockett = options.socket;
    this.lastEmit = 0;
    this.shapes = options.shapesStorage; //layers are to keep various ... static? layers in the model
    //this.layers = {};
    //this.layers.grid = [];

    this.selectedShapeIndex = -1;
    that = this;
    /*
     * Reacting to the mouse events
     */

    /*$.subscribe('mouse_move', function( event, obj ) {
        that.handleMouseMoveEvent.call(that, obj);
    });*/

    /*if(this.pubsub){
        this.pubsub.on('mouse_move', function(obj) {
            that.handleMouseMoveEvent.call(that, obj);
        });
    }*/

    $.subscribe('shape_selected', function (event, obj) {
      that.handleShapeSelectedEvent.call(that, obj);
    });
    /*.subscribe('new_shape_created', function( event, obj ) {
        that.addShape(ShapeFactory.createFromJSON(obj.new_shape));
    });*/

    if (this.pubsub) {
      this.pubsub.on('new_shape_created', function (obj) {
        that.addShape(ShapeFactory.createFromJSON(obj.new_shape));
      });
    } //

    /*$.subscribe('shape_deleted', function( event, obj ) {
        that.deleteShapeByIndex(obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('shape_deleted', function (obj) {
        that.deleteShapeByIndex(obj);
      });
    } //

    /*$.subscribe('shape_updated', function( event, obj ) {
        that.updateShapeByIndex(obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('shape_updated', function (obj) {
        that.updateShapeByIndex(obj);
      });
    } //from toolbar


    $.subscribe('deleteShape', function (event, obj) {
      that.deleteShape();
    }); //initGrid(this, this.canvas.height(), this.canvas.width());

    this.canvas.on('fromContextMenu', {}, function (e, param) {
      switch (param.key) {
        case "delete":
          that.deleteShape();
      }
    });
  };

  this.init(options);
  /*
   * This one simply highlights shapes if mouse passes over them
   * 
   * @param {type} obj
   * @returns {none}
   */

  /*this.handleMouseMoveEvent = function (obj) {
     if(obj.isDrawing === false){
        this.markSelectedShapeIfMouseOverIt(obj.mouseCoordsOverCanvas.x, obj.mouseCoordsOverCanvas.y);
     }       
  };*/

  /*
   * Calculates zoom level based on a direction of the mouse wheel 
   */

  this.caclculateZoomLevel = function (direction) {
    return this.zoomLevel;
  };

  this.getZoomLevel = function () {
    return this.getScale();
  };

  this.setPanDisplacement = function (panDisplacement) {
    this.panDisplacement = panDisplacement;
    this.render();
  };

  this.getPanDisplacement = function () {
    return this.panDisplacement;
  };

  this.addShape = function (shape) {
    this.shapes.add(shape);
    this.render();
  };
  /*
   * I am wondering if I should return a copy of the shapesStorage instead of real thing ?
   * To prevent anyone from outside BasicModel changing it.
   */


  this.getShapes = function () {
    return this.shapes;
  };
  /*
   * 
   */


  this.getShapesRawForDebugging = function () {
    return this.shapes.returnRawData();
  };
  /*
   * 
   */


  this.getShape = function (index) {
    if (index !== undefined && index !== null && index >= 0 && index < this.shapes.size()) {
      return this.shapes.get(index);
    } else {
      console.info("Trying to get a shape illegally; index = " + index + "; # of shapes = " + this.shapes.size());
      return null;
    }
  };
  /*
   * 
   */

  /*this.getMouseDownOrigin = function () {
      return this.mouseDownOrigin;
  };*/
  //TODO refactor as this function does more than deletign shape
  // need to refactor out the code for selection


  this.deleteShape = function () {
    if (this.selectedShapeIndex !== -1) {
      var shapeIndex = this.selectedShapeIndex;
      this.shapes["delete"](this.selectedShapeIndex);
      this.selectedShapeIndex = -1;
      this.render();
      this.emitShapeDeleted(shapeIndex);
    }
  };

  this.deleteShapeByIndex = function (obj) {
    var index = obj.deletedShapeIndex; //cannot just simply do if(index) because index can be 0

    if (index !== undefined && index !== null && index >= 0 && index < this.shapes.size()) {
      this.shapes["delete"](index);
      this.render();
    } else {
      console.info("Trying to do illegal shape delete; index = " + index + "; # of shapes = " + this.shapes.size());
    }
  };

  this.updateShape = function (index, newShape) {
    //cannot just simply do if(index) because index can be 0
    if (index !== undefined && index !== null && index >= 0 && index < this.shapes.size()) {
      this.shapes.updateByIndex(index, newShape);
      this.render();
    } else {
      console.info("Trying to do illegal shape update; index = " + index + "; # of shapes = " + this.shapes.size());
    }
  };

  this.updateShapeByIndex = function (obj) {
    var shapeIndex = obj.shapeIndex;
    this.shapes.updateByIndex(shapeIndex, ShapeFactory.createFromJSON(obj.shape));
    this.render();
  };

  this.emitShapeDeleted = function (shapeIndex) {
    this.sockett.emit('shapeDeleted', {
      'deletedShapeIndex': shapeIndex
    });
  };

  this.emitShapeUpdated = function (shapeIndex) {
    this.sockett.emit('shapeUpdated', {
      'shapeIndex': shapeIndex,
      'shape': this.shapes.get(shapeIndex)
    });
  };
  /*this.setSelectedShape = function (index) {
      if (index >= 0 && index < this.shapes.size()) {
          this.selectedShapeIndex = index;
          $.publish("shape_selected", {selectedShapeIndex:index});
          this.render();
      }
  };*/

  /*
   * param obj : when shape is selected need to set its index here so that it can be drawn as selected
   * (do not draw in the SelectedShapeModel because it will be another shape overlayed on top of the shape here)
   */


  this.handleShapeSelectedEvent = function (obj) {
    if (obj.selectedShapeIndex >= 0 && obj.selectedShapeIndex < this.shapes.size()) {
      this.selectedShapeIndex = obj.selectedShapeIndex;
      this.render();
    }
  };
  /*this.markSelectedShapeIfMouseOverIt = function (mouseX, mouseY) {
      var selectedShapeIndex = this.getSelectedShapeIndex(mouseX, mouseY);
      this.setSelectedShape(selectedShapeIndex);
  };*/

  /*this.getSelectedShapeIndex = function (mouseX, mouseY) {
      var len = this.shapes.size();
      for (var i = 0; i < len; i++) {
          var shape = this.shapes.get(i);
          if (shape.isPointOver(this.context, mouseX, mouseY)) {
              return i;
          }
      }
      return -1;
  };*/


  this.clearCanvas = function () {
    var topLeft = {};
    topLeft.x = (0 - this.panDisplacement.x) / this.getScale();
    topLeft.y = (0 - this.panDisplacement.y) / this.getScale();
    var bottomRight = {};
    bottomRight.x = this.canvas.width() / this.getScale();
    bottomRight.y = this.canvas.height() / this.getScale();
    this.context.clearRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
  };

  this.render = function () {
    var len = this.shapes.size();
    this.clearCanvas();

    for (var i = 0; i < len; i++) {
      var shape = this.shapes.get(i);

      if (i === this.selectedShapeIndex) {
        shape.drawSelected(this.context);
      } else {
        shape.draw(this.context);
      }
    } //this.drawGrid();  

  };
  /*
   * Little test of performance
   */


  function createManyLines() {
    var numLinesForTest = 10000; //at work 10 < x < 1580
    // 10 < y < 530

    for (var i = 0; i < numLinesForTest; i++) {
      x1 = Math.floor(Math.random() * 1580 + 1);
      x2 = Math.floor(Math.random() * 1580 + 1);
      y1 = Math.floor(Math.random() * 530 + 1);
      y2 = Math.floor(Math.random() * 530 + 1);
      var obj = new Line(x1, y1, x2, y2);
      this.shapes.add(obj);
    }
  }
}

BasicModel.prototype = Object.create(BaseModel.prototype, {
  constructor: {
    value: BasicModel
  }
});
module.exports = BasicModel;

},{"../matrix/Transform":6,"../shapes/Line":27,"../shapes/ShapeFactory":30,"./BaseModel":7}],9:[function(require,module,exports){
"use strict";

/* 
 * Draw grid lines:
 * - typically slightly bleaker lines for the grid
 * - and brighter lines at 0,0 point to highlight the origin point
 */
var Layers = require('../layers/LayerAPI');

var Line = require('../shapes/Line');

var BaseModel = require('./BaseModel');

function CanvasBorderModel(options) {
  //const options = {};
  //options.canvas = jQuery(Layers.oneOf(options.canvas)); 
  //options.socket = null;
  BaseModel.call(this, options); //call the super constructor

  /*
   * 
   */

  this.render = function (context, obj) {
    this.clearCanvas();
    this.canvasBorder.forEach(function (element) {
      element.draw(this.context, 1 / this.zoomLevel);
    }, this);
  };
  /*
   * Override parent's object default behaviour
   */


  this.handleZoomEvent = function (obj) {//need to override the method not to zoom in this context
  };
  /*
   * Override parent's object default behaviour
   */


  this.handlePanEvent = function (obj) {};
  /*
   * Override parent's object default behaviour
   */


  this.handleSocketPanEvent = function (obj) {};

  this.clearCanvas = function () {
    this.context.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
  };
  /*
   * Function still does a few too many things but at least it is now refactored into itrs own d
   * dedicated function to set the size (and color for now) for the border
   */


  this.setBorderSize = function (canvasWidth, canvasHeight, offsetX) {
    this.canvasBorder = []; // holds 4 lines to draw the border around the canvas
    //draw a "border" line under the ruler ticks

    var border = new Line(0, offsetX, canvasWidth, offsetX);
    border.setStrokeStyle(this.borderLineColor);
    this.canvasBorder.push(border); //top horizontal

    border = new Line(canvasWidth, 0, canvasWidth, canvasHeight);
    border.setStrokeStyle(this.borderLineColor);
    this.canvasBorder.push(border); //right vertical

    border = new Line(canvasWidth, canvasHeight, 0, canvasHeight);
    border.setStrokeStyle(this.borderLineColor);
    this.canvasBorder.push(border); //bottom horizontal

    border = new Line(offsetX, canvasHeight, offsetX, 0);
    border.setStrokeStyle(this.borderLineColor);
    this.canvasBorder.push(border); //left vertical
  };
  /*
   * Each canvas model needs to be able to react to canvas resizing.
   * In this case we need to either shorten or extend the vertical ruler ticks
   * depending if the canvas shrunk or enlarged
   */


  this.reactToCanvasResizeEvent = function () {
    this.canvasWidth = this.canvas.width() - this.offsetWidth; // -1 is for line width (otherwise line will be drawn on the outside of the canvas)

    this.canvasHeight = this.canvas.height() - this.offsetHeight;
    this.setBorderSize(this.canvasWidth, this.canvasHeight, this.offsetX);
  };
  /*
   * Handle the resize event from here and call super's method
   */


  this.handleWindowResizedEvent = function (obj) {
    this.__proto__.handleWindowResizedEvent.call(this, obj, true);

    this.reactToCanvasResizeEvent();
    this.render(obj);
  };

  this.init = function () {
    //the edges of the RulerLayer are not flush with Paper canvas edges, the are offset
    this.offsetX = 20;
    this.offsetWidth = 1; //1 is for line width (otherwise line will be drawn on the outside of the canvas)

    this.offsetHeight = 2;
    this.borderLineColor = "#000000";
    this.canvasWidth = this.canvas.width() - this.offsetWidth; // -1 is for line width (otherwise line will be drawn on the outside of the canvas)

    this.canvasHeight = this.canvas.height() - this.offsetHeight;
    this.setBorderSize(this.canvasWidth, this.canvasHeight, this.offsetX);
    this.render();
  };

  this.init();
}

CanvasBorderModel.prototype = Object.create(BaseModel.prototype, {
  constructor: {
    value: CanvasBorderModel
  }
});
module.exports = CanvasBorderModel;

},{"../layers/LayerAPI":5,"../shapes/Line":27,"./BaseModel":7}],10:[function(require,module,exports){
"use strict";

/* 
 * This is a "temporary" drawing model. It is representation of 
 * a "shape being drawn" and nothings else really. Could be called 
 * CurrentlyDrawnShapeModel to be clearer.
 *
 * Whatever user currently is drawing on the screen is supported via this model.
 * Unlike permanent model which stores all drawn shapes.
 *
 * This model passes its context into shapes when they need to draw themselves.
 * This context must "transform"ed according to the current panDisplacement and zoomLevel
 * in order for shpes to be drawn in appropriate locations
 *
 */
var Line = require('../shapes/Line');

var Circle = require('../shapes/Circle');

var FreeForm = require('../shapes/FreeForm');

var ShapeFactory = require('../shapes/ShapeFactory');

var Transform = require('../matrix/Transform');

var Layers = require('../layers/LayerAPI');

var BaseModel = require('./BaseModel');

function CurrentDrawingModel(options) {
  BaseModel.call(this, options); // call the super constructor

  var that;

  this.init = function (options) {
    that = this;
    this.sockett = options.socket;
    this.currentDrawingMode = 0; // line, circle, freeform, ...  or 0

    this.currentShape = null; // shape currently being drawn

    this.context = options.context;
    this.pubsub = options.pubsub;
    /*
     * Set the origin for the shape to be drawn
     */

    /*$.subscribe('mouse_left_button_down', function( event, obj ) {
      that.handleMouseLeftButtonDownEvent.call(that, obj);
    });*/
    // HMMMMMMM, I already have PUBSUB in the BaseModel for this
    // should not really fire the same event the second time

    if (this.pubsub) {
      this.pubsub.on('mouse_left_button_down', function (obj) {//that.handleMouseLeftButtonDownEvent.call(that, obj);
      });
    }
    /*$.subscribe('mouse_left_button_up', function( event, obj ) {
      that.handleMouseLeftButtonUpEvent.call(that, obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('mouse_left_button_up', function () {
        that.handleMouseLeftButtonUpEvent.call(that);
      });
    }
    /*
     * This is in response to server published event to update clients
     * with currently drawn shape
     */

    /*$.subscribe('update_current_drawing_model', function( event, obj ) {
        that.setCurrentShape(ShapeFactory.createFromJSON(obj));
        that.render();
    });*/


    if (this.pubsub) {
      this.pubsub.on('update_current_drawing_model', function (obj) {
        that.setCurrentShape(ShapeFactory.createFromJSON(obj));
        that.render();
      });
    }
    /*$.subscribe('selectDrawingMode', function( event, obj ) {
        that.setDrawingMode(obj.key);
    });*/


    $.subscribe('stopDrawing', function (event, obj) {
      that.setDrawingMode(0);
    });
    /*$.subscribe('new_shape_created', function( event, obj ) {
        that.flushBuffer();
        that.render();
    });*/

    if (this.pubsub) {
      this.pubsub.on('new_shape_created', function (obj) {
        that.flushBuffer();
        that.render();
      });
    }
    /*
     * Reacting to the mouse events
     */

    /*$.subscribe('mouse_move', function( event, obj ) {
        that.handleMouseMoveEvent.call(that, obj);
    });*/


    if (this.pubsub) {
      this.pubsub.on('mouse_move', function (obj) {
        that.handleMouseMoveEvent.call(that, obj);
      });
    }
  };

  this.init(options);
  /*
   * This event, even though a mouse move event, has a parameter
   * isDrawingset to ttrue in order makes sense in this context. The context
   * of drawing shapes that is
   * 
   * @param {type} obj
   * @returns {none}
   */

  this.handleMouseMoveEvent = function (obj) {
    if (obj.isDrawing === true) {
      this.updateCurrentShape(obj.mouseCoordsOverCanvasAdjusted.x, obj.mouseCoordsOverCanvasAdjusted.y); //initiate event to be sent to all connected clients

      this.emitChangeToCurrentShape();
      this.render();
    }
  };
  /*
   * When left mouse button is up it means that drawing has finished
   * Need to get the latest drawn shape and place on the queue for subscribers
   * @param {type} obj
   * @returns {none}
   */


  this.handleMouseLeftButtonUpEvent = function () {
    // Really not big fan of doing it that way. Spreading the knowledge about the types of Shapes
    // across the file here. Not sure if there is a better way. But prefer not having this
    // if === "Point". It might lead to explosion (if === ShapeType). I guess I woonder if shapes
    // themselves should be aware that they are finished to be drawn and emit appropriate events
    // Yes, I think this is a better way to go.
    // meanwhile this one is needed for desktop version, mobile fires mouse_down event on touchend
    var shape = this.getCurrentShape(); //

    if (shape !== null && shape.isValid() && shape.constructor.name === "Point") {
      this.flushBuffer();

      if (this.pubsub) {
        this.pubsub.emit('new_shape_created', {
          "new_shape": shape.toJSON()
        });
      }

      this.emitNewShapeCreated({
        "new_shape": shape.toJSON()
      });
    }
  };
  /*
   * When left mouse button is down it can be either a start of panning
   * or a start of a new shape
  * The event itself is subscribed to in BaseModel and handled there and here 
   *
   * @param {type} obj
   * @returns {none}
   */


  this.handleMouseLeftButtonDownEvent = function (obj) {
    var shape = this.getCurrentShape();

    if (shape !== null && shape.isValid()) {
      this.flushBuffer(); //$.publish("new_shape_created", {"new_shape": shape.toJSON()});

      if (this.pubsub) {
        this.pubsub.emit('new_shape_created', {
          "new_shape": shape.toJSON()
        });
      }

      this.emitNewShapeCreated({
        "new_shape": shape.toJSON()
      });
    } else {
      if (obj === null) {
        return;
      } // this call needs to be made in order to keep drawing shapes in the correct location after panning


      CurrentDrawingModel.prototype.handleMouseLeftButtonDownEvent.call(this, obj); // looks like this is StartShape event

      if (obj.isDrawing === true) {
        var shapeParams = {
          'originX': obj.mouseCoordsOverCanvasAdjusted.x,
          'originY': obj.mouseCoordsOverCanvasAdjusted.y
        };
        this.setCurrentShape(ShapeFactory.initialize(obj.currentDrawingMode, shapeParams));
        this.emitChangeToCurrentShape();
        this.render();
      }
    }
  };
  /*
   * Sets the currentDrawingMode to the string of the shape (line, circle, ...)
   * or 0 when user stops drawing
   */


  this.setDrawingMode = function (drawingMode) {
    this.currentDrawingMode = drawingMode;
  };

  this.getDrawingMode = function () {
    return this.currentDrawingMode;
  };
  /*
   * Poorly named function. It does 3 things not just flushes buffer!
   */


  this.flushBuffer = function () {
    this.setCurrentShape(null);
    this.render();
  };
  /*
   * Returns the property currentShape of if it is undefined then null
   */


  this.getCurrentShape = function () {
    return this.currentShape || null;
  };

  this.emitChangeToCurrentShape = function () {
    if (this.getCurrentShape() !== null) {
      this.sockett.emit('socket_shape_being_drawn_changed', this.getCurrentShape().toJSON());
    }
  };

  this.emitNewShapeCreated = function (shape) {
    this.sockett.emit('shapeCreated', shape);
  };

  this.updateCurrentShape = function (pageX, pageY) {
    if (this.getCurrentShape() !== null) {
      this.getCurrentShape().update(pageX, pageY);
    }
  };

  this.setCurrentShape = function (shape) {
    this.currentShape = shape;
  };

  this.clearCanvas = function () {
    var topLeft = {};
    topLeft.x = (0 - this.panDisplacement.x) / this.zoomLevel;
    topLeft.y = (0 - this.panDisplacement.y) / this.zoomLevel;
    var bottomRight = {};
    bottomRight.x = this.canvas.width() / this.zoomLevel;
    bottomRight.y = this.canvas.height() / this.zoomLevel;
    this.context.clearRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
  };
  /*
   * this.panDisplacement and this.zoomLevel are very important in this model
   * because the context passed into shapes for drawing needs to be transformed accordingly
   */


  this.render = function () {
    this.clearCanvas();

    if (this.getCurrentShape() !== null) {
      this.getCurrentShape().draw(this.context);
    }
  };
}

CurrentDrawingModel.prototype = Object.create(BaseModel.prototype, {
  constructor: {
    value: CurrentDrawingModel
  }
});
module.exports = CurrentDrawingModel;

},{"../layers/LayerAPI":5,"../matrix/Transform":6,"../shapes/Circle":25,"../shapes/FreeForm":26,"../shapes/Line":27,"../shapes/ShapeFactory":30,"./BaseModel":7}],11:[function(require,module,exports){
"use strict";

/* 
 * This model is responsible for maintaing drawing state
 * For example:
 * - is drawing being panned currently
 * - is drawing being zoomed
 * - is drawing being drawn :)
 * 
 */
function DrawingStateModel(options) {
  var that;

  this.init = function (options) {
    that = this;
    this.drawing = false;
    this.currentDrawingMode = 0;
    this.panning = false;
    this.pubsub = options.pubsub;
    /*$.subscribe('mouse_left_button_up', function( event, obj ) {
        that.handleMouseLeftButtonUpEvent.call(that, obj);
    });*/

    if (this.pubsub) {
      this.pubsub.on('mouse_left_button_up', function (obj) {
        that.handleMouseLeftButtonUpEvent.call(that);
      });
    }
    /*
     * User either wants pan or draw
     */

    /*$.subscribe('mouse_left_button_down', function( event, obj ) {
       if (that.getDrawingMode()) {
          that.setDrawing(true);
       } else {
          that.setPanning(true);
       }
    });*/


    if (this.pubsub) {
      this.pubsub.on('mouse_left_button_down', function () {
        if (that.getDrawingMode()) {
          that.setDrawing(true);
        } else {
          that.setPanning(true);
        }
      });
    } //when mouse button is up or left the canvas
    //need to reset the drawing modes

    /*$.subscribe('mouse_left_canvas_boundaries', function( event, obj ) {
        that.setDrawing(false);
        that.setPanning(false);
        //that.setDrawingMode(false); //cannot do that ... connected clients get messed up ... one has circle, then another line, then the first will have line drawn isntead of circle after switching
    });*/


    if (this.pubsub) {
      this.pubsub.on('mouse_left_canvas_boundaries', function () {
        that.setDrawing(false);
        that.setPanning(false);
      });
    }

    $.subscribe('selectDrawingMode', function (event, obj) {
      that.handleSelectDrawingModeEvent(obj);
    });
    $.subscribe('stopDrawing', function (event, obj) {
      that.handleStopDrawingEvent();
    });
  };

  this.init(options);
  /*
   * When left mouse is up it is either drfawing is finished (user needs to
   * hold mouse down to draw) or panning is finished
   */

  this.handleMouseLeftButtonUpEvent = function () {
    //this.setDrawing(false);
    this.setPanning(false);
  };
  /*
   * set drawing mode to the name of the shape being drawn
   * 
   * @param {type} obj
   * @returns {none}
   */


  this.handleSelectDrawingModeEvent = function (obj) {
    //this.setDrawingMode(true);
    this.setDrawingMode(obj.key);
  };
  /*
   * set drawing mode to the name of the shape being drawn
   * 
   * @param {type} obj
   * @returns {none}
   */


  this.handleStopDrawingEvent = function (obj) {
    this.setDrawingMode(false);
  };

  this.setDrawing = function (isDrawing) {
    this.drawing = isDrawing;
  };

  this.isDrawing = function () {
    return this.drawing;
  };

  this.setPanning = function (isPanning) {
    this.panning = isPanning;
  };

  this.isPanning = function () {
    return this.panning;
  };
  /*
   * We either are drawing or not
   * 
   * @param {boolean} drawingMode
   * @returns {none}
   */


  this.setDrawingMode = function (drawingMode) {
    this.currentDrawingMode = drawingMode;
  };

  this.getDrawingMode = function () {
    return this.currentDrawingMode;
  };
}

module.exports = DrawingStateModel;

},{}],12:[function(require,module,exports){
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DrawingToolbarModel =
/*#__PURE__*/
function () {
  function DrawingToolbarModel() {
    _classCallCheck(this, DrawingToolbarModel);

    var self = this;
    $("#toolbar_straight_line").change(function () {
      $.publish("selectDrawingMode", {
        'key': 'straightLine'
      });
    });
    $("#toolbar_circle").change(function () {
      //$("#paper").trigger("selectDrawingMode", {"param": "one", "key": "circle"});
      $.publish("selectDrawingMode", {
        'key': 'circle'
      });
    });
    $("#toolbar_freeform").change(function () {
      //$("#paper").trigger("selectDrawingMode", {"param": "one", "key": "freeForm"});
      $.publish("selectDrawingMode", {
        'key': 'freeForm'
      });
    });
    $("#toolbar_point").change(function () {
      //$("#paper").trigger("selectDrawingMode", {"param": "one", "key": "freeForm"});
      $.publish("selectDrawingMode", {
        'key': 'point'
      });
    });
    $("#toolbar_stop_drawing").change(function () {
      //$("#paper").trigger("stopDrawing", {});
      $.publish("stopDrawing", {
        'key': 'stop_drawing'
      });
    });
    $("#toolbar_delete_shape").click(function () {
      $.publish("deleteShape", {});
    });
    $.subscribe('selectDrawingMode', function (event, obj) {
      self.handleSelectDrawingModeEvent(obj);
    });
    $.subscribe('stopDrawing', function (event, obj) {
      self.handleStopDrawingEvent();
    }); //that.handleSelectDrawingMode.call(that, obj);
  }
  /*
   * Need to highlight appropriate button on the toolbar when event is fired
   */


  _createClass(DrawingToolbarModel, [{
    key: "handleSelectDrawingModeEvent",
    value: function handleSelectDrawingModeEvent(obj) {
      switch (obj.key) {
        case "straightLine":
          $("#toolbar_straight_line").parent().addClass("active").siblings().removeClass("active");
          break;

        case "circle":
          $("#toolbar_circle").parent().addClass("active").siblings().removeClass("active");
          break;

        case "freeForm":
          $("#toolbar_freeform").parent().addClass("active").siblings().removeClass("active");
          break;

        case "point":
          $("#toolbar_point").parent().addClass("active").siblings().removeClass("active");
          break;

        case "freeForm":
          $("#toolbar_stop_drawing").parent().addClass("active").siblings().removeClass("active");
          break;

        default: //do nothing

      }
    }
  }, {
    key: "handleStopDrawingEvent",

    /*
     *
     */
    value: function handleStopDrawingEvent() {
      $("#toolbar_stop_drawing").parent().addClass("active").siblings().removeClass("active");
    }
  }]);

  return DrawingToolbarModel;
}();

module.exports = DrawingToolbarModel;

},{}],13:[function(require,module,exports){
"use strict";

/* 
 * Draw grid lines:
 * - typically slightly bleaker lines for the grid
 * - and brighter lines at 0,0 point to highlight the origin point
 */
var Layers = require('../layers/LayerAPI');

var Line = require('../shapes/Line');

var BaseModel = require('./BaseModel');

function GridModel(options) {
  //const options = {};
  //const canvas = options.canvas;
  //options.canvas = $(Layers.create(options.canvas, "GridLayer", 5));
  //options.socket = socket;
  BaseModel.call(this, options); //call the super constructor

  var that;

  this.init = function (options) {
    that = this;
    this.offset = 15;
    this.gridLines = []; //holds vertical and horizontal lines of the grid

    this.gridVisible = true;
    this.gridLinewidth = 1;
    this.gridLineColor = "#bebebe";
    this.zerothGridLineColor = "#000000";
    this.canvas = options.canvas;
    this.context = options.context;
    jQuery.subscribe('toggleGridVisibility', function () {
      that.toggleGridVisibility();
    });
    this.pubsub = options.pubsub;
    /*$.subscribe('mouse_left_button_up', function( event, obj ) {
      that.handleMouseLeftButtonUpEvent.call(that, obj);
    });*/

    this.initGrid();
    this.render();
  };

  this.initGrid = function () {
    var frequency = 100;
    var width = this.canvas.width();
    var height = this.canvas.height(); //

    this.gridLines = []; //to draw the grid, all I am doing for now is draw grid lines 100 * width to the left and right
    //basically just creating so many of them that I am not likely to pan that far.
    //TODO: make it more effective by doing on demand drawing 

    for (var i = 0 - width * 100; i < width * 100; i = i + frequency) {
      var vLine = new Line(i, 0 - height * 100, i, height * 100);
      vLine.setStrokeStyle(this.gridLineColor);

      if (i === 0) {
        vLine.setStrokeStyle(this.zerothGridLineColor);
      }

      this.gridLines.push(vLine);
    }

    for (var i = 0 - height * 100; i < height * 100; i = i + frequency) {
      var hLine = new Line(0 - width * 100, i, width * 100, i);
      hLine.setStrokeStyle(this.gridLineColor);

      if (i === 0) {
        hLine.setStrokeStyle(this.zerothGridLineColor);
      }

      this.gridLines.push(hLine);
    }
  };

  this.toggleGridVisibility = function () {
    this.gridVisible = !this.gridVisible;
    this.render();
  };

  this.clearCanvas = function () {
    var topLeft = {};
    topLeft.x = (0 - this.panDisplacement.x) / this.zoomLevel;
    topLeft.y = (0 - this.panDisplacement.y) / this.zoomLevel;
    var bottomRight = {};
    bottomRight.x = this.canvas.width() / this.zoomLevel;
    bottomRight.y = this.canvas.height() / this.zoomLevel;
    this.context.clearRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
  };

  this.render = function () {
    this.clearCanvas();

    if (this.gridVisible) {
      var gLen = this.gridLines.length;

      for (var i = 0; i < gLen; i++) {
        var shape = this.gridLines[i];
        shape.draw(this.context, this.gridLinewidth / this.zoomLevel);
      }
    }
  };

  this.init(options);
}

GridModel.prototype = Object.create(BaseModel.prototype, {
  constructor: {
    value: GridModel
  }
});
module.exports = GridModel;

},{"../layers/LayerAPI":5,"../shapes/Line":27,"./BaseModel":7}],14:[function(require,module,exports){
"use strict";

/* 
 * This model is nothing more just an information storage
 * about the current pan and zoom values.
 * Each model actually inherits this values from BaseModel right now
 */
var BaseModel = require('./BaseModel');

function PanZoomModel(options) {
  BaseModel.call(this, options); //call the super constructor

  var that;

  this.init = function () {
    that = this;
  };

  this.init();

  this.getPanDisplacement = function () {
    return this.panDisplacement;
  };

  this.render = function () {//do nothing
  };

  this.getMouseDownOrigin = function () {
    return this.mouseDownOrigin;
  };
}

PanZoomModel.prototype = Object.create(BaseModel.prototype, {
  constructor: {
    value: PanZoomModel
  }
});
module.exports = PanZoomModel;

},{"./BaseModel":7}],15:[function(require,module,exports){
"use strict";

var Line = require('../../shapes/Line');

var RulerGuide = require('./RulerGuide');
/* 
 * Ruler Guide which runs horizontally from the left vertical ruler to mouse pointer
 * (to the right edge of canvas, optionally)
 */


function HorizontalRulerGuideModel(options) {
  RulerGuide.call(this, options); //call the super constructor

  this.init = function () {};

  this.init();
  /*
   * 
   */

  this.render = function (context, obj) {
    this.clearCanvas(); //seeems like not necessarily the right place for this calculation

    if (obj && obj.mouseCoordsOverCanvas) {
      var y = obj.mouseCoordsOverCanvas.y;
      var vLine = new Line(0, y, this.canvasWidth, y);
      vLine.draw(this.context);
    }
  };
  /*
   * Override parent's object default behaviour
   */


  this.handleZoomEvent = function (obj) {//need to override the method not to zoom in this context
  };
  /*
   * Override parent's object default behaviour
   */


  this.handlePanEvent = function (obj) {};
  /*
   * Override parent's object default behaviour
   */


  this.handleSocketPanEvent = function (obj) {};
} // Setup prototype chain by setting prototype property, 
// RulerGuide is prototype.
// And a single property constroctor = HorizontalRulerGuideModel


HorizontalRulerGuideModel.prototype = Object.create(RulerGuide.prototype, {
  constructor: {
    value: HorizontalRulerGuideModel
  }
});
module.exports = HorizontalRulerGuideModel;

},{"../../shapes/Line":27,"./RulerGuide":16}],16:[function(require,module,exports){
"use strict";

var Layers = require('../../layers/LayerAPI');

var BaseModel = require('../BaseModel');
/* 
 * An parent class from which horizontal and vertical guides will inherit
 */


function RulerGuide(options) {
  //const options = {};
  //options.canvas = $(Layers.create(options.canvas, "RulerGuidesLayer", 5)); 
  //options.socket = null;
  BaseModel.call(this, options); //call the super constructor

  var that = this;
  this.toggleRulerGuideVisibilityStatusValue = 0; //this.canvas = $(Layers.create(canvas, "RulerGuidesLayer", 5)); 
  //this.context = this.canvas[0].getContext('2d'); // set up context for render function
  //this.context.transform = new Transform(this.context);

  this.canvasWidth = this.canvas.width();
  this.canvasHeight = this.canvas.height();
  $.subscribe('toggleRulerGuideVisibility', function (event, obj) {
    that.handleToggleRulerGuideVisibilityEvent.call(that, obj); //need this call to clear canvas after Guides were turned off

    that.clearCanvas.call(that);
  });
  /*
   * Reacting to the mouse events
   */

  /*$.subscribe('mouse_move', function( event, obj ) {;
  	if(that.shouldDrawRulerGuides()){
  		that.render.call(that, that.context, obj);
  	}
  });*/

  if (this.pubsub) {
    this.pubsub.on('mouse_move', function (obj) {
      if (that.shouldDrawRulerGuides()) {
        that.render.call(that, that.context, obj);
      }
    });
  }
}

; // This literally overrides the prototype, so to avoid loosing all the RulerGuide.prototype functions
//this prototype decalrations must be before the functions are added to the prototype

RulerGuide.prototype = Object.create(BaseModel.prototype, {
  constructor: {
    value: RulerGuide
  }
});
/*
 * Check if Ruler Guides should be drawn.
 *
 * @returns true in case Ruler Guides need to be drawn
 */

RulerGuide.prototype.shouldDrawRulerGuides = function () {
  if (this.toggleRulerGuideVisibilityStatusValue === 1) {
    return true;
  }

  return false;
};

RulerGuide.prototype.handleToggleRulerGuideVisibilityEvent = function () {
  this.toggleRulerGuideVisibilityStatus();
};

RulerGuide.prototype.toggleRulerGuideVisibilityStatus = function (context) {
  this.toggleRulerGuideVisibilityStatusValue = this.toggleRulerGuideVisibilityStatusValue === 0 ? 1 : 0;
};

RulerGuide.prototype.render = function (context) {
  console.log("render is not implemented in RulerGuide");
};

RulerGuide.prototype.clearCanvas = function () {
  var topLeft = {}; //topLeft.x = (0 - this.panDisplacement.x)/this.zoomLevel;
  //topLeft.y = (0 - this.panDisplacement.y)/this.zoomLevel;

  var bottomRight = {}; //bottomRight.x = (this.canvas.width() )/this.zoomLevel;
  //bottomRight.y = (this.canvas.height() )/this.zoomLevel;

  /*this.context.clearRect(
          topLeft.x, topLeft.y, 
          bottomRight.x, bottomRight.y);*/

  this.context.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
};

module.exports = RulerGuide;

},{"../../layers/LayerAPI":5,"../BaseModel":7}],17:[function(require,module,exports){
"use strict";

var Line = require('../../shapes/Line');

var RulerGuide = require('./RulerGuide');
/* 
 * Ruler Guide which runs horizontally from the left vertical ruler to mouse pointer
 * (to the right edge of canvas, optionally)
 */


function VerticalRulerGuideModel(options) {
  RulerGuide.call(this, options); //call the super constructor

  this.init = function () {};

  this.init();
  /*
   * 
   */

  this.render = function (context, obj) {
    this.clearCanvas(); //seeems like not necessarily the right place for this calculation

    if (obj && obj.mouseCoordsOverCanvas) {
      var x = obj.mouseCoordsOverCanvas.x;
      var vLine = new Line(x, 0, x, this.canvasHeight);
      vLine.draw(this.context);
    }
  };
  /*
   * Override parent's object default behaviour
   */


  this.handleZoomEvent = function (obj) {//need to override the method not to zoom in this context
  };
  /*
   * Override parent's object default behaviour
   */


  this.handlePanEvent = function (obj) {};
  /*
   * Override parent's object default behaviour
   */


  this.handleSocketPanEvent = function (obj) {};
}

VerticalRulerGuideModel.prototype = Object.create(RulerGuide.prototype, {
  constructor: {
    value: VerticalRulerGuideModel
  }
});
module.exports = VerticalRulerGuideModel;

},{"../../shapes/Line":27,"./RulerGuide":16}],18:[function(require,module,exports){
"use strict";

/* 
 * Draw ruler
 */
var Line = require('../../shapes/Line');

var RulerModel = require('./RulerModel'); //var $ = global.jQuery = require("jquery"); // needed for testing, to avoid "$ is not defined"
//console.info($);


function HorizontalRulerModel(options) {
  RulerModel.call(this, options); //call the super constructor

  var that;

  this.updateRulerHorizontalTicksArray = function (rulerTicksArray, limit, sign, offset) {
    //rulerTicksArray = [];
    var lOffset = offset ? Math.ceil(offset / 10) * 10 : 0; // needs to be rounded to the nearest 10 (since ruler is in 10s)
    //console.info("lOffset " + offset);
    //console.info(lOffset);

    for (var i = 0; i < limit; i += 10) {
      if (i < lOffset) {
        continue;
      }

      var coord = sign * i; //console.info("coord:" + coord + " limit:" + limit);

      if (coord < 0 && coord > limit) {
        continue;
      }

      var x0 = coord + this.offsetX;
      var y0 = this.offsetY;
      var x1 = x0;
      var y1 = coord / 100 === parseInt(coord / 100) ? 10 : 15;
      var line = new Line(x0, y0, x1, y1);

      if (y1 === 10) {
        line.setLabel(coord);
        line.setLabelOrientation("horizontal");
      }

      line.setModelScale(this.getScaleX());
      line.setStrokeStyle(this.rulerLineColor);
      rulerTicksArray.push(line);
    }

    return rulerTicksArray;
  };

  this.createRuler = function () {
    var width = this.canvas.width();
    this.rulerTicks.horizontal = [];
    this.rulerTicks.horizontal = this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, width, 1);
  };

  this.clearCanvas = function () {
    var topLeft = {};
    topLeft.x = (0 - this.panDisplacement.x) / this.zoomLevel;
    topLeft.y = (0 - this.panDisplacement.y) / this.zoomLevel;
    var bottomRight = {};
    bottomRight.x = this.canvas.width() / this.zoomLevel;
    bottomRight.y = this.canvas.height() / this.zoomLevel;

    if (this.context) {
      this.context.clearRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }
  };
  /*
   * Each canvas model needs to be able to react to canvas resizing.
   * In this case we need to either shorten or extend the vertical ruler ticks
   * depending if the canvas shrunk or enlarged
   */


  this.reactToCanvasResizeEvent = function () {
    var width = this.canvas.width();
    this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, width, 1);
  };

  this.updateRuler = function (obj) {
    this.rulerTicks.horizontal = [];
    var width = this.canvas.width(); //console.info(this.panDisplacement.x);
    //panning to the right (dragging 0 to the right)

    if (this.panDisplacement.x >= 0) {
      //because the ticks must start from 0 I need fill array to the right and left of 0
      //thus 2 calls to this.updateRulerVerticalTicksArray
      var leftLimit = this.panDisplacement.x / this.getScaleX(); //to the left from 0 when panning right (moving 0 to the right)

      this.rulerTicks.horizontal = this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, leftLimit, -1);
      var rightLimit = width / this.getScaleX() - this.panDisplacement.x / this.getScaleX(); //from left border to 0 mark when panning right (moving 0 to the right)

      this.rulerTicks.horizontal = this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, rightLimit, 1);
    } //panning to the left (dragging 0 to the left)


    if (this.panDisplacement.x / this.getScaleX() < 0) {
      var _rightLimit = width / this.getScaleX() + this.panDisplacement.x / this.getScaleX() * -1;

      var leftOrigin = -1.0 * this.panDisplacement.x / this.getScaleX();
      this.rulerTicks.horizontal = this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, _rightLimit, 1, leftOrigin);
    }
  };

  this.render = function () {
    this.clearCanvas();
    this.rulerTicks.horizontal.forEach(function (element) {
      element.draw(this.context, 1 / this.zoomLevel);
    }, this);
  };
  /*
   * Override parent's object default behaviour
   */


  this.handleZoomEvent = function (obj) {
    this.zoomLevel *= obj.zoomL; // must add this.offsetX because the origin of the canvas is offset

    this.context.transform.zoomOnPoint(obj.zoomL, 1, obj.origin.x - this.offsetX, 0); //const matrix = this.context.transform.getMatrix();

    this.panDisplacement.x = this.context.transform.getXTranslation();
    var trans = this.offsetX - this.offsetX * this.getScale();
    this.panDisplacement.x += trans;
    this.updateRuler(obj);
    this.render(this.context, obj);
  };
  /*
   * Override parent's object default behaviour
   */


  this.handlePanEvent = function (obj) {
    //console.info(this);
    //console.info(this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x));
    //console.info(obj.mouseCoordsOverCanvas.x + " - ruler - " + this.mouseDownOrigin.x + " - ruler - " + this.panDisplacement.x);
    this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x); // trans here is alittle bit to account for the fact that origin of the canvas is offset
    //var trans = this.context.transform.getXTranslation();

    var trans = this.offsetX - this.offsetX * this.getScale();
    this.panDisplacement.x += trans;
    this.context.transform.translate(this.panDisplacement.x, 0);
    this.updateRuler(obj);
    this.render(this.context, obj);
  };
  /*
   * Override parent's object default behaviour
   */


  this.handleSocketPanEvent = function (obj) {
    this.mouseDownOrigin = obj.origin;
    this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x); //this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);

    this.context.transform.translate(this.panDisplacement.x, this.panDisplacement.y);
    this.updateRuler(obj);
    this.render(this.context, obj);
  };
  /*
   * Handle the resize event from here and call super's method
   */


  this.handleWindowResizedEvent = function (obj) {
    this.__proto__.handleWindowResizedEvent.call(this, obj, true);

    this.updateRuler();
    this.render(obj);
  };
  /*
   * This init() function must be on the bottom of the declaration
   * otherwise (because there is no function hoisting) functions called from init()
   * will be from parents prototype since the this's  function are not defined yet
   * for example render()
   */


  this.init = function () {
    that = this;
    this.rulerOriginX = this.offsetX;
    this.rulerOriginY = this.offsetY;
    this.createRuler();
    this.render();
  };

  this.init();
} // This literally overrides the prototype, so to avoid loosing all the RulerGuide.prototype functions
//this prototype decalrations must be before the functions are added to the prototype


HorizontalRulerModel.prototype = Object.create(RulerModel.prototype, {
  constructor: {
    value: HorizontalRulerModel
  }
});
module.exports = HorizontalRulerModel;

},{"../../shapes/Line":27,"./RulerModel":19}],19:[function(require,module,exports){
"use strict";

/* 
 * Draw ruler
 */
var Layers = require('../../layers/LayerAPI');

var BaseModel = require('../BaseModel');

function RulerModel(options) {
  //options.canvas = jQuery(Layers.oneOf(options.canvas)); 
  BaseModel.call(this, options); //call the super constructor

  var that;

  this.init = function () {
    //the edges of the RulerLayer are not flush with Paper canvas edges, the are offset
    this.offsetX = 20;
    this.offsetY = 21;
    this.rulerTicks = {}; // object to hold all the lines required to draw ruler

    this.rulerTicks.horizontal = [];
    this.rulerTicks.vertical = []; //this.canvasBorder = []; // holds 4 lines to draw the border around the canvas
    //this.canvas = $(Layers.oneOf(canvas)); 
    //this.context = this.canvas[0].getContext('2d'); // set up context for render function

    this.rulerLineColor = "#000000"; //that = this;
    //this.createRuler();
    //this.render();
  };

  this.init();

  this.createRuler = function () {};

  this.clearCanvas = function () {};

  this.render = function () {
    console.info("up");
    /*
    this.clearCanvas();
      this.rulerTicksAndOutline.horizontal.forEach(function(element){
        element.draw(this.context);
    }, this);
      this.rulerTicksAndOutline.vertical.forEach(function(element){
        element.draw(this.context);
    }, this);
      this.canvasBorder.forEach(function(element){
        element.draw(this.context);
    }, this);
    */
  };
  /*
   * Override parent's object default behaviour
   */

  /*this.handleZoomEvent = function (obj) {
    };*/

  /*
   * Override parent's object default behaviour
   */

  /*this.handlePanEvent = function (obj) {
      
      this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
  
      //this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);
        this.context.transform.translate(
          this.panDisplacement.x, 0
      );
        this.renderHorizontalRuler(this.context, obj);
      
  };*/

  /*
   * Override parent's object default behaviour
   */

  /*this.handleSocketPanEvent = function (obj) {
    };*/

} // This literally overrides the prototype, so to avoid loosing all the RulerGuide.prototype functions
//this prototype decalrations must be before the functions are added to the prototype


RulerModel.prototype = Object.create(BaseModel.prototype, {
  constructor: {
    value: RulerModel
  }
});
module.exports = RulerModel;

},{"../../layers/LayerAPI":5,"../BaseModel":7}],20:[function(require,module,exports){
"use strict";

/* 
 * Draw ruler
 * TODO: sucky comments. Need to improve.
 */
var Line = require('../../shapes/Line');

var RulerModel = require('./RulerModel'); //var $ = global.jQuery = require("jquery"); // needed for testing, to avoid "$ is not defined"
//console.info($);


function VerticalRulerModel(options) {
  RulerModel.call(this, options); //call the super constructor

  var that;

  this.updateRulerVerticalTicksArray = function (rulerTicksArray, limit, sign) {
    //rulerTicksArray = [];
    for (var i = 0; i < limit; i += 10) {
      var coord = sign * i;
      var y0 = coord + this.offsetX;
      var x0 = this.offsetX;
      var y1 = y0;
      var x1 = coord / 100 === parseInt(coord / 100) ? 10 : 15;
      var line = new Line(x0, y0, x1, y1);

      if (x1 === 10) {
        line.setLabel(coord);
        line.setLabelOrientation("vertical");
      }

      line.setModelScale(this.getScaleY());
      line.setStrokeStyle(this.rulerLineColor);
      rulerTicksArray.push(line);
    }

    return rulerTicksArray;
  };

  this.createRuler = function () {
    var height = this.canvas.height();
    this.rulerTicks.vertical = this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, height, 1);
  };

  this.clearCanvas = function () {
    var topLeft = {};
    topLeft.x = (0 - this.panDisplacement.x) / this.getScaleX();
    topLeft.y = (0 - this.panDisplacement.y) / this.getScaleY();
    var bottomRight = {};
    bottomRight.x = this.canvas.width() / this.getScaleX();
    bottomRight.y = this.canvas.height() / this.getScaleY();
    this.context.clearRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
  };
  /*
   * Each canvas model nned to be able to react to canvas resizing.
   * In this case we need to either shorten or extend the vertical ruler ticks
   * depending if the canvas shrunk or enlarged
   */


  this.reactToCanvasResizeEvent = function () {
    var height = this.canvas.height();
    this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, height, 1);
  };

  this.updateRuler = function (obj) {
    this.rulerTicks.vertical = [];
    var height = this.canvas.height(); //panning down (dragging 0 down)

    if (this.panDisplacement.y / this.getScaleY() >= 0) {
      // because the ticks must start from 0 I need fill array to the right and left of 0
      // thus 2 calls to this.updateRulerVerticalTicksArray
      var upperLimit = this.panDisplacement.y / this.getScaleY();
      this.rulerTicks.vertical = this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, upperLimit, -1);
      var lowerLimit = height / this.getScaleY() - this.panDisplacement.y / this.getScaleY();
      this.rulerTicks.vertical = this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, lowerLimit, 1);
    } //panning up (dragging 0 up)


    if (this.panDisplacement.y / this.getScaleY() < 0) {
      var _lowerLimit = height / this.getScaleY() + this.panDisplacement.y / this.getScaleY() * -1;

      this.rulerTicks.vertical = this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, _lowerLimit, 1);
    }
  };

  this.render = function () {
    this.clearCanvas();
    this.rulerTicks.vertical.forEach(function (element) {
      element.draw(this.context, 1 / this.zoomLevel);
    }, this);
  };
  /*
   * Override parent's object default behaviour
   */


  this.handleZoomEvent = function (obj) {
    this.zoomLevel *= obj.zoomL; // must add this.offsetX because the origin of the canvas is offset

    this.context.transform.zoomOnPoint(1, obj.zoomL, 0, obj.origin.y + this.offsetX);
    var matrix = this.context.transform.getMatrix();
    this.panDisplacement.y = this.context.transform.getYTranslation();
    this.updateRuler(obj);
    this.render(this.context, obj);
  };
  /*
   * Override parent's object default behaviour
   */


  this.handlePanEvent = function (obj) {
    this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y); // trans here is a little bit to account for the fact that origin of the canvas is offset

    var trans = this.context.transform.getYTranslation();
    trans = this.offsetX - this.offsetX * this.getScaleY();
    this.panDisplacement.y += trans;
    this.context.transform.translate(0, this.panDisplacement.y);
    this.updateRuler(obj);
    this.render(this.context, obj);
  };
  /*
   * Override parent's object default behaviour
   */


  this.handleSocketPanEvent = function (obj) {
    this.mouseDownOrigin = obj.origin; //this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);

    this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);
    this.context.transform.translate(this.panDisplacement.x, this.panDisplacement.y);
    this.updateRuler(obj);
    this.render(this.context, obj);
  };
  /*
   * Handle the resize event from here and call super's method
   */


  this.handleWindowResizedEvent = function (obj) {
    this.__proto__.handleWindowResizedEvent.call(this, obj, true);

    this.updateRuler();
    this.render(obj);
  };
  /*
   * This init() function must be on the bottom of the declaration
   * otherwise (because there is no function hoisting) functions called from init()
   * will be from parents prototype since the this's  function are not defined yet
   * for example render()
   */


  this.init = function () {
    that = this;
    this.rulerOriginX = this.offsetX;
    this.rulerOriginY = this.offsetY;
    this.createRuler();
    this.render();
  };

  this.init();
} // This literally overrides the prototype, so to avoid loosing all the RulerGuide.prototype functions
//this prototype decalrations must be before the functions are added to the prototype


VerticalRulerModel.prototype = Object.create(RulerModel.prototype, {
  constructor: {
    value: VerticalRulerModel
  }
});
module.exports = VerticalRulerModel;

},{"../../shapes/Line":27,"./RulerModel":19}],21:[function(require,module,exports){
'use strict';

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var BaseModel = require('./BaseModel');

var SelectedShapeModel =
/*#__PURE__*/
function (_BaseModel) {
  _inherits(SelectedShapeModel, _BaseModel);

  function SelectedShapeModel(options) {
    var _this;

    _classCallCheck(this, SelectedShapeModel);

    //BaseModel.call(this, options); //call the super constructor
    _this = _possibleConstructorReturn(this, _getPrototypeOf(SelectedShapeModel).call(this, options));
    _this.sockett = options.socket;
    _this.shapes = options.shapesStorage;
    _this.selectedShapeIndex = -1;

    var that = _assertThisInitialized(_this);
    /*if(this.pubsub){
        this.pubsub.on('mouse_move', function(obj) {
            that.handleMouseMoveEvent(obj);
        });
    }*/
    //BaseModel handles 'mouse_left_button_down' too, so "local" handleMouseLeftButtonDownEvent will be called via
    //polymorhism. I need to call super's method here though. 

    /*if(this.pubsub){
        this.pubsub.on('mouse_left_button_down', function(obj) {
            that.handleMouseLeftButtonDownEvent(obj);
        });
    }*/


    return _this;
  }
  /*
   * This one simply highlights shapes if mouse passes over them
   * 
   * @param {type} obj
   * @returns {none}
   */

  /*handleMouseMoveEvent (obj) {
     if(obj.isDrawing === false){
        this.markSelectedShapeIfMouseOverIt(obj.mouseCoordsOverCanvas.x, obj.mouseCoordsOverCanvas.y);
     }       
  };*/


  _createClass(SelectedShapeModel, [{
    key: "markSelectedShapeIfMouseOverIt",
    value: function markSelectedShapeIfMouseOverIt(mouseX, mouseY) {
      var selectedShapeIndex = this.getSelectedShapeIndex(mouseX, mouseY);
      this.setSelectedShape(selectedShapeIndex);
    }
  }, {
    key: "getSelectedShapeIndex",
    value: function getSelectedShapeIndex(mouseX, mouseY) {
      var len = this.shapes.size();

      for (var i = 0; i < len; i++) {
        var shape = this.shapes.get(i);

        if (shape.isPointOver(this.context, mouseX, mouseY)) {
          return i;
        }
      }

      return -1;
    }
  }, {
    key: "setSelectedShape",
    value: function setSelectedShape(index) {
      if (index >= 0 && index < this.shapes.size()) {
        this.selectedShapeIndex = index;
        $.publish("shape_selected", {
          selectedShapeIndex: index
        });
      }
    }
  }, {
    key: "handleMouseLeftButtonDownEvent",
    value: function handleMouseLeftButtonDownEvent(obj) {
      if (obj === null) {
        return;
      }

      BaseModel.prototype.handleMouseLeftButtonDownEvent(obj);
      this.markSelectedShapeIfMouseOverIt(obj.mouseCoordsOverCanvasAdjusted.x, obj.mouseCoordsOverCanvasAdjusted.y);
    }
    /*
     * placeholder in order not to crush when parent BaseModel calls its children's render method
     */

  }, {
    key: "render",
    value: function render() {//need to have it there because BaseModel calls render() for its children, i.e. when panning
    }
  }]);

  return SelectedShapeModel;
}(BaseModel);
/*
SelectedShapeModel.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: SelectedShapeModel
    }
});
*/


module.exports = SelectedShapeModel;

},{"./BaseModel":7}],22:[function(require,module,exports){
"use strict";

var EventEmitter = require('events').EventEmitter,
    pubsub = new EventEmitter().setMaxListeners(100);

module.exports = pubsub;
pubsub.on('loggedIn', function (msg) {
  console.log(msg);
});

},{"events":1}],23:[function(require,module,exports){
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var HorizontalRulerModel = require('../models/RulerModel/HorizontalRulerModel');

var VerticalRulerModel = require('../models/RulerModel/VerticalRulerModel');

var GridModel = require('../models/GridModel');

var DrawingToolbarModel = require('../models/DrawingToolbarModel');

var InfoBlock = require('../info_block/InfoBlock');

var BasicModel = require('../models/BasicModel');

var ShapeStoreArray = require('../storage/shapes/ShapeStoreArray'); // needs to be injected into BasicModel


var HorizontalRulerGuideModel = require('../models/RulerGuide/HorizontalRulerGuideModel');

var VerticalalRulerGuideModel = require('../models/RulerGuide/VerticalRulerGuideModel');

var CurrentDrawingModel = require('../models/CurrentDrawingModel');

var SelectedShapeModel = require('../models/SelectedShapeModel');

var CanvasBorderModel = require('../models/CanvasBorderModel');

var Layers = require('../layers/LayerAPI');

var Transform = require('../matrix/Transform');

var InitializationFactory =
/*#__PURE__*/
function () {
  /*
  param: {} options
      options.canvas = canvas;
      options.socket = socket;
      options.pubsub = pubsub;
      ...
      options.context = context;
  */
  function InitializationFactory(options) {
    _classCallCheck(this, InitializationFactory);

    //let originalContext = options.canvas[0].getContext('2d');
    //originalContext.transform = new Transform(originalContext);
    var originalCanvas = options.canvas; // need to preserve the original

    /*
    options.canvas = this.createCanvas(originalCanvas, "current_drawing_canvas");
    options.context = this.createCanvasContext(options.canvas);
    new CurrentDrawingModel(options);
    */

    new CurrentDrawingModel(this.prepModelOptions(options, originalCanvas, "current_drawing_canvas"));
    options.shapesStorage = new ShapeStoreArray();
    new SelectedShapeModel(this.prepModelOptions(options, originalCanvas, "selected_shape_canvas")); //
    //t.markSelectedShapeIfMouseOverIt({});

    /*
    options.canvas = this.createCanvas(originalCanvas, "basic_canvas");
    options.context = this.createCanvasContext(options.canvas);
    const basicModel = new BasicModel(options); // need to inject it into InfoBlock
    */

    var basicModel = new BasicModel(this.prepModelOptions(options, originalCanvas, "basic_canvas")); //need to inject it into InfoBlock therefore creating variable
    //shapeStorage is only needed for BasicModel so can set it to null at this point

    options.shapesStorage = null; //draws little info block about selected shape
    //options.shapesStorage = null;
    //options.canvas = null;
    //options.context = null;

    options.basicModel = basicModel;
    new InfoBlock(options); //options.canvas = $(Layers.create(originalCanvas, "GridLayer", 5));

    /*
    options.canvas = this.createCanvas(originalCanvas, "grid_canvas", 5);
    options.context = this.createCanvasContext(options.canvas);
    new GridModel(options);
    */

    new GridModel(this.prepModelOptions(options, originalCanvas, "grid_canvas", 5));
    new DrawingToolbarModel(); //draws toolbar and handles its events

    /*
    options.canvas = this.createCanvas(originalCanvas, "ruler_guide_canvas_horizontal", 5);
    options.context = this.createCanvasContext(options.canvas);
    new HorizontalRulerGuideModel(options); //draws ruler guide
    */

    new HorizontalRulerGuideModel(this.prepModelOptions(options, originalCanvas, "ruler_guide_canvas_horizontal", 5)); //draws ruler guide

    /*
    options.canvas = this.createCanvas(originalCanvas, "ruler_guide_canvas_vertical", 5);
    options.context = this.createCanvasContext(options.canvas);
    new VerticalalRulerGuideModel(options); //draws ruler guide
    */

    new VerticalalRulerGuideModel(this.prepModelOptions(options, originalCanvas, "ruler_guide_canvas_vertical", 5)); //draws ruler guide
    // Following canvases need to be offset

    /*
    options.canvas = this.createOffsetCanvas(originalCanvas, "ruler_canvas_horizontal");
    options.context = this.createCanvasContext(options.canvas);
    new HorizontalRulerModel(options); //draws ruler
    */

    new HorizontalRulerModel(this.prepOffsetModelOptions(options, originalCanvas, "ruler_canvas_horizontal")); //draws ruler

    /*
    options.canvas = this.createOffsetCanvas(originalCanvas, "ruler_canvas_vertical");
    options.context = this.createCanvasContext(options.canvas);
    new VerticalRulerModel(options); //draws ruler
    */

    new VerticalRulerModel(this.prepOffsetModelOptions(options, originalCanvas, "ruler_canvas_vertical")); //draws ruler

    /*
    options.canvas = this.createOffsetCanvas(originalCanvas, "border_canvas");
    options.context = this.createCanvasContext(options.canvas);
    new CanvasBorderModel(options); //draws bounding box
    */

    new CanvasBorderModel(this.prepOffsetModelOptions(options, originalCanvas, "border_canvas")); //draws bounding box
  }

  _createClass(InitializationFactory, [{
    key: "prepModelOptions",
    value: function prepModelOptions(options, canvas, canvasName, zIndex) {
      if (zIndex) {
        options.canvas = this.createCanvas(canvas, canvasName, zIndex);
      } else {
        options.canvas = this.createCanvas(canvas, canvasName);
      }

      options.context = this.createCanvasContext(options.canvas);
      return options;
    }
  }, {
    key: "prepOffsetModelOptions",
    value: function prepOffsetModelOptions(options, canvas, canvasName) {
      options.canvas = this.createOffsetCanvas(canvas, canvasName);
      options.context = this.createCanvasContext(options.canvas);
      return options;
    }
  }, {
    key: "createCanvas",
    value: function createCanvas(originalCanvas, id, zIndex) {
      var newCanvas = $(Layers.create(originalCanvas, id, zIndex));
      return newCanvas;
    }
  }, {
    key: "createOffsetCanvas",
    value: function createOffsetCanvas(originalCanvas, id) {
      var newCanvas = jQuery(Layers.oneOf(originalCanvas, id));
      return newCanvas;
    }
  }, {
    key: "createCanvasContext",
    value: function createCanvasContext(canvas) {
      var context = canvas[0].getContext('2d');
      context.transform = new Transform(context);
      return context;
    }
  }]);

  return InitializationFactory;
}();

module.exports = InitializationFactory;

},{"../info_block/InfoBlock":4,"../layers/LayerAPI":5,"../matrix/Transform":6,"../models/BasicModel":8,"../models/CanvasBorderModel":9,"../models/CurrentDrawingModel":10,"../models/DrawingToolbarModel":12,"../models/GridModel":13,"../models/RulerGuide/HorizontalRulerGuideModel":15,"../models/RulerGuide/VerticalRulerGuideModel":17,"../models/RulerModel/HorizontalRulerModel":18,"../models/RulerModel/VerticalRulerModel":20,"../models/SelectedShapeModel":21,"../storage/shapes/ShapeStoreArray":32}],24:[function(require,module,exports){
"use strict";

/*
This is a main controller file. It initializes everything and ...
- handles socket.on() events for all browser CLIENTs
*/
var MouseHandler = require('./actions/MouseHandler');

var DrawingStateModel = require('./models/DrawingStateModel');

var EventFactory = require('./events/EventFactory');

var InitializationFactory = require('./scaffold/InitializationFactory');

var PanZoomModel = require('./models/PanZoomModel');

var Line = require('./shapes/Line');

var pubsub = require('./pubsub/PubSub');

var Transform = require('./matrix/Transform');

var uuidV4 = require('uuid/v4'); // A $( document ).ready() block.


$(function () {
  var socket = io.connect();
  var UUID = uuidV4();

  function getRoomName() {
    var pathname = window.location.pathname;
    var fields = pathname.split('/'); //get the last element

    var room_name = fields[fields.length - 1];
    return room_name || 'default';
  } //


  socket.on('connect', function (data) {
    socket.emit('join', {
      name: 'anonymous',
      room_name: getRoomName(),
      uuid: UUID
    });
  }); //need to fire that to get the users who joined the room before the user who is joining in now
  //socket.emit('get-users', {room_name: getRoomName()}); 

  socket.on('all-users', function (data) {
    pubsub.emit('users_updated', {
      users: data,
      selfUUID: UUID
    });
  });
  var canvas = $('#paper'); //console.info(canvas.parent().width());
  //console.info(canvas.parent().height());
  //console.info(canvas.parent().outerWidth());
  //console.info(canvas.parent().outerHeight());

  var arbitraryOffsetWidth = 5; //offset from the right edge

  var arbitraryOffsetHeight = 5; // offset from the bottom

  canvas.width(canvas.parent().width() - arbitraryOffsetWidth);
  canvas.height(canvas.parent().height() - arbitraryOffsetHeight); //experimenting with canvas width and height ...

  $(document).ready(function () {
    $(window).resize(function (data) {
      var canvas = $('#paper');
      canvas.width(canvas.parent().width() - arbitraryOffsetWidth);
      canvas.height(canvas.parent().height() - arbitraryOffsetHeight);
      pubsub.emit('window_resized', {
        canvas: canvas
      }); //console.info(canvas.height());
      //console.info(canvas.parent().width());
      //console.info(canvas.parent().height());
      //canvas.width(canvas.parent().width()-30);
      //canvas.height(canvas.parent().height()-30);
    });
    var breakpointWidth = 768; //this Bootstrap's breakpoint to switch from Desktop to Mobile mode
    //click handler for buttons in the menu

    $("div[data-toggle='buttons'] label").on('click', function () {
      if ($(window).width() < breakpointWidth) {
        $('.navbar-toggle').click(); //collapse the navbar
      }
    }); //click handler for showing/hiding Preperties Sideber from the menu

    $('#nav-bar-toggle-sidebar').on('click', function () {
      $('#sidebar-wrapper').toggleClass("toggled");

      if ($(window).width() < breakpointWidth) {
        $('.navbar-toggle').click(); //toggle collapse of the navbar
      }
    }); //make sidebar draggable
    //$(function() {

    $("#sidebar-wrapper").draggable(); //});
  }); // This demo depends on the canvas element

  if (!('getContext' in document.createElement('canvas'))) {
    alert('Sorry, it looks like your browser does not support canvas!');
    return false;
  }

  var options = {};
  options.canvas = canvas;
  options.socket = socket;
  options.pubsub = pubsub;
  var context = options.canvas[0].getContext('2d'); //context.transform = new Transform(); // init

  context.transform = new Transform(this.context); //console.info(context);

  options.context = context;
  var panZoomModel = new PanZoomModel(options); // just a holder of informaiton

  var drawingStateModel = new DrawingStateModel(options);
  new InitializationFactory(options); // funny code;; TODO: re-factor , i mean why create new object when a static method will suffice

  var mouseHandler = new MouseHandler(canvas);
  var lastEmit = $.now(); //All of these socket.on events handlers are needed as one doesn't subscribe
  //to "socket" events on the client files. There is a dedicated pub/sub abstraction.

  socket.on('socket_panning', function (data) {
    //$.publish("socket_panning", data);
    pubsub.emit('socket_panning', data);
  });
  socket.on('zooming', function (data) {
    //$.publish("socket_zooming", data);
    pubsub.emit('socket_zooming', data);
  });
  socket.on('shapeDeleted', function (data) {
    //$.publish("shape_deleted", data);
    pubsub.emit('shape_deleted', data);
  });
  socket.on('shapeUpdated', function (data) {
    //$.publish("shape_updated", data);
    pubsub.emit('shape_updated', data);
  });
  socket.on('shapeCreated', function (data) {
    //$.publish("new_shape_created", data);
    pubsub.emit('new_shape_created', data);
  });
  /*
   * Currently drawn shape is sent to connected clients
   */

  socket.on('updateCurrentDrawingModel', function (data) {
    //$.publish("update_current_drawing_model", data);
    pubsub.emit('update_current_drawing_model', data);
  });
  /*
   * Mouse wheel zooming events handler
   * Not sure if this event should be here or somewhere else ...
   */

  canvas.on('mousewheel DOMMouseScroll', function (event) {
    var localEvent = window.event || event.originalEvent; // old IE support

    var direction = localEvent.detail < 0 || localEvent.wheelDelta > 0 ? 1 : -1;
    var zoomLevel = panZoomModel.getScale();
    var transformMatrixObject = EventFactory.getChangeTransformMatrixObject();
    transformMatrixObject.panDisplacement = panZoomModel.getPanDisplacement();
    transformMatrixObject.zoomL = direction < 0 ? 0.8 : 1.25;
    transformMatrixObject.zoomLevel = zoomLevel * transformMatrixObject.zoomL;
    var mCoords = mouseHandler.convertToCanvasCoords(localEvent.pageX, localEvent.pageY);
    var panDis = panZoomModel.getPanDisplacement();
    var origin = {};
    origin.x = (mCoords.x - panDis.x) / panZoomModel.getScale();
    origin.y = (mCoords.y - panDis.y) / panZoomModel.getScale();
    transformMatrixObject.origin = origin; //publish event for local client consumption
    //$.publish("client_zooming", transformMatrixObject);

    pubsub.emit('client_zooming', transformMatrixObject); //send event to the server
    //if ($.now() - lastEmit > 30) {

    socket.emit('socket_zoom', transformMatrixObject); //lastEmit = $.now();
    //}
  });
  /*
   * This object ets created wheneevr user clicks mouse down or touches the screen
   */

  function getMouseDownOrTouchObject(coords) {
    var mouseDownEventObject = EventFactory.getMouseDownEventObject();
    var mCoords = mouseHandler.convertToCanvasCoords(coords.pageX, coords.pageY);
    var panDis = panZoomModel.getPanDisplacement();
    var origin = {};
    origin.x = (mCoords.x - panDis.x) / panZoomModel.getScale();
    origin.y = (mCoords.y - panDis.y) / panZoomModel.getScale();
    mouseDownEventObject.mouseCoordsOverCanvasAdjusted = origin; //

    if (drawingStateModel.getDrawingMode() !== false && drawingStateModel.getDrawingMode() !== 0) {
      mouseDownEventObject.isDrawing = true;
      mouseDownEventObject.currentDrawingMode = drawingStateModel.getDrawingMode();
    }

    return mouseDownEventObject;
  }
  /*
   * User touched screen on a mobile device
   */


  canvas.on('touchstart', function (evt) {
    var event = evt.originalEvent;
    event.preventDefault();
    var mouseDownEventObject = getMouseDownOrTouchObject(event.targetTouches[0]);
    pubsub.emit('mouse_left_button_down', mouseDownEventObject);
  });
  /*
   * Another mouse event.
   * Perhaps it should have its own "repository"
   */

  canvas.on('mousedown', function (e) {
    e.preventDefault();

    switch (e.which) {
      case 1:
        var mouseDownEventObject = getMouseDownOrTouchObject(e);
        pubsub.emit('mouse_left_button_down', mouseDownEventObject);
        break;

      case 2:
        //Middle Mouse button pressed
        break;

      case 3:
        //Right Mouse button pressed
        break;

      default: //You have a strange Mouse!

    }
  });
  /*
   *
   */

  function reactToMouseMoveOrTouchMoveEventObject(coords) {
    var mouseEventObject = EventFactory.getMouseMoveEventObject();
    mouseEventObject.mouseCoordsOverCanvas = mouseHandler.convertToCanvasCoords(coords.pageX, coords.pageY);
    var panDisplacement = panZoomModel.getPanDisplacement();
    var adjustedMouseCoords = {};
    adjustedMouseCoords.x = (mouseEventObject.mouseCoordsOverCanvas.x - panDisplacement.x) / panZoomModel.getScale();
    adjustedMouseCoords.y = (mouseEventObject.mouseCoordsOverCanvas.y - panDisplacement.y) / panZoomModel.getScale();
    mouseEventObject.mouseCoordsOverCanvasAdjusted = adjustedMouseCoords;
    mouseEventObject.panDisplacement = panDisplacement;
    mouseEventObject.zoomLevel = panZoomModel.getScale(); //

    if (drawingStateModel.isDrawing() === true) {
      mouseEventObject.isDrawing = true;
      pubsub.emit('mouse_move', mouseEventObject);
    } else if (drawingStateModel.isPanning() === true) {
      mouseEventObject.origin = panZoomModel.getMouseDownOrigin();
      pubsub.emit('client_panning', mouseEventObject);
      socket.emit('socket_pan', mouseEventObject);
    } else {
      mouseEventObject.isDrawing = false; //it is default value

      mouseEventObject.isPanning = false;
      pubsub.emit('mouse_move', mouseEventObject);
    }
  }
  /*
   * User moves finger over the canvas
   */


  canvas.on('touchmove', function (evt) {
    var event = evt.originalEvent;
    event.preventDefault();
    reactToMouseMoveOrTouchMoveEventObject(event.targetTouches[0]);
  });
  /*
   * Yet another mouse event handler, the more comments I write about these
   * the more I think they should be refactored into its own class
   */

  canvas.on('mousemove', function (e) {
    reactToMouseMoveOrTouchMoveEventObject(e);
  });
  /*
   * Another mouse event handler ...
   */

  canvas.bind('mouseleave', function (e) {
    //$.publish("mouse_left_canvas_boundaries", {});
    pubsub.emit('mouse_left_canvas_boundaries');
  });
  /*
   * User lifted the finger off the canvas
   */

  canvas.on('touchend', function (evt) {
    var event = evt.originalEvent;
    event.preventDefault(); //pubsub.emit('mouse_left_button_up');
    //const mouseDownEventObject = getMouseDownOrTouchObject(event.targetTouches[0]);

    pubsub.emit('mouse_left_button_down', null);
  });
  /*
   * Fairly obvious mouse event hadler
   */

  canvas.on('mouseup', function (e) {
    switch (e.which) {
      //which mouse button
      case 1:
        //broadcast that left mouse was released  
        pubsub.emit('mouse_left_button_up');
        break;

      case 2:
        //Middle Mouse button released.
        break;

      case 3:
        //Right Mouse button released.
        break;

      default: //You have a strange Mouse!

    }
  }); // Remove inactive clients after 10 seconds of inactivity

  /*
  setInterval(function () {
        for (ident in clients) {
          if ($.now() - clients[ident].updated > 10000) {
              // Last update was more than 10 seconds ago. 
              // This user has probably closed the page
              cursors[ident].remove();
              delete clients[ident];
              delete cursors[ident];
          }
      }
    }, 10000);
  */
});

},{"./actions/MouseHandler":2,"./events/EventFactory":3,"./matrix/Transform":6,"./models/DrawingStateModel":11,"./models/PanZoomModel":14,"./pubsub/PubSub":22,"./scaffold/InitializationFactory":23,"./shapes/Line":27,"uuid/v4":37}],25:[function(require,module,exports){
"use strict";

var Shape = require('./Shape');

var calculateDistance = require('../utils/utils').calculateDistance; //var utils = require('./utils');
//var isCollinear = require('./utils').isCollinear;
//var isPointOnLine = require('./utils').isPointOnLine;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function Circle(centerX, centerY, toX, toY) {
  Shape.call(this); //call super constructor

  this.init = function (centerX, centerY, radius) {
    this.centerX = centerX;
    this.centerY = centerY;
    this.radius = radius;

    if (typeof Path2D === 'function') {
      this.path = new Path2D();
    } else {
      console.log("Path2D is not defined");
    }

    this.path.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI);
  };

  this.init(centerX, centerY, toX, toY);

  this.draw = function (context) {
    context.beginPath();
    context.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI);
    context.strokeStyle = '#000000';

    if (this.getStrokeStyle() !== null) {
      context.strokeStyle = this.getStrokeStyle();
    }

    context.stroke();
  };
  /*
   * Before any shape gets created this function is called to determine if the current shape is valid
   * i.e if length > 0 or radius > 0
   */


  this.isValid = function () {
    if (this.radius > 0) {
      return true;
    }

    return false;
  };

  this.isPointOver = function (context, x, y) {
    // circle formula
    // (x - this.centerX)^2 + (y - this.centerY)^2 = this.radius^2; // if we add margin +- margin;
    var firstParam = Math.pow(x - this.centerX, 2);
    var secondParam = Math.pow(y - this.centerY, 2);
    var lhs = firstParam + secondParam; // left hand side

    var rhs = this.radius * this.radius; // right hand side
    //console.info( lhs + " -- " + rhs)

    var delta = Math.abs(lhs - rhs);
    var margin = 400;

    if (delta < margin) {
      return true;
    }

    return false; //return context.isPointInStroke(this.path, x, y);
  };

  this.drawSelected = function (context) {
    dragHandles = []; //img.onload = function () {

    context.drawImage(imgDragHandle, this.centerX - 5, this.centerY - 5);
    var handle = {};
    handle.origin = {};
    handle.origin.x = this.originX;
    handle.origin.y = this.originY;
    dragHandles.push(handle); //draw handles on circumference

    for (var i = 0; i < 4; i = i + 0.5) {
      var result = {};
      var angle = i * Math.PI;
      result.Y = Math.round(this.centerY + this.radius * Math.sin(angle));
      result.X = Math.round(this.centerX + this.radius * Math.cos(angle));
      context.drawImage(imgDragHandle, result.X - 5, result.Y - 5);
      handle = {};
      handle.origin = {};
      handle.origin.x = result.X;
      handle.origin.y = result.Y;
      dragHandles.push(handle);
    }

    context.beginPath();
    context.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI);
    context.strokeStyle = '#ff0000';
    context.stroke();
    context.strokeStyle = '#000000';
  };

  this.toJSON = function () {
    //calling parent class to get common properties like strokeStyle, UUID
    var obj = Circle.prototype.toJSON.call(this);
    obj.type = "Circle";
    obj.centerX = this.centerX;
    obj.centerY = this.centerY;
    obj.radius = this.radius;
    obj.strokeStyle = this.getStrokeStyle();
    return obj;
  };

  this.toString = function () {
    var numberOfDecimalPoints = 100;
    var retval = "<div class='panel-heading'>Circle</div>" + "<div class='panel-body'>" + "centerX: " + Math.round(this.centerX * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />centerY: " + Math.round(this.centerY * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />radius: " + Math.round(this.radius * numberOfDecimalPoints) / numberOfDecimalPoints + "</div>";
    return retval;
  };
  /*
   * This get s displayed in Property popup
   */


  this.toHTMLString = function () {
    var numberOfDecimalPoints = 100; //"X: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +

    var retval = "<div class='panel-body'>" + "<div class='panel panel-info'>" + "<div class='panel-heading'>Origin</div>" + "<div class='panel-body'>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>X</span>" + "<input name='centerX' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + Math.round(this.centerX * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Y</span>" + "<input name='centerY' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + Math.round(this.centerY * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "</div>" + "</div>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Radius</span>" + "<input name='radius' type='text' class='form-control' placeholder='radius' aria-describedby='sizing-addon3' value='" + Math.round(this.radius * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Stroke Style</span>" + "<input name='strokeStyle' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + this.getStrokeStyle() + "'>" + "</div>";
    "</div>";
    return retval;
  };

  this.update = function (newEndpointX, newEndpointY) {
    this.radius = calculateDistance(this.centerX, this.centerY, newEndpointX, newEndpointY);
  };
}

Circle.prototype = Object.create(Shape.prototype, {
  constructor: {
    value: Circle
  }
});
module.exports = Circle;

},{"../utils/utils":33,"./Shape":29}],26:[function(require,module,exports){
"use strict";

var Shape = require('./Shape');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function FreeForm(points) {
  Shape.call(this); //call super constructor

  this.init = function (points) {
    this.points = points;

    if (typeof Path2D === 'function') {
      this.path = new Path2D();
    } else {
      console.log("Path2D is not defined");
    }

    for (var i = 0; i < this.points.length - 1; i++) {
      this.path.moveTo(this.points[i].x, this.points[i].y);
      this.path.lineTo(this.points[i + 1].x, this.points[i + 1].y);
    }
  };

  this.init(points);

  this.draw = function (context) {
    context.beginPath();

    for (var i = 0; i < this.points.length - 1; i++) {
      context.moveTo(this.points[i].x, this.points[i].y);
      context.lineTo(this.points[i + 1].x, this.points[i + 1].y);
    }

    context.strokeStyle = '#000000';

    if (this.getStrokeStyle() !== null) {
      context.strokeStyle = this.getStrokeStyle();
    }

    context.stroke();
  };
  /*
   * Before freeform gets created make sure that
   * - either there is more than 2 points
   * - or if there are 2 points only then they are not the same
   */


  this.isValid = function () {
    if (this.points.length > 2) {
      return true;
    } else if (this.points.length === 2) {
      if (Math.abs(this.points[0].x - this.points[1].x) > 0 || Math.abs(this.points[0].y - this.points[1].y) > 0) {
        return true;
      }
    }

    return false;
  };

  this.isPointOver = function (context, x, y) {
    return context.isPointInStroke(this.path, x, y); //return context.isPointInPath(this.path, x, y);
  };

  this.drawSelected = function (context) {
    dragHandles = [];
    context.drawImage(imgDragHandle, this.points[0].x - 5, this.points[0].y - 5);
    var handle = {};
    handle.origin = {};
    handle.origin.x = this.originX;
    handle.origin.y = this.originY;
    dragHandles.push(handle);
    context.drawImage(imgDragHandle, this.points[this.points.length - 1].x - 5, this.points[this.points.length - 1].y - 5);
    handle = {};
    handle.origin = {};
    handle.origin.x = this.endX;
    handle.origin.y = this.endY;
    dragHandles.push(handle);
    context.beginPath();

    for (var i = 0; i < this.points.length - 1; i++) {
      context.moveTo(this.points[i].x, this.points[i].y);
      context.lineTo(this.points[i + 1].x, this.points[i + 1].y);
    }

    context.strokeStyle = '#ff0000';
    context.stroke();
    context.strokeStyle = '#000000';
  };

  this.toJSON = function () {
    //calling parent class to get common properties like strokeStyle, UUID
    var obj = FreeForm.prototype.toJSON.call(this);
    obj.type = "FreeForm";
    obj.points = this.points;
    obj.strokeStyle = this.getStrokeStyle();
    return obj;
  };

  this.toString = function () {
    var numberOfDecimalPoints = 100;
    var retval = "<div class='panel-heading'>Freeform</div>" + "<div class='panel-body'>" + "#points: " + this.points.length + "<br />ps[0]X: " + Math.round(this.points[0].x * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />ps[0]Y: " + Math.round(this.points[0].y * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />ps[last]X: " + Math.round(this.points[this.points.length - 1].x * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />ps[last]Y: " + Math.round(this.points[this.points.length - 1].y * numberOfDecimalPoints) / numberOfDecimalPoints + "</div>";
    return retval;
  };
  /*
   * This get s displayed in Property popup
   */


  this.toHTMLString = function () {
    var numberOfDecimalPoints = 100; //"X: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +

    var retval = "<input name='points' type='hidden' value='" + JSON.stringify(this.points) + "' />" + "<h3><span class='label label-default'>Need some mechanism to allow editing of " + this.points.length + " points in Freeform</span></h3> <br />" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Stroke Style</span>" + "<input name='strokeStyle' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + this.getStrokeStyle() + "'>" + "</div>";
    "</div>";
    return retval;
  };

  this.update = function (newEndpointX, newEndpointY) {
    var newPoint = {};
    newPoint.x = newEndpointX;
    newPoint.y = newEndpointY;
    this.points.push(newPoint);
  };
} //the second parameter to make instanceof work


FreeForm.prototype = Object.create(Shape.prototype, {
  constructor: {
    value: FreeForm
  }
});
module.exports = FreeForm;

},{"./Shape":29}],27:[function(require,module,exports){
'use strict';

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Shape = require('./Shape'); //var utils = require('./utils');


var isCollinear = require('../utils/utils').isCollinear; //var calculateDistance = require('./utils').calculateDistance;
//var isPointOnLine = require('./utils').isPointOnLine;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var Line =
/*#__PURE__*/
function (_Shape) {
  _inherits(Line, _Shape);

  function Line(startX, startY, endX, endY) {
    var _this;

    _classCallCheck(this, Line);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Line).call(this));
    _this.originX = startX;
    _this.originY = startY;

    if (typeof Path2D === 'function') {
      _this.path = new Path2D();
    } else {
      console.log("Path2D is not defined");
    }

    _this.endX = endX;
    _this.endY = endY;

    _this.path.moveTo(_this.originX, _this.originY);

    _this.path.lineTo(_this.endX, _this.endY);

    return _this;
  }
  /*
   * Before any shape gets created this function is called to determine if the current shape is valid
   * i.e if length > 0 or radius > 0
   */


  _createClass(Line, [{
    key: "isValid",
    value: function isValid() {
      if (Math.abs(this.originX - this.endX) > 0 || Math.abs(this.originY - this.endY) > 0) {
        return true;
      }

      return false;
    }
    /*
     * horizontal and vertical lines look faded on canvas
     * http://stackoverflow.com/questions/10373695/drawing-lines-in-canvas-but-the-last-ones-are-faded
     */

  }, {
    key: "draw",
    value: function draw(context, lineWidth) {
      context.strokeStyle = '#000000';

      if (this.getStrokeStyle() !== null) {
        context.strokeStyle = this.getStrokeStyle();
      }

      var yEnd1 = this.originY;
      var yEnd2 = this.endY;

      if (yEnd1 === yEnd2) {
        //horizontal line
        yEnd1 = Math.floor(yEnd1) + 0.5;
        yEnd2 = yEnd1;
      }

      var xEnd1 = this.originX;
      var xEnd2 = this.endX;

      if (xEnd1 === xEnd2) {
        //horizontal line
        xEnd1 = Math.floor(xEnd1) + 0.5;
        xEnd2 = xEnd1;
      }

      context.beginPath();

      if (lineWidth) {
        context.lineWidth = lineWidth;
      }

      context.moveTo(xEnd1, yEnd1);
      context.lineTo(xEnd2, yEnd2);

      if (this.label !== null && _typeof(this.label) !== undefined) {
        if (this.getLabelOrientation() === "vertical") {
          /*
          context.transform.save();
          console.info(xEnd2 + " :: " +yEnd2);
          context.transform.translate(xEnd2, yEnd2);
          context.transform.rotate(-Math.PI/2);
          context.strokeText(this.label, 0, 3);
          context.transform.restore();
          */
          context.transform.save();
          context.font = "10px Arial, sans-serif";
          context.transform.zoomOnPoint(1, 1 / this.getModelScale(), xEnd2, yEnd2);
          context.strokeStyle = 'black';
          context.lineWidth = 1;
          context.fillText(this.label, xEnd2 - 9, yEnd2 + 8);
          context.transform.restore();
        } else {
          context.transform.save();
          context.font = "10px Arial, sans-serif";
          context.transform.zoomOnPoint(1 / this.getModelScale(), 1, xEnd2, yEnd2);
          context.strokeStyle = 'black';
          context.lineWidth = 1;
          context.fillText(this.label, xEnd2, yEnd2 + 4);
          context.transform.restore();
        }
      }

      context.stroke();
    }
  }, {
    key: "isPointOver",
    value: function isPointOver(context, x, y) {
      /*return context.isPointInStroke(this.path, x, y);*/
      var crossproduct = isCollinear(x, y, this.originX, this.originY, this.endX, this.endY); // crossproduct === 0 in the perfext vector world, here we need threshold

      var margin = 400;

      if (crossproduct < margin && crossproduct > -margin && (this.originX <= x && x <= this.endX || this.endX <= x && x <= this.originX) && (this.originY <= y && y <= this.endY || this.endY <= y && y <= this.originY)) {
        return true;
      }

      return false;
    }
  }, {
    key: "drawSelected",
    value: function drawSelected(context) {
      context.beginPath();
      dragHandles = []; //img.onload = function () {

      context.drawImage(imgDragHandle, this.originX - 5, this.originY - 5);
      var handle = {};
      handle.origin = {};
      handle.origin.x = this.originX;
      handle.origin.y = this.originY;
      dragHandles.push(handle);
      context.drawImage(imgDragHandle, this.endX - 5, this.endY - 5);
      handle = {};
      handle.origin = {};
      handle.origin.x = this.endX;
      handle.origin.y = this.endY;
      dragHandles.push(handle); //};
      //selectedShapeIndex = i;
      //shapesPoc[i].trigger("mouseIsOver");

      context.moveTo(this.originX, this.originY);
      context.lineTo(this.endX, this.endY);
      context.strokeStyle = '#ff0000';
      context.stroke();
      context.strokeStyle = '#000000';
    }
  }, {
    key: "toJSON",
    value: function toJSON() {
      //calling parent class to get common properties like strokeStyle, UUID
      //const obj = Line.prototype.toJSON.call(this);
      var obj = _get(_getPrototypeOf(Line.prototype), "toJSON", this).call(this);

      obj.type = "Line";
      obj.originX = this.originX;
      obj.originY = this.originY;
      obj.endX = this.endX;
      obj.endY = this.endY;
      return obj;
    }
  }, {
    key: "toString",
    value: function toString() {
      var numberOfDecimalPoints = 100;
      var retval = "<div class='panel-heading'>Line</div>" + "<div class='panel-body'>" + "origX: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />origY: " + Math.round(this.originY * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />endX: " + Math.round(this.endX * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />endY: " + Math.round(this.endY * numberOfDecimalPoints) / numberOfDecimalPoints + "</div>";
      return retval;
    }
    /*
     * This get s displayed in Property popup
     */

  }, {
    key: "toHTMLString",
    value: function toHTMLString() {
      var numberOfDecimalPoints = 100; //"X: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +

      var retval = "<div class='panel-body'>" + "<div class='panel panel-info'>" + "<div class='panel-heading'>Origin</div>" + "<div class='panel-body'>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>X</span>" + "<input name='originX' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Y</span>" + "<input name='originY' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + Math.round(this.originY * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "</div>" + "</div>" + "<div class='panel panel-info'>" + "<div class='panel-heading'>End</div>" + "<div class='panel-body'>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>X</span>" + "<input name='endX' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + Math.round(this.endX * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Y</span>" + "<input name='endY' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + Math.round(this.endY * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "</div>" + "</div>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Stroke Style</span>" + "<input name='strokeStyle' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + this.getStrokeStyle() + "'>" + "</div>" + "</div>";
      return retval;
    }
    /*
     * When shape is being drawn some sort of parameter changes
     * in this case end of the line. So the to be drawn the end of the line needs to be updated
     */

  }, {
    key: "update",
    value: function update(newEndpointX, newEndpointY) {
      this.endX = newEndpointX;
      this.endY = newEndpointY;
    }
  }]);

  return Line;
}(Shape);
/*
Line.prototype = Object.create(Shape.prototype, {
    constructor: {
        value: Line
    }
});
*/
//Object.setPrototypeOf(Line.prototype, Shape);// If you do not do this you will get a TypeError when you invoke speak


module.exports = Line;

},{"../utils/utils":33,"./Shape":29}],28:[function(require,module,exports){
'use strict';

var Shape = require('./Shape');
/* 
 * Point class. 
 * Point2D that is cause there is no need for 3D in this fun little app
 */


function Point(startX, startY) {
  Shape.call(this); //call super constructor

  this.init = function (startX, startY) {
    this.X = startX;
    this.Y = startY;

    if (typeof Path2D === 'function') {
      this.path = new Path2D();
    } else {
      console.log("Path2D is not defined");
    }

    this.DrawnRectangleSize = 3; //in pixels; including one pixel for the point itself;

    this.DrawnRectangleSizeWhenSelected = 3; //in pixels; including one pixel for the point itself;
    // when mouse over the point how many pixels will be padded around the point
    // to determine if point is selected

    this.SensitivityPadding = 1;
    var area = this.SensitivityPadding * 2 + 1; //total square side length of the sensitive area around pixel

    this.path.rect(this.X - this.SensitivityPadding, this.Y - this.SensitivityPadding, area, area);
  };

  this.init(startX, startY);
  /*
   * http://stackoverflow.com/questions/7812514/drawing-a-dot-on-html5-canvas
   */

  this.draw = function (context, lineWidth) {
    context.strokeStyle = '#000000';

    if (this.getStrokeStyle() !== null) {
      context.strokeStyle = this.getStrokeStyle();
    }

    context.beginPath();

    if (lineWidth) {
      context.lineWidth = lineWidth;
    }

    var offset = Math.floor(this.DrawnRectangleSize / 2);
    context.fillRect(this.X - offset, this.Y - offset, this.DrawnRectangleSize, this.DrawnRectangleSize);
    context.stroke();
  };

  this.isPointOver = function (context, x, y) {
    var deltaX = Math.abs(x - this.X);
    var deltaY = Math.abs(y - this.Y);
    var margin = 3; //console.info(deltaX + " - " + deltaY);

    if (deltaX < margin && deltaY < margin) {
      return true;
    }

    return false; //return context.isPointInPath(this.path, x, y);
  };

  this.drawSelected = function (context) {
    context.beginPath();
    dragHandles = []; //img.onload = function () {

    context.drawImage(imgDragHandle, this.X - 4.5, this.Y - 4.5);
    var handle = {};
    handle.origin = {};
    handle.origin.x = this.originX;
    handle.origin.y = this.originY;
    dragHandles.push(handle);
    context.fillStyle = '#ff0000';
    var offset = Math.floor(this.DrawnRectangleSizeWhenSelected / 2);
    context.fillRect(this.X - offset, this.Y - offset, this.DrawnRectangleSizeWhenSelected, this.DrawnRectangleSizeWhenSelected);
    context.fillStyle = '#000000';
  };

  this.toJSON = function () {
    //calling parent class to get common properties like strokeStyle, UUID
    var obj = Point.prototype.toJSON.call(this);
    obj.type = "Point";
    obj.X = this.X;
    obj.Y = this.Y;
    obj.strokeStyle = this.getStrokeStyle();
    return obj;
  };

  this.toString = function () {
    var numberOfDecimalPoints = 100;
    var retval = "<div class='panel-heading'>Point</div>" + "<div class='panel-body'>" + "X: " + Math.round(this.X * numberOfDecimalPoints) / numberOfDecimalPoints + "<br />Y: " + Math.round(this.Y * numberOfDecimalPoints) / numberOfDecimalPoints + "</div>";
    return retval;
  };
  /*
   * This get s displayed in Property popup
   */


  this.toHTMLString = function () {
    var numberOfDecimalPoints = 100; //"X: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +

    var retval = "<div class='panel-body'>" + "<div class='panel panel-info'>" + "<div class='panel-heading'>Origin</div>" + "<div class='panel-body'>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>X</span>" + "<input name='X' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + Math.round(this.X * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Y</span>" + "<input name='Y' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + Math.round(this.Y * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" + "</div>" + "</div>" + "</div>" + "<div class='input-group input-group-sm'>" + "<span class='input-group-addon' id='sizing-addon3'>Stroke Style</span>" + "<input name='strokeStyle' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" + this.getStrokeStyle() + "'>" + "</div>" + "</div>";
    return retval;
  };

  this.update = function (newEndpointX, newEndpointY) {
    this.X = newEndpointX;
    this.Y = newEndpointY;
  };
}

Point.prototype = Object.create(Shape.prototype, {
  constructor: {
    value: Point
  }
});
module.exports = Point;

},{"./Shape":29}],29:[function(require,module,exports){
'use strict';

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var uuidV4 = require('uuid/v4');
/* 
 *
 */


function Shape() {
  var that = this;
  this.UUID = uuidV4(); //https://www.w3schools.com/tags/ref_canvas.asp look at here for properties

  this.strokeStyle = "#000000";
  this.lineWidth = "1"; //shape line width, in pixels

  this.label = null;
  this.labelOrientation = "horizontal";
  this.modelScale = 1;
}

Shape.prototype.setStrokeStyle = function (color) {
  if (color) {
    this.strokeStyle = color;
  }
};

Shape.prototype.getStrokeStyle = function () {
  return this.strokeStyle || null;
};
/*
 * Used for creation of objects from JSON 
 */


Shape.prototype.setUUID = function (UUID) {
  if (UUID) {
    this.UUID = UUID;
  }
};

Shape.prototype.getUUID = function () {
  return this.UUID || null;
};

Shape.prototype.setLabel = function (label) {
  if (label !== null && _typeof(label) !== undefined) {
    this.label = label;
  }
};

Shape.prototype.getLabel = function () {
  return this.label || null;
};

Shape.prototype.setModelScale = function (modelScale) {
  this.modelScale = modelScale;
};

Shape.prototype.getModelScale = function () {
  return this.modelScale || 1;
};

Shape.prototype.setLabelOrientation = function (labelOrientation) {
  this.labelOrientation = labelOrientation;
};

Shape.prototype.getLabelOrientation = function () {
  return this.labelOrientation || null;
};

Shape.prototype.draw = function (context) {
  console.log("draw is not implemented here");
};

Shape.prototype.isPointOver = function (ctx, x, y) {
  console.log("isPointOver is not implemented here");
};

Shape.prototype.drawSelected = function (x, y) {
  console.log("drawSelected is not implemented here");
};

Shape.prototype.toJSON = function (x, y) {
  var obj = {};
  obj.strokeStyle = this.getStrokeStyle();
  obj.UUID = this.getUUID();
  obj.label = this.getLabel();
  return obj;
};
/*
 * Before any shape gets created this function is called to determine if the current shape is valid
 * i.e if length > 0 or radius > 0
 */


Shape.prototype.isValid = function () {
  return true;
};
/*
 * This get s displayed in Property popup
 */


Shape.prototype.toHTMLString = function (x, y) {
  console.log("toHTMLString is not implemented here");
};
/*
 * Updates the shape for drawing. Making assumptions that this is a needed
 * 'update' method. I mean the naming is pretty generic... 
 */


Shape.prototype.update = function (x, y) {
  console.log("update is not implemented here");
};

module.exports = Shape;

},{"uuid/v4":37}],30:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* 
 * Generate objects based on type in JSON string
 */
var Circle = require('../shapes/Circle');

var Line = require('../shapes/Line');

var FreeForm = require('../shapes/FreeForm');

var Point = require('../shapes/Point');

var ShapeFactory =
/*#__PURE__*/
function () {
  function ShapeFactory() {
    _classCallCheck(this, ShapeFactory);
  }

  _createClass(ShapeFactory, null, [{
    key: "createFromJSON",
    value: function createFromJSON(jsonRepresentationOfShape) {
      return ShapeFactory.create(jsonRepresentationOfShape.type, jsonRepresentationOfShape);
    }
  }, {
    key: "create",
    value: function create(shapeType, params) {
      if (ShapeFactory.isCircle(shapeType)) {
        var circle = new Circle(params.centerX, params.centerY, params.radius);
        circle.setStrokeStyle(params.strokeStyle);
        circle.setUUID(params.UUID);
        return circle;
      } else if (ShapeFactory.isLine(shapeType)) {
        var line = new Line(params.originX, params.originY, params.endX, params.endY);
        line.setStrokeStyle(params.strokeStyle);
        line.setUUID(params.UUID);
        line.setLabel(params.label);
        return line;
      } else if (ShapeFactory.isFreeform(shapeType)) {
        var freeform = new FreeForm(params.points);
        freeform.setStrokeStyle(params.strokeStyle);
        freeform.setUUID(params.UUID);
        return freeform;
      } else if (ShapeFactory.isPoint(shapeType)) {
        var point = new Point(params.X, params.Y);
        point.setStrokeStyle(params.strokeStyle);
        point.setUUID(params.UUID);
        return point;
      } else {
        console.log(shapeType + ": this Shape type is not implemented in the factory");
      }
    }
    /*
     * This method only takes origin parameters and providex defaults for end point and radius
     */

  }, {
    key: "initialize",
    value: function initialize(shapeType, params) {
      if (ShapeFactory.isCircle(shapeType)) {
        return new Circle(params.originX, params.originY, 0);
      } else if (ShapeFactory.isLine(shapeType)) {
        var line = new Line(params.originX, params.originY, params.originX, params.originY); //line.setStrokeStyle(strokeStyle);

        return line;
      } else if (ShapeFactory.isFreeform(shapeType)) {
        var points = [];
        var origin = {};
        origin.x = params.originX;
        origin.y = params.originY;
        points.push(origin);
        return new FreeForm(points);
      } else if (ShapeFactory.isPoint(shapeType)) {
        return new Point(params.originX, params.originY);
      } else {
        console.log(shapeType + ": this Shape type is not implemented in the initialization factory");
      }
    }
    /*
     * Utility function to determine if param (string) is a circle
     */

  }, {
    key: "isCircle",
    value: function isCircle(param) {
      if (param === "Circle" || param === "circle") {
        return true;
      }

      return false;
    }
    /*
     * Utility function to determine if param (string) is a line
     */

  }, {
    key: "isLine",
    value: function isLine(param) {
      if (param === "Line" || param === "straightLine") {
        return true;
      }

      return false;
    }
    /*
     * Utility function to determine if param (string) is a freeform
     */

  }, {
    key: "isFreeform",
    value: function isFreeform(param) {
      if (param === "FreeForm" || param === "freeForm") {
        return true;
      }

      return false;
    }
    /*
     * Utility function to determine if param (string) is a point
     */

  }, {
    key: "isPoint",
    value: function isPoint(param) {
      if (param === "Point" || param === "point") {
        return true;
      }

      return false;
    }
  }]);

  return ShapeFactory;
}();

module.exports = ShapeFactory;

},{"../shapes/Circle":25,"../shapes/FreeForm":26,"../shapes/Line":27,"../shapes/Point":28}],31:[function(require,module,exports){
'use strict';
/*
 * Parent class abstraction for Shape storage.
 * The idea is that referring to shapes in an array by index might be a little too brittle
 * It might be a mre consistent approach to refer to shape by UUID
 * and thus an associative array (object) a Map might be needed
 */

function ShapeStore() {}

ShapeStore.prototype.add = function (obj) {
  console.log("add is not implemented here");
};

ShapeStore.prototype.get = function () {
  console.log("get is not implemented here");
};

ShapeStore.prototype["delete"] = function () {
  console.log("delete is not implemented here");
};

ShapeStore.prototype.size = function () {
  console.log("size is not implemented here");
};
/*
 * Useful for debugging
 */


ShapeStore.prototype.returnRawData = function () {
  console.log("size is not implemented here");
};

module.exports = ShapeStore;

},{}],32:[function(require,module,exports){
'use strict';

var ShapeStore = require('./ShapeStore');
/*
 * Implementation of the ShapeStore that urelies on [] for storage
 */


function ShapeStoreArray() {
  ShapeStore.call(this); //call super constructor

  this.init = function () {
    this.shapeStore = [];
  };

  this.init();

  this.add = function (obj) {
    this.shapeStore.push(obj);
  };

  this.get = function (index) {
    return this.shapeStore[index];
  };

  this["delete"] = function (index) {
    this.shapeStore.splice(index, 1);
  };
  /*
   * VEry specific to Arrays function 
   */


  this.updateByIndex = function (index, newShape) {
    this.shapeStore[index] = newShape;
  };

  this.size = function () {
    return this.shapeStore.length;
  };

  this.returnRawData = function () {
    return this.shapeStore;
  };
}

ShapeStoreArray.prototype = Object.create(ShapeStore.prototype, {
  constructor: {
    value: ShapeStoreArray
  }
});
module.exports = ShapeStoreArray;

},{"./ShapeStore":31}],33:[function(require,module,exports){
"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
exports.isCollinear = function isCollinear(x1, y1, x2, y2, x3, y3) {
  var crossproduct = (y1 - y2) * (x3 - x2) - (x1 - x2) * (y3 - y2);
  return crossproduct;
};

exports.calculateDistance = function calculateDistance(x1, y1, x2, y2) {
  var distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
  return distance;
};

exports.isPointOnLine = function isPointOnLine(x1, y1, x2, y2, x3, y3) {
  var crossproduct = isCollinear(x1, y1, x2, y2, x3, y3); // crossproduct === 0 in the perfext vector world

  if (crossproduct < 200 && crossproduct > -200 && (x2 <= x1 && x1 <= x3 || x3 <= x1 && x1 <= x2) && (y2 <= y1 && y1 <= y3 || y3 <= y1 && y1 <= y2)) {
    return true;
  }

  return false;
};

},{}],34:[function(require,module,exports){
/*! decimal.js v7.5.1 https://github.com/MikeMcl/decimal.js/LICENCE */
;(function (globalScope) {
  'use strict';


  /*
   *  decimal.js v7.5.1
   *  An arbitrary-precision Decimal type for JavaScript.
   *  https://github.com/MikeMcl/decimal.js
   *  Copyright (c) 2017 Michael Mclaughlin <M8ch88l@gmail.com>
   *  MIT Licence
   */


  // -----------------------------------  EDITABLE DEFAULTS  ------------------------------------ //


    // The maximum exponent magnitude.
    // The limit on the value of `toExpNeg`, `toExpPos`, `minE` and `maxE`.
  var EXP_LIMIT = 9e15,                      // 0 to 9e15

    // The limit on the value of `precision`, and on the value of the first argument to
    // `toDecimalPlaces`, `toExponential`, `toFixed`, `toPrecision` and `toSignificantDigits`.
    MAX_DIGITS = 1e9,                        // 0 to 1e9

    // Base conversion alphabet.
    NUMERALS = '0123456789abcdef',

    // The natural logarithm of 10 (1025 digits).
    LN10 = '2.3025850929940456840179914546843642076011014886287729760333279009675726096773524802359972050895982983419677840422862486334095254650828067566662873690987816894829072083255546808437998948262331985283935053089653777326288461633662222876982198867465436674744042432743651550489343149393914796194044002221051017141748003688084012647080685567743216228355220114804663715659121373450747856947683463616792101806445070648000277502684916746550586856935673420670581136429224554405758925724208241314695689016758940256776311356919292033376587141660230105703089634572075440370847469940168269282808481184289314848524948644871927809676271275775397027668605952496716674183485704422507197965004714951050492214776567636938662976979522110718264549734772662425709429322582798502585509785265383207606726317164309505995087807523710333101197857547331541421808427543863591778117054309827482385045648019095610299291824318237525357709750539565187697510374970888692180205189339507238539205144634197265287286965110862571492198849978748873771345686209167058',

    // Pi (1025 digits).
    PI = '3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456485669234603486104543266482133936072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491298336733624406566430860213949463952247371907021798609437027705392171762931767523846748184676694051320005681271452635608277857713427577896091736371787214684409012249534301465495853710507922796892589235420199561121290219608640344181598136297747713099605187072113499999983729780499510597317328160963185950244594553469083026425223082533446850352619311881710100031378387528865875332083814206171776691473035982534904287554687311595628638823537875937519577818577805321712268066130019278766111959092164201989380952572010654858632789',


    // The initial configuration properties of the Decimal constructor.
    Decimal = {

      // These values must be integers within the stated ranges (inclusive).
      // Most of these values can be changed at run-time using the `Decimal.config` method.

      // The maximum number of significant digits of the result of a calculation or base conversion.
      // E.g. `Decimal.config({ precision: 20 });`
      precision: 20,                         // 1 to MAX_DIGITS

      // The rounding mode used when rounding to `precision`.
      //
      // ROUND_UP         0 Away from zero.
      // ROUND_DOWN       1 Towards zero.
      // ROUND_CEIL       2 Towards +Infinity.
      // ROUND_FLOOR      3 Towards -Infinity.
      // ROUND_HALF_UP    4 Towards nearest neighbour. If equidistant, up.
      // ROUND_HALF_DOWN  5 Towards nearest neighbour. If equidistant, down.
      // ROUND_HALF_EVEN  6 Towards nearest neighbour. If equidistant, towards even neighbour.
      // ROUND_HALF_CEIL  7 Towards nearest neighbour. If equidistant, towards +Infinity.
      // ROUND_HALF_FLOOR 8 Towards nearest neighbour. If equidistant, towards -Infinity.
      //
      // E.g.
      // `Decimal.rounding = 4;`
      // `Decimal.rounding = Decimal.ROUND_HALF_UP;`
      rounding: 4,                           // 0 to 8

      // The modulo mode used when calculating the modulus: a mod n.
      // The quotient (q = a / n) is calculated according to the corresponding rounding mode.
      // The remainder (r) is calculated as: r = a - n * q.
      //
      // UP         0 The remainder is positive if the dividend is negative, else is negative.
      // DOWN       1 The remainder has the same sign as the dividend (JavaScript %).
      // FLOOR      3 The remainder has the same sign as the divisor (Python %).
      // HALF_EVEN  6 The IEEE 754 remainder function.
      // EUCLID     9 Euclidian division. q = sign(n) * floor(a / abs(n)). Always positive.
      //
      // Truncated division (1), floored division (3), the IEEE 754 remainder (6), and Euclidian
      // division (9) are commonly used for the modulus operation. The other rounding modes can also
      // be used, but they may not give useful results.
      modulo: 1,                             // 0 to 9

      // The exponent value at and beneath which `toString` returns exponential notation.
      // JavaScript numbers: -7
      toExpNeg: -7,                          // 0 to -EXP_LIMIT

      // The exponent value at and above which `toString` returns exponential notation.
      // JavaScript numbers: 21
      toExpPos:  21,                         // 0 to EXP_LIMIT

      // The minimum exponent value, beneath which underflow to zero occurs.
      // JavaScript numbers: -324  (5e-324)
      minE: -EXP_LIMIT,                      // -1 to -EXP_LIMIT

      // The maximum exponent value, above which overflow to Infinity occurs.
      // JavaScript numbers: 308  (1.7976931348623157e+308)
      maxE: EXP_LIMIT,                       // 1 to EXP_LIMIT

      // Whether to use cryptographically-secure random number generation, if available.
      crypto: false                          // true/false
    },


  // ----------------------------------- END OF EDITABLE DEFAULTS ------------------------------- //


    inexact, noConflict, quadrant,
    external = true,

    decimalError = '[DecimalError] ',
    invalidArgument = decimalError + 'Invalid argument: ',
    precisionLimitExceeded = decimalError + 'Precision limit exceeded',
    cryptoUnavailable = decimalError + 'crypto unavailable',

    mathfloor = Math.floor,
    mathpow = Math.pow,

    isBinary = /^0b([01]+(\.[01]*)?|\.[01]+)(p[+-]?\d+)?$/i,
    isHex = /^0x([0-9a-f]+(\.[0-9a-f]*)?|\.[0-9a-f]+)(p[+-]?\d+)?$/i,
    isOctal = /^0o([0-7]+(\.[0-7]*)?|\.[0-7]+)(p[+-]?\d+)?$/i,
    isDecimal = /^(\d+(\.\d*)?|\.\d+)(e[+-]?\d+)?$/i,

    BASE = 1e7,
    LOG_BASE = 7,
    MAX_SAFE_INTEGER = 9007199254740991,

    LN10_PRECISION = LN10.length - 1,
    PI_PRECISION = PI.length - 1,

    // Decimal.prototype object
    P = {};


  // Decimal prototype methods


  /*
   *  absoluteValue             abs
   *  ceil
   *  comparedTo                cmp
   *  cosine                    cos
   *  cubeRoot                  cbrt
   *  decimalPlaces             dp
   *  dividedBy                 div
   *  dividedToIntegerBy        divToInt
   *  equals                    eq
   *  floor
   *  greaterThan               gt
   *  greaterThanOrEqualTo      gte
   *  hyperbolicCosine          cosh
   *  hyperbolicSine            sinh
   *  hyperbolicTangent         tanh
   *  inverseCosine             acos
   *  inverseHyperbolicCosine   acosh
   *  inverseHyperbolicSine     asinh
   *  inverseHyperbolicTangent  atanh
   *  inverseSine               asin
   *  inverseTangent            atan
   *  isFinite
   *  isInteger                 isInt
   *  isNaN
   *  isNegative                isNeg
   *  isPositive                isPos
   *  isZero
   *  lessThan                  lt
   *  lessThanOrEqualTo         lte
   *  logarithm                 log
   *  [maximum]                 [max]
   *  [minimum]                 [min]
   *  minus                     sub
   *  modulo                    mod
   *  naturalExponential        exp
   *  naturalLogarithm          ln
   *  negated                   neg
   *  plus                      add
   *  precision                 sd
   *  round
   *  sine                      sin
   *  squareRoot                sqrt
   *  tangent                   tan
   *  times                     mul
   *  toBinary
   *  toDecimalPlaces           toDP
   *  toExponential
   *  toFixed
   *  toFraction
   *  toHexadecimal             toHex
   *  toNearest
   *  toNumber
   *  toOctal
   *  toPower                   pow
   *  toPrecision
   *  toSignificantDigits       toSD
   *  toString
   *  truncated                 trunc
   *  valueOf                   toJSON
   */


  /*
   * Return a new Decimal whose value is the absolute value of this Decimal.
   *
   */
  P.absoluteValue = P.abs = function () {
    var x = new this.constructor(this);
    if (x.s < 0) x.s = 1;
    return finalise(x);
  };


  /*
   * Return a new Decimal whose value is the value of this Decimal rounded to a whole number in the
   * direction of positive Infinity.
   *
   */
  P.ceil = function () {
    return finalise(new this.constructor(this), this.e + 1, 2);
  };


  /*
   * Return
   *   1    if the value of this Decimal is greater than the value of `y`,
   *  -1    if the value of this Decimal is less than the value of `y`,
   *   0    if they have the same value,
   *   NaN  if the value of either Decimal is NaN.
   *
   */
  P.comparedTo = P.cmp = function (y) {
    var i, j, xdL, ydL,
      x = this,
      xd = x.d,
      yd = (y = new x.constructor(y)).d,
      xs = x.s,
      ys = y.s;

    // Either NaN or ±Infinity?
    if (!xd || !yd) {
      return !xs || !ys ? NaN : xs !== ys ? xs : xd === yd ? 0 : !xd ^ xs < 0 ? 1 : -1;
    }

    // Either zero?
    if (!xd[0] || !yd[0]) return xd[0] ? xs : yd[0] ? -ys : 0;

    // Signs differ?
    if (xs !== ys) return xs;

    // Compare exponents.
    if (x.e !== y.e) return x.e > y.e ^ xs < 0 ? 1 : -1;

    xdL = xd.length;
    ydL = yd.length;

    // Compare digit by digit.
    for (i = 0, j = xdL < ydL ? xdL : ydL; i < j; ++i) {
      if (xd[i] !== yd[i]) return xd[i] > yd[i] ^ xs < 0 ? 1 : -1;
    }

    // Compare lengths.
    return xdL === ydL ? 0 : xdL > ydL ^ xs < 0 ? 1 : -1;
  };


  /*
   * Return a new Decimal whose value is the cosine of the value in radians of this Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-1, 1]
   *
   * cos(0)         = 1
   * cos(-0)        = 1
   * cos(Infinity)  = NaN
   * cos(-Infinity) = NaN
   * cos(NaN)       = NaN
   *
   */
  P.cosine = P.cos = function () {
    var pr, rm,
      x = this,
      Ctor = x.constructor;

    if (!x.d) return new Ctor(NaN);

    // cos(0) = cos(-0) = 1
    if (!x.d[0]) return new Ctor(1);

    pr = Ctor.precision;
    rm = Ctor.rounding;
    Ctor.precision = pr + Math.max(x.e, x.sd()) + LOG_BASE;
    Ctor.rounding = 1;

    x = cosine(Ctor, toLessThanHalfPi(Ctor, x));

    Ctor.precision = pr;
    Ctor.rounding = rm;

    return finalise(quadrant == 2 || quadrant == 3 ? x.neg() : x, pr, rm, true);
  };


  /*
   *
   * Return a new Decimal whose value is the cube root of the value of this Decimal, rounded to
   * `precision` significant digits using rounding mode `rounding`.
   *
   *  cbrt(0)  =  0
   *  cbrt(-0) = -0
   *  cbrt(1)  =  1
   *  cbrt(-1) = -1
   *  cbrt(N)  =  N
   *  cbrt(-I) = -I
   *  cbrt(I)  =  I
   *
   * Math.cbrt(x) = (x < 0 ? -Math.pow(-x, 1/3) : Math.pow(x, 1/3))
   *
   */
  P.cubeRoot = P.cbrt = function () {
    var e, m, n, r, rep, s, sd, t, t3, t3plusx,
      x = this,
      Ctor = x.constructor;

    if (!x.isFinite() || x.isZero()) return new Ctor(x);
    external = false;

    // Initial estimate.
    s = x.s * Math.pow(x.s * x, 1 / 3);

     // Math.cbrt underflow/overflow?
     // Pass x to Math.pow as integer, then adjust the exponent of the result.
    if (!s || Math.abs(s) == 1 / 0) {
      n = digitsToString(x.d);
      e = x.e;

      // Adjust n exponent so it is a multiple of 3 away from x exponent.
      if (s = (e - n.length + 1) % 3) n += (s == 1 || s == -2 ? '0' : '00');
      s = Math.pow(n, 1 / 3);

      // Rarely, e may be one less than the result exponent value.
      e = mathfloor((e + 1) / 3) - (e % 3 == (e < 0 ? -1 : 2));

      if (s == 1 / 0) {
        n = '5e' + e;
      } else {
        n = s.toExponential();
        n = n.slice(0, n.indexOf('e') + 1) + e;
      }

      r = new Ctor(n);
      r.s = x.s;
    } else {
      r = new Ctor(s.toString());
    }

    sd = (e = Ctor.precision) + 3;

    // Halley's method.
    // TODO? Compare Newton's method.
    for (;;) {
      t = r;
      t3 = t.times(t).times(t);
      t3plusx = t3.plus(x);
      r = divide(t3plusx.plus(x).times(t), t3plusx.plus(t3), sd + 2, 1);

      // TODO? Replace with for-loop and checkRoundingDigits.
      if (digitsToString(t.d).slice(0, sd) === (n = digitsToString(r.d)).slice(0, sd)) {
        n = n.slice(sd - 3, sd + 1);

        // The 4th rounding digit may be in error by -1 so if the 4 rounding digits are 9999 or 4999
        // , i.e. approaching a rounding boundary, continue the iteration.
        if (n == '9999' || !rep && n == '4999') {

          // On the first iteration only, check to see if rounding up gives the exact result as the
          // nines may infinitely repeat.
          if (!rep) {
            finalise(t, e + 1, 0);

            if (t.times(t).times(t).eq(x)) {
              r = t;
              break;
            }
          }

          sd += 4;
          rep = 1;
        } else {

          // If the rounding digits are null, 0{0,4} or 50{0,3}, check for an exact result.
          // If not, then there are further digits and m will be truthy.
          if (!+n || !+n.slice(1) && n.charAt(0) == '5') {

            // Truncate to the first rounding digit.
            finalise(r, e + 1, 1);
            m = !r.times(r).times(r).eq(x);
          }

          break;
        }
      }
    }

    external = true;

    return finalise(r, e, Ctor.rounding, m);
  };


  /*
   * Return the number of decimal places of the value of this Decimal.
   *
   */
  P.decimalPlaces = P.dp = function () {
    var w,
      d = this.d,
      n = NaN;

    if (d) {
      w = d.length - 1;
      n = (w - mathfloor(this.e / LOG_BASE)) * LOG_BASE;

      // Subtract the number of trailing zeros of the last word.
      w = d[w];
      if (w) for (; w % 10 == 0; w /= 10) n--;
      if (n < 0) n = 0;
    }

    return n;
  };


  /*
   *  n / 0 = I
   *  n / N = N
   *  n / I = 0
   *  0 / n = 0
   *  0 / 0 = N
   *  0 / N = N
   *  0 / I = 0
   *  N / n = N
   *  N / 0 = N
   *  N / N = N
   *  N / I = N
   *  I / n = I
   *  I / 0 = I
   *  I / N = N
   *  I / I = N
   *
   * Return a new Decimal whose value is the value of this Decimal divided by `y`, rounded to
   * `precision` significant digits using rounding mode `rounding`.
   *
   */
  P.dividedBy = P.div = function (y) {
    return divide(this, new this.constructor(y));
  };


  /*
   * Return a new Decimal whose value is the integer part of dividing the value of this Decimal
   * by the value of `y`, rounded to `precision` significant digits using rounding mode `rounding`.
   *
   */
  P.dividedToIntegerBy = P.divToInt = function (y) {
    var x = this,
      Ctor = x.constructor;
    return finalise(divide(x, new Ctor(y), 0, 1, 1), Ctor.precision, Ctor.rounding);
  };


  /*
   * Return true if the value of this Decimal is equal to the value of `y`, otherwise return false.
   *
   */
  P.equals = P.eq = function (y) {
    return this.cmp(y) === 0;
  };


  /*
   * Return a new Decimal whose value is the value of this Decimal rounded to a whole number in the
   * direction of negative Infinity.
   *
   */
  P.floor = function () {
    return finalise(new this.constructor(this), this.e + 1, 3);
  };


  /*
   * Return true if the value of this Decimal is greater than the value of `y`, otherwise return
   * false.
   *
   */
  P.greaterThan = P.gt = function (y) {
    return this.cmp(y) > 0;
  };


  /*
   * Return true if the value of this Decimal is greater than or equal to the value of `y`,
   * otherwise return false.
   *
   */
  P.greaterThanOrEqualTo = P.gte = function (y) {
    var k = this.cmp(y);
    return k == 1 || k === 0;
  };


  /*
   * Return a new Decimal whose value is the hyperbolic cosine of the value in radians of this
   * Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [1, Infinity]
   *
   * cosh(x) = 1 + x^2/2! + x^4/4! + x^6/6! + ...
   *
   * cosh(0)         = 1
   * cosh(-0)        = 1
   * cosh(Infinity)  = Infinity
   * cosh(-Infinity) = Infinity
   * cosh(NaN)       = NaN
   *
   *  x        time taken (ms)   result
   * 1000      9                 9.8503555700852349694e+433
   * 10000     25                4.4034091128314607936e+4342
   * 100000    171               1.4033316802130615897e+43429
   * 1000000   3817              1.5166076984010437725e+434294
   * 10000000  abandoned after 2 minute wait
   *
   * TODO? Compare performance of cosh(x) = 0.5 * (exp(x) + exp(-x))
   *
   */
  P.hyperbolicCosine = P.cosh = function () {
    var k, n, pr, rm, len,
      x = this,
      Ctor = x.constructor,
      one = new Ctor(1);

    if (!x.isFinite()) return new Ctor(x.s ? 1 / 0 : NaN);
    if (x.isZero()) return one;

    pr = Ctor.precision;
    rm = Ctor.rounding;
    Ctor.precision = pr + Math.max(x.e, x.sd()) + 4;
    Ctor.rounding = 1;
    len = x.d.length;

    // Argument reduction: cos(4x) = 1 - 8cos^2(x) + 8cos^4(x) + 1
    // i.e. cos(x) = 1 - cos^2(x/4)(8 - 8cos^2(x/4))

    // Estimate the optimum number of times to use the argument reduction.
    // TODO? Estimation reused from cosine() and may not be optimal here.
    if (len < 32) {
      k = Math.ceil(len / 3);
      n = Math.pow(4, -k).toString();
    } else {
      k = 16;
      n = '2.3283064365386962890625e-10';
    }

    x = taylorSeries(Ctor, 1, x.times(n), new Ctor(1), true);

    // Reverse argument reduction
    var cosh2_x,
      i = k,
      d8 = new Ctor(8);
    for (; i--;) {
      cosh2_x = x.times(x);
      x = one.minus(cosh2_x.times(d8.minus(cosh2_x.times(d8))));
    }

    return finalise(x, Ctor.precision = pr, Ctor.rounding = rm, true);
  };


  /*
   * Return a new Decimal whose value is the hyperbolic sine of the value in radians of this
   * Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-Infinity, Infinity]
   *
   * sinh(x) = x + x^3/3! + x^5/5! + x^7/7! + ...
   *
   * sinh(0)         = 0
   * sinh(-0)        = -0
   * sinh(Infinity)  = Infinity
   * sinh(-Infinity) = -Infinity
   * sinh(NaN)       = NaN
   *
   * x        time taken (ms)
   * 10       2 ms
   * 100      5 ms
   * 1000     14 ms
   * 10000    82 ms
   * 100000   886 ms            1.4033316802130615897e+43429
   * 200000   2613 ms
   * 300000   5407 ms
   * 400000   8824 ms
   * 500000   13026 ms          8.7080643612718084129e+217146
   * 1000000  48543 ms
   *
   * TODO? Compare performance of sinh(x) = 0.5 * (exp(x) - exp(-x))
   *
   */
  P.hyperbolicSine = P.sinh = function () {
    var k, pr, rm, len,
      x = this,
      Ctor = x.constructor;

    if (!x.isFinite() || x.isZero()) return new Ctor(x);

    pr = Ctor.precision;
    rm = Ctor.rounding;
    Ctor.precision = pr + Math.max(x.e, x.sd()) + 4;
    Ctor.rounding = 1;
    len = x.d.length;

    if (len < 3) {
      x = taylorSeries(Ctor, 2, x, x, true);
    } else {

      // Alternative argument reduction: sinh(3x) = sinh(x)(3 + 4sinh^2(x))
      // i.e. sinh(x) = sinh(x/3)(3 + 4sinh^2(x/3))
      // 3 multiplications and 1 addition

      // Argument reduction: sinh(5x) = sinh(x)(5 + sinh^2(x)(20 + 16sinh^2(x)))
      // i.e. sinh(x) = sinh(x/5)(5 + sinh^2(x/5)(20 + 16sinh^2(x/5)))
      // 4 multiplications and 2 additions

      // Estimate the optimum number of times to use the argument reduction.
      k = 1.4 * Math.sqrt(len);
      k = k > 16 ? 16 : k | 0;

      x = x.times(Math.pow(5, -k));

      x = taylorSeries(Ctor, 2, x, x, true);

      // Reverse argument reduction
      var sinh2_x,
        d5 = new Ctor(5),
        d16 = new Ctor(16),
        d20 = new Ctor(20);
      for (; k--;) {
        sinh2_x = x.times(x);
        x = x.times(d5.plus(sinh2_x.times(d16.times(sinh2_x).plus(d20))));
      }
    }

    Ctor.precision = pr;
    Ctor.rounding = rm;

    return finalise(x, pr, rm, true);
  };


  /*
   * Return a new Decimal whose value is the hyperbolic tangent of the value in radians of this
   * Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-1, 1]
   *
   * tanh(x) = sinh(x) / cosh(x)
   *
   * tanh(0)         = 0
   * tanh(-0)        = -0
   * tanh(Infinity)  = 1
   * tanh(-Infinity) = -1
   * tanh(NaN)       = NaN
   *
   */
  P.hyperbolicTangent = P.tanh = function () {
    var pr, rm,
      x = this,
      Ctor = x.constructor;

    if (!x.isFinite()) return new Ctor(x.s);
    if (x.isZero()) return new Ctor(x);

    pr = Ctor.precision;
    rm = Ctor.rounding;
    Ctor.precision = pr + 7;
    Ctor.rounding = 1;

    return divide(x.sinh(), x.cosh(), Ctor.precision = pr, Ctor.rounding = rm);
  };


  /*
   * Return a new Decimal whose value is the arccosine (inverse cosine) in radians of the value of
   * this Decimal.
   *
   * Domain: [-1, 1]
   * Range: [0, pi]
   *
   * acos(x) = pi/2 - asin(x)
   *
   * acos(0)       = pi/2
   * acos(-0)      = pi/2
   * acos(1)       = 0
   * acos(-1)      = pi
   * acos(1/2)     = pi/3
   * acos(-1/2)    = 2*pi/3
   * acos(|x| > 1) = NaN
   * acos(NaN)     = NaN
   *
   */
  P.inverseCosine = P.acos = function () {
    var halfPi,
      x = this,
      Ctor = x.constructor,
      k = x.abs().cmp(1),
      pr = Ctor.precision,
      rm = Ctor.rounding;

    if (k !== -1) {
      return k === 0
        // |x| is 1
        ? x.isNeg() ? getPi(Ctor, pr, rm) : new Ctor(0)
        // |x| > 1 or x is NaN
        : new Ctor(NaN);
    }

    if (x.isZero()) return getPi(Ctor, pr + 4, rm).times(0.5);

    // TODO? Special case acos(0.5) = pi/3 and acos(-0.5) = 2*pi/3

    Ctor.precision = pr + 6;
    Ctor.rounding = 1;

    x = x.asin();
    halfPi = getPi(Ctor, pr + 4, rm).times(0.5);

    Ctor.precision = pr;
    Ctor.rounding = rm;

    return halfPi.minus(x);
  };


  /*
   * Return a new Decimal whose value is the inverse of the hyperbolic cosine in radians of the
   * value of this Decimal.
   *
   * Domain: [1, Infinity]
   * Range: [0, Infinity]
   *
   * acosh(x) = ln(x + sqrt(x^2 - 1))
   *
   * acosh(x < 1)     = NaN
   * acosh(NaN)       = NaN
   * acosh(Infinity)  = Infinity
   * acosh(-Infinity) = NaN
   * acosh(0)         = NaN
   * acosh(-0)        = NaN
   * acosh(1)         = 0
   * acosh(-1)        = NaN
   *
   */
  P.inverseHyperbolicCosine = P.acosh = function () {
    var pr, rm,
      x = this,
      Ctor = x.constructor;

    if (x.lte(1)) return new Ctor(x.eq(1) ? 0 : NaN);
    if (!x.isFinite()) return new Ctor(x);

    pr = Ctor.precision;
    rm = Ctor.rounding;
    Ctor.precision = pr + Math.max(Math.abs(x.e), x.sd()) + 4;
    Ctor.rounding = 1;
    external = false;

    x = x.times(x).minus(1).sqrt().plus(x);

    external = true;
    Ctor.precision = pr;
    Ctor.rounding = rm;

    return x.ln();
  };


  /*
   * Return a new Decimal whose value is the inverse of the hyperbolic sine in radians of the value
   * of this Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-Infinity, Infinity]
   *
   * asinh(x) = ln(x + sqrt(x^2 + 1))
   *
   * asinh(NaN)       = NaN
   * asinh(Infinity)  = Infinity
   * asinh(-Infinity) = -Infinity
   * asinh(0)         = 0
   * asinh(-0)        = -0
   *
   */
  P.inverseHyperbolicSine = P.asinh = function () {
    var pr, rm,
      x = this,
      Ctor = x.constructor;

    if (!x.isFinite() || x.isZero()) return new Ctor(x);

    pr = Ctor.precision;
    rm = Ctor.rounding;
    Ctor.precision = pr + 2 * Math.max(Math.abs(x.e), x.sd()) + 6;
    Ctor.rounding = 1;
    external = false;

    x = x.times(x).plus(1).sqrt().plus(x);

    external = true;
    Ctor.precision = pr;
    Ctor.rounding = rm;

    return x.ln();
  };


  /*
   * Return a new Decimal whose value is the inverse of the hyperbolic tangent in radians of the
   * value of this Decimal.
   *
   * Domain: [-1, 1]
   * Range: [-Infinity, Infinity]
   *
   * atanh(x) = 0.5 * ln((1 + x) / (1 - x))
   *
   * atanh(|x| > 1)   = NaN
   * atanh(NaN)       = NaN
   * atanh(Infinity)  = NaN
   * atanh(-Infinity) = NaN
   * atanh(0)         = 0
   * atanh(-0)        = -0
   * atanh(1)         = Infinity
   * atanh(-1)        = -Infinity
   *
   */
  P.inverseHyperbolicTangent = P.atanh = function () {
    var pr, rm, wpr, xsd,
      x = this,
      Ctor = x.constructor;

    if (!x.isFinite()) return new Ctor(NaN);
    if (x.e >= 0) return new Ctor(x.abs().eq(1) ? x.s / 0 : x.isZero() ? x : NaN);

    pr = Ctor.precision;
    rm = Ctor.rounding;
    xsd = x.sd();

    if (Math.max(xsd, pr) < 2 * -x.e - 1) return finalise(new Ctor(x), pr, rm, true);

    Ctor.precision = wpr = xsd - x.e;

    x = divide(x.plus(1), new Ctor(1).minus(x), wpr + pr, 1);

    Ctor.precision = pr + 4;
    Ctor.rounding = 1;

    x = x.ln();

    Ctor.precision = pr;
    Ctor.rounding = rm;

    return x.times(0.5);
  };


  /*
   * Return a new Decimal whose value is the arcsine (inverse sine) in radians of the value of this
   * Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-pi/2, pi/2]
   *
   * asin(x) = 2*atan(x/(1 + sqrt(1 - x^2)))
   *
   * asin(0)       = 0
   * asin(-0)      = -0
   * asin(1/2)     = pi/6
   * asin(-1/2)    = -pi/6
   * asin(1)       = pi/2
   * asin(-1)      = -pi/2
   * asin(|x| > 1) = NaN
   * asin(NaN)     = NaN
   *
   * TODO? Compare performance of Taylor series.
   *
   */
  P.inverseSine = P.asin = function () {
    var halfPi, k,
      pr, rm,
      x = this,
      Ctor = x.constructor;

    if (x.isZero()) return new Ctor(x);

    k = x.abs().cmp(1);
    pr = Ctor.precision;
    rm = Ctor.rounding;

    if (k !== -1) {

      // |x| is 1
      if (k === 0) {
        halfPi = getPi(Ctor, pr + 4, rm).times(0.5);
        halfPi.s = x.s;
        return halfPi;
      }

      // |x| > 1 or x is NaN
      return new Ctor(NaN);
    }

    // TODO? Special case asin(1/2) = pi/6 and asin(-1/2) = -pi/6

    Ctor.precision = pr + 6;
    Ctor.rounding = 1;

    x = x.div(new Ctor(1).minus(x.times(x)).sqrt().plus(1)).atan();

    Ctor.precision = pr;
    Ctor.rounding = rm;

    return x.times(2);
  };


  /*
   * Return a new Decimal whose value is the arctangent (inverse tangent) in radians of the value
   * of this Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-pi/2, pi/2]
   *
   * atan(x) = x - x^3/3 + x^5/5 - x^7/7 + ...
   *
   * atan(0)         = 0
   * atan(-0)        = -0
   * atan(1)         = pi/4
   * atan(-1)        = -pi/4
   * atan(Infinity)  = pi/2
   * atan(-Infinity) = -pi/2
   * atan(NaN)       = NaN
   *
   */
  P.inverseTangent = P.atan = function () {
    var i, j, k, n, px, t, r, wpr, x2,
      x = this,
      Ctor = x.constructor,
      pr = Ctor.precision,
      rm = Ctor.rounding;

    if (!x.isFinite()) {
      if (!x.s) return new Ctor(NaN);
      if (pr + 4 <= PI_PRECISION) {
        r = getPi(Ctor, pr + 4, rm).times(0.5);
        r.s = x.s;
        return r;
      }
    } else if (x.isZero()) {
      return new Ctor(x);
    } else if (x.abs().eq(1) && pr + 4 <= PI_PRECISION) {
      r = getPi(Ctor, pr + 4, rm).times(0.25);
      r.s = x.s;
      return r;
    }

    Ctor.precision = wpr = pr + 10;
    Ctor.rounding = 1;

    // TODO? if (x >= 1 && pr <= PI_PRECISION) atan(x) = halfPi * x.s - atan(1 / x);

    // Argument reduction
    // Ensure |x| < 0.42
    // atan(x) = 2 * atan(x / (1 + sqrt(1 + x^2)))

    k = Math.min(28, wpr / LOG_BASE + 2 | 0);

    for (i = k; i; --i) x = x.div(x.times(x).plus(1).sqrt().plus(1));

    external = false;

    j = Math.ceil(wpr / LOG_BASE);
    n = 1;
    x2 = x.times(x);
    r = new Ctor(x);
    px = x;

    // atan(x) = x - x^3/3 + x^5/5 - x^7/7 + ...
    for (; i !== -1;) {
      px = px.times(x2);
      t = r.minus(px.div(n += 2));

      px = px.times(x2);
      r = t.plus(px.div(n += 2));

      if (r.d[j] !== void 0) for (i = j; r.d[i] === t.d[i] && i--;);
    }

    if (k) r = r.times(2 << (k - 1));

    external = true;

    return finalise(r, Ctor.precision = pr, Ctor.rounding = rm, true);
  };


  /*
   * Return true if the value of this Decimal is a finite number, otherwise return false.
   *
   */
  P.isFinite = function () {
    return !!this.d;
  };


  /*
   * Return true if the value of this Decimal is an integer, otherwise return false.
   *
   */
  P.isInteger = P.isInt = function () {
    return !!this.d && mathfloor(this.e / LOG_BASE) > this.d.length - 2;
  };


  /*
   * Return true if the value of this Decimal is NaN, otherwise return false.
   *
   */
  P.isNaN = function () {
    return !this.s;
  };


  /*
   * Return true if the value of this Decimal is negative, otherwise return false.
   *
   */
  P.isNegative = P.isNeg = function () {
    return this.s < 0;
  };


  /*
   * Return true if the value of this Decimal is positive, otherwise return false.
   *
   */
  P.isPositive = P.isPos = function () {
    return this.s > 0;
  };


  /*
   * Return true if the value of this Decimal is 0 or -0, otherwise return false.
   *
   */
  P.isZero = function () {
    return !!this.d && this.d[0] === 0;
  };


  /*
   * Return true if the value of this Decimal is less than `y`, otherwise return false.
   *
   */
  P.lessThan = P.lt = function (y) {
    return this.cmp(y) < 0;
  };


  /*
   * Return true if the value of this Decimal is less than or equal to `y`, otherwise return false.
   *
   */
  P.lessThanOrEqualTo = P.lte = function (y) {
    return this.cmp(y) < 1;
  };


  /*
   * Return the logarithm of the value of this Decimal to the specified base, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * If no base is specified, return log[10](arg).
   *
   * log[base](arg) = ln(arg) / ln(base)
   *
   * The result will always be correctly rounded if the base of the log is 10, and 'almost always'
   * otherwise:
   *
   * Depending on the rounding mode, the result may be incorrectly rounded if the first fifteen
   * rounding digits are [49]99999999999999 or [50]00000000000000. In that case, the maximum error
   * between the result and the correctly rounded result will be one ulp (unit in the last place).
   *
   * log[-b](a)       = NaN
   * log[0](a)        = NaN
   * log[1](a)        = NaN
   * log[NaN](a)      = NaN
   * log[Infinity](a) = NaN
   * log[b](0)        = -Infinity
   * log[b](-0)       = -Infinity
   * log[b](-a)       = NaN
   * log[b](1)        = 0
   * log[b](Infinity) = Infinity
   * log[b](NaN)      = NaN
   *
   * [base] {number|string|Decimal} The base of the logarithm.
   *
   */
  P.logarithm = P.log = function (base) {
    var isBase10, d, denominator, k, inf, num, sd, r,
      arg = this,
      Ctor = arg.constructor,
      pr = Ctor.precision,
      rm = Ctor.rounding,
      guard = 5;

    // Default base is 10.
    if (base == null) {
      base = new Ctor(10);
      isBase10 = true;
    } else {
      base = new Ctor(base);
      d = base.d;

      // Return NaN if base is negative, or non-finite, or is 0 or 1.
      if (base.s < 0 || !d || !d[0] || base.eq(1)) return new Ctor(NaN);

      isBase10 = base.eq(10);
    }

    d = arg.d;

    // Is arg negative, non-finite, 0 or 1?
    if (arg.s < 0 || !d || !d[0] || arg.eq(1)) {
      return new Ctor(d && !d[0] ? -1 / 0 : arg.s != 1 ? NaN : d ? 0 : 1 / 0);
    }

    // The result will have a non-terminating decimal expansion if base is 10 and arg is not an
    // integer power of 10.
    if (isBase10) {
      if (d.length > 1) {
        inf = true;
      } else {
        for (k = d[0]; k % 10 === 0;) k /= 10;
        inf = k !== 1;
      }
    }

    external = false;
    sd = pr + guard;
    num = naturalLogarithm(arg, sd);
    denominator = isBase10 ? getLn10(Ctor, sd + 10) : naturalLogarithm(base, sd);

    // The result will have 5 rounding digits.
    r = divide(num, denominator, sd, 1);

    // If at a rounding boundary, i.e. the result's rounding digits are [49]9999 or [50]0000,
    // calculate 10 further digits.
    //
    // If the result is known to have an infinite decimal expansion, repeat this until it is clear
    // that the result is above or below the boundary. Otherwise, if after calculating the 10
    // further digits, the last 14 are nines, round up and assume the result is exact.
    // Also assume the result is exact if the last 14 are zero.
    //
    // Example of a result that will be incorrectly rounded:
    // log[1048576](4503599627370502) = 2.60000000000000009610279511444746...
    // The above result correctly rounded using ROUND_CEIL to 1 decimal place should be 2.7, but it
    // will be given as 2.6 as there are 15 zeros immediately after the requested decimal place, so
    // the exact result would be assumed to be 2.6, which rounded using ROUND_CEIL to 1 decimal
    // place is still 2.6.
    if (checkRoundingDigits(r.d, k = pr, rm)) {

      do {
        sd += 10;
        num = naturalLogarithm(arg, sd);
        denominator = isBase10 ? getLn10(Ctor, sd + 10) : naturalLogarithm(base, sd);
        r = divide(num, denominator, sd, 1);

        if (!inf) {

          // Check for 14 nines from the 2nd rounding digit, as the first may be 4.
          if (+digitsToString(r.d).slice(k + 1, k + 15) + 1 == 1e14) {
            r = finalise(r, pr + 1, 0);
          }

          break;
        }
      } while (checkRoundingDigits(r.d, k += 10, rm));
    }

    external = true;

    return finalise(r, pr, rm);
  };


  /*
   * Return a new Decimal whose value is the maximum of the arguments and the value of this Decimal.
   *
   * arguments {number|string|Decimal}
   *
  P.max = function () {
    Array.prototype.push.call(arguments, this);
    return maxOrMin(this.constructor, arguments, 'lt');
  };
   */


  /*
   * Return a new Decimal whose value is the minimum of the arguments and the value of this Decimal.
   *
   * arguments {number|string|Decimal}
   *
  P.min = function () {
    Array.prototype.push.call(arguments, this);
    return maxOrMin(this.constructor, arguments, 'gt');
  };
   */


  /*
   *  n - 0 = n
   *  n - N = N
   *  n - I = -I
   *  0 - n = -n
   *  0 - 0 = 0
   *  0 - N = N
   *  0 - I = -I
   *  N - n = N
   *  N - 0 = N
   *  N - N = N
   *  N - I = N
   *  I - n = I
   *  I - 0 = I
   *  I - N = N
   *  I - I = N
   *
   * Return a new Decimal whose value is the value of this Decimal minus `y`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   */
  P.minus = P.sub = function (y) {
    var d, e, i, j, k, len, pr, rm, xd, xe, xLTy, yd,
      x = this,
      Ctor = x.constructor;

    y = new Ctor(y);

    // If either is not finite...
    if (!x.d || !y.d) {

      // Return NaN if either is NaN.
      if (!x.s || !y.s) y = new Ctor(NaN);

      // Return y negated if x is finite and y is ±Infinity.
      else if (x.d) y.s = -y.s;

      // Return x if y is finite and x is ±Infinity.
      // Return x if both are ±Infinity with different signs.
      // Return NaN if both are ±Infinity with the same sign.
      else y = new Ctor(y.d || x.s !== y.s ? x : NaN);

      return y;
    }

    // If signs differ...
    if (x.s != y.s) {
      y.s = -y.s;
      return x.plus(y);
    }

    xd = x.d;
    yd = y.d;
    pr = Ctor.precision;
    rm = Ctor.rounding;

    // If either is zero...
    if (!xd[0] || !yd[0]) {

      // Return y negated if x is zero and y is non-zero.
      if (yd[0]) y.s = -y.s;

      // Return x if y is zero and x is non-zero.
      else if (xd[0]) y = new Ctor(x);

      // Return zero if both are zero.
      // From IEEE 754 (2008) 6.3: 0 - 0 = -0 - -0 = -0 when rounding to -Infinity.
      else return new Ctor(rm === 3 ? -0 : 0);

      return external ? finalise(y, pr, rm) : y;
    }

    // x and y are finite, non-zero numbers with the same sign.

    // Calculate base 1e7 exponents.
    e = mathfloor(y.e / LOG_BASE);
    xe = mathfloor(x.e / LOG_BASE);

    xd = xd.slice();
    k = xe - e;

    // If base 1e7 exponents differ...
    if (k) {
      xLTy = k < 0;

      if (xLTy) {
        d = xd;
        k = -k;
        len = yd.length;
      } else {
        d = yd;
        e = xe;
        len = xd.length;
      }

      // Numbers with massively different exponents would result in a very high number of
      // zeros needing to be prepended, but this can be avoided while still ensuring correct
      // rounding by limiting the number of zeros to `Math.ceil(pr / LOG_BASE) + 2`.
      i = Math.max(Math.ceil(pr / LOG_BASE), len) + 2;

      if (k > i) {
        k = i;
        d.length = 1;
      }

      // Prepend zeros to equalise exponents.
      d.reverse();
      for (i = k; i--;) d.push(0);
      d.reverse();

    // Base 1e7 exponents equal.
    } else {

      // Check digits to determine which is the bigger number.

      i = xd.length;
      len = yd.length;
      xLTy = i < len;
      if (xLTy) len = i;

      for (i = 0; i < len; i++) {
        if (xd[i] != yd[i]) {
          xLTy = xd[i] < yd[i];
          break;
        }
      }

      k = 0;
    }

    if (xLTy) {
      d = xd;
      xd = yd;
      yd = d;
      y.s = -y.s;
    }

    len = xd.length;

    // Append zeros to `xd` if shorter.
    // Don't add zeros to `yd` if shorter as subtraction only needs to start at `yd` length.
    for (i = yd.length - len; i > 0; --i) xd[len++] = 0;

    // Subtract yd from xd.
    for (i = yd.length; i > k;) {

      if (xd[--i] < yd[i]) {
        for (j = i; j && xd[--j] === 0;) xd[j] = BASE - 1;
        --xd[j];
        xd[i] += BASE;
      }

      xd[i] -= yd[i];
    }

    // Remove trailing zeros.
    for (; xd[--len] === 0;) xd.pop();

    // Remove leading zeros and adjust exponent accordingly.
    for (; xd[0] === 0; xd.shift()) --e;

    // Zero?
    if (!xd[0]) return new Ctor(rm === 3 ? -0 : 0);

    y.d = xd;
    y.e = getBase10Exponent(xd, e);

    return external ? finalise(y, pr, rm) : y;
  };


  /*
   *   n % 0 =  N
   *   n % N =  N
   *   n % I =  n
   *   0 % n =  0
   *  -0 % n = -0
   *   0 % 0 =  N
   *   0 % N =  N
   *   0 % I =  0
   *   N % n =  N
   *   N % 0 =  N
   *   N % N =  N
   *   N % I =  N
   *   I % n =  N
   *   I % 0 =  N
   *   I % N =  N
   *   I % I =  N
   *
   * Return a new Decimal whose value is the value of this Decimal modulo `y`, rounded to
   * `precision` significant digits using rounding mode `rounding`.
   *
   * The result depends on the modulo mode.
   *
   */
  P.modulo = P.mod = function (y) {
    var q,
      x = this,
      Ctor = x.constructor;

    y = new Ctor(y);

    // Return NaN if x is ±Infinity or NaN, or y is NaN or ±0.
    if (!x.d || !y.s || y.d && !y.d[0]) return new Ctor(NaN);

    // Return x if y is ±Infinity or x is ±0.
    if (!y.d || x.d && !x.d[0]) {
      return finalise(new Ctor(x), Ctor.precision, Ctor.rounding);
    }

    // Prevent rounding of intermediate calculations.
    external = false;

    if (Ctor.modulo == 9) {

      // Euclidian division: q = sign(y) * floor(x / abs(y))
      // result = x - q * y    where  0 <= result < abs(y)
      q = divide(x, y.abs(), 0, 3, 1);
      q.s *= y.s;
    } else {
      q = divide(x, y, 0, Ctor.modulo, 1);
    }

    q = q.times(y);

    external = true;

    return x.minus(q);
  };


  /*
   * Return a new Decimal whose value is the natural exponential of the value of this Decimal,
   * i.e. the base e raised to the power the value of this Decimal, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   */
  P.naturalExponential = P.exp = function () {
    return naturalExponential(this);
  };


  /*
   * Return a new Decimal whose value is the natural logarithm of the value of this Decimal,
   * rounded to `precision` significant digits using rounding mode `rounding`.
   *
   */
  P.naturalLogarithm = P.ln = function () {
    return naturalLogarithm(this);
  };


  /*
   * Return a new Decimal whose value is the value of this Decimal negated, i.e. as if multiplied by
   * -1.
   *
   */
  P.negated = P.neg = function () {
    var x = new this.constructor(this);
    x.s = -x.s;
    return finalise(x);
  };


  /*
   *  n + 0 = n
   *  n + N = N
   *  n + I = I
   *  0 + n = n
   *  0 + 0 = 0
   *  0 + N = N
   *  0 + I = I
   *  N + n = N
   *  N + 0 = N
   *  N + N = N
   *  N + I = N
   *  I + n = I
   *  I + 0 = I
   *  I + N = N
   *  I + I = I
   *
   * Return a new Decimal whose value is the value of this Decimal plus `y`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   */
  P.plus = P.add = function (y) {
    var carry, d, e, i, k, len, pr, rm, xd, yd,
      x = this,
      Ctor = x.constructor;

    y = new Ctor(y);

    // If either is not finite...
    if (!x.d || !y.d) {

      // Return NaN if either is NaN.
      if (!x.s || !y.s) y = new Ctor(NaN);

      // Return x if y is finite and x is ±Infinity.
      // Return x if both are ±Infinity with the same sign.
      // Return NaN if both are ±Infinity with different signs.
      // Return y if x is finite and y is ±Infinity.
      else if (!x.d) y = new Ctor(y.d || x.s === y.s ? x : NaN);

      return y;
    }

     // If signs differ...
    if (x.s != y.s) {
      y.s = -y.s;
      return x.minus(y);
    }

    xd = x.d;
    yd = y.d;
    pr = Ctor.precision;
    rm = Ctor.rounding;

    // If either is zero...
    if (!xd[0] || !yd[0]) {

      // Return x if y is zero.
      // Return y if y is non-zero.
      if (!yd[0]) y = new Ctor(x);

      return external ? finalise(y, pr, rm) : y;
    }

    // x and y are finite, non-zero numbers with the same sign.

    // Calculate base 1e7 exponents.
    k = mathfloor(x.e / LOG_BASE);
    e = mathfloor(y.e / LOG_BASE);

    xd = xd.slice();
    i = k - e;

    // If base 1e7 exponents differ...
    if (i) {

      if (i < 0) {
        d = xd;
        i = -i;
        len = yd.length;
      } else {
        d = yd;
        e = k;
        len = xd.length;
      }

      // Limit number of zeros prepended to max(ceil(pr / LOG_BASE), len) + 1.
      k = Math.ceil(pr / LOG_BASE);
      len = k > len ? k + 1 : len + 1;

      if (i > len) {
        i = len;
        d.length = 1;
      }

      // Prepend zeros to equalise exponents. Note: Faster to use reverse then do unshifts.
      d.reverse();
      for (; i--;) d.push(0);
      d.reverse();
    }

    len = xd.length;
    i = yd.length;

    // If yd is longer than xd, swap xd and yd so xd points to the longer array.
    if (len - i < 0) {
      i = len;
      d = yd;
      yd = xd;
      xd = d;
    }

    // Only start adding at yd.length - 1 as the further digits of xd can be left as they are.
    for (carry = 0; i;) {
      carry = (xd[--i] = xd[i] + yd[i] + carry) / BASE | 0;
      xd[i] %= BASE;
    }

    if (carry) {
      xd.unshift(carry);
      ++e;
    }

    // Remove trailing zeros.
    // No need to check for zero, as +x + +y != 0 && -x + -y != 0
    for (len = xd.length; xd[--len] == 0;) xd.pop();

    y.d = xd;
    y.e = getBase10Exponent(xd, e);

    return external ? finalise(y, pr, rm) : y;
  };


  /*
   * Return the number of significant digits of the value of this Decimal.
   *
   * [z] {boolean|number} Whether to count integer-part trailing zeros: true, false, 1 or 0.
   *
   */
  P.precision = P.sd = function (z) {
    var k,
      x = this;

    if (z !== void 0 && z !== !!z && z !== 1 && z !== 0) throw Error(invalidArgument + z);

    if (x.d) {
      k = getPrecision(x.d);
      if (z && x.e + 1 > k) k = x.e + 1;
    } else {
      k = NaN;
    }

    return k;
  };


  /*
   * Return a new Decimal whose value is the value of this Decimal rounded to a whole number using
   * rounding mode `rounding`.
   *
   */
  P.round = function () {
    var x = this,
      Ctor = x.constructor;

    return finalise(new Ctor(x), x.e + 1, Ctor.rounding);
  };


  /*
   * Return a new Decimal whose value is the sine of the value in radians of this Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-1, 1]
   *
   * sin(x) = x - x^3/3! + x^5/5! - ...
   *
   * sin(0)         = 0
   * sin(-0)        = -0
   * sin(Infinity)  = NaN
   * sin(-Infinity) = NaN
   * sin(NaN)       = NaN
   *
   */
  P.sine = P.sin = function () {
    var pr, rm,
      x = this,
      Ctor = x.constructor;

    if (!x.isFinite()) return new Ctor(NaN);
    if (x.isZero()) return new Ctor(x);

    pr = Ctor.precision;
    rm = Ctor.rounding;
    Ctor.precision = pr + Math.max(x.e, x.sd()) + LOG_BASE;
    Ctor.rounding = 1;

    x = sine(Ctor, toLessThanHalfPi(Ctor, x));

    Ctor.precision = pr;
    Ctor.rounding = rm;

    return finalise(quadrant > 2 ? x.neg() : x, pr, rm, true);
  };


  /*
   * Return a new Decimal whose value is the square root of this Decimal, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   *  sqrt(-n) =  N
   *  sqrt(N)  =  N
   *  sqrt(-I) =  N
   *  sqrt(I)  =  I
   *  sqrt(0)  =  0
   *  sqrt(-0) = -0
   *
   */
  P.squareRoot = P.sqrt = function () {
    var m, n, sd, r, rep, t,
      x = this,
      d = x.d,
      e = x.e,
      s = x.s,
      Ctor = x.constructor;

    // Negative/NaN/Infinity/zero?
    if (s !== 1 || !d || !d[0]) {
      return new Ctor(!s || s < 0 && (!d || d[0]) ? NaN : d ? x : 1 / 0);
    }

    external = false;

    // Initial estimate.
    s = Math.sqrt(+x);

    // Math.sqrt underflow/overflow?
    // Pass x to Math.sqrt as integer, then adjust the exponent of the result.
    if (s == 0 || s == 1 / 0) {
      n = digitsToString(d);

      if ((n.length + e) % 2 == 0) n += '0';
      s = Math.sqrt(n);
      e = mathfloor((e + 1) / 2) - (e < 0 || e % 2);

      if (s == 1 / 0) {
        n = '1e' + e;
      } else {
        n = s.toExponential();
        n = n.slice(0, n.indexOf('e') + 1) + e;
      }

      r = new Ctor(n);
    } else {
      r = new Ctor(s.toString());
    }

    sd = (e = Ctor.precision) + 3;

    // Newton-Raphson iteration.
    for (;;) {
      t = r;
      r = t.plus(divide(x, t, sd + 2, 1)).times(0.5);

      // TODO? Replace with for-loop and checkRoundingDigits.
      if (digitsToString(t.d).slice(0, sd) === (n = digitsToString(r.d)).slice(0, sd)) {
        n = n.slice(sd - 3, sd + 1);

        // The 4th rounding digit may be in error by -1 so if the 4 rounding digits are 9999 or
        // 4999, i.e. approaching a rounding boundary, continue the iteration.
        if (n == '9999' || !rep && n == '4999') {

          // On the first iteration only, check to see if rounding up gives the exact result as the
          // nines may infinitely repeat.
          if (!rep) {
            finalise(t, e + 1, 0);

            if (t.times(t).eq(x)) {
              r = t;
              break;
            }
          }

          sd += 4;
          rep = 1;
        } else {

          // If the rounding digits are null, 0{0,4} or 50{0,3}, check for an exact result.
          // If not, then there are further digits and m will be truthy.
          if (!+n || !+n.slice(1) && n.charAt(0) == '5') {

            // Truncate to the first rounding digit.
            finalise(r, e + 1, 1);
            m = !r.times(r).eq(x);
          }

          break;
        }
      }
    }

    external = true;

    return finalise(r, e, Ctor.rounding, m);
  };


  /*
   * Return a new Decimal whose value is the tangent of the value in radians of this Decimal.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-Infinity, Infinity]
   *
   * tan(0)         = 0
   * tan(-0)        = -0
   * tan(Infinity)  = NaN
   * tan(-Infinity) = NaN
   * tan(NaN)       = NaN
   *
   */
  P.tangent = P.tan = function () {
    var pr, rm,
      x = this,
      Ctor = x.constructor;

    if (!x.isFinite()) return new Ctor(NaN);
    if (x.isZero()) return new Ctor(x);

    pr = Ctor.precision;
    rm = Ctor.rounding;
    Ctor.precision = pr + 10;
    Ctor.rounding = 1;

    x = x.sin();
    x.s = 1;
    x = divide(x, new Ctor(1).minus(x.times(x)).sqrt(), pr + 10, 0);

    Ctor.precision = pr;
    Ctor.rounding = rm;

    return finalise(quadrant == 2 || quadrant == 4 ? x.neg() : x, pr, rm, true);
  };


  /*
   *  n * 0 = 0
   *  n * N = N
   *  n * I = I
   *  0 * n = 0
   *  0 * 0 = 0
   *  0 * N = N
   *  0 * I = N
   *  N * n = N
   *  N * 0 = N
   *  N * N = N
   *  N * I = N
   *  I * n = I
   *  I * 0 = N
   *  I * N = N
   *  I * I = I
   *
   * Return a new Decimal whose value is this Decimal times `y`, rounded to `precision` significant
   * digits using rounding mode `rounding`.
   *
   */
  P.times = P.mul = function (y) {
    var carry, e, i, k, r, rL, t, xdL, ydL,
      x = this,
      Ctor = x.constructor,
      xd = x.d,
      yd = (y = new Ctor(y)).d;

    y.s *= x.s;

     // If either is NaN, ±Infinity or ±0...
    if (!xd || !xd[0] || !yd || !yd[0]) {

      return new Ctor(!y.s || xd && !xd[0] && !yd || yd && !yd[0] && !xd

        // Return NaN if either is NaN.
        // Return NaN if x is ±0 and y is ±Infinity, or y is ±0 and x is ±Infinity.
        ? NaN

        // Return ±Infinity if either is ±Infinity.
        // Return ±0 if either is ±0.
        : !xd || !yd ? y.s / 0 : y.s * 0);
    }

    e = mathfloor(x.e / LOG_BASE) + mathfloor(y.e / LOG_BASE);
    xdL = xd.length;
    ydL = yd.length;

    // Ensure xd points to the longer array.
    if (xdL < ydL) {
      r = xd;
      xd = yd;
      yd = r;
      rL = xdL;
      xdL = ydL;
      ydL = rL;
    }

    // Initialise the result array with zeros.
    r = [];
    rL = xdL + ydL;
    for (i = rL; i--;) r.push(0);

    // Multiply!
    for (i = ydL; --i >= 0;) {
      carry = 0;
      for (k = xdL + i; k > i;) {
        t = r[k] + yd[i] * xd[k - i - 1] + carry;
        r[k--] = t % BASE | 0;
        carry = t / BASE | 0;
      }

      r[k] = (r[k] + carry) % BASE | 0;
    }

    // Remove trailing zeros.
    for (; !r[--rL];) r.pop();

    if (carry) ++e;
    else r.shift();

    y.d = r;
    y.e = getBase10Exponent(r, e);

    return external ? finalise(y, Ctor.precision, Ctor.rounding) : y;
  };


  /*
   * Return a string representing the value of this Decimal in base 2, round to `sd` significant
   * digits using rounding mode `rm`.
   *
   * If the optional `sd` argument is present then return binary exponential notation.
   *
   * [sd] {number} Significant digits. Integer, 1 to MAX_DIGITS inclusive.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   */
  P.toBinary = function (sd, rm) {
    return toStringBinary(this, 2, sd, rm);
  };


  /*
   * Return a new Decimal whose value is the value of this Decimal rounded to a maximum of `dp`
   * decimal places using rounding mode `rm` or `rounding` if `rm` is omitted.
   *
   * If `dp` is omitted, return a new Decimal whose value is the value of this Decimal.
   *
   * [dp] {number} Decimal places. Integer, 0 to MAX_DIGITS inclusive.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   */
  P.toDecimalPlaces = P.toDP = function (dp, rm) {
    var x = this,
      Ctor = x.constructor;

    x = new Ctor(x);
    if (dp === void 0) return x;

    checkInt32(dp, 0, MAX_DIGITS);

    if (rm === void 0) rm = Ctor.rounding;
    else checkInt32(rm, 0, 8);

    return finalise(x, dp + x.e + 1, rm);
  };


  /*
   * Return a string representing the value of this Decimal in exponential notation rounded to
   * `dp` fixed decimal places using rounding mode `rounding`.
   *
   * [dp] {number} Decimal places. Integer, 0 to MAX_DIGITS inclusive.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   */
  P.toExponential = function (dp, rm) {
    var str,
      x = this,
      Ctor = x.constructor;

    if (dp === void 0) {
      str = finiteToString(x, true);
    } else {
      checkInt32(dp, 0, MAX_DIGITS);

      if (rm === void 0) rm = Ctor.rounding;
      else checkInt32(rm, 0, 8);

      x = finalise(new Ctor(x), dp + 1, rm);
      str = finiteToString(x, true, dp + 1);
    }

    return x.isNeg() && !x.isZero() ? '-' + str : str;
  };


  /*
   * Return a string representing the value of this Decimal in normal (fixed-point) notation to
   * `dp` fixed decimal places and rounded using rounding mode `rm` or `rounding` if `rm` is
   * omitted.
   *
   * As with JavaScript numbers, (-0).toFixed(0) is '0', but e.g. (-0.00001).toFixed(0) is '-0'.
   *
   * [dp] {number} Decimal places. Integer, 0 to MAX_DIGITS inclusive.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   * (-0).toFixed(0) is '0', but (-0.1).toFixed(0) is '-0'.
   * (-0).toFixed(1) is '0.0', but (-0.01).toFixed(1) is '-0.0'.
   * (-0).toFixed(3) is '0.000'.
   * (-0.5).toFixed(0) is '-0'.
   *
   */
  P.toFixed = function (dp, rm) {
    var str, y,
      x = this,
      Ctor = x.constructor;

    if (dp === void 0) {
      str = finiteToString(x);
    } else {
      checkInt32(dp, 0, MAX_DIGITS);

      if (rm === void 0) rm = Ctor.rounding;
      else checkInt32(rm, 0, 8);

      y = finalise(new Ctor(x), dp + x.e + 1, rm);
      str = finiteToString(y, false, dp + y.e + 1);
    }

    // To determine whether to add the minus sign look at the value before it was rounded,
    // i.e. look at `x` rather than `y`.
    return x.isNeg() && !x.isZero() ? '-' + str : str;
  };


  /*
   * Return an array representing the value of this Decimal as a simple fraction with an integer
   * numerator and an integer denominator.
   *
   * The denominator will be a positive non-zero value less than or equal to the specified maximum
   * denominator. If a maximum denominator is not specified, the denominator will be the lowest
   * value necessary to represent the number exactly.
   *
   * [maxD] {number|string|Decimal} Maximum denominator. Integer >= 1 and < Infinity.
   *
   */
  P.toFraction = function (maxD) {
    var d, d0, d1, d2, e, k, n, n0, n1, pr, q, r,
      x = this,
      xd = x.d,
      Ctor = x.constructor;

    if (!xd) return new Ctor(x);

    n1 = d0 = new Ctor(1);
    d1 = n0 = new Ctor(0);

    d = new Ctor(d1);
    e = d.e = getPrecision(xd) - x.e - 1;
    k = e % LOG_BASE;
    d.d[0] = mathpow(10, k < 0 ? LOG_BASE + k : k);

    if (maxD == null) {

      // d is 10**e, the minimum max-denominator needed.
      maxD = e > 0 ? d : n1;
    } else {
      n = new Ctor(maxD);
      if (!n.isInt() || n.lt(n1)) throw Error(invalidArgument + n);
      maxD = n.gt(d) ? (e > 0 ? d : n1) : n;
    }

    external = false;
    n = new Ctor(digitsToString(xd));
    pr = Ctor.precision;
    Ctor.precision = e = xd.length * LOG_BASE * 2;

    for (;;)  {
      q = divide(n, d, 0, 1, 1);
      d2 = d0.plus(q.times(d1));
      if (d2.cmp(maxD) == 1) break;
      d0 = d1;
      d1 = d2;
      d2 = n1;
      n1 = n0.plus(q.times(d2));
      n0 = d2;
      d2 = d;
      d = n.minus(q.times(d2));
      n = d2;
    }

    d2 = divide(maxD.minus(d0), d1, 0, 1, 1);
    n0 = n0.plus(d2.times(n1));
    d0 = d0.plus(d2.times(d1));
    n0.s = n1.s = x.s;

    // Determine which fraction is closer to x, n0/d0 or n1/d1?
    r = divide(n1, d1, e, 1).minus(x).abs().cmp(divide(n0, d0, e, 1).minus(x).abs()) < 1
        ? [n1, d1] : [n0, d0];

    Ctor.precision = pr;
    external = true;

    return r;
  };


  /*
   * Return a string representing the value of this Decimal in base 16, round to `sd` significant
   * digits using rounding mode `rm`.
   *
   * If the optional `sd` argument is present then return binary exponential notation.
   *
   * [sd] {number} Significant digits. Integer, 1 to MAX_DIGITS inclusive.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   */
  P.toHexadecimal = P.toHex = function (sd, rm) {
    return toStringBinary(this, 16, sd, rm);
  };



  /*
   * Returns a new Decimal whose value is the nearest multiple of the magnitude of `y` to the value
   * of this Decimal.
   *
   * If the value of this Decimal is equidistant from two multiples of `y`, the rounding mode `rm`,
   * or `Decimal.rounding` if `rm` is omitted, determines the direction of the nearest multiple.
   *
   * In the context of this method, rounding mode 4 (ROUND_HALF_UP) is the same as rounding mode 0
   * (ROUND_UP), and so on.
   *
   * The return value will always have the same sign as this Decimal, unless either this Decimal
   * or `y` is NaN, in which case the return value will be also be NaN.
   *
   * The return value is not affected by the value of `precision`.
   *
   * y {number|string|Decimal} The magnitude to round to a multiple of.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   * 'toNearest() rounding mode not an integer: {rm}'
   * 'toNearest() rounding mode out of range: {rm}'
   *
   */
  P.toNearest = function (y, rm) {
    var x = this,
      Ctor = x.constructor;

    x = new Ctor(x);

    if (y == null) {

      // If x is not finite, return x.
      if (!x.d) return x;

      y = new Ctor(1);
      rm = Ctor.rounding;
    } else {
      y = new Ctor(y);
      if (rm !== void 0) checkInt32(rm, 0, 8);

      // If x is not finite, return x if y is not NaN, else NaN.
      if (!x.d) return y.s ? x : y;

      // If y is not finite, return Infinity with the sign of x if y is Infinity, else NaN.
      if (!y.d) {
        if (y.s) y.s = x.s;
        return y;
      }
    }

    // If y is not zero, calculate the nearest multiple of y to x.
    if (y.d[0]) {
      external = false;
      if (rm < 4) rm = [4, 5, 7, 8][rm];
      x = divide(x, y, 0, rm, 1).times(y);
      external = true;
      finalise(x);

    // If y is zero, return zero with the sign of x.
    } else {
      y.s = x.s;
      x = y;
    }

    return x;
  };


  /*
   * Return the value of this Decimal converted to a number primitive.
   * Zero keeps its sign.
   *
   */
  P.toNumber = function () {
    return +this;
  };


  /*
   * Return a string representing the value of this Decimal in base 8, round to `sd` significant
   * digits using rounding mode `rm`.
   *
   * If the optional `sd` argument is present then return binary exponential notation.
   *
   * [sd] {number} Significant digits. Integer, 1 to MAX_DIGITS inclusive.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   */
  P.toOctal = function (sd, rm) {
    return toStringBinary(this, 8, sd, rm);
  };


  /*
   * Return a new Decimal whose value is the value of this Decimal raised to the power `y`, rounded
   * to `precision` significant digits using rounding mode `rounding`.
   *
   * ECMAScript compliant.
   *
   *   pow(x, NaN)                           = NaN
   *   pow(x, ±0)                            = 1

   *   pow(NaN, non-zero)                    = NaN
   *   pow(abs(x) > 1, +Infinity)            = +Infinity
   *   pow(abs(x) > 1, -Infinity)            = +0
   *   pow(abs(x) == 1, ±Infinity)           = NaN
   *   pow(abs(x) < 1, +Infinity)            = +0
   *   pow(abs(x) < 1, -Infinity)            = +Infinity
   *   pow(+Infinity, y > 0)                 = +Infinity
   *   pow(+Infinity, y < 0)                 = +0
   *   pow(-Infinity, odd integer > 0)       = -Infinity
   *   pow(-Infinity, even integer > 0)      = +Infinity
   *   pow(-Infinity, odd integer < 0)       = -0
   *   pow(-Infinity, even integer < 0)      = +0
   *   pow(+0, y > 0)                        = +0
   *   pow(+0, y < 0)                        = +Infinity
   *   pow(-0, odd integer > 0)              = -0
   *   pow(-0, even integer > 0)             = +0
   *   pow(-0, odd integer < 0)              = -Infinity
   *   pow(-0, even integer < 0)             = +Infinity
   *   pow(finite x < 0, finite non-integer) = NaN
   *
   * For non-integer or very large exponents pow(x, y) is calculated using
   *
   *   x^y = exp(y*ln(x))
   *
   * Assuming the first 15 rounding digits are each equally likely to be any digit 0-9, the
   * probability of an incorrectly rounded result
   * P([49]9{14} | [50]0{14}) = 2 * 0.2 * 10^-14 = 4e-15 = 1/2.5e+14
   * i.e. 1 in 250,000,000,000,000
   *
   * If a result is incorrectly rounded the maximum error will be 1 ulp (unit in last place).
   *
   * y {number|string|Decimal} The power to which to raise this Decimal.
   *
   */
  P.toPower = P.pow = function (y) {
    var e, k, pr, r, rm, s,
      x = this,
      Ctor = x.constructor,
      yn = +(y = new Ctor(y));

    // Either ±Infinity, NaN or ±0?
    if (!x.d || !y.d || !x.d[0] || !y.d[0]) return new Ctor(mathpow(+x, yn));

    x = new Ctor(x);

    if (x.eq(1)) return x;

    pr = Ctor.precision;
    rm = Ctor.rounding;

    if (y.eq(1)) return finalise(x, pr, rm);

    // y exponent
    e = mathfloor(y.e / LOG_BASE);

    // If y is a small integer use the 'exponentiation by squaring' algorithm.
    if (e >= y.d.length - 1 && (k = yn < 0 ? -yn : yn) <= MAX_SAFE_INTEGER) {
      r = intPow(Ctor, x, k, pr);
      return y.s < 0 ? new Ctor(1).div(r) : finalise(r, pr, rm);
    }

    s = x.s;

    // if x is negative
    if (s < 0) {

      // if y is not an integer
      if (e < y.d.length - 1) return new Ctor(NaN);

      // Result is positive if x is negative and the last digit of integer y is even.
      if ((y.d[e] & 1) == 0) s = 1;

      // if x.eq(-1)
      if (x.e == 0 && x.d[0] == 1 && x.d.length == 1) {
        x.s = s;
        return x;
      }
    }

    // Estimate result exponent.
    // x^y = 10^e,  where e = y * log10(x)
    // log10(x) = log10(x_significand) + x_exponent
    // log10(x_significand) = ln(x_significand) / ln(10)
    k = mathpow(+x, yn);
    e = k == 0 || !isFinite(k)
      ? mathfloor(yn * (Math.log('0.' + digitsToString(x.d)) / Math.LN10 + x.e + 1))
      : new Ctor(k + '').e;

    // Exponent estimate may be incorrect e.g. x: 0.999999999999999999, y: 2.29, e: 0, r.e: -1.

    // Overflow/underflow?
    if (e > Ctor.maxE + 1 || e < Ctor.minE - 1) return new Ctor(e > 0 ? s / 0 : 0);

    external = false;
    Ctor.rounding = x.s = 1;

    // Estimate the extra guard digits needed to ensure five correct rounding digits from
    // naturalLogarithm(x). Example of failure without these extra digits (precision: 10):
    // new Decimal(2.32456).pow('2087987436534566.46411')
    // should be 1.162377823e+764914905173815, but is 1.162355823e+764914905173815
    k = Math.min(12, (e + '').length);

    // r = x^y = exp(y*ln(x))
    r = naturalExponential(y.times(naturalLogarithm(x, pr + k)), pr);

    // r may be Infinity, e.g. (0.9999999999999999).pow(-1e+40)
    if (r.d) {

      // Truncate to the required precision plus five rounding digits.
      r = finalise(r, pr + 5, 1);

      // If the rounding digits are [49]9999 or [50]0000 increase the precision by 10 and recalculate
      // the result.
      if (checkRoundingDigits(r.d, pr, rm)) {
        e = pr + 10;

        // Truncate to the increased precision plus five rounding digits.
        r = finalise(naturalExponential(y.times(naturalLogarithm(x, e + k)), e), e + 5, 1);

        // Check for 14 nines from the 2nd rounding digit (the first rounding digit may be 4 or 9).
        if (+digitsToString(r.d).slice(pr + 1, pr + 15) + 1 == 1e14) {
          r = finalise(r, pr + 1, 0);
        }
      }
    }

    r.s = s;
    external = true;
    Ctor.rounding = rm;

    return finalise(r, pr, rm);
  };


  /*
   * Return a string representing the value of this Decimal rounded to `sd` significant digits
   * using rounding mode `rounding`.
   *
   * Return exponential notation if `sd` is less than the number of digits necessary to represent
   * the integer part of the value in normal notation.
   *
   * [sd] {number} Significant digits. Integer, 1 to MAX_DIGITS inclusive.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   */
  P.toPrecision = function (sd, rm) {
    var str,
      x = this,
      Ctor = x.constructor;

    if (sd === void 0) {
      str = finiteToString(x, x.e <= Ctor.toExpNeg || x.e >= Ctor.toExpPos);
    } else {
      checkInt32(sd, 1, MAX_DIGITS);

      if (rm === void 0) rm = Ctor.rounding;
      else checkInt32(rm, 0, 8);

      x = finalise(new Ctor(x), sd, rm);
      str = finiteToString(x, sd <= x.e || x.e <= Ctor.toExpNeg, sd);
    }

    return x.isNeg() && !x.isZero() ? '-' + str : str;
  };


  /*
   * Return a new Decimal whose value is the value of this Decimal rounded to a maximum of `sd`
   * significant digits using rounding mode `rm`, or to `precision` and `rounding` respectively if
   * omitted.
   *
   * [sd] {number} Significant digits. Integer, 1 to MAX_DIGITS inclusive.
   * [rm] {number} Rounding mode. Integer, 0 to 8 inclusive.
   *
   * 'toSD() digits out of range: {sd}'
   * 'toSD() digits not an integer: {sd}'
   * 'toSD() rounding mode not an integer: {rm}'
   * 'toSD() rounding mode out of range: {rm}'
   *
   */
  P.toSignificantDigits = P.toSD = function (sd, rm) {
    var x = this,
      Ctor = x.constructor;

    if (sd === void 0) {
      sd = Ctor.precision;
      rm = Ctor.rounding;
    } else {
      checkInt32(sd, 1, MAX_DIGITS);

      if (rm === void 0) rm = Ctor.rounding;
      else checkInt32(rm, 0, 8);
    }

    return finalise(new Ctor(x), sd, rm);
  };


  /*
   * Return a string representing the value of this Decimal.
   *
   * Return exponential notation if this Decimal has a positive exponent equal to or greater than
   * `toExpPos`, or a negative exponent equal to or less than `toExpNeg`.
   *
   */
  P.toString = function () {
    var x = this,
      Ctor = x.constructor,
      str = finiteToString(x, x.e <= Ctor.toExpNeg || x.e >= Ctor.toExpPos);

    return x.isNeg() && !x.isZero() ? '-' + str : str;
  };


  /*
   * Return a new Decimal whose value is the value of this Decimal truncated to a whole number.
   *
   */
  P.truncated = P.trunc = function () {
    return finalise(new this.constructor(this), this.e + 1, 1);
  };


  /*
   * Return a string representing the value of this Decimal.
   * Unlike `toString`, negative zero will include the minus sign.
   *
   */
  P.valueOf = P.toJSON = function () {
    var x = this,
      Ctor = x.constructor,
      str = finiteToString(x, x.e <= Ctor.toExpNeg || x.e >= Ctor.toExpPos);

    return x.isNeg() ? '-' + str : str;
  };


  /*
  // Add aliases to match BigDecimal method names.
  // P.add = P.plus;
  P.subtract = P.minus;
  P.multiply = P.times;
  P.divide = P.div;
  P.remainder = P.mod;
  P.compareTo = P.cmp;
  P.negate = P.neg;
   */


  // Helper functions for Decimal.prototype (P) and/or Decimal methods, and their callers.


  /*
   *  digitsToString           P.cubeRoot, P.logarithm, P.squareRoot, P.toFraction, P.toPower,
   *                           finiteToString, naturalExponential, naturalLogarithm
   *  checkInt32               P.toDecimalPlaces, P.toExponential, P.toFixed, P.toNearest,
   *                           P.toPrecision, P.toSignificantDigits, toStringBinary, random
   *  checkRoundingDigits      P.logarithm, P.toPower, naturalExponential, naturalLogarithm
   *  convertBase              toStringBinary, parseOther
   *  cos                      P.cos
   *  divide                   P.atanh, P.cubeRoot, P.dividedBy, P.dividedToIntegerBy,
   *                           P.logarithm, P.modulo, P.squareRoot, P.tan, P.tanh, P.toFraction,
   *                           P.toNearest, toStringBinary, naturalExponential, naturalLogarithm,
   *                           taylorSeries, atan2, parseOther
   *  finalise                 P.absoluteValue, P.atan, P.atanh, P.ceil, P.cos, P.cosh,
   *                           P.cubeRoot, P.dividedToIntegerBy, P.floor, P.logarithm, P.minus,
   *                           P.modulo, P.negated, P.plus, P.round, P.sin, P.sinh, P.squareRoot,
   *                           P.tan, P.times, P.toDecimalPlaces, P.toExponential, P.toFixed,
   *                           P.toNearest, P.toPower, P.toPrecision, P.toSignificantDigits,
   *                           P.truncated, divide, getLn10, getPi, naturalExponential,
   *                           naturalLogarithm, ceil, floor, round, trunc
   *  finiteToString           P.toExponential, P.toFixed, P.toPrecision, P.toString, P.valueOf,
   *                           toStringBinary
   *  getBase10Exponent        P.minus, P.plus, P.times, parseOther
   *  getLn10                  P.logarithm, naturalLogarithm
   *  getPi                    P.acos, P.asin, P.atan, toLessThanHalfPi, atan2
   *  getPrecision             P.precision, P.toFraction
   *  getZeroString            digitsToString, finiteToString
   *  intPow                   P.toPower, parseOther
   *  isOdd                    toLessThanHalfPi
   *  maxOrMin                 max, min
   *  naturalExponential       P.naturalExponential, P.toPower
   *  naturalLogarithm         P.acosh, P.asinh, P.atanh, P.logarithm, P.naturalLogarithm,
   *                           P.toPower, naturalExponential
   *  nonFiniteToString        finiteToString, toStringBinary
   *  parseDecimal             Decimal
   *  parseOther               Decimal
   *  sin                      P.sin
   *  taylorSeries             P.cosh, P.sinh, cos, sin
   *  toLessThanHalfPi         P.cos, P.sin
   *  toStringBinary           P.toBinary, P.toHexadecimal, P.toOctal
   *  truncate                 intPow
   *
   *  Throws:                  P.logarithm, P.precision, P.toFraction, checkInt32, getLn10, getPi,
   *                           naturalLogarithm, config, parseOther, random, Decimal
   */


  function digitsToString(d) {
    var i, k, ws,
      indexOfLastWord = d.length - 1,
      str = '',
      w = d[0];

    if (indexOfLastWord > 0) {
      str += w;
      for (i = 1; i < indexOfLastWord; i++) {
        ws = d[i] + '';
        k = LOG_BASE - ws.length;
        if (k) str += getZeroString(k);
        str += ws;
      }

      w = d[i];
      ws = w + '';
      k = LOG_BASE - ws.length;
      if (k) str += getZeroString(k);
    } else if (w === 0) {
      return '0';
    }

    // Remove trailing zeros of last w.
    for (; w % 10 === 0;) w /= 10;

    return str + w;
  }


  function checkInt32(i, min, max) {
    if (i !== ~~i || i < min || i > max) {
      throw Error(invalidArgument + i);
    }
  }


  /*
   * Check 5 rounding digits if `repeating` is null, 4 otherwise.
   * `repeating == null` if caller is `log` or `pow`,
   * `repeating != null` if caller is `naturalLogarithm` or `naturalExponential`.
   */
  function checkRoundingDigits(d, i, rm, repeating) {
    var di, k, r, rd;

    // Get the length of the first word of the array d.
    for (k = d[0]; k >= 10; k /= 10) --i;

    // Is the rounding digit in the first word of d?
    if (--i < 0) {
      i += LOG_BASE;
      di = 0;
    } else {
      di = Math.ceil((i + 1) / LOG_BASE);
      i %= LOG_BASE;
    }

    // i is the index (0 - 6) of the rounding digit.
    // E.g. if within the word 3487563 the first rounding digit is 5,
    // then i = 4, k = 1000, rd = 3487563 % 1000 = 563
    k = mathpow(10, LOG_BASE - i);
    rd = d[di] % k | 0;

    if (repeating == null) {
      if (i < 3) {
        if (i == 0) rd = rd / 100 | 0;
        else if (i == 1) rd = rd / 10 | 0;
        r = rm < 4 && rd == 99999 || rm > 3 && rd == 49999 || rd == 50000 || rd == 0;
      } else {
        r = (rm < 4 && rd + 1 == k || rm > 3 && rd + 1 == k / 2) &&
          (d[di + 1] / k / 100 | 0) == mathpow(10, i - 2) - 1 ||
            (rd == k / 2 || rd == 0) && (d[di + 1] / k / 100 | 0) == 0;
      }
    } else {
      if (i < 4) {
        if (i == 0) rd = rd / 1000 | 0;
        else if (i == 1) rd = rd / 100 | 0;
        else if (i == 2) rd = rd / 10 | 0;
        r = (repeating || rm < 4) && rd == 9999 || !repeating && rm > 3 && rd == 4999;
      } else {
        r = ((repeating || rm < 4) && rd + 1 == k ||
        (!repeating && rm > 3) && rd + 1 == k / 2) &&
          (d[di + 1] / k / 1000 | 0) == mathpow(10, i - 3) - 1;
      }
    }

    return r;
  }


  // Convert string of `baseIn` to an array of numbers of `baseOut`.
  // Eg. convertBase('255', 10, 16) returns [15, 15].
  // Eg. convertBase('ff', 16, 10) returns [2, 5, 5].
  function convertBase(str, baseIn, baseOut) {
    var j,
      arr = [0],
      arrL,
      i = 0,
      strL = str.length;

    for (; i < strL;) {
      for (arrL = arr.length; arrL--;) arr[arrL] *= baseIn;
      arr[0] += NUMERALS.indexOf(str.charAt(i++));
      for (j = 0; j < arr.length; j++) {
        if (arr[j] > baseOut - 1) {
          if (arr[j + 1] === void 0) arr[j + 1] = 0;
          arr[j + 1] += arr[j] / baseOut | 0;
          arr[j] %= baseOut;
        }
      }
    }

    return arr.reverse();
  }


  /*
   * cos(x) = 1 - x^2/2! + x^4/4! - ...
   * |x| < pi/2
   *
   */
  function cosine(Ctor, x) {
    var k, y,
      len = x.d.length;

    // Argument reduction: cos(4x) = 8*(cos^4(x) - cos^2(x)) + 1
    // i.e. cos(x) = 8*(cos^4(x/4) - cos^2(x/4)) + 1

    // Estimate the optimum number of times to use the argument reduction.
    if (len < 32) {
      k = Math.ceil(len / 3);
      y = Math.pow(4, -k).toString();
    } else {
      k = 16;
      y = '2.3283064365386962890625e-10';
    }

    Ctor.precision += k;

    x = taylorSeries(Ctor, 1, x.times(y), new Ctor(1));

    // Reverse argument reduction
    for (var i = k; i--;) {
      var cos2x = x.times(x);
      x = cos2x.times(cos2x).minus(cos2x).times(8).plus(1);
    }

    Ctor.precision -= k;

    return x;
  }


  /*
   * Perform division in the specified base.
   */
  var divide = (function () {

    // Assumes non-zero x and k, and hence non-zero result.
    function multiplyInteger(x, k, base) {
      var temp,
        carry = 0,
        i = x.length;

      for (x = x.slice(); i--;) {
        temp = x[i] * k + carry;
        x[i] = temp % base | 0;
        carry = temp / base | 0;
      }

      if (carry) x.unshift(carry);

      return x;
    }

    function compare(a, b, aL, bL) {
      var i, r;

      if (aL != bL) {
        r = aL > bL ? 1 : -1;
      } else {
        for (i = r = 0; i < aL; i++) {
          if (a[i] != b[i]) {
            r = a[i] > b[i] ? 1 : -1;
            break;
          }
        }
      }

      return r;
    }

    function subtract(a, b, aL, base) {
      var i = 0;

      // Subtract b from a.
      for (; aL--;) {
        a[aL] -= i;
        i = a[aL] < b[aL] ? 1 : 0;
        a[aL] = i * base + a[aL] - b[aL];
      }

      // Remove leading zeros.
      for (; !a[0] && a.length > 1;) a.shift();
    }

    return function (x, y, pr, rm, dp, base) {
      var cmp, e, i, k, logBase, more, prod, prodL, q, qd, rem, remL, rem0, sd, t, xi, xL, yd0,
        yL, yz,
        Ctor = x.constructor,
        sign = x.s == y.s ? 1 : -1,
        xd = x.d,
        yd = y.d;

      // Either NaN, Infinity or 0?
      if (!xd || !xd[0] || !yd || !yd[0]) {

        return new Ctor(// Return NaN if either NaN, or both Infinity or 0.
          !x.s || !y.s || (xd ? yd && xd[0] == yd[0] : !yd) ? NaN :

          // Return ±0 if x is 0 or y is ±Infinity, or return ±Infinity as y is 0.
          xd && xd[0] == 0 || !yd ? sign * 0 : sign / 0);
      }

      if (base) {
        logBase = 1;
        e = x.e - y.e;
      } else {
        base = BASE;
        logBase = LOG_BASE;
        e = mathfloor(x.e / logBase) - mathfloor(y.e / logBase);
      }

      yL = yd.length;
      xL = xd.length;
      q = new Ctor(sign);
      qd = q.d = [];

      // Result exponent may be one less than e.
      // The digit array of a Decimal from toStringBinary may have trailing zeros.
      for (i = 0; yd[i] == (xd[i] || 0); i++);

      if (yd[i] > (xd[i] || 0)) e--;

      if (pr == null) {
        sd = pr = Ctor.precision;
        rm = Ctor.rounding;
      } else if (dp) {
        sd = pr + (x.e - y.e) + 1;
      } else {
        sd = pr;
      }

      if (sd < 0) {
        qd.push(1);
        more = true;
      } else {

        // Convert precision in number of base 10 digits to base 1e7 digits.
        sd = sd / logBase + 2 | 0;
        i = 0;

        // divisor < 1e7
        if (yL == 1) {
          k = 0;
          yd = yd[0];
          sd++;

          // k is the carry.
          for (; (i < xL || k) && sd--; i++) {
            t = k * base + (xd[i] || 0);
            qd[i] = t / yd | 0;
            k = t % yd | 0;
          }

          more = k || i < xL;

        // divisor >= 1e7
        } else {

          // Normalise xd and yd so highest order digit of yd is >= base/2
          k = base / (yd[0] + 1) | 0;

          if (k > 1) {
            yd = multiplyInteger(yd, k, base);
            xd = multiplyInteger(xd, k, base);
            yL = yd.length;
            xL = xd.length;
          }

          xi = yL;
          rem = xd.slice(0, yL);
          remL = rem.length;

          // Add zeros to make remainder as long as divisor.
          for (; remL < yL;) rem[remL++] = 0;

          yz = yd.slice();
          yz.unshift(0);
          yd0 = yd[0];

          if (yd[1] >= base / 2) ++yd0;

          do {
            k = 0;

            // Compare divisor and remainder.
            cmp = compare(yd, rem, yL, remL);

            // If divisor < remainder.
            if (cmp < 0) {

              // Calculate trial digit, k.
              rem0 = rem[0];
              if (yL != remL) rem0 = rem0 * base + (rem[1] || 0);

              // k will be how many times the divisor goes into the current remainder.
              k = rem0 / yd0 | 0;

              //  Algorithm:
              //  1. product = divisor * trial digit (k)
              //  2. if product > remainder: product -= divisor, k--
              //  3. remainder -= product
              //  4. if product was < remainder at 2:
              //    5. compare new remainder and divisor
              //    6. If remainder > divisor: remainder -= divisor, k++

              if (k > 1) {
                if (k >= base) k = base - 1;

                // product = divisor * trial digit.
                prod = multiplyInteger(yd, k, base);
                prodL = prod.length;
                remL = rem.length;

                // Compare product and remainder.
                cmp = compare(prod, rem, prodL, remL);

                // product > remainder.
                if (cmp == 1) {
                  k--;

                  // Subtract divisor from product.
                  subtract(prod, yL < prodL ? yz : yd, prodL, base);
                }
              } else {

                // cmp is -1.
                // If k is 0, there is no need to compare yd and rem again below, so change cmp to 1
                // to avoid it. If k is 1 there is a need to compare yd and rem again below.
                if (k == 0) cmp = k = 1;
                prod = yd.slice();
              }

              prodL = prod.length;
              if (prodL < remL) prod.unshift(0);

              // Subtract product from remainder.
              subtract(rem, prod, remL, base);

              // If product was < previous remainder.
              if (cmp == -1) {
                remL = rem.length;

                // Compare divisor and new remainder.
                cmp = compare(yd, rem, yL, remL);

                // If divisor < new remainder, subtract divisor from remainder.
                if (cmp < 1) {
                  k++;

                  // Subtract divisor from remainder.
                  subtract(rem, yL < remL ? yz : yd, remL, base);
                }
              }

              remL = rem.length;
            } else if (cmp === 0) {
              k++;
              rem = [0];
            }    // if cmp === 1, k will be 0

            // Add the next digit, k, to the result array.
            qd[i++] = k;

            // Update the remainder.
            if (cmp && rem[0]) {
              rem[remL++] = xd[xi] || 0;
            } else {
              rem = [xd[xi]];
              remL = 1;
            }

          } while ((xi++ < xL || rem[0] !== void 0) && sd--);

          more = rem[0] !== void 0;
        }

        // Leading zero?
        if (!qd[0]) qd.shift();
      }

      // logBase is 1 when divide is being used for base conversion.
      if (logBase == 1) {
        q.e = e;
        inexact = more;
      } else {

        // To calculate q.e, first get the number of digits of qd[0].
        for (i = 1, k = qd[0]; k >= 10; k /= 10) i++;
        q.e = i + e * logBase - 1;

        finalise(q, dp ? pr + q.e + 1 : pr, rm, more);
      }

      return q;
    };
  })();


  /*
   * Round `x` to `sd` significant digits using rounding mode `rm`.
   * Check for over/under-flow.
   */
   function finalise(x, sd, rm, isTruncated) {
    var digits, i, j, k, rd, roundUp, w, xd, xdi,
      Ctor = x.constructor;

    // Don't round if sd is null or undefined.
    out: if (sd != null) {
      xd = x.d;

      // Infinity/NaN.
      if (!xd) return x;

      // rd: the rounding digit, i.e. the digit after the digit that may be rounded up.
      // w: the word of xd containing rd, a base 1e7 number.
      // xdi: the index of w within xd.
      // digits: the number of digits of w.
      // i: what would be the index of rd within w if all the numbers were 7 digits long (i.e. if
      // they had leading zeros)
      // j: if > 0, the actual index of rd within w (if < 0, rd is a leading zero).

      // Get the length of the first word of the digits array xd.
      for (digits = 1, k = xd[0]; k >= 10; k /= 10) digits++;
      i = sd - digits;

      // Is the rounding digit in the first word of xd?
      if (i < 0) {
        i += LOG_BASE;
        j = sd;
        w = xd[xdi = 0];

        // Get the rounding digit at index j of w.
        rd = w / mathpow(10, digits - j - 1) % 10 | 0;
      } else {
        xdi = Math.ceil((i + 1) / LOG_BASE);
        k = xd.length;
        if (xdi >= k) {
          if (isTruncated) {

            // Needed by `naturalExponential`, `naturalLogarithm` and `squareRoot`.
            for (; k++ <= xdi;) xd.push(0);
            w = rd = 0;
            digits = 1;
            i %= LOG_BASE;
            j = i - LOG_BASE + 1;
          } else {
            break out;
          }
        } else {
          w = k = xd[xdi];

          // Get the number of digits of w.
          for (digits = 1; k >= 10; k /= 10) digits++;

          // Get the index of rd within w.
          i %= LOG_BASE;

          // Get the index of rd within w, adjusted for leading zeros.
          // The number of leading zeros of w is given by LOG_BASE - digits.
          j = i - LOG_BASE + digits;

          // Get the rounding digit at index j of w.
          rd = j < 0 ? 0 : w / mathpow(10, digits - j - 1) % 10 | 0;
        }
      }

      // Are there any non-zero digits after the rounding digit?
      isTruncated = isTruncated || sd < 0 ||
        xd[xdi + 1] !== void 0 || (j < 0 ? w : w % mathpow(10, digits - j - 1));

      // The expression `w % mathpow(10, digits - j - 1)` returns all the digits of w to the right
      // of the digit at (left-to-right) index j, e.g. if w is 908714 and j is 2, the expression
      // will give 714.

      roundUp = rm < 4
        ? (rd || isTruncated) && (rm == 0 || rm == (x.s < 0 ? 3 : 2))
        : rd > 5 || rd == 5 && (rm == 4 || isTruncated || rm == 6 &&

          // Check whether the digit to the left of the rounding digit is odd.
          ((i > 0 ? j > 0 ? w / mathpow(10, digits - j) : 0 : xd[xdi - 1]) % 10) & 1 ||
            rm == (x.s < 0 ? 8 : 7));

      if (sd < 1 || !xd[0]) {
        xd.length = 0;
        if (roundUp) {

          // Convert sd to decimal places.
          sd -= x.e + 1;

          // 1, 0.1, 0.01, 0.001, 0.0001 etc.
          xd[0] = mathpow(10, (LOG_BASE - sd % LOG_BASE) % LOG_BASE);
          x.e = -sd || 0;
        } else {

          // Zero.
          xd[0] = x.e = 0;
        }

        return x;
      }

      // Remove excess digits.
      if (i == 0) {
        xd.length = xdi;
        k = 1;
        xdi--;
      } else {
        xd.length = xdi + 1;
        k = mathpow(10, LOG_BASE - i);

        // E.g. 56700 becomes 56000 if 7 is the rounding digit.
        // j > 0 means i > number of leading zeros of w.
        xd[xdi] = j > 0 ? (w / mathpow(10, digits - j) % mathpow(10, j) | 0) * k : 0;
      }

      if (roundUp) {
        for (;;) {

          // Is the digit to be rounded up in the first word of xd?
          if (xdi == 0) {

            // i will be the length of xd[0] before k is added.
            for (i = 1, j = xd[0]; j >= 10; j /= 10) i++;
            j = xd[0] += k;
            for (k = 1; j >= 10; j /= 10) k++;

            // if i != k the length has increased.
            if (i != k) {
              x.e++;
              if (xd[0] == BASE) xd[0] = 1;
            }

            break;
          } else {
            xd[xdi] += k;
            if (xd[xdi] != BASE) break;
            xd[xdi--] = 0;
            k = 1;
          }
        }
      }

      // Remove trailing zeros.
      for (i = xd.length; xd[--i] === 0;) xd.pop();
    }

    if (external) {

      // Overflow?
      if (x.e > Ctor.maxE) {

        // Infinity.
        x.d = null;
        x.e = NaN;

      // Underflow?
      } else if (x.e < Ctor.minE) {

        // Zero.
        x.e = 0;
        x.d = [0];
        // Ctor.underflow = true;
      } // else Ctor.underflow = false;
    }

    return x;
  }


  function finiteToString(x, isExp, sd) {
    if (!x.isFinite()) return nonFiniteToString(x);
    var k,
      e = x.e,
      str = digitsToString(x.d),
      len = str.length;

    if (isExp) {
      if (sd && (k = sd - len) > 0) {
        str = str.charAt(0) + '.' + str.slice(1) + getZeroString(k);
      } else if (len > 1) {
        str = str.charAt(0) + '.' + str.slice(1);
      }

      str = str + (x.e < 0 ? 'e' : 'e+') + x.e;
    } else if (e < 0) {
      str = '0.' + getZeroString(-e - 1) + str;
      if (sd && (k = sd - len) > 0) str += getZeroString(k);
    } else if (e >= len) {
      str += getZeroString(e + 1 - len);
      if (sd && (k = sd - e - 1) > 0) str = str + '.' + getZeroString(k);
    } else {
      if ((k = e + 1) < len) str = str.slice(0, k) + '.' + str.slice(k);
      if (sd && (k = sd - len) > 0) {
        if (e + 1 === len) str += '.';
        str += getZeroString(k);
      }
    }

    return str;
  }


  // Calculate the base 10 exponent from the base 1e7 exponent.
  function getBase10Exponent(digits, e) {
    var w = digits[0];

    // Add the number of digits of the first word of the digits array.
    for ( e *= LOG_BASE; w >= 10; w /= 10) e++;
    return e;
  }


  function getLn10(Ctor, sd, pr) {
    if (sd > LN10_PRECISION) {

      // Reset global state in case the exception is caught.
      external = true;
      if (pr) Ctor.precision = pr;
      throw Error(precisionLimitExceeded);
    }
    return finalise(new Ctor(LN10), sd, 1, true);
  }


  function getPi(Ctor, sd, rm) {
    if (sd > PI_PRECISION) throw Error(precisionLimitExceeded);
    return finalise(new Ctor(PI), sd, rm, true);
  }


  function getPrecision(digits) {
    var w = digits.length - 1,
      len = w * LOG_BASE + 1;

    w = digits[w];

    // If non-zero...
    if (w) {

      // Subtract the number of trailing zeros of the last word.
      for (; w % 10 == 0; w /= 10) len--;

      // Add the number of digits of the first word.
      for (w = digits[0]; w >= 10; w /= 10) len++;
    }

    return len;
  }


  function getZeroString(k) {
    var zs = '';
    for (; k--;) zs += '0';
    return zs;
  }


  /*
   * Return a new Decimal whose value is the value of Decimal `x` to the power `n`, where `n` is an
   * integer of type number.
   *
   * Implements 'exponentiation by squaring'. Called by `pow` and `parseOther`.
   *
   */
  function intPow(Ctor, x, n, pr) {
    var isTruncated,
      r = new Ctor(1),

      // Max n of 9007199254740991 takes 53 loop iterations.
      // Maximum digits array length; leaves [28, 34] guard digits.
      k = Math.ceil(pr / LOG_BASE + 4);

    external = false;

    for (;;) {
      if (n % 2) {
        r = r.times(x);
        if (truncate(r.d, k)) isTruncated = true;
      }

      n = mathfloor(n / 2);
      if (n === 0) {

        // To ensure correct rounding when r.d is truncated, increment the last word if it is zero.
        n = r.d.length - 1;
        if (isTruncated && r.d[n] === 0) ++r.d[n];
        break;
      }

      x = x.times(x);
      truncate(x.d, k);
    }

    external = true;

    return r;
  }


  function isOdd(n) {
    return n.d[n.d.length - 1] & 1;
  }


  /*
   * Handle `max` and `min`. `ltgt` is 'lt' or 'gt'.
   */
  function maxOrMin(Ctor, args, ltgt) {
    var y,
      x = new Ctor(args[0]),
      i = 0;

    for (; ++i < args.length;) {
      y = new Ctor(args[i]);
      if (!y.s) {
        x = y;
        break;
      } else if (x[ltgt](y)) {
        x = y;
      }
    }

    return x;
  }


  /*
   * Return a new Decimal whose value is the natural exponential of `x` rounded to `sd` significant
   * digits.
   *
   * Taylor/Maclaurin series.
   *
   * exp(x) = x^0/0! + x^1/1! + x^2/2! + x^3/3! + ...
   *
   * Argument reduction:
   *   Repeat x = x / 32, k += 5, until |x| < 0.1
   *   exp(x) = exp(x / 2^k)^(2^k)
   *
   * Previously, the argument was initially reduced by
   * exp(x) = exp(r) * 10^k  where r = x - k * ln10, k = floor(x / ln10)
   * to first put r in the range [0, ln10], before dividing by 32 until |x| < 0.1, but this was
   * found to be slower than just dividing repeatedly by 32 as above.
   *
   * Max integer argument: exp('20723265836946413') = 6.3e+9000000000000000
   * Min integer argument: exp('-20723265836946411') = 1.2e-9000000000000000
   * (Math object integer min/max: Math.exp(709) = 8.2e+307, Math.exp(-745) = 5e-324)
   *
   *  exp(Infinity)  = Infinity
   *  exp(-Infinity) = 0
   *  exp(NaN)       = NaN
   *  exp(±0)        = 1
   *
   *  exp(x) is non-terminating for any finite, non-zero x.
   *
   *  The result will always be correctly rounded.
   *
   */
  function naturalExponential(x, sd) {
    var denominator, guard, j, pow, sum, t, wpr,
      rep = 0,
      i = 0,
      k = 0,
      Ctor = x.constructor,
      rm = Ctor.rounding,
      pr = Ctor.precision;

    // 0/NaN/Infinity?
    if (!x.d || !x.d[0] || x.e > 17) {

      return new Ctor(x.d
        ? !x.d[0] ? 1 : x.s < 0 ? 0 : 1 / 0
        : x.s ? x.s < 0 ? 0 : x : 0 / 0);
    }

    if (sd == null) {
      external = false;
      wpr = pr;
    } else {
      wpr = sd;
    }

    t = new Ctor(0.03125);

    // while abs(x) >= 0.1
    while (x.e > -2) {

      // x = x / 2^5
      x = x.times(t);
      k += 5;
    }

    // Use 2 * log10(2^k) + 5 (empirically derived) to estimate the increase in precision
    // necessary to ensure the first 4 rounding digits are correct.
    guard = Math.log(mathpow(2, k)) / Math.LN10 * 2 + 5 | 0;
    wpr += guard;
    denominator = pow = sum = new Ctor(1);
    Ctor.precision = wpr;

    for (;;) {
      pow = finalise(pow.times(x), wpr, 1);
      denominator = denominator.times(++i);
      t = sum.plus(divide(pow, denominator, wpr, 1));

      if (digitsToString(t.d).slice(0, wpr) === digitsToString(sum.d).slice(0, wpr)) {
        j = k;
        while (j--) sum = finalise(sum.times(sum), wpr, 1);

        // Check to see if the first 4 rounding digits are [49]999.
        // If so, repeat the summation with a higher precision, otherwise
        // e.g. with precision: 18, rounding: 1
        // exp(18.404272462595034083567793919843761) = 98372560.1229999999 (should be 98372560.123)
        // `wpr - guard` is the index of first rounding digit.
        if (sd == null) {

          if (rep < 3 && checkRoundingDigits(sum.d, wpr - guard, rm, rep)) {
            Ctor.precision = wpr += 10;
            denominator = pow = t = new Ctor(1);
            i = 0;
            rep++;
          } else {
            return finalise(sum, Ctor.precision = pr, rm, external = true);
          }
        } else {
          Ctor.precision = pr;
          return sum;
        }
      }

      sum = t;
    }
  }


  /*
   * Return a new Decimal whose value is the natural logarithm of `x` rounded to `sd` significant
   * digits.
   *
   *  ln(-n)        = NaN
   *  ln(0)         = -Infinity
   *  ln(-0)        = -Infinity
   *  ln(1)         = 0
   *  ln(Infinity)  = Infinity
   *  ln(-Infinity) = NaN
   *  ln(NaN)       = NaN
   *
   *  ln(n) (n != 1) is non-terminating.
   *
   */
  function naturalLogarithm(y, sd) {
    var c, c0, denominator, e, numerator, rep, sum, t, wpr, x1, x2,
      n = 1,
      guard = 10,
      x = y,
      xd = x.d,
      Ctor = x.constructor,
      rm = Ctor.rounding,
      pr = Ctor.precision;

    // Is x negative or Infinity, NaN, 0 or 1?
    if (x.s < 0 || !xd || !xd[0] || !x.e && xd[0] == 1 && xd.length == 1) {
      return new Ctor(xd && !xd[0] ? -1 / 0 : x.s != 1 ? NaN : xd ? 0 : x);
    }

    if (sd == null) {
      external = false;
      wpr = pr;
    } else {
      wpr = sd;
    }

    Ctor.precision = wpr += guard;
    c = digitsToString(xd);
    c0 = c.charAt(0);

    if (Math.abs(e = x.e) < 1.5e15) {

      // Argument reduction.
      // The series converges faster the closer the argument is to 1, so using
      // ln(a^b) = b * ln(a),   ln(a) = ln(a^b) / b
      // multiply the argument by itself until the leading digits of the significand are 7, 8, 9,
      // 10, 11, 12 or 13, recording the number of multiplications so the sum of the series can
      // later be divided by this number, then separate out the power of 10 using
      // ln(a*10^b) = ln(a) + b*ln(10).

      // max n is 21 (gives 0.9, 1.0 or 1.1) (9e15 / 21 = 4.2e14).
      //while (c0 < 9 && c0 != 1 || c0 == 1 && c.charAt(1) > 1) {
      // max n is 6 (gives 0.7 - 1.3)
      while (c0 < 7 && c0 != 1 || c0 == 1 && c.charAt(1) > 3) {
        x = x.times(y);
        c = digitsToString(x.d);
        c0 = c.charAt(0);
        n++;
      }

      e = x.e;

      if (c0 > 1) {
        x = new Ctor('0.' + c);
        e++;
      } else {
        x = new Ctor(c0 + '.' + c.slice(1));
      }
    } else {

      // The argument reduction method above may result in overflow if the argument y is a massive
      // number with exponent >= 1500000000000000 (9e15 / 6 = 1.5e15), so instead recall this
      // function using ln(x*10^e) = ln(x) + e*ln(10).
      t = getLn10(Ctor, wpr + 2, pr).times(e + '');
      x = naturalLogarithm(new Ctor(c0 + '.' + c.slice(1)), wpr - guard).plus(t);
      Ctor.precision = pr;

      return sd == null ? finalise(x, pr, rm, external = true) : x;
    }

    // x1 is x reduced to a value near 1.
    x1 = x;

    // Taylor series.
    // ln(y) = ln((1 + x)/(1 - x)) = 2(x + x^3/3 + x^5/5 + x^7/7 + ...)
    // where x = (y - 1)/(y + 1)    (|x| < 1)
    sum = numerator = x = divide(x.minus(1), x.plus(1), wpr, 1);
    x2 = finalise(x.times(x), wpr, 1);
    denominator = 3;

    for (;;) {
      numerator = finalise(numerator.times(x2), wpr, 1);
      t = sum.plus(divide(numerator, new Ctor(denominator), wpr, 1));

      if (digitsToString(t.d).slice(0, wpr) === digitsToString(sum.d).slice(0, wpr)) {
        sum = sum.times(2);

        // Reverse the argument reduction. Check that e is not 0 because, besides preventing an
        // unnecessary calculation, -0 + 0 = +0 and to ensure correct rounding -0 needs to stay -0.
        if (e !== 0) sum = sum.plus(getLn10(Ctor, wpr + 2, pr).times(e + ''));
        sum = divide(sum, new Ctor(n), wpr, 1);

        // Is rm > 3 and the first 4 rounding digits 4999, or rm < 4 (or the summation has
        // been repeated previously) and the first 4 rounding digits 9999?
        // If so, restart the summation with a higher precision, otherwise
        // e.g. with precision: 12, rounding: 1
        // ln(135520028.6126091714265381533) = 18.7246299999 when it should be 18.72463.
        // `wpr - guard` is the index of first rounding digit.
        if (sd == null) {
          if (checkRoundingDigits(sum.d, wpr - guard, rm, rep)) {
            Ctor.precision = wpr += guard;
            t = numerator = x = divide(x1.minus(1), x1.plus(1), wpr, 1);
            x2 = finalise(x.times(x), wpr, 1);
            denominator = rep = 1;
          } else {
            return finalise(sum, Ctor.precision = pr, rm, external = true);
          }
        } else {
          Ctor.precision = pr;
          return sum;
        }
      }

      sum = t;
      denominator += 2;
    }
  }


  // ±Infinity, NaN.
  function nonFiniteToString(x) {
    // Unsigned.
    return String(x.s * x.s / 0);
  }


  /*
   * Parse the value of a new Decimal `x` from string `str`.
   */
  function parseDecimal(x, str) {
    var e, i, len;

    // Decimal point?
    if ((e = str.indexOf('.')) > -1) str = str.replace('.', '');

    // Exponential form?
    if ((i = str.search(/e/i)) > 0) {

      // Determine exponent.
      if (e < 0) e = i;
      e += +str.slice(i + 1);
      str = str.substring(0, i);
    } else if (e < 0) {

      // Integer.
      e = str.length;
    }

    // Determine leading zeros.
    for (i = 0; str.charCodeAt(i) === 48; i++);

    // Determine trailing zeros.
    for (len = str.length; str.charCodeAt(len - 1) === 48; --len);
    str = str.slice(i, len);

    if (str) {
      len -= i;
      x.e = e = e - i - 1;
      x.d = [];

      // Transform base

      // e is the base 10 exponent.
      // i is where to slice str to get the first word of the digits array.
      i = (e + 1) % LOG_BASE;
      if (e < 0) i += LOG_BASE;

      if (i < len) {
        if (i) x.d.push(+str.slice(0, i));
        for (len -= LOG_BASE; i < len;) x.d.push(+str.slice(i, i += LOG_BASE));
        str = str.slice(i);
        i = LOG_BASE - str.length;
      } else {
        i -= len;
      }

      for (; i--;) str += '0';
      x.d.push(+str);

      if (external) {

        // Overflow?
        if (x.e > x.constructor.maxE) {

          // Infinity.
          x.d = null;
          x.e = NaN;

        // Underflow?
        } else if (x.e < x.constructor.minE) {

          // Zero.
          x.e = 0;
          x.d = [0];
          // x.constructor.underflow = true;
        } // else x.constructor.underflow = false;
      }
    } else {

      // Zero.
      x.e = 0;
      x.d = [0];
    }

    return x;
  }


  /*
   * Parse the value of a new Decimal `x` from a string `str`, which is not a decimal value.
   */
  function parseOther(x, str) {
    var base, Ctor, divisor, i, isFloat, len, p, xd, xe;

    if (str === 'Infinity' || str === 'NaN') {
      if (!+str) x.s = NaN;
      x.e = NaN;
      x.d = null;
      return x;
    }

    if (isHex.test(str))  {
      base = 16;
      str = str.toLowerCase();
    } else if (isBinary.test(str))  {
      base = 2;
    } else if (isOctal.test(str))  {
      base = 8;
    } else {
      throw Error(invalidArgument + str);
    }

    // Is there a binary exponent part?
    i = str.search(/p/i);

    if (i > 0) {
      p = +str.slice(i + 1);
      str = str.substring(2, i);
    } else {
      str = str.slice(2);
    }

    // Convert `str` as an integer then divide the result by `base` raised to a power such that the
    // fraction part will be restored.
    i = str.indexOf('.');
    isFloat = i >= 0;
    Ctor = x.constructor;

    if (isFloat) {
      str = str.replace('.', '');
      len = str.length;
      i = len - i;

      // log[10](16) = 1.2041... , log[10](88) = 1.9444....
      divisor = intPow(Ctor, new Ctor(base), i, i * 2);
    }

    xd = convertBase(str, base, BASE);
    xe = xd.length - 1;

    // Remove trailing zeros.
    for (i = xe; xd[i] === 0; --i) xd.pop();
    if (i < 0) return new Ctor(x.s * 0);
    x.e = getBase10Exponent(xd, xe);
    x.d = xd;
    external = false;

    // At what precision to perform the division to ensure exact conversion?
    // maxDecimalIntegerPartDigitCount = ceil(log[10](b) * otherBaseIntegerPartDigitCount)
    // log[10](2) = 0.30103, log[10](8) = 0.90309, log[10](16) = 1.20412
    // E.g. ceil(1.2 * 3) = 4, so up to 4 decimal digits are needed to represent 3 hex int digits.
    // maxDecimalFractionPartDigitCount = {Hex:4|Oct:3|Bin:1} * otherBaseFractionPartDigitCount
    // Therefore using 4 * the number of digits of str will always be enough.
    if (isFloat) x = divide(x, divisor, len * 4);

    // Multiply by the binary exponent part if present.
    if (p) x = x.times(Math.abs(p) < 54 ? Math.pow(2, p) : Decimal.pow(2, p));
    external = true;

    return x;
  }


  /*
   * sin(x) = x - x^3/3! + x^5/5! - ...
   * |x| < pi/2
   *
   */
  function sine(Ctor, x) {
    var k,
      len = x.d.length;

    if (len < 3) return taylorSeries(Ctor, 2, x, x);

    // Argument reduction: sin(5x) = 16*sin^5(x) - 20*sin^3(x) + 5*sin(x)
    // i.e. sin(x) = 16*sin^5(x/5) - 20*sin^3(x/5) + 5*sin(x/5)
    // and  sin(x) = sin(x/5)(5 + sin^2(x/5)(16sin^2(x/5) - 20))

    // Estimate the optimum number of times to use the argument reduction.
    k = 1.4 * Math.sqrt(len);
    k = k > 16 ? 16 : k | 0;

    // Max k before Math.pow precision loss is 22
    x = x.times(Math.pow(5, -k));
    x = taylorSeries(Ctor, 2, x, x);

    // Reverse argument reduction
    var sin2_x,
      d5 = new Ctor(5),
      d16 = new Ctor(16),
      d20 = new Ctor(20);
    for (; k--;) {
      sin2_x = x.times(x);
      x = x.times(d5.plus(sin2_x.times(d16.times(sin2_x).minus(d20))));
    }

    return x;
  }


  // Calculate Taylor series for `cos`, `cosh`, `sin` and `sinh`.
  function taylorSeries(Ctor, n, x, y, isHyperbolic) {
    var j, t, u, x2,
      i = 1,
      pr = Ctor.precision,
      k = Math.ceil(pr / LOG_BASE);

    external = false;
    x2 = x.times(x);
    u = new Ctor(y);

    for (;;) {
      t = divide(u.times(x2), new Ctor(n++ * n++), pr, 1);
      u = isHyperbolic ? y.plus(t) : y.minus(t);
      y = divide(t.times(x2), new Ctor(n++ * n++), pr, 1);
      t = u.plus(y);

      if (t.d[k] !== void 0) {
        for (j = k; t.d[j] === u.d[j] && j--;);
        if (j == -1) break;
      }

      j = u;
      u = y;
      y = t;
      t = j;
      i++;
    }

    external = true;
    t.d.length = k + 1;

    return t;
  }


  // Return the absolute value of `x` reduced to less than or equal to half pi.
  function toLessThanHalfPi(Ctor, x) {
    var t,
      isNeg = x.s < 0,
      pi = getPi(Ctor, Ctor.precision, 1),
      halfPi = pi.times(0.5);

    x = x.abs();

    if (x.lte(halfPi)) {
      quadrant = isNeg ? 4 : 1;
      return x;
    }

    t = x.divToInt(pi);

    if (t.isZero()) {
      quadrant = isNeg ? 3 : 2;
    } else {
      x = x.minus(t.times(pi));

      // 0 <= x < pi
      if (x.lte(halfPi)) {
        quadrant = isOdd(t) ? (isNeg ? 2 : 3) : (isNeg ? 4 : 1);
        return x;
      }

      quadrant = isOdd(t) ? (isNeg ? 1 : 4) : (isNeg ? 3 : 2);
    }

    return x.minus(pi).abs();
  }


  /*
   * Return the value of Decimal `x` as a string in base `baseOut`.
   *
   * If the optional `sd` argument is present include a binary exponent suffix.
   */
  function toStringBinary(x, baseOut, sd, rm) {
    var base, e, i, k, len, roundUp, str, xd, y,
      Ctor = x.constructor,
      isExp = sd !== void 0;

    if (isExp) {
      checkInt32(sd, 1, MAX_DIGITS);
      if (rm === void 0) rm = Ctor.rounding;
      else checkInt32(rm, 0, 8);
    } else {
      sd = Ctor.precision;
      rm = Ctor.rounding;
    }

    if (!x.isFinite()) {
      str = nonFiniteToString(x);
    } else {
      str = finiteToString(x);
      i = str.indexOf('.');

      // Use exponential notation according to `toExpPos` and `toExpNeg`? No, but if required:
      // maxBinaryExponent = floor((decimalExponent + 1) * log[2](10))
      // minBinaryExponent = floor(decimalExponent * log[2](10))
      // log[2](10) = 3.321928094887362347870319429489390175864

      if (isExp) {
        base = 2;
        if (baseOut == 16) {
          sd = sd * 4 - 3;
        } else if (baseOut == 8) {
          sd = sd * 3 - 2;
        }
      } else {
        base = baseOut;
      }

      // Convert the number as an integer then divide the result by its base raised to a power such
      // that the fraction part will be restored.

      // Non-integer.
      if (i >= 0) {
        str = str.replace('.', '');
        y = new Ctor(1);
        y.e = str.length - i;
        y.d = convertBase(finiteToString(y), 10, base);
        y.e = y.d.length;
      }

      xd = convertBase(str, 10, base);
      e = len = xd.length;

      // Remove trailing zeros.
      for (; xd[--len] == 0;) xd.pop();

      if (!xd[0]) {
        str = isExp ? '0p+0' : '0';
      } else {
        if (i < 0) {
          e--;
        } else {
          x = new Ctor(x);
          x.d = xd;
          x.e = e;
          x = divide(x, y, sd, rm, 0, base);
          xd = x.d;
          e = x.e;
          roundUp = inexact;
        }

        // The rounding digit, i.e. the digit after the digit that may be rounded up.
        i = xd[sd];
        k = base / 2;
        roundUp = roundUp || xd[sd + 1] !== void 0;

        roundUp = rm < 4
          ? (i !== void 0 || roundUp) && (rm === 0 || rm === (x.s < 0 ? 3 : 2))
          : i > k || i === k && (rm === 4 || roundUp || rm === 6 && xd[sd - 1] & 1 ||
            rm === (x.s < 0 ? 8 : 7));

        xd.length = sd;

        if (roundUp) {

          // Rounding up may mean the previous digit has to be rounded up and so on.
          for (; ++xd[--sd] > base - 1;) {
            xd[sd] = 0;
            if (!sd) {
              ++e;
              xd.unshift(1);
            }
          }
        }

        // Determine trailing zeros.
        for (len = xd.length; !xd[len - 1]; --len);

        // E.g. [4, 11, 15] becomes 4bf.
        for (i = 0, str = ''; i < len; i++) str += NUMERALS.charAt(xd[i]);

        // Add binary exponent suffix?
        if (isExp) {
          if (len > 1) {
            if (baseOut == 16 || baseOut == 8) {
              i = baseOut == 16 ? 4 : 3;
              for (--len; len % i; len++) str += '0';
              xd = convertBase(str, base, baseOut);
              for (len = xd.length; !xd[len - 1]; --len);

              // xd[0] will always be be 1
              for (i = 1, str = '1.'; i < len; i++) str += NUMERALS.charAt(xd[i]);
            } else {
              str = str.charAt(0) + '.' + str.slice(1);
            }
          }

          str =  str + (e < 0 ? 'p' : 'p+') + e;
        } else if (e < 0) {
          for (; ++e;) str = '0' + str;
          str = '0.' + str;
        } else {
          if (++e > len) for (e -= len; e-- ;) str += '0';
          else if (e < len) str = str.slice(0, e) + '.' + str.slice(e);
        }
      }

      str = (baseOut == 16 ? '0x' : baseOut == 2 ? '0b' : baseOut == 8 ? '0o' : '') + str;
    }

    return x.s < 0 ? '-' + str : str;
  }


  // Does not strip trailing zeros.
  function truncate(arr, len) {
    if (arr.length > len) {
      arr.length = len;
      return true;
    }
  }


  // Decimal methods


  /*
   *  abs
   *  acos
   *  acosh
   *  add
   *  asin
   *  asinh
   *  atan
   *  atanh
   *  atan2
   *  cbrt
   *  ceil
   *  clone
   *  config
   *  cos
   *  cosh
   *  div
   *  exp
   *  floor
   *  hypot
   *  ln
   *  log
   *  log2
   *  log10
   *  max
   *  min
   *  mod
   *  mul
   *  pow
   *  random
   *  round
   *  set
   *  sign
   *  sin
   *  sinh
   *  sqrt
   *  sub
   *  tan
   *  tanh
   *  trunc
   */


  /*
   * Return a new Decimal whose value is the absolute value of `x`.
   *
   * x {number|string|Decimal}
   *
   */
  function abs(x) {
    return new this(x).abs();
  }


  /*
   * Return a new Decimal whose value is the arccosine in radians of `x`.
   *
   * x {number|string|Decimal}
   *
   */
  function acos(x) {
    return new this(x).acos();
  }


  /*
   * Return a new Decimal whose value is the inverse of the hyperbolic cosine of `x`, rounded to
   * `precision` significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function acosh(x) {
    return new this(x).acosh();
  }


  /*
   * Return a new Decimal whose value is the sum of `x` and `y`, rounded to `precision` significant
   * digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   * y {number|string|Decimal}
   *
   */
  function add(x, y) {
    return new this(x).plus(y);
  }


  /*
   * Return a new Decimal whose value is the arcsine in radians of `x`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   *
   */
  function asin(x) {
    return new this(x).asin();
  }


  /*
   * Return a new Decimal whose value is the inverse of the hyperbolic sine of `x`, rounded to
   * `precision` significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function asinh(x) {
    return new this(x).asinh();
  }


  /*
   * Return a new Decimal whose value is the arctangent in radians of `x`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   *
   */
  function atan(x) {
    return new this(x).atan();
  }


  /*
   * Return a new Decimal whose value is the inverse of the hyperbolic tangent of `x`, rounded to
   * `precision` significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function atanh(x) {
    return new this(x).atanh();
  }


  /*
   * Return a new Decimal whose value is the arctangent in radians of `y/x` in the range -pi to pi
   * (inclusive), rounded to `precision` significant digits using rounding mode `rounding`.
   *
   * Domain: [-Infinity, Infinity]
   * Range: [-pi, pi]
   *
   * y {number|string|Decimal} The y-coordinate.
   * x {number|string|Decimal} The x-coordinate.
   *
   * atan2(±0, -0)               = ±pi
   * atan2(±0, +0)               = ±0
   * atan2(±0, -x)               = ±pi for x > 0
   * atan2(±0, x)                = ±0 for x > 0
   * atan2(-y, ±0)               = -pi/2 for y > 0
   * atan2(y, ±0)                = pi/2 for y > 0
   * atan2(±y, -Infinity)        = ±pi for finite y > 0
   * atan2(±y, +Infinity)        = ±0 for finite y > 0
   * atan2(±Infinity, x)         = ±pi/2 for finite x
   * atan2(±Infinity, -Infinity) = ±3*pi/4
   * atan2(±Infinity, +Infinity) = ±pi/4
   * atan2(NaN, x) = NaN
   * atan2(y, NaN) = NaN
   *
   */
  function atan2(y, x) {
    y = new this(y);
    x = new this(x);
    var r,
      pr = this.precision,
      rm = this.rounding,
      wpr = pr + 4;

    // Either NaN
    if (!y.s || !x.s) {
      r = new this(NaN);

    // Both ±Infinity
    } else if (!y.d && !x.d) {
      r = getPi(this, wpr, 1).times(x.s > 0 ? 0.25 : 0.75);
      r.s = y.s;

    // x is ±Infinity or y is ±0
    } else if (!x.d || y.isZero()) {
      r = x.s < 0 ? getPi(this, pr, rm) : new this(0);
      r.s = y.s;

    // y is ±Infinity or x is ±0
    } else if (!y.d || x.isZero()) {
      r = getPi(this, wpr, 1).times(0.5);
      r.s = y.s;

    // Both non-zero and finite
    } else if (x.s < 0) {
      this.precision = wpr;
      this.rounding = 1;
      r = this.atan(divide(y, x, wpr, 1));
      x = getPi(this, wpr, 1);
      this.precision = pr;
      this.rounding = rm;
      r = y.s < 0 ? r.minus(x) : r.plus(x);
    } else {
      r = this.atan(divide(y, x, wpr, 1));
    }

    return r;
  }


  /*
   * Return a new Decimal whose value is the cube root of `x`, rounded to `precision` significant
   * digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   *
   */
  function cbrt(x) {
    return new this(x).cbrt();
  }


  /*
   * Return a new Decimal whose value is `x` rounded to an integer using `ROUND_CEIL`.
   *
   * x {number|string|Decimal}
   *
   */
  function ceil(x) {
    return finalise(x = new this(x), x.e + 1, 2);
  }


  /*
   * Configure global settings for a Decimal constructor.
   *
   * `obj` is an object with one or more of the following properties,
   *
   *   precision  {number}
   *   rounding   {number}
   *   toExpNeg   {number}
   *   toExpPos   {number}
   *   maxE       {number}
   *   minE       {number}
   *   modulo     {number}
   *   crypto     {boolean|number}
   *
   * E.g. Decimal.config({ precision: 20, rounding: 4 })
   *
   */
  function config(obj) {
    if (!obj || typeof obj !== 'object') throw Error(decimalError + 'Object expected');
    var i, p, v,
      ps = [
        'precision', 1, MAX_DIGITS,
        'rounding', 0, 8,
        'toExpNeg', -EXP_LIMIT, 0,
        'toExpPos', 0, EXP_LIMIT,
        'maxE', 0, EXP_LIMIT,
        'minE', -EXP_LIMIT, 0,
        'modulo', 0, 9
      ];

    for (i = 0; i < ps.length; i += 3) {
      if ((v = obj[p = ps[i]]) !== void 0) {
        if (mathfloor(v) === v && v >= ps[i + 1] && v <= ps[i + 2]) this[p] = v;
        else throw Error(invalidArgument + p + ': ' + v);
      }
    }

    if ((v = obj[p = 'crypto']) !== void 0) {
      if (v === true || v === false || v === 0 || v === 1) {
        if (v) {
          if (typeof crypto != 'undefined' && crypto &&
            (crypto.getRandomValues || crypto.randomBytes)) {
            this[p] = true;
          } else {
            throw Error(cryptoUnavailable);
          }
        } else {
          this[p] = false;
        }
      } else {
        throw Error(invalidArgument + p + ': ' + v);
      }
    }

    return this;
  }


  /*
   * Return a new Decimal whose value is the cosine of `x`, rounded to `precision` significant
   * digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function cos(x) {
    return new this(x).cos();
  }


  /*
   * Return a new Decimal whose value is the hyperbolic cosine of `x`, rounded to precision
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function cosh(x) {
    return new this(x).cosh();
  }


  /*
   * Create and return a Decimal constructor with the same configuration properties as this Decimal
   * constructor.
   *
   */
  function clone(obj) {
    var i, p, ps;

    /*
     * The Decimal constructor and exported function.
     * Return a new Decimal instance.
     *
     * v {number|string|Decimal} A numeric value.
     *
     */
    function Decimal(v) {
      var e, i, t,
        x = this;

      // Decimal called without new.
      if (!(x instanceof Decimal)) return new Decimal(v);

      // Retain a reference to this Decimal constructor, and shadow Decimal.prototype.constructor
      // which points to Object.
      x.constructor = Decimal;

      // Duplicate.
      if (v instanceof Decimal) {
        x.s = v.s;
        x.e = v.e;
        x.d = (v = v.d) ? v.slice() : v;
        return;
      }

      t = typeof v;

      if (t === 'number') {
        if (v === 0) {
          x.s = 1 / v < 0 ? -1 : 1;
          x.e = 0;
          x.d = [0];
          return;
        }

        if (v < 0) {
          v = -v;
          x.s = -1;
        } else {
          x.s = 1;
        }

        // Fast path for small integers.
        if (v === ~~v && v < 1e7) {
          for (e = 0, i = v; i >= 10; i /= 10) e++;
          x.e = e;
          x.d = [v];
          return;

        // Infinity, NaN.
        } else if (v * 0 !== 0) {
          if (!v) x.s = NaN;
          x.e = NaN;
          x.d = null;
          return;
        }

        return parseDecimal(x, v.toString());

      } else if (t !== 'string') {
        throw Error(invalidArgument + v);
      }

      // Minus sign?
      if (v.charCodeAt(0) === 45) {
        v = v.slice(1);
        x.s = -1;
      } else {
        x.s = 1;
      }

      return isDecimal.test(v) ? parseDecimal(x, v) : parseOther(x, v);
    }

    Decimal.prototype = P;

    Decimal.ROUND_UP = 0;
    Decimal.ROUND_DOWN = 1;
    Decimal.ROUND_CEIL = 2;
    Decimal.ROUND_FLOOR = 3;
    Decimal.ROUND_HALF_UP = 4;
    Decimal.ROUND_HALF_DOWN = 5;
    Decimal.ROUND_HALF_EVEN = 6;
    Decimal.ROUND_HALF_CEIL = 7;
    Decimal.ROUND_HALF_FLOOR = 8;
    Decimal.EUCLID = 9;

    Decimal.config = Decimal.set = config;
    Decimal.clone = clone;

    Decimal.abs = abs;
    Decimal.acos = acos;
    Decimal.acosh = acosh;        // ES6
    Decimal.add = add;
    Decimal.asin = asin;
    Decimal.asinh = asinh;        // ES6
    Decimal.atan = atan;
    Decimal.atanh = atanh;        // ES6
    Decimal.atan2 = atan2;
    Decimal.cbrt = cbrt;          // ES6
    Decimal.ceil = ceil;
    Decimal.cos = cos;
    Decimal.cosh = cosh;          // ES6
    Decimal.div = div;
    Decimal.exp = exp;
    Decimal.floor = floor;
    Decimal.hypot = hypot;        // ES6
    Decimal.ln = ln;
    Decimal.log = log;
    Decimal.log10 = log10;        // ES6
    Decimal.log2 = log2;          // ES6
    Decimal.max = max;
    Decimal.min = min;
    Decimal.mod = mod;
    Decimal.mul = mul;
    Decimal.pow = pow;
    Decimal.random = random;
    Decimal.round = round;
    Decimal.sign = sign;          // ES6
    Decimal.sin = sin;
    Decimal.sinh = sinh;          // ES6
    Decimal.sqrt = sqrt;
    Decimal.sub = sub;
    Decimal.tan = tan;
    Decimal.tanh = tanh;          // ES6
    Decimal.trunc = trunc;        // ES6

    if (obj === void 0) obj = {};
    if (obj) {
      ps = ['precision', 'rounding', 'toExpNeg', 'toExpPos', 'maxE', 'minE', 'modulo', 'crypto'];
      for (i = 0; i < ps.length;) if (!obj.hasOwnProperty(p = ps[i++])) obj[p] = this[p];
    }

    Decimal.config(obj);

    return Decimal;
  }


  /*
   * Return a new Decimal whose value is `x` divided by `y`, rounded to `precision` significant
   * digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   * y {number|string|Decimal}
   *
   */
  function div(x, y) {
    return new this(x).div(y);
  }


  /*
   * Return a new Decimal whose value is the natural exponential of `x`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} The power to which to raise the base of the natural log.
   *
   */
  function exp(x) {
    return new this(x).exp();
  }


  /*
   * Return a new Decimal whose value is `x` round to an integer using `ROUND_FLOOR`.
   *
   * x {number|string|Decimal}
   *
   */
  function floor(x) {
    return finalise(x = new this(x), x.e + 1, 3);
  }


  /*
   * Return a new Decimal whose value is the square root of the sum of the squares of the arguments,
   * rounded to `precision` significant digits using rounding mode `rounding`.
   *
   * hypot(a, b, ...) = sqrt(a^2 + b^2 + ...)
   *
   */
  function hypot() {
    var i, n,
      t = new this(0);

    external = false;

    for (i = 0; i < arguments.length;) {
      n = new this(arguments[i++]);
      if (!n.d) {
        if (n.s) {
          external = true;
          return new this(1 / 0);
        }
        t = n;
      } else if (t.d) {
        t = t.plus(n.times(n));
      }
    }

    external = true;

    return t.sqrt();
  }


  /*
   * Return a new Decimal whose value is the natural logarithm of `x`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   *
   */
  function ln(x) {
    return new this(x).ln();
  }


  /*
   * Return a new Decimal whose value is the log of `x` to the base `y`, or to base 10 if no base
   * is specified, rounded to `precision` significant digits using rounding mode `rounding`.
   *
   * log[y](x)
   *
   * x {number|string|Decimal} The argument of the logarithm.
   * y {number|string|Decimal} The base of the logarithm.
   *
   */
  function log(x, y) {
    return new this(x).log(y);
  }


  /*
   * Return a new Decimal whose value is the base 2 logarithm of `x`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   *
   */
  function log2(x) {
    return new this(x).log(2);
  }


  /*
   * Return a new Decimal whose value is the base 10 logarithm of `x`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   *
   */
  function log10(x) {
    return new this(x).log(10);
  }


  /*
   * Return a new Decimal whose value is the maximum of the arguments.
   *
   * arguments {number|string|Decimal}
   *
   */
  function max() {
    return maxOrMin(this, arguments, 'lt');
  }


  /*
   * Return a new Decimal whose value is the minimum of the arguments.
   *
   * arguments {number|string|Decimal}
   *
   */
  function min() {
    return maxOrMin(this, arguments, 'gt');
  }


  /*
   * Return a new Decimal whose value is `x` modulo `y`, rounded to `precision` significant digits
   * using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   * y {number|string|Decimal}
   *
   */
  function mod(x, y) {
    return new this(x).mod(y);
  }


  /*
   * Return a new Decimal whose value is `x` multiplied by `y`, rounded to `precision` significant
   * digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   * y {number|string|Decimal}
   *
   */
  function mul(x, y) {
    return new this(x).mul(y);
  }


  /*
   * Return a new Decimal whose value is `x` raised to the power `y`, rounded to precision
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} The base.
   * y {number|string|Decimal} The exponent.
   *
   */
  function pow(x, y) {
    return new this(x).pow(y);
  }


  /*
   * Returns a new Decimal with a random value equal to or greater than 0 and less than 1, and with
   * `sd`, or `Decimal.precision` if `sd` is omitted, significant digits (or less if trailing zeros
   * are produced).
   *
   * [sd] {number} Significant digits. Integer, 0 to MAX_DIGITS inclusive.
   *
   */
  function random(sd) {
    var d, e, k, n,
      i = 0,
      r = new this(1),
      rd = [];

    if (sd === void 0) sd = this.precision;
    else checkInt32(sd, 1, MAX_DIGITS);

    k = Math.ceil(sd / LOG_BASE);

    if (!this.crypto) {
      for (; i < k;) rd[i++] = Math.random() * 1e7 | 0;

    // Browsers supporting crypto.getRandomValues.
    } else if (crypto.getRandomValues) {
      d = crypto.getRandomValues(new Uint32Array(k));

      for (; i < k;) {
        n = d[i];

        // 0 <= n < 4294967296
        // Probability n >= 4.29e9, is 4967296 / 4294967296 = 0.00116 (1 in 865).
        if (n >= 4.29e9) {
          d[i] = crypto.getRandomValues(new Uint32Array(1))[0];
        } else {

          // 0 <= n <= 4289999999
          // 0 <= (n % 1e7) <= 9999999
          rd[i++] = n % 1e7;
        }
      }

    // Node.js supporting crypto.randomBytes.
    } else if (crypto.randomBytes) {

      // buffer
      d = crypto.randomBytes(k *= 4);

      for (; i < k;) {

        // 0 <= n < 2147483648
        n = d[i] + (d[i + 1] << 8) + (d[i + 2] << 16) + ((d[i + 3] & 0x7f) << 24);

        // Probability n >= 2.14e9, is 7483648 / 2147483648 = 0.0035 (1 in 286).
        if (n >= 2.14e9) {
          crypto.randomBytes(4).copy(d, i);
        } else {

          // 0 <= n <= 2139999999
          // 0 <= (n % 1e7) <= 9999999
          rd.push(n % 1e7);
          i += 4;
        }
      }

      i = k / 4;
    } else {
      throw Error(cryptoUnavailable);
    }

    k = rd[--i];
    sd %= LOG_BASE;

    // Convert trailing digits to zeros according to sd.
    if (k && sd) {
      n = mathpow(10, LOG_BASE - sd);
      rd[i] = (k / n | 0) * n;
    }

    // Remove trailing words which are zero.
    for (; rd[i] === 0; i--) rd.pop();

    // Zero?
    if (i < 0) {
      e = 0;
      rd = [0];
    } else {
      e = -1;

      // Remove leading words which are zero and adjust exponent accordingly.
      for (; rd[0] === 0; e -= LOG_BASE) rd.shift();

      // Count the digits of the first word of rd to determine leading zeros.
      for (k = 1, n = rd[0]; n >= 10; n /= 10) k++;

      // Adjust the exponent for leading zeros of the first word of rd.
      if (k < LOG_BASE) e -= LOG_BASE - k;
    }

    r.e = e;
    r.d = rd;

    return r;
  }


  /*
   * Return a new Decimal whose value is `x` rounded to an integer using rounding mode `rounding`.
   *
   * To emulate `Math.round`, set rounding to 7 (ROUND_HALF_CEIL).
   *
   * x {number|string|Decimal}
   *
   */
  function round(x) {
    return finalise(x = new this(x), x.e + 1, this.rounding);
  }


  /*
   * Return
   *   1    if x > 0,
   *  -1    if x < 0,
   *   0    if x is 0,
   *  -0    if x is -0,
   *   NaN  otherwise
   *
   */
  function sign(x) {
    x = new this(x);
    return x.d ? (x.d[0] ? x.s : 0 * x.s) : x.s || NaN;
  }


  /*
   * Return a new Decimal whose value is the sine of `x`, rounded to `precision` significant digits
   * using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function sin(x) {
    return new this(x).sin();
  }


  /*
   * Return a new Decimal whose value is the hyperbolic sine of `x`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function sinh(x) {
    return new this(x).sinh();
  }


  /*
   * Return a new Decimal whose value is the square root of `x`, rounded to `precision` significant
   * digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   *
   */
  function sqrt(x) {
    return new this(x).sqrt();
  }


  /*
   * Return a new Decimal whose value is `x` minus `y`, rounded to `precision` significant digits
   * using rounding mode `rounding`.
   *
   * x {number|string|Decimal}
   * y {number|string|Decimal}
   *
   */
  function sub(x, y) {
    return new this(x).sub(y);
  }


  /*
   * Return a new Decimal whose value is the tangent of `x`, rounded to `precision` significant
   * digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function tan(x) {
    return new this(x).tan();
  }


  /*
   * Return a new Decimal whose value is the hyperbolic tangent of `x`, rounded to `precision`
   * significant digits using rounding mode `rounding`.
   *
   * x {number|string|Decimal} A value in radians.
   *
   */
  function tanh(x) {
    return new this(x).tanh();
  }


  /*
   * Return a new Decimal whose value is `x` truncated to an integer.
   *
   * x {number|string|Decimal}
   *
   */
  function trunc(x) {
    return finalise(x = new this(x), x.e + 1, 1);
  }


  // Create and configure initial Decimal constructor.
  Decimal = clone(Decimal);

  Decimal['default'] = Decimal.Decimal = Decimal;

  // Create the internal constants from their string values.
  LN10 = new Decimal(LN10);
  PI = new Decimal(PI);


  // Export.


  // AMD.
  if (typeof define == 'function' && define.amd) {
    define(function () {
      return Decimal;
    });

  // Node and other environments that support module.exports.
  } else if (typeof module != 'undefined' && module.exports) {
    module.exports = Decimal;

  // Browser.
  } else {
    if (!globalScope) {
      globalScope = typeof self != 'undefined' && self && self.self == self
        ? self : Function('return this')();
    }

    noConflict = globalScope.Decimal;
    Decimal.noConflict = function () {
      globalScope.Decimal = noConflict;
      return Decimal;
    };

    globalScope.Decimal = Decimal;
  }
})(this);

},{}],35:[function(require,module,exports){
/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */
var byteToHex = [];
for (var i = 0; i < 256; ++i) {
  byteToHex[i] = (i + 0x100).toString(16).substr(1);
}

function bytesToUuid(buf, offset) {
  var i = offset || 0;
  var bth = byteToHex;
  // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4
  return ([
    bth[buf[i++]], bth[buf[i++]],
    bth[buf[i++]], bth[buf[i++]], '-',
    bth[buf[i++]], bth[buf[i++]], '-',
    bth[buf[i++]], bth[buf[i++]], '-',
    bth[buf[i++]], bth[buf[i++]], '-',
    bth[buf[i++]], bth[buf[i++]],
    bth[buf[i++]], bth[buf[i++]],
    bth[buf[i++]], bth[buf[i++]]
  ]).join('');
}

module.exports = bytesToUuid;

},{}],36:[function(require,module,exports){
// Unique ID creation requires a high quality random # generator.  In the
// browser this is a little complicated due to unknown quality of Math.random()
// and inconsistent support for the `crypto` API.  We do the best we can via
// feature-detection

// getRandomValues needs to be invoked in a context where "this" is a Crypto
// implementation. Also, find the complete implementation of crypto on IE11.
var getRandomValues = (typeof(crypto) != 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto)) ||
                      (typeof(msCrypto) != 'undefined' && typeof window.msCrypto.getRandomValues == 'function' && msCrypto.getRandomValues.bind(msCrypto));

if (getRandomValues) {
  // WHATWG crypto RNG - http://wiki.whatwg.org/wiki/Crypto
  var rnds8 = new Uint8Array(16); // eslint-disable-line no-undef

  module.exports = function whatwgRNG() {
    getRandomValues(rnds8);
    return rnds8;
  };
} else {
  // Math.random()-based (RNG)
  //
  // If all else fails, use Math.random().  It's fast, but is of unspecified
  // quality.
  var rnds = new Array(16);

  module.exports = function mathRNG() {
    for (var i = 0, r; i < 16; i++) {
      if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
      rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
    }

    return rnds;
  };
}

},{}],37:[function(require,module,exports){
var rng = require('./lib/rng');
var bytesToUuid = require('./lib/bytesToUuid');

function v4(options, buf, offset) {
  var i = buf && offset || 0;

  if (typeof(options) == 'string') {
    buf = options === 'binary' ? new Array(16) : null;
    options = null;
  }
  options = options || {};

  var rnds = options.random || (options.rng || rng)();

  // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
  rnds[6] = (rnds[6] & 0x0f) | 0x40;
  rnds[8] = (rnds[8] & 0x3f) | 0x80;

  // Copy bytes to buffer, if provided
  if (buf) {
    for (var ii = 0; ii < 16; ++ii) {
      buf[i + ii] = rnds[ii];
    }
  }

  return buf || bytesToUuid(rnds);
}

module.exports = v4;

},{"./lib/bytesToUuid":35,"./lib/rng":36}]},{},[24])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6L1VzZXJzL2RlbmlzbS9BcHBEYXRhL1JvYW1pbmcvbnBtL25vZGVfbW9kdWxlcy93YXRjaGlmeS9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwiQzovVXNlcnMvZGVuaXNtL0FwcERhdGEvUm9hbWluZy9ucG0vbm9kZV9tb2R1bGVzL3dhdGNoaWZ5L25vZGVfbW9kdWxlcy9ldmVudHMvZXZlbnRzLmpzIiwiYXNzZXRzXFxqc1xcYWN0aW9uc1xcTW91c2VIYW5kbGVyLmpzIiwiYXNzZXRzXFxqc1xcZXZlbnRzXFxFdmVudEZhY3RvcnkuanMiLCJhc3NldHNcXGpzXFxpbmZvX2Jsb2NrXFxJbmZvQmxvY2suanMiLCJhc3NldHNcXGpzXFxsYXllcnNcXExheWVyQVBJLmpzIiwiYXNzZXRzXFxqc1xcbWF0cml4XFxUcmFuc2Zvcm0uanMiLCJhc3NldHNcXGpzXFxtb2RlbHNcXEJhc2VNb2RlbC5qcyIsImFzc2V0c1xcanNcXG1vZGVsc1xcQmFzaWNNb2RlbC5qcyIsImFzc2V0c1xcanNcXG1vZGVsc1xcQ2FudmFzQm9yZGVyTW9kZWwuanMiLCJhc3NldHNcXGpzXFxtb2RlbHNcXEN1cnJlbnREcmF3aW5nTW9kZWwuanMiLCJhc3NldHNcXGpzXFxtb2RlbHNcXERyYXdpbmdTdGF0ZU1vZGVsLmpzIiwiYXNzZXRzXFxqc1xcbW9kZWxzXFxEcmF3aW5nVG9vbGJhck1vZGVsLmpzIiwiYXNzZXRzXFxqc1xcbW9kZWxzXFxHcmlkTW9kZWwuanMiLCJhc3NldHNcXGpzXFxtb2RlbHNcXFBhblpvb21Nb2RlbC5qcyIsImFzc2V0c1xcanNcXG1vZGVsc1xcUnVsZXJHdWlkZVxcSG9yaXpvbnRhbFJ1bGVyR3VpZGVNb2RlbC5qcyIsImFzc2V0c1xcanNcXG1vZGVsc1xcUnVsZXJHdWlkZVxcUnVsZXJHdWlkZS5qcyIsImFzc2V0c1xcanNcXG1vZGVsc1xcUnVsZXJHdWlkZVxcVmVydGljYWxSdWxlckd1aWRlTW9kZWwuanMiLCJhc3NldHNcXGpzXFxtb2RlbHNcXFJ1bGVyTW9kZWxcXEhvcml6b250YWxSdWxlck1vZGVsLmpzIiwiYXNzZXRzXFxqc1xcbW9kZWxzXFxSdWxlck1vZGVsXFxSdWxlck1vZGVsLmpzIiwiYXNzZXRzXFxqc1xcbW9kZWxzXFxSdWxlck1vZGVsXFxWZXJ0aWNhbFJ1bGVyTW9kZWwuanMiLCJhc3NldHNcXGpzXFxtb2RlbHNcXFNlbGVjdGVkU2hhcGVNb2RlbC5qcyIsImFzc2V0c1xcanNcXHB1YnN1YlxcUHViU3ViLmpzIiwiYXNzZXRzXFxqc1xcc2NhZmZvbGRcXEluaXRpYWxpemF0aW9uRmFjdG9yeS5qcyIsImFzc2V0c1xcanNcXHNjcmlwdC5qcyIsImFzc2V0c1xcanNcXHNoYXBlc1xcQ2lyY2xlLmpzIiwiYXNzZXRzXFxqc1xcc2hhcGVzXFxGcmVlRm9ybS5qcyIsImFzc2V0c1xcanNcXHNoYXBlc1xcTGluZS5qcyIsImFzc2V0c1xcanNcXHNoYXBlc1xcUG9pbnQuanMiLCJhc3NldHNcXGpzXFxzaGFwZXNcXFNoYXBlLmpzIiwiYXNzZXRzXFxqc1xcc2hhcGVzXFxTaGFwZUZhY3RvcnkuanMiLCJhc3NldHNcXGpzXFxzdG9yYWdlXFxzaGFwZXNcXFNoYXBlU3RvcmUuanMiLCJhc3NldHNcXGpzXFxzdG9yYWdlXFxzaGFwZXNcXFNoYXBlU3RvcmVBcnJheS5qcyIsImFzc2V0c1xcanNcXHV0aWxzXFx1dGlscy5qcyIsIi4uL25vZGVfbW9kdWxlcy9kZWNpbWFsLmpzL2RlY2ltYWwuanMiLCIuLi9ub2RlX21vZHVsZXMvdXVpZC9saWIvYnl0ZXNUb1V1aWQuanMiLCIuLi9ub2RlX21vZHVsZXMvdXVpZC9saWIvcm5nLWJyb3dzZXIuanMiLCIuLi9ub2RlX21vZHVsZXMvdXVpZC92NC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQzlTQTs7Ozs7QUFNQSxTQUFTLFlBQVQsQ0FBc0IsTUFBdEIsRUFBOEI7QUFFMUI7Ozs7O0FBS0EsTUFBSSxJQUFJLEdBQUcsSUFBWDs7QUFDQSxPQUFLLElBQUwsR0FBWSxVQUFVLE1BQVYsRUFBa0I7QUFFMUIsU0FBSyxNQUFMLEdBQWMsTUFBZDtBQUNBLFNBQUssT0FBTCxHQUFlLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxVQUFmLENBQTBCLElBQTFCLENBQWYsQ0FIMEIsQ0FHc0I7QUFFbkQsR0FMRDs7QUFPQSxPQUFLLElBQUwsQ0FBVSxNQUFWOztBQUVBLE9BQUsscUJBQUwsR0FBNkIsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXNCO0FBQy9DO0FBQ0EsUUFBSSxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQVAsRUFBYjtBQUVBLFFBQU0sQ0FBQyxHQUFHLEtBQUssR0FBRyxNQUFNLENBQUMsSUFBekI7QUFDQSxRQUFNLENBQUMsR0FBRyxLQUFLLEdBQUcsTUFBTSxDQUFDLEdBQXpCO0FBRUEsV0FBTztBQUFDLFdBQUksQ0FBTDtBQUFRLFdBQUk7QUFBWixLQUFQO0FBQ0gsR0FSRDtBQVVIOztBQUVELE1BQU0sQ0FBQyxPQUFQLEdBQWlCLFlBQWpCOzs7QUNuQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7O0lBUU0sWTs7Ozs7Ozs7OztBQUVGOzs7Ozs7Ozs4Q0FRa0M7QUFDakMsYUFBTztBQUNOLFFBQUEscUJBQXFCLEVBQUU7QUFBRTtBQUN4QixVQUFBLENBQUMsRUFBRSxFQURtQjtBQUV0QixVQUFBLENBQUMsRUFBRTtBQUZtQixTQURqQjtBQUtBLFFBQUEsNkJBQTZCLEVBQUU7QUFBRTtBQUM5QixVQUFBLENBQUMsRUFBRSxFQUR5QjtBQUU1QixVQUFBLENBQUMsRUFBRTtBQUZ5QixTQUwvQjtBQVNOLFFBQUEsU0FBUyxFQUFDLEtBVEo7QUFVTixRQUFBLFNBQVMsRUFBQyxLQVZKO0FBV0EsUUFBQSxlQUFlLEVBQUMsQ0FYaEI7QUFZQSxRQUFBLFNBQVMsRUFBQyxDQVpWO0FBYUEsUUFBQSxNQUFNLEVBQUMsRUFiUDtBQWFXO0FBQ1gsUUFBQSxrQkFBa0IsRUFBQyxDQWRuQixDQWNxQjs7QUFkckIsT0FBUDtBQWdCQTtBQUVEOzs7Ozs7Ozs7Ozs4Q0FRa0M7QUFDakMsYUFBTztBQUNOLFFBQUEsNkJBQTZCLEVBQUU7QUFBRTtBQUNoQyxVQUFBLENBQUMsRUFBRSxFQUQyQjtBQUU5QixVQUFBLENBQUMsRUFBRTtBQUYyQixTQUR6QjtBQUtOLFFBQUEsTUFBTSxFQUFFLE1BTEY7QUFNQSxRQUFBLFNBQVMsRUFBQztBQU5WLE9BQVA7QUFRQTtBQUVEOzs7Ozs7O3lDQUk2QjtBQUM1QixhQUFPO0FBQ04sUUFBQSxTQUFTLEVBQUU7QUFETCxPQUFQO0FBR0E7QUFDQzs7Ozs7OztxREFJeUM7QUFDdEMsYUFBTztBQUNKLFFBQUEsU0FBUyxFQUFFLENBRFA7QUFFSixRQUFBLE1BQU0sRUFBQyxFQUZIO0FBR0osUUFBQSxlQUFlLEVBQUU7QUFBRTtBQUNoQixVQUFBLENBQUMsRUFBRSxFQURXO0FBRWQsVUFBQSxDQUFDLEVBQUU7QUFGVyxTQUhiO0FBT0osUUFBQSxxQkFBcUIsRUFBRTtBQUFFO0FBQ3RCLFVBQUEsQ0FBQyxFQUFFLEVBRGlCO0FBRXBCLFVBQUEsQ0FBQyxFQUFFO0FBRmlCO0FBUG5CLE9BQVA7QUFZRjs7Ozs7O0FBR1AsTUFBTSxDQUFDLE9BQVAsR0FBaUIsWUFBakI7Ozs7O0FDdkZBLElBQUksWUFBWSxHQUFHLE9BQU8sQ0FBQyx3QkFBRCxDQUExQjtBQUNBOzs7Ozs7QUFLQSxTQUFTLFNBQVQsQ0FBbUIsT0FBbkIsRUFBNEI7QUFFeEI7Ozs7QUFJQSxNQUFJLElBQUksR0FBRyxJQUFYOztBQUNBLE9BQUssSUFBTCxHQUFZLFVBQVUsT0FBVixFQUFtQjtBQUUzQixTQUFLLFVBQUwsR0FBa0IsT0FBTyxDQUFDLFVBQTFCO0FBRUEsU0FBSyxNQUFMLEdBQWMsT0FBTyxDQUFDLE1BQXRCLENBSjJCLENBTTNCO0FBQ0k7QUFDSjs7QUFDQSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLGdCQUFmLEVBQWlDLFVBQVMsR0FBVCxFQUFjO0FBQzNDLFFBQUEsSUFBSSxDQUFDLGVBQUwsQ0FBcUIsSUFBckIsQ0FBMEIsSUFBMUIsRUFBZ0MsR0FBaEM7QUFDSCxPQUZEO0FBR0g7O0FBRUQsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxlQUFmLEVBQWdDLFVBQVMsR0FBVCxFQUFjO0FBQzFDLFFBQUEsSUFBSSxDQUFDLGtCQUFMLENBQXdCLElBQXhCLENBQTZCLElBQTdCLEVBQW1DLEdBQW5DO0FBQ0gsT0FGRDtBQUdIO0FBRUQ7Ozs7O0FBR0EsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxnQkFBZixFQUFpQyxVQUFTLEdBQVQsRUFBYztBQUMzQyxRQUFBLElBQUksQ0FBQyxlQUFMLENBQXFCLElBQXJCLENBQTBCLElBQTFCLEVBQWdDLEdBQWhDO0FBQ0gsT0FGRDtBQUdIO0FBQ0Q7Ozs7O0FBR0EsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxnQkFBZixFQUFpQyxVQUFTLEdBQVQsRUFBYztBQUMzQyxRQUFBLElBQUksQ0FBQyxjQUFMLENBQW9CLElBQXBCLENBQXlCLElBQXpCLEVBQStCLEdBQS9CO0FBQ0gsT0FGRDtBQUdIO0FBRUQ7Ozs7O0FBR0EsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxnQkFBZixFQUFpQyxVQUFTLEdBQVQsRUFBYztBQUMzQyxRQUFBLElBQUksQ0FBQyxjQUFMLENBQW9CLElBQXBCLENBQXlCLElBQXpCLEVBQStCLEdBQS9CO0FBQ0gsT0FGRDtBQUdIO0FBQ0Q7Ozs7O0FBR0EsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxZQUFmLEVBQTZCLFVBQVMsR0FBVCxFQUFjO0FBQ3ZDLFFBQUEsSUFBSSxDQUFDLG9CQUFMLENBQTBCLElBQTFCLENBQStCLElBQS9CLEVBQXFDLEdBQXJDO0FBQ0gsT0FGRDtBQUdIOztBQUNELElBQUEsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxnQkFBWixFQUE4QixVQUFVLEtBQVYsRUFBaUIsR0FBakIsRUFBdUI7QUFDakQsTUFBQSxJQUFJLENBQUMsd0JBQUwsQ0FBOEIsSUFBOUIsQ0FBbUMsSUFBbkMsRUFBeUMsR0FBekM7QUFDSCxLQUZEO0FBR0EsSUFBQSxDQUFDLENBQUMsU0FBRixDQUFZLG1CQUFaLEVBQWlDLFVBQVUsS0FBVixFQUFrQjtBQUMvQyxNQUFBLE9BQU8sQ0FBQyxJQUFSLENBQWEsSUFBSSxDQUFDLFVBQUwsQ0FBZ0Isd0JBQWhCLEVBQWI7QUFDSCxLQUZEO0FBR0EsSUFBQSxDQUFDLENBQUMsU0FBRixDQUFZLDRCQUFaLEVBQTBDLFVBQVUsS0FBVixFQUFpQixHQUFqQixFQUF1QjtBQUU3RDtBQUNBLE1BQUEsTUFBTSxDQUFDLFVBQUQsQ0FBTixDQUFtQixNQUFuQjtBQUVBLFVBQUksU0FBUyxHQUFHLEdBQUcsQ0FBQyxLQUFKLENBQVUsR0FBVixDQUFoQjtBQUNBLFVBQUksS0FBSyxHQUFPLEVBQWhCO0FBRUEsTUFBQSxDQUFDLENBQUMsSUFBRixDQUFPLFNBQVAsRUFBa0IsVUFBUyxHQUFULEVBQWMsS0FBZCxFQUFxQjtBQUNuQyxZQUFJLElBQUksR0FBRyxLQUFLLENBQUMsS0FBTixDQUFZLEdBQVosQ0FBWDs7QUFDQSxZQUFHLElBQUksQ0FBQyxDQUFELENBQUosS0FBWSxRQUFmLEVBQXdCO0FBQ3BCLFVBQUEsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFELENBQUwsQ0FBTCxHQUFpQixJQUFJLENBQUMsS0FBTCxDQUFXLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFELENBQUwsQ0FBN0IsQ0FBakI7QUFDQSxpQkFBTyxJQUFQLENBRm9CLENBRVA7QUFDaEI7O0FBQ0QsUUFBQSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUQsQ0FBTCxDQUFMLEdBQWlCLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFELENBQUwsQ0FBbkM7QUFDSCxPQVBEO0FBUUEsVUFBSSxRQUFRLEdBQUcsSUFBZjs7QUFDQSxVQUFHO0FBQ0MsUUFBQSxRQUFRLEdBQUcsWUFBWSxDQUFDLGNBQWIsQ0FBNEIsS0FBNUIsQ0FBWDtBQUNILE9BRkQsQ0FFRSxPQUFNLEVBQU4sRUFBUyxDQUNQO0FBQ0g7O0FBRUQsVUFBSSxRQUFRLEtBQUssSUFBYixJQUFxQixRQUFRLENBQUMsT0FBVCxFQUF6QixFQUE0QztBQUN4QyxRQUFBLElBQUksQ0FBQyxVQUFMLENBQWdCLFdBQWhCLENBQTRCLEtBQUssQ0FBQyxhQUFsQyxFQUFpRCxRQUFqRDtBQUVBLFFBQUEsSUFBSSxDQUFDLFVBQUwsQ0FBZ0IsZ0JBQWhCLENBQWlDLEtBQUssQ0FBQyxhQUF2QztBQUNIO0FBQ0osS0E1QkQ7QUE4QkEsSUFBQSxDQUFDLENBQUMsU0FBRixDQUFZLHdCQUFaLEVBQXNDLFVBQVUsS0FBVixFQUFpQixLQUFqQixFQUF5QjtBQUUzRCxVQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBTCxDQUFnQixRQUFoQixDQUF5QixLQUF6QixDQUFkLENBRjJELENBRzNEOztBQUNBLFVBQUksSUFBSSxHQUFHLG9CQUNILHFEQURHLEdBRUQsNEJBRkMsR0FJQyx1QkFKRCxHQUtDLDZCQUxELEdBTUcsNEJBTkgsR0FPSywyRUFQTCxHQVFLLGlEQVJMLEdBU0csUUFUSCxHQVVHLDBCQVZILEdBV0ssNEJBWEwsR0FZSyxpRkFaTCxHQWFLLFNBYkwsR0FjRyxRQWRILEdBZUcsNEJBZkgsR0FnQkssbUZBaEJMLEdBaUJHLFFBakJILEdBa0JDLFFBbEJELEdBb0JELFFBcEJDLEdBcUJILFFBckJSLENBSjJELENBMEIzRDs7QUFDQSxVQUFJLEtBQUssSUFBSSxJQUFiLEVBQW1CO0FBQ2YsUUFBQSxJQUFJLEdBQUcsb0JBQ0gscURBREcsR0FFRCw0QkFGQyxHQUlDLHVCQUpELEdBS0MsNkJBTEQsR0FNRyw0QkFOSCxHQU9LLDJFQVBMLEdBUUssMEJBUkwsR0FRa0MsS0FBSyxDQUFDLFdBQU4sQ0FBa0IsSUFScEQsR0FRMkQsT0FSM0QsR0FTRyxRQVRILEdBVUcsMEJBVkgsR0FXSyw0QkFYTCxHQVdtQyxLQUFLLENBQUMsWUFBTixFQVhuQyxHQVlLLDBDQVpMLEdBWWtELEtBQUssQ0FBQyxXQUFOLENBQWtCLElBWnBFLEdBWTJFLEtBWjNFLEdBYUssbURBYkwsR0FhMkQsS0FiM0QsR0FhbUUsS0FibkUsR0FjSyxTQWRMLEdBZUcsUUFmSCxHQWdCRyw0QkFoQkgsR0FpQkssdUpBakJMLEdBa0JLLG1GQWxCTCxHQW1CRyxRQW5CSCxHQW9CQyxRQXBCRCxHQXNCRCxRQXRCQyxHQXVCSCxRQXZCSjtBQXdCSCxPQXBEMEQsQ0FzRDNEOzs7QUFDQSxVQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBRCxDQUFiO0FBQ0EsTUFBQSxLQUFLLENBQUMsS0FBTixDQUFZO0FBQUMsUUFBQSxRQUFRLEVBQUU7QUFBWCxPQUFaO0FBQ0gsS0F6REQ7QUEyREgsR0FySkQ7O0FBdUpBLE9BQUssSUFBTCxDQUFVLE9BQVY7O0FBRUEsT0FBSyxrQkFBTCxHQUEwQixVQUFVLEdBQVYsRUFBZTtBQUVyQyxRQUFNLEtBQUssR0FBRyxHQUFHLENBQUMsS0FBSixDQUFVLE1BQVYsQ0FBa0IsVUFBUyxJQUFULEVBQWM7QUFDMUMsYUFBTyxJQUFJLENBQUMsSUFBTCxLQUFjLEdBQUcsQ0FBQyxRQUF6QjtBQUNILEtBRmEsQ0FBZDtBQUdBLElBQUEsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEIsSUFBOUIsQ0FBb0MsS0FBSyxDQUFDLE1BQU4sR0FBYyxDQUFsRDtBQUVILEdBUEQ7O0FBU0EsT0FBSyxlQUFMLEdBQXVCLFVBQVUsR0FBVixFQUFlO0FBQ2xDLElBQUEsQ0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQixJQUFqQixDQUFzQixHQUFHLENBQUMsU0FBMUI7QUFDSCxHQUZEOztBQUlBLE9BQUssY0FBTCxHQUFzQixVQUFVLEdBQVYsRUFBZTtBQUNqQyxJQUFBLENBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWSxJQUFaLENBQWlCLEdBQUcsQ0FBQyxlQUFKLENBQW9CLENBQXJDO0FBQ0EsSUFBQSxDQUFDLENBQUMsUUFBRCxDQUFELENBQVksSUFBWixDQUFpQixHQUFHLENBQUMsZUFBSixDQUFvQixDQUFyQztBQUNILEdBSEQ7O0FBSUEsT0FBSyxvQkFBTCxHQUE0QixVQUFVLEdBQVYsRUFBZTtBQUV2QyxJQUFBLENBQUMsQ0FBQyxVQUFELENBQUQsQ0FBYyxJQUFkLENBQW1CLEdBQUcsQ0FBQyxxQkFBSixDQUEwQixDQUE3QztBQUNBLElBQUEsQ0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjLElBQWQsQ0FBbUIsR0FBRyxDQUFDLHFCQUFKLENBQTBCLENBQTdDO0FBRUEsSUFBQSxDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QixJQUF2QixDQUE0QixHQUFHLENBQUMsNkJBQUosQ0FBa0MsQ0FBOUQ7QUFDQSxJQUFBLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCLElBQXZCLENBQTRCLEdBQUcsQ0FBQyw2QkFBSixDQUFrQyxDQUE5RDtBQUNILEdBUEQ7QUFRQTs7Ozs7QUFHQSxPQUFLLHdCQUFMLEdBQWdDLFVBQVUsR0FBVixFQUFlO0FBRTNDLFFBQU0sZUFBZSxHQUFHLHVEQUF4QjtBQUNBLFFBQU0sYUFBYSxHQUFHLFFBQXRCLENBSDJDLENBSTNDOztBQUNBLFFBQU0sTUFBTSxHQUFHLHlHQUF3RyxHQUFHLENBQUMsa0JBQTVHLEdBQWdJLHlCQUEvSTtBQUVBLElBQUEsQ0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUIsSUFBdkIsQ0FBNEIsZUFBZSxHQUFHLElBQUksQ0FBQyxVQUFMLENBQWdCLFFBQWhCLENBQXlCLEdBQUcsQ0FBQyxrQkFBN0IsRUFBaUQsUUFBakQsRUFBbEIsR0FBZ0YsUUFBaEYsR0FBMkYsTUFBM0YsR0FBb0csYUFBaEk7QUFDSCxHQVJEO0FBVUg7O0FBRUQsTUFBTSxDQUFDLE9BQVAsR0FBaUIsU0FBakI7Ozs7Ozs7Ozs7O0FDOU1BO0FBRUE7QUFDQTtBQUNBOztBQUNBOzs7Ozs7O0lBT00sUTs7Ozs7Ozs7OzJCQUNhLGMsRUFBZ0IsRSxFQUFJLE0sRUFBUTtBQUN2QyxVQUFJLG9CQUFvQixHQUFHLGNBQWMsQ0FBQyxNQUFmLEVBQTNCO0FBQ0EsVUFBSSxNQUFNLEdBQUcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYjtBQUNBLE1BQUEsTUFBTSxDQUFDLEVBQVAsR0FBZ0IsRUFBaEI7QUFDQSxNQUFBLE1BQU0sQ0FBQyxLQUFQLENBQWEsR0FBYixHQUFvQixvQkFBb0IsQ0FBQyxHQUFyQixHQUEyQixJQUEvQztBQUNBLE1BQUEsTUFBTSxDQUFDLEtBQVAsQ0FBYSxJQUFiLEdBQXFCLG9CQUFvQixDQUFDLElBQXJCLEdBQTRCLElBQWpEO0FBRUEsTUFBQSxNQUFNLENBQUMsS0FBUCxDQUFhLGFBQWIsR0FBNkIsTUFBN0I7QUFFQSxNQUFBLE1BQU0sQ0FBQyxLQUFQLEdBQWdCLGNBQWMsQ0FBQyxLQUFmLEVBQWhCO0FBQ0EsTUFBQSxNQUFNLENBQUMsTUFBUCxHQUFnQixjQUFjLENBQUMsTUFBZixFQUFoQjtBQUNBLE1BQUEsTUFBTSxDQUFDLEtBQVAsQ0FBYSxNQUFiLEdBQXdCLENBQXhCOztBQUNBLFVBQUcsTUFBSCxFQUFVO0FBQ04sUUFBQSxNQUFNLENBQUMsS0FBUCxDQUFhLE1BQWIsR0FBd0IsTUFBeEI7QUFDSDs7QUFDRCxNQUFBLE1BQU0sQ0FBQyxLQUFQLENBQWEsUUFBYixHQUF3QixVQUF4QjtBQUNBLE1BQUEsTUFBTSxDQUFDLEtBQVAsQ0FBYSxNQUFiLEdBQXdCLE1BQXhCO0FBQ0EsTUFBQSxRQUFRLENBQUMsSUFBVCxDQUFjLFdBQWQsQ0FBMEIsTUFBMUI7QUFDQSxhQUFPLE1BQVA7QUFDSDs7OzBCQUVhLGMsRUFBZ0IsRSxFQUFJO0FBQzlCLFVBQUcsY0FBSCxFQUFrQjtBQUNkLFlBQUksTUFBTSxHQUFHLEVBQWI7QUFDQSxZQUFJLG9CQUFvQixHQUFHLGNBQWMsQ0FBQyxNQUFmLEVBQTNCO0FBQ0EsWUFBSSxNQUFNLEdBQUcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYjtBQUNBLFFBQUEsTUFBTSxDQUFDLEVBQVAsR0FBZ0IsRUFBaEI7QUFDQSxRQUFBLE1BQU0sQ0FBQyxZQUFQLEdBQXNCLE1BQXRCLENBTGMsQ0FLZ0I7O0FBQzlCLFFBQUEsTUFBTSxDQUFDLEtBQVAsQ0FBYSxHQUFiLEdBQXFCLG9CQUFvQixDQUFDLEdBQXJCLEdBQTJCLE1BQTVCLEdBQXNDLElBQTFEO0FBQ0EsUUFBQSxNQUFNLENBQUMsS0FBUCxDQUFhLElBQWIsR0FBc0Isb0JBQW9CLENBQUMsSUFBckIsR0FBNEIsTUFBN0IsR0FBdUMsSUFBNUQ7QUFFQSxRQUFBLE1BQU0sQ0FBQyxLQUFQLENBQWEsYUFBYixHQUE2QixNQUE3QjtBQUVBLFFBQUEsTUFBTSxDQUFDLEtBQVAsR0FBZ0IsY0FBYyxDQUFDLEtBQWYsS0FBeUIsTUFBekM7QUFDQSxRQUFBLE1BQU0sQ0FBQyxNQUFQLEdBQWdCLGNBQWMsQ0FBQyxNQUFmLEtBQTBCLE1BQTFDO0FBQ0EsUUFBQSxNQUFNLENBQUMsS0FBUCxDQUFhLE1BQWIsR0FBd0IsQ0FBeEI7QUFDQSxRQUFBLE1BQU0sQ0FBQyxLQUFQLENBQWEsUUFBYixHQUF3QixVQUF4QjtBQUNBLFFBQUEsTUFBTSxDQUFDLEtBQVAsQ0FBYSxNQUFiLEdBQXdCLE1BQXhCO0FBQ0EsUUFBQSxRQUFRLENBQUMsSUFBVCxDQUFjLFdBQWQsQ0FBMEIsTUFBMUI7QUFDQSxlQUFPLE1BQVA7QUFDSDs7QUFDRCxhQUFPLElBQVA7QUFDSDs7Ozs7O0FBR0wsTUFBTSxDQUFDLE9BQVAsR0FBaUIsUUFBakI7Ozs7O0FDMURBOzs7Ozs7Ozs7Ozs7OztBQWVBLElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxZQUFELENBQXJCOztBQUVBLFNBQVMsU0FBVCxDQUFtQixPQUFuQixFQUE0QjtBQUV4QjtBQUNBLEVBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWTtBQUFFLElBQUEsU0FBUyxFQUFFLEVBQWI7QUFBaUIsSUFBQSxRQUFRLEVBQUU7QUFBM0IsR0FBWjtBQUVBLE9BQUssT0FBTCxHQUFlLE9BQWY7QUFDQSxPQUFLLE1BQUwsR0FBYyxDQUFDLElBQUksT0FBSixDQUFZLENBQVosQ0FBRCxFQUFpQixJQUFJLE9BQUosQ0FBWSxDQUFaLENBQWpCLEVBQWlDLElBQUksT0FBSixDQUFZLENBQVosQ0FBakMsRUFBaUQsSUFBSSxPQUFKLENBQVksQ0FBWixDQUFqRCxFQUFpRSxJQUFJLE9BQUosQ0FBWSxDQUFaLENBQWpFLEVBQWlGLElBQUksT0FBSixDQUFZLENBQVosQ0FBakYsQ0FBZCxDQU53QixDQU13Rjs7QUFDaEgsT0FBSyxLQUFMLEdBQWEsRUFBYixDQVB3QixDQVN4QjtBQUNBO0FBQ0E7O0FBRUEsT0FBSyxVQUFMLEdBQWtCLFVBQVMsT0FBVCxFQUFrQjtBQUNoQyxTQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0gsR0FGRDs7QUFJQSxPQUFLLFNBQUwsR0FBaUIsWUFBVztBQUV4QixRQUFNLEtBQUssR0FBRyxFQUFkOztBQUNBLFNBQUksSUFBSSxDQUFDLEdBQUMsQ0FBVixFQUFhLENBQUMsR0FBQyxLQUFLLE1BQUwsQ0FBWSxNQUEzQixFQUFtQyxDQUFDLEVBQXBDLEVBQXVDO0FBQ25DLE1BQUEsS0FBSyxDQUFDLElBQU4sQ0FBVyxLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsUUFBZixFQUFYO0FBQ0g7O0FBQ0QsV0FBTyxLQUFQO0FBQ0gsR0FQRDs7QUFTQSxPQUFLLGVBQUwsR0FBdUIsWUFBVztBQUM5QixXQUFPLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxRQUFmLEVBQVA7QUFDSCxHQUZEOztBQUlBLE9BQUssZUFBTCxHQUF1QixZQUFXO0FBQzlCLFdBQU8sS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLFFBQWYsRUFBUDtBQUNILEdBRkQ7O0FBSUEsT0FBSyxTQUFMLEdBQWlCLFlBQVc7QUFDeEIsV0FBTyxLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsUUFBZixFQUFQO0FBQ0gsR0FGRDs7QUFJQSxPQUFLLFNBQUwsR0FBaUIsWUFBVztBQUN4QixXQUFPLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxRQUFmLEVBQVA7QUFDSCxHQUZEOztBQUtBLE9BQUssU0FBTCxHQUFpQixVQUFTLENBQVQsRUFBWTtBQUN6QjtBQUNBLFNBQUssTUFBTCxHQUFjLENBQUMsSUFBSSxPQUFKLENBQVksQ0FBQyxDQUFDLENBQUQsQ0FBYixDQUFELEVBQW9CLElBQUksT0FBSixDQUFZLENBQUMsQ0FBQyxDQUFELENBQWIsQ0FBcEIsRUFBdUMsSUFBSSxPQUFKLENBQVksQ0FBQyxDQUFDLENBQUQsQ0FBYixDQUF2QyxFQUEwRCxJQUFJLE9BQUosQ0FBWSxDQUFDLENBQUMsQ0FBRCxDQUFiLENBQTFELEVBQTZFLElBQUksT0FBSixDQUFZLENBQUMsQ0FBQyxDQUFELENBQWIsQ0FBN0UsRUFBZ0csSUFBSSxPQUFKLENBQVksQ0FBQyxDQUFDLENBQUQsQ0FBYixDQUFoRyxDQUFkO0FBQ0EsU0FBSyxZQUFMO0FBQ0gsR0FKRDs7QUFNQSxPQUFLLFdBQUwsR0FBbUIsVUFBUyxDQUFULEVBQVk7QUFDM0IsV0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFELENBQUYsRUFBTSxDQUFDLENBQUMsQ0FBRCxDQUFQLEVBQVcsQ0FBQyxDQUFDLENBQUQsQ0FBWixFQUFnQixDQUFDLENBQUMsQ0FBRCxDQUFqQixFQUFxQixDQUFDLENBQUMsQ0FBRCxDQUF0QixFQUEwQixDQUFDLENBQUMsQ0FBRCxDQUEzQixDQUFQO0FBQ0gsR0FGRCxDQWpEd0IsQ0FxRHhCO0FBQ0E7QUFDQTs7O0FBRUEsT0FBSyxJQUFMLEdBQVksWUFBVztBQUNuQixRQUFJLE1BQU0sR0FBRyxLQUFLLFdBQUwsQ0FBaUIsS0FBSyxTQUFMLEVBQWpCLENBQWI7QUFDQSxTQUFLLEtBQUwsQ0FBVyxJQUFYLENBQWdCLE1BQWhCO0FBRUEsUUFBSSxLQUFLLE9BQVQsRUFBa0IsS0FBSyxPQUFMLENBQWEsSUFBYjtBQUNyQixHQUxEOztBQU9BLE9BQUssT0FBTCxHQUFlLFlBQVc7QUFDdEIsUUFBSSxLQUFLLEtBQUwsQ0FBVyxNQUFYLEdBQW9CLENBQXhCLEVBQTJCO0FBQ3ZCLFVBQUksTUFBTSxHQUFHLEtBQUssS0FBTCxDQUFXLEdBQVgsRUFBYjtBQUNBLFdBQUssU0FBTCxDQUFlLE1BQWY7QUFDSDs7QUFFRCxRQUFJLEtBQUssT0FBVCxFQUFrQixLQUFLLE9BQUwsQ0FBYSxPQUFiO0FBQ3JCLEdBUEQsQ0FoRXdCLENBeUV4QjtBQUNBO0FBQ0E7OztBQUVBLE9BQUssWUFBTCxHQUFvQixZQUFXO0FBQzNCLFFBQUksS0FBSyxPQUFULEVBQWtCO0FBQ2QsV0FBSyxPQUFMLENBQWEsWUFBYixDQUNJLEtBQUssTUFBTCxDQUFZLENBQVosQ0FESixFQUNvQjtBQUNoQixXQUFLLE1BQUwsQ0FBWSxDQUFaLENBRkosRUFFb0I7QUFDaEIsV0FBSyxNQUFMLENBQVksQ0FBWixDQUhKLEVBR29CO0FBQ2hCLFdBQUssTUFBTCxDQUFZLENBQVosQ0FKSixFQUlvQjtBQUNoQixXQUFLLE1BQUwsQ0FBWSxDQUFaLENBTEosRUFLb0I7QUFDaEIsV0FBSyxNQUFMLENBQVksQ0FBWixDQU5KLENBTW9CO0FBTnBCO0FBUUg7QUFDSixHQVhEO0FBYUE7Ozs7Ozs7Ozs7QUFRQSxPQUFLLGNBQUwsR0FBc0IsVUFBUyxDQUFULEVBQVksQ0FBWixFQUFlO0FBQ2pDLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLElBQWYsQ0FBb0IsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLEtBQWYsQ0FBcUIsQ0FBckIsRUFBd0IsSUFBeEIsQ0FBNkIsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLEtBQWYsQ0FBcUIsQ0FBckIsQ0FBN0IsQ0FBcEIsQ0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxJQUFmLENBQW9CLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxLQUFmLENBQXFCLENBQXJCLEVBQXdCLElBQXhCLENBQTZCLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxLQUFmLENBQXFCLENBQXJCLENBQTdCLENBQXBCLENBQWpCO0FBRUEsU0FBSyxZQUFMO0FBQ0gsR0FMRDs7QUFNQSxPQUFLLG1CQUFMLEdBQTJCLFVBQVMsQ0FBVCxFQUFZLENBQVosRUFBZTtBQUN0QztBQUNBO0FBQ0E7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxJQUFmLENBQW9CLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxLQUFmLENBQXFCLENBQXJCLEVBQXdCLElBQXhCLENBQTZCLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxLQUFmLENBQXFCLENBQXJCLENBQTdCLENBQXBCLENBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsSUFBZixDQUFvQixLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsS0FBZixDQUFxQixDQUFyQixFQUF3QixJQUF4QixDQUE2QixLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsS0FBZixDQUFxQixDQUFyQixDQUE3QixDQUFwQixDQUFqQjtBQUNILEdBTkQ7O0FBUUEsT0FBSyxTQUFMLEdBQWlCLFVBQVMsQ0FBVCxFQUFZLENBQVosRUFBZTtBQUM1QixTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLElBQUksT0FBSixDQUFZLENBQVosQ0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLElBQUksT0FBSixDQUFZLENBQVosQ0FBakI7QUFFQSxTQUFLLFlBQUw7QUFDSCxHQUxEOztBQU1BLE9BQUssY0FBTCxHQUFzQixVQUFTLENBQVQsRUFBWSxDQUFaLEVBQWU7QUFDakMsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixJQUFJLE9BQUosQ0FBWSxDQUFaLENBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixJQUFJLE9BQUosQ0FBWSxDQUFaLENBQWpCO0FBQ0gsR0FIRDs7QUFJQSxPQUFLLE1BQUwsR0FBYyxVQUFTLEdBQVQsRUFBYztBQUN4QixRQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBTCxDQUFTLEdBQVQsQ0FBUjtBQUNBLFFBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFMLENBQVMsR0FBVCxDQUFSO0FBQ0EsUUFBSSxHQUFHLEdBQUcsS0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixDQUFqQixHQUFxQixLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLENBQWhEO0FBQ0EsUUFBSSxHQUFHLEdBQUcsS0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixDQUFqQixHQUFxQixLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLENBQWhEO0FBQ0EsUUFBSSxHQUFHLEdBQUcsS0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixDQUFDLENBQWxCLEdBQXNCLEtBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsQ0FBakQ7QUFDQSxRQUFJLEdBQUcsR0FBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLENBQUMsQ0FBbEIsR0FBc0IsS0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixDQUFqRDtBQUNBLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsR0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEdBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixHQUFqQjtBQUNBLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsR0FBakI7QUFFQSxTQUFLLFlBQUw7QUFDSCxHQWJEOztBQWVBLE9BQUssS0FBTCxHQUFhLFVBQVMsRUFBVCxFQUFhLEVBQWIsRUFBaUI7QUFDMUIsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsS0FBZixDQUFxQixFQUFyQixDQUFqQjtBQUNBLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLEtBQWYsQ0FBcUIsRUFBckIsQ0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxLQUFmLENBQXFCLEVBQXJCLENBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsS0FBZixDQUFxQixFQUFyQixDQUFqQjtBQUVBLFNBQUssWUFBTDtBQUNILEdBUEQ7O0FBU0EsT0FBSyxRQUFMLEdBQWdCLFVBQVMsQ0FBVCxFQUFZLENBQVosRUFBZTtBQUMzQixTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLElBQUksT0FBSixDQUFZLENBQVosQ0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLElBQUksT0FBSixDQUFZLENBQVosQ0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLElBQUksT0FBSixDQUFZLENBQVosQ0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLElBQUksT0FBSixDQUFZLENBQVosQ0FBakI7QUFFQSxTQUFLLFlBQUw7QUFDSCxHQVBEOztBQVNBLE9BQUssVUFBTCxHQUFrQixVQUFTLEVBQVQsRUFBYSxFQUFiLEVBQWlCO0FBQy9CLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLEtBQWYsQ0FBcUIsRUFBckIsQ0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxLQUFmLENBQXFCLEVBQXJCLENBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsS0FBZixDQUFxQixFQUFyQixDQUFqQjtBQUNBLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLEtBQWYsQ0FBcUIsRUFBckIsQ0FBakIsQ0FKK0IsQ0FNL0I7QUFDSCxHQVBEO0FBUUE7Ozs7O0FBR0EsT0FBSyxjQUFMLEdBQXNCLFlBQVU7QUFDNUIsV0FBTyxLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQVA7QUFDSCxHQUZEO0FBR0E7Ozs7Ozs7Ozs7QUFTQSxPQUFLLFdBQUwsR0FBbUIsVUFBUyxFQUFULEVBQWEsRUFBYixFQUFpQixDQUFqQixFQUFvQixDQUFwQixFQUF1QjtBQUV0QztBQUNBO0FBQ0EsSUFBQSxFQUFFLEdBQUcsSUFBSSxPQUFKLENBQVksRUFBWixDQUFMO0FBQ0EsSUFBQSxFQUFFLEdBQUcsSUFBSSxPQUFKLENBQVksRUFBWixDQUFMO0FBRUEsSUFBQSxDQUFDLEdBQUcsSUFBSSxPQUFKLENBQVksQ0FBWixDQUFKO0FBQ0EsSUFBQSxDQUFDLEdBQUcsSUFBSSxPQUFKLENBQVksQ0FBWixDQUFKLENBUnNDLENBVXRDO0FBQ0E7QUFFQTtBQUNBO0FBRUE7O0FBQ0EsU0FBSyxtQkFBTCxDQUF5QixDQUF6QixFQUE0QixDQUE1QixFQWpCc0MsQ0FrQnRDO0FBQ0E7O0FBQ0EsU0FBSyxVQUFMLENBQWdCLEVBQWhCLEVBQW9CLEVBQXBCLEVBcEJzQyxDQXFCdEM7QUFDQTtBQUNEO0FBQ0M7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7QUFFQSxTQUFLLG1CQUFMLENBQXlCLENBQUMsQ0FBMUIsRUFBNkIsQ0FBQyxDQUE5QixFQS9Cc0MsQ0FnQ3RDO0FBRUE7O0FBRUEsU0FBSyxZQUFMLEdBcENzQyxDQXNDdEM7QUFDSCxHQXZDRCxDQWxMd0IsQ0EyTnhCO0FBQ0E7QUFDQTs7O0FBRUEsT0FBSyxhQUFMLEdBQXFCLFVBQVMsR0FBVCxFQUFjO0FBQy9CLFFBQUksR0FBRyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsRUFBWCxHQUFnQixHQUExQjtBQUNBLFNBQUssTUFBTCxDQUFZLEdBQVo7QUFDSCxHQUhEOztBQUtBLE9BQUssV0FBTCxHQUFtQixVQUFTLEdBQVQsRUFBYyxDQUFkLEVBQWlCLENBQWpCLEVBQW9CO0FBQ25DLFNBQUssU0FBTCxDQUFlLENBQWYsRUFBa0IsQ0FBbEI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxHQUFaO0FBQ0EsU0FBSyxTQUFMLENBQWUsQ0FBQyxDQUFoQixFQUFtQixDQUFDLENBQXBCO0FBQ0EsU0FBSyxZQUFMO0FBQ0gsR0FMRDs7QUFPQSxPQUFLLGtCQUFMLEdBQTBCLFVBQVMsR0FBVCxFQUFjLENBQWQsRUFBaUIsQ0FBakIsRUFBb0I7QUFDMUMsU0FBSyxTQUFMLENBQWUsQ0FBZixFQUFrQixDQUFsQjtBQUNBLFNBQUssYUFBTCxDQUFtQixHQUFuQjtBQUNBLFNBQUssU0FBTCxDQUFlLENBQUMsQ0FBaEIsRUFBbUIsQ0FBQyxDQUFwQjtBQUNBLFNBQUssWUFBTDtBQUNILEdBTEQ7O0FBT0EsT0FBSyxRQUFMLEdBQWdCLFlBQVc7QUFDdkIsU0FBSyxDQUFMLEdBQVMsQ0FBQyxDQUFELEVBQUcsQ0FBSCxFQUFLLENBQUwsRUFBTyxDQUFQLEVBQVMsQ0FBVCxFQUFXLENBQVgsQ0FBVDtBQUNBLFNBQUssWUFBTDtBQUNILEdBSEQ7O0FBS0EsT0FBSyxRQUFMLEdBQWdCLFVBQVMsTUFBVCxFQUFpQjtBQUM3QixRQUFJLEdBQUcsR0FBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLE1BQU0sQ0FBQyxDQUFQLENBQVMsQ0FBVCxDQUFqQixHQUErQixLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLE1BQU0sQ0FBQyxDQUFQLENBQVMsQ0FBVCxDQUExRDtBQUNBLFFBQUksR0FBRyxHQUFHLEtBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsTUFBTSxDQUFDLENBQVAsQ0FBUyxDQUFULENBQWpCLEdBQStCLEtBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsTUFBTSxDQUFDLENBQVAsQ0FBUyxDQUFULENBQTFEO0FBRUEsUUFBSSxHQUFHLEdBQUcsS0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixNQUFNLENBQUMsQ0FBUCxDQUFTLENBQVQsQ0FBakIsR0FBK0IsS0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixNQUFNLENBQUMsQ0FBUCxDQUFTLENBQVQsQ0FBMUQ7QUFDQSxRQUFJLEdBQUcsR0FBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLE1BQU0sQ0FBQyxDQUFQLENBQVMsQ0FBVCxDQUFqQixHQUErQixLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLE1BQU0sQ0FBQyxDQUFQLENBQVMsQ0FBVCxDQUExRDtBQUVBLFFBQUksRUFBRSxHQUFHLEtBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsTUFBTSxDQUFDLENBQVAsQ0FBUyxDQUFULENBQWpCLEdBQStCLEtBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsTUFBTSxDQUFDLENBQVAsQ0FBUyxDQUFULENBQWhELEdBQThELEtBQUssTUFBTCxDQUFZLENBQVosQ0FBdkU7QUFDQSxRQUFJLEVBQUUsR0FBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLE1BQU0sQ0FBQyxDQUFQLENBQVMsQ0FBVCxDQUFqQixHQUErQixLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLE1BQU0sQ0FBQyxDQUFQLENBQVMsQ0FBVCxDQUFoRCxHQUE4RCxLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQXZFO0FBRUEsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixHQUFqQjtBQUNBLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsR0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEdBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixHQUFqQjtBQUNBLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsRUFBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEVBQWpCO0FBQ0EsU0FBSyxZQUFMO0FBQ0gsR0FqQkQ7O0FBbUJBLE9BQUssTUFBTCxHQUFjLFlBQVc7QUFDckIsUUFBSSxDQUFDLEdBQUcsS0FBSyxLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBakIsR0FBa0MsS0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQXhELENBQVI7QUFDQSxRQUFJLEVBQUUsR0FBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLENBQTFCO0FBQ0EsUUFBSSxFQUFFLEdBQUcsQ0FBQyxLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQUQsR0FBa0IsQ0FBM0I7QUFDQSxRQUFJLEVBQUUsR0FBRyxDQUFDLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBRCxHQUFrQixDQUEzQjtBQUNBLFFBQUksRUFBRSxHQUFHLEtBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsQ0FBMUI7QUFDQSxRQUFJLEVBQUUsR0FBRyxDQUFDLElBQUksS0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQWpCLEdBQWtDLEtBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsS0FBSyxNQUFMLENBQVksQ0FBWixDQUF2RCxDQUFWO0FBQ0EsUUFBSSxFQUFFLEdBQUcsQ0FBQyxJQUFJLEtBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsS0FBSyxNQUFMLENBQVksQ0FBWixDQUFqQixHQUFrQyxLQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBdkQsQ0FBVjtBQUNBLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsRUFBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEVBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixFQUFqQjtBQUNBLFNBQUssTUFBTCxDQUFZLENBQVosSUFBaUIsRUFBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxDQUFaLElBQWlCLEVBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksQ0FBWixJQUFpQixFQUFqQjtBQUNBLFNBQUssWUFBTDtBQUNILEdBZkQsQ0ExUXdCLENBMlJ2QjtBQUNEO0FBQ0E7OztBQUVBLE9BQUssY0FBTCxHQUFzQixVQUFTLENBQVQsRUFBWSxDQUFaLEVBQWU7QUFDakMsV0FBTztBQUNILE1BQUEsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQUosR0FBcUIsQ0FBQyxHQUFHLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBekIsR0FBMEMsS0FBSyxNQUFMLENBQVksQ0FBWixDQUQxQztBQUVILE1BQUEsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQUosR0FBcUIsQ0FBQyxHQUFHLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBekIsR0FBMEMsS0FBSyxNQUFMLENBQVksQ0FBWjtBQUYxQyxLQUFQO0FBSUgsR0FMRDtBQU1IOztBQUVELE1BQU0sQ0FBQyxPQUFQLEdBQWlCLFNBQWpCOzs7OztBQ3hUQTs7Ozs7Ozs7O0FBVUEsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLHFCQUFELENBQXZCLEMsQ0FDQTs7O0FBRUEsU0FBUyxTQUFULENBQW1CLE9BQW5CLEVBQTRCO0FBRXhCLE1BQUksSUFBSjs7QUFFQSxPQUFLLElBQUwsR0FBWSxVQUFVLE9BQVYsRUFBbUI7QUFFM0IsUUFBRyxPQUFPLENBQUMsTUFBWCxFQUFrQjtBQUNkLFdBQUssT0FBTCxHQUFlLE9BQU8sQ0FBQyxNQUF2QjtBQUNIOztBQUVELFNBQUssU0FBTCxHQUFpQixDQUFqQjtBQUVBLFNBQUssZUFBTCxHQUF1QixFQUF2QjtBQUNBLFNBQUssZUFBTCxDQUFxQixDQUFyQixHQUF5QixDQUF6QjtBQUNBLFNBQUssZUFBTCxDQUFxQixDQUFyQixHQUF5QixDQUF6QjtBQUVBLFNBQUssZUFBTCxHQUF1QixFQUF2QixDQVoyQixDQVlEOztBQUMxQixTQUFLLGVBQUwsQ0FBcUIsQ0FBckIsR0FBeUIsQ0FBekI7QUFDQSxTQUFLLGVBQUwsQ0FBcUIsQ0FBckIsR0FBeUIsQ0FBekI7QUFFQSxTQUFLLE1BQUwsR0FBYyxPQUFPLENBQUMsTUFBdEI7O0FBRUEsUUFBRyxPQUFPLENBQUMsTUFBWCxFQUFrQjtBQUNkLFdBQUssTUFBTCxHQUFjLE9BQU8sQ0FBQyxNQUF0QjtBQUNIOztBQUVELFNBQUssT0FBTCxHQUFlLE9BQU8sQ0FBQyxPQUF2QjtBQUVBLElBQUEsSUFBSSxHQUFHLElBQVA7QUFFQTs7OztBQUdBOzs7O0FBR0EsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxnQkFBZixFQUFpQyxVQUFTLEdBQVQsRUFBYztBQUMzQyxRQUFBLElBQUksQ0FBQyxlQUFMLENBQXFCLElBQXJCLENBQTBCLElBQTFCLEVBQWdDLEdBQWhDO0FBQ0gsT0FGRDtBQUdIO0FBQ0Q7Ozs7QUFHQTs7Ozs7QUFJQSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLGdCQUFmLEVBQWlDLFVBQVMsR0FBVCxFQUFjO0FBQzNDLFFBQUEsSUFBSSxDQUFDLGVBQUwsQ0FBcUIsSUFBckIsQ0FBMEIsSUFBMUIsRUFBZ0MsR0FBaEM7QUFDSCxPQUZEO0FBR0g7QUFFRDs7Ozs7QUFHQSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLGdCQUFmLEVBQWlDLFVBQVMsR0FBVCxFQUFjO0FBQzNDLFFBQUEsSUFBSSxDQUFDLGNBQUwsQ0FBb0IsSUFBcEIsQ0FBeUIsSUFBekIsRUFBK0IsR0FBL0I7QUFDSCxPQUZEO0FBR0g7QUFFRDs7Ozs7QUFHQSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLGdCQUFmLEVBQWlDLFVBQVMsR0FBVCxFQUFjO0FBQzNDLFFBQUEsSUFBSSxDQUFDLG9CQUFMLENBQTBCLElBQTFCLENBQStCLElBQS9CLEVBQXFDLEdBQXJDO0FBQ0gsT0FGRDtBQUdIO0FBQ0Q7Ozs7QUFHQTs7O0FBR0E7OztBQUNBLFFBQUcsS0FBSyxNQUFSLEVBQWU7QUFDWCxXQUFLLE1BQUwsQ0FBWSxFQUFaLENBQWUsd0JBQWYsRUFBeUMsVUFBUyxHQUFULEVBQWM7QUFDbkQsUUFBQSxJQUFJLENBQUMsOEJBQUwsQ0FBb0MsSUFBcEMsQ0FBeUMsSUFBekMsRUFBK0MsR0FBL0M7QUFDSCxPQUZEO0FBR0g7O0FBRUQsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxnQkFBZixFQUFpQyxVQUFTLEdBQVQsRUFBYztBQUMzQyxRQUFBLElBQUksQ0FBQyx3QkFBTCxDQUE4QixJQUE5QixDQUFtQyxJQUFuQyxFQUF5QyxHQUF6QztBQUNILE9BRkQ7QUFHSDtBQUVKLEdBdEZEOztBQXdGQSxPQUFLLElBQUwsQ0FBVSxPQUFWO0FBQ0g7O0FBRUQsU0FBUyxDQUFDLFNBQVYsQ0FBb0IsUUFBcEIsR0FBK0IsVUFBVSxHQUFWLEVBQWU7QUFDMUMsU0FBTyxLQUFLLE9BQUwsQ0FBYSxTQUFiLENBQXVCLFNBQXZCLEVBQVA7QUFDSCxDQUZEOztBQUlBLFNBQVMsQ0FBQyxTQUFWLENBQW9CLFNBQXBCLEdBQWdDLFVBQVUsR0FBVixFQUFlO0FBQzNDLFNBQU8sS0FBSyxPQUFMLENBQWEsU0FBYixDQUF1QixTQUF2QixFQUFQO0FBQ0gsQ0FGRDs7QUFHQSxTQUFTLENBQUMsU0FBVixDQUFvQixTQUFwQixHQUFnQyxVQUFVLEdBQVYsRUFBZTtBQUMzQyxTQUFPLEtBQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsU0FBdkIsRUFBUDtBQUNILENBRkQ7QUFHQTs7Ozs7Ozs7OztBQVFBLFNBQVMsQ0FBQyxTQUFWLENBQW9CLHdCQUFwQixHQUErQyxVQUFVLEdBQVYsRUFBZSxXQUFmLEVBQTRCO0FBQ3ZFLE1BQUksTUFBTSxHQUFHLENBQWI7O0FBQ0EsTUFBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsWUFBZixLQUFnQyxTQUFuQyxFQUE2QztBQUN6QyxJQUFBLE1BQU0sR0FBRyxLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsWUFBeEI7QUFDSCxHQUpzRSxDQUt2RTs7O0FBQ0EsT0FBSyxNQUFMLENBQVksS0FBWixDQUFrQixHQUFHLENBQUMsTUFBSixDQUFXLEtBQVgsS0FBbUIsTUFBckM7QUFDQSxPQUFLLE1BQUwsQ0FBWSxNQUFaLENBQW1CLEdBQUcsQ0FBQyxNQUFKLENBQVcsTUFBWCxLQUFvQixNQUF2QyxFQVB1RSxDQVF2RTs7QUFDQSxPQUFLLE9BQUwsQ0FBYSxNQUFiLENBQW9CLEtBQXBCLEdBQTRCLEdBQUcsQ0FBQyxNQUFKLENBQVcsS0FBWCxLQUFtQixNQUEvQztBQUNBLE9BQUssT0FBTCxDQUFhLE1BQWIsQ0FBb0IsTUFBcEIsR0FBNkIsR0FBRyxDQUFDLE1BQUosQ0FBVyxNQUFYLEtBQW9CLE1BQWpELENBVnVFLENBV3ZFOztBQUNBLE9BQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsU0FBdkIsQ0FDSSxLQUFLLGVBQUwsQ0FBcUIsQ0FEekIsRUFDNEIsS0FBSyxlQUFMLENBQXFCLENBRGpEO0FBR0E7OztBQUdBOztBQUNBLE1BQUksV0FBSixFQUFpQjtBQUNiO0FBQ0g7O0FBQ0QsT0FBSyxNQUFMLENBQVksSUFBWixDQUFpQixJQUFqQixFQUF1QixHQUF2QjtBQUNILENBdkJEO0FBd0JBOzs7Ozs7Ozs7QUFPQSxTQUFTLENBQUMsU0FBVixDQUFvQiw4QkFBcEIsR0FBcUQsVUFBVSxHQUFWLEVBQWU7QUFDbkUsTUFBSSxHQUFHLEtBQUssSUFBWixFQUFpQjtBQUNoQjtBQUNBLEdBSGtFLENBSW5FOzs7QUFDRyxPQUFLLGVBQUwsR0FBdUIsR0FBRyxDQUFDLDZCQUEzQjtBQUNILENBTkQ7O0FBUUEsU0FBUyxDQUFDLFNBQVYsQ0FBb0IsZUFBcEIsR0FBc0MsVUFBVSxHQUFWLEVBQWU7QUFDakQsT0FBSyxTQUFMLElBQWtCLEdBQUcsQ0FBQyxLQUF0QjtBQUVBLE9BQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsV0FBdkIsQ0FDSSxHQUFHLENBQUMsS0FEUixFQUNlLEdBQUcsQ0FBQyxLQURuQixFQUMwQixHQUFHLENBQUMsTUFBSixDQUFXLENBRHJDLEVBQ3dDLEdBQUcsQ0FBQyxNQUFKLENBQVcsQ0FEbkQsRUFIaUQsQ0FPakQ7O0FBQ0EsT0FBSyxlQUFMLENBQXFCLENBQXJCLEdBQXlCLEtBQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsZUFBdkIsRUFBekI7QUFDQSxPQUFLLGVBQUwsQ0FBcUIsQ0FBckIsR0FBeUIsS0FBSyxPQUFMLENBQWEsU0FBYixDQUF1QixlQUF2QixFQUF6QjtBQUVBLE9BQUssTUFBTCxDQUFZLEtBQUssT0FBakIsRUFBMEIsR0FBMUI7QUFDSCxDQVpEOztBQWNBLFNBQVMsQ0FBQyxTQUFWLENBQW9CLGNBQXBCLEdBQXFDLFVBQVUsR0FBVixFQUFlO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBSyxlQUFMLENBQXFCLENBQXJCLElBQTBCLEtBQUssMEJBQUwsQ0FBZ0MsR0FBRyxDQUFDLHFCQUFKLENBQTBCLENBQTFELEVBQTZELEtBQUssZUFBTCxDQUFxQixDQUFsRixFQUFxRixLQUFLLGVBQUwsQ0FBcUIsQ0FBMUcsQ0FBMUI7QUFFQSxPQUFLLGVBQUwsQ0FBcUIsQ0FBckIsSUFBMEIsS0FBSywwQkFBTCxDQUFnQyxHQUFHLENBQUMscUJBQUosQ0FBMEIsQ0FBMUQsRUFBNkQsS0FBSyxlQUFMLENBQXFCLENBQWxGLEVBQXFGLEtBQUssZUFBTCxDQUFxQixDQUExRyxDQUExQjtBQUVBLE9BQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsU0FBdkIsQ0FDSSxLQUFLLGVBQUwsQ0FBcUIsQ0FEekIsRUFDNEIsS0FBSyxlQUFMLENBQXFCLENBRGpEO0FBSUEsT0FBSyxNQUFMLENBQVksS0FBSyxPQUFqQixFQUEwQixHQUExQjtBQUNILENBZEQ7QUFnQkE7Ozs7Ozs7QUFLQSxTQUFTLENBQUMsU0FBVixDQUFvQixvQkFBcEIsR0FBMkMsVUFBVSxHQUFWLEVBQWU7QUFDdEQsT0FBSyxlQUFMLEdBQXVCLEdBQUcsQ0FBQyxNQUEzQjtBQUVBLE9BQUssZUFBTCxDQUFxQixDQUFyQixJQUEwQixLQUFLLDBCQUFMLENBQWdDLEdBQUcsQ0FBQyxxQkFBSixDQUEwQixDQUExRCxFQUE2RCxLQUFLLGVBQUwsQ0FBcUIsQ0FBbEYsRUFBcUYsS0FBSyxlQUFMLENBQXFCLENBQTFHLENBQTFCO0FBRUEsT0FBSyxlQUFMLENBQXFCLENBQXJCLElBQTBCLEtBQUssMEJBQUwsQ0FBZ0MsR0FBRyxDQUFDLHFCQUFKLENBQTBCLENBQTFELEVBQTZELEtBQUssZUFBTCxDQUFxQixDQUFsRixFQUFxRixLQUFLLGVBQUwsQ0FBcUIsQ0FBMUcsQ0FBMUI7QUFFQSxPQUFLLE9BQUwsQ0FBYSxTQUFiLENBQXVCLFNBQXZCLENBQ0ksS0FBSyxlQUFMLENBQXFCLENBRHpCLEVBQzRCLEtBQUssZUFBTCxDQUFxQixDQURqRDtBQUlBLE9BQUssTUFBTCxDQUFZLEtBQUssT0FBakIsRUFBMEIsR0FBMUI7QUFDSCxDQVpEO0FBY0E7Ozs7Ozs7Ozs7QUFRQSxTQUFTLENBQUMsU0FBVixDQUFvQiwwQkFBcEIsR0FBaUQsVUFBVSwyQkFBVixFQUF1QyxxQkFBdkMsRUFBOEQscUJBQTlELEVBQXFGO0FBRTlIO0FBQ0E7QUFDQTtBQUNBLE1BQUksT0FBTyxHQUFHLENBQUMsMkJBQTJCLEdBQUcscUJBQS9CLElBQXNELEtBQUssU0FBekUsQ0FMOEgsQ0FNOUg7QUFDQTs7QUFDQSxNQUFJLEtBQUssR0FBRyxDQUFDLHFCQUFxQixHQUFHLE9BQXpCLElBQW9DLEtBQUssU0FBckQsQ0FSOEgsQ0FTOUg7O0FBQ0EsTUFBSSxVQUFVLEdBQUksS0FBSyxHQUFHLENBQVQsR0FBYyxDQUFkLEdBQWtCLENBQUMsQ0FBcEM7QUFDQSxFQUFBLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBTCxDQUFTLEtBQVQsQ0FBUixDQVg4SCxDQVk5SDs7QUFDQSxTQUFRLEtBQUssR0FBRyxVQUFoQjtBQUNQLENBZEQ7O0FBZ0JBLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLFNBQWpCOzs7OztBQzlPQTs7Ozs7Ozs7Ozs7O0FBYUEsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLGdCQUFELENBQWxCOztBQUNBLElBQUksWUFBWSxHQUFHLE9BQU8sQ0FBQyx3QkFBRCxDQUExQjs7QUFDQSxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMscUJBQUQsQ0FBdkI7O0FBQ0EsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLGFBQUQsQ0FBdkIsQyxDQUNBOzs7QUFFQSxTQUFTLFVBQVQsQ0FBb0IsT0FBcEIsRUFBNkI7QUFFekI7QUFDQTtBQUNBO0FBQ0EsRUFBQSxTQUFTLENBQUMsSUFBVixDQUFlLElBQWYsRUFBcUIsT0FBckIsRUFMeUIsQ0FLTTs7QUFFL0IsTUFBSSxJQUFKOztBQUNBLE9BQUssSUFBTCxHQUFZLFVBQVUsT0FBVixFQUFtQjtBQUUzQixTQUFLLE9BQUwsR0FBZSxPQUFPLENBQUMsTUFBdkI7QUFFQSxTQUFLLFFBQUwsR0FBZ0IsQ0FBaEI7QUFFQSxTQUFLLE1BQUwsR0FBYyxPQUFPLENBQUMsYUFBdEIsQ0FOMkIsQ0FPM0I7QUFDQTtBQUNBOztBQUVBLFNBQUssa0JBQUwsR0FBMEIsQ0FBQyxDQUEzQjtBQUVBLElBQUEsSUFBSSxHQUFHLElBQVA7QUFFQTs7OztBQUdBOzs7O0FBR0E7Ozs7OztBQUtBLElBQUEsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxnQkFBWixFQUE4QixVQUFVLEtBQVYsRUFBaUIsR0FBakIsRUFBdUI7QUFDakQsTUFBQSxJQUFJLENBQUMsd0JBQUwsQ0FBOEIsSUFBOUIsQ0FBbUMsSUFBbkMsRUFBeUMsR0FBekM7QUFDSCxLQUZEO0FBSUE7Ozs7QUFHQSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLG1CQUFmLEVBQW9DLFVBQVMsR0FBVCxFQUFjO0FBQzlDLFFBQUEsSUFBSSxDQUFDLFFBQUwsQ0FBYyxZQUFZLENBQUMsY0FBYixDQUE0QixHQUFHLENBQUMsU0FBaEMsQ0FBZDtBQUNILE9BRkQ7QUFHSCxLQXJDMEIsQ0F1QzNCOztBQUNBOzs7OztBQUdBLFFBQUcsS0FBSyxNQUFSLEVBQWU7QUFDWCxXQUFLLE1BQUwsQ0FBWSxFQUFaLENBQWUsZUFBZixFQUFnQyxVQUFTLEdBQVQsRUFBYztBQUMxQyxRQUFBLElBQUksQ0FBQyxrQkFBTCxDQUF3QixHQUF4QjtBQUNILE9BRkQ7QUFHSCxLQS9DMEIsQ0FpRDNCOztBQUNBOzs7OztBQUdBLFFBQUcsS0FBSyxNQUFSLEVBQWU7QUFDWCxXQUFLLE1BQUwsQ0FBWSxFQUFaLENBQWUsZUFBZixFQUFnQyxVQUFTLEdBQVQsRUFBYztBQUMxQyxRQUFBLElBQUksQ0FBQyxrQkFBTCxDQUF3QixHQUF4QjtBQUNILE9BRkQ7QUFHSCxLQXpEMEIsQ0EyRDNCOzs7QUFDQSxJQUFBLENBQUMsQ0FBQyxTQUFGLENBQVksYUFBWixFQUEyQixVQUFVLEtBQVYsRUFBaUIsR0FBakIsRUFBdUI7QUFDOUMsTUFBQSxJQUFJLENBQUMsV0FBTDtBQUNILEtBRkQsRUE1RDJCLENBZ0UzQjs7QUFFQSxTQUFLLE1BQUwsQ0FBWSxFQUFaLENBQWUsaUJBQWYsRUFBa0MsRUFBbEMsRUFBc0MsVUFBVSxDQUFWLEVBQWEsS0FBYixFQUFvQjtBQUN0RCxjQUFRLEtBQUssQ0FBQyxHQUFkO0FBQ0ksYUFBSyxRQUFMO0FBQ0ksVUFBQSxJQUFJLENBQUMsV0FBTDtBQUZSO0FBSUgsS0FMRDtBQVNILEdBM0VEOztBQTZFQSxPQUFLLElBQUwsQ0FBVSxPQUFWO0FBSUE7Ozs7Ozs7QUFNQTs7Ozs7O0FBTUE7Ozs7QUFHQSxPQUFLLG1CQUFMLEdBQTJCLFVBQVUsU0FBVixFQUFxQjtBQUM1QyxXQUFPLEtBQUssU0FBWjtBQUNILEdBRkQ7O0FBSUEsT0FBSyxZQUFMLEdBQW9CLFlBQVc7QUFDM0IsV0FBTyxLQUFLLFFBQUwsRUFBUDtBQUNILEdBRkQ7O0FBSUEsT0FBSyxrQkFBTCxHQUEwQixVQUFVLGVBQVYsRUFBNEI7QUFDbEQsU0FBSyxlQUFMLEdBQXVCLGVBQXZCO0FBQ0EsU0FBSyxNQUFMO0FBQ0gsR0FIRDs7QUFLQSxPQUFLLGtCQUFMLEdBQTBCLFlBQWE7QUFDbkMsV0FBTyxLQUFLLGVBQVo7QUFDSCxHQUZEOztBQUlBLE9BQUssUUFBTCxHQUFnQixVQUFVLEtBQVYsRUFBaUI7QUFDN0IsU0FBSyxNQUFMLENBQVksR0FBWixDQUFnQixLQUFoQjtBQUNBLFNBQUssTUFBTDtBQUNILEdBSEQ7QUFJQTs7Ozs7O0FBSUEsT0FBSyxTQUFMLEdBQWlCLFlBQVk7QUFDekIsV0FBTyxLQUFLLE1BQVo7QUFDSCxHQUZEO0FBR0E7Ozs7O0FBR0EsT0FBSyx3QkFBTCxHQUFnQyxZQUFZO0FBQ3hDLFdBQU8sS0FBSyxNQUFMLENBQVksYUFBWixFQUFQO0FBQ0gsR0FGRDtBQUdBOzs7OztBQUdBLE9BQUssUUFBTCxHQUFnQixVQUFVLEtBQVYsRUFBaUI7QUFDN0IsUUFBSSxLQUFLLEtBQUssU0FBVixJQUF3QixLQUFLLEtBQUssSUFBbEMsSUFBMEMsS0FBSyxJQUFJLENBQW5ELElBQXdELEtBQUssR0FBRyxLQUFLLE1BQUwsQ0FBWSxJQUFaLEVBQXBFLEVBQXVGO0FBQ25GLGFBQU8sS0FBSyxNQUFMLENBQVksR0FBWixDQUFnQixLQUFoQixDQUFQO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsTUFBQSxPQUFPLENBQUMsSUFBUixDQUFhLDhDQUE4QyxLQUE5QyxHQUF1RCxrQkFBdkQsR0FBNEUsS0FBSyxNQUFMLENBQVksSUFBWixFQUF6RjtBQUNBLGFBQU8sSUFBUDtBQUNIO0FBQ0osR0FQRDtBQVNBOzs7O0FBR0E7OztBQUtBO0FBQ0E7OztBQUNBLE9BQUssV0FBTCxHQUFtQixZQUFXO0FBQzFCLFFBQUksS0FBSyxrQkFBTCxLQUE0QixDQUFDLENBQWpDLEVBQW9DO0FBQ2hDLFVBQU0sVUFBVSxHQUFHLEtBQUssa0JBQXhCO0FBQ0EsV0FBSyxNQUFMLFdBQW1CLEtBQUssa0JBQXhCO0FBQ0EsV0FBSyxrQkFBTCxHQUEwQixDQUFDLENBQTNCO0FBRUEsV0FBSyxNQUFMO0FBRUEsV0FBSyxnQkFBTCxDQUFzQixVQUF0QjtBQUNIO0FBQ0osR0FWRDs7QUFZQSxPQUFLLGtCQUFMLEdBQTBCLFVBQVMsR0FBVCxFQUFjO0FBRXBDLFFBQU0sS0FBSyxHQUFHLEdBQUcsQ0FBQyxpQkFBbEIsQ0FGb0MsQ0FHcEM7O0FBQ0EsUUFBSSxLQUFLLEtBQUssU0FBVixJQUF3QixLQUFLLEtBQUssSUFBbEMsSUFBMEMsS0FBSyxJQUFJLENBQW5ELElBQXdELEtBQUssR0FBRyxLQUFLLE1BQUwsQ0FBWSxJQUFaLEVBQXBFLEVBQXVGO0FBQ25GLFdBQUssTUFBTCxXQUFtQixLQUFuQjtBQUVBLFdBQUssTUFBTDtBQUNILEtBSkQsTUFJTztBQUNILE1BQUEsT0FBTyxDQUFDLElBQVIsQ0FBYSxnREFBZ0QsS0FBaEQsR0FBeUQsa0JBQXpELEdBQThFLEtBQUssTUFBTCxDQUFZLElBQVosRUFBM0Y7QUFDSDtBQUNKLEdBWEQ7O0FBYUEsT0FBSyxXQUFMLEdBQW1CLFVBQVMsS0FBVCxFQUFnQixRQUFoQixFQUEwQjtBQUN6QztBQUNBLFFBQUksS0FBSyxLQUFLLFNBQVYsSUFBd0IsS0FBSyxLQUFLLElBQWxDLElBQTBDLEtBQUssSUFBSSxDQUFuRCxJQUF3RCxLQUFLLEdBQUcsS0FBSyxNQUFMLENBQVksSUFBWixFQUFwRSxFQUF1RjtBQUNuRixXQUFLLE1BQUwsQ0FBWSxhQUFaLENBQTBCLEtBQTFCLEVBQWlDLFFBQWpDO0FBRUEsV0FBSyxNQUFMO0FBQ0gsS0FKRCxNQUlPO0FBQ0gsTUFBQSxPQUFPLENBQUMsSUFBUixDQUFhLGdEQUFnRCxLQUFoRCxHQUF1RCxrQkFBdkQsR0FBNEUsS0FBSyxNQUFMLENBQVksSUFBWixFQUF6RjtBQUNIO0FBQ0osR0FURDs7QUFXQSxPQUFLLGtCQUFMLEdBQTBCLFVBQVMsR0FBVCxFQUFjO0FBQ3BDLFFBQU0sVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUF2QjtBQUVBLFNBQUssTUFBTCxDQUFZLGFBQVosQ0FBMEIsVUFBMUIsRUFBc0MsWUFBWSxDQUFDLGNBQWIsQ0FBNEIsR0FBRyxDQUFDLEtBQWhDLENBQXRDO0FBRUEsU0FBSyxNQUFMO0FBQ0gsR0FORDs7QUFRQSxPQUFLLGdCQUFMLEdBQXdCLFVBQVMsVUFBVCxFQUFxQjtBQUN6QyxTQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLGNBQWxCLEVBQWtDO0FBQzlCLDJCQUFzQjtBQURRLEtBQWxDO0FBR0gsR0FKRDs7QUFNQSxPQUFLLGdCQUFMLEdBQXdCLFVBQVMsVUFBVCxFQUFxQjtBQUN6QyxTQUFLLE9BQUwsQ0FBYSxJQUFiLENBQWtCLGNBQWxCLEVBQWtDO0FBQzlCLG9CQUFlLFVBRGU7QUFFOUIsZUFBUyxLQUFLLE1BQUwsQ0FBWSxHQUFaLENBQWdCLFVBQWhCO0FBRnFCLEtBQWxDO0FBSUgsR0FMRDtBQU9BOzs7Ozs7OztBQU9BOzs7Ozs7QUFJQSxPQUFLLHdCQUFMLEdBQWdDLFVBQVUsR0FBVixFQUFlO0FBQzNDLFFBQUksR0FBRyxDQUFDLGtCQUFKLElBQTBCLENBQTFCLElBQStCLEdBQUcsQ0FBQyxrQkFBSixHQUF5QixLQUFLLE1BQUwsQ0FBWSxJQUFaLEVBQTVELEVBQWdGO0FBQzVFLFdBQUssa0JBQUwsR0FBMEIsR0FBRyxDQUFDLGtCQUE5QjtBQUNBLFdBQUssTUFBTDtBQUNIO0FBQ0osR0FMRDtBQU9BOzs7OztBQUtBOzs7Ozs7Ozs7Ozs7QUFXQSxPQUFLLFdBQUwsR0FBbUIsWUFBVztBQUUxQixRQUFJLE9BQU8sR0FBRyxFQUFkO0FBQ0EsSUFBQSxPQUFPLENBQUMsQ0FBUixHQUFZLENBQUMsSUFBSSxLQUFLLGVBQUwsQ0FBcUIsQ0FBMUIsSUFBNkIsS0FBSyxRQUFMLEVBQXpDO0FBQ0EsSUFBQSxPQUFPLENBQUMsQ0FBUixHQUFZLENBQUMsSUFBSSxLQUFLLGVBQUwsQ0FBcUIsQ0FBMUIsSUFBNkIsS0FBSyxRQUFMLEVBQXpDO0FBRUEsUUFBSSxXQUFXLEdBQUcsRUFBbEI7QUFDQSxJQUFBLFdBQVcsQ0FBQyxDQUFaLEdBQWlCLEtBQUssTUFBTCxDQUFZLEtBQVosRUFBRCxHQUF1QixLQUFLLFFBQUwsRUFBdkM7QUFDQSxJQUFBLFdBQVcsQ0FBQyxDQUFaLEdBQWlCLEtBQUssTUFBTCxDQUFZLE1BQVosRUFBRCxHQUF3QixLQUFLLFFBQUwsRUFBeEM7QUFFQSxTQUFLLE9BQUwsQ0FBYSxTQUFiLENBQ1EsT0FBTyxDQUFDLENBRGhCLEVBQ21CLE9BQU8sQ0FBQyxDQUQzQixFQUVRLFdBQVcsQ0FBQyxDQUZwQixFQUV1QixXQUFXLENBQUMsQ0FGbkM7QUFJSCxHQWREOztBQWdCQSxPQUFLLE1BQUwsR0FBYyxZQUFZO0FBRXRCLFFBQUksR0FBRyxHQUFHLEtBQUssTUFBTCxDQUFZLElBQVosRUFBVjtBQUVBLFNBQUssV0FBTDs7QUFFQSxTQUFLLElBQUksQ0FBQyxHQUFHLENBQWIsRUFBZ0IsQ0FBQyxHQUFHLEdBQXBCLEVBQXlCLENBQUMsRUFBMUIsRUFBOEI7QUFDMUIsVUFBSSxLQUFLLEdBQUcsS0FBSyxNQUFMLENBQVksR0FBWixDQUFnQixDQUFoQixDQUFaOztBQUVBLFVBQUksQ0FBQyxLQUFLLEtBQUssa0JBQWYsRUFBbUM7QUFDL0IsUUFBQSxLQUFLLENBQUMsWUFBTixDQUFtQixLQUFLLE9BQXhCO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsUUFBQSxLQUFLLENBQUMsSUFBTixDQUFXLEtBQUssT0FBaEI7QUFDSDtBQUNKLEtBZHFCLENBZ0J0Qjs7QUFFSCxHQWxCRDtBQW9CQTs7Ozs7QUFHQSxXQUFTLGVBQVQsR0FBMkI7QUFDdkIsUUFBSSxlQUFlLEdBQUcsS0FBdEIsQ0FEdUIsQ0FFdkI7QUFDQTs7QUFDQSxTQUFLLElBQUksQ0FBQyxHQUFHLENBQWIsRUFBZ0IsQ0FBQyxHQUFHLGVBQXBCLEVBQXFDLENBQUMsRUFBdEMsRUFBMEM7QUFDdEMsTUFBQSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUwsQ0FBWSxJQUFJLENBQUMsTUFBTCxLQUFnQixJQUFqQixHQUF5QixDQUFwQyxDQUFMO0FBQ0EsTUFBQSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUwsQ0FBWSxJQUFJLENBQUMsTUFBTCxLQUFnQixJQUFqQixHQUF5QixDQUFwQyxDQUFMO0FBRUEsTUFBQSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUwsQ0FBWSxJQUFJLENBQUMsTUFBTCxLQUFnQixHQUFqQixHQUF3QixDQUFuQyxDQUFMO0FBQ0EsTUFBQSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUwsQ0FBWSxJQUFJLENBQUMsTUFBTCxLQUFnQixHQUFqQixHQUF3QixDQUFuQyxDQUFMO0FBRUEsVUFBSSxHQUFHLEdBQUcsSUFBSSxJQUFKLENBQVMsRUFBVCxFQUFhLEVBQWIsRUFBaUIsRUFBakIsRUFBcUIsRUFBckIsQ0FBVjtBQUNBLFdBQUssTUFBTCxDQUFZLEdBQVosQ0FBZ0IsR0FBaEI7QUFDSDtBQUNKO0FBRUo7O0FBRUQsVUFBVSxDQUFDLFNBQVgsR0FBdUIsTUFBTSxDQUFDLE1BQVAsQ0FBYyxTQUFTLENBQUMsU0FBeEIsRUFBbUM7QUFDdEQsRUFBQSxXQUFXLEVBQUU7QUFDVCxJQUFBLEtBQUssRUFBRTtBQURFO0FBRHlDLENBQW5DLENBQXZCO0FBTUEsTUFBTSxDQUFDLE9BQVAsR0FBaUIsVUFBakI7Ozs7O0FDN1VBOzs7OztBQUtBLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxvQkFBRCxDQUFwQjs7QUFDQSxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMsZ0JBQUQsQ0FBbEI7O0FBQ0EsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLGFBQUQsQ0FBdkI7O0FBRUEsU0FBUyxpQkFBVCxDQUE0QixPQUE1QixFQUFxQztBQUVqQztBQUVBO0FBQ0E7QUFDQSxFQUFBLFNBQVMsQ0FBQyxJQUFWLENBQWUsSUFBZixFQUFxQixPQUFyQixFQU5pQyxDQU1GOztBQUUvQjs7OztBQUdBLE9BQUssTUFBTCxHQUFjLFVBQVUsT0FBVixFQUFtQixHQUFuQixFQUF3QjtBQUNsQyxTQUFLLFdBQUw7QUFDQSxTQUFLLFlBQUwsQ0FBa0IsT0FBbEIsQ0FBMEIsVUFBUyxPQUFULEVBQWlCO0FBQ3ZDLE1BQUEsT0FBTyxDQUFDLElBQVIsQ0FBYSxLQUFLLE9BQWxCLEVBQTJCLElBQUUsS0FBSyxTQUFsQztBQUNILEtBRkQsRUFFRyxJQUZIO0FBR0gsR0FMRDtBQU9BOzs7OztBQUdBLE9BQUssZUFBTCxHQUF1QixVQUFVLEdBQVYsRUFBZSxDQUNsQztBQUNILEdBRkQ7QUFHQTs7Ozs7QUFHQSxPQUFLLGNBQUwsR0FBc0IsVUFBVSxHQUFWLEVBQWUsQ0FFcEMsQ0FGRDtBQUdBOzs7OztBQUdBLE9BQUssb0JBQUwsR0FBNEIsVUFBVSxHQUFWLEVBQWUsQ0FFMUMsQ0FGRDs7QUFJQSxPQUFLLFdBQUwsR0FBbUIsWUFBVztBQUMxQixTQUFLLE9BQUwsQ0FBYSxTQUFiLENBQ1EsQ0FEUixFQUNXLENBRFgsRUFFUSxLQUFLLFdBRmIsRUFFMEIsS0FBSyxZQUYvQjtBQUdILEdBSkQ7QUFNQTs7Ozs7O0FBSUEsT0FBSyxhQUFMLEdBQXFCLFVBQVMsV0FBVCxFQUFzQixZQUF0QixFQUFvQyxPQUFwQyxFQUE0QztBQUU3RCxTQUFLLFlBQUwsR0FBb0IsRUFBcEIsQ0FGNkQsQ0FFckM7QUFFeEI7O0FBQ0EsUUFBSSxNQUFNLEdBQUcsSUFBSSxJQUFKLENBQVMsQ0FBVCxFQUFZLE9BQVosRUFBcUIsV0FBckIsRUFBa0MsT0FBbEMsQ0FBYjtBQUNBLElBQUEsTUFBTSxDQUFDLGNBQVAsQ0FBc0IsS0FBSyxlQUEzQjtBQUNBLFNBQUssWUFBTCxDQUFrQixJQUFsQixDQUF1QixNQUF2QixFQVA2RCxDQU83Qjs7QUFFaEMsSUFBQSxNQUFNLEdBQUcsSUFBSSxJQUFKLENBQVMsV0FBVCxFQUFzQixDQUF0QixFQUF5QixXQUF6QixFQUFzQyxZQUF0QyxDQUFUO0FBQ0EsSUFBQSxNQUFNLENBQUMsY0FBUCxDQUFzQixLQUFLLGVBQTNCO0FBQ0EsU0FBSyxZQUFMLENBQWtCLElBQWxCLENBQXVCLE1BQXZCLEVBWDZELENBVzdCOztBQUVoQyxJQUFBLE1BQU0sR0FBRyxJQUFJLElBQUosQ0FBUyxXQUFULEVBQXNCLFlBQXRCLEVBQW9DLENBQXBDLEVBQXVDLFlBQXZDLENBQVQ7QUFDQSxJQUFBLE1BQU0sQ0FBQyxjQUFQLENBQXNCLEtBQUssZUFBM0I7QUFDQSxTQUFLLFlBQUwsQ0FBa0IsSUFBbEIsQ0FBdUIsTUFBdkIsRUFmNkQsQ0FlN0I7O0FBRWhDLElBQUEsTUFBTSxHQUFHLElBQUksSUFBSixDQUFTLE9BQVQsRUFBa0IsWUFBbEIsRUFBZ0MsT0FBaEMsRUFBeUMsQ0FBekMsQ0FBVDtBQUNBLElBQUEsTUFBTSxDQUFDLGNBQVAsQ0FBc0IsS0FBSyxlQUEzQjtBQUNBLFNBQUssWUFBTCxDQUFrQixJQUFsQixDQUF1QixNQUF2QixFQW5CNkQsQ0FtQjdCO0FBRW5DLEdBckJEO0FBdUJBOzs7Ozs7O0FBS0MsT0FBSyx3QkFBTCxHQUFnQyxZQUFVO0FBQ3ZDLFNBQUssV0FBTCxHQUFtQixLQUFLLE1BQUwsQ0FBWSxLQUFaLEtBQXNCLEtBQUssV0FBOUMsQ0FEdUMsQ0FDb0I7O0FBQzNELFNBQUssWUFBTCxHQUFvQixLQUFLLE1BQUwsQ0FBWSxNQUFaLEtBQXVCLEtBQUssWUFBaEQ7QUFDQSxTQUFLLGFBQUwsQ0FBbUIsS0FBSyxXQUF4QixFQUFxQyxLQUFLLFlBQTFDLEVBQXdELEtBQUssT0FBN0Q7QUFDSCxHQUpBO0FBS0Q7Ozs7O0FBR0EsT0FBSyx3QkFBTCxHQUFnQyxVQUFTLEdBQVQsRUFBYztBQUMxQyxTQUFLLFNBQUwsQ0FBZSx3QkFBZixDQUF3QyxJQUF4QyxDQUE2QyxJQUE3QyxFQUFtRCxHQUFuRCxFQUF3RCxJQUF4RDs7QUFDQSxTQUFLLHdCQUFMO0FBQ0EsU0FBSyxNQUFMLENBQVksR0FBWjtBQUNILEdBSkQ7O0FBS0EsT0FBSyxJQUFMLEdBQVksWUFBWTtBQUNwQjtBQUNBLFNBQUssT0FBTCxHQUFlLEVBQWY7QUFDQSxTQUFLLFdBQUwsR0FBbUIsQ0FBbkIsQ0FIb0IsQ0FHRTs7QUFDdEIsU0FBSyxZQUFMLEdBQW9CLENBQXBCO0FBRUEsU0FBSyxlQUFMLEdBQXVCLFNBQXZCO0FBRUEsU0FBSyxXQUFMLEdBQW1CLEtBQUssTUFBTCxDQUFZLEtBQVosS0FBc0IsS0FBSyxXQUE5QyxDQVJvQixDQVF1Qzs7QUFDM0QsU0FBSyxZQUFMLEdBQW9CLEtBQUssTUFBTCxDQUFZLE1BQVosS0FBdUIsS0FBSyxZQUFoRDtBQUVBLFNBQUssYUFBTCxDQUFtQixLQUFLLFdBQXhCLEVBQXFDLEtBQUssWUFBMUMsRUFBd0QsS0FBSyxPQUE3RDtBQUVBLFNBQUssTUFBTDtBQUNILEdBZEQ7O0FBZUEsT0FBSyxJQUFMO0FBQ0g7O0FBRUQsaUJBQWlCLENBQUMsU0FBbEIsR0FBOEIsTUFBTSxDQUFDLE1BQVAsQ0FBYyxTQUFTLENBQUMsU0FBeEIsRUFBbUM7QUFDN0QsRUFBQSxXQUFXLEVBQUU7QUFDVCxJQUFBLEtBQUssRUFBRTtBQURFO0FBRGdELENBQW5DLENBQTlCO0FBTUEsTUFBTSxDQUFDLE9BQVAsR0FBaUIsaUJBQWpCOzs7OztBQ3pIQTs7Ozs7Ozs7Ozs7OztBQWNBLElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxnQkFBRCxDQUFsQjs7QUFDQSxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsa0JBQUQsQ0FBcEI7O0FBQ0EsSUFBSSxRQUFRLEdBQUcsT0FBTyxDQUFDLG9CQUFELENBQXRCOztBQUNBLElBQUksWUFBWSxHQUFHLE9BQU8sQ0FBQyx3QkFBRCxDQUExQjs7QUFFQSxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMscUJBQUQsQ0FBdkI7O0FBRUEsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLG9CQUFELENBQXBCOztBQUVBLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxhQUFELENBQXZCOztBQUVBLFNBQVMsbUJBQVQsQ0FBOEIsT0FBOUIsRUFBdUM7QUFFbkMsRUFBQSxTQUFTLENBQUMsSUFBVixDQUFlLElBQWYsRUFBcUIsT0FBckIsRUFGbUMsQ0FFSjs7QUFFL0IsTUFBSSxJQUFKOztBQUNBLE9BQUssSUFBTCxHQUFZLFVBQVUsT0FBVixFQUFtQjtBQUUzQixJQUFBLElBQUksR0FBRyxJQUFQO0FBRUEsU0FBSyxPQUFMLEdBQWUsT0FBTyxDQUFDLE1BQXZCO0FBRUEsU0FBSyxrQkFBTCxHQUEwQixDQUExQixDQU4yQixDQU1FOztBQUU3QixTQUFLLFlBQUwsR0FBb0IsSUFBcEIsQ0FSMkIsQ0FRRDs7QUFFMUIsU0FBSyxPQUFMLEdBQWUsT0FBTyxDQUFDLE9BQXZCO0FBRUEsU0FBSyxNQUFMLEdBQWMsT0FBTyxDQUFDLE1BQXRCO0FBR0E7Ozs7QUFHQTs7O0FBSU47QUFDQTs7QUFDTSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLHdCQUFmLEVBQXlDLFVBQVMsR0FBVCxFQUFjLENBQ25EO0FBQ0gsT0FGRDtBQUdIO0FBRUQ7Ozs7O0FBR0EsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxzQkFBZixFQUF1QyxZQUFXO0FBQzlDLFFBQUEsSUFBSSxDQUFDLDRCQUFMLENBQWtDLElBQWxDLENBQXVDLElBQXZDO0FBQ0gsT0FGRDtBQUdIO0FBRUQ7Ozs7O0FBSUE7Ozs7OztBQUlBLFFBQUcsS0FBSyxNQUFSLEVBQWU7QUFDWCxXQUFLLE1BQUwsQ0FBWSxFQUFaLENBQWUsOEJBQWYsRUFBK0MsVUFBUyxHQUFULEVBQWM7QUFDekQsUUFBQSxJQUFJLENBQUMsZUFBTCxDQUFxQixZQUFZLENBQUMsY0FBYixDQUE0QixHQUE1QixDQUFyQjtBQUNBLFFBQUEsSUFBSSxDQUFDLE1BQUw7QUFDSCxPQUhEO0FBSUg7QUFFRDs7Ozs7QUFJQSxJQUFBLENBQUMsQ0FBQyxTQUFGLENBQVksYUFBWixFQUEyQixVQUFVLEtBQVYsRUFBaUIsR0FBakIsRUFBdUI7QUFDOUMsTUFBQSxJQUFJLENBQUMsY0FBTCxDQUFvQixDQUFwQjtBQUNILEtBRkQ7QUFJQTs7Ozs7QUFJQSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLG1CQUFmLEVBQW9DLFVBQVMsR0FBVCxFQUFjO0FBQ2hELFFBQUEsSUFBSSxDQUFDLFdBQUw7QUFDQSxRQUFBLElBQUksQ0FBQyxNQUFMO0FBQ0QsT0FIRDtBQUlIO0FBSUQ7Ozs7QUFHQTs7Ozs7QUFHQSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLFlBQWYsRUFBNkIsVUFBUyxHQUFULEVBQWM7QUFDdkMsUUFBQSxJQUFJLENBQUMsb0JBQUwsQ0FBMEIsSUFBMUIsQ0FBK0IsSUFBL0IsRUFBcUMsR0FBckM7QUFDSCxPQUZEO0FBR0g7QUFDSixHQXRGRDs7QUF3RkEsT0FBSyxJQUFMLENBQVUsT0FBVjtBQUVBOzs7Ozs7Ozs7QUFRQSxPQUFLLG9CQUFMLEdBQTRCLFVBQVUsR0FBVixFQUFlO0FBRXZDLFFBQUcsR0FBRyxDQUFDLFNBQUosS0FBa0IsSUFBckIsRUFBMEI7QUFDdEIsV0FBSyxrQkFBTCxDQUF3QixHQUFHLENBQUMsNkJBQUosQ0FBa0MsQ0FBMUQsRUFBNkQsR0FBRyxDQUFDLDZCQUFKLENBQWtDLENBQS9GLEVBRHNCLENBRXRCOztBQUNBLFdBQUssd0JBQUw7QUFDQSxXQUFLLE1BQUw7QUFDSDtBQUVKLEdBVEQ7QUFXQTs7Ozs7Ozs7QUFNQSxPQUFLLDRCQUFMLEdBQW9DLFlBQVk7QUFFbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ00sUUFBTSxLQUFLLEdBQUcsS0FBSyxlQUFMLEVBQWQsQ0FUNEMsQ0FVNUM7O0FBQ0EsUUFBSSxLQUFLLEtBQUssSUFBVixJQUFrQixLQUFLLENBQUMsT0FBTixFQUFsQixJQUFxQyxLQUFLLENBQUMsV0FBTixDQUFrQixJQUFsQixLQUEyQixPQUFwRSxFQUE2RTtBQUNsRixXQUFLLFdBQUw7O0FBQ1MsVUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNiLGFBQUssTUFBTCxDQUFZLElBQVosQ0FBaUIsbUJBQWpCLEVBQXNDO0FBQUMsdUJBQWEsS0FBSyxDQUFDLE1BQU47QUFBZCxTQUF0QztBQUNEOztBQUNELFdBQUssbUJBQUwsQ0FBeUI7QUFBQyxxQkFBYSxLQUFLLENBQUMsTUFBTjtBQUFkLE9BQXpCO0FBQ0g7QUFFSixHQW5CRDtBQW9CQTs7Ozs7Ozs7OztBQVFBLE9BQUssOEJBQUwsR0FBc0MsVUFBVSxHQUFWLEVBQWU7QUFHdkQsUUFBTSxLQUFLLEdBQUcsS0FBSyxlQUFMLEVBQWQ7O0FBRU0sUUFBSSxLQUFLLEtBQUssSUFBVixJQUFrQixLQUFLLENBQUMsT0FBTixFQUF0QixFQUF1QztBQUM1QyxXQUFLLFdBQUwsR0FENEMsQ0FFbkM7O0FBQ0EsVUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNiLGFBQUssTUFBTCxDQUFZLElBQVosQ0FBaUIsbUJBQWpCLEVBQXNDO0FBQUMsdUJBQWEsS0FBSyxDQUFDLE1BQU47QUFBZCxTQUF0QztBQUNEOztBQUNELFdBQUssbUJBQUwsQ0FBeUI7QUFBQyxxQkFBYSxLQUFLLENBQUMsTUFBTjtBQUFkLE9BQXpCO0FBQ0gsS0FQRCxNQU9PO0FBRVosVUFBSSxHQUFHLEtBQUssSUFBWixFQUFpQjtBQUNoQjtBQUNBLE9BSlcsQ0FLWjs7O0FBQ0EsTUFBQSxtQkFBbUIsQ0FBQyxTQUFwQixDQUE4Qiw4QkFBOUIsQ0FBNkQsSUFBN0QsQ0FBa0UsSUFBbEUsRUFBd0UsR0FBeEUsRUFOWSxDQVFaOztBQUNBLFVBQUcsR0FBRyxDQUFDLFNBQUosS0FBa0IsSUFBckIsRUFBMEI7QUFDekIsWUFBTSxXQUFXLEdBQUc7QUFDbkIscUJBQVksR0FBRyxDQUFDLDZCQUFKLENBQWtDLENBRDNCO0FBRW5CLHFCQUFZLEdBQUcsQ0FBQyw2QkFBSixDQUFrQztBQUYzQixTQUFwQjtBQUlBLGFBQUssZUFBTCxDQUFxQixZQUFZLENBQUMsVUFBYixDQUF3QixHQUFHLENBQUMsa0JBQTVCLEVBQWdELFdBQWhELENBQXJCO0FBQ0EsYUFBSyx3QkFBTDtBQUNBLGFBQUssTUFBTDtBQUNBO0FBQ0Q7QUFDRSxHQS9CRDtBQWlDQTs7Ozs7O0FBSUEsT0FBSyxjQUFMLEdBQXNCLFVBQVUsV0FBVixFQUF1QjtBQUN6QyxTQUFLLGtCQUFMLEdBQTBCLFdBQTFCO0FBQ0gsR0FGRDs7QUFJQSxPQUFLLGNBQUwsR0FBc0IsWUFBVztBQUM3QixXQUFPLEtBQUssa0JBQVo7QUFDSCxHQUZEO0FBR0E7Ozs7O0FBR0EsT0FBSyxXQUFMLEdBQW1CLFlBQVk7QUFDM0IsU0FBSyxlQUFMLENBQXFCLElBQXJCO0FBQ0EsU0FBSyxNQUFMO0FBQ0gsR0FIRDtBQUlBOzs7OztBQUdBLE9BQUssZUFBTCxHQUF1QixZQUFZO0FBQy9CLFdBQU8sS0FBSyxZQUFMLElBQXFCLElBQTVCO0FBQ0gsR0FGRDs7QUFJQSxPQUFLLHdCQUFMLEdBQWdDLFlBQVc7QUFDdkMsUUFBRyxLQUFLLGVBQUwsT0FBMkIsSUFBOUIsRUFBbUM7QUFDL0IsV0FBSyxPQUFMLENBQWEsSUFBYixDQUFrQixrQ0FBbEIsRUFBc0QsS0FBSyxlQUFMLEdBQXVCLE1BQXZCLEVBQXREO0FBQ0g7QUFDSixHQUpEOztBQU1BLE9BQUssbUJBQUwsR0FBMkIsVUFBUyxLQUFULEVBQWdCO0FBQ3ZDLFNBQUssT0FBTCxDQUFhLElBQWIsQ0FBa0IsY0FBbEIsRUFBa0MsS0FBbEM7QUFDSCxHQUZEOztBQU1BLE9BQUssa0JBQUwsR0FBMEIsVUFBVSxLQUFWLEVBQWlCLEtBQWpCLEVBQXdCO0FBQzlDLFFBQUcsS0FBSyxlQUFMLE9BQTJCLElBQTlCLEVBQW1DO0FBQy9CLFdBQUssZUFBTCxHQUF1QixNQUF2QixDQUE4QixLQUE5QixFQUFxQyxLQUFyQztBQUNIO0FBQ0osR0FKRDs7QUFNQSxPQUFLLGVBQUwsR0FBdUIsVUFBVSxLQUFWLEVBQWlCO0FBQ3BDLFNBQUssWUFBTCxHQUFvQixLQUFwQjtBQUNILEdBRkQ7O0FBSUEsT0FBSyxXQUFMLEdBQW1CLFlBQVc7QUFFMUIsUUFBSSxPQUFPLEdBQUcsRUFBZDtBQUNBLElBQUEsT0FBTyxDQUFDLENBQVIsR0FBWSxDQUFDLElBQUksS0FBSyxlQUFMLENBQXFCLENBQTFCLElBQTZCLEtBQUssU0FBOUM7QUFDQSxJQUFBLE9BQU8sQ0FBQyxDQUFSLEdBQVksQ0FBQyxJQUFJLEtBQUssZUFBTCxDQUFxQixDQUExQixJQUE2QixLQUFLLFNBQTlDO0FBRUEsUUFBSSxXQUFXLEdBQUcsRUFBbEI7QUFDQSxJQUFBLFdBQVcsQ0FBQyxDQUFaLEdBQWlCLEtBQUssTUFBTCxDQUFZLEtBQVosRUFBRCxHQUF1QixLQUFLLFNBQTVDO0FBQ0EsSUFBQSxXQUFXLENBQUMsQ0FBWixHQUFpQixLQUFLLE1BQUwsQ0FBWSxNQUFaLEVBQUQsR0FBd0IsS0FBSyxTQUE3QztBQUVBLFNBQUssT0FBTCxDQUFhLFNBQWIsQ0FDUSxPQUFPLENBQUMsQ0FEaEIsRUFDbUIsT0FBTyxDQUFDLENBRDNCLEVBRVEsV0FBVyxDQUFDLENBRnBCLEVBRXVCLFdBQVcsQ0FBQyxDQUZuQztBQUdILEdBYkQ7QUFlQTs7Ozs7O0FBSUEsT0FBSyxNQUFMLEdBQWMsWUFBWTtBQUN0QixTQUFLLFdBQUw7O0FBQ0EsUUFBRyxLQUFLLGVBQUwsT0FBMkIsSUFBOUIsRUFBbUM7QUFDL0IsV0FBSyxlQUFMLEdBQXVCLElBQXZCLENBQTRCLEtBQUssT0FBakM7QUFDSDtBQUNKLEdBTEQ7QUFNSDs7QUFFRCxtQkFBbUIsQ0FBQyxTQUFwQixHQUFnQyxNQUFNLENBQUMsTUFBUCxDQUFjLFNBQVMsQ0FBQyxTQUF4QixFQUFtQztBQUMvRCxFQUFBLFdBQVcsRUFBRTtBQUNULElBQUEsS0FBSyxFQUFFO0FBREU7QUFEa0QsQ0FBbkMsQ0FBaEM7QUFNQSxNQUFNLENBQUMsT0FBUCxHQUFpQixtQkFBakI7Ozs7O0FDOVJBOzs7Ozs7OztBQVNBLFNBQVMsaUJBQVQsQ0FBNEIsT0FBNUIsRUFBcUM7QUFDakMsTUFBSSxJQUFKOztBQUNBLE9BQUssSUFBTCxHQUFZLFVBQVUsT0FBVixFQUFtQjtBQUM3QixJQUFBLElBQUksR0FBRyxJQUFQO0FBRUEsU0FBSyxPQUFMLEdBQWUsS0FBZjtBQUNBLFNBQUssa0JBQUwsR0FBMEIsQ0FBMUI7QUFDQSxTQUFLLE9BQUwsR0FBZSxLQUFmO0FBRUEsU0FBSyxNQUFMLEdBQWMsT0FBTyxDQUFDLE1BQXRCO0FBRUE7Ozs7QUFHQSxRQUFHLEtBQUssTUFBUixFQUFlO0FBQ1gsV0FBSyxNQUFMLENBQVksRUFBWixDQUFlLHNCQUFmLEVBQXVDLFVBQVMsR0FBVCxFQUFjO0FBQ2pELFFBQUEsSUFBSSxDQUFDLDRCQUFMLENBQWtDLElBQWxDLENBQXVDLElBQXZDO0FBQ0gsT0FGRDtBQUdIO0FBRUQ7Ozs7QUFHQTs7Ozs7Ozs7O0FBT0EsUUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNiLFdBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSx3QkFBZixFQUF5QyxZQUFXO0FBQ2xELFlBQUksSUFBSSxDQUFDLGNBQUwsRUFBSixFQUEyQjtBQUN6QixVQUFBLElBQUksQ0FBQyxVQUFMLENBQWdCLElBQWhCO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsVUFBQSxJQUFJLENBQUMsVUFBTCxDQUFnQixJQUFoQjtBQUNEO0FBQ0YsT0FORDtBQU9ELEtBcEM0QixDQXNDN0I7QUFDQTs7QUFDQTs7Ozs7OztBQUtBLFFBQUcsS0FBSyxNQUFSLEVBQWU7QUFDYixXQUFLLE1BQUwsQ0FBWSxFQUFaLENBQWUsOEJBQWYsRUFBK0MsWUFBVztBQUN4RCxRQUFBLElBQUksQ0FBQyxVQUFMLENBQWdCLEtBQWhCO0FBQ0EsUUFBQSxJQUFJLENBQUMsVUFBTCxDQUFnQixLQUFoQjtBQUNELE9BSEQ7QUFJRDs7QUFFRCxJQUFBLENBQUMsQ0FBQyxTQUFGLENBQVksbUJBQVosRUFBaUMsVUFBVSxLQUFWLEVBQWlCLEdBQWpCLEVBQXVCO0FBQ3RELE1BQUEsSUFBSSxDQUFDLDRCQUFMLENBQWtDLEdBQWxDO0FBQ0QsS0FGRDtBQUlBLElBQUEsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxhQUFaLEVBQTJCLFVBQVUsS0FBVixFQUFpQixHQUFqQixFQUF1QjtBQUNoRCxNQUFBLElBQUksQ0FBQyxzQkFBTDtBQUNELEtBRkQ7QUFJRixHQTVEQTs7QUE4REQsT0FBSyxJQUFMLENBQVUsT0FBVjtBQUNBOzs7OztBQUlBLE9BQUssNEJBQUwsR0FBb0MsWUFBVTtBQUMzQztBQUNBLFNBQUssVUFBTCxDQUFnQixLQUFoQjtBQUNGLEdBSEQ7QUFLQzs7Ozs7Ozs7QUFNQSxPQUFLLDRCQUFMLEdBQW9DLFVBQVUsR0FBVixFQUFlO0FBQy9DO0FBQ0EsU0FBSyxjQUFMLENBQW9CLEdBQUcsQ0FBQyxHQUF4QjtBQUNILEdBSEQ7QUFJQTs7Ozs7Ozs7QUFNQSxPQUFLLHNCQUFMLEdBQThCLFVBQVUsR0FBVixFQUFlO0FBQ3pDLFNBQUssY0FBTCxDQUFvQixLQUFwQjtBQUNILEdBRkQ7O0FBSUEsT0FBSyxVQUFMLEdBQWtCLFVBQVUsU0FBVixFQUFxQjtBQUNuQyxTQUFLLE9BQUwsR0FBZSxTQUFmO0FBQ0gsR0FGRDs7QUFHQSxPQUFLLFNBQUwsR0FBaUIsWUFBVTtBQUN2QixXQUFPLEtBQUssT0FBWjtBQUNILEdBRkQ7O0FBSUEsT0FBSyxVQUFMLEdBQWtCLFVBQVUsU0FBVixFQUFxQjtBQUNuQyxTQUFLLE9BQUwsR0FBZSxTQUFmO0FBQ0gsR0FGRDs7QUFHQSxPQUFLLFNBQUwsR0FBaUIsWUFBVTtBQUN2QixXQUFPLEtBQUssT0FBWjtBQUNILEdBRkQ7QUFJQTs7Ozs7Ozs7QUFNQSxPQUFLLGNBQUwsR0FBcUIsVUFBVSxXQUFWLEVBQXVCO0FBQ3hDLFNBQUssa0JBQUwsR0FBMEIsV0FBMUI7QUFDSCxHQUZEOztBQUdBLE9BQUssY0FBTCxHQUFzQixZQUFVO0FBQzVCLFdBQU8sS0FBSyxrQkFBWjtBQUNILEdBRkQ7QUFJSDs7QUFFRCxNQUFNLENBQUMsT0FBUCxHQUFpQixpQkFBakI7OztBQ3BJQTs7Ozs7Ozs7SUFFTSxtQjs7O0FBRUwsaUNBQWM7QUFBQTs7QUFFYixRQUFNLElBQUksR0FBRyxJQUFiO0FBRUcsSUFBQSxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QixNQUE1QixDQUNDLFlBQVc7QUFDVixNQUFBLENBQUMsQ0FBQyxPQUFGLENBQVUsbUJBQVYsRUFBK0I7QUFBRSxlQUFRO0FBQVYsT0FBL0I7QUFDQSxLQUhGO0FBTUgsSUFBQSxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQixNQUFyQixDQUNJLFlBQVc7QUFDWDtBQUNBLE1BQUEsQ0FBQyxDQUFDLE9BQUYsQ0FBVSxtQkFBVixFQUErQjtBQUFFLGVBQVE7QUFBVixPQUEvQjtBQUNGLEtBSkY7QUFPQSxJQUFBLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCLE1BQXZCLENBQ0ksWUFBVztBQUNYO0FBQ0EsTUFBQSxDQUFDLENBQUMsT0FBRixDQUFVLG1CQUFWLEVBQStCO0FBQUUsZUFBUTtBQUFWLE9BQS9CO0FBQ0YsS0FKRjtBQU9FLElBQUEsQ0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0IsTUFBcEIsQ0FDSSxZQUFXO0FBQ1Q7QUFDQSxNQUFBLENBQUMsQ0FBQyxPQUFGLENBQVUsbUJBQVYsRUFBK0I7QUFBRSxlQUFRO0FBQVYsT0FBL0I7QUFDSCxLQUpIO0FBT0YsSUFBQSxDQUFDLENBQUMsdUJBQUQsQ0FBRCxDQUEyQixNQUEzQixDQUNJLFlBQVc7QUFDWDtBQUNBLE1BQUEsQ0FBQyxDQUFDLE9BQUYsQ0FBVSxhQUFWLEVBQXlCO0FBQUMsZUFBUTtBQUFULE9BQXpCO0FBQ0YsS0FKRjtBQVFFLElBQUEsQ0FBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkIsS0FBM0IsQ0FDSSxZQUFXO0FBQ1QsTUFBQSxDQUFDLENBQUMsT0FBRixDQUFVLGFBQVYsRUFBeUIsRUFBekI7QUFFSCxLQUpIO0FBT0YsSUFBQSxDQUFDLENBQUMsU0FBRixDQUFZLG1CQUFaLEVBQ0MsVUFBVSxLQUFWLEVBQWlCLEdBQWpCLEVBQXVCO0FBQ3RCLE1BQUEsSUFBSSxDQUFDLDRCQUFMLENBQWtDLEdBQWxDO0FBQ00sS0FIUjtBQUtNLElBQUEsQ0FBQyxDQUFDLFNBQUYsQ0FBWSxhQUFaLEVBQ0wsVUFBVSxLQUFWLEVBQWlCLEdBQWpCLEVBQXVCO0FBQ3RCLE1BQUEsSUFBSSxDQUFDLHNCQUFMO0FBQ00sS0FIRixFQW5ETyxDQXdEWjtBQUVEO0FBRUQ7Ozs7Ozs7aURBR2lDLEcsRUFBSztBQUNsQyxjQUFPLEdBQUcsQ0FBQyxHQUFYO0FBQ0MsYUFBSyxjQUFMO0FBQ0MsVUFBQSxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QixNQUE1QixHQUFxQyxRQUFyQyxDQUE4QyxRQUE5QyxFQUF3RCxRQUF4RCxHQUFtRSxXQUFuRSxDQUErRSxRQUEvRTtBQUNEOztBQUNBLGFBQUssUUFBTDtBQUNDLFVBQUEsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUIsTUFBckIsR0FBOEIsUUFBOUIsQ0FBdUMsUUFBdkMsRUFBaUQsUUFBakQsR0FBNEQsV0FBNUQsQ0FBd0UsUUFBeEU7QUFDRDs7QUFDQSxhQUFLLFVBQUw7QUFDQyxVQUFBLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCLE1BQXZCLEdBQWdDLFFBQWhDLENBQXlDLFFBQXpDLEVBQW1ELFFBQW5ELEdBQThELFdBQTlELENBQTBFLFFBQTFFO0FBQ0Q7O0FBQ0EsYUFBSyxPQUFMO0FBQ0MsVUFBQSxDQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQixNQUFwQixHQUE2QixRQUE3QixDQUFzQyxRQUF0QyxFQUFnRCxRQUFoRCxHQUEyRCxXQUEzRCxDQUF1RSxRQUF2RTtBQUNEOztBQUNFLGFBQUssVUFBTDtBQUNFLFVBQUEsQ0FBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkIsTUFBM0IsR0FBb0MsUUFBcEMsQ0FBNkMsUUFBN0MsRUFBdUQsUUFBdkQsR0FBa0UsV0FBbEUsQ0FBOEUsUUFBOUU7QUFDRjs7QUFFRixnQkFqQkQsQ0FpQlU7O0FBakJWO0FBbUJBOzs7O0FBQ0Q7Ozs2Q0FHMEI7QUFDekIsTUFBQSxDQUFDLENBQUMsdUJBQUQsQ0FBRCxDQUEyQixNQUEzQixHQUFvQyxRQUFwQyxDQUE2QyxRQUE3QyxFQUF1RCxRQUF2RCxHQUFrRSxXQUFsRSxDQUE4RSxRQUE5RTtBQUNBOzs7Ozs7QUFHTCxNQUFNLENBQUMsT0FBUCxHQUFpQixtQkFBakI7Ozs7O0FDaEdBOzs7OztBQUtBLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxvQkFBRCxDQUFwQjs7QUFDQSxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMsZ0JBQUQsQ0FBbEI7O0FBQ0EsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLGFBQUQsQ0FBdkI7O0FBRUEsU0FBUyxTQUFULENBQW9CLE9BQXBCLEVBQTZCO0FBRXpCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBQSxTQUFTLENBQUMsSUFBVixDQUFlLElBQWYsRUFBcUIsT0FBckIsRUFOeUIsQ0FNTTs7QUFFL0IsTUFBSSxJQUFKOztBQUNBLE9BQUssSUFBTCxHQUFZLFVBQVUsT0FBVixFQUFtQjtBQUUzQixJQUFBLElBQUksR0FBRyxJQUFQO0FBRUEsU0FBSyxNQUFMLEdBQWMsRUFBZDtBQUVBLFNBQUssU0FBTCxHQUFpQixFQUFqQixDQU4yQixDQU1OOztBQUVyQixTQUFLLFdBQUwsR0FBbUIsSUFBbkI7QUFFQSxTQUFLLGFBQUwsR0FBcUIsQ0FBckI7QUFFQSxTQUFLLGFBQUwsR0FBcUIsU0FBckI7QUFDQSxTQUFLLG1CQUFMLEdBQTJCLFNBQTNCO0FBRUEsU0FBSyxNQUFMLEdBQWMsT0FBTyxDQUFDLE1BQXRCO0FBRUEsU0FBSyxPQUFMLEdBQWUsT0FBTyxDQUFDLE9BQXZCO0FBRUEsSUFBQSxNQUFNLENBQUMsU0FBUCxDQUFpQixzQkFBakIsRUFBeUMsWUFBVztBQUNoRCxNQUFBLElBQUksQ0FBQyxvQkFBTDtBQUNILEtBRkQ7QUFJQSxTQUFLLE1BQUwsR0FBYyxPQUFPLENBQUMsTUFBdEI7QUFFQTs7OztBQUlBLFNBQUssUUFBTDtBQUNBLFNBQUssTUFBTDtBQUNILEdBL0JEOztBQWlDQSxPQUFLLFFBQUwsR0FBZ0IsWUFBWTtBQUN4QixRQUFNLFNBQVMsR0FBRyxHQUFsQjtBQUNBLFFBQU0sS0FBSyxHQUFHLEtBQUssTUFBTCxDQUFZLEtBQVosRUFBZDtBQUNBLFFBQU0sTUFBTSxHQUFHLEtBQUssTUFBTCxDQUFZLE1BQVosRUFBZixDQUh3QixDQUl4Qjs7QUFDQSxTQUFLLFNBQUwsR0FBaUIsRUFBakIsQ0FMd0IsQ0FNeEI7QUFDQTtBQUNBOztBQUNBLFNBQUksSUFBSSxDQUFDLEdBQUcsSUFBSSxLQUFLLEdBQUMsR0FBdEIsRUFBMkIsQ0FBQyxHQUFHLEtBQUssR0FBRSxHQUF0QyxFQUEyQyxDQUFDLEdBQUcsQ0FBQyxHQUFDLFNBQWpELEVBQTJEO0FBQ3ZELFVBQUksS0FBSyxHQUFHLElBQUksSUFBSixDQUFTLENBQVQsRUFBWSxJQUFJLE1BQU0sR0FBQyxHQUF2QixFQUE0QixDQUE1QixFQUErQixNQUFNLEdBQUMsR0FBdEMsQ0FBWjtBQUNBLE1BQUEsS0FBSyxDQUFDLGNBQU4sQ0FBcUIsS0FBSyxhQUExQjs7QUFDQSxVQUFHLENBQUMsS0FBSyxDQUFULEVBQVc7QUFDUCxRQUFBLEtBQUssQ0FBQyxjQUFOLENBQXFCLEtBQUssbUJBQTFCO0FBQ0g7O0FBQ0QsV0FBSyxTQUFMLENBQWUsSUFBZixDQUFvQixLQUFwQjtBQUNIOztBQUVELFNBQUksSUFBSSxDQUFDLEdBQUcsSUFBSSxNQUFNLEdBQUMsR0FBdkIsRUFBNEIsQ0FBQyxHQUFHLE1BQU0sR0FBQyxHQUF2QyxFQUE0QyxDQUFDLEdBQUcsQ0FBQyxHQUFDLFNBQWxELEVBQTREO0FBQ3hELFVBQUksS0FBSyxHQUFHLElBQUksSUFBSixDQUFTLElBQUksS0FBSyxHQUFDLEdBQW5CLEVBQXdCLENBQXhCLEVBQTJCLEtBQUssR0FBQyxHQUFqQyxFQUFzQyxDQUF0QyxDQUFaO0FBQ0EsTUFBQSxLQUFLLENBQUMsY0FBTixDQUFxQixLQUFLLGFBQTFCOztBQUNBLFVBQUcsQ0FBQyxLQUFLLENBQVQsRUFBVztBQUNQLFFBQUEsS0FBSyxDQUFDLGNBQU4sQ0FBcUIsS0FBSyxtQkFBMUI7QUFDSDs7QUFDRCxXQUFLLFNBQUwsQ0FBZSxJQUFmLENBQW9CLEtBQXBCO0FBQ0g7QUFDSixHQTFCRDs7QUE0QkEsT0FBSyxvQkFBTCxHQUE0QixZQUFXO0FBQ25DLFNBQUssV0FBTCxHQUFtQixDQUFDLEtBQUssV0FBekI7QUFDQSxTQUFLLE1BQUw7QUFDSCxHQUhEOztBQUtBLE9BQUssV0FBTCxHQUFtQixZQUFXO0FBQzFCLFFBQUksT0FBTyxHQUFHLEVBQWQ7QUFDQSxJQUFBLE9BQU8sQ0FBQyxDQUFSLEdBQVksQ0FBQyxJQUFJLEtBQUssZUFBTCxDQUFxQixDQUExQixJQUE2QixLQUFLLFNBQTlDO0FBQ0EsSUFBQSxPQUFPLENBQUMsQ0FBUixHQUFZLENBQUMsSUFBSSxLQUFLLGVBQUwsQ0FBcUIsQ0FBMUIsSUFBNkIsS0FBSyxTQUE5QztBQUVBLFFBQUksV0FBVyxHQUFHLEVBQWxCO0FBQ0EsSUFBQSxXQUFXLENBQUMsQ0FBWixHQUFpQixLQUFLLE1BQUwsQ0FBWSxLQUFaLEVBQUQsR0FBdUIsS0FBSyxTQUE1QztBQUNBLElBQUEsV0FBVyxDQUFDLENBQVosR0FBaUIsS0FBSyxNQUFMLENBQVksTUFBWixFQUFELEdBQXdCLEtBQUssU0FBN0M7QUFFQSxTQUFLLE9BQUwsQ0FBYSxTQUFiLENBQ1EsT0FBTyxDQUFDLENBRGhCLEVBQ21CLE9BQU8sQ0FBQyxDQUQzQixFQUVRLFdBQVcsQ0FBQyxDQUZwQixFQUV1QixXQUFXLENBQUMsQ0FGbkM7QUFHSCxHQVpEOztBQWNBLE9BQUssTUFBTCxHQUFjLFlBQVk7QUFDdEIsU0FBSyxXQUFMOztBQUVBLFFBQUksS0FBSyxXQUFULEVBQXVCO0FBQ25CLFVBQUksSUFBSSxHQUFHLEtBQUssU0FBTCxDQUFlLE1BQTFCOztBQUNBLFdBQUssSUFBSSxDQUFDLEdBQUcsQ0FBYixFQUFnQixDQUFDLEdBQUcsSUFBcEIsRUFBMEIsQ0FBQyxFQUEzQixFQUErQjtBQUMzQixZQUFJLEtBQUssR0FBRyxLQUFLLFNBQUwsQ0FBZSxDQUFmLENBQVo7QUFDQSxRQUFBLEtBQUssQ0FBQyxJQUFOLENBQVcsS0FBSyxPQUFoQixFQUF5QixLQUFLLGFBQUwsR0FBbUIsS0FBSyxTQUFqRDtBQUNIO0FBQ0o7QUFDSixHQVZEOztBQVlBLE9BQUssSUFBTCxDQUFVLE9BQVY7QUFDSDs7QUFFRCxTQUFTLENBQUMsU0FBVixHQUFzQixNQUFNLENBQUMsTUFBUCxDQUFjLFNBQVMsQ0FBQyxTQUF4QixFQUFtQztBQUNyRCxFQUFBLFdBQVcsRUFBRTtBQUNULElBQUEsS0FBSyxFQUFFO0FBREU7QUFEd0MsQ0FBbkMsQ0FBdEI7QUFNQSxNQUFNLENBQUMsT0FBUCxHQUFpQixTQUFqQjs7Ozs7QUN2SEE7Ozs7O0FBTUEsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLGFBQUQsQ0FBdkI7O0FBRUEsU0FBUyxZQUFULENBQXNCLE9BQXRCLEVBQStCO0FBRTNCLEVBQUEsU0FBUyxDQUFDLElBQVYsQ0FBZSxJQUFmLEVBQXFCLE9BQXJCLEVBRjJCLENBRUk7O0FBRS9CLE1BQUksSUFBSjs7QUFDQSxPQUFLLElBQUwsR0FBWSxZQUFZO0FBRXBCLElBQUEsSUFBSSxHQUFHLElBQVA7QUFFSCxHQUpEOztBQU1BLE9BQUssSUFBTDs7QUFFQSxPQUFLLGtCQUFMLEdBQTBCLFlBQWE7QUFDbkMsV0FBTyxLQUFLLGVBQVo7QUFDSCxHQUZEOztBQUlBLE9BQUssTUFBTCxHQUFjLFlBQWEsQ0FDdkI7QUFDSCxHQUZEOztBQUlBLE9BQUssa0JBQUwsR0FBMEIsWUFBWTtBQUNsQyxXQUFPLEtBQUssZUFBWjtBQUNILEdBRkQ7QUFJSDs7QUFFRCxZQUFZLENBQUMsU0FBYixHQUF5QixNQUFNLENBQUMsTUFBUCxDQUFjLFNBQVMsQ0FBQyxTQUF4QixFQUFtQztBQUN4RCxFQUFBLFdBQVcsRUFBRTtBQUNULElBQUEsS0FBSyxFQUFFO0FBREU7QUFEMkMsQ0FBbkMsQ0FBekI7QUFNQSxNQUFNLENBQUMsT0FBUCxHQUFpQixZQUFqQjs7Ozs7QUN6Q0EsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLG1CQUFELENBQWxCOztBQUNBLElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyxjQUFELENBQXhCO0FBRUE7Ozs7OztBQUlBLFNBQVMseUJBQVQsQ0FBbUMsT0FBbkMsRUFBNEM7QUFFeEMsRUFBQSxVQUFVLENBQUMsSUFBWCxDQUFnQixJQUFoQixFQUFzQixPQUF0QixFQUZ3QyxDQUVSOztBQUVoQyxPQUFLLElBQUwsR0FBWSxZQUFZLENBQ3ZCLENBREQ7O0FBR0EsT0FBSyxJQUFMO0FBRUE7Ozs7QUFHQSxPQUFLLE1BQUwsR0FBYyxVQUFVLE9BQVYsRUFBbUIsR0FBbkIsRUFBd0I7QUFDbEMsU0FBSyxXQUFMLEdBRGtDLENBRWxDOztBQUNBLFFBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxxQkFBZCxFQUFvQztBQUNoQyxVQUFNLENBQUMsR0FBRyxHQUFHLENBQUMscUJBQUosQ0FBMEIsQ0FBcEM7QUFDQSxVQUFJLEtBQUssR0FBRyxJQUFJLElBQUosQ0FBUyxDQUFULEVBQVksQ0FBWixFQUFlLEtBQUssV0FBcEIsRUFBaUMsQ0FBakMsQ0FBWjtBQUNBLE1BQUEsS0FBSyxDQUFDLElBQU4sQ0FBVyxLQUFLLE9BQWhCO0FBQ0g7QUFDSixHQVJEO0FBVUE7Ozs7O0FBR0EsT0FBSyxlQUFMLEdBQXVCLFVBQVUsR0FBVixFQUFlLENBQ2xDO0FBQ0gsR0FGRDtBQUdBOzs7OztBQUdBLE9BQUssY0FBTCxHQUFzQixVQUFVLEdBQVYsRUFBZSxDQUVwQyxDQUZEO0FBR0E7Ozs7O0FBR0EsT0FBSyxvQkFBTCxHQUE0QixVQUFVLEdBQVYsRUFBZSxDQUUxQyxDQUZEO0FBTUgsQyxDQUNEO0FBQ0E7QUFDQTs7O0FBQ0EseUJBQXlCLENBQUMsU0FBMUIsR0FBc0MsTUFBTSxDQUFDLE1BQVAsQ0FBYyxVQUFVLENBQUMsU0FBekIsRUFBb0M7QUFDdEUsRUFBQSxXQUFXLEVBQUU7QUFDVCxJQUFBLEtBQUssRUFBRTtBQURFO0FBRHlELENBQXBDLENBQXRDO0FBTUEsTUFBTSxDQUFDLE9BQVAsR0FBaUIseUJBQWpCOzs7OztBQzVEQSxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsdUJBQUQsQ0FBcEI7O0FBQ0EsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLGNBQUQsQ0FBdkI7QUFDQTs7Ozs7QUFHQSxTQUFTLFVBQVQsQ0FBb0IsT0FBcEIsRUFBNkI7QUFFekI7QUFDQTtBQUNBO0FBQ0EsRUFBQSxTQUFTLENBQUMsSUFBVixDQUFlLElBQWYsRUFBcUIsT0FBckIsRUFMeUIsQ0FLTTs7QUFFbEMsTUFBTSxJQUFJLEdBQUcsSUFBYjtBQUNBLE9BQUsscUNBQUwsR0FBNkMsQ0FBN0MsQ0FSNEIsQ0FVekI7QUFDQTtBQUNBOztBQUVBLE9BQUssV0FBTCxHQUFtQixLQUFLLE1BQUwsQ0FBWSxLQUFaLEVBQW5CO0FBQ0EsT0FBSyxZQUFMLEdBQW9CLEtBQUssTUFBTCxDQUFZLE1BQVosRUFBcEI7QUFFSCxFQUFBLENBQUMsQ0FBQyxTQUFGLENBQVksNEJBQVosRUFBMEMsVUFBVSxLQUFWLEVBQWlCLEdBQWpCLEVBQXVCO0FBQzFELElBQUEsSUFBSSxDQUFDLHFDQUFMLENBQTJDLElBQTNDLENBQWdELElBQWhELEVBQXNELEdBQXRELEVBRDBELENBRTFEOztBQUNBLElBQUEsSUFBSSxDQUFDLFdBQUwsQ0FBaUIsSUFBakIsQ0FBc0IsSUFBdEI7QUFDSCxHQUpKO0FBTUc7Ozs7QUFHQTs7Ozs7O0FBS0EsTUFBRyxLQUFLLE1BQVIsRUFBZTtBQUNYLFNBQUssTUFBTCxDQUFZLEVBQVosQ0FBZSxZQUFmLEVBQTZCLFVBQVMsR0FBVCxFQUFjO0FBQ3ZDLFVBQUcsSUFBSSxDQUFDLHFCQUFMLEVBQUgsRUFBZ0M7QUFDNUIsUUFBQSxJQUFJLENBQUMsTUFBTCxDQUFZLElBQVosQ0FBaUIsSUFBakIsRUFBdUIsSUFBSSxDQUFDLE9BQTVCLEVBQXFDLEdBQXJDO0FBQ0g7QUFDSixLQUpEO0FBS0g7QUFDSjs7QUFBQSxDLENBRUQ7QUFDQTs7QUFDQSxVQUFVLENBQUMsU0FBWCxHQUF1QixNQUFNLENBQUMsTUFBUCxDQUFjLFNBQVMsQ0FBQyxTQUF4QixFQUFtQztBQUN0RCxFQUFBLFdBQVcsRUFBRTtBQUNULElBQUEsS0FBSyxFQUFFO0FBREU7QUFEeUMsQ0FBbkMsQ0FBdkI7QUFPQTs7Ozs7O0FBS0EsVUFBVSxDQUFDLFNBQVgsQ0FBcUIscUJBQXJCLEdBQTZDLFlBQVk7QUFDeEQsTUFBSSxLQUFLLHFDQUFMLEtBQStDLENBQW5ELEVBQXFEO0FBQ3BELFdBQU8sSUFBUDtBQUNBOztBQUNELFNBQU8sS0FBUDtBQUNBLENBTEQ7O0FBT0EsVUFBVSxDQUFDLFNBQVgsQ0FBcUIscUNBQXJCLEdBQTZELFlBQVk7QUFDeEUsT0FBSyxnQ0FBTDtBQUNBLENBRkQ7O0FBSUEsVUFBVSxDQUFDLFNBQVgsQ0FBcUIsZ0NBQXJCLEdBQXdELFVBQVUsT0FBVixFQUFtQjtBQUN2RSxPQUFLLHFDQUFMLEdBQTZDLEtBQUsscUNBQUwsS0FBK0MsQ0FBL0MsR0FBbUQsQ0FBbkQsR0FBdUQsQ0FBcEc7QUFDSCxDQUZEOztBQUlBLFVBQVUsQ0FBQyxTQUFYLENBQXFCLE1BQXJCLEdBQThCLFVBQVUsT0FBVixFQUFtQjtBQUM3QyxFQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVkseUNBQVo7QUFDSCxDQUZEOztBQUlBLFVBQVUsQ0FBQyxTQUFYLENBQXFCLFdBQXJCLEdBQW1DLFlBQVc7QUFFdEMsTUFBSSxPQUFPLEdBQUcsRUFBZCxDQUZzQyxDQUd0QztBQUNBOztBQUVBLE1BQUksV0FBVyxHQUFHLEVBQWxCLENBTnNDLENBT3RDO0FBQ0E7O0FBRUE7Ozs7QUFHQSxPQUFLLE9BQUwsQ0FBYSxTQUFiLENBQ1EsQ0FEUixFQUNXLENBRFgsRUFFUSxLQUFLLFdBRmIsRUFFMEIsS0FBSyxZQUYvQjtBQUdQLENBaEJEOztBQWtCQSxNQUFNLENBQUMsT0FBUCxHQUFpQixVQUFqQjs7Ozs7QUNoR0EsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLG1CQUFELENBQWxCOztBQUNBLElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyxjQUFELENBQXhCO0FBRUE7Ozs7OztBQUlBLFNBQVMsdUJBQVQsQ0FBaUMsT0FBakMsRUFBMEM7QUFFdEMsRUFBQSxVQUFVLENBQUMsSUFBWCxDQUFnQixJQUFoQixFQUFzQixPQUF0QixFQUZzQyxDQUVOOztBQUVoQyxPQUFLLElBQUwsR0FBWSxZQUFZLENBQ3ZCLENBREQ7O0FBR0EsT0FBSyxJQUFMO0FBRUE7Ozs7QUFHQSxPQUFLLE1BQUwsR0FBYyxVQUFVLE9BQVYsRUFBbUIsR0FBbkIsRUFBd0I7QUFDbEMsU0FBSyxXQUFMLEdBRGtDLENBRWxDOztBQUNBLFFBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxxQkFBZCxFQUFvQztBQUNoQyxVQUFNLENBQUMsR0FBRyxHQUFHLENBQUMscUJBQUosQ0FBMEIsQ0FBcEM7QUFDQSxVQUFJLEtBQUssR0FBRyxJQUFJLElBQUosQ0FBUyxDQUFULEVBQVksQ0FBWixFQUFlLENBQWYsRUFBa0IsS0FBSyxZQUF2QixDQUFaO0FBQ0EsTUFBQSxLQUFLLENBQUMsSUFBTixDQUFXLEtBQUssT0FBaEI7QUFDSDtBQUNKLEdBUkQ7QUFTQTs7Ozs7QUFHQSxPQUFLLGVBQUwsR0FBdUIsVUFBVSxHQUFWLEVBQWUsQ0FDbEM7QUFDSCxHQUZEO0FBR0E7Ozs7O0FBR0EsT0FBSyxjQUFMLEdBQXNCLFVBQVUsR0FBVixFQUFlLENBRXBDLENBRkQ7QUFHQTs7Ozs7QUFHQSxPQUFLLG9CQUFMLEdBQTRCLFVBQVUsR0FBVixFQUFlLENBRTFDLENBRkQ7QUFLSDs7QUFDRCx1QkFBdUIsQ0FBQyxTQUF4QixHQUFvQyxNQUFNLENBQUMsTUFBUCxDQUFjLFVBQVUsQ0FBQyxTQUF6QixFQUFvQztBQUNwRSxFQUFBLFdBQVcsRUFBRTtBQUNULElBQUEsS0FBSyxFQUFFO0FBREU7QUFEdUQsQ0FBcEMsQ0FBcEM7QUFNQSxNQUFNLENBQUMsT0FBUCxHQUFpQix1QkFBakI7Ozs7O0FDdkRBOzs7QUFJQSxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMsbUJBQUQsQ0FBbEI7O0FBQ0EsSUFBSSxVQUFVLEdBQUcsT0FBTyxDQUFDLGNBQUQsQ0FBeEIsQyxDQUNBO0FBQ0E7OztBQUNBLFNBQVMsb0JBQVQsQ0FBK0IsT0FBL0IsRUFBd0M7QUFFcEMsRUFBQSxVQUFVLENBQUMsSUFBWCxDQUFnQixJQUFoQixFQUFzQixPQUF0QixFQUZvQyxDQUVKOztBQUVoQyxNQUFJLElBQUo7O0FBRUEsT0FBSywrQkFBTCxHQUF1QyxVQUFVLGVBQVYsRUFBMkIsS0FBM0IsRUFBa0MsSUFBbEMsRUFBd0MsTUFBeEMsRUFBZ0Q7QUFDcEY7QUFDQyxRQUFNLE9BQU8sR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUwsQ0FBVSxNQUFNLEdBQUcsRUFBbkIsSUFBeUIsRUFBNUIsR0FBaUMsQ0FBdkQsQ0FGbUYsQ0FFekI7QUFDMUQ7QUFDQTs7QUFDQSxTQUFJLElBQUksQ0FBQyxHQUFHLENBQVosRUFBZSxDQUFDLEdBQUcsS0FBbkIsRUFBMkIsQ0FBQyxJQUFJLEVBQWhDLEVBQW1DO0FBQy9CLFVBQUcsQ0FBQyxHQUFHLE9BQVAsRUFBZTtBQUNYO0FBQ0g7O0FBQ0QsVUFBSSxLQUFLLEdBQUcsSUFBSSxHQUFHLENBQW5CLENBSitCLENBSy9COztBQUNBLFVBQUcsS0FBSyxHQUFHLENBQVIsSUFBYSxLQUFLLEdBQUcsS0FBeEIsRUFBOEI7QUFDMUI7QUFDSDs7QUFDRCxVQUFJLEVBQUUsR0FBRyxLQUFLLEdBQUcsS0FBSyxPQUF0QjtBQUNBLFVBQUksRUFBRSxHQUFHLEtBQUssT0FBZDtBQUVBLFVBQUksRUFBRSxHQUFHLEVBQVQ7QUFDQSxVQUFJLEVBQUUsR0FBSSxLQUFLLEdBQUcsR0FBUixLQUFnQixRQUFRLENBQUMsS0FBSyxHQUFHLEdBQVQsQ0FBekIsR0FBMEMsRUFBMUMsR0FBK0MsRUFBeEQ7QUFFQSxVQUFNLElBQUksR0FBRyxJQUFJLElBQUosQ0FBUyxFQUFULEVBQWEsRUFBYixFQUFpQixFQUFqQixFQUFxQixFQUFyQixDQUFiOztBQUNBLFVBQUcsRUFBRSxLQUFLLEVBQVYsRUFBYTtBQUNULFFBQUEsSUFBSSxDQUFDLFFBQUwsQ0FBYyxLQUFkO0FBQ0EsUUFBQSxJQUFJLENBQUMsbUJBQUwsQ0FBeUIsWUFBekI7QUFDSDs7QUFDRCxNQUFBLElBQUksQ0FBQyxhQUFMLENBQW1CLEtBQUssU0FBTCxFQUFuQjtBQUNBLE1BQUEsSUFBSSxDQUFDLGNBQUwsQ0FBb0IsS0FBSyxjQUF6QjtBQUNBLE1BQUEsZUFBZSxDQUFDLElBQWhCLENBQXFCLElBQXJCO0FBQ0g7O0FBRUQsV0FBTyxlQUFQO0FBQ0gsR0EvQkQ7O0FBaUNBLE9BQUssV0FBTCxHQUFtQixZQUFZO0FBQzNCLFFBQUksS0FBSyxHQUFHLEtBQUssTUFBTCxDQUFZLEtBQVosRUFBWjtBQUNBLFNBQUssVUFBTCxDQUFnQixVQUFoQixHQUE2QixFQUE3QjtBQUNBLFNBQUssVUFBTCxDQUFnQixVQUFoQixHQUE2QixLQUFLLCtCQUFMLENBQXFDLEtBQUssVUFBTCxDQUFnQixVQUFyRCxFQUFpRSxLQUFqRSxFQUF3RSxDQUF4RSxDQUE3QjtBQUNILEdBSkQ7O0FBTUEsT0FBSyxXQUFMLEdBQW1CLFlBQVc7QUFFMUIsUUFBSSxPQUFPLEdBQUcsRUFBZDtBQUNBLElBQUEsT0FBTyxDQUFDLENBQVIsR0FBWSxDQUFDLElBQUksS0FBSyxlQUFMLENBQXFCLENBQTFCLElBQTZCLEtBQUssU0FBOUM7QUFDQSxJQUFBLE9BQU8sQ0FBQyxDQUFSLEdBQVksQ0FBQyxJQUFJLEtBQUssZUFBTCxDQUFxQixDQUExQixJQUE2QixLQUFLLFNBQTlDO0FBRUEsUUFBSSxXQUFXLEdBQUcsRUFBbEI7QUFDQSxJQUFBLFdBQVcsQ0FBQyxDQUFaLEdBQWlCLEtBQUssTUFBTCxDQUFZLEtBQVosRUFBRCxHQUF1QixLQUFLLFNBQTVDO0FBQ0EsSUFBQSxXQUFXLENBQUMsQ0FBWixHQUFpQixLQUFLLE1BQUwsQ0FBWSxNQUFaLEVBQUQsR0FBd0IsS0FBSyxTQUE3Qzs7QUFFQSxRQUFHLEtBQUssT0FBUixFQUFnQjtBQUNaLFdBQUssT0FBTCxDQUFhLFNBQWIsQ0FDSSxPQUFPLENBQUMsQ0FEWixFQUNlLE9BQU8sQ0FBQyxDQUR2QixFQUVJLFdBQVcsQ0FBQyxDQUZoQixFQUVtQixXQUFXLENBQUMsQ0FGL0I7QUFHSDtBQUdKLEdBakJEO0FBa0JBOzs7Ozs7O0FBS0EsT0FBSyx3QkFBTCxHQUFnQyxZQUFVO0FBQ3RDLFFBQUksS0FBSyxHQUFHLEtBQUssTUFBTCxDQUFZLEtBQVosRUFBWjtBQUNBLFNBQUssK0JBQUwsQ0FBcUMsS0FBSyxVQUFMLENBQWdCLFVBQXJELEVBQWlFLEtBQWpFLEVBQXdFLENBQXhFO0FBQ0gsR0FIRDs7QUFLQSxPQUFLLFdBQUwsR0FBbUIsVUFBVSxHQUFWLEVBQWU7QUFDOUIsU0FBSyxVQUFMLENBQWdCLFVBQWhCLEdBQTZCLEVBQTdCO0FBRUEsUUFBSSxLQUFLLEdBQUcsS0FBSyxNQUFMLENBQVksS0FBWixFQUFaLENBSDhCLENBSTlCO0FBQ0E7O0FBQ0EsUUFBRyxLQUFLLGVBQUwsQ0FBcUIsQ0FBckIsSUFBMEIsQ0FBN0IsRUFBZ0M7QUFDNUI7QUFDQTtBQUNBLFVBQU0sU0FBUyxHQUFHLEtBQUssZUFBTCxDQUFxQixDQUFyQixHQUF1QixLQUFLLFNBQUwsRUFBekMsQ0FINEIsQ0FJNUI7O0FBQ0EsV0FBSyxVQUFMLENBQWdCLFVBQWhCLEdBQTZCLEtBQUssK0JBQUwsQ0FBcUMsS0FBSyxVQUFMLENBQWdCLFVBQXJELEVBQWlFLFNBQWpFLEVBQTRFLENBQUMsQ0FBN0UsQ0FBN0I7QUFFQSxVQUFNLFVBQVUsR0FBRyxLQUFLLEdBQUMsS0FBSyxTQUFMLEVBQU4sR0FBeUIsS0FBSyxlQUFMLENBQXFCLENBQXJCLEdBQXVCLEtBQUssU0FBTCxFQUFuRSxDQVA0QixDQVE1Qjs7QUFDQSxXQUFLLFVBQUwsQ0FBZ0IsVUFBaEIsR0FBNkIsS0FBSywrQkFBTCxDQUFxQyxLQUFLLFVBQUwsQ0FBZ0IsVUFBckQsRUFBaUUsVUFBakUsRUFBNkUsQ0FBN0UsQ0FBN0I7QUFDSCxLQWhCNkIsQ0FpQjlCOzs7QUFDQSxRQUFHLEtBQUssZUFBTCxDQUFxQixDQUFyQixHQUF1QixLQUFLLFNBQUwsRUFBdkIsR0FBMEMsQ0FBN0MsRUFBZ0Q7QUFDNUMsVUFBTSxXQUFVLEdBQUcsS0FBSyxHQUFDLEtBQUssU0FBTCxFQUFOLEdBQXlCLEtBQUssZUFBTCxDQUFxQixDQUFyQixHQUF1QixLQUFLLFNBQUwsRUFBdkIsR0FBMEMsQ0FBQyxDQUF2Rjs7QUFDQSxVQUFNLFVBQVUsR0FBRyxDQUFDLEdBQUQsR0FBSyxLQUFLLGVBQUwsQ0FBcUIsQ0FBMUIsR0FBNEIsS0FBSyxTQUFMLEVBQS9DO0FBQ0EsV0FBSyxVQUFMLENBQWdCLFVBQWhCLEdBQTZCLEtBQUssK0JBQUwsQ0FBcUMsS0FBSyxVQUFMLENBQWdCLFVBQXJELEVBQWlFLFdBQWpFLEVBQTZFLENBQTdFLEVBQWdGLFVBQWhGLENBQTdCO0FBQ0g7QUFFSixHQXhCRDs7QUEwQkEsT0FBSyxNQUFMLEdBQWMsWUFBWTtBQUN0QixTQUFLLFdBQUw7QUFDQSxTQUFLLFVBQUwsQ0FBZ0IsVUFBaEIsQ0FBMkIsT0FBM0IsQ0FBbUMsVUFBUyxPQUFULEVBQWlCO0FBQ2hELE1BQUEsT0FBTyxDQUFDLElBQVIsQ0FBYSxLQUFLLE9BQWxCLEVBQTJCLElBQUUsS0FBSyxTQUFsQztBQUNILEtBRkQsRUFFRyxJQUZIO0FBR0gsR0FMRDtBQU1BOzs7OztBQUdBLE9BQUssZUFBTCxHQUF1QixVQUFVLEdBQVYsRUFBZTtBQUNsQyxTQUFLLFNBQUwsSUFBa0IsR0FBRyxDQUFDLEtBQXRCLENBRGtDLENBRWxDOztBQUNBLFNBQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsV0FBdkIsQ0FDSSxHQUFHLENBQUMsS0FEUixFQUNlLENBRGYsRUFDa0IsR0FBRyxDQUFDLE1BQUosQ0FBVyxDQUFYLEdBQWUsS0FBSyxPQUR0QyxFQUMrQyxDQUQvQyxFQUhrQyxDQU1sQzs7QUFDQSxTQUFLLGVBQUwsQ0FBcUIsQ0FBckIsR0FBeUIsS0FBSyxPQUFMLENBQWEsU0FBYixDQUF1QixlQUF2QixFQUF6QjtBQUNBLFFBQUksS0FBSyxHQUFHLEtBQUssT0FBTCxHQUFlLEtBQUssT0FBTCxHQUFhLEtBQUssUUFBTCxFQUF4QztBQUNBLFNBQUssZUFBTCxDQUFxQixDQUFyQixJQUEwQixLQUExQjtBQUVBLFNBQUssV0FBTCxDQUFpQixHQUFqQjtBQUNBLFNBQUssTUFBTCxDQUFZLEtBQUssT0FBakIsRUFBMEIsR0FBMUI7QUFDSCxHQWJEO0FBY0E7Ozs7O0FBR0EsT0FBSyxjQUFMLEdBQXNCLFVBQVUsR0FBVixFQUFlO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBLFNBQUssZUFBTCxDQUFxQixDQUFyQixJQUEwQixLQUFLLDBCQUFMLENBQWdDLEdBQUcsQ0FBQyxxQkFBSixDQUEwQixDQUExRCxFQUE2RCxLQUFLLGVBQUwsQ0FBcUIsQ0FBbEYsRUFBcUYsS0FBSyxlQUFMLENBQXFCLENBQTFHLENBQTFCLENBSmlDLENBS2pDO0FBQ0E7O0FBQ0EsUUFBSSxLQUFLLEdBQUcsS0FBSyxPQUFMLEdBQWUsS0FBSyxPQUFMLEdBQWEsS0FBSyxRQUFMLEVBQXhDO0FBQ0EsU0FBSyxlQUFMLENBQXFCLENBQXJCLElBQTBCLEtBQTFCO0FBRUEsU0FBSyxPQUFMLENBQWEsU0FBYixDQUF1QixTQUF2QixDQUNJLEtBQUssZUFBTCxDQUFxQixDQUR6QixFQUM0QixDQUQ1QjtBQUdBLFNBQUssV0FBTCxDQUFpQixHQUFqQjtBQUNBLFNBQUssTUFBTCxDQUFZLEtBQUssT0FBakIsRUFBMEIsR0FBMUI7QUFDSCxHQWZEO0FBZ0JBOzs7OztBQUdBLE9BQUssb0JBQUwsR0FBNEIsVUFBVSxHQUFWLEVBQWU7QUFDdkMsU0FBSyxlQUFMLEdBQXVCLEdBQUcsQ0FBQyxNQUEzQjtBQUNBLFNBQUssZUFBTCxDQUFxQixDQUFyQixJQUEwQixLQUFLLDBCQUFMLENBQWdDLEdBQUcsQ0FBQyxxQkFBSixDQUEwQixDQUExRCxFQUE2RCxLQUFLLGVBQUwsQ0FBcUIsQ0FBbEYsRUFBcUYsS0FBSyxlQUFMLENBQXFCLENBQTFHLENBQTFCLENBRnVDLENBR3ZDOztBQUNBLFNBQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsU0FBdkIsQ0FDSSxLQUFLLGVBQUwsQ0FBcUIsQ0FEekIsRUFDNEIsS0FBSyxlQUFMLENBQXFCLENBRGpEO0FBR0EsU0FBSyxXQUFMLENBQWlCLEdBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksS0FBSyxPQUFqQixFQUEwQixHQUExQjtBQUNILEdBVEQ7QUFVQTs7Ozs7QUFHQyxPQUFLLHdCQUFMLEdBQWdDLFVBQVMsR0FBVCxFQUFjO0FBQzNDLFNBQUssU0FBTCxDQUFlLHdCQUFmLENBQXdDLElBQXhDLENBQTZDLElBQTdDLEVBQW1ELEdBQW5ELEVBQXdELElBQXhEOztBQUNBLFNBQUssV0FBTDtBQUNBLFNBQUssTUFBTCxDQUFZLEdBQVo7QUFDRixHQUpEO0FBS0Q7Ozs7Ozs7O0FBTUEsT0FBSyxJQUFMLEdBQVksWUFBWTtBQUNwQixJQUFBLElBQUksR0FBRyxJQUFQO0FBQ0EsU0FBSyxZQUFMLEdBQW9CLEtBQUssT0FBekI7QUFDQSxTQUFLLFlBQUwsR0FBb0IsS0FBSyxPQUF6QjtBQUNBLFNBQUssV0FBTDtBQUNBLFNBQUssTUFBTDtBQUVILEdBUEQ7O0FBU0EsT0FBSyxJQUFMO0FBRUgsQyxDQUVEO0FBQ0E7OztBQUNBLG9CQUFvQixDQUFDLFNBQXJCLEdBQWlDLE1BQU0sQ0FBQyxNQUFQLENBQWMsVUFBVSxDQUFDLFNBQXpCLEVBQW9DO0FBQ2pFLEVBQUEsV0FBVyxFQUFFO0FBQ1QsSUFBQSxLQUFLLEVBQUU7QUFERTtBQURvRCxDQUFwQyxDQUFqQztBQU1BLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLG9CQUFqQjs7Ozs7QUNyTUE7OztBQUdBLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyx1QkFBRCxDQUFwQjs7QUFDQSxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsY0FBRCxDQUF2Qjs7QUFFQSxTQUFTLFVBQVQsQ0FBcUIsT0FBckIsRUFBOEI7QUFFMUI7QUFFQSxFQUFBLFNBQVMsQ0FBQyxJQUFWLENBQWUsSUFBZixFQUFxQixPQUFyQixFQUowQixDQUlLOztBQUUvQixNQUFJLElBQUo7O0FBQ0EsT0FBSyxJQUFMLEdBQVksWUFBWTtBQUVwQjtBQUNBLFNBQUssT0FBTCxHQUFlLEVBQWY7QUFDQSxTQUFLLE9BQUwsR0FBZSxFQUFmO0FBRUEsU0FBSyxVQUFMLEdBQWtCLEVBQWxCLENBTm9CLENBTUM7O0FBQ3JCLFNBQUssVUFBTCxDQUFnQixVQUFoQixHQUE2QixFQUE3QjtBQUNBLFNBQUssVUFBTCxDQUFnQixRQUFoQixHQUEyQixFQUEzQixDQVJvQixDQVVwQjtBQUVBO0FBQ0E7O0FBRUEsU0FBSyxjQUFMLEdBQXNCLFNBQXRCLENBZm9CLENBaUJwQjtBQUVBO0FBQ0E7QUFDSCxHQXJCRDs7QUFzQkEsT0FBSyxJQUFMOztBQUVBLE9BQUssV0FBTCxHQUFtQixZQUFZLENBRTlCLENBRkQ7O0FBSUEsT0FBSyxXQUFMLEdBQW1CLFlBQVcsQ0FFN0IsQ0FGRDs7QUFJQSxPQUFLLE1BQUwsR0FBYyxZQUFZO0FBQ3RCLElBQUEsT0FBTyxDQUFDLElBQVIsQ0FBYSxJQUFiO0FBQ0E7Ozs7Ozs7Ozs7OztBQWdCSCxHQWxCRDtBQW9CQTs7OztBQUdBOzs7QUFHQTs7OztBQUdBOzs7Ozs7Ozs7Ozs7QUFhQTs7OztBQUdBOzs7QUFLSCxDLENBRUQ7QUFDQTs7O0FBQ0EsVUFBVSxDQUFDLFNBQVgsR0FBdUIsTUFBTSxDQUFDLE1BQVAsQ0FBYyxTQUFTLENBQUMsU0FBeEIsRUFBbUM7QUFDdEQsRUFBQSxXQUFXLEVBQUU7QUFDVCxJQUFBLEtBQUssRUFBRTtBQURFO0FBRHlDLENBQW5DLENBQXZCO0FBTUEsTUFBTSxDQUFDLE9BQVAsR0FBaUIsVUFBakI7Ozs7O0FDekdBOzs7O0FBS0EsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLG1CQUFELENBQWxCOztBQUNBLElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyxjQUFELENBQXhCLEMsQ0FDQTtBQUNBOzs7QUFDQSxTQUFTLGtCQUFULENBQTZCLE9BQTdCLEVBQXNDO0FBRWxDLEVBQUEsVUFBVSxDQUFDLElBQVgsQ0FBZ0IsSUFBaEIsRUFBc0IsT0FBdEIsRUFGa0MsQ0FFRjs7QUFFaEMsTUFBSSxJQUFKOztBQUVBLE9BQUssNkJBQUwsR0FBcUMsVUFBVSxlQUFWLEVBQTJCLEtBQTNCLEVBQWtDLElBQWxDLEVBQXdDO0FBQ3pFO0FBQ0EsU0FBSSxJQUFJLENBQUMsR0FBRyxDQUFaLEVBQWUsQ0FBQyxHQUFHLEtBQW5CLEVBQTJCLENBQUMsSUFBSSxFQUFoQyxFQUFtQztBQUMvQixVQUFJLEtBQUssR0FBRyxJQUFJLEdBQUcsQ0FBbkI7QUFDQSxVQUFJLEVBQUUsR0FBRyxLQUFLLEdBQUcsS0FBSyxPQUF0QjtBQUNBLFVBQUksRUFBRSxHQUFHLEtBQUssT0FBZDtBQUVBLFVBQUksRUFBRSxHQUFHLEVBQVQ7QUFDQSxVQUFJLEVBQUUsR0FBSSxLQUFLLEdBQUcsR0FBUixLQUFnQixRQUFRLENBQUMsS0FBSyxHQUFHLEdBQVQsQ0FBekIsR0FBMEMsRUFBMUMsR0FBK0MsRUFBeEQ7QUFFQSxVQUFNLElBQUksR0FBRyxJQUFJLElBQUosQ0FBUyxFQUFULEVBQWEsRUFBYixFQUFpQixFQUFqQixFQUFxQixFQUFyQixDQUFiOztBQUNBLFVBQUcsRUFBRSxLQUFLLEVBQVYsRUFBYTtBQUNULFFBQUEsSUFBSSxDQUFDLFFBQUwsQ0FBYyxLQUFkO0FBQ0EsUUFBQSxJQUFJLENBQUMsbUJBQUwsQ0FBeUIsVUFBekI7QUFDSDs7QUFDRCxNQUFBLElBQUksQ0FBQyxhQUFMLENBQW1CLEtBQUssU0FBTCxFQUFuQjtBQUNBLE1BQUEsSUFBSSxDQUFDLGNBQUwsQ0FBb0IsS0FBSyxjQUF6QjtBQUNBLE1BQUEsZUFBZSxDQUFDLElBQWhCLENBQXFCLElBQXJCO0FBQ0g7O0FBRUQsV0FBTyxlQUFQO0FBQ0gsR0FyQkQ7O0FBd0JBLE9BQUssV0FBTCxHQUFtQixZQUFZO0FBQzNCLFFBQUksTUFBTSxHQUFHLEtBQUssTUFBTCxDQUFZLE1BQVosRUFBYjtBQUNBLFNBQUssVUFBTCxDQUFnQixRQUFoQixHQUEyQixLQUFLLDZCQUFMLENBQW1DLEtBQUssVUFBTCxDQUFnQixRQUFuRCxFQUE2RCxNQUE3RCxFQUFxRSxDQUFyRSxDQUEzQjtBQUNILEdBSEQ7O0FBS0EsT0FBSyxXQUFMLEdBQW1CLFlBQVc7QUFDMUIsUUFBSSxPQUFPLEdBQUcsRUFBZDtBQUNBLElBQUEsT0FBTyxDQUFDLENBQVIsR0FBWSxDQUFDLElBQUksS0FBSyxlQUFMLENBQXFCLENBQTFCLElBQTZCLEtBQUssU0FBTCxFQUF6QztBQUNBLElBQUEsT0FBTyxDQUFDLENBQVIsR0FBWSxDQUFDLElBQUksS0FBSyxlQUFMLENBQXFCLENBQTFCLElBQTZCLEtBQUssU0FBTCxFQUF6QztBQUVBLFFBQUksV0FBVyxHQUFHLEVBQWxCO0FBQ0EsSUFBQSxXQUFXLENBQUMsQ0FBWixHQUFpQixLQUFLLE1BQUwsQ0FBWSxLQUFaLEVBQUQsR0FBdUIsS0FBSyxTQUFMLEVBQXZDO0FBQ0EsSUFBQSxXQUFXLENBQUMsQ0FBWixHQUFpQixLQUFLLE1BQUwsQ0FBWSxNQUFaLEVBQUQsR0FBd0IsS0FBSyxTQUFMLEVBQXhDO0FBRUEsU0FBSyxPQUFMLENBQWEsU0FBYixDQUNRLE9BQU8sQ0FBQyxDQURoQixFQUNtQixPQUFPLENBQUMsQ0FEM0IsRUFFUSxXQUFXLENBQUMsQ0FGcEIsRUFFdUIsV0FBVyxDQUFDLENBRm5DO0FBR0gsR0FaRDtBQWFBOzs7Ozs7O0FBS0EsT0FBSyx3QkFBTCxHQUFnQyxZQUFVO0FBQ3RDLFFBQUksTUFBTSxHQUFHLEtBQUssTUFBTCxDQUFZLE1BQVosRUFBYjtBQUNBLFNBQUssNkJBQUwsQ0FBbUMsS0FBSyxVQUFMLENBQWdCLFFBQW5ELEVBQTZELE1BQTdELEVBQXFFLENBQXJFO0FBQ0gsR0FIRDs7QUFLQSxPQUFLLFdBQUwsR0FBbUIsVUFBVSxHQUFWLEVBQWU7QUFFOUIsU0FBSyxVQUFMLENBQWdCLFFBQWhCLEdBQTJCLEVBQTNCO0FBRUEsUUFBSSxNQUFNLEdBQUcsS0FBSyxNQUFMLENBQVksTUFBWixFQUFiLENBSjhCLENBSzlCOztBQUNBLFFBQUcsS0FBSyxlQUFMLENBQXFCLENBQXJCLEdBQXVCLEtBQUssU0FBTCxFQUF2QixJQUEyQyxDQUE5QyxFQUFpRDtBQUM3QztBQUNBO0FBQ0EsVUFBTSxVQUFVLEdBQUcsS0FBSyxlQUFMLENBQXFCLENBQXJCLEdBQXVCLEtBQUssU0FBTCxFQUExQztBQUNBLFdBQUssVUFBTCxDQUFnQixRQUFoQixHQUEyQixLQUFLLDZCQUFMLENBQW1DLEtBQUssVUFBTCxDQUFnQixRQUFuRCxFQUE2RCxVQUE3RCxFQUF5RSxDQUFDLENBQTFFLENBQTNCO0FBRUEsVUFBTSxVQUFVLEdBQUcsTUFBTSxHQUFDLEtBQUssU0FBTCxFQUFQLEdBQTBCLEtBQUssZUFBTCxDQUFxQixDQUFyQixHQUF1QixLQUFLLFNBQUwsRUFBcEU7QUFDQSxXQUFLLFVBQUwsQ0FBZ0IsUUFBaEIsR0FBMkIsS0FBSyw2QkFBTCxDQUFtQyxLQUFLLFVBQUwsQ0FBZ0IsUUFBbkQsRUFBNkQsVUFBN0QsRUFBeUUsQ0FBekUsQ0FBM0I7QUFDSCxLQWQ2QixDQWU5Qjs7O0FBQ0EsUUFBRyxLQUFLLGVBQUwsQ0FBcUIsQ0FBckIsR0FBdUIsS0FBSyxTQUFMLEVBQXZCLEdBQTBDLENBQTdDLEVBQWdEO0FBQzVDLFVBQU0sV0FBVSxHQUFHLE1BQU0sR0FBQyxLQUFLLFNBQUwsRUFBUCxHQUEwQixLQUFLLGVBQUwsQ0FBcUIsQ0FBckIsR0FBdUIsS0FBSyxTQUFMLEVBQXZCLEdBQTBDLENBQUMsQ0FBeEY7O0FBQ0EsV0FBSyxVQUFMLENBQWdCLFFBQWhCLEdBQTJCLEtBQUssNkJBQUwsQ0FBbUMsS0FBSyxVQUFMLENBQWdCLFFBQW5ELEVBQTZELFdBQTdELEVBQXlFLENBQXpFLENBQTNCO0FBQ0g7QUFFSixHQXJCRDs7QUF1QkEsT0FBSyxNQUFMLEdBQWMsWUFBWTtBQUN0QixTQUFLLFdBQUw7QUFDQSxTQUFLLFVBQUwsQ0FBZ0IsUUFBaEIsQ0FBeUIsT0FBekIsQ0FBaUMsVUFBUyxPQUFULEVBQWlCO0FBQzlDLE1BQUEsT0FBTyxDQUFDLElBQVIsQ0FBYSxLQUFLLE9BQWxCLEVBQTJCLElBQUUsS0FBSyxTQUFsQztBQUNILEtBRkQsRUFFRyxJQUZIO0FBR0gsR0FMRDtBQU9BOzs7OztBQUdBLE9BQUssZUFBTCxHQUF1QixVQUFVLEdBQVYsRUFBZTtBQUNsQyxTQUFLLFNBQUwsSUFBa0IsR0FBRyxDQUFDLEtBQXRCLENBRGtDLENBR2xDOztBQUNBLFNBQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsV0FBdkIsQ0FDSSxDQURKLEVBQ08sR0FBRyxDQUFDLEtBRFgsRUFDa0IsQ0FEbEIsRUFDcUIsR0FBRyxDQUFDLE1BQUosQ0FBVyxDQUFYLEdBQWUsS0FBSyxPQUR6QztBQUlBLFFBQU0sTUFBTSxHQUFHLEtBQUssT0FBTCxDQUFhLFNBQWIsQ0FBdUIsU0FBdkIsRUFBZjtBQUNBLFNBQUssZUFBTCxDQUFxQixDQUFyQixHQUF5QixLQUFLLE9BQUwsQ0FBYSxTQUFiLENBQXVCLGVBQXZCLEVBQXpCO0FBRUEsU0FBSyxXQUFMLENBQWlCLEdBQWpCO0FBQ0EsU0FBSyxNQUFMLENBQVksS0FBSyxPQUFqQixFQUEwQixHQUExQjtBQUNILEdBYkQ7QUFjQTs7Ozs7QUFHQSxPQUFLLGNBQUwsR0FBc0IsVUFBVSxHQUFWLEVBQWU7QUFFakMsU0FBSyxlQUFMLENBQXFCLENBQXJCLElBQTBCLEtBQUssMEJBQUwsQ0FBZ0MsR0FBRyxDQUFDLHFCQUFKLENBQTBCLENBQTFELEVBQTZELEtBQUssZUFBTCxDQUFxQixDQUFsRixFQUFxRixLQUFLLGVBQUwsQ0FBcUIsQ0FBMUcsQ0FBMUIsQ0FGaUMsQ0FJakM7O0FBQ0EsUUFBSSxLQUFLLEdBQUcsS0FBSyxPQUFMLENBQWEsU0FBYixDQUF1QixlQUF2QixFQUFaO0FBQ0EsSUFBQSxLQUFLLEdBQUcsS0FBSyxPQUFMLEdBQWUsS0FBSyxPQUFMLEdBQWEsS0FBSyxTQUFMLEVBQXBDO0FBQ0EsU0FBSyxlQUFMLENBQXFCLENBQXJCLElBQTBCLEtBQTFCO0FBRUEsU0FBSyxPQUFMLENBQWEsU0FBYixDQUF1QixTQUF2QixDQUNJLENBREosRUFDTyxLQUFLLGVBQUwsQ0FBcUIsQ0FENUI7QUFJQSxTQUFLLFdBQUwsQ0FBaUIsR0FBakI7QUFFQSxTQUFLLE1BQUwsQ0FBWSxLQUFLLE9BQWpCLEVBQTBCLEdBQTFCO0FBQ0gsR0FoQkQ7QUFpQkE7Ozs7O0FBR0EsT0FBSyxvQkFBTCxHQUE0QixVQUFVLEdBQVYsRUFBZTtBQUN2QyxTQUFLLGVBQUwsR0FBdUIsR0FBRyxDQUFDLE1BQTNCLENBRHVDLENBR3ZDOztBQUVBLFNBQUssZUFBTCxDQUFxQixDQUFyQixJQUEwQixLQUFLLDBCQUFMLENBQWdDLEdBQUcsQ0FBQyxxQkFBSixDQUEwQixDQUExRCxFQUE2RCxLQUFLLGVBQUwsQ0FBcUIsQ0FBbEYsRUFBcUYsS0FBSyxlQUFMLENBQXFCLENBQTFHLENBQTFCO0FBRUEsU0FBSyxPQUFMLENBQWEsU0FBYixDQUF1QixTQUF2QixDQUNJLEtBQUssZUFBTCxDQUFxQixDQUR6QixFQUM0QixLQUFLLGVBQUwsQ0FBcUIsQ0FEakQ7QUFHQSxTQUFLLFdBQUwsQ0FBaUIsR0FBakI7QUFDQSxTQUFLLE1BQUwsQ0FBWSxLQUFLLE9BQWpCLEVBQTBCLEdBQTFCO0FBRUgsR0FiRDtBQWNBOzs7OztBQUdDLE9BQUssd0JBQUwsR0FBZ0MsVUFBUyxHQUFULEVBQWM7QUFDM0MsU0FBSyxTQUFMLENBQWUsd0JBQWYsQ0FBd0MsSUFBeEMsQ0FBNkMsSUFBN0MsRUFBbUQsR0FBbkQsRUFBd0QsSUFBeEQ7O0FBQ0EsU0FBSyxXQUFMO0FBQ0EsU0FBSyxNQUFMLENBQVksR0FBWjtBQUNGLEdBSkQ7QUFLRDs7Ozs7Ozs7QUFNQSxPQUFLLElBQUwsR0FBWSxZQUFZO0FBQ3BCLElBQUEsSUFBSSxHQUFHLElBQVA7QUFDQSxTQUFLLFlBQUwsR0FBb0IsS0FBSyxPQUF6QjtBQUNBLFNBQUssWUFBTCxHQUFvQixLQUFLLE9BQXpCO0FBQ0EsU0FBSyxXQUFMO0FBQ0EsU0FBSyxNQUFMO0FBRUgsR0FQRDs7QUFTQSxPQUFLLElBQUw7QUFFSCxDLENBRUQ7QUFDQTs7O0FBQ0Esa0JBQWtCLENBQUMsU0FBbkIsR0FBK0IsTUFBTSxDQUFDLE1BQVAsQ0FBYyxVQUFVLENBQUMsU0FBekIsRUFBb0M7QUFDL0QsRUFBQSxXQUFXLEVBQUU7QUFDVCxJQUFBLEtBQUssRUFBRTtBQURFO0FBRGtELENBQXBDLENBQS9CO0FBTUEsTUFBTSxDQUFDLE9BQVAsR0FBaUIsa0JBQWpCOzs7QUMxTEE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLGFBQUQsQ0FBdkI7O0lBQ00sa0I7Ozs7O0FBRUYsOEJBQVksT0FBWixFQUFxQjtBQUFBOztBQUFBOztBQUNqQjtBQUNBLDRGQUFNLE9BQU47QUFDQSxVQUFLLE9BQUwsR0FBZSxPQUFPLENBQUMsTUFBdkI7QUFDQSxVQUFLLE1BQUwsR0FBYyxPQUFPLENBQUMsYUFBdEI7QUFDQSxVQUFLLGtCQUFMLEdBQTBCLENBQUMsQ0FBM0I7O0FBRUEsUUFBTSxJQUFJLGdDQUFWO0FBRUE7Ozs7O0FBS0E7QUFDQTs7QUFDQTs7Ozs7OztBQWhCaUI7QUFxQnBCO0FBQ0Q7Ozs7Ozs7QUFNQTs7Ozs7Ozs7O21EQU1nQyxNLEVBQVEsTSxFQUFRO0FBQzVDLFVBQUksa0JBQWtCLEdBQUcsS0FBSyxxQkFBTCxDQUEyQixNQUEzQixFQUFtQyxNQUFuQyxDQUF6QjtBQUNBLFdBQUssZ0JBQUwsQ0FBc0Isa0JBQXRCO0FBQ0g7OzswQ0FFc0IsTSxFQUFRLE0sRUFBUTtBQUNuQyxVQUFJLEdBQUcsR0FBRyxLQUFLLE1BQUwsQ0FBWSxJQUFaLEVBQVY7O0FBQ0EsV0FBSyxJQUFJLENBQUMsR0FBRyxDQUFiLEVBQWdCLENBQUMsR0FBRyxHQUFwQixFQUF5QixDQUFDLEVBQTFCLEVBQThCO0FBQzFCLFlBQUksS0FBSyxHQUFHLEtBQUssTUFBTCxDQUFZLEdBQVosQ0FBZ0IsQ0FBaEIsQ0FBWjs7QUFDQSxZQUFJLEtBQUssQ0FBQyxXQUFOLENBQWtCLEtBQUssT0FBdkIsRUFBZ0MsTUFBaEMsRUFBd0MsTUFBeEMsQ0FBSixFQUFxRDtBQUNqRCxpQkFBTyxDQUFQO0FBQ0g7QUFDSjs7QUFDRCxhQUFPLENBQUMsQ0FBUjtBQUNIOzs7cUNBRWlCLEssRUFBTztBQUNyQixVQUFJLEtBQUssSUFBSSxDQUFULElBQWMsS0FBSyxHQUFHLEtBQUssTUFBTCxDQUFZLElBQVosRUFBMUIsRUFBOEM7QUFDMUMsYUFBSyxrQkFBTCxHQUEwQixLQUExQjtBQUNBLFFBQUEsQ0FBQyxDQUFDLE9BQUYsQ0FBVSxnQkFBVixFQUE0QjtBQUFDLFVBQUEsa0JBQWtCLEVBQUM7QUFBcEIsU0FBNUI7QUFDSDtBQUNKOzs7bURBRStCLEcsRUFBSztBQUN2QyxVQUFJLEdBQUcsS0FBSyxJQUFaLEVBQWlCO0FBQ2hCO0FBQ0E7O0FBQ0ssTUFBQSxTQUFTLENBQUMsU0FBVixDQUFvQiw4QkFBcEIsQ0FBbUQsR0FBbkQ7QUFDQSxXQUFLLDhCQUFMLENBQW9DLEdBQUcsQ0FBQyw2QkFBSixDQUFrQyxDQUF0RSxFQUF5RSxHQUFHLENBQUMsNkJBQUosQ0FBa0MsQ0FBM0c7QUFDSDtBQUNEOzs7Ozs7NkJBR1EsQ0FDSjtBQUNIOzs7O0VBdkU0QixTO0FBeUVqQzs7Ozs7Ozs7O0FBT0EsTUFBTSxDQUFDLE9BQVAsR0FBaUIsa0JBQWpCOzs7OztBQ2xGQSxJQUFJLFlBQVksR0FBRyxPQUFPLENBQUMsUUFBRCxDQUFQLENBQWtCLFlBQXJDO0FBQUEsSUFDSSxNQUFNLEdBQUcsSUFBSSxZQUFKLEdBQW1CLGVBQW5CLENBQW1DLEdBQW5DLENBRGI7O0FBR0EsTUFBTSxDQUFDLE9BQVAsR0FBa0IsTUFBbEI7QUFFQSxNQUFNLENBQUMsRUFBUCxDQUFVLFVBQVYsRUFBc0IsVUFBUyxHQUFULEVBQWM7QUFDaEMsRUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLEdBQVo7QUFDSCxDQUZEOzs7QUNMQTs7Ozs7Ozs7QUFFQSxJQUFJLG9CQUFvQixHQUFHLE9BQU8sQ0FBQywyQ0FBRCxDQUFsQzs7QUFDQSxJQUFJLGtCQUFrQixHQUFHLE9BQU8sQ0FBQyx5Q0FBRCxDQUFoQzs7QUFDQSxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMscUJBQUQsQ0FBdkI7O0FBQ0EsSUFBSSxtQkFBbUIsR0FBRyxPQUFPLENBQUMsK0JBQUQsQ0FBakM7O0FBRUEsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLHlCQUFELENBQXZCOztBQUNBLElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyxzQkFBRCxDQUF4Qjs7QUFDQSxJQUFJLGVBQWUsR0FBRyxPQUFPLENBQUMsbUNBQUQsQ0FBN0IsQyxDQUFvRTs7O0FBRXBFLElBQUkseUJBQXlCLEdBQUcsT0FBTyxDQUFDLGdEQUFELENBQXZDOztBQUNBLElBQUkseUJBQXlCLEdBQUcsT0FBTyxDQUFDLDhDQUFELENBQXZDOztBQUNBLElBQUksbUJBQW1CLEdBQUcsT0FBTyxDQUFDLCtCQUFELENBQWpDOztBQUNBLElBQUksa0JBQWtCLEdBQUcsT0FBTyxDQUFDLDhCQUFELENBQWhDOztBQUVBLElBQUksaUJBQWlCLEdBQUcsT0FBTyxDQUFDLDZCQUFELENBQS9COztBQUVBLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxvQkFBRCxDQUFwQjs7QUFFQSxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMscUJBQUQsQ0FBdkI7O0lBRU0scUI7OztBQUNGOzs7Ozs7OztBQVFBLGlDQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFFakI7QUFDQTtBQUNBLFFBQU0sY0FBYyxHQUFHLE9BQU8sQ0FBQyxNQUEvQixDQUppQixDQUlzQjs7QUFFdkM7Ozs7OztBQUtBLFFBQUksbUJBQUosQ0FBd0IsS0FBSyxnQkFBTCxDQUFzQixPQUF0QixFQUErQixjQUEvQixFQUErQyx3QkFBL0MsQ0FBeEI7QUFFQSxJQUFBLE9BQU8sQ0FBQyxhQUFSLEdBQXdCLElBQUksZUFBSixFQUF4QjtBQUVBLFFBQUksa0JBQUosQ0FBdUIsS0FBSyxnQkFBTCxDQUFzQixPQUF0QixFQUErQixjQUEvQixFQUErQyx1QkFBL0MsQ0FBdkIsRUFmaUIsQ0FlZ0Y7QUFDakc7O0FBQ0E7Ozs7OztBQUtBLFFBQU0sVUFBVSxHQUFHLElBQUksVUFBSixDQUFlLEtBQUssZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsY0FBL0IsRUFBK0MsY0FBL0MsQ0FBZixDQUFuQixDQXRCaUIsQ0FzQmtGO0FBQ25HOztBQUNBLElBQUEsT0FBTyxDQUFDLGFBQVIsR0FBd0IsSUFBeEIsQ0F4QmlCLENBeUJqQjtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxJQUFBLE9BQU8sQ0FBQyxVQUFSLEdBQXFCLFVBQXJCO0FBQ0EsUUFBSSxTQUFKLENBQWMsT0FBZCxFQTlCaUIsQ0FnQ2pCOztBQUNBOzs7Ozs7QUFLQSxRQUFJLFNBQUosQ0FBYyxLQUFLLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLGNBQS9CLEVBQStDLGFBQS9DLEVBQThELENBQTlELENBQWQ7QUFFQSxRQUFJLG1CQUFKLEdBeENpQixDQXdDVTs7QUFDM0I7Ozs7OztBQUtBLFFBQUkseUJBQUosQ0FBOEIsS0FBSyxnQkFBTCxDQUFzQixPQUF0QixFQUErQixjQUEvQixFQUErQywrQkFBL0MsRUFBZ0YsQ0FBaEYsQ0FBOUIsRUE5Q2lCLENBOENrRzs7QUFDbkg7Ozs7OztBQUtBLFFBQUkseUJBQUosQ0FBOEIsS0FBSyxnQkFBTCxDQUFzQixPQUF0QixFQUErQixjQUEvQixFQUErQyw2QkFBL0MsRUFBOEUsQ0FBOUUsQ0FBOUIsRUFwRGlCLENBb0RnRztBQUNqSDs7QUFDQTs7Ozs7O0FBS0EsUUFBSSxvQkFBSixDQUF5QixLQUFLLHNCQUFMLENBQTRCLE9BQTVCLEVBQXFDLGNBQXJDLEVBQXFELHlCQUFyRCxDQUF6QixFQTNEaUIsQ0EyRDBGOztBQUMzRzs7Ozs7O0FBS0EsUUFBSSxrQkFBSixDQUF1QixLQUFLLHNCQUFMLENBQTRCLE9BQTVCLEVBQXFDLGNBQXJDLEVBQXFELHVCQUFyRCxDQUF2QixFQWpFaUIsQ0FpRXNGOztBQUN2Rzs7Ozs7O0FBS0EsUUFBSSxpQkFBSixDQUFzQixLQUFLLHNCQUFMLENBQTRCLE9BQTVCLEVBQXFDLGNBQXJDLEVBQXFELGVBQXJELENBQXRCLEVBdkVpQixDQXVFNkU7QUFDakc7Ozs7cUNBRWdCLE8sRUFBUyxNLEVBQVEsVSxFQUFZLE0sRUFBTztBQUNqRCxVQUFHLE1BQUgsRUFBVTtBQUNOLFFBQUEsT0FBTyxDQUFDLE1BQVIsR0FBaUIsS0FBSyxZQUFMLENBQWtCLE1BQWxCLEVBQTBCLFVBQTFCLEVBQXNDLE1BQXRDLENBQWpCO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsUUFBQSxPQUFPLENBQUMsTUFBUixHQUFpQixLQUFLLFlBQUwsQ0FBa0IsTUFBbEIsRUFBMEIsVUFBMUIsQ0FBakI7QUFDSDs7QUFDRCxNQUFBLE9BQU8sQ0FBQyxPQUFSLEdBQWtCLEtBQUssbUJBQUwsQ0FBeUIsT0FBTyxDQUFDLE1BQWpDLENBQWxCO0FBQ0EsYUFBTyxPQUFQO0FBQ0g7OzsyQ0FDc0IsTyxFQUFTLE0sRUFBUSxVLEVBQVc7QUFDL0MsTUFBQSxPQUFPLENBQUMsTUFBUixHQUFpQixLQUFLLGtCQUFMLENBQXdCLE1BQXhCLEVBQWdDLFVBQWhDLENBQWpCO0FBQ0EsTUFBQSxPQUFPLENBQUMsT0FBUixHQUFrQixLQUFLLG1CQUFMLENBQXlCLE9BQU8sQ0FBQyxNQUFqQyxDQUFsQjtBQUNBLGFBQU8sT0FBUDtBQUNIOzs7aUNBQ1ksYyxFQUFnQixFLEVBQUksTSxFQUFPO0FBQ3BDLFVBQU0sU0FBUyxHQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBUCxDQUFjLGNBQWQsRUFBOEIsRUFBOUIsRUFBa0MsTUFBbEMsQ0FBRCxDQUFwQjtBQUNBLGFBQU8sU0FBUDtBQUNIOzs7dUNBQ21CLGMsRUFBZ0IsRSxFQUFJO0FBQ3BDLFVBQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBUCxDQUFhLGNBQWIsRUFBNkIsRUFBN0IsQ0FBRCxDQUF4QjtBQUNBLGFBQU8sU0FBUDtBQUNIOzs7d0NBQ21CLE0sRUFBTztBQUN2QixVQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBRCxDQUFOLENBQVUsVUFBVixDQUFxQixJQUFyQixDQUFkO0FBQ0EsTUFBQSxPQUFPLENBQUMsU0FBUixHQUFvQixJQUFJLFNBQUosQ0FBYyxPQUFkLENBQXBCO0FBQ0EsYUFBTyxPQUFQO0FBQ0g7Ozs7OztBQUdMLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLHFCQUFqQjs7Ozs7QUN0SUE7Ozs7QUFLQSxJQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsd0JBQUQsQ0FBNUI7O0FBRUEsSUFBTSxpQkFBaUIsR0FBRyxPQUFPLENBQUMsNEJBQUQsQ0FBakM7O0FBRUEsSUFBTSxZQUFZLEdBQUcsT0FBTyxDQUFDLHVCQUFELENBQTVCOztBQUVBLElBQU0scUJBQXFCLEdBQUcsT0FBTyxDQUFDLGtDQUFELENBQXJDOztBQUVBLElBQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyx1QkFBRCxDQUE1Qjs7QUFFQSxJQUFNLElBQUksR0FBRyxPQUFPLENBQUMsZUFBRCxDQUFwQjs7QUFFQSxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsaUJBQUQsQ0FBdEI7O0FBRUEsSUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLG9CQUFELENBQXpCOztBQUVBLElBQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxTQUFELENBQXRCLEMsQ0FFQTs7O0FBQ0EsQ0FBQyxDQUFDLFlBQVk7QUFFVixNQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsT0FBSCxFQUFiO0FBRUEsTUFBTSxJQUFJLEdBQUcsTUFBTSxFQUFuQjs7QUFFQSxXQUFTLFdBQVQsR0FBc0I7QUFDbEIsUUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVAsQ0FBZ0IsUUFBakM7QUFDQSxRQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsS0FBVCxDQUFlLEdBQWYsQ0FBZixDQUZrQixDQUdsQjs7QUFDQSxRQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQVAsR0FBYyxDQUFmLENBQXhCO0FBQ0EsV0FBTyxTQUFTLElBQUksU0FBcEI7QUFDSCxHQVpTLENBYVY7OztBQUNBLEVBQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxTQUFWLEVBQXFCLFVBQVUsSUFBVixFQUFnQjtBQUNqQyxJQUFBLE1BQU0sQ0FBQyxJQUFQLENBQWEsTUFBYixFQUFxQjtBQUNqQixNQUFBLElBQUksRUFBRSxXQURXO0FBRWpCLE1BQUEsU0FBUyxFQUFFLFdBQVcsRUFGTDtBQUdqQixNQUFBLElBQUksRUFBQztBQUhZLEtBQXJCO0FBS0gsR0FORCxFQWRVLENBcUJWO0FBQ0E7O0FBRUEsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLFdBQVYsRUFBdUIsVUFBUyxJQUFULEVBQWM7QUFDakMsSUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLGVBQVosRUFBNkI7QUFBQyxNQUFBLEtBQUssRUFBRSxJQUFSO0FBQWMsTUFBQSxRQUFRLEVBQUU7QUFBeEIsS0FBN0I7QUFDSCxHQUZEO0FBSUEsTUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLFFBQUQsQ0FBZCxDQTVCVSxDQTZCVjtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxNQUFNLG9CQUFvQixHQUFHLENBQTdCLENBbENVLENBa0NzQjs7QUFDaEMsTUFBTSxxQkFBcUIsR0FBRyxDQUE5QixDQW5DVSxDQW1DdUI7O0FBRWpDLEVBQUEsTUFBTSxDQUFDLEtBQVAsQ0FBYSxNQUFNLENBQUMsTUFBUCxHQUFnQixLQUFoQixLQUF3QixvQkFBckM7QUFDQSxFQUFBLE1BQU0sQ0FBQyxNQUFQLENBQWMsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsTUFBaEIsS0FBeUIscUJBQXZDLEVBdENVLENBdUNWOztBQUNBLEVBQUEsQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZLEtBQVosQ0FBa0IsWUFBVTtBQUV4QixJQUFBLENBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVSxNQUFWLENBQWlCLFVBQVMsSUFBVCxFQUFjO0FBQzNCLFVBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxRQUFELENBQWQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxLQUFQLENBQWEsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsS0FBaEIsS0FBd0Isb0JBQXJDO0FBQ0EsTUFBQSxNQUFNLENBQUMsTUFBUCxDQUFjLE1BQU0sQ0FBQyxNQUFQLEdBQWdCLE1BQWhCLEtBQXlCLHFCQUF2QztBQUNBLE1BQUEsTUFBTSxDQUFDLElBQVAsQ0FBWSxnQkFBWixFQUE4QjtBQUFDLFFBQUEsTUFBTSxFQUFFO0FBQVQsT0FBOUIsRUFKMkIsQ0FLM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNILEtBVkQ7QUFZQSxRQUFJLGVBQWUsR0FBRyxHQUF0QixDQWR3QixDQWNFO0FBQzFCOztBQUNBLElBQUEsQ0FBQyxDQUFDLGtDQUFELENBQUQsQ0FBc0MsRUFBdEMsQ0FBeUMsT0FBekMsRUFBa0QsWUFBVTtBQUN4RCxVQUFHLENBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVSxLQUFWLEtBQW9CLGVBQXZCLEVBQXlDO0FBQ3JDLFFBQUEsQ0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0IsS0FBcEIsR0FEcUMsQ0FDUjtBQUNoQztBQUVKLEtBTEQsRUFoQndCLENBc0J4Qjs7QUFDQSxJQUFBLENBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCLEVBQTdCLENBQWdDLE9BQWhDLEVBQXlDLFlBQVU7QUFDL0MsTUFBQSxDQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQixXQUF0QixDQUFrQyxTQUFsQzs7QUFDQSxVQUFHLENBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVSxLQUFWLEtBQW9CLGVBQXZCLEVBQXlDO0FBQ3JDLFFBQUEsQ0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0IsS0FBcEIsR0FEcUMsQ0FDUjtBQUNoQztBQUNKLEtBTEQsRUF2QndCLENBNkJ4QjtBQUNBOztBQUNJLElBQUEsQ0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0IsU0FBdEIsR0EvQm9CLENBZ0N4QjtBQUNILEdBakNELEVBeENVLENBNEVWOztBQUNBLE1BQUksRUFBRSxnQkFBZ0IsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBbEIsQ0FBSixFQUF5RDtBQUNyRCxJQUFBLEtBQUssQ0FBQyw0REFBRCxDQUFMO0FBQ0EsV0FBTyxLQUFQO0FBQ0g7O0FBRUQsTUFBTSxPQUFPLEdBQUcsRUFBaEI7QUFDQSxFQUFBLE9BQU8sQ0FBQyxNQUFSLEdBQWlCLE1BQWpCO0FBQ0EsRUFBQSxPQUFPLENBQUMsTUFBUixHQUFpQixNQUFqQjtBQUNBLEVBQUEsT0FBTyxDQUFDLE1BQVIsR0FBaUIsTUFBakI7QUFFQSxNQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBUixDQUFlLENBQWYsRUFBa0IsVUFBbEIsQ0FBNkIsSUFBN0IsQ0FBZCxDQXZGVSxDQXdGVjs7QUFDQSxFQUFBLE9BQU8sQ0FBQyxTQUFSLEdBQW9CLElBQUksU0FBSixDQUFjLEtBQUssT0FBbkIsQ0FBcEIsQ0F6RlUsQ0EwRlY7O0FBQ0EsRUFBQSxPQUFPLENBQUMsT0FBUixHQUFrQixPQUFsQjtBQUdBLE1BQU0sWUFBWSxHQUFHLElBQUksWUFBSixDQUFpQixPQUFqQixDQUFyQixDQTlGVSxDQThGc0M7O0FBRWhELE1BQUksaUJBQWlCLEdBQUcsSUFBSSxpQkFBSixDQUFzQixPQUF0QixDQUF4QjtBQUVBLE1BQUkscUJBQUosQ0FBMEIsT0FBMUIsRUFsR1UsQ0FrRzBCOztBQUVwQyxNQUFJLFlBQVksR0FBRyxJQUFJLFlBQUosQ0FBaUIsTUFBakIsQ0FBbkI7QUFFQSxNQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsR0FBRixFQUFmLENBdEdVLENBd0diO0FBQ0E7O0FBRUcsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLGdCQUFWLEVBQTRCLFVBQVUsSUFBVixFQUFnQjtBQUN4QztBQUNBLElBQUEsTUFBTSxDQUFDLElBQVAsQ0FBWSxnQkFBWixFQUE4QixJQUE5QjtBQUNILEdBSEQ7QUFLQSxFQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsU0FBVixFQUFxQixVQUFVLElBQVYsRUFBZ0I7QUFDakM7QUFDQSxJQUFBLE1BQU0sQ0FBQyxJQUFQLENBQVksZ0JBQVosRUFBOEIsSUFBOUI7QUFDSCxHQUhEO0FBS0EsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLGNBQVYsRUFBMEIsVUFBVSxJQUFWLEVBQWdCO0FBQ3RDO0FBQ0EsSUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLGVBQVosRUFBNkIsSUFBN0I7QUFDSCxHQUhEO0FBS0EsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLGNBQVYsRUFBMEIsVUFBVSxJQUFWLEVBQWdCO0FBQ3RDO0FBQ0EsSUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLGVBQVosRUFBNkIsSUFBN0I7QUFDSCxHQUhEO0FBS0EsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLGNBQVYsRUFBMEIsVUFBVSxJQUFWLEVBQWdCO0FBQ3RDO0FBQ0EsSUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLG1CQUFaLEVBQWlDLElBQWpDO0FBQ0gsR0FIRDtBQUtBOzs7O0FBR0EsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLDJCQUFWLEVBQXVDLFVBQVUsSUFBVixFQUFnQjtBQUNuRDtBQUNBLElBQUEsTUFBTSxDQUFDLElBQVAsQ0FBWSw4QkFBWixFQUE0QyxJQUE1QztBQUNILEdBSEQ7QUFLQTs7Ozs7QUFJQSxFQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsMkJBQVYsRUFBdUMsVUFBVSxLQUFWLEVBQWlCO0FBQ3BELFFBQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxLQUFQLElBQWdCLEtBQUssQ0FBQyxhQUF6QyxDQURvRCxDQUNJOztBQUV4RCxRQUFNLFNBQVMsR0FBSSxVQUFVLENBQUMsTUFBWCxHQUFvQixDQUFwQixJQUF5QixVQUFVLENBQUMsVUFBWCxHQUF3QixDQUFsRCxHQUF1RCxDQUF2RCxHQUEyRCxDQUFDLENBQTlFO0FBQ0EsUUFBTSxTQUFTLEdBQUcsWUFBWSxDQUFDLFFBQWIsRUFBbEI7QUFFQSxRQUFNLHFCQUFxQixHQUFHLFlBQVksQ0FBQyw4QkFBYixFQUE5QjtBQUVBLElBQUEscUJBQXFCLENBQUMsZUFBdEIsR0FBd0MsWUFBWSxDQUFDLGtCQUFiLEVBQXhDO0FBQ0EsSUFBQSxxQkFBcUIsQ0FBQyxLQUF0QixHQUE4QixTQUFTLEdBQUcsQ0FBWixHQUFnQixHQUFoQixHQUFzQixJQUFwRDtBQUVBLElBQUEscUJBQXFCLENBQUMsU0FBdEIsR0FBa0MsU0FBUyxHQUFHLHFCQUFxQixDQUFDLEtBQXBFO0FBRUEsUUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLHFCQUFiLENBQW1DLFVBQVUsQ0FBQyxLQUE5QyxFQUFxRCxVQUFVLENBQUMsS0FBaEUsQ0FBaEI7QUFDQSxRQUFNLE1BQU0sR0FBRyxZQUFZLENBQUMsa0JBQWIsRUFBZjtBQUVBLFFBQU0sTUFBTSxHQUFHLEVBQWY7QUFDQSxJQUFBLE1BQU0sQ0FBQyxDQUFQLEdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBUixHQUFZLE1BQU0sQ0FBQyxDQUFwQixJQUF5QixZQUFZLENBQUMsUUFBYixFQUFwQztBQUNBLElBQUEsTUFBTSxDQUFDLENBQVAsR0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFSLEdBQVksTUFBTSxDQUFDLENBQXBCLElBQXlCLFlBQVksQ0FBQyxRQUFiLEVBQXBDO0FBRUEsSUFBQSxxQkFBcUIsQ0FBQyxNQUF0QixHQUErQixNQUEvQixDQXBCb0QsQ0FzQjFEO0FBQ0E7O0FBQ00sSUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLGdCQUFaLEVBQThCLHFCQUE5QixFQXhCb0QsQ0EwQnBEO0FBQ0E7O0FBQ0EsSUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLGFBQVosRUFBMkIscUJBQTNCLEVBNUJvRCxDQTZCaEQ7QUFDSjtBQUNILEdBL0JEO0FBaUNBOzs7O0FBR0EsV0FBUyx5QkFBVCxDQUFtQyxNQUFuQyxFQUEwQztBQUN0QyxRQUFNLG9CQUFvQixHQUFHLFlBQVksQ0FBQyx1QkFBYixFQUE3QjtBQUNBLFFBQU0sT0FBTyxHQUFHLFlBQVksQ0FBQyxxQkFBYixDQUFtQyxNQUFNLENBQUMsS0FBMUMsRUFBaUQsTUFBTSxDQUFDLEtBQXhELENBQWhCO0FBQ0EsUUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLGtCQUFiLEVBQWY7QUFFQSxRQUFNLE1BQU0sR0FBRyxFQUFmO0FBQ0EsSUFBQSxNQUFNLENBQUMsQ0FBUCxHQUFXLENBQUMsT0FBTyxDQUFDLENBQVIsR0FBWSxNQUFNLENBQUMsQ0FBcEIsSUFBeUIsWUFBWSxDQUFDLFFBQWIsRUFBcEM7QUFDQSxJQUFBLE1BQU0sQ0FBQyxDQUFQLEdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBUixHQUFZLE1BQU0sQ0FBQyxDQUFwQixJQUF5QixZQUFZLENBQUMsUUFBYixFQUFwQztBQUVBLElBQUEsb0JBQW9CLENBQUMsNkJBQXJCLEdBQXFELE1BQXJELENBVHNDLENBVXRDOztBQUNBLFFBQUksaUJBQWlCLENBQUMsY0FBbEIsT0FBdUMsS0FBdkMsSUFBZ0QsaUJBQWlCLENBQUMsY0FBbEIsT0FBdUMsQ0FBM0YsRUFBOEY7QUFDMUYsTUFBQSxvQkFBb0IsQ0FBQyxTQUFyQixHQUFpQyxJQUFqQztBQUNBLE1BQUEsb0JBQW9CLENBQUMsa0JBQXJCLEdBQTBDLGlCQUFpQixDQUFDLGNBQWxCLEVBQTFDO0FBQ0g7O0FBQ0QsV0FBTyxvQkFBUDtBQUVIO0FBRUQ7Ozs7O0FBR0EsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLFlBQVYsRUFBd0IsVUFBVSxHQUFWLEVBQWU7QUFDbkMsUUFBTSxLQUFLLEdBQUcsR0FBRyxDQUFDLGFBQWxCO0FBQ0EsSUFBQSxLQUFLLENBQUMsY0FBTjtBQUNBLFFBQU0sb0JBQW9CLEdBQUcseUJBQXlCLENBQUMsS0FBSyxDQUFDLGFBQU4sQ0FBb0IsQ0FBcEIsQ0FBRCxDQUF0RDtBQUNBLElBQUEsTUFBTSxDQUFDLElBQVAsQ0FBWSx3QkFBWixFQUFzQyxvQkFBdEM7QUFFSCxHQU5EO0FBUUE7Ozs7O0FBSUEsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLFdBQVYsRUFBdUIsVUFBVSxDQUFWLEVBQWE7QUFDaEMsSUFBQSxDQUFDLENBQUMsY0FBRjs7QUFDQSxZQUFRLENBQUMsQ0FBQyxLQUFWO0FBQ0ksV0FBSyxDQUFMO0FBQ0ksWUFBTSxvQkFBb0IsR0FBRyx5QkFBeUIsQ0FBQyxDQUFELENBQXREO0FBQ0EsUUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLHdCQUFaLEVBQXNDLG9CQUF0QztBQUNBOztBQUNKLFdBQUssQ0FBTDtBQUNJO0FBQ0E7O0FBQ0osV0FBSyxDQUFMO0FBQ0k7QUFDQTs7QUFDSixjQVhKLENBWVE7O0FBWlI7QUFjSCxHQWhCRDtBQWtCQTs7OztBQUdBLFdBQVMsc0NBQVQsQ0FBZ0QsTUFBaEQsRUFBdUQ7QUFFbkQsUUFBTSxnQkFBZ0IsR0FBRyxZQUFZLENBQUMsdUJBQWIsRUFBekI7QUFDQSxJQUFBLGdCQUFnQixDQUFDLHFCQUFqQixHQUF5QyxZQUFZLENBQUMscUJBQWIsQ0FBbUMsTUFBTSxDQUFDLEtBQTFDLEVBQWlELE1BQU0sQ0FBQyxLQUF4RCxDQUF6QztBQUVBLFFBQU0sZUFBZSxHQUFHLFlBQVksQ0FBQyxrQkFBYixFQUF4QjtBQUNBLFFBQU0sbUJBQW1CLEdBQUcsRUFBNUI7QUFFQSxJQUFBLG1CQUFtQixDQUFDLENBQXBCLEdBQXdCLENBQUMsZ0JBQWdCLENBQUMscUJBQWpCLENBQXVDLENBQXZDLEdBQTJDLGVBQWUsQ0FBQyxDQUE1RCxJQUFpRSxZQUFZLENBQUMsUUFBYixFQUF6RjtBQUNBLElBQUEsbUJBQW1CLENBQUMsQ0FBcEIsR0FBd0IsQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBakIsQ0FBdUMsQ0FBdkMsR0FBMkMsZUFBZSxDQUFDLENBQTVELElBQWlFLFlBQVksQ0FBQyxRQUFiLEVBQXpGO0FBRUEsSUFBQSxnQkFBZ0IsQ0FBQyw2QkFBakIsR0FBaUQsbUJBQWpEO0FBQ0EsSUFBQSxnQkFBZ0IsQ0FBQyxlQUFqQixHQUFtQyxlQUFuQztBQUNBLElBQUEsZ0JBQWdCLENBQUMsU0FBakIsR0FBNkIsWUFBWSxDQUFDLFFBQWIsRUFBN0IsQ0FibUQsQ0FjbkQ7O0FBQ0EsUUFBSSxpQkFBaUIsQ0FBQyxTQUFsQixPQUFrQyxJQUF0QyxFQUE0QztBQUN4QyxNQUFBLGdCQUFnQixDQUFDLFNBQWpCLEdBQTZCLElBQTdCO0FBQ0EsTUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLFlBQVosRUFBMEIsZ0JBQTFCO0FBQ0gsS0FIRCxNQUdPLElBQUksaUJBQWlCLENBQUMsU0FBbEIsT0FBa0MsSUFBdEMsRUFBNEM7QUFDL0MsTUFBQSxnQkFBZ0IsQ0FBQyxNQUFqQixHQUEwQixZQUFZLENBQUMsa0JBQWIsRUFBMUI7QUFDQSxNQUFBLE1BQU0sQ0FBQyxJQUFQLENBQVksZ0JBQVosRUFBOEIsZ0JBQTlCO0FBQ0EsTUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLFlBQVosRUFBMEIsZ0JBQTFCO0FBQ0gsS0FKTSxNQUlBO0FBQ0gsTUFBQSxnQkFBZ0IsQ0FBQyxTQUFqQixHQUE2QixLQUE3QixDQURHLENBQ2lDOztBQUNwQyxNQUFBLGdCQUFnQixDQUFDLFNBQWpCLEdBQTZCLEtBQTdCO0FBQ0EsTUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLFlBQVosRUFBMEIsZ0JBQTFCO0FBQ0g7QUFDSjtBQUNEOzs7OztBQUdBLEVBQUEsTUFBTSxDQUFDLEVBQVAsQ0FBVSxXQUFWLEVBQXVCLFVBQVUsR0FBVixFQUFlO0FBQ2xDLFFBQU0sS0FBSyxHQUFHLEdBQUcsQ0FBQyxhQUFsQjtBQUNBLElBQUEsS0FBSyxDQUFDLGNBQU47QUFDQSxJQUFBLHNDQUFzQyxDQUFDLEtBQUssQ0FBQyxhQUFOLENBQW9CLENBQXBCLENBQUQsQ0FBdEM7QUFDSCxHQUpEO0FBS0E7Ozs7O0FBSUEsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLFdBQVYsRUFBdUIsVUFBVSxDQUFWLEVBQWE7QUFDaEMsSUFBQSxzQ0FBc0MsQ0FBQyxDQUFELENBQXRDO0FBQ0gsR0FGRDtBQUlBOzs7O0FBR0EsRUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLFlBQVosRUFBMEIsVUFBVSxDQUFWLEVBQWE7QUFDbkM7QUFDQSxJQUFBLE1BQU0sQ0FBQyxJQUFQLENBQVksOEJBQVo7QUFDSCxHQUhEO0FBSUE7Ozs7QUFHQSxFQUFBLE1BQU0sQ0FBQyxFQUFQLENBQVUsVUFBVixFQUFzQixVQUFVLEdBQVYsRUFBZTtBQUNqQyxRQUFNLEtBQUssR0FBRyxHQUFHLENBQUMsYUFBbEI7QUFDQSxJQUFBLEtBQUssQ0FBQyxjQUFOLEdBRmlDLENBR2pDO0FBRU47O0FBQ00sSUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLHdCQUFaLEVBQXNDLElBQXRDO0FBQ0gsR0FQRDtBQVNBOzs7O0FBR0EsRUFBQSxNQUFNLENBQUMsRUFBUCxDQUFVLFNBQVYsRUFBcUIsVUFBVSxDQUFWLEVBQWE7QUFFOUIsWUFBUSxDQUFDLENBQUMsS0FBVjtBQUFtQjtBQUNmLFdBQUssQ0FBTDtBQUNJO0FBQ0EsUUFBQSxNQUFNLENBQUMsSUFBUCxDQUFZLHNCQUFaO0FBQ0E7O0FBQ0osV0FBSyxDQUFMO0FBQ0k7QUFDQTs7QUFDSixXQUFLLENBQUw7QUFDSTtBQUNBOztBQUNKLGNBWEosQ0FZUTs7QUFaUjtBQWNILEdBaEJELEVBN1NVLENBK1RWOztBQUNBOzs7Ozs7Ozs7Ozs7O0FBZ0JILENBaFZBLENBQUQ7Ozs7O0FDeEJBLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxTQUFELENBQW5COztBQUVBLElBQUksaUJBQWlCLEdBQUcsT0FBTyxDQUFDLGdCQUFELENBQVAsQ0FBMEIsaUJBQWxELEMsQ0FDQTtBQUNBO0FBRUE7O0FBQ0E7Ozs7Ozs7QUFLQSxTQUFTLE1BQVQsQ0FBZ0IsT0FBaEIsRUFBeUIsT0FBekIsRUFBa0MsR0FBbEMsRUFBdUMsR0FBdkMsRUFBNEM7QUFFeEMsRUFBQSxLQUFLLENBQUMsSUFBTixDQUFXLElBQVgsRUFGd0MsQ0FFdEI7O0FBRWxCLE9BQUssSUFBTCxHQUFZLFVBQVUsT0FBVixFQUFtQixPQUFuQixFQUE0QixNQUE1QixFQUFvQztBQUU1QyxTQUFLLE9BQUwsR0FBZSxPQUFmO0FBQ0EsU0FBSyxPQUFMLEdBQWUsT0FBZjtBQUVBLFNBQUssTUFBTCxHQUFjLE1BQWQ7O0FBRUEsUUFBRyxPQUFPLE1BQVAsS0FBa0IsVUFBckIsRUFBZ0M7QUFDNUIsV0FBSyxJQUFMLEdBQVksSUFBSSxNQUFKLEVBQVo7QUFDSCxLQUZELE1BRU87QUFDSCxNQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksdUJBQVo7QUFDSDs7QUFFRCxTQUFLLElBQUwsQ0FBVSxHQUFWLENBQWMsS0FBSyxPQUFuQixFQUE0QixLQUFLLE9BQWpDLEVBQTBDLEtBQUssTUFBL0MsRUFBdUQsQ0FBdkQsRUFBMEQsSUFBSSxJQUFJLENBQUMsRUFBbkU7QUFDSCxHQWREOztBQWVBLE9BQUssSUFBTCxDQUFVLE9BQVYsRUFBbUIsT0FBbkIsRUFBNEIsR0FBNUIsRUFBaUMsR0FBakM7O0FBRUEsT0FBSyxJQUFMLEdBQVksVUFBVSxPQUFWLEVBQW1CO0FBQzNCLElBQUEsT0FBTyxDQUFDLFNBQVI7QUFDQSxJQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksS0FBSyxPQUFqQixFQUEwQixLQUFLLE9BQS9CLEVBQXdDLEtBQUssTUFBN0MsRUFBcUQsQ0FBckQsRUFBd0QsSUFBSSxJQUFJLENBQUMsRUFBakU7QUFFQSxJQUFBLE9BQU8sQ0FBQyxXQUFSLEdBQXNCLFNBQXRCOztBQUNBLFFBQUcsS0FBSyxjQUFMLE9BQTBCLElBQTdCLEVBQW1DO0FBQ2hDLE1BQUEsT0FBTyxDQUFDLFdBQVIsR0FBc0IsS0FBSyxjQUFMLEVBQXRCO0FBQ0Y7O0FBRUQsSUFBQSxPQUFPLENBQUMsTUFBUjtBQUNILEdBVkQ7QUFXQTs7Ozs7O0FBSUEsT0FBSyxPQUFMLEdBQWUsWUFBVTtBQUNyQixRQUFJLEtBQUssTUFBTCxHQUFjLENBQWxCLEVBQW9CO0FBQ2hCLGFBQU8sSUFBUDtBQUNIOztBQUNELFdBQU8sS0FBUDtBQUNILEdBTEQ7O0FBTUEsT0FBSyxXQUFMLEdBQW1CLFVBQVUsT0FBVixFQUFtQixDQUFuQixFQUFzQixDQUF0QixFQUF5QjtBQUN4QztBQUNBO0FBQ0EsUUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUwsQ0FBVyxDQUFDLEdBQUcsS0FBSyxPQUFwQixFQUE4QixDQUE5QixDQUFuQjtBQUNBLFFBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFMLENBQVcsQ0FBQyxHQUFHLEtBQUssT0FBcEIsRUFBOEIsQ0FBOUIsQ0FBcEI7QUFDQSxRQUFNLEdBQUcsR0FBRyxVQUFVLEdBQUcsV0FBekIsQ0FMd0MsQ0FLRjs7QUFDdEMsUUFBTSxHQUFHLEdBQUcsS0FBSyxNQUFMLEdBQWMsS0FBSyxNQUEvQixDQU53QyxDQU1EO0FBQ3ZDOztBQUNBLFFBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxHQUFMLENBQVMsR0FBRyxHQUFHLEdBQWYsQ0FBZDtBQUNBLFFBQU0sTUFBTSxHQUFHLEdBQWY7O0FBQ0EsUUFBSSxLQUFLLEdBQUcsTUFBWixFQUFtQjtBQUNmLGFBQU8sSUFBUDtBQUNIOztBQUNELFdBQU8sS0FBUCxDQWJ3QyxDQWN4QztBQUNILEdBZkQ7O0FBaUJBLE9BQUssWUFBTCxHQUFvQixVQUFVLE9BQVYsRUFBbUI7QUFFbkMsSUFBQSxXQUFXLEdBQUcsRUFBZCxDQUZtQyxDQUduQzs7QUFDQSxJQUFBLE9BQU8sQ0FBQyxTQUFSLENBQWtCLGFBQWxCLEVBQWlDLEtBQUssT0FBTCxHQUFlLENBQWhELEVBQW1ELEtBQUssT0FBTCxHQUFlLENBQWxFO0FBQ0EsUUFBSSxNQUFNLEdBQUcsRUFBYjtBQUNBLElBQUEsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsRUFBaEI7QUFDQSxJQUFBLE1BQU0sQ0FBQyxNQUFQLENBQWMsQ0FBZCxHQUFrQixLQUFLLE9BQXZCO0FBQ0EsSUFBQSxNQUFNLENBQUMsTUFBUCxDQUFjLENBQWQsR0FBa0IsS0FBSyxPQUF2QjtBQUNBLElBQUEsV0FBVyxDQUFDLElBQVosQ0FBaUIsTUFBakIsRUFUbUMsQ0FXbkM7O0FBQ0EsU0FBSSxJQUFJLENBQUMsR0FBQyxDQUFWLEVBQWEsQ0FBQyxHQUFHLENBQWpCLEVBQW9CLENBQUMsR0FBQyxDQUFDLEdBQUMsR0FBeEIsRUFBNEI7QUFDeEIsVUFBSSxNQUFNLEdBQUcsRUFBYjtBQUNBLFVBQUksS0FBSyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBckI7QUFDQSxNQUFBLE1BQU0sQ0FBQyxDQUFQLEdBQVcsSUFBSSxDQUFDLEtBQUwsQ0FBWSxLQUFLLE9BQUwsR0FBZSxLQUFLLE1BQUwsR0FBYyxJQUFJLENBQUMsR0FBTCxDQUFVLEtBQVYsQ0FBekMsQ0FBWDtBQUNBLE1BQUEsTUFBTSxDQUFDLENBQVAsR0FBVyxJQUFJLENBQUMsS0FBTCxDQUFZLEtBQUssT0FBTCxHQUFlLEtBQUssTUFBTCxHQUFjLElBQUksQ0FBQyxHQUFMLENBQVUsS0FBVixDQUF6QyxDQUFYO0FBRUEsTUFBQSxPQUFPLENBQUMsU0FBUixDQUFrQixhQUFsQixFQUFpQyxNQUFNLENBQUMsQ0FBUCxHQUFXLENBQTVDLEVBQStDLE1BQU0sQ0FBQyxDQUFQLEdBQVcsQ0FBMUQ7QUFDQSxNQUFBLE1BQU0sR0FBRyxFQUFUO0FBQ0EsTUFBQSxNQUFNLENBQUMsTUFBUCxHQUFnQixFQUFoQjtBQUNBLE1BQUEsTUFBTSxDQUFDLE1BQVAsQ0FBYyxDQUFkLEdBQWtCLE1BQU0sQ0FBQyxDQUF6QjtBQUNBLE1BQUEsTUFBTSxDQUFDLE1BQVAsQ0FBYyxDQUFkLEdBQWtCLE1BQU0sQ0FBQyxDQUF6QjtBQUNBLE1BQUEsV0FBVyxDQUFDLElBQVosQ0FBaUIsTUFBakI7QUFDSDs7QUFHRCxJQUFBLE9BQU8sQ0FBQyxTQUFSO0FBQ0EsSUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLEtBQUssT0FBakIsRUFBMEIsS0FBSyxPQUEvQixFQUF3QyxLQUFLLE1BQTdDLEVBQXFELENBQXJELEVBQXdELElBQUksSUFBSSxDQUFDLEVBQWpFO0FBRUEsSUFBQSxPQUFPLENBQUMsV0FBUixHQUFzQixTQUF0QjtBQUNBLElBQUEsT0FBTyxDQUFDLE1BQVI7QUFDQSxJQUFBLE9BQU8sQ0FBQyxXQUFSLEdBQXNCLFNBQXRCO0FBQ0gsR0FqQ0Q7O0FBbUNBLE9BQUssTUFBTCxHQUFjLFlBQVU7QUFDcEI7QUFDQSxRQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsU0FBUCxDQUFpQixNQUFqQixDQUF3QixJQUF4QixDQUE2QixJQUE3QixDQUFaO0FBQ0EsSUFBQSxHQUFHLENBQUMsSUFBSixHQUFXLFFBQVg7QUFFQSxJQUFBLEdBQUcsQ0FBQyxPQUFKLEdBQWMsS0FBSyxPQUFuQjtBQUNBLElBQUEsR0FBRyxDQUFDLE9BQUosR0FBYyxLQUFLLE9BQW5CO0FBRUEsSUFBQSxHQUFHLENBQUMsTUFBSixHQUFhLEtBQUssTUFBbEI7QUFFQSxJQUFBLEdBQUcsQ0FBQyxXQUFKLEdBQWtCLEtBQUssY0FBTCxFQUFsQjtBQUVBLFdBQU8sR0FBUDtBQUVILEdBZEQ7O0FBZ0JBLE9BQUssUUFBTCxHQUFnQixZQUFVO0FBQ3RCLFFBQUkscUJBQXFCLEdBQUcsR0FBNUI7QUFFQSxRQUFJLE1BQU0sR0FBRyw0Q0FDTCwwQkFESyxHQUVMLFdBRkssR0FFUyxJQUFJLENBQUMsS0FBTCxDQUFXLEtBQUssT0FBTCxHQUFlLHFCQUExQixJQUFtRCxxQkFGNUQsR0FHTCxpQkFISyxHQUdlLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxPQUFMLEdBQWUscUJBQTFCLElBQW1ELHFCQUhsRSxHQUlMLGdCQUpLLEdBSWMsSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLE1BQUwsR0FBYyxxQkFBekIsSUFBa0QscUJBSmhFLEdBS0wsUUFMUjtBQU9BLFdBQU8sTUFBUDtBQUNILEdBWEQ7QUFZQTs7Ozs7QUFHQSxPQUFLLFlBQUwsR0FBb0IsWUFBVTtBQUMxQixRQUFJLHFCQUFxQixHQUFHLEdBQTVCLENBRDBCLENBRTFCOztBQUNBLFFBQUksTUFBTSxHQUFHLDZCQUNMLGdDQURLLEdBRUQseUNBRkMsR0FHRCwwQkFIQyxHQUlHLDBDQUpILEdBS08sNkRBTFAsR0FNTyx3SEFOUCxHQU9PLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxPQUFMLEdBQWUscUJBQTFCLElBQW1ELHFCQVAxRCxHQU9rRixJQVBsRixHQVFHLFFBUkgsR0FTRywwQ0FUSCxHQVVPLDZEQVZQLEdBV08sd0hBWFAsR0FZTyxJQUFJLENBQUMsS0FBTCxDQUFXLEtBQUssT0FBTCxHQUFlLHFCQUExQixJQUFtRCxxQkFaMUQsR0FZa0YsSUFabEYsR0FhRyxRQWJILEdBY0QsUUFkQyxHQWVMLFFBZkssR0FpQkwsMENBakJLLEdBa0JELGtFQWxCQyxHQW1CRCxxSEFuQkMsR0FvQkQsSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLE1BQUwsR0FBYyxxQkFBekIsSUFBa0QscUJBcEJqRCxHQW9CeUUsSUFwQnpFLEdBcUJMLFFBckJLLEdBdUJMLDBDQXZCSyxHQXdCRCx3RUF4QkMsR0F5QkQsNEhBekJDLEdBMEJELEtBQUssY0FBTCxFQTFCQyxHQTBCdUIsSUExQnZCLEdBMkJMLFFBM0JSO0FBNkJRO0FBRVIsV0FBTyxNQUFQO0FBQ0gsR0FuQ0Q7O0FBcUNBLE9BQUssTUFBTCxHQUFjLFVBQVUsWUFBVixFQUF3QixZQUF4QixFQUFxQztBQUMvQyxTQUFLLE1BQUwsR0FBYyxpQkFBaUIsQ0FBQyxLQUFLLE9BQU4sRUFBZSxLQUFLLE9BQXBCLEVBQTZCLFlBQTdCLEVBQTJDLFlBQTNDLENBQS9CO0FBQ0gsR0FGRDtBQUdIOztBQUNELE1BQU0sQ0FBQyxTQUFQLEdBQW1CLE1BQU0sQ0FBQyxNQUFQLENBQWMsS0FBSyxDQUFDLFNBQXBCLEVBQStCO0FBQzlDLEVBQUEsV0FBVyxFQUFFO0FBQ1QsSUFBQSxLQUFLLEVBQUU7QUFERTtBQURpQyxDQUEvQixDQUFuQjtBQU1BLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLE1BQWpCOzs7OztBQ3hMQSxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsU0FBRCxDQUFuQjtBQUNBOzs7Ozs7O0FBS0EsU0FBUyxRQUFULENBQWtCLE1BQWxCLEVBQTBCO0FBRXRCLEVBQUEsS0FBSyxDQUFDLElBQU4sQ0FBVyxJQUFYLEVBRnNCLENBRUo7O0FBRWxCLE9BQUssSUFBTCxHQUFZLFVBQVUsTUFBVixFQUFrQjtBQUUxQixTQUFLLE1BQUwsR0FBYyxNQUFkOztBQUNBLFFBQUcsT0FBTyxNQUFQLEtBQWtCLFVBQXJCLEVBQWdDO0FBQzVCLFdBQUssSUFBTCxHQUFZLElBQUksTUFBSixFQUFaO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsTUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHVCQUFaO0FBQ0g7O0FBQ0QsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFiLEVBQWdCLENBQUMsR0FBRyxLQUFLLE1BQUwsQ0FBWSxNQUFaLEdBQXFCLENBQXpDLEVBQTRDLENBQUMsRUFBN0MsRUFBaUQ7QUFDN0MsV0FBSyxJQUFMLENBQVUsTUFBVixDQUFpQixLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsQ0FBaEMsRUFBbUMsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLENBQWxEO0FBQ0EsV0FBSyxJQUFMLENBQVUsTUFBVixDQUFpQixLQUFLLE1BQUwsQ0FBWSxDQUFDLEdBQUcsQ0FBaEIsRUFBbUIsQ0FBcEMsRUFBdUMsS0FBSyxNQUFMLENBQVksQ0FBQyxHQUFHLENBQWhCLEVBQW1CLENBQTFEO0FBQ0g7QUFDSixHQVpEOztBQWFBLE9BQUssSUFBTCxDQUFVLE1BQVY7O0FBRUEsT0FBSyxJQUFMLEdBQVksVUFBVSxPQUFWLEVBQW1CO0FBRTNCLElBQUEsT0FBTyxDQUFDLFNBQVI7O0FBQ0EsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFiLEVBQWdCLENBQUMsR0FBRyxLQUFLLE1BQUwsQ0FBWSxNQUFaLEdBQXFCLENBQXpDLEVBQTRDLENBQUMsRUFBN0MsRUFBaUQ7QUFDN0MsTUFBQSxPQUFPLENBQUMsTUFBUixDQUFlLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxDQUE5QixFQUFpQyxLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsQ0FBaEQ7QUFDQSxNQUFBLE9BQU8sQ0FBQyxNQUFSLENBQWUsS0FBSyxNQUFMLENBQVksQ0FBQyxHQUFHLENBQWhCLEVBQW1CLENBQWxDLEVBQXFDLEtBQUssTUFBTCxDQUFZLENBQUMsR0FBRyxDQUFoQixFQUFtQixDQUF4RDtBQUNIOztBQUNELElBQUEsT0FBTyxDQUFDLFdBQVIsR0FBc0IsU0FBdEI7O0FBQ0EsUUFBRyxLQUFLLGNBQUwsT0FBMEIsSUFBN0IsRUFBbUM7QUFDaEMsTUFBQSxPQUFPLENBQUMsV0FBUixHQUFzQixLQUFLLGNBQUwsRUFBdEI7QUFDRjs7QUFDRCxJQUFBLE9BQU8sQ0FBQyxNQUFSO0FBRUgsR0FiRDtBQWNBOzs7Ozs7O0FBS0EsT0FBSyxPQUFMLEdBQWUsWUFBVTtBQUNyQixRQUFJLEtBQUssTUFBTCxDQUFZLE1BQVosR0FBcUIsQ0FBekIsRUFBMkI7QUFDdkIsYUFBTyxJQUFQO0FBQ0gsS0FGRCxNQUVPLElBQUcsS0FBSyxNQUFMLENBQVksTUFBWixLQUF1QixDQUExQixFQUE0QjtBQUMvQixVQUFJLElBQUksQ0FBQyxHQUFMLENBQVMsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLENBQWYsR0FBbUIsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLENBQTNDLElBQWdELENBQWhELElBQXFELElBQUksQ0FBQyxHQUFMLENBQVMsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLENBQWYsR0FBbUIsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLENBQTNDLElBQWdELENBQXpHLEVBQTJHO0FBQ3ZHLGVBQU8sSUFBUDtBQUNIO0FBQ0o7O0FBQ0QsV0FBTyxLQUFQO0FBQ0gsR0FURDs7QUFVQSxPQUFLLFdBQUwsR0FBbUIsVUFBVSxPQUFWLEVBQW1CLENBQW5CLEVBQXNCLENBQXRCLEVBQXlCO0FBQ3hDLFdBQU8sT0FBTyxDQUFDLGVBQVIsQ0FBd0IsS0FBSyxJQUE3QixFQUFtQyxDQUFuQyxFQUFzQyxDQUF0QyxDQUFQLENBRHdDLENBRXhDO0FBQ0gsR0FIRDs7QUFLQSxPQUFLLFlBQUwsR0FBb0IsVUFBVSxPQUFWLEVBQW1CO0FBRW5DLElBQUEsV0FBVyxHQUFHLEVBQWQ7QUFFQSxJQUFBLE9BQU8sQ0FBQyxTQUFSLENBQWtCLGFBQWxCLEVBQWlDLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxDQUFmLEdBQW1CLENBQXBELEVBQXVELEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxDQUFmLEdBQW1CLENBQTFFO0FBQ0EsUUFBSSxNQUFNLEdBQUcsRUFBYjtBQUNBLElBQUEsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsRUFBaEI7QUFDQSxJQUFBLE1BQU0sQ0FBQyxNQUFQLENBQWMsQ0FBZCxHQUFrQixLQUFLLE9BQXZCO0FBQ0EsSUFBQSxNQUFNLENBQUMsTUFBUCxDQUFjLENBQWQsR0FBa0IsS0FBSyxPQUF2QjtBQUNBLElBQUEsV0FBVyxDQUFDLElBQVosQ0FBaUIsTUFBakI7QUFFQSxJQUFBLE9BQU8sQ0FBQyxTQUFSLENBQWtCLGFBQWxCLEVBQWlDLEtBQUssTUFBTCxDQUFZLEtBQUssTUFBTCxDQUFZLE1BQVosR0FBbUIsQ0FBL0IsRUFBa0MsQ0FBbEMsR0FBc0MsQ0FBdkUsRUFBMEUsS0FBSyxNQUFMLENBQVksS0FBSyxNQUFMLENBQVksTUFBWixHQUFtQixDQUEvQixFQUFrQyxDQUFsQyxHQUFzQyxDQUFoSDtBQUNBLElBQUEsTUFBTSxHQUFHLEVBQVQ7QUFDQSxJQUFBLE1BQU0sQ0FBQyxNQUFQLEdBQWdCLEVBQWhCO0FBQ0EsSUFBQSxNQUFNLENBQUMsTUFBUCxDQUFjLENBQWQsR0FBa0IsS0FBSyxJQUF2QjtBQUNBLElBQUEsTUFBTSxDQUFDLE1BQVAsQ0FBYyxDQUFkLEdBQWtCLEtBQUssSUFBdkI7QUFDQSxJQUFBLFdBQVcsQ0FBQyxJQUFaLENBQWlCLE1BQWpCO0FBRUEsSUFBQSxPQUFPLENBQUMsU0FBUjs7QUFDQSxTQUFLLElBQUksQ0FBQyxHQUFHLENBQWIsRUFBZ0IsQ0FBQyxHQUFHLEtBQUssTUFBTCxDQUFZLE1BQVosR0FBcUIsQ0FBekMsRUFBNEMsQ0FBQyxFQUE3QyxFQUFpRDtBQUM3QyxNQUFBLE9BQU8sQ0FBQyxNQUFSLENBQWUsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLENBQTlCLEVBQWlDLEtBQUssTUFBTCxDQUFZLENBQVosRUFBZSxDQUFoRDtBQUNBLE1BQUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxLQUFLLE1BQUwsQ0FBWSxDQUFDLEdBQUcsQ0FBaEIsRUFBbUIsQ0FBbEMsRUFBcUMsS0FBSyxNQUFMLENBQVksQ0FBQyxHQUFHLENBQWhCLEVBQW1CLENBQXhEO0FBQ0g7O0FBQ0QsSUFBQSxPQUFPLENBQUMsV0FBUixHQUFzQixTQUF0QjtBQUNBLElBQUEsT0FBTyxDQUFDLE1BQVI7QUFDQSxJQUFBLE9BQU8sQ0FBQyxXQUFSLEdBQXNCLFNBQXRCO0FBQ0gsR0ExQkQ7O0FBNEJBLE9BQUssTUFBTCxHQUFjLFlBQVU7QUFFcEI7QUFDQSxRQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBVCxDQUFtQixNQUFuQixDQUEwQixJQUExQixDQUErQixJQUEvQixDQUFaO0FBRUEsSUFBQSxHQUFHLENBQUMsSUFBSixHQUFXLFVBQVg7QUFFQSxJQUFBLEdBQUcsQ0FBQyxNQUFKLEdBQWEsS0FBSyxNQUFsQjtBQUVBLElBQUEsR0FBRyxDQUFDLFdBQUosR0FBa0IsS0FBSyxjQUFMLEVBQWxCO0FBRUEsV0FBTyxHQUFQO0FBQ0gsR0FaRDs7QUFhQSxPQUFLLFFBQUwsR0FBZ0IsWUFBVTtBQUN0QixRQUFJLHFCQUFxQixHQUFHLEdBQTVCO0FBRUEsUUFBSSxNQUFNLEdBQUcsOENBQ0wsMEJBREssR0FFTCxXQUZLLEdBRVMsS0FBSyxNQUFMLENBQVksTUFGckIsR0FHTCxnQkFISyxHQUdjLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxNQUFMLENBQVksQ0FBWixFQUFlLENBQWYsR0FBbUIscUJBQTlCLElBQXVELHFCQUhyRSxHQUlKLGdCQUpJLEdBSWUsSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLE1BQUwsQ0FBWSxDQUFaLEVBQWUsQ0FBZixHQUFtQixxQkFBOUIsSUFBdUQscUJBSnRFLEdBS0wsbUJBTEssR0FLaUIsSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLE1BQUwsQ0FBWSxLQUFLLE1BQUwsQ0FBWSxNQUFaLEdBQW1CLENBQS9CLEVBQWtDLENBQWxDLEdBQXNDLHFCQUFqRCxJQUEwRSxxQkFMM0YsR0FNTCxtQkFOSyxHQU1pQixJQUFJLENBQUMsS0FBTCxDQUFXLEtBQUssTUFBTCxDQUFZLEtBQUssTUFBTCxDQUFZLE1BQVosR0FBbUIsQ0FBL0IsRUFBa0MsQ0FBbEMsR0FBc0MscUJBQWpELElBQTBFLHFCQU4zRixHQU9MLFFBUFI7QUFTQSxXQUFPLE1BQVA7QUFFSCxHQWREO0FBZ0JBOzs7OztBQUdBLE9BQUssWUFBTCxHQUFvQixZQUFVO0FBQzFCLFFBQUkscUJBQXFCLEdBQUcsR0FBNUIsQ0FEMEIsQ0FFMUI7O0FBQ0EsUUFBSSxNQUFNLEdBQUcsK0NBQStDLElBQUksQ0FBQyxTQUFMLENBQWUsS0FBSyxNQUFwQixDQUEvQyxHQUE2RSxNQUE3RSxHQUdMLGdGQUhLLEdBRzhFLEtBQUssTUFBTCxDQUFZLE1BSDFGLEdBR21HLHdDQUhuRyxHQU1MLDBDQU5LLEdBT0Qsd0VBUEMsR0FRRCw0SEFSQyxHQVNELEtBQUssY0FBTCxFQVRDLEdBU3VCLElBVHZCLEdBVUwsUUFWUjtBQVlRO0FBRVIsV0FBTyxNQUFQO0FBQ0gsR0FsQkQ7O0FBb0JBLE9BQUssTUFBTCxHQUFjLFVBQVUsWUFBVixFQUF3QixZQUF4QixFQUFxQztBQUMvQyxRQUFJLFFBQVEsR0FBRyxFQUFmO0FBQ0EsSUFBQSxRQUFRLENBQUMsQ0FBVCxHQUFhLFlBQWI7QUFDQSxJQUFBLFFBQVEsQ0FBQyxDQUFULEdBQWEsWUFBYjtBQUNBLFNBQUssTUFBTCxDQUFZLElBQVosQ0FBaUIsUUFBakI7QUFDSCxHQUxEO0FBTUgsQyxDQUNEOzs7QUFDQSxRQUFRLENBQUMsU0FBVCxHQUFxQixNQUFNLENBQUMsTUFBUCxDQUFjLEtBQUssQ0FBQyxTQUFwQixFQUErQjtBQUNoRCxFQUFBLFdBQVcsRUFBRTtBQUNULElBQUEsS0FBSyxFQUFFO0FBREU7QUFEbUMsQ0FBL0IsQ0FBckI7QUFNQSxNQUFNLENBQUMsT0FBUCxHQUFpQixRQUFqQjs7O0FDekpBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsU0FBRCxDQUFuQixDLENBRUE7OztBQUNBLElBQUksV0FBVyxHQUFHLE9BQU8sQ0FBQyxnQkFBRCxDQUFQLENBQTBCLFdBQTVDLEMsQ0FDQTtBQUNBOztBQUVBOzs7Ozs7O0lBS00sSTs7Ozs7QUFFRixnQkFBWSxNQUFaLEVBQW9CLE1BQXBCLEVBQTRCLElBQTVCLEVBQWtDLElBQWxDLEVBQXVDO0FBQUE7O0FBQUE7O0FBQ25DO0FBRUEsVUFBSyxPQUFMLEdBQWUsTUFBZjtBQUNBLFVBQUssT0FBTCxHQUFlLE1BQWY7O0FBQ0EsUUFBRyxPQUFPLE1BQVAsS0FBa0IsVUFBckIsRUFBZ0M7QUFDNUIsWUFBSyxJQUFMLEdBQVksSUFBSSxNQUFKLEVBQVo7QUFDSCxLQUZELE1BRU87QUFDSCxNQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksdUJBQVo7QUFDSDs7QUFFRCxVQUFLLElBQUwsR0FBWSxJQUFaO0FBQ0EsVUFBSyxJQUFMLEdBQVksSUFBWjs7QUFFQSxVQUFLLElBQUwsQ0FBVSxNQUFWLENBQWlCLE1BQUssT0FBdEIsRUFBK0IsTUFBSyxPQUFwQzs7QUFDQSxVQUFLLElBQUwsQ0FBVSxNQUFWLENBQWlCLE1BQUssSUFBdEIsRUFBNEIsTUFBSyxJQUFqQzs7QUFmbUM7QUFpQnRDO0FBQ0Q7Ozs7Ozs7OzhCQUlVO0FBQ04sVUFBSyxJQUFJLENBQUMsR0FBTCxDQUFTLEtBQUssT0FBTCxHQUFlLEtBQUssSUFBN0IsSUFBcUMsQ0FBckMsSUFBMEMsSUFBSSxDQUFDLEdBQUwsQ0FBUyxLQUFLLE9BQUwsR0FBZSxLQUFLLElBQTdCLElBQXFDLENBQXBGLEVBQXNGO0FBQ2xGLGVBQU8sSUFBUDtBQUNIOztBQUNELGFBQU8sS0FBUDtBQUNIO0FBQ0Q7Ozs7Ozs7eUJBSU0sTyxFQUFTLFMsRUFBVztBQUV0QixNQUFBLE9BQU8sQ0FBQyxXQUFSLEdBQXNCLFNBQXRCOztBQUVBLFVBQUcsS0FBSyxjQUFMLE9BQTBCLElBQTdCLEVBQW1DO0FBQ2hDLFFBQUEsT0FBTyxDQUFDLFdBQVIsR0FBc0IsS0FBSyxjQUFMLEVBQXRCO0FBQ0Y7O0FBRUQsVUFBSSxLQUFLLEdBQUcsS0FBSyxPQUFqQjtBQUNBLFVBQUksS0FBSyxHQUFHLEtBQUssSUFBakI7O0FBQ0EsVUFBRyxLQUFLLEtBQUssS0FBYixFQUFtQjtBQUFFO0FBQ2pCLFFBQUEsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBWCxJQUFrQixHQUExQjtBQUNBLFFBQUEsS0FBSyxHQUFHLEtBQVI7QUFDSDs7QUFDRCxVQUFJLEtBQUssR0FBRyxLQUFLLE9BQWpCO0FBQ0EsVUFBSSxLQUFLLEdBQUcsS0FBSyxJQUFqQjs7QUFFQSxVQUFHLEtBQUssS0FBSyxLQUFiLEVBQW1CO0FBQUU7QUFDakIsUUFBQSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFYLElBQWtCLEdBQTFCO0FBQ0EsUUFBQSxLQUFLLEdBQUcsS0FBUjtBQUNIOztBQUVELE1BQUEsT0FBTyxDQUFDLFNBQVI7O0FBRUEsVUFBRyxTQUFILEVBQWE7QUFDVixRQUFBLE9BQU8sQ0FBQyxTQUFSLEdBQW9CLFNBQXBCO0FBQ0Y7O0FBQ0QsTUFBQSxPQUFPLENBQUMsTUFBUixDQUFlLEtBQWYsRUFBc0IsS0FBdEI7QUFDQSxNQUFBLE9BQU8sQ0FBQyxNQUFSLENBQWUsS0FBZixFQUFzQixLQUF0Qjs7QUFFQSxVQUFHLEtBQUssS0FBTCxLQUFlLElBQWYsSUFBdUIsUUFBTyxLQUFLLEtBQVosTUFBc0IsU0FBaEQsRUFBMEQ7QUFDdEQsWUFBRyxLQUFLLG1CQUFMLE9BQStCLFVBQWxDLEVBQTZDO0FBQ3pDOzs7Ozs7OztBQVFBLFVBQUEsT0FBTyxDQUFDLFNBQVIsQ0FBa0IsSUFBbEI7QUFDQSxVQUFBLE9BQU8sQ0FBQyxJQUFSLEdBQWUsd0JBQWY7QUFDQSxVQUFBLE9BQU8sQ0FBQyxTQUFSLENBQWtCLFdBQWxCLENBQThCLENBQTlCLEVBQWlDLElBQUUsS0FBSyxhQUFMLEVBQW5DLEVBQXlELEtBQXpELEVBQWlFLEtBQWpFO0FBQ0EsVUFBQSxPQUFPLENBQUMsV0FBUixHQUFzQixPQUF0QjtBQUNBLFVBQUEsT0FBTyxDQUFDLFNBQVIsR0FBb0IsQ0FBcEI7QUFDQSxVQUFBLE9BQU8sQ0FBQyxRQUFSLENBQWlCLEtBQUssS0FBdEIsRUFBNkIsS0FBSyxHQUFHLENBQXJDLEVBQXdDLEtBQUssR0FBRyxDQUFoRDtBQUNBLFVBQUEsT0FBTyxDQUFDLFNBQVIsQ0FBa0IsT0FBbEI7QUFDSCxTQWhCRCxNQWdCTztBQUNILFVBQUEsT0FBTyxDQUFDLFNBQVIsQ0FBa0IsSUFBbEI7QUFDQSxVQUFBLE9BQU8sQ0FBQyxJQUFSLEdBQWUsd0JBQWY7QUFDQSxVQUFBLE9BQU8sQ0FBQyxTQUFSLENBQWtCLFdBQWxCLENBQThCLElBQUUsS0FBSyxhQUFMLEVBQWhDLEVBQXNELENBQXRELEVBQXlELEtBQXpELEVBQWdFLEtBQWhFO0FBQ0EsVUFBQSxPQUFPLENBQUMsV0FBUixHQUFzQixPQUF0QjtBQUNBLFVBQUEsT0FBTyxDQUFDLFNBQVIsR0FBb0IsQ0FBcEI7QUFDQSxVQUFBLE9BQU8sQ0FBQyxRQUFSLENBQWlCLEtBQUssS0FBdEIsRUFBNkIsS0FBN0IsRUFBb0MsS0FBSyxHQUFHLENBQTVDO0FBQ0EsVUFBQSxPQUFPLENBQUMsU0FBUixDQUFrQixPQUFsQjtBQUNIO0FBQ0o7O0FBQ0QsTUFBQSxPQUFPLENBQUMsTUFBUjtBQUNIOzs7Z0NBRVcsTyxFQUFTLEMsRUFBRyxDLEVBQUc7QUFDdkI7QUFFQSxVQUFJLFlBQVksR0FBRyxXQUFXLENBQUMsQ0FBRCxFQUFJLENBQUosRUFBTyxLQUFLLE9BQVosRUFBcUIsS0FBSyxPQUExQixFQUFtQyxLQUFLLElBQXhDLEVBQThDLEtBQUssSUFBbkQsQ0FBOUIsQ0FIdUIsQ0FLdkI7O0FBQ0EsVUFBTSxNQUFNLEdBQUcsR0FBZjs7QUFDQSxVQUVLLFlBQVksR0FBRyxNQUFmLElBQXlCLFlBQVksR0FBRyxDQUFDLE1BQTFDLEtBRUssS0FBSyxPQUFMLElBQWdCLENBQWhCLElBQXFCLENBQUMsSUFBSSxLQUFLLElBQWhDLElBQ0MsS0FBSyxJQUFMLElBQWEsQ0FBYixJQUFrQixDQUFDLElBQUksS0FBSyxPQUhqQyxNQU1LLEtBQUssT0FBTCxJQUFnQixDQUFoQixJQUFxQixDQUFDLElBQUksS0FBSyxJQUFoQyxJQUNDLEtBQUssSUFBTCxJQUFhLENBQWIsSUFBa0IsQ0FBQyxJQUFJLEtBQUssT0FQakMsQ0FGSixFQVlBO0FBQ0ksZUFBTyxJQUFQO0FBQ0g7O0FBQ0QsYUFBTyxLQUFQO0FBRUg7OztpQ0FDYSxPLEVBQVM7QUFFbkIsTUFBQSxPQUFPLENBQUMsU0FBUjtBQUNBLE1BQUEsV0FBVyxHQUFHLEVBQWQsQ0FIbUIsQ0FJbkI7O0FBQ0EsTUFBQSxPQUFPLENBQUMsU0FBUixDQUFrQixhQUFsQixFQUFpQyxLQUFLLE9BQUwsR0FBZSxDQUFoRCxFQUFtRCxLQUFLLE9BQUwsR0FBZSxDQUFsRTtBQUNBLFVBQUksTUFBTSxHQUFHLEVBQWI7QUFDQSxNQUFBLE1BQU0sQ0FBQyxNQUFQLEdBQWdCLEVBQWhCO0FBQ0EsTUFBQSxNQUFNLENBQUMsTUFBUCxDQUFjLENBQWQsR0FBa0IsS0FBSyxPQUF2QjtBQUNBLE1BQUEsTUFBTSxDQUFDLE1BQVAsQ0FBYyxDQUFkLEdBQWtCLEtBQUssT0FBdkI7QUFDQSxNQUFBLFdBQVcsQ0FBQyxJQUFaLENBQWlCLE1BQWpCO0FBRUEsTUFBQSxPQUFPLENBQUMsU0FBUixDQUFrQixhQUFsQixFQUFpQyxLQUFLLElBQUwsR0FBWSxDQUE3QyxFQUFnRCxLQUFLLElBQUwsR0FBWSxDQUE1RDtBQUNBLE1BQUEsTUFBTSxHQUFHLEVBQVQ7QUFDQSxNQUFBLE1BQU0sQ0FBQyxNQUFQLEdBQWdCLEVBQWhCO0FBQ0EsTUFBQSxNQUFNLENBQUMsTUFBUCxDQUFjLENBQWQsR0FBa0IsS0FBSyxJQUF2QjtBQUNBLE1BQUEsTUFBTSxDQUFDLE1BQVAsQ0FBYyxDQUFkLEdBQWtCLEtBQUssSUFBdkI7QUFDQSxNQUFBLFdBQVcsQ0FBQyxJQUFaLENBQWlCLE1BQWpCLEVBakJtQixDQWtCbkI7QUFDQTtBQUVBOztBQUNBLE1BQUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxLQUFLLE9BQXBCLEVBQTZCLEtBQUssT0FBbEM7QUFDQSxNQUFBLE9BQU8sQ0FBQyxNQUFSLENBQWUsS0FBSyxJQUFwQixFQUEwQixLQUFLLElBQS9CO0FBQ0EsTUFBQSxPQUFPLENBQUMsV0FBUixHQUFzQixTQUF0QjtBQUNBLE1BQUEsT0FBTyxDQUFDLE1BQVI7QUFDQSxNQUFBLE9BQU8sQ0FBQyxXQUFSLEdBQXNCLFNBQXRCO0FBQ0g7Ozs2QkFFUTtBQUNMO0FBQ0E7QUFDQSxVQUFNLEdBQUcsbUVBQVQ7O0FBRUEsTUFBQSxHQUFHLENBQUMsSUFBSixHQUFXLE1BQVg7QUFFQSxNQUFBLEdBQUcsQ0FBQyxPQUFKLEdBQWMsS0FBSyxPQUFuQjtBQUNBLE1BQUEsR0FBRyxDQUFDLE9BQUosR0FBYyxLQUFLLE9BQW5CO0FBRUEsTUFBQSxHQUFHLENBQUMsSUFBSixHQUFXLEtBQUssSUFBaEI7QUFDQSxNQUFBLEdBQUcsQ0FBQyxJQUFKLEdBQVcsS0FBSyxJQUFoQjtBQUVBLGFBQU8sR0FBUDtBQUNIOzs7K0JBRVU7QUFDUCxVQUFJLHFCQUFxQixHQUFHLEdBQTVCO0FBRUEsVUFBSSxNQUFNLEdBQUcsMENBQ0wsMEJBREssR0FFTCxTQUZLLEdBRU8sSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLE9BQUwsR0FBZSxxQkFBMUIsSUFBbUQscUJBRjFELEdBR0wsZUFISyxHQUdhLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxPQUFMLEdBQWUscUJBQTFCLElBQW1ELHFCQUhoRSxHQUlMLGNBSkssR0FJWSxJQUFJLENBQUMsS0FBTCxDQUFXLEtBQUssSUFBTCxHQUFZLHFCQUF2QixJQUFnRCxxQkFKNUQsR0FLTCxjQUxLLEdBS1ksSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLElBQUwsR0FBWSxxQkFBdkIsSUFBZ0QscUJBTDVELEdBTUwsUUFOUjtBQVFBLGFBQU8sTUFBUDtBQUNIO0FBQ0Q7Ozs7OzttQ0FHZTtBQUNYLFVBQUkscUJBQXFCLEdBQUcsR0FBNUIsQ0FEVyxDQUVYOztBQUNBLFVBQUksTUFBTSxHQUFHLDZCQUNMLGdDQURLLEdBRUQseUNBRkMsR0FHRCwwQkFIQyxHQUlHLDBDQUpILEdBS08sNkRBTFAsR0FNTyx3SEFOUCxHQU9PLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxPQUFMLEdBQWUscUJBQTFCLElBQW1ELHFCQVAxRCxHQU9rRixJQVBsRixHQVFHLFFBUkgsR0FTRywwQ0FUSCxHQVVPLDZEQVZQLEdBV08sd0hBWFAsR0FZTyxJQUFJLENBQUMsS0FBTCxDQUFXLEtBQUssT0FBTCxHQUFlLHFCQUExQixJQUFtRCxxQkFaMUQsR0FZa0YsSUFabEYsR0FhRyxRQWJILEdBY0QsUUFkQyxHQWVMLFFBZkssR0FtQkwsZ0NBbkJLLEdBb0JELHNDQXBCQyxHQXFCRCwwQkFyQkMsR0FzQkcsMENBdEJILEdBdUJPLDZEQXZCUCxHQXdCTyxxSEF4QlAsR0F5Qk8sSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLElBQUwsR0FBWSxxQkFBdkIsSUFBZ0QscUJBekJ2RCxHQXlCK0UsSUF6Qi9FLEdBMEJHLFFBMUJILEdBMkJHLDBDQTNCSCxHQTRCTyw2REE1QlAsR0E2Qk8scUhBN0JQLEdBOEJPLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxJQUFMLEdBQVkscUJBQXZCLElBQWdELHFCQTlCdkQsR0E4QitFLElBOUIvRSxHQStCRyxRQS9CSCxHQWdDRCxRQWhDQyxHQWlDTCxRQWpDSyxHQW1DTCwwQ0FuQ0ssR0FvQ0Qsd0VBcENDLEdBcUNELDRIQXJDQyxHQXNDRCxLQUFLLGNBQUwsRUF0Q0MsR0FzQ3VCLElBdEN2QixHQXVDTCxRQXZDSyxHQXdDVCxRQXhDSjtBQTBDQSxhQUFPLE1BQVA7QUFDSDtBQUVEOzs7Ozs7OzJCQUlRLFksRUFBYyxZLEVBQWE7QUFDL0IsV0FBSyxJQUFMLEdBQVksWUFBWjtBQUNBLFdBQUssSUFBTCxHQUFZLFlBQVo7QUFDSDs7OztFQTNPYyxLO0FBNk9uQjs7Ozs7OztBQU9BOzs7QUFFQSxNQUFNLENBQUMsT0FBUCxHQUFpQixJQUFqQjs7O0FDblFBOztBQUNBLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxTQUFELENBQW5CO0FBRUE7Ozs7OztBQUlBLFNBQVMsS0FBVCxDQUFlLE1BQWYsRUFBdUIsTUFBdkIsRUFBK0I7QUFFM0IsRUFBQSxLQUFLLENBQUMsSUFBTixDQUFXLElBQVgsRUFGMkIsQ0FFVDs7QUFFbEIsT0FBSyxJQUFMLEdBQVksVUFBVSxNQUFWLEVBQWtCLE1BQWxCLEVBQTBCO0FBQ2xDLFNBQUssQ0FBTCxHQUFTLE1BQVQ7QUFDQSxTQUFLLENBQUwsR0FBUyxNQUFUOztBQUNBLFFBQUcsT0FBTyxNQUFQLEtBQWtCLFVBQXJCLEVBQWdDO0FBQzVCLFdBQUssSUFBTCxHQUFZLElBQUksTUFBSixFQUFaO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsTUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHVCQUFaO0FBQ0g7O0FBRUQsU0FBSyxrQkFBTCxHQUEwQixDQUExQixDQVRrQyxDQVNMOztBQUM3QixTQUFLLDhCQUFMLEdBQXNDLENBQXRDLENBVmtDLENBVU87QUFDekM7QUFDQTs7QUFDQSxTQUFLLGtCQUFMLEdBQTBCLENBQTFCO0FBQ0EsUUFBTSxJQUFJLEdBQUcsS0FBSyxrQkFBTCxHQUEwQixDQUExQixHQUE4QixDQUEzQyxDQWRrQyxDQWNXOztBQUM3QyxTQUFLLElBQUwsQ0FBVSxJQUFWLENBQWUsS0FBSyxDQUFMLEdBQVMsS0FBSyxrQkFBN0IsRUFBaUQsS0FBSyxDQUFMLEdBQVMsS0FBSyxrQkFBL0QsRUFBbUYsSUFBbkYsRUFBeUYsSUFBekY7QUFDSCxHQWhCRDs7QUFrQkEsT0FBSyxJQUFMLENBQVUsTUFBVixFQUFrQixNQUFsQjtBQUVBOzs7O0FBR0EsT0FBSyxJQUFMLEdBQVksVUFBVSxPQUFWLEVBQW9CLFNBQXBCLEVBQStCO0FBRXZDLElBQUEsT0FBTyxDQUFDLFdBQVIsR0FBc0IsU0FBdEI7O0FBQ0EsUUFBRyxLQUFLLGNBQUwsT0FBMEIsSUFBN0IsRUFBbUM7QUFDaEMsTUFBQSxPQUFPLENBQUMsV0FBUixHQUFzQixLQUFLLGNBQUwsRUFBdEI7QUFDRjs7QUFFRCxJQUFBLE9BQU8sQ0FBQyxTQUFSOztBQUVBLFFBQUcsU0FBSCxFQUFhO0FBQ1YsTUFBQSxPQUFPLENBQUMsU0FBUixHQUFvQixTQUFwQjtBQUNGOztBQUVELFFBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxrQkFBTCxHQUF3QixDQUFuQyxDQUFmO0FBQ0EsSUFBQSxPQUFPLENBQUMsUUFBUixDQUFpQixLQUFLLENBQUwsR0FBUyxNQUExQixFQUFrQyxLQUFLLENBQUwsR0FBUyxNQUEzQyxFQUFtRCxLQUFLLGtCQUF4RCxFQUE0RSxLQUFLLGtCQUFqRjtBQUVBLElBQUEsT0FBTyxDQUFDLE1BQVI7QUFDSCxHQWpCRDs7QUFtQkEsT0FBSyxXQUFMLEdBQW1CLFVBQVUsT0FBVixFQUFtQixDQUFuQixFQUFzQixDQUF0QixFQUF5QjtBQUN4QyxRQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBTCxDQUFTLENBQUMsR0FBRyxLQUFLLENBQWxCLENBQWY7QUFDQSxRQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBTCxDQUFTLENBQUMsR0FBRyxLQUFLLENBQWxCLENBQWY7QUFDQSxRQUFNLE1BQU0sR0FBRyxDQUFmLENBSHdDLENBSXhDOztBQUNBLFFBQUksTUFBTSxHQUFHLE1BQVQsSUFBbUIsTUFBTSxHQUFHLE1BQWhDLEVBQXVDO0FBQ25DLGFBQU8sSUFBUDtBQUNIOztBQUNELFdBQU8sS0FBUCxDQVJ3QyxDQVV4QztBQUNILEdBWEQ7O0FBWUEsT0FBSyxZQUFMLEdBQW9CLFVBQVUsT0FBVixFQUFtQjtBQUVuQyxJQUFBLE9BQU8sQ0FBQyxTQUFSO0FBQ0EsSUFBQSxXQUFXLEdBQUcsRUFBZCxDQUhtQyxDQUluQzs7QUFDQSxJQUFBLE9BQU8sQ0FBQyxTQUFSLENBQWtCLGFBQWxCLEVBQWlDLEtBQUssQ0FBTCxHQUFTLEdBQTFDLEVBQStDLEtBQUssQ0FBTCxHQUFTLEdBQXhEO0FBQ0EsUUFBSSxNQUFNLEdBQUcsRUFBYjtBQUNBLElBQUEsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsRUFBaEI7QUFDQSxJQUFBLE1BQU0sQ0FBQyxNQUFQLENBQWMsQ0FBZCxHQUFrQixLQUFLLE9BQXZCO0FBQ0EsSUFBQSxNQUFNLENBQUMsTUFBUCxDQUFjLENBQWQsR0FBa0IsS0FBSyxPQUF2QjtBQUNBLElBQUEsV0FBVyxDQUFDLElBQVosQ0FBaUIsTUFBakI7QUFHQSxJQUFBLE9BQU8sQ0FBQyxTQUFSLEdBQW9CLFNBQXBCO0FBRUEsUUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLDhCQUFMLEdBQW9DLENBQS9DLENBQWY7QUFDQSxJQUFBLE9BQU8sQ0FBQyxRQUFSLENBQWlCLEtBQUssQ0FBTCxHQUFTLE1BQTFCLEVBQWtDLEtBQUssQ0FBTCxHQUFTLE1BQTNDLEVBQW1ELEtBQUssOEJBQXhELEVBQXdGLEtBQUssOEJBQTdGO0FBRUEsSUFBQSxPQUFPLENBQUMsU0FBUixHQUFvQixTQUFwQjtBQUVILEdBcEJEOztBQXNCQSxPQUFLLE1BQUwsR0FBYyxZQUFVO0FBRXBCO0FBQ0EsUUFBTSxHQUFHLEdBQUcsS0FBSyxDQUFDLFNBQU4sQ0FBZ0IsTUFBaEIsQ0FBdUIsSUFBdkIsQ0FBNEIsSUFBNUIsQ0FBWjtBQUVBLElBQUEsR0FBRyxDQUFDLElBQUosR0FBVyxPQUFYO0FBRUEsSUFBQSxHQUFHLENBQUMsQ0FBSixHQUFRLEtBQUssQ0FBYjtBQUNBLElBQUEsR0FBRyxDQUFDLENBQUosR0FBUSxLQUFLLENBQWI7QUFFQSxJQUFBLEdBQUcsQ0FBQyxXQUFKLEdBQWtCLEtBQUssY0FBTCxFQUFsQjtBQUVBLFdBQU8sR0FBUDtBQUNILEdBYkQ7O0FBZUEsT0FBSyxRQUFMLEdBQWdCLFlBQVU7QUFDdEIsUUFBSSxxQkFBcUIsR0FBRyxHQUE1QjtBQUVBLFFBQUksTUFBTSxHQUFHLDJDQUNMLDBCQURLLEdBRUwsS0FGSyxHQUVHLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxDQUFMLEdBQVMscUJBQXBCLElBQTZDLHFCQUZoRCxHQUdMLFdBSEssR0FHUyxJQUFJLENBQUMsS0FBTCxDQUFXLEtBQUssQ0FBTCxHQUFTLHFCQUFwQixJQUE2QyxxQkFIdEQsR0FJTCxRQUpSO0FBTUEsV0FBTyxNQUFQO0FBQ0gsR0FWRDtBQVdBOzs7OztBQUdBLE9BQUssWUFBTCxHQUFvQixZQUFVO0FBQzFCLFFBQUkscUJBQXFCLEdBQUcsR0FBNUIsQ0FEMEIsQ0FFMUI7O0FBQ0EsUUFBSSxNQUFNLEdBQUcsNkJBQ0wsZ0NBREssR0FFRCx5Q0FGQyxHQUdELDBCQUhDLEdBSUcsMENBSkgsR0FLTyw2REFMUCxHQU1PLGtIQU5QLEdBT08sSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFLLENBQUwsR0FBUyxxQkFBcEIsSUFBNkMscUJBUHBELEdBTzRFLElBUDVFLEdBUUcsUUFSSCxHQVNHLDBDQVRILEdBVU8sNkRBVlAsR0FXTyxrSEFYUCxHQVlPLElBQUksQ0FBQyxLQUFMLENBQVcsS0FBSyxDQUFMLEdBQVMscUJBQXBCLElBQTZDLHFCQVpwRCxHQVk0RSxJQVo1RSxHQWFHLFFBYkgsR0FjRCxRQWRDLEdBZUwsUUFmSyxHQWlCTCwwQ0FqQkssR0FrQkQsd0VBbEJDLEdBbUJELDRIQW5CQyxHQW9CRCxLQUFLLGNBQUwsRUFwQkMsR0FvQnVCLElBcEJ2QixHQXFCTCxRQXJCSyxHQXNCVCxRQXRCSjtBQXdCQSxXQUFPLE1BQVA7QUFDSCxHQTVCRDs7QUE4QkEsT0FBSyxNQUFMLEdBQWMsVUFBVSxZQUFWLEVBQXdCLFlBQXhCLEVBQXFDO0FBQy9DLFNBQUssQ0FBTCxHQUFTLFlBQVQ7QUFDQSxTQUFLLENBQUwsR0FBUyxZQUFUO0FBQ0gsR0FIRDtBQUlIOztBQUNELEtBQUssQ0FBQyxTQUFOLEdBQWtCLE1BQU0sQ0FBQyxNQUFQLENBQWMsS0FBSyxDQUFDLFNBQXBCLEVBQStCO0FBQzdDLEVBQUEsV0FBVyxFQUFFO0FBQ1QsSUFBQSxLQUFLLEVBQUU7QUFERTtBQURnQyxDQUEvQixDQUFsQjtBQU1BLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLEtBQWpCOzs7QUM3SkE7Ozs7QUFDQSxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsU0FBRCxDQUF0QjtBQUNBOzs7OztBQUdBLFNBQVMsS0FBVCxHQUFpQjtBQUNoQixNQUFNLElBQUksR0FBRyxJQUFiO0FBRUcsT0FBSyxJQUFMLEdBQVksTUFBTSxFQUFsQixDQUhhLENBSWhCOztBQUNBLE9BQUssV0FBTCxHQUFtQixTQUFuQjtBQUNBLE9BQUssU0FBTCxHQUFpQixHQUFqQixDQU5nQixDQU1NOztBQUVuQixPQUFLLEtBQUwsR0FBYSxJQUFiO0FBQ0EsT0FBSyxnQkFBTCxHQUF3QixZQUF4QjtBQUNBLE9BQUssVUFBTCxHQUFrQixDQUFsQjtBQUVIOztBQUdELEtBQUssQ0FBQyxTQUFOLENBQWdCLGNBQWhCLEdBQWlDLFVBQVUsS0FBVixFQUFpQjtBQUM5QyxNQUFHLEtBQUgsRUFBUztBQUNMLFNBQUssV0FBTCxHQUFtQixLQUFuQjtBQUNIO0FBQ0osQ0FKRDs7QUFLQSxLQUFLLENBQUMsU0FBTixDQUFnQixjQUFoQixHQUFpQyxZQUFZO0FBQ3pDLFNBQU8sS0FBSyxXQUFMLElBQW9CLElBQTNCO0FBQ0gsQ0FGRDtBQUdBOzs7OztBQUdBLEtBQUssQ0FBQyxTQUFOLENBQWdCLE9BQWhCLEdBQTBCLFVBQVUsSUFBVixFQUFnQjtBQUN0QyxNQUFHLElBQUgsRUFBUTtBQUNKLFNBQUssSUFBTCxHQUFZLElBQVo7QUFDSDtBQUVKLENBTEQ7O0FBTUEsS0FBSyxDQUFDLFNBQU4sQ0FBZ0IsT0FBaEIsR0FBMEIsWUFBWTtBQUNsQyxTQUFPLEtBQUssSUFBTCxJQUFhLElBQXBCO0FBQ0gsQ0FGRDs7QUFJQSxLQUFLLENBQUMsU0FBTixDQUFnQixRQUFoQixHQUEyQixVQUFVLEtBQVYsRUFBaUI7QUFDeEMsTUFBRyxLQUFLLEtBQUssSUFBVixJQUFrQixRQUFPLEtBQVAsTUFBaUIsU0FBdEMsRUFBZ0Q7QUFDNUMsU0FBSyxLQUFMLEdBQWEsS0FBYjtBQUNIO0FBQ0osQ0FKRDs7QUFLQSxLQUFLLENBQUMsU0FBTixDQUFnQixRQUFoQixHQUEyQixZQUFZO0FBQ25DLFNBQU8sS0FBSyxLQUFMLElBQWMsSUFBckI7QUFDSCxDQUZEOztBQUlBLEtBQUssQ0FBQyxTQUFOLENBQWdCLGFBQWhCLEdBQWdDLFVBQVUsVUFBVixFQUFzQjtBQUM5QyxPQUFLLFVBQUwsR0FBa0IsVUFBbEI7QUFDUCxDQUZEOztBQUdBLEtBQUssQ0FBQyxTQUFOLENBQWdCLGFBQWhCLEdBQWdDLFlBQVk7QUFDeEMsU0FBTyxLQUFLLFVBQUwsSUFBbUIsQ0FBMUI7QUFDSCxDQUZEOztBQUlBLEtBQUssQ0FBQyxTQUFOLENBQWdCLG1CQUFoQixHQUFzQyxVQUFVLGdCQUFWLEVBQTRCO0FBQzFELE9BQUssZ0JBQUwsR0FBd0IsZ0JBQXhCO0FBQ1AsQ0FGRDs7QUFHQSxLQUFLLENBQUMsU0FBTixDQUFnQixtQkFBaEIsR0FBc0MsWUFBWTtBQUM5QyxTQUFPLEtBQUssZ0JBQUwsSUFBeUIsSUFBaEM7QUFDSCxDQUZEOztBQUlBLEtBQUssQ0FBQyxTQUFOLENBQWdCLElBQWhCLEdBQXVCLFVBQVUsT0FBVixFQUFtQjtBQUN0QyxFQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksOEJBQVo7QUFDSCxDQUZEOztBQUdBLEtBQUssQ0FBQyxTQUFOLENBQWdCLFdBQWhCLEdBQThCLFVBQVUsR0FBVixFQUFlLENBQWYsRUFBa0IsQ0FBbEIsRUFBcUI7QUFDL0MsRUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHFDQUFaO0FBQ0gsQ0FGRDs7QUFHQSxLQUFLLENBQUMsU0FBTixDQUFnQixZQUFoQixHQUErQixVQUFVLENBQVYsRUFBYSxDQUFiLEVBQWdCO0FBQzNDLEVBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxzQ0FBWjtBQUNILENBRkQ7O0FBR0EsS0FBSyxDQUFDLFNBQU4sQ0FBZ0IsTUFBaEIsR0FBeUIsVUFBVSxDQUFWLEVBQWEsQ0FBYixFQUFnQjtBQUNyQyxNQUFNLEdBQUcsR0FBQyxFQUFWO0FBQ0EsRUFBQSxHQUFHLENBQUMsV0FBSixHQUFrQixLQUFLLGNBQUwsRUFBbEI7QUFDQSxFQUFBLEdBQUcsQ0FBQyxJQUFKLEdBQVcsS0FBSyxPQUFMLEVBQVg7QUFDQSxFQUFBLEdBQUcsQ0FBQyxLQUFKLEdBQVksS0FBSyxRQUFMLEVBQVo7QUFDQSxTQUFPLEdBQVA7QUFDSCxDQU5EO0FBT0E7Ozs7OztBQUlBLEtBQUssQ0FBQyxTQUFOLENBQWdCLE9BQWhCLEdBQTBCLFlBQVU7QUFDaEMsU0FBTyxJQUFQO0FBQ0gsQ0FGRDtBQUdBOzs7OztBQUdBLEtBQUssQ0FBQyxTQUFOLENBQWdCLFlBQWhCLEdBQStCLFVBQVUsQ0FBVixFQUFhLENBQWIsRUFBZ0I7QUFDM0MsRUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHNDQUFaO0FBQ0gsQ0FGRDtBQUlBOzs7Ozs7QUFJQSxLQUFLLENBQUMsU0FBTixDQUFnQixNQUFoQixHQUF5QixVQUFVLENBQVYsRUFBYSxDQUFiLEVBQWdCO0FBQ3JDLEVBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxnQ0FBWjtBQUNILENBRkQ7O0FBSUEsTUFBTSxDQUFDLE9BQVAsR0FBaUIsS0FBakI7Ozs7Ozs7Ozs7O0FDdEdBOzs7QUFJQSxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsa0JBQUQsQ0FBcEI7O0FBQ0EsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLGdCQUFELENBQWxCOztBQUNBLElBQUksUUFBUSxHQUFHLE9BQU8sQ0FBQyxvQkFBRCxDQUF0Qjs7QUFDQSxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsaUJBQUQsQ0FBbkI7O0lBRU0sWTs7Ozs7Ozs7O21DQUVxQix5QixFQUEyQjtBQUM5QyxhQUFPLFlBQVksQ0FBQyxNQUFiLENBQW9CLHlCQUF5QixDQUFDLElBQTlDLEVBQW9ELHlCQUFwRCxDQUFQO0FBQ0g7OzsyQkFFYyxTLEVBQVcsTSxFQUFRO0FBQzlCLFVBQUksWUFBWSxDQUFDLFFBQWIsQ0FBc0IsU0FBdEIsQ0FBSixFQUFzQztBQUNsQyxZQUFNLE1BQU0sR0FBSSxJQUFJLE1BQUosQ0FDSixNQUFNLENBQUMsT0FESCxFQUVKLE1BQU0sQ0FBQyxPQUZILEVBR0osTUFBTSxDQUFDLE1BSEgsQ0FBaEI7QUFLQSxRQUFBLE1BQU0sQ0FBQyxjQUFQLENBQXNCLE1BQU0sQ0FBQyxXQUE3QjtBQUNBLFFBQUEsTUFBTSxDQUFDLE9BQVAsQ0FBZSxNQUFNLENBQUMsSUFBdEI7QUFDQSxlQUFPLE1BQVA7QUFDSCxPQVRELE1BU08sSUFBSSxZQUFZLENBQUMsTUFBYixDQUFvQixTQUFwQixDQUFKLEVBQW9DO0FBQ3ZDLFlBQU0sSUFBSSxHQUFHLElBQUksSUFBSixDQUNULE1BQU0sQ0FBQyxPQURFLEVBRVQsTUFBTSxDQUFDLE9BRkUsRUFHVCxNQUFNLENBQUMsSUFIRSxFQUlULE1BQU0sQ0FBQyxJQUpFLENBQWI7QUFNQSxRQUFBLElBQUksQ0FBQyxjQUFMLENBQW9CLE1BQU0sQ0FBQyxXQUEzQjtBQUNBLFFBQUEsSUFBSSxDQUFDLE9BQUwsQ0FBYSxNQUFNLENBQUMsSUFBcEI7QUFDQSxRQUFBLElBQUksQ0FBQyxRQUFMLENBQWMsTUFBTSxDQUFDLEtBQXJCO0FBQ0EsZUFBTyxJQUFQO0FBQ0gsT0FYTSxNQVdBLElBQUksWUFBWSxDQUFDLFVBQWIsQ0FBd0IsU0FBeEIsQ0FBSixFQUF3QztBQUMzQyxZQUFNLFFBQVEsR0FBRyxJQUFJLFFBQUosQ0FDTCxNQUFNLENBQUMsTUFERixDQUFqQjtBQUdBLFFBQUEsUUFBUSxDQUFDLGNBQVQsQ0FBd0IsTUFBTSxDQUFDLFdBQS9CO0FBQ0EsUUFBQSxRQUFRLENBQUMsT0FBVCxDQUFpQixNQUFNLENBQUMsSUFBeEI7QUFDQSxlQUFPLFFBQVA7QUFDSCxPQVBNLE1BT0EsSUFBSSxZQUFZLENBQUMsT0FBYixDQUFxQixTQUFyQixDQUFKLEVBQXFDO0FBQ3hDLFlBQU0sS0FBSyxHQUFHLElBQUksS0FBSixDQUNGLE1BQU0sQ0FBQyxDQURMLEVBRUYsTUFBTSxDQUFDLENBRkwsQ0FBZDtBQUlBLFFBQUEsS0FBSyxDQUFDLGNBQU4sQ0FBcUIsTUFBTSxDQUFDLFdBQTVCO0FBQ0EsUUFBQSxLQUFLLENBQUMsT0FBTixDQUFjLE1BQU0sQ0FBQyxJQUFyQjtBQUNBLGVBQU8sS0FBUDtBQUNILE9BUk0sTUFRQTtBQUNILFFBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxTQUFTLEdBQUcscURBQXhCO0FBQ0g7QUFDSjtBQUVEOzs7Ozs7K0JBR21CLFMsRUFBVyxNLEVBQVE7QUFDbEMsVUFBSSxZQUFZLENBQUMsUUFBYixDQUFzQixTQUF0QixDQUFKLEVBQXNDO0FBQ2xDLGVBQU8sSUFBSSxNQUFKLENBQ0gsTUFBTSxDQUFDLE9BREosRUFFSCxNQUFNLENBQUMsT0FGSixFQUdILENBSEcsQ0FBUDtBQUtILE9BTkQsTUFNTyxJQUFJLFlBQVksQ0FBQyxNQUFiLENBQW9CLFNBQXBCLENBQUosRUFBb0M7QUFDdkMsWUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFKLENBQ1QsTUFBTSxDQUFDLE9BREUsRUFFVCxNQUFNLENBQUMsT0FGRSxFQUdULE1BQU0sQ0FBQyxPQUhFLEVBSVQsTUFBTSxDQUFDLE9BSkUsQ0FBYixDQUR1QyxDQU92Qzs7QUFDQSxlQUFPLElBQVA7QUFDSCxPQVRNLE1BU0EsSUFBSSxZQUFZLENBQUMsVUFBYixDQUF3QixTQUF4QixDQUFKLEVBQXdDO0FBQzNDLFlBQU0sTUFBTSxHQUFHLEVBQWY7QUFDQSxZQUFNLE1BQU0sR0FBRyxFQUFmO0FBQ0EsUUFBQSxNQUFNLENBQUMsQ0FBUCxHQUFXLE1BQU0sQ0FBQyxPQUFsQjtBQUNBLFFBQUEsTUFBTSxDQUFDLENBQVAsR0FBVyxNQUFNLENBQUMsT0FBbEI7QUFDQSxRQUFBLE1BQU0sQ0FBQyxJQUFQLENBQVksTUFBWjtBQUNBLGVBQU8sSUFBSSxRQUFKLENBQ0gsTUFERyxDQUFQO0FBR0gsT0FUTSxNQVNBLElBQUksWUFBWSxDQUFDLE9BQWIsQ0FBcUIsU0FBckIsQ0FBSixFQUFxQztBQUN4QyxlQUFPLElBQUksS0FBSixDQUNILE1BQU0sQ0FBQyxPQURKLEVBRUgsTUFBTSxDQUFDLE9BRkosQ0FBUDtBQUlILE9BTE0sTUFLQTtBQUNILFFBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxTQUFTLEdBQUcsb0VBQXhCO0FBQ0g7QUFDSjtBQUNEOzs7Ozs7NkJBR2lCLEssRUFBTztBQUNwQixVQUFHLEtBQUssS0FBSyxRQUFWLElBQXNCLEtBQUssS0FBSyxRQUFuQyxFQUE0QztBQUN4QyxlQUFPLElBQVA7QUFDSDs7QUFDRCxhQUFPLEtBQVA7QUFDSDtBQUVEOzs7Ozs7MkJBR2UsSyxFQUFPO0FBQ2xCLFVBQUcsS0FBSyxLQUFLLE1BQVYsSUFBb0IsS0FBSyxLQUFLLGNBQWpDLEVBQWdEO0FBQzVDLGVBQU8sSUFBUDtBQUNIOztBQUNELGFBQU8sS0FBUDtBQUNIO0FBRUQ7Ozs7OzsrQkFHbUIsSyxFQUFPO0FBQ3RCLFVBQUcsS0FBSyxLQUFLLFVBQVYsSUFBd0IsS0FBSyxLQUFLLFVBQXJDLEVBQWdEO0FBQzVDLGVBQU8sSUFBUDtBQUNIOztBQUNELGFBQU8sS0FBUDtBQUNIO0FBRUQ7Ozs7Ozs0QkFHZ0IsSyxFQUFPO0FBQ25CLFVBQUcsS0FBSyxLQUFLLE9BQVYsSUFBcUIsS0FBSyxLQUFLLE9BQWxDLEVBQTBDO0FBQ3RDLGVBQU8sSUFBUDtBQUNIOztBQUNELGFBQU8sS0FBUDtBQUNIOzs7Ozs7QUFHTCxNQUFNLENBQUMsT0FBUCxHQUFpQixZQUFqQjs7O0FDdElBO0FBQ0E7Ozs7Ozs7QUFPQSxTQUFTLFVBQVQsR0FBc0IsQ0FFckI7O0FBRUQsVUFBVSxDQUFDLFNBQVgsQ0FBcUIsR0FBckIsR0FBMkIsVUFBVSxHQUFWLEVBQWU7QUFDdEMsRUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLDZCQUFaO0FBRUgsQ0FIRDs7QUFLQSxVQUFVLENBQUMsU0FBWCxDQUFxQixHQUFyQixHQUEyQixZQUFZO0FBQ25DLEVBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSw2QkFBWjtBQUNILENBRkQ7O0FBSUEsVUFBVSxDQUFDLFNBQVgsYUFBOEIsWUFBWTtBQUN0QyxFQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksZ0NBQVo7QUFDSCxDQUZEOztBQUlBLFVBQVUsQ0FBQyxTQUFYLENBQXFCLElBQXJCLEdBQTRCLFlBQVk7QUFDcEMsRUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLDhCQUFaO0FBQ0gsQ0FGRDtBQUlBOzs7OztBQUdBLFVBQVUsQ0FBQyxTQUFYLENBQXFCLGFBQXJCLEdBQXFDLFlBQVk7QUFDN0MsRUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLDhCQUFaO0FBQ0gsQ0FGRDs7QUFJQSxNQUFNLENBQUMsT0FBUCxHQUFpQixVQUFqQjs7O0FDcENBOztBQUNBLElBQUksVUFBVSxHQUFHLE9BQU8sQ0FBQyxjQUFELENBQXhCO0FBRUE7Ozs7O0FBR0EsU0FBUyxlQUFULEdBQTJCO0FBRXZCLEVBQUEsVUFBVSxDQUFDLElBQVgsQ0FBZ0IsSUFBaEIsRUFGdUIsQ0FFQTs7QUFFdkIsT0FBSyxJQUFMLEdBQVksWUFBWTtBQUNwQixTQUFLLFVBQUwsR0FBa0IsRUFBbEI7QUFDSCxHQUZEOztBQUlBLE9BQUssSUFBTDs7QUFFQSxPQUFLLEdBQUwsR0FBVyxVQUFTLEdBQVQsRUFBYTtBQUNwQixTQUFLLFVBQUwsQ0FBZ0IsSUFBaEIsQ0FBcUIsR0FBckI7QUFDSCxHQUZEOztBQUlBLE9BQUssR0FBTCxHQUFXLFVBQVMsS0FBVCxFQUFlO0FBQ3RCLFdBQU8sS0FBSyxVQUFMLENBQWdCLEtBQWhCLENBQVA7QUFDSCxHQUZEOztBQUlBLG1CQUFjLFVBQVMsS0FBVCxFQUFlO0FBQ3pCLFNBQUssVUFBTCxDQUFnQixNQUFoQixDQUF1QixLQUF2QixFQUE4QixDQUE5QjtBQUNILEdBRkQ7QUFHQTs7Ozs7QUFHQSxPQUFLLGFBQUwsR0FBcUIsVUFBUyxLQUFULEVBQWdCLFFBQWhCLEVBQXlCO0FBQzFDLFNBQUssVUFBTCxDQUFnQixLQUFoQixJQUF5QixRQUF6QjtBQUNILEdBRkQ7O0FBSUEsT0FBSyxJQUFMLEdBQVksWUFBVTtBQUNsQixXQUFPLEtBQUssVUFBTCxDQUFnQixNQUF2QjtBQUNILEdBRkQ7O0FBSUEsT0FBSyxhQUFMLEdBQXFCLFlBQVU7QUFDM0IsV0FBTyxLQUFLLFVBQVo7QUFDSCxHQUZEO0FBSUg7O0FBR0QsZUFBZSxDQUFDLFNBQWhCLEdBQTRCLE1BQU0sQ0FBQyxNQUFQLENBQWMsVUFBVSxDQUFDLFNBQXpCLEVBQW9DO0FBQzVELEVBQUEsV0FBVyxFQUFFO0FBQ1QsSUFBQSxLQUFLLEVBQUU7QUFERTtBQUQrQyxDQUFwQyxDQUE1QjtBQU1BLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLGVBQWpCOzs7OztBQ25EQTs7Ozs7QUFLQSxPQUFPLENBQUMsV0FBUixHQUFzQixTQUFTLFdBQVQsQ0FBcUIsRUFBckIsRUFBeUIsRUFBekIsRUFBNkIsRUFBN0IsRUFBaUMsRUFBakMsRUFBcUMsRUFBckMsRUFBeUMsRUFBekMsRUFBNkM7QUFDL0QsTUFBSSxZQUFZLEdBQUcsQ0FBQyxFQUFFLEdBQUcsRUFBTixLQUFhLEVBQUUsR0FBRyxFQUFsQixJQUF3QixDQUFDLEVBQUUsR0FBRyxFQUFOLEtBQWEsRUFBRSxHQUFHLEVBQWxCLENBQTNDO0FBQ0EsU0FBTyxZQUFQO0FBQ0gsQ0FIRDs7QUFLQSxPQUFPLENBQUMsaUJBQVIsR0FBNEIsU0FBUyxpQkFBVCxDQUEyQixFQUEzQixFQUErQixFQUEvQixFQUFtQyxFQUFuQyxFQUF1QyxFQUF2QyxFQUEyQztBQUNuRSxNQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBTCxDQUFVLENBQUMsRUFBRSxHQUFHLEVBQU4sS0FBYSxFQUFFLEdBQUcsRUFBbEIsSUFBd0IsQ0FBQyxFQUFFLEdBQUcsRUFBTixLQUFhLEVBQUUsR0FBRyxFQUFsQixDQUFsQyxDQUFmO0FBQ0EsU0FBTyxRQUFQO0FBQ0gsQ0FIRDs7QUFLQSxPQUFPLENBQUMsYUFBUixHQUF3QixTQUFTLGFBQVQsQ0FBdUIsRUFBdkIsRUFBMkIsRUFBM0IsRUFBK0IsRUFBL0IsRUFBbUMsRUFBbkMsRUFBdUMsRUFBdkMsRUFBMkMsRUFBM0MsRUFBK0M7QUFDbkUsTUFBSSxZQUFZLEdBQUcsV0FBVyxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLEVBQWIsRUFBaUIsRUFBakIsRUFBcUIsRUFBckIsQ0FBOUIsQ0FEbUUsQ0FFbkU7O0FBRUEsTUFBSyxZQUFZLEdBQUcsR0FBZixJQUFzQixZQUFZLEdBQUcsQ0FBQyxHQUF2QyxLQUFpRCxFQUFFLElBQUksRUFBTixJQUFZLEVBQUUsSUFBSSxFQUFuQixJQUEyQixFQUFFLElBQUksRUFBTixJQUFZLEVBQUUsSUFBSSxFQUE3RixNQUF1RyxFQUFFLElBQUksRUFBTixJQUFZLEVBQUUsSUFBSSxFQUFuQixJQUEyQixFQUFFLElBQUksRUFBTixJQUFZLEVBQUUsSUFBSSxFQUFuSixDQUFKLEVBQTZKO0FBQ3pKLFdBQU8sSUFBUDtBQUNIOztBQUNELFNBQU8sS0FBUDtBQUNILENBUkQ7OztBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9zSkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLy8gQ29weXJpZ2h0IEpveWVudCwgSW5jLiBhbmQgb3RoZXIgTm9kZSBjb250cmlidXRvcnMuXG4vL1xuLy8gUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGFcbi8vIGNvcHkgb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGVcbi8vIFwiU29mdHdhcmVcIiksIHRvIGRlYWwgaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZ1xuLy8gd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHMgdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLFxuLy8gZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwgYW5kL29yIHNlbGwgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdFxuLy8gcGVyc29ucyB0byB3aG9tIHRoZSBTb2Z0d2FyZSBpcyBmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlXG4vLyBmb2xsb3dpbmcgY29uZGl0aW9uczpcbi8vXG4vLyBUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZFxuLy8gaW4gYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG4vL1xuLy8gVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTU1xuLy8gT1IgSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRlxuLy8gTUVSQ0hBTlRBQklMSVRZLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTlxuLy8gTk8gRVZFTlQgU0hBTEwgVEhFIEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sXG4vLyBEQU1BR0VTIE9SIE9USEVSIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1Jcbi8vIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEVcbi8vIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEUgU09GVFdBUkUuXG5cbmZ1bmN0aW9uIEV2ZW50RW1pdHRlcigpIHtcbiAgdGhpcy5fZXZlbnRzID0gdGhpcy5fZXZlbnRzIHx8IHt9O1xuICB0aGlzLl9tYXhMaXN0ZW5lcnMgPSB0aGlzLl9tYXhMaXN0ZW5lcnMgfHwgdW5kZWZpbmVkO1xufVxubW9kdWxlLmV4cG9ydHMgPSBFdmVudEVtaXR0ZXI7XG5cbi8vIEJhY2t3YXJkcy1jb21wYXQgd2l0aCBub2RlIDAuMTAueFxuRXZlbnRFbWl0dGVyLkV2ZW50RW1pdHRlciA9IEV2ZW50RW1pdHRlcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fZXZlbnRzID0gdW5kZWZpbmVkO1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fbWF4TGlzdGVuZXJzID0gdW5kZWZpbmVkO1xuXG4vLyBCeSBkZWZhdWx0IEV2ZW50RW1pdHRlcnMgd2lsbCBwcmludCBhIHdhcm5pbmcgaWYgbW9yZSB0aGFuIDEwIGxpc3RlbmVycyBhcmVcbi8vIGFkZGVkIHRvIGl0LiBUaGlzIGlzIGEgdXNlZnVsIGRlZmF1bHQgd2hpY2ggaGVscHMgZmluZGluZyBtZW1vcnkgbGVha3MuXG5FdmVudEVtaXR0ZXIuZGVmYXVsdE1heExpc3RlbmVycyA9IDEwO1xuXG4vLyBPYnZpb3VzbHkgbm90IGFsbCBFbWl0dGVycyBzaG91bGQgYmUgbGltaXRlZCB0byAxMC4gVGhpcyBmdW5jdGlvbiBhbGxvd3Ncbi8vIHRoYXQgdG8gYmUgaW5jcmVhc2VkLiBTZXQgdG8gemVybyBmb3IgdW5saW1pdGVkLlxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5zZXRNYXhMaXN0ZW5lcnMgPSBmdW5jdGlvbihuKSB7XG4gIGlmICghaXNOdW1iZXIobikgfHwgbiA8IDAgfHwgaXNOYU4obikpXG4gICAgdGhyb3cgVHlwZUVycm9yKCduIG11c3QgYmUgYSBwb3NpdGl2ZSBudW1iZXInKTtcbiAgdGhpcy5fbWF4TGlzdGVuZXJzID0gbjtcbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmVtaXQgPSBmdW5jdGlvbih0eXBlKSB7XG4gIHZhciBlciwgaGFuZGxlciwgbGVuLCBhcmdzLCBpLCBsaXN0ZW5lcnM7XG5cbiAgaWYgKCF0aGlzLl9ldmVudHMpXG4gICAgdGhpcy5fZXZlbnRzID0ge307XG5cbiAgLy8gSWYgdGhlcmUgaXMgbm8gJ2Vycm9yJyBldmVudCBsaXN0ZW5lciB0aGVuIHRocm93LlxuICBpZiAodHlwZSA9PT0gJ2Vycm9yJykge1xuICAgIGlmICghdGhpcy5fZXZlbnRzLmVycm9yIHx8XG4gICAgICAgIChpc09iamVjdCh0aGlzLl9ldmVudHMuZXJyb3IpICYmICF0aGlzLl9ldmVudHMuZXJyb3IubGVuZ3RoKSkge1xuICAgICAgZXIgPSBhcmd1bWVudHNbMV07XG4gICAgICBpZiAoZXIgaW5zdGFuY2VvZiBFcnJvcikge1xuICAgICAgICB0aHJvdyBlcjsgLy8gVW5oYW5kbGVkICdlcnJvcicgZXZlbnRcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIEF0IGxlYXN0IGdpdmUgc29tZSBraW5kIG9mIGNvbnRleHQgdG8gdGhlIHVzZXJcbiAgICAgICAgdmFyIGVyciA9IG5ldyBFcnJvcignVW5jYXVnaHQsIHVuc3BlY2lmaWVkIFwiZXJyb3JcIiBldmVudC4gKCcgKyBlciArICcpJyk7XG4gICAgICAgIGVyci5jb250ZXh0ID0gZXI7XG4gICAgICAgIHRocm93IGVycjtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBoYW5kbGVyID0gdGhpcy5fZXZlbnRzW3R5cGVdO1xuXG4gIGlmIChpc1VuZGVmaW5lZChoYW5kbGVyKSlcbiAgICByZXR1cm4gZmFsc2U7XG5cbiAgaWYgKGlzRnVuY3Rpb24oaGFuZGxlcikpIHtcbiAgICBzd2l0Y2ggKGFyZ3VtZW50cy5sZW5ndGgpIHtcbiAgICAgIC8vIGZhc3QgY2FzZXNcbiAgICAgIGNhc2UgMTpcbiAgICAgICAgaGFuZGxlci5jYWxsKHRoaXMpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgMjpcbiAgICAgICAgaGFuZGxlci5jYWxsKHRoaXMsIGFyZ3VtZW50c1sxXSk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAzOlxuICAgICAgICBoYW5kbGVyLmNhbGwodGhpcywgYXJndW1lbnRzWzFdLCBhcmd1bWVudHNbMl0pO1xuICAgICAgICBicmVhaztcbiAgICAgIC8vIHNsb3dlclxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG4gICAgICAgIGhhbmRsZXIuYXBwbHkodGhpcywgYXJncyk7XG4gICAgfVxuICB9IGVsc2UgaWYgKGlzT2JqZWN0KGhhbmRsZXIpKSB7XG4gICAgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG4gICAgbGlzdGVuZXJzID0gaGFuZGxlci5zbGljZSgpO1xuICAgIGxlbiA9IGxpc3RlbmVycy5sZW5ndGg7XG4gICAgZm9yIChpID0gMDsgaSA8IGxlbjsgaSsrKVxuICAgICAgbGlzdGVuZXJzW2ldLmFwcGx5KHRoaXMsIGFyZ3MpO1xuICB9XG5cbiAgcmV0dXJuIHRydWU7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmFkZExpc3RlbmVyID0gZnVuY3Rpb24odHlwZSwgbGlzdGVuZXIpIHtcbiAgdmFyIG07XG5cbiAgaWYgKCFpc0Z1bmN0aW9uKGxpc3RlbmVyKSlcbiAgICB0aHJvdyBUeXBlRXJyb3IoJ2xpc3RlbmVyIG11c3QgYmUgYSBmdW5jdGlvbicpO1xuXG4gIGlmICghdGhpcy5fZXZlbnRzKVxuICAgIHRoaXMuX2V2ZW50cyA9IHt9O1xuXG4gIC8vIFRvIGF2b2lkIHJlY3Vyc2lvbiBpbiB0aGUgY2FzZSB0aGF0IHR5cGUgPT09IFwibmV3TGlzdGVuZXJcIiEgQmVmb3JlXG4gIC8vIGFkZGluZyBpdCB0byB0aGUgbGlzdGVuZXJzLCBmaXJzdCBlbWl0IFwibmV3TGlzdGVuZXJcIi5cbiAgaWYgKHRoaXMuX2V2ZW50cy5uZXdMaXN0ZW5lcilcbiAgICB0aGlzLmVtaXQoJ25ld0xpc3RlbmVyJywgdHlwZSxcbiAgICAgICAgICAgICAgaXNGdW5jdGlvbihsaXN0ZW5lci5saXN0ZW5lcikgP1xuICAgICAgICAgICAgICBsaXN0ZW5lci5saXN0ZW5lciA6IGxpc3RlbmVyKTtcblxuICBpZiAoIXRoaXMuX2V2ZW50c1t0eXBlXSlcbiAgICAvLyBPcHRpbWl6ZSB0aGUgY2FzZSBvZiBvbmUgbGlzdGVuZXIuIERvbid0IG5lZWQgdGhlIGV4dHJhIGFycmF5IG9iamVjdC5cbiAgICB0aGlzLl9ldmVudHNbdHlwZV0gPSBsaXN0ZW5lcjtcbiAgZWxzZSBpZiAoaXNPYmplY3QodGhpcy5fZXZlbnRzW3R5cGVdKSlcbiAgICAvLyBJZiB3ZSd2ZSBhbHJlYWR5IGdvdCBhbiBhcnJheSwganVzdCBhcHBlbmQuXG4gICAgdGhpcy5fZXZlbnRzW3R5cGVdLnB1c2gobGlzdGVuZXIpO1xuICBlbHNlXG4gICAgLy8gQWRkaW5nIHRoZSBzZWNvbmQgZWxlbWVudCwgbmVlZCB0byBjaGFuZ2UgdG8gYXJyYXkuXG4gICAgdGhpcy5fZXZlbnRzW3R5cGVdID0gW3RoaXMuX2V2ZW50c1t0eXBlXSwgbGlzdGVuZXJdO1xuXG4gIC8vIENoZWNrIGZvciBsaXN0ZW5lciBsZWFrXG4gIGlmIChpc09iamVjdCh0aGlzLl9ldmVudHNbdHlwZV0pICYmICF0aGlzLl9ldmVudHNbdHlwZV0ud2FybmVkKSB7XG4gICAgaWYgKCFpc1VuZGVmaW5lZCh0aGlzLl9tYXhMaXN0ZW5lcnMpKSB7XG4gICAgICBtID0gdGhpcy5fbWF4TGlzdGVuZXJzO1xuICAgIH0gZWxzZSB7XG4gICAgICBtID0gRXZlbnRFbWl0dGVyLmRlZmF1bHRNYXhMaXN0ZW5lcnM7XG4gICAgfVxuXG4gICAgaWYgKG0gJiYgbSA+IDAgJiYgdGhpcy5fZXZlbnRzW3R5cGVdLmxlbmd0aCA+IG0pIHtcbiAgICAgIHRoaXMuX2V2ZW50c1t0eXBlXS53YXJuZWQgPSB0cnVlO1xuICAgICAgY29uc29sZS5lcnJvcignKG5vZGUpIHdhcm5pbmc6IHBvc3NpYmxlIEV2ZW50RW1pdHRlciBtZW1vcnkgJyArXG4gICAgICAgICAgICAgICAgICAgICdsZWFrIGRldGVjdGVkLiAlZCBsaXN0ZW5lcnMgYWRkZWQuICcgK1xuICAgICAgICAgICAgICAgICAgICAnVXNlIGVtaXR0ZXIuc2V0TWF4TGlzdGVuZXJzKCkgdG8gaW5jcmVhc2UgbGltaXQuJyxcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fZXZlbnRzW3R5cGVdLmxlbmd0aCk7XG4gICAgICBpZiAodHlwZW9mIGNvbnNvbGUudHJhY2UgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgLy8gbm90IHN1cHBvcnRlZCBpbiBJRSAxMFxuICAgICAgICBjb25zb2xlLnRyYWNlKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uID0gRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5hZGRMaXN0ZW5lcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vbmNlID0gZnVuY3Rpb24odHlwZSwgbGlzdGVuZXIpIHtcbiAgaWYgKCFpc0Z1bmN0aW9uKGxpc3RlbmVyKSlcbiAgICB0aHJvdyBUeXBlRXJyb3IoJ2xpc3RlbmVyIG11c3QgYmUgYSBmdW5jdGlvbicpO1xuXG4gIHZhciBmaXJlZCA9IGZhbHNlO1xuXG4gIGZ1bmN0aW9uIGcoKSB7XG4gICAgdGhpcy5yZW1vdmVMaXN0ZW5lcih0eXBlLCBnKTtcblxuICAgIGlmICghZmlyZWQpIHtcbiAgICAgIGZpcmVkID0gdHJ1ZTtcbiAgICAgIGxpc3RlbmVyLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgfVxuICB9XG5cbiAgZy5saXN0ZW5lciA9IGxpc3RlbmVyO1xuICB0aGlzLm9uKHR5cGUsIGcpO1xuXG4gIHJldHVybiB0aGlzO1xufTtcblxuLy8gZW1pdHMgYSAncmVtb3ZlTGlzdGVuZXInIGV2ZW50IGlmZiB0aGUgbGlzdGVuZXIgd2FzIHJlbW92ZWRcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlTGlzdGVuZXIgPSBmdW5jdGlvbih0eXBlLCBsaXN0ZW5lcikge1xuICB2YXIgbGlzdCwgcG9zaXRpb24sIGxlbmd0aCwgaTtcblxuICBpZiAoIWlzRnVuY3Rpb24obGlzdGVuZXIpKVxuICAgIHRocm93IFR5cGVFcnJvcignbGlzdGVuZXIgbXVzdCBiZSBhIGZ1bmN0aW9uJyk7XG5cbiAgaWYgKCF0aGlzLl9ldmVudHMgfHwgIXRoaXMuX2V2ZW50c1t0eXBlXSlcbiAgICByZXR1cm4gdGhpcztcblxuICBsaXN0ID0gdGhpcy5fZXZlbnRzW3R5cGVdO1xuICBsZW5ndGggPSBsaXN0Lmxlbmd0aDtcbiAgcG9zaXRpb24gPSAtMTtcblxuICBpZiAobGlzdCA9PT0gbGlzdGVuZXIgfHxcbiAgICAgIChpc0Z1bmN0aW9uKGxpc3QubGlzdGVuZXIpICYmIGxpc3QubGlzdGVuZXIgPT09IGxpc3RlbmVyKSkge1xuICAgIGRlbGV0ZSB0aGlzLl9ldmVudHNbdHlwZV07XG4gICAgaWYgKHRoaXMuX2V2ZW50cy5yZW1vdmVMaXN0ZW5lcilcbiAgICAgIHRoaXMuZW1pdCgncmVtb3ZlTGlzdGVuZXInLCB0eXBlLCBsaXN0ZW5lcik7XG5cbiAgfSBlbHNlIGlmIChpc09iamVjdChsaXN0KSkge1xuICAgIGZvciAoaSA9IGxlbmd0aDsgaS0tID4gMDspIHtcbiAgICAgIGlmIChsaXN0W2ldID09PSBsaXN0ZW5lciB8fFxuICAgICAgICAgIChsaXN0W2ldLmxpc3RlbmVyICYmIGxpc3RbaV0ubGlzdGVuZXIgPT09IGxpc3RlbmVyKSkge1xuICAgICAgICBwb3NpdGlvbiA9IGk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChwb3NpdGlvbiA8IDApXG4gICAgICByZXR1cm4gdGhpcztcblxuICAgIGlmIChsaXN0Lmxlbmd0aCA9PT0gMSkge1xuICAgICAgbGlzdC5sZW5ndGggPSAwO1xuICAgICAgZGVsZXRlIHRoaXMuX2V2ZW50c1t0eXBlXTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGlzdC5zcGxpY2UocG9zaXRpb24sIDEpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLl9ldmVudHMucmVtb3ZlTGlzdGVuZXIpXG4gICAgICB0aGlzLmVtaXQoJ3JlbW92ZUxpc3RlbmVyJywgdHlwZSwgbGlzdGVuZXIpO1xuICB9XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUFsbExpc3RlbmVycyA9IGZ1bmN0aW9uKHR5cGUpIHtcbiAgdmFyIGtleSwgbGlzdGVuZXJzO1xuXG4gIGlmICghdGhpcy5fZXZlbnRzKVxuICAgIHJldHVybiB0aGlzO1xuXG4gIC8vIG5vdCBsaXN0ZW5pbmcgZm9yIHJlbW92ZUxpc3RlbmVyLCBubyBuZWVkIHRvIGVtaXRcbiAgaWYgKCF0aGlzLl9ldmVudHMucmVtb3ZlTGlzdGVuZXIpIHtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMClcbiAgICAgIHRoaXMuX2V2ZW50cyA9IHt9O1xuICAgIGVsc2UgaWYgKHRoaXMuX2V2ZW50c1t0eXBlXSlcbiAgICAgIGRlbGV0ZSB0aGlzLl9ldmVudHNbdHlwZV07XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICAvLyBlbWl0IHJlbW92ZUxpc3RlbmVyIGZvciBhbGwgbGlzdGVuZXJzIG9uIGFsbCBldmVudHNcbiAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHtcbiAgICBmb3IgKGtleSBpbiB0aGlzLl9ldmVudHMpIHtcbiAgICAgIGlmIChrZXkgPT09ICdyZW1vdmVMaXN0ZW5lcicpIGNvbnRpbnVlO1xuICAgICAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoa2V5KTtcbiAgICB9XG4gICAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoJ3JlbW92ZUxpc3RlbmVyJyk7XG4gICAgdGhpcy5fZXZlbnRzID0ge307XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBsaXN0ZW5lcnMgPSB0aGlzLl9ldmVudHNbdHlwZV07XG5cbiAgaWYgKGlzRnVuY3Rpb24obGlzdGVuZXJzKSkge1xuICAgIHRoaXMucmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXJzKTtcbiAgfSBlbHNlIGlmIChsaXN0ZW5lcnMpIHtcbiAgICAvLyBMSUZPIG9yZGVyXG4gICAgd2hpbGUgKGxpc3RlbmVycy5sZW5ndGgpXG4gICAgICB0aGlzLnJlbW92ZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVyc1tsaXN0ZW5lcnMubGVuZ3RoIC0gMV0pO1xuICB9XG4gIGRlbGV0ZSB0aGlzLl9ldmVudHNbdHlwZV07XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmxpc3RlbmVycyA9IGZ1bmN0aW9uKHR5cGUpIHtcbiAgdmFyIHJldDtcbiAgaWYgKCF0aGlzLl9ldmVudHMgfHwgIXRoaXMuX2V2ZW50c1t0eXBlXSlcbiAgICByZXQgPSBbXTtcbiAgZWxzZSBpZiAoaXNGdW5jdGlvbih0aGlzLl9ldmVudHNbdHlwZV0pKVxuICAgIHJldCA9IFt0aGlzLl9ldmVudHNbdHlwZV1dO1xuICBlbHNlXG4gICAgcmV0ID0gdGhpcy5fZXZlbnRzW3R5cGVdLnNsaWNlKCk7XG4gIHJldHVybiByZXQ7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmxpc3RlbmVyQ291bnQgPSBmdW5jdGlvbih0eXBlKSB7XG4gIGlmICh0aGlzLl9ldmVudHMpIHtcbiAgICB2YXIgZXZsaXN0ZW5lciA9IHRoaXMuX2V2ZW50c1t0eXBlXTtcblxuICAgIGlmIChpc0Z1bmN0aW9uKGV2bGlzdGVuZXIpKVxuICAgICAgcmV0dXJuIDE7XG4gICAgZWxzZSBpZiAoZXZsaXN0ZW5lcilcbiAgICAgIHJldHVybiBldmxpc3RlbmVyLmxlbmd0aDtcbiAgfVxuICByZXR1cm4gMDtcbn07XG5cbkV2ZW50RW1pdHRlci5saXN0ZW5lckNvdW50ID0gZnVuY3Rpb24oZW1pdHRlciwgdHlwZSkge1xuICByZXR1cm4gZW1pdHRlci5saXN0ZW5lckNvdW50KHR5cGUpO1xufTtcblxuZnVuY3Rpb24gaXNGdW5jdGlvbihhcmcpIHtcbiAgcmV0dXJuIHR5cGVvZiBhcmcgPT09ICdmdW5jdGlvbic7XG59XG5cbmZ1bmN0aW9uIGlzTnVtYmVyKGFyZykge1xuICByZXR1cm4gdHlwZW9mIGFyZyA9PT0gJ251bWJlcic7XG59XG5cbmZ1bmN0aW9uIGlzT2JqZWN0KGFyZykge1xuICByZXR1cm4gdHlwZW9mIGFyZyA9PT0gJ29iamVjdCcgJiYgYXJnICE9PSBudWxsO1xufVxuXG5mdW5jdGlvbiBpc1VuZGVmaW5lZChhcmcpIHtcbiAgcmV0dXJuIGFyZyA9PT0gdm9pZCAwO1xufVxuIiwiLyogXHJcbiAqIFRvIGNoYW5nZSB0aGlzIGxpY2Vuc2UgaGVhZGVyLCBjaG9vc2UgTGljZW5zZSBIZWFkZXJzIGluIFByb2plY3QgUHJvcGVydGllcy5cclxuICogVG8gY2hhbmdlIHRoaXMgdGVtcGxhdGUgZmlsZSwgY2hvb3NlIFRvb2xzIHwgVGVtcGxhdGVzXHJcbiAqIGFuZCBvcGVuIHRoZSB0ZW1wbGF0ZSBpbiB0aGUgZWRpdG9yLlxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIE1vdXNlSGFuZGxlcihjYW52YXMpIHtcclxuICAgIFxyXG4gICAgLypcclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIHt0eXBlfSBjYW52YXNcclxuICAgICAqIEByZXR1cm5zIHt1bmRlZmluZWR9Y2FudmFzIC0galF1ZXJ5IG9iamVjdFxyXG4gICAgICovXHJcbiAgICB2YXIgdGhhdCA9IHRoaXM7XHJcbiAgICB0aGlzLmluaXQgPSBmdW5jdGlvbiAoY2FudmFzKSB7XHJcblxyXG4gICAgICAgIHRoaXMuY2FudmFzID0gY2FudmFzO1xyXG4gICAgICAgIHRoaXMuY29udGV4dCA9IHRoaXMuY2FudmFzWzBdLmdldENvbnRleHQoJzJkJyk7IC8vIHNldCB1cCBjb250ZXh0IGZvciByZW5kZXIgZnVuY3Rpb25cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuaW5pdChjYW52YXMpO1xyXG4gICAgXHJcbiAgICB0aGlzLmNvbnZlcnRUb0NhbnZhc0Nvb3JkcyA9IGZ1bmN0aW9uKHBhZ2VYLCBwYWdlWSl7XHJcbiAgICAgICAgLy9pbiBjYXNlIGNhbnZhcyBnZXQgbW92ZWQgZHVyaW5nIHRoZSBwYWdlIGxpZmVjeWNsZVxyXG4gICAgICAgIHZhciBvZmZzZXQgPSBjYW52YXMub2Zmc2V0KCk7XHJcblxyXG4gICAgICAgIGNvbnN0IHggPSBwYWdlWCAtIG9mZnNldC5sZWZ0O1xyXG4gICAgICAgIGNvbnN0IHkgPSBwYWdlWSAtIG9mZnNldC50b3A7XHJcblxyXG4gICAgICAgIHJldHVybiB7XCJ4XCI6eCwgXCJ5XCI6eX07XHJcbiAgICB9O1xyXG5cclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBNb3VzZUhhbmRsZXI7XHJcbiIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qXHJcbiAqIEEgY2VudHJhbCBmYWN0b3J5IHdoaWNoIHByb2R1Y2VzIHRoZSBkYXRhIHN0cnVjdHVyZXMgZm9yIGFsbCBldmVudHNcclxuICogXHJcbiAqIFJhdGhlciB0aGFuIGNyZWF0aW5nIGRpZmZlcmVudCBldmVudHMgYWxsIG92ZXIgdGhlIGNvZGUgYSBjZW50cmFsIGZhY3RvcnlcclxuICogc2hvdWxkIGFsbG93IGZvciBiZXR0ZXIgY29udHJvbCBhbmQgY2hhbmdlIG1hbmFnZW1lbnQuIEluIG90aGVyIHdvcmRzXHJcbiAqIGluIG9yZGUgdG8gZmlyZSBhbiBldmVudCBhIHN0cnVjdHVyZSBmb3IgdGhlIGV2ZW50IG11c3QgYmUgdGFrZW4gZnJvbVxyXG4gKiB0aGlzIGZhY3RvcnkgYW5kIGl0cyBmaWVsZHMgZmlsbGVkIGFzIHJlcXVpcmVkLlxyXG4gKi9cclxuY2xhc3MgRXZlbnRGYWN0b3J5IHtcclxuICAgXHRcclxuICAgXHQvKlxyXG4gICBcdCAqIFNvbWUgY29uc2lkZXJhdGlvbiBmb3IgbW91c2VNb3ZlIGV2ZW50XHJcblx0ICAgICpcclxuICAgXHQgKiBSYXRoZXIgdGhhbiBjcmVhdGluZyBhIGRyYXdpbmcgZXZlbnQgd2l0aCBjaGFuZ2luZyBtb3VzZSBjb29yZGluYXRlc1xyXG4gICBcdCAqIGZvciB0aGlzIGl0ZXJhdGlvbiBkZWNpZGVkIHRoYXQgY3JlYXRpbiBhIGdlbmVyaWMgZXZlbnQgd2l0aCBwcm9wZXJ0aWVzIHN1Y2ggYXNcclxuICAgXHQgKiBpc0RyYXdpbmcnIGlzIHByZWZlcnJlZC4gV2lsbCBzZWUgaWYgaXQgZ29lcyBvdXQgb2YgY29udHJvbCB3aXRoIGEgbnVtYmVyIG9mIHJlcXVpcmVkXHJcbiAgIFx0ICogcHJvcGVydGllcywgaW4gdGhpcyBjYXNlIG1pZ2h0IHJldmlzaXQgdGhlIGFwcHJvYWNoLlxyXG5cdCAgICAqL1xyXG4gICBcdHN0YXRpYyBnZXRNb3VzZU1vdmVFdmVudE9iamVjdCAoKSB7XHJcbiAgIFx0XHRyZXR1cm4ge1xyXG4gICBcdFx0XHRtb3VzZUNvb3Jkc092ZXJDYW52YXM6IHsgLy9tb3VzZSBjb29yZGluYXRlcyBvdmVyIGNhbnZhcyAobm90IGFkanVzdGVkIHRvIHRoZSBjdXJyZW50IHBhbiBhbmQgem9vbSlcclxuICAgXHRcdFx0XHR4OiBcIlwiLFxyXG4gICBcdFx0XHRcdHk6IFwiXCJcclxuICAgXHRcdFx0fSxcclxuICAgICAgICAgICAgbW91c2VDb29yZHNPdmVyQ2FudmFzQWRqdXN0ZWQ6IHsgLy9tb3VzZSBjb29yZGluYXRlcyBvdmVyIGNhbnZhcyBhZGp1c3RlZCB0byB0aGUgY3VycmVudCBwYW4gYW5kIHpvb21cclxuICAgICAgICAgICAgICAgeDogXCJcIixcclxuICAgICAgICAgICAgICAgeTogXCJcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICBcdFx0XHRpc0RyYXdpbmc6ZmFsc2UsXHJcbiAgIFx0XHRcdGlzUGFubmluZzpmYWxzZSxcclxuICAgICAgICAgICAgcGFuRGlzcGxhY2VtZW50OjAsXHJcbiAgICAgICAgICAgIHpvb21MZXZlbDoxLFxyXG4gICAgICAgICAgICBvcmlnaW46e30sIC8vIHdoZXJlIHRoZSBtb3ZlIGV2ZW50IHN0YXJ0ZWRcclxuICAgICAgICAgICAgY3VycmVudERyYXdpbmdNb2RlOjAgLy8gdGhpcyBpcyBmb3IgY2FzZXMgd2hlbiBpc0RyYXdpbmcgaXMgc2V0IHRvIHRydWUgb25lIG5lZWRzIHRvIGtub3cgd2hhdCBTaGFwZSB0byBjcmVhdGUgaW4gb25Nb3VzZURvd25cclxuICAgXHRcdH1cclxuICAgXHR9XHJcblxyXG4gICBcdC8qXHJcbiAgIFx0ICogU29tZSBjb25zaWRlcmF0aW9uIGZvciBtb3VzZURvd24gZXZlbnRcclxuXHQgICAgKlxyXG4gICBcdCAqIEEgY291cGxlIG9mIG9wdGlvbnMgaGVyZTpcclxuICAgXHQgKiAgLSBwYXNzIG9yaWdpbiBhbHJlYWR5IGFkanVzdGVkIHRvIHBhbiBhbmQgem9vbVxyXG4gICBcdCAqICAtIHBhc3MgcmF3IGNvb3JkaW5hdGVzIGFuZCBhZGp1c3Qgb24gdGhlIGNsaWVudFxyXG4gICBcdCAqXHJcblx0ICAgICovXHJcbiAgIFx0c3RhdGljIGdldE1vdXNlRG93bkV2ZW50T2JqZWN0ICgpIHtcclxuICAgXHRcdHJldHVybiB7XHJcbiAgIFx0XHRcdG1vdXNlQ29vcmRzT3ZlckNhbnZhc0FkanVzdGVkOiB7IC8vbW91c2UgY29vcmRpbmF0ZXMgb3ZlciBjYW52YXMgYWRqdXN0ZWQgdG8gdGhlIGN1cnJlbnQgcGFuIGFuZCB6b29tXHJcbiAgIFx0XHRcdFx0eDogXCJcIixcclxuICAgXHRcdFx0XHR5OiBcIlwiXHJcbiAgIFx0XHRcdH0sXHJcbiAgIFx0XHRcdGJ1dHRvbjogXCJsZWZ0XCIsXHJcbiAgICAgICAgICAgIGlzRHJhd2luZzpmYWxzZVxyXG4gICBcdFx0fVxyXG4gICBcdH1cclxuXHJcbiAgIFx0LypcclxuICAgXHQgKiBFdmVudCBmb3Igd2hlbiBjbGllbnQgem9vbXMgaW4gb3Igb3V0XHJcbiAgIFx0ICpcclxuXHQgICAgKi9cclxuICAgXHRzdGF0aWMgZ2V0Wm9vbUV2ZW50T2JqZWN0ICgpIHtcclxuICAgXHRcdHJldHVybiB7XHJcbiAgIFx0XHRcdGRpcmVjdGlvbjogMFxyXG4gICBcdFx0fVxyXG4gICBcdH1cclxuICAgICAgLypcclxuICAgICAgICogRXZlbnQgZm9yIHdoZW4gY2xpZW50IHpvb21zIGluIG9yIG91dCwgcGFucyAuLi5cclxuICAgICAgICogQW55IHRpbWUgdHJhbnNmb3JtIG1hdHJpeCBuZWVkcyB0byBiZSBhZGp1c3RlZCBvbiBhbnkgbGF5ZXJcclxuICAgICAgICovXHJcbiAgICAgIHN0YXRpYyBnZXRDaGFuZ2VUcmFuc2Zvcm1NYXRyaXhPYmplY3QgKCkge1xyXG4gICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB6b29tTGV2ZWw6IDEsXHJcbiAgICAgICAgICAgIG9yaWdpbjp7fSxcclxuICAgICAgICAgICAgcGFuRGlzcGxhY2VtZW50OiB7IC8vcGFuIGRpc3BsYWNlbWVudCB2YWx1ZXNcclxuICAgICAgICAgICAgICAgeDogXCJcIixcclxuICAgICAgICAgICAgICAgeTogXCJcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBtb3VzZUNvb3Jkc092ZXJDYW52YXM6IHsgLy8vL21vdXNlIGNvb3JkaW5hdGVzIG92ZXIgY2FudmFzIChub3QgYWRqdXN0ZWQgdG8gdGhlIGN1cnJlbnQgcGFuIGFuZCB6b29tKSwgdGVtcG9yYXJ5IGZvciBiYWNrd2FyZHMgY29wbWF0aWJpbGl0eVxyXG4gICAgICAgICAgICAgICB4OiBcIlwiLFxyXG4gICAgICAgICAgICAgICB5OiBcIlwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgfVxyXG4gICAgICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gRXZlbnRGYWN0b3J5OyIsInZhciBTaGFwZUZhY3RvcnkgPSByZXF1aXJlKCcuLi9zaGFwZXMvU2hhcGVGYWN0b3J5Jyk7XHJcbi8qIFxyXG4gKiBUaGlzIGlzIGEgbW9kZWwgc3VwcG9ydHMgaW5mb3JtYXRpb24gKGJsb2NrKSBkaXNwbGF5XHJcbiAqIGZvciBhIGN1cnJlbnRseSBzZWxlY3RlZCBzaGFwZVxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIEluZm9CbG9jayhvcHRpb25zKSB7XHJcbiAgICBcclxuICAgIC8qXHJcbiAgICAgKiBAcGFyYW0ge3R5cGV9IGNhbnZhc1xyXG4gICAgICogQHJldHVybnMge3VuZGVmaW5lZH1jYW52YXMgLSBqUXVlcnkgb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIHZhciB0aGF0ID0gdGhpcztcclxuICAgIHRoaXMuaW5pdCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XHJcblxyXG4gICAgICAgIHRoaXMuYmFzaWNNb2RlbCA9IG9wdGlvbnMuYmFzaWNNb2RlbDtcclxuXHJcbiAgICAgICAgdGhpcy5wdWJzdWIgPSBvcHRpb25zLnB1YnN1YjtcclxuXHJcbiAgICAgICAgLy8kLnN1YnNjcmliZSgnY2xpZW50X3pvb21pbmcnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgICAgLy90aGF0LmhhbmRsZVpvb21FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgLy99KTtcclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdjbGllbnRfem9vbWluZycsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVab29tRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgICAgdGhpcy5wdWJzdWIub24oJ3VzZXJzX3VwZGF0ZWQnLCBmdW5jdGlvbihvYmopIHtcclxuICAgICAgICAgICAgICAgIHRoYXQuaGFuZGxlVXNlcnNVcGRhdGVkLmNhbGwodGhhdCwgb2JqKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKiQuc3Vic2NyaWJlKCdzb2NrZXRfem9vbWluZycsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LmhhbmRsZVpvb21FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdzb2NrZXRfem9vbWluZycsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVab29tRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLyokLnN1YnNjcmliZSgnY2xpZW50X3Bhbm5pbmcnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgICAgdGhhdC5oYW5kbGVQYW5FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdjbGllbnRfcGFubmluZycsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVQYW5FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLyokLnN1YnNjcmliZSgnc29ja2V0X3Bhbm5pbmcnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgICAgdGhhdC5oYW5kbGVQYW5FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdzb2NrZXRfcGFubmluZycsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVQYW5FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvKiQuc3Vic2NyaWJlKCdtb3VzZV9tb3ZlJywgZnVuY3Rpb24oIGV2ZW50LCBvYmogKSB7XHJcbiAgICAgICAgICAgIHRoYXQuaGFuZGxlTW91c2VNb3ZlRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgIH0pOyovXHJcbiAgICAgICAgaWYodGhpcy5wdWJzdWIpe1xyXG4gICAgICAgICAgICB0aGlzLnB1YnN1Yi5vbignbW91c2VfbW92ZScsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZU1vdmVFdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAkLnN1YnNjcmliZSgnc2hhcGVfc2VsZWN0ZWQnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgICAgdGhhdC5oYW5kbGVTaGFwZVNlbGVjdGVkRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQuc3Vic2NyaWJlKCdkZWJ1Z19zaG93X3NoYXBlcycsIGZ1bmN0aW9uKCBldmVudCApIHtcclxuICAgICAgICAgICAgY29uc29sZS5pbmZvKHRoYXQuYmFzaWNNb2RlbC5nZXRTaGFwZXNSYXdGb3JEZWJ1Z2dpbmcoKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJC5zdWJzY3JpYmUoJ3Byb3BlcnRpZXNfZm9ybV9zdWJtaXNzaW9uJywgZnVuY3Rpb24oIGV2ZW50LCBvYmogKSB7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAvL2pRdWVyeShcIiNwcm9wZXJ0aWVzRm9ybVwiKS5oaWRlKCk7XHJcbiAgICAgICAgICAgIGpRdWVyeShcIiNteU1vZGFsXCIpLnJlbW92ZSgpO1xyXG5cclxuICAgICAgICAgICAgdmFyIGZvcm1fZGF0YSA9IG9iai5zcGxpdCgnJicpO1xyXG4gICAgICAgICAgICB2YXIgaW5wdXQgICAgID0ge307XHJcblxyXG4gICAgICAgICAgICAkLmVhY2goZm9ybV9kYXRhLCBmdW5jdGlvbihrZXksIHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IHZhbHVlLnNwbGl0KCc9Jyk7XHJcbiAgICAgICAgICAgICAgICBpZihkYXRhWzBdID09PSBcInBvaW50c1wiKXtcclxuICAgICAgICAgICAgICAgICAgICBpbnB1dFtkYXRhWzBdXSA9IEpTT04ucGFyc2UoZGVjb2RlVVJJQ29tcG9uZW50KGRhdGFbMV0pKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTsgLy8gaXQgaXMgbm90ICdjb250aW51ZScgYmVjYXVzZSB3ZSBhcmUgaW4gJ2VhY2gnIGxvb3Agbm90IHJlZ3VsYXIgJ2ZvciBsb29wJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaW5wdXRbZGF0YVswXV0gPSBkZWNvZGVVUklDb21wb25lbnQoZGF0YVsxXSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBsZXQgbmV3U2hhcGUgPSBudWxsO1xyXG4gICAgICAgICAgICB0cnl7XHJcbiAgICAgICAgICAgICAgICBuZXdTaGFwZSA9IFNoYXBlRmFjdG9yeS5jcmVhdGVGcm9tSlNPTihpbnB1dCk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2goZXgpe1xyXG4gICAgICAgICAgICAgICAgLy9zaG91bGQgbWF5IGJlIHBvcHVwIHVzZXIgbWVzc2FnZSBhYm91dCBhbiBhdHRlbXB0IHRvIGNyZWF0ZSBpbnZhbGlkIHNoYXBlIC4uLlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZiAobmV3U2hhcGUgIT09IG51bGwgJiYgbmV3U2hhcGUuaXNWYWxpZCgpKXtcclxuICAgICAgICAgICAgICAgIHRoYXQuYmFzaWNNb2RlbC51cGRhdGVTaGFwZShpbnB1dC5zZWxlY3RlZEluZGV4LCBuZXdTaGFwZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhhdC5iYXNpY01vZGVsLmVtaXRTaGFwZVVwZGF0ZWQoaW5wdXQuc2VsZWN0ZWRJbmRleCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJC5zdWJzY3JpYmUoJ3Nob3dfcHJvcGVydGllc19kaWFsb2cnLCBmdW5jdGlvbiggZXZlbnQsIGluZGV4ICkge1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgc2hhcGUgPSB0aGF0LmJhc2ljTW9kZWwuZ2V0U2hhcGUoaW5kZXgpO1xyXG4gICAgICAgICAgICAvL2RlZmF1bHQsIGlmIG5vdCBoc2FwZSBpcyBzZWxlY3RlZCBwb3B1cCBpbmZvcm1hdGl2ZSBtZXNzYWdlXHJcbiAgICAgICAgICAgIGxldCBodG1sID0gJzwhLS0gTW9kYWwgLS0+ICcgK1xyXG4gICAgICAgICAgICAgICAgICAgICc8ZGl2IGlkPVwibXlNb2RhbFwiIGNsYXNzPVwibW9kYWwgZmFkZVwiIHJvbGU9XCJkaWFsb2dcIj4nICtcclxuICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibW9kYWwtZGlhbG9nXCI+JyArXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnPCEtLSBNb2RhbCBjb250ZW50LS0+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibW9kYWwtaGVhZGVyXCI+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjbG9zZVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCI+JnRpbWVzOzwvYnV0dG9uPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCI+Tm8gc2hhcGUgc2VsZWN0ZWQhPC9oND4nICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAnPC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPGZvcm0gaWQ9XCJwcm9wZXJ0aWVzRm9ybVwiPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIiBpZD1cInNpemluZy1hZGRvbjNcIj5QbGVhc2Ugc2VsZWN0IGEgc2hhcGU8L3NwYW4+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC9mb3JtPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPicrXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHRcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+JytcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAnPC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nICtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAnPC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgJzwvZGl2Pic7XHJcbiAgICAgICAgICAgIC8vaWYgdGhlcmUgaXMgYSBzZWxlY3RlZCBzaGFwZSwgb3ZlcndyaXRlIGRlZmF1bHQgdGhlblxyXG4gICAgICAgICAgICBpZiAoc2hhcGUgIT0gbnVsbCApe1xyXG4gICAgICAgICAgICAgICAgaHRtbCA9ICc8IS0tIE1vZGFsIC0tPiAnICtcclxuICAgICAgICAgICAgICAgICAgICAnPGRpdiBpZD1cIm15TW9kYWxcIiBjbGFzcz1cIm1vZGFsIGZhZGVcIiByb2xlPVwiZGlhbG9nXCI+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm1vZGFsLWRpYWxvZ1wiPicgK1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgJzwhLS0gTW9kYWwgY29udGVudC0tPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj4nICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPiZ0aW1lczs8L2J1dHRvbj4nICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8aDQgY2xhc3M9XCJtb2RhbC10aXRsZVwiPicgKyBzaGFwZS5jb25zdHJ1Y3Rvci5uYW1lICsgJzwvaDQ+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJzwvZGl2PicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxmb3JtIGlkPVwicHJvcGVydGllc0Zvcm1cIj4nKyBzaGFwZS50b0hUTUxTdHJpbmcoKSArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPGlucHV0IG5hbWU9XCJ0eXBlXCIgdHlwZT1cImhpZGRlblwiIHZhbHVlPVwiJyArIHNoYXBlLmNvbnN0cnVjdG9yLm5hbWUgKyAnXCIvPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxpbnB1dCBuYW1lPVwic2VsZWN0ZWRJbmRleFwiIHR5cGU9XCJoaWRkZW5cIiB2YWx1ZT1cIicgKyBpbmRleCArICdcIi8+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC9mb3JtPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPicgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0XCIgb25DbGljaz1cIiQucHVibGlzaChcXCdwcm9wZXJ0aWVzX2Zvcm1fc3VibWlzc2lvblxcJywgJChcXCcjcHJvcGVydGllc0Zvcm1cXCcpLnNlcmlhbGl6ZSgpKVwiPlN1Ym1pdDwvYnV0dG9uPicrXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHRcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPkNsb3NlPC9idXR0b24+JytcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAnPC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nICtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAnPC9kaXY+JyArXHJcbiAgICAgICAgICAgICAgICAgICAgJzwvZGl2Pic7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vJChcIiNteU1vZGFsXCIpLm1vZGFsKCk7XHJcbiAgICAgICAgICAgIHZhciBtb2RhbCA9ICQoaHRtbCk7XHJcbiAgICAgICAgICAgIG1vZGFsLm1vZGFsKHtiYWNrZHJvcDogZmFsc2V9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5pbml0KG9wdGlvbnMpO1xyXG4gICAgXHJcbiAgICB0aGlzLmhhbmRsZVVzZXJzVXBkYXRlZCA9IGZ1bmN0aW9uIChvYmopIHtcclxuXHJcbiAgICAgICAgY29uc3QgdXNlcnMgPSBvYmoudXNlcnMuZmlsdGVyKCBmdW5jdGlvbihpdGVtKXtcclxuICAgICAgICAgICAgcmV0dXJuIGl0ZW0udXVpZCAhPT0gb2JqLnNlbGZVVUlEO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgJChcIiNudW1iZXJfb2Zfcm9vbV92aXNpdG9yc1wiKS5odG1sKCh1c2Vycy5sZW5ndGggKzEpKTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuaGFuZGxlWm9vbUV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG4gICAgICAgICQoXCIjem9vbV92YWx1ZVwiKS5odG1sKG9iai56b29tTGV2ZWwpO1xyXG4gICAgfTtcclxuICAgIFxyXG4gICAgdGhpcy5oYW5kbGVQYW5FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICAkKFwiI3Bhbl94XCIpLmh0bWwob2JqLnBhbkRpc3BsYWNlbWVudC54KTsgICAgICAgIFxyXG4gICAgICAgICQoXCIjcGFuX3lcIikuaHRtbChvYmoucGFuRGlzcGxhY2VtZW50LnkpO1xyXG4gICAgfTtcclxuICAgIHRoaXMuaGFuZGxlTW91c2VNb3ZlRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcblxyXG4gICAgICAgICQoXCIjbW91c2VfeFwiKS5odG1sKG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMueCk7ICAgICAgICBcclxuICAgICAgICAkKFwiI21vdXNlX3lcIikuaHRtbChvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzLnkpOyBcclxuXHJcbiAgICAgICAgJChcIiNtb3VzZV9hZGp1c3RlZF94XCIpLmh0bWwob2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhc0FkanVzdGVkLngpOyAgICAgICAgXHJcbiAgICAgICAgJChcIiNtb3VzZV9hZGp1c3RlZF95XCIpLmh0bWwob2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhc0FkanVzdGVkLnkpOyBcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogcGFyYW0gb2JqIDogXHJcbiAgICAgKi9cclxuICAgIHRoaXMuaGFuZGxlU2hhcGVTZWxlY3RlZEV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG5cclxuICAgICAgICBjb25zdCBpbnB1dEdyb3VwU3RhcnQgPSAnPGRpdiBjbGFzcz1cInBhbmVsIHBhbmVsLWRlZmF1bHRcIiBzdHlsZT1cIm1hcmdpbjo0cHg7XCI+JztcclxuICAgICAgICBjb25zdCBpbnB1dEdyb3VwRW5kID0gJzwvZGl2Pic7XHJcbiAgICAgICAgLy9jb25zdCBidXR0b24gPSAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLXNtXCIgZGF0YS10b2dnbGU9XCJtb2RhbFwiIGRhdGEtdGFyZ2V0PVwiI215TW9kYWxcIj5Qcm9wZXJ0aWVzPC9idXR0b24+JztcclxuICAgICAgICBjb25zdCBidXR0b24gPSAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXByaW1hcnkgYnRuLXNtXCIgb25DbGljaz1cIiQucHVibGlzaChcXCdzaG93X3Byb3BlcnRpZXNfZGlhbG9nXFwnLCAnKyBvYmouc2VsZWN0ZWRTaGFwZUluZGV4ICsnKVwiID5Qcm9wZXJ0aWVzPC9idXR0b24+JztcclxuXHJcbiAgICAgICAgJChcIiNzaGFwZV9wcm9wZXJ0aWVzXCIpLmh0bWwoaW5wdXRHcm91cFN0YXJ0ICsgdGhhdC5iYXNpY01vZGVsLmdldFNoYXBlKG9iai5zZWxlY3RlZFNoYXBlSW5kZXgpLnRvU3RyaW5nKCkgKyBcIjxiciAvPlwiICsgYnV0dG9uICsgaW5wdXRHcm91cEVuZCk7XHJcbiAgICB9O1xyXG4gICAgXHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSW5mb0Jsb2NrOyIsIi8vU2hvdWxkIGl0IGJlIGEgU2luZ2xldG9uIHBlcmhhcHM/XHJcblxyXG4vLyBpbnRlcmVzdGluZyBkaXNjdXNzaW9uOlxyXG4vLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzM2ODA0MjkvY2xpY2stdGhyb3VnaC1hLWRpdi10by11bmRlcmx5aW5nLWVsZW1lbnRzXHJcbi8vIHRoZXJlIGlzIGFuIG9wdGlvbiB0aGVyZSBcclxuLypcclxuICAgICAkKCcjZWxlbWVudG9udG9wKS5jbGljayhmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICQoJyNlbGVtZW50b250b3ApLmhpZGUoKTtcclxuICAgICAgICAkKGRvY3VtZW50LmVsZW1lbnRGcm9tUG9pbnQoZS5jbGllbnRYLCBlLmNsaWVudFkpKS50cmlnZ2VyKFwiY2xpY2tcIik7XHJcbiAgICAgICAgJCgnI2VsZW1lbnRvbnRvcCcpLnNob3coKTtcclxuICAgIH0pO1xyXG4gKi9cclxuY2xhc3MgTGF5ZXJBUEkge1xyXG4gICAgc3RhdGljIGNyZWF0ZSAob3JpZ2luYWxDYW52YXMsIGlkLCB6SW5kZXgpIHtcclxuICAgICAgICB2YXIgb3JpZ2luYWxDYW52YXNPZmZzZXQgPSBvcmlnaW5hbENhbnZhcy5vZmZzZXQoKTtcclxuICAgICAgICB2YXIgY2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJyk7XHJcbiAgICAgICAgY2FudmFzLmlkICAgICA9IGlkO1xyXG4gICAgICAgIGNhbnZhcy5zdHlsZS50b3AgPSAgb3JpZ2luYWxDYW52YXNPZmZzZXQudG9wICsgXCJweFwiO1xyXG4gICAgICAgIGNhbnZhcy5zdHlsZS5sZWZ0ID0gIG9yaWdpbmFsQ2FudmFzT2Zmc2V0LmxlZnQgKyBcInB4XCI7XHJcbiAgICAgICAgXHJcbiAgICAgICAgY2FudmFzLnN0eWxlLnBvaW50ZXJFdmVudHMgPSBcIm5vbmVcIjtcclxuXHJcbiAgICAgICAgY2FudmFzLndpZHRoICA9IG9yaWdpbmFsQ2FudmFzLndpZHRoKCk7XHJcbiAgICAgICAgY2FudmFzLmhlaWdodCA9IG9yaWdpbmFsQ2FudmFzLmhlaWdodCgpO1xyXG4gICAgICAgIGNhbnZhcy5zdHlsZS56SW5kZXggICA9IDg7XHJcbiAgICAgICAgaWYoekluZGV4KXtcclxuICAgICAgICAgICAgY2FudmFzLnN0eWxlLnpJbmRleCAgID0gekluZGV4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYW52YXMuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XHJcbiAgICAgICAgY2FudmFzLnN0eWxlLmJvcmRlciAgID0gXCJub25lXCI7XHJcbiAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChjYW52YXMpO1xyXG4gICAgICAgIHJldHVybiBjYW52YXM7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIG9uZU9mIChvcmlnaW5hbENhbnZhcywgaWQpIHtcclxuICAgICAgICBpZihvcmlnaW5hbENhbnZhcyl7XHJcbiAgICAgICAgICAgIHZhciBvZmZzZXQgPSAyMDtcclxuICAgICAgICAgICAgdmFyIG9yaWdpbmFsQ2FudmFzT2Zmc2V0ID0gb3JpZ2luYWxDYW52YXMub2Zmc2V0KCk7XHJcbiAgICAgICAgICAgIHZhciBjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTtcclxuICAgICAgICAgICAgY2FudmFzLmlkICAgICA9IGlkO1xyXG4gICAgICAgICAgICBjYW52YXMuY3VzdG9tT2Zmc2V0ID0gb2Zmc2V0OyAvLyB1c2VkIHdoZW4gdGhlIGNuYXZhcyBpcyByZS1kcmF3biBUT0RPOiBsb29rIGludG8gd2h5IHRoaXMgaXMgbmVlZGVkLlxyXG4gICAgICAgICAgICBjYW52YXMuc3R5bGUudG9wID0gIChvcmlnaW5hbENhbnZhc09mZnNldC50b3AgLSBvZmZzZXQpICsgXCJweFwiO1xyXG4gICAgICAgICAgICBjYW52YXMuc3R5bGUubGVmdCA9ICAob3JpZ2luYWxDYW52YXNPZmZzZXQubGVmdCAtIG9mZnNldCkgKyBcInB4XCI7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBjYW52YXMuc3R5bGUucG9pbnRlckV2ZW50cyA9IFwibm9uZVwiO1xyXG5cclxuICAgICAgICAgICAgY2FudmFzLndpZHRoICA9IG9yaWdpbmFsQ2FudmFzLndpZHRoKCkgKyBvZmZzZXQ7XHJcbiAgICAgICAgICAgIGNhbnZhcy5oZWlnaHQgPSBvcmlnaW5hbENhbnZhcy5oZWlnaHQoKSArIG9mZnNldDtcclxuICAgICAgICAgICAgY2FudmFzLnN0eWxlLnpJbmRleCAgID0gOTtcclxuICAgICAgICAgICAgY2FudmFzLnN0eWxlLnBvc2l0aW9uID0gXCJhYnNvbHV0ZVwiO1xyXG4gICAgICAgICAgICBjYW52YXMuc3R5bGUuYm9yZGVyICAgPSBcIm5vbmVcIjtcclxuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChjYW52YXMpO1xyXG4gICAgICAgICAgICByZXR1cm4gY2FudmFzO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBMYXllckFQSTsiLCIvKlxyXG4gKiBUcmFuc2Zvcm0gdHJhY2tlclxyXG4gKlxyXG4gKiBAYXV0aG9yIEtldmluIE1vb3QgPGtldmluLm1vb3RAZ21haWwuY29tPlxyXG4gKiBCYXNlZCBvbiBhIGNsYXNzIGNyZWF0ZWQgYnkgU2ltb24gU2FycmlzIC0gd3d3LnNpbW9uc2FycmlzLmNvbSAtIHNhcnJpc0BhY20ub3JnXHJcbiAqIFxyXG4gKiBodHRwczovL2dpdGh1Yi5jb20vc2ltb25zYXJyaXMvQ2FudmFzLXR1dG9yaWFscy9ibG9iL21hc3Rlci90cmFuc2Zvcm0uanNcclxuICogXHJcbiAqIEl0IHVzZXMgc2V0VHJhbnNmb3JtKCkgSFRNTCBDYW52YXMgZnVuY3Rpb24gdG8gcmVzZXQgdGhlIGN1cnJlbnQgdHJhbnNmb3JtYXRpb24gdG8gdGhlIGlkZW50ZXR5IG1hdHJpeFxyXG4gKiBBZnRlciBldmVyeSB0cmFuc2xhdGUsIHNjYWxlIC4uLiBzZXRUcmFuc2Zvcm0gaXMgY2FsbGVkIHdoaWNoIG1lYW5zIHRoYXQgYWxsIGNvbnNlcXV0aXZlIHRyYW5zZm9ybWF0aW9uXHJcbiAqIHdpbGwgb25seSBhZmZlY3QgZHJhd2luZ3MgbWFkZSBhZnRlciB0aGUgc2V0VHJhbnNmb3JtIG1ldGhvZCBpcyBjYWxsZWQuIFdoaWNoIGFsbG93cyBmb3Igem9vbSB0byBzaW1wbHlcclxuICogZ28gdXAuIDEuMSAuLi4gMS4yIC4uLiAxLjMgYW5kIHRoZW4gLi4uIDEuMiB3aWxsYSBjdHVhbGx5IHNjYWxlIGRvd24uIEluIHVzdWFsIGNvbnRleHQgKHdpdGhvdXQgdGhpcyB0cmFuc2Zvcm0gXHJcbiAqIGxpYnJhcnkpIGdvaW5nIHRvIC4uLiAxLjIgd2lsbCBhY3R1YWxseSBzdGlsbCBzY2FsZSB1cCAgYnkgYSBmYWN0b3Igb2YgMS4yIGluc3RlYWQgb2Ygc2NhbGluZyBkb3duIGJ5IDAuMSBcclxuICovXHJcblxyXG52YXIgRGVjaW1hbCA9IHJlcXVpcmUoJ2RlY2ltYWwuanMnKTtcclxuXHJcbmZ1bmN0aW9uIFRyYW5zZm9ybShjb250ZXh0KSB7XHJcblxyXG4gICAgLy8gU2V0IHRoZSBwcmVjaXNpb24gYW5kIHJvdW5kaW5nIG9mIHRoZSBkZWZhdWx0IERlY2ltYWwgY29uc3RydWN0b3JcclxuICAgIERlY2ltYWwuc2V0KHsgcHJlY2lzaW9uOiAxMCwgcm91bmRpbmc6IDYgfSlcclxuICAgIFxyXG4gICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcclxuICAgIHRoaXMubWF0cml4ID0gW25ldyBEZWNpbWFsKDEpLCBuZXcgRGVjaW1hbCgwKSwgbmV3IERlY2ltYWwoMCksIG5ldyBEZWNpbWFsKDEpLCBuZXcgRGVjaW1hbCgwKSwgbmV3IERlY2ltYWwoMCldOyAvL2luaXRpYWxpemUgd2l0aCB0aGUgaWRlbnRpdHkgbWF0cml4XHJcbiAgICB0aGlzLnN0YWNrID0gW107XHJcbiAgICBcclxuICAgIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgICAvLyBDb25zdHJ1Y3RvciwgZ2V0dGVyL3NldHRlclxyXG4gICAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gICAgXHJcbiAgICBcclxuICAgIHRoaXMuc2V0Q29udGV4dCA9IGZ1bmN0aW9uKGNvbnRleHQpIHtcclxuICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmdldE1hdHJpeCA9IGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICBjb25zdCBjbG9uZSA9IFtdO1xyXG4gICAgICAgIGZvcih2YXIgaT0wOyBpPHRoaXMubWF0cml4Lmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgICAgY2xvbmUucHVzaCh0aGlzLm1hdHJpeFtpXS50b051bWJlcigpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNsb25lO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmdldFhUcmFuc2xhdGlvbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1hdHJpeFs0XS50b051bWJlcigpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmdldFlUcmFuc2xhdGlvbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1hdHJpeFs1XS50b051bWJlcigpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmdldFNjYWxlWCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1hdHJpeFswXS50b051bWJlcigpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmdldFNjYWxlWSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1hdHJpeFszXS50b051bWJlcigpO1xyXG4gICAgfTtcclxuXHJcbiAgICBcclxuICAgIHRoaXMuc2V0TWF0cml4ID0gZnVuY3Rpb24obSkge1xyXG4gICAgICAgIC8vdGhpcy5tYXRyaXggPSBbbVswXSxtWzFdLG1bMl0sbVszXSxtWzRdLG1bNV1dO1xyXG4gICAgICAgIHRoaXMubWF0cml4ID0gW25ldyBEZWNpbWFsKG1bMF0pLCBuZXcgRGVjaW1hbChtWzFdKSwgbmV3IERlY2ltYWwobVsyXSksIG5ldyBEZWNpbWFsKG1bM10pLCBuZXcgRGVjaW1hbChtWzRdKSwgbmV3IERlY2ltYWwobVs1XSldO1xyXG4gICAgICAgIHRoaXMuc2V0VHJhbnNmb3JtKCk7XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLmNsb25lTWF0cml4ID0gZnVuY3Rpb24obSkge1xyXG4gICAgICAgIHJldHVybiBbbVswXSxtWzFdLG1bMl0sbVszXSxtWzRdLG1bNV1dO1xyXG4gICAgfTtcclxuICAgIFxyXG4gICAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAgIC8vIFN0YWNrXHJcbiAgICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gICAgXHJcbiAgICB0aGlzLnNhdmUgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgbWF0cml4ID0gdGhpcy5jbG9uZU1hdHJpeCh0aGlzLmdldE1hdHJpeCgpKTtcclxuICAgICAgICB0aGlzLnN0YWNrLnB1c2gobWF0cml4KTtcclxuICAgICAgICBcclxuICAgICAgICBpZiAodGhpcy5jb250ZXh0KSB0aGlzLmNvbnRleHQuc2F2ZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnJlc3RvcmUgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5zdGFjay5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHZhciBtYXRyaXggPSB0aGlzLnN0YWNrLnBvcCgpO1xyXG4gICAgICAgICAgICB0aGlzLnNldE1hdHJpeChtYXRyaXgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICBpZiAodGhpcy5jb250ZXh0KSB0aGlzLmNvbnRleHQucmVzdG9yZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gICAgLy8gTWF0cml4XHJcbiAgICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuICAgIHRoaXMuc2V0VHJhbnNmb3JtID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGV4dCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHQuc2V0VHJhbnNmb3JtKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5tYXRyaXhbMF0sIC8vSG9yaXpvbnRhbCBzY2FsaW5nXHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hdHJpeFsxXSwgLy9Ib3Jpem9udGFsIHNrZXdpbmdcclxuICAgICAgICAgICAgICAgIHRoaXMubWF0cml4WzJdLCAvL1ZlcnRpY2FsIHNrZXdpbmdcclxuICAgICAgICAgICAgICAgIHRoaXMubWF0cml4WzNdLCAvL1ZlcnRpY2FsIHNjYWxpbmdcclxuICAgICAgICAgICAgICAgIHRoaXMubWF0cml4WzRdLCAvL0hvcml6b250YWwgbW92aW5nXHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hdHJpeFs1XSAgLy9WZXJ0aWNhbCBtb3ZpbmdcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICAvKlxyXG4gICAgdGhpcy50cmFuc2xhdGUgPSBmdW5jdGlvbih4LCB5KSB7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbNF0gKz0gdGhpcy5tYXRyaXhbMF0gKiB4ICsgdGhpcy5tYXRyaXhbMl0gKiB5O1xyXG4gICAgICAgIHRoaXMubWF0cml4WzVdICs9IHRoaXMubWF0cml4WzFdICogeCArIHRoaXMubWF0cml4WzNdICogeTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNldFRyYW5zZm9ybSgpO1xyXG4gICAgfTtcclxuICAgICovXHJcbiAgICB0aGlzLnRyYW5zbGF0ZURlbHRhID0gZnVuY3Rpb24oeCwgeSkge1xyXG4gICAgICAgIHRoaXMubWF0cml4WzRdID0gdGhpcy5tYXRyaXhbNF0ucGx1cyh0aGlzLm1hdHJpeFswXS50aW1lcyh4KS5wbHVzKHRoaXMubWF0cml4WzJdLnRpbWVzKHkpKSk7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbNV0gPSB0aGlzLm1hdHJpeFs1XS5wbHVzKHRoaXMubWF0cml4WzFdLnRpbWVzKHgpLnBsdXModGhpcy5tYXRyaXhbM10udGltZXMoeSkpKTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNldFRyYW5zZm9ybSgpO1xyXG4gICAgfTtcclxuICAgIHRoaXMudHJhbnNsYXRlRGVsdGFMaWdodCA9IGZ1bmN0aW9uKHgsIHkpIHtcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyggICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1hdHJpeFswXS50b051bWJlcigpICApO1xyXG4gICAgICAgIC8vY29uc29sZS5pbmZvKCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubWF0cml4WzJdLnRpbWVzKHkpLnRvTnVtYmVyKCkgICk7XHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8oIHRoaXMubWF0cml4WzBdLnRpbWVzKHgpLnBsdXModGhpcy5tYXRyaXhbMl0udGltZXMoeSkpLnRvTnVtYmVyKCkgKTtcclxuICAgICAgICB0aGlzLm1hdHJpeFs0XSA9IHRoaXMubWF0cml4WzRdLnBsdXModGhpcy5tYXRyaXhbMF0udGltZXMoeCkucGx1cyh0aGlzLm1hdHJpeFsyXS50aW1lcyh5KSkpO1xyXG4gICAgICAgIHRoaXMubWF0cml4WzVdID0gdGhpcy5tYXRyaXhbNV0ucGx1cyh0aGlzLm1hdHJpeFsxXS50aW1lcyh4KS5wbHVzKHRoaXMubWF0cml4WzNdLnRpbWVzKHkpKSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMudHJhbnNsYXRlID0gZnVuY3Rpb24oeCwgeSkge1xyXG4gICAgICAgIHRoaXMubWF0cml4WzRdID0gbmV3IERlY2ltYWwoeCk7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbNV0gPSBuZXcgRGVjaW1hbCh5KTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNldFRyYW5zZm9ybSgpO1xyXG4gICAgfTtcclxuICAgIHRoaXMudHJhbnNsYXRlTGlnaHQgPSBmdW5jdGlvbih4LCB5KSB7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbNF0gPSBuZXcgRGVjaW1hbCh4KTtcclxuICAgICAgICB0aGlzLm1hdHJpeFs1XSA9IG5ldyBEZWNpbWFsKHkpO1xyXG4gICAgfTtcclxuICAgIHRoaXMucm90YXRlID0gZnVuY3Rpb24ocmFkKSB7XHJcbiAgICAgICAgdmFyIGMgPSBNYXRoLmNvcyhyYWQpO1xyXG4gICAgICAgIHZhciBzID0gTWF0aC5zaW4ocmFkKTtcclxuICAgICAgICB2YXIgbTExID0gdGhpcy5tYXRyaXhbMF0gKiBjICsgdGhpcy5tYXRyaXhbMl0gKiBzO1xyXG4gICAgICAgIHZhciBtMTIgPSB0aGlzLm1hdHJpeFsxXSAqIGMgKyB0aGlzLm1hdHJpeFszXSAqIHM7XHJcbiAgICAgICAgdmFyIG0yMSA9IHRoaXMubWF0cml4WzBdICogLXMgKyB0aGlzLm1hdHJpeFsyXSAqIGM7XHJcbiAgICAgICAgdmFyIG0yMiA9IHRoaXMubWF0cml4WzFdICogLXMgKyB0aGlzLm1hdHJpeFszXSAqIGM7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMF0gPSBtMTE7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMV0gPSBtMTI7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMl0gPSBtMjE7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbM10gPSBtMjI7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5zZXRUcmFuc2Zvcm0oKTtcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuc2NhbGUgPSBmdW5jdGlvbihzeCwgc3kpIHtcclxuICAgICAgICB0aGlzLm1hdHJpeFswXSA9IHRoaXMubWF0cml4WzBdLnRpbWVzKHN4KTtcclxuICAgICAgICB0aGlzLm1hdHJpeFsxXSA9IHRoaXMubWF0cml4WzFdLnRpbWVzKHN4KTtcclxuICAgICAgICB0aGlzLm1hdHJpeFsyXSA9IHRoaXMubWF0cml4WzJdLnRpbWVzKHN5KTtcclxuICAgICAgICB0aGlzLm1hdHJpeFszXSA9IHRoaXMubWF0cml4WzNdLnRpbWVzKHN5KTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNldFRyYW5zZm9ybSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnNldFNjYWxlID0gZnVuY3Rpb24oeCwgeSkge1xyXG4gICAgICAgIHRoaXMubWF0cml4WzBdID0gbmV3IERlY2ltYWwoeCk7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMV0gPSBuZXcgRGVjaW1hbCh4KTtcclxuICAgICAgICB0aGlzLm1hdHJpeFsyXSA9IG5ldyBEZWNpbWFsKHkpO1xyXG4gICAgICAgIHRoaXMubWF0cml4WzNdID0gbmV3IERlY2ltYWwoeSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5zZXRUcmFuc2Zvcm0oKTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5zY2FsZUxpZ2h0ID0gZnVuY3Rpb24oc3gsIHN5KSB7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMF0gPSB0aGlzLm1hdHJpeFswXS50aW1lcyhzeCk7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMV0gPSB0aGlzLm1hdHJpeFsxXS50aW1lcyhzeCk7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMl0gPSB0aGlzLm1hdHJpeFsyXS50aW1lcyhzeSk7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbM10gPSB0aGlzLm1hdHJpeFszXS50aW1lcyhzeSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy90aGlzLnNldFRyYW5zZm9ybSgpO1xyXG4gICAgfTtcclxuICAgIC8qIEFzc3VtaW5nIHVuaWZvcm0gc2NhbGluZyBcclxuICAgICAqXHJcbiAgICAgKi9cclxuICAgIHRoaXMuZ2V0U2NhbGVGYWN0b3IgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1hdHJpeFswXTtcclxuICAgIH1cclxuICAgIC8qXHJcbiAgICB0aGlzLnNjYWxlID0gZnVuY3Rpb24oc3gsIHN5KSB7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMF0gPSBzeDtcclxuICAgICAgICB0aGlzLm1hdHJpeFszXSA9IHN5O1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuc2V0VHJhbnNmb3JtKCk7XHJcbiAgICAgICAgLy90aGlzLmNvbnRleHQudHJhbnNsYXRlKHgsIHkpO1xyXG4gICAgfTsqL1xyXG5cclxuICAgIHRoaXMuem9vbU9uUG9pbnQgPSBmdW5jdGlvbihzeCwgc3ksIHgsIHkpIHtcclxuXHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8oc3ggKyBcIiA6OiBcIiArIHN5KTtcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyh4ICsgXCIgOjogXCIgKyB5KTtcclxuICAgICAgICBzeCA9IG5ldyBEZWNpbWFsKHN4KTtcclxuICAgICAgICBzeSA9IG5ldyBEZWNpbWFsKHN5KTtcclxuXHJcbiAgICAgICAgeCA9IG5ldyBEZWNpbWFsKHgpO1xyXG4gICAgICAgIHkgPSBuZXcgRGVjaW1hbCh5KTtcclxuXHJcbiAgICAgICAgLy90aGlzLm1hdHJpeFs0XSA9IG5ldyBEZWNpbWFsKHgpO1xyXG4gICAgICAgIC8vdGhpcy5tYXRyaXhbNV0gPSBuZXcgRGVjaW1hbCh5KTtcclxuICAgICAgICBcclxuICAgICAgICAvL21vdmUgY29vcmRpbmF0ZSBzeXN0ZW0gb3JpZ2luIHRvIHRoZSB4LHkgKG1vdXNlIHBvc2l0aW9uIG92ZXIgdGhlIGNhbnZhcylcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyh0aGlzLm1hdHJpeFs0XS50b051bWJlcigpICsgXCIgPC00IDAgNS0+IFwiICsgdGhpcy5tYXRyaXhbNV0udG9OdW1iZXIoKSk7XHJcbiBcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyh4ICsgXCIgLSBcIiArIHkpO1xyXG4gICAgICAgIHRoaXMudHJhbnNsYXRlRGVsdGFMaWdodCh4LCB5KTtcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyh0aGlzLm1hdHJpeFs0XS50b051bWJlcigpICsgXCIgPC00IDEgNS0+IFwiICsgdGhpcy5tYXRyaXhbNV0udG9OdW1iZXIoKSk7XHJcbiAgICAgICAgLy9zY2FsZSBcclxuICAgICAgICB0aGlzLnNjYWxlTGlnaHQoc3gsIHN5KTtcclxuICAgICAgICAvL2NvbnN0IHNjYWxlRmFjdG9yID0gdGhpcy5nZXRTY2FsZUZhY3RvcigpO1xyXG4gICAgICAgIC8vXHJcbiAgICAgICAvL2NvbnNvbGUuaW5mbyhcInNjYWxlIDpcIiArc2NhbGVGYWN0b3IpO1xyXG4gICAgICAgIC8vY29uc29sZS5pbmZvKFwiQmFjayAgWDpcIiArIHgubWludXMoeC50aW1lcyhzY2FsZUZhY3RvcikpLnRvTnVtYmVyKCkpO1xyXG4gICAgICAgIC8vIG1vdmUgdGhlIGNvb3JkaW5hdGUgc3lzdGVtIGJhY2sgdG8gd2hlcmUgaXQgd2FzXHJcbiAgICAgICAgLy90aGlzLnRyYW5zbGF0ZURlbHRhTGlnaHQoeC5taW51cyh4LnRpbWVzKHNjYWxlRmFjdG9yKSksIHkubWludXMoeS50aW1lcyhzY2FsZUZhY3RvcikpKTtcclxuICAgICAgICAvL29yaWdpbnggLT0gbW91c2V4LyhzY2FsZSp6b29tKSAtIG1vdXNleC9zY2FsZTtcclxuXHJcbiAgICAgICAgLy9vcmlnaW54ID0gb3JpZ2lueCAtIG9yaWdpbngvem9vbSAtb3JpZ2lueFxyXG5cclxuICAgICAgICB0aGlzLnRyYW5zbGF0ZURlbHRhTGlnaHQoLXgsIC15KTtcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyh0aGlzLm1hdHJpeFs0XS50b051bWJlcigpICsgXCIgPC00IDIgNS0+IFwiICsgdGhpcy5tYXRyaXhbNV0udG9OdW1iZXIoKSk7XHJcblxyXG4gICAgICAgIC8vdGhpcy5yZXN0b3JlKCk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5zZXRUcmFuc2Zvcm0oKTtcclxuXHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8odGhpcy5nZXRNYXRyaXgoKSk7XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gICAgLy8gTWF0cml4IGV4dGVuc2lvbnNcclxuICAgIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG4gICAgdGhpcy5yb3RhdGVEZWdyZWVzID0gZnVuY3Rpb24oZGVnKSB7XHJcbiAgICAgICAgdmFyIHJhZCA9IGRlZyAqIE1hdGguUEkgLyAxODA7XHJcbiAgICAgICAgdGhpcy5yb3RhdGUocmFkKTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5yb3RhdGVBYm91dCA9IGZ1bmN0aW9uKHJhZCwgeCwgeSkge1xyXG4gICAgICAgIHRoaXMudHJhbnNsYXRlKHgsIHkpO1xyXG4gICAgICAgIHRoaXMucm90YXRlKHJhZCk7XHJcbiAgICAgICAgdGhpcy50cmFuc2xhdGUoLXgsIC15KTtcclxuICAgICAgICB0aGlzLnNldFRyYW5zZm9ybSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMucm90YXRlRGVncmVlc0Fib3V0ID0gZnVuY3Rpb24oZGVnLCB4LCB5KSB7XHJcbiAgICAgICAgdGhpcy50cmFuc2xhdGUoeCwgeSk7XHJcbiAgICAgICAgdGhpcy5yb3RhdGVEZWdyZWVzKGRlZyk7XHJcbiAgICAgICAgdGhpcy50cmFuc2xhdGUoLXgsIC15KTtcclxuICAgICAgICB0aGlzLnNldFRyYW5zZm9ybSgpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICB0aGlzLmlkZW50aXR5ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdGhpcy5tID0gWzEsMCwwLDEsMCwwXTtcclxuICAgICAgICB0aGlzLnNldFRyYW5zZm9ybSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm11bHRpcGx5ID0gZnVuY3Rpb24obWF0cml4KSB7XHJcbiAgICAgICAgdmFyIG0xMSA9IHRoaXMubWF0cml4WzBdICogbWF0cml4Lm1bMF0gKyB0aGlzLm1hdHJpeFsyXSAqIG1hdHJpeC5tWzFdO1xyXG4gICAgICAgIHZhciBtMTIgPSB0aGlzLm1hdHJpeFsxXSAqIG1hdHJpeC5tWzBdICsgdGhpcy5tYXRyaXhbM10gKiBtYXRyaXgubVsxXTtcclxuXHJcbiAgICAgICAgdmFyIG0yMSA9IHRoaXMubWF0cml4WzBdICogbWF0cml4Lm1bMl0gKyB0aGlzLm1hdHJpeFsyXSAqIG1hdHJpeC5tWzNdO1xyXG4gICAgICAgIHZhciBtMjIgPSB0aGlzLm1hdHJpeFsxXSAqIG1hdHJpeC5tWzJdICsgdGhpcy5tYXRyaXhbM10gKiBtYXRyaXgubVszXTtcclxuXHJcbiAgICAgICAgdmFyIGR4ID0gdGhpcy5tYXRyaXhbMF0gKiBtYXRyaXgubVs0XSArIHRoaXMubWF0cml4WzJdICogbWF0cml4Lm1bNV0gKyB0aGlzLm1hdHJpeFs0XTtcclxuICAgICAgICB2YXIgZHkgPSB0aGlzLm1hdHJpeFsxXSAqIG1hdHJpeC5tWzRdICsgdGhpcy5tYXRyaXhbM10gKiBtYXRyaXgubVs1XSArIHRoaXMubWF0cml4WzVdO1xyXG5cclxuICAgICAgICB0aGlzLm1hdHJpeFswXSA9IG0xMTtcclxuICAgICAgICB0aGlzLm1hdHJpeFsxXSA9IG0xMjtcclxuICAgICAgICB0aGlzLm1hdHJpeFsyXSA9IG0yMTtcclxuICAgICAgICB0aGlzLm1hdHJpeFszXSA9IG0yMjtcclxuICAgICAgICB0aGlzLm1hdHJpeFs0XSA9IGR4O1xyXG4gICAgICAgIHRoaXMubWF0cml4WzVdID0gZHk7XHJcbiAgICAgICAgdGhpcy5zZXRUcmFuc2Zvcm0oKTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5pbnZlcnQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgZCA9IDEgLyAodGhpcy5tYXRyaXhbMF0gKiB0aGlzLm1hdHJpeFszXSAtIHRoaXMubWF0cml4WzFdICogdGhpcy5tYXRyaXhbMl0pO1xyXG4gICAgICAgIHZhciBtMCA9IHRoaXMubWF0cml4WzNdICogZDtcclxuICAgICAgICB2YXIgbTEgPSAtdGhpcy5tYXRyaXhbMV0gKiBkO1xyXG4gICAgICAgIHZhciBtMiA9IC10aGlzLm1hdHJpeFsyXSAqIGQ7XHJcbiAgICAgICAgdmFyIG0zID0gdGhpcy5tYXRyaXhbMF0gKiBkO1xyXG4gICAgICAgIHZhciBtNCA9IGQgKiAodGhpcy5tYXRyaXhbMl0gKiB0aGlzLm1hdHJpeFs1XSAtIHRoaXMubWF0cml4WzNdICogdGhpcy5tYXRyaXhbNF0pO1xyXG4gICAgICAgIHZhciBtNSA9IGQgKiAodGhpcy5tYXRyaXhbMV0gKiB0aGlzLm1hdHJpeFs0XSAtIHRoaXMubWF0cml4WzBdICogdGhpcy5tYXRyaXhbNV0pO1xyXG4gICAgICAgIHRoaXMubWF0cml4WzBdID0gbTA7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbMV0gPSBtMTtcclxuICAgICAgICB0aGlzLm1hdHJpeFsyXSA9IG0yO1xyXG4gICAgICAgIHRoaXMubWF0cml4WzNdID0gbTM7XHJcbiAgICAgICAgdGhpcy5tYXRyaXhbNF0gPSBtNDtcclxuICAgICAgICB0aGlzLm1hdHJpeFs1XSA9IG01O1xyXG4gICAgICAgIHRoaXMuc2V0VHJhbnNmb3JtKCk7XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAgIC8vIEhlbHBlcnNcclxuICAgIC8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG4gICAgdGhpcy50cmFuc2Zvcm1Qb2ludCA9IGZ1bmN0aW9uKHgsIHkpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB4OiB4ICogdGhpcy5tYXRyaXhbMF0gKyB5ICogdGhpcy5tYXRyaXhbMl0gKyB0aGlzLm1hdHJpeFs0XSwgXHJcbiAgICAgICAgICAgIHk6IHggKiB0aGlzLm1hdHJpeFsxXSArIHkgKiB0aGlzLm1hdHJpeFszXSArIHRoaXMubWF0cml4WzVdXHJcbiAgICAgICAgfTtcclxuICAgIH07XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gVHJhbnNmb3JtOyIsIi8qIFxyXG4gKiBUaGlzIGlzIEJBU0UgbW9kZWwgZnJvbSB3aGljaCBtYW55IG90aGVyIG1vZGVscyBzaG91bGQgaW5oZXJpdFxyXG4gKiBJdCBwcm92aWRlcyBjb21tb24gZnVuY3Rpb25hbGl0eSBmb3I6XHJcbiAqIC0gem9vbWluZ1xyXG4gKiAtIHBhbm5pbmdcclxuICogLSBjbGVhcmluZyBjYW52YXNcclxuXHJcbiAqIFxyXG4gKi9cclxuXHJcbnZhciBUcmFuc2Zvcm0gPSByZXF1aXJlKCcuLi9tYXRyaXgvVHJhbnNmb3JtJyk7XHJcbi8vdmFyIFNoYXBlU3RvcmVBcnJheSA9IHJlcXVpcmUoJy4uL3N0b3JhZ2Uvc2hhcGVzL1NoYXBlU3RvcmVBcnJheScpO1xyXG5cclxuZnVuY3Rpb24gQmFzZU1vZGVsKG9wdGlvbnMpIHtcclxuXHJcbiAgICB2YXIgdGhhdDtcclxuXHJcbiAgICB0aGlzLmluaXQgPSBmdW5jdGlvbiAob3B0aW9ucykge1xyXG4gICAgICAgIFxyXG4gICAgICAgIGlmKG9wdGlvbnMuc29ja2V0KXtcclxuICAgICAgICAgICAgdGhpcy5zb2NrZXR0ID0gb3B0aW9ucy5zb2NrZXQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnpvb21MZXZlbCA9IDE7XHJcblxyXG4gICAgICAgIHRoaXMucGFuRGlzcGxhY2VtZW50ID0ge307XHJcbiAgICAgICAgdGhpcy5wYW5EaXNwbGFjZW1lbnQueCA9IDA7XHJcbiAgICAgICAgdGhpcy5wYW5EaXNwbGFjZW1lbnQueSA9IDA7XHJcblxyXG4gICAgICAgIHRoaXMubW91c2VEb3duT3JpZ2luID0ge307Ly9tb3VzZURvd25PcmlnaW4gLSBzdGFydCBmb3IgcGFuXHJcbiAgICAgICAgdGhpcy5tb3VzZURvd25PcmlnaW4ueCA9IDA7XHJcbiAgICAgICAgdGhpcy5tb3VzZURvd25PcmlnaW4ueSA9IDA7XHJcblxyXG4gICAgICAgIHRoaXMucHVic3ViID0gb3B0aW9ucy5wdWJzdWI7XHJcbiAgICAgICAgXHJcbiAgICAgICAgaWYob3B0aW9ucy5jYW52YXMpe1xyXG4gICAgICAgICAgICB0aGlzLmNhbnZhcyA9IG9wdGlvbnMuY2FudmFzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5jb250ZXh0ID0gb3B0aW9ucy5jb250ZXh0O1xyXG4gXHJcbiAgICAgICAgdGhhdCA9IHRoaXM7XHJcblxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgICogWm9vbSBvbiB0aGUgY2xpZW50XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgLypqUXVlcnkuc3Vic2NyaWJlKCdjbGllbnRfem9vbWluZycsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LmhhbmRsZVpvb21FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdjbGllbnRfem9vbWluZycsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVab29tRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLypcclxuICAgICAgICAgKiBcclxuICAgICAgICAgKi9cclxuICAgICAgICAvKmpRdWVyeS5zdWJzY3JpYmUoJ3NvY2tldF96b29taW5nJywgZnVuY3Rpb24oIGV2ZW50LCBvYmogKSB7XHJcbiAgICAgICAgICAgIHRoYXQuaGFuZGxlWm9vbUV2ZW50LmNhbGwodGhhdCwgb2JqKTtcclxuXHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdzb2NrZXRfem9vbWluZycsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVab29tRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qalF1ZXJ5LnN1YnNjcmliZSgnY2xpZW50X3Bhbm5pbmcnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgICAgdGhhdC5oYW5kbGVQYW5FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdjbGllbnRfcGFubmluZycsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVQYW5FdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLypqUXVlcnkuc3Vic2NyaWJlKCdzb2NrZXRfcGFubmluZycsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LmhhbmRsZVNvY2tldFBhbkV2ZW50LmNhbGwodGhhdCwgb2JqKTtcclxuICAgICAgICB9KTsqL1xyXG4gICAgICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgICAgdGhpcy5wdWJzdWIub24oJ3NvY2tldF9wYW5uaW5nJywgZnVuY3Rpb24ob2JqKSB7XHJcbiAgICAgICAgICAgICAgICB0aGF0LmhhbmRsZVNvY2tldFBhbkV2ZW50LmNhbGwodGhhdCwgb2JqKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgICogU2V0IHRoZSBvcmlnaW4gZm9yIHBhbiBzdGFydCBwb3NpdGlvblxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIC8qalF1ZXJ5LnN1YnNjcmliZSgnbW91c2VfbGVmdF9idXR0b25fZG93bicsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZUxlZnRCdXR0b25Eb3duRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgIH0pOyovXHJcbiAgICAgICAgLy9wdWJzdWIgaXMgbm90IGd1YXJhbnRlZWQgYXQgYWxsLCBzb21lIG1vZGVscyBkbyBub3QgbmVlZCBpdFxyXG4gICAgICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgICAgdGhpcy5wdWJzdWIub24oJ21vdXNlX2xlZnRfYnV0dG9uX2Rvd24nLCBmdW5jdGlvbihvYmopIHtcdFx0XHRcdFxyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZUxlZnRCdXR0b25Eb3duRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgICAgdGhpcy5wdWJzdWIub24oJ3dpbmRvd19yZXNpemVkJywgZnVuY3Rpb24ob2JqKSB7XHJcbiAgICAgICAgICAgICAgICB0aGF0LmhhbmRsZVdpbmRvd1Jlc2l6ZWRFdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gXHJcblxyXG4gICAgfTtcclxuICAgIFxyXG4gICAgdGhpcy5pbml0KG9wdGlvbnMpO1xyXG59XHJcblxyXG5CYXNlTW9kZWwucHJvdG90eXBlLmdldFNjYWxlID0gZnVuY3Rpb24gKG9iaikge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udGV4dC50cmFuc2Zvcm0uZ2V0U2NhbGVYKCk7XHJcbn07XHJcblxyXG5CYXNlTW9kZWwucHJvdG90eXBlLmdldFNjYWxlWCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgIHJldHVybiB0aGlzLmNvbnRleHQudHJhbnNmb3JtLmdldFNjYWxlWCgpO1xyXG59O1xyXG5CYXNlTW9kZWwucHJvdG90eXBlLmdldFNjYWxlWSA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgIHJldHVybiB0aGlzLmNvbnRleHQudHJhbnNmb3JtLmdldFNjYWxlWSgpO1xyXG59O1xyXG4vKlxyXG4gKiBOZWVkIHRvIHNldCB0aGUgb3JpZ2luIHdoZW4gbGVmdCBtb3VzZSBidXR0b24gaXMgcHJlc3NlZCBkb3duXHJcbiAqIGFzIGl0IGlzIGdvaW5nIHRvIGJlIGEgbmV3IHNoYXBlIG9yaWdpbiBvciBzdGFydCBvZiBwYW5cclxuICogXHJcbiAqIEBwYXJhbSB7dHlwZX0gb2JqXHJcbiAqIEBwYXJhbSB7dHlwZX0gYm9vbCBpbmRpY2F0ZXMgaWYgdGhpcyBmdW5jdGlvbiBzaG91bGQgY2FsbCByZW5kZXIgZnVuY3Rpb25cclxuICogQHJldHVybnMge25vbmV9XHJcbiAqL1xyXG5CYXNlTW9kZWwucHJvdG90eXBlLmhhbmRsZVdpbmRvd1Jlc2l6ZWRFdmVudCA9IGZ1bmN0aW9uIChvYmosIGRvTm90UmVuZGVyKSB7XHJcbiAgICBsZXQgb2Zmc2V0ID0gMDtcclxuICAgIGlmKHRoaXMuY2FudmFzWzBdLmN1c3RvbU9mZnNldCAhPT0gdW5kZWZpbmVkKXtcclxuICAgICAgICBvZmZzZXQgPSB0aGlzLmNhbnZhc1swXS5jdXN0b21PZmZzZXQ7XHJcbiAgICB9XHJcbiAgICAvL1xyXG4gICAgdGhpcy5jYW52YXMud2lkdGgob2JqLmNhbnZhcy53aWR0aCgpK29mZnNldCk7XHJcbiAgICB0aGlzLmNhbnZhcy5oZWlnaHQob2JqLmNhbnZhcy5oZWlnaHQoKStvZmZzZXQpO1xyXG4gICAgLy9cclxuICAgIHRoaXMuY29udGV4dC5jYW52YXMud2lkdGggPSBvYmouY2FudmFzLndpZHRoKCkrb2Zmc2V0O1xyXG4gICAgdGhpcy5jb250ZXh0LmNhbnZhcy5oZWlnaHQgPSBvYmouY2FudmFzLmhlaWdodCgpK29mZnNldDsgIFxyXG4gICAgLy9cclxuICAgIHRoaXMuY29udGV4dC50cmFuc2Zvcm0udHJhbnNsYXRlKFxyXG4gICAgICAgIHRoaXMucGFuRGlzcGxhY2VtZW50LngsIHRoaXMucGFuRGlzcGxhY2VtZW50LnlcclxuICAgICk7XHJcbiAgICAvKmlmICh0eXBlb2YgdGhpcy5yZWFjdFRvQ2FudmFzUmVzaXplRXZlbnQgPT09IFwiZnVuY3Rpb25cIikgeyBcclxuICAgICAgICB0aGlzLnJlYWN0VG9DYW52YXNSZXNpemVFdmVudCgpO1xyXG4gICAgfSovXHJcbiAgICAvL1xyXG4gICAgaWYgKGRvTm90UmVuZGVyKSB7IFxyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHRoaXMucmVuZGVyLmNhbGwodGhpcywgb2JqKTtcclxufTtcclxuLypcclxuICogTmVlZCB0byBzZXQgdGhlIG9yaWdpbiB3aGVuIGxlZnQgbW91c2UgYnV0dG9uIGlzIHByZXNzZWQgZG93blxyXG4gKiBhcyBpdCBpcyBnb2luZyB0byBiZSBhIG5ldyBzaGFwZSBvcmlnaW4gb3Igc3RhcnQgb2YgcGFuXHJcbiAqIFxyXG4gKiBAcGFyYW0ge3R5cGV9IG9ialxyXG4gKiBAcmV0dXJucyB7bm9uZX1cclxuICovXHJcbkJhc2VNb2RlbC5wcm90b3R5cGUuaGFuZGxlTW91c2VMZWZ0QnV0dG9uRG93bkV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG5cdGlmKCBvYmogPT09IG51bGwpe1xyXG5cdFx0cmV0dXJuO1xyXG5cdH1cclxuXHQvL3RoaXMgY2FsbCBtYWtlcyBhIGh1Z2UgZGlmZmVyZW5jZSB0byBwYW5uaW5nIGFuZCB6b29taW5nXHJcbiAgICB0aGlzLm1vdXNlRG93bk9yaWdpbiA9IG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXNBZGp1c3RlZDtcclxufTtcclxuXHJcbkJhc2VNb2RlbC5wcm90b3R5cGUuaGFuZGxlWm9vbUV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG4gICAgdGhpcy56b29tTGV2ZWwgKj0gb2JqLnpvb21MO1xyXG4gICAgXHJcbiAgICB0aGlzLmNvbnRleHQudHJhbnNmb3JtLnpvb21PblBvaW50KFxyXG4gICAgICAgIG9iai56b29tTCwgb2JqLnpvb21MLCBvYmoub3JpZ2luLngsIG9iai5vcmlnaW4ueVxyXG4gICAgKTtcclxuXHJcbiAgICAvL2NvbnN0IG1hdHJpeCA9IHRoaXMuY29udGV4dC50cmFuc2Zvcm0uZ2V0TWF0cml4KCk7XHJcbiAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC54ID0gdGhpcy5jb250ZXh0LnRyYW5zZm9ybS5nZXRYVHJhbnNsYXRpb24oKTtcclxuICAgIHRoaXMucGFuRGlzcGxhY2VtZW50LnkgPSB0aGlzLmNvbnRleHQudHJhbnNmb3JtLmdldFlUcmFuc2xhdGlvbigpO1xyXG5cclxuICAgIHRoaXMucmVuZGVyKHRoaXMuY29udGV4dCwgb2JqKTtcclxufTtcclxuXHJcbkJhc2VNb2RlbC5wcm90b3R5cGUuaGFuZGxlUGFuRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcbiAgICAvL2NvbnNvbGUuaW5mbyh0aGlzKTtcclxuICAgIC8vY29uc29sZS5pbmZvKHRoaXMuY2FsY3VsYXRlRGVsdGFEaXNwbGFjZW1lbnQob2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy54LCB0aGlzLnBhbkRpc3BsYWNlbWVudC54LCB0aGlzLm1vdXNlRG93bk9yaWdpbi54KSk7XHJcbiAgICAvL2NvbnNvbGUuaW5mbyhvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzLnggKyBcIiAtIGJhc2Vtb2RlbCAtIFwiICsgdGhpcy5tb3VzZURvd25PcmlnaW4ueCArIFwiIC0gYmFzZW1vZGVsIC0gXCIgKyB0aGlzLnBhbkRpc3BsYWNlbWVudC54KTtcclxuICAgIC8vY29uc29sZS5pbmZvKHRoaXMucGFuRGlzcGxhY2VtZW50KTtcclxuICAgIHRoaXMucGFuRGlzcGxhY2VtZW50LnggKz0gdGhpcy5jYWxjdWxhdGVEZWx0YURpc3BsYWNlbWVudChvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzLngsIHRoaXMucGFuRGlzcGxhY2VtZW50LngsIHRoaXMubW91c2VEb3duT3JpZ2luLngpO1xyXG4gICAgXHJcbiAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC55ICs9IHRoaXMuY2FsY3VsYXRlRGVsdGFEaXNwbGFjZW1lbnQob2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy55LCB0aGlzLnBhbkRpc3BsYWNlbWVudC55LCB0aGlzLm1vdXNlRG93bk9yaWdpbi55KTtcclxuXHJcbiAgICB0aGlzLmNvbnRleHQudHJhbnNmb3JtLnRyYW5zbGF0ZShcclxuICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC54LCB0aGlzLnBhbkRpc3BsYWNlbWVudC55XHJcbiAgICApO1xyXG5cclxuICAgIHRoaXMucmVuZGVyKHRoaXMuY29udGV4dCwgb2JqKTtcclxufTtcclxuXHJcbi8qXHJcbiAqIFRoZXJlIGlzIHNvbWUgc3BlY2lmaWMgdG8gc29ja2V0IHBhbm5pbmcgYmVjYXVzZVxyXG4gKiB0aGVyZSB3YXMgbm8gbW91c2VfZG93biBldmVudCB0byBzZXQgdGhlIG9yaWdpblxyXG4gKiBBbmQgdGhpcyBpcyBub3RoaW5nIHRvIGNhbGN1bGF0ZSBoZXJlLCBqdXN0IHZhbHVlcyB0byB0aGUgb25lcyBwYXNzZWQgaW4gdGhlIGV2ZW50XHJcbiAqL1xyXG5CYXNlTW9kZWwucHJvdG90eXBlLmhhbmRsZVNvY2tldFBhbkV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG4gICAgdGhpcy5tb3VzZURvd25PcmlnaW4gPSBvYmoub3JpZ2luO1xyXG5cclxuICAgIHRoaXMucGFuRGlzcGxhY2VtZW50LnggKz0gdGhpcy5jYWxjdWxhdGVEZWx0YURpc3BsYWNlbWVudChvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzLngsIHRoaXMucGFuRGlzcGxhY2VtZW50LngsIHRoaXMubW91c2VEb3duT3JpZ2luLngpO1xyXG4gICAgXHJcbiAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC55ICs9IHRoaXMuY2FsY3VsYXRlRGVsdGFEaXNwbGFjZW1lbnQob2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy55LCB0aGlzLnBhbkRpc3BsYWNlbWVudC55LCB0aGlzLm1vdXNlRG93bk9yaWdpbi55KTtcclxuXHJcbiAgICB0aGlzLmNvbnRleHQudHJhbnNmb3JtLnRyYW5zbGF0ZShcclxuICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC54LCB0aGlzLnBhbkRpc3BsYWNlbWVudC55XHJcbiAgICApO1xyXG5cclxuICAgIHRoaXMucmVuZGVyKHRoaXMuY29udGV4dCwgb2JqKTtcclxufTtcclxuXHJcbi8qXHJcbiAqIENhbGN1bGF0ZXMgZGlzcGFsY2VtZW50IG9uIG9uZSBheGlzIGJhc2VkIG9uIHRoZSBtb3VzZUNvb3Jkc092ZXJDYW52YXMgKHBhc3NlZCBpbilcclxuICogYW5kIHByZXZpb3VzbHkgc3ZhZWQgTW91c2VEb3duT3JpZ2luXHJcbiAqXHJcbiAqIEBwYXJhbSBtb3VzZUNvb3Jkc092ZXJDYW52YXNPblhvclkgaXMgY3VycmVudCBtb3VzZSBwb3NpdGlvbiBvdmVyIHRoZSBjYW52YXMgZWl0aGVyIG92ZXIgWCBvciBZIGF4aXNcclxuICogQHBhcmFtIHBhbkRpc3BsYWNlbWVudE9uWG9yWSBjdXJyZW50IHBhbiBkaXNwbGFjZW1lbnRcclxuICogQHBhcmFtIG1vdXNlRG93bk9yaWdpbk9uWG9yWSBtb3VzZSBwb3NpdGlvbiB3aGVuIHBhbiBzdGFydGVkXHJcbiAqL1xyXG5CYXNlTW9kZWwucHJvdG90eXBlLmNhbGN1bGF0ZURlbHRhRGlzcGxhY2VtZW50ID0gZnVuY3Rpb24gKG1vdXNlQ29vcmRzT3ZlckNhbnZhc09uWG9yWSwgcGFuRGlzcGxhY2VtZW50T25Yb3JZLCBtb3VzZURvd25PcmlnaW5PblhvclkpIHtcclxuXHJcbiAgICAgICAgLy8gb2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcyBpcyBjdXJyZW50IG1vdXNlIHBvc2l0aW9uIG92ZXIgdGhlIGNhbnZhczsgSXQgZG9lc24ndCBhY2NvdW50IFxyXG4gICAgICAgIC8vIGZvciB6b29tIG9yIHByZXZpb3VzIGRpY3BsYWNlbWVudFxyXG4gICAgICAgIC8vY29uc29sZS5pbmZvKFwiY2FsY3VsYXRlRGVsdGFEaXNwbGFjZW1lbnQgcGFyYW1ldGVycyA9IFwiICsgbW91c2VDb29yZHNPdmVyQ2FudmFzT25Yb3JZICsgXCIgOiBcIiArIHBhbkRpc3BsYWNlbWVudE9uWG9yWSArIFwiIDogXCIgKyBtb3VzZURvd25PcmlnaW5PblhvclkgKyBcIiA6IFwiICsgdGhpcy56b29tTGV2ZWwpO1xyXG4gICAgICAgIHZhciBjdXJyZW50ID0gKG1vdXNlQ29vcmRzT3ZlckNhbnZhc09uWG9yWSAtIHBhbkRpc3BsYWNlbWVudE9uWG9yWSkvdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8oXCJjYWxjdWxhdGVEZWx0YURpc3BsYWNlbWVudCBjdXJyZW50ID0gXCIgKyBjdXJyZW50KTtcclxuICAgICAgICAvL25lZWQgdG8gbXVsdGlwbHkgYnkgdGhpcy56b29tTGV2ZWwgdG8gY29udmVydCBkaXNwbGFjZW1lbnQgYmFjayB0byBzY3JlZW4gY29vcmRzXHJcbiAgICAgICAgdmFyIGRpZmZYID0gKG1vdXNlRG93bk9yaWdpbk9uWG9yWSAtIGN1cnJlbnQpICogdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8oXCJjYWxjdWxhdGVEZWx0YURpc3BsYWNlbWVudCBkaWZmWCA9IFwiICsgZGlmZlgpO1xyXG4gICAgICAgIHZhciBkaXJlY3Rpb25YID0gKGRpZmZYIDwgMCkgPyAxIDogLTE7XHJcbiAgICAgICAgZGlmZlggPSBNYXRoLmFicyhkaWZmWCk7XHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8oXCJjYWxjdWxhdGVEZWx0YURpc3BsYWNlbWVudCBkaWZmWCAqIGRpcmVjdGlvblggPSBcIiArIChkaWZmWCAqIGRpcmVjdGlvblgpKTtcclxuICAgICAgICByZXR1cm4gKGRpZmZYICogZGlyZWN0aW9uWCk7XHJcbn07XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IEJhc2VNb2RlbDsiLCIvKiBcclxuICogVGhpcyBtb2RlbCBtYWludGFpbnMgYW4gYXJyYXkgb2YgYWxsIHNoYXBlcyBkcmF3biAoZm9yIHJlZHJhd2luZyBvbiBjYW52YXNcclxuICogYW5kIGFueSBtYW5pcHVsYXRpb24pXHJcbiAqIEl0IGFsc28gZHJhd3MgdGhlc2Ugc2hhcGVzIG9uIHRoZSBjYW52YXMgKGNsZWFycyBjYW52YXMgYXMgd2VsbCkuIEhvd2V2ZXIsIGl0IG9ubHlcclxuICogXCJkcmF3c1wiIHRoZSBzaGFwZXMgZnJvbSBcInNoYXBlc1wiIGFycmF5LiBUaGVyZSBpcyBhIGRpZmZlcmVuY2UgYmV0d2VlblxyXG4gKiB0aG9zZSBhbmQgYSBzaGFwZSBiZWluZyBkcmF3biAodGhpcyBvbmUgaGFwcGVucyBpbiBpdHMgb3duIG1vZGVsKSBhbmQgbGF0ZXJcclxuICogYWRkZWQgdG8gXCJzaGFwZXNcIiBhcnJheS5cclxuICogSXQgYWxzbyBoYW5kbGVzIFwicGFuXCIgZXZlbnQgYW5kIFwiaW5pdFwicyBhIGdyaWRcclxuICogTWFrZXMgc2hhcGVzIHNlbGVjdGVkXHJcbiAqIFxyXG4gKiBUT0RPOiBUb28gbWFueSByZXNwb25zaWJpbGl0aWVzIGFuZCBpdCBzaG91bGQgYmUgcmVmYWN0b3JlZFxyXG4gKi9cclxuXHJcbnZhciBMaW5lID0gcmVxdWlyZSgnLi4vc2hhcGVzL0xpbmUnKTtcclxudmFyIFNoYXBlRmFjdG9yeSA9IHJlcXVpcmUoJy4uL3NoYXBlcy9TaGFwZUZhY3RvcnknKTtcclxudmFyIFRyYW5zZm9ybSA9IHJlcXVpcmUoJy4uL21hdHJpeC9UcmFuc2Zvcm0nKTtcclxudmFyIEJhc2VNb2RlbCA9IHJlcXVpcmUoJy4vQmFzZU1vZGVsJyk7XHJcbi8vdmFyIFNoYXBlU3RvcmVBcnJheSA9IHJlcXVpcmUoJy4uL3N0b3JhZ2Uvc2hhcGVzL1NoYXBlU3RvcmVBcnJheScpO1xyXG5cclxuZnVuY3Rpb24gQmFzaWNNb2RlbChvcHRpb25zKSB7XHJcblxyXG4gICAgLy9jb25zdCBvcHRpb25zID0ge307XHJcbiAgICAvL29wdGlvbnMuY2FudmFzID0gY2FudmFzO1xyXG4gICAgLy9vcHRpb25zLnNvY2tldCA9IHNvY2tldDtcclxuICAgIEJhc2VNb2RlbC5jYWxsKHRoaXMsIG9wdGlvbnMpOyAvL2NhbGwgdGhlIHN1cGVyIGNvbnN0cnVjdG9yXHJcbiAgICBcclxuICAgIHZhciB0aGF0O1xyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNvY2tldHQgPSBvcHRpb25zLnNvY2tldDtcclxuXHJcbiAgICAgICAgdGhpcy5sYXN0RW1pdCA9IDA7XHJcblxyXG4gICAgICAgIHRoaXMuc2hhcGVzID0gb3B0aW9ucy5zaGFwZXNTdG9yYWdlO1xyXG4gICAgICAgIC8vbGF5ZXJzIGFyZSB0byBrZWVwIHZhcmlvdXMgLi4uIHN0YXRpYz8gbGF5ZXJzIGluIHRoZSBtb2RlbFxyXG4gICAgICAgIC8vdGhpcy5sYXllcnMgPSB7fTtcclxuICAgICAgICAvL3RoaXMubGF5ZXJzLmdyaWQgPSBbXTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNlbGVjdGVkU2hhcGVJbmRleCA9IC0xO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoYXQgPSB0aGlzO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgICogUmVhY3RpbmcgdG8gdGhlIG1vdXNlIGV2ZW50c1xyXG4gICAgICAgICAqL1xyXG4gICAgICAgIC8qJC5zdWJzY3JpYmUoJ21vdXNlX21vdmUnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZU1vdmVFdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICAvKmlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgICAgdGhpcy5wdWJzdWIub24oJ21vdXNlX21vdmUnLCBmdW5jdGlvbihvYmopIHtcclxuICAgICAgICAgICAgICAgIHRoYXQuaGFuZGxlTW91c2VNb3ZlRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9Ki9cclxuICAgICAgICAkLnN1YnNjcmliZSgnc2hhcGVfc2VsZWN0ZWQnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgICAgdGhhdC5oYW5kbGVTaGFwZVNlbGVjdGVkRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvKi5zdWJzY3JpYmUoJ25ld19zaGFwZV9jcmVhdGVkJywgZnVuY3Rpb24oIGV2ZW50LCBvYmogKSB7XHJcbiAgICAgICAgICAgIHRoYXQuYWRkU2hhcGUoU2hhcGVGYWN0b3J5LmNyZWF0ZUZyb21KU09OKG9iai5uZXdfc2hhcGUpKTtcclxuICAgICAgICB9KTsqL1xyXG4gICAgICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgICAgdGhpcy5wdWJzdWIub24oJ25ld19zaGFwZV9jcmVhdGVkJywgZnVuY3Rpb24ob2JqKSB7XHJcbiAgICAgICAgICAgICAgICB0aGF0LmFkZFNoYXBlKFNoYXBlRmFjdG9yeS5jcmVhdGVGcm9tSlNPTihvYmoubmV3X3NoYXBlKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy9cclxuICAgICAgICAvKiQuc3Vic2NyaWJlKCdzaGFwZV9kZWxldGVkJywgZnVuY3Rpb24oIGV2ZW50LCBvYmogKSB7XHJcbiAgICAgICAgICAgIHRoYXQuZGVsZXRlU2hhcGVCeUluZGV4KG9iaik7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdzaGFwZV9kZWxldGVkJywgZnVuY3Rpb24ob2JqKSB7XHJcbiAgICAgICAgICAgICAgICB0aGF0LmRlbGV0ZVNoYXBlQnlJbmRleChvYmopO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgLyokLnN1YnNjcmliZSgnc2hhcGVfdXBkYXRlZCcsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LnVwZGF0ZVNoYXBlQnlJbmRleChvYmopO1xyXG4gICAgICAgIH0pOyovXHJcbiAgICAgICAgaWYodGhpcy5wdWJzdWIpe1xyXG4gICAgICAgICAgICB0aGlzLnB1YnN1Yi5vbignc2hhcGVfdXBkYXRlZCcsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC51cGRhdGVTaGFwZUJ5SW5kZXgob2JqKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvL2Zyb20gdG9vbGJhclxyXG4gICAgICAgICQuc3Vic2NyaWJlKCdkZWxldGVTaGFwZScsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LmRlbGV0ZVNoYXBlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy9pbml0R3JpZCh0aGlzLCB0aGlzLmNhbnZhcy5oZWlnaHQoKSwgdGhpcy5jYW52YXMud2lkdGgoKSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5jYW52YXMub24oJ2Zyb21Db250ZXh0TWVudScsIHt9LCBmdW5jdGlvbiAoZSwgcGFyYW0pIHtcclxuICAgICAgICAgICAgc3dpdGNoIChwYXJhbS5rZXkpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgXCJkZWxldGVcIjpcclxuICAgICAgICAgICAgICAgICAgICB0aGF0LmRlbGV0ZVNoYXBlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIFxyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgfTtcclxuICAgIFxyXG4gICAgdGhpcy5pbml0KG9wdGlvbnMpO1xyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgLypcclxuICAgICAqIFRoaXMgb25lIHNpbXBseSBoaWdobGlnaHRzIHNoYXBlcyBpZiBtb3VzZSBwYXNzZXMgb3ZlciB0aGVtXHJcbiAgICAgKiBcclxuICAgICAqIEBwYXJhbSB7dHlwZX0gb2JqXHJcbiAgICAgKiBAcmV0dXJucyB7bm9uZX1cclxuICAgICAqL1xyXG4gICAgLyp0aGlzLmhhbmRsZU1vdXNlTW92ZUV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG4gICAgICAgaWYob2JqLmlzRHJhd2luZyA9PT0gZmFsc2Upe1xyXG4gICAgICAgICAgdGhpcy5tYXJrU2VsZWN0ZWRTaGFwZUlmTW91c2VPdmVySXQob2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy54LCBvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzLnkpO1xyXG4gICAgICAgfSAgICAgICBcclxuICAgIH07Ki9cclxuXHJcbiAgICAvKlxyXG4gICAgICogQ2FsY3VsYXRlcyB6b29tIGxldmVsIGJhc2VkIG9uIGEgZGlyZWN0aW9uIG9mIHRoZSBtb3VzZSB3aGVlbCBcclxuICAgICAqL1xyXG4gICAgdGhpcy5jYWNsY3VsYXRlWm9vbUxldmVsID0gZnVuY3Rpb24gKGRpcmVjdGlvbikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnpvb21MZXZlbDtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5nZXRab29tTGV2ZWwgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRTY2FsZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnNldFBhbkRpc3BsYWNlbWVudCA9IGZ1bmN0aW9uKCBwYW5EaXNwbGFjZW1lbnQgKSB7XHJcbiAgICAgICAgdGhpcy5wYW5EaXNwbGFjZW1lbnQgPSBwYW5EaXNwbGFjZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIoKTtcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuZ2V0UGFuRGlzcGxhY2VtZW50ID0gZnVuY3Rpb24oICApIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wYW5EaXNwbGFjZW1lbnQ7XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLmFkZFNoYXBlID0gZnVuY3Rpb24gKHNoYXBlKSB7XHJcbiAgICAgICAgdGhpcy5zaGFwZXMuYWRkKHNoYXBlKTtcclxuICAgICAgICB0aGlzLnJlbmRlcigpO1xyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBJIGFtIHdvbmRlcmluZyBpZiBJIHNob3VsZCByZXR1cm4gYSBjb3B5IG9mIHRoZSBzaGFwZXNTdG9yYWdlIGluc3RlYWQgb2YgcmVhbCB0aGluZyA/XHJcbiAgICAgKiBUbyBwcmV2ZW50IGFueW9uZSBmcm9tIG91dHNpZGUgQmFzaWNNb2RlbCBjaGFuZ2luZyBpdC5cclxuICAgICAqL1xyXG4gICAgdGhpcy5nZXRTaGFwZXMgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2hhcGVzO1xyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBcclxuICAgICAqL1xyXG4gICAgdGhpcy5nZXRTaGFwZXNSYXdGb3JEZWJ1Z2dpbmcgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2hhcGVzLnJldHVyblJhd0RhdGEoKTtcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogXHJcbiAgICAgKi9cclxuICAgIHRoaXMuZ2V0U2hhcGUgPSBmdW5jdGlvbiAoaW5kZXgpIHtcclxuICAgICAgICBpZiggaW5kZXggIT09IHVuZGVmaW5lZCAgJiYgaW5kZXggIT09IG51bGwgJiYgaW5kZXggPj0gMCAmJiBpbmRleCA8IHRoaXMuc2hhcGVzLnNpemUoKSl7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnNoYXBlcy5nZXQoaW5kZXgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhcIlRyeWluZyB0byBnZXQgYSBzaGFwZSBpbGxlZ2FsbHk7IGluZGV4ID0gXCIgKyBpbmRleCAgKyBcIjsgIyBvZiBzaGFwZXMgPSBcIiArIHRoaXMuc2hhcGVzLnNpemUoKSk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH0gXHJcbiAgICB9O1xyXG5cclxuICAgIC8qXHJcbiAgICAgKiBcclxuICAgICAqL1xyXG4gICAgLyp0aGlzLmdldE1vdXNlRG93bk9yaWdpbiA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5tb3VzZURvd25PcmlnaW47XHJcbiAgICB9OyovXHJcbiAgICBcclxuICAgIFxyXG4gICAgLy9UT0RPIHJlZmFjdG9yIGFzIHRoaXMgZnVuY3Rpb24gZG9lcyBtb3JlIHRoYW4gZGVsZXRpZ24gc2hhcGVcclxuICAgIC8vIG5lZWQgdG8gcmVmYWN0b3Igb3V0IHRoZSBjb2RlIGZvciBzZWxlY3Rpb25cclxuICAgIHRoaXMuZGVsZXRlU2hhcGUgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZFNoYXBlSW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNoYXBlSW5kZXggPSB0aGlzLnNlbGVjdGVkU2hhcGVJbmRleDtcclxuICAgICAgICAgICAgdGhpcy5zaGFwZXMuZGVsZXRlKHRoaXMuc2VsZWN0ZWRTaGFwZUluZGV4KTtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFNoYXBlSW5kZXggPSAtMTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucmVuZGVyKCk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLmVtaXRTaGFwZURlbGV0ZWQoc2hhcGVJbmRleCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmRlbGV0ZVNoYXBlQnlJbmRleCA9IGZ1bmN0aW9uKG9iaikge1xyXG5cclxuICAgICAgICBjb25zdCBpbmRleCA9IG9iai5kZWxldGVkU2hhcGVJbmRleDtcclxuICAgICAgICAvL2Nhbm5vdCBqdXN0IHNpbXBseSBkbyBpZihpbmRleCkgYmVjYXVzZSBpbmRleCBjYW4gYmUgMFxyXG4gICAgICAgIGlmKCBpbmRleCAhPT0gdW5kZWZpbmVkICAmJiBpbmRleCAhPT0gbnVsbCAmJiBpbmRleCA+PSAwICYmIGluZGV4IDwgdGhpcy5zaGFwZXMuc2l6ZSgpKXtcclxuICAgICAgICAgICAgdGhpcy5zaGFwZXMuZGVsZXRlKGluZGV4KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucmVuZGVyKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5pbmZvKFwiVHJ5aW5nIHRvIGRvIGlsbGVnYWwgc2hhcGUgZGVsZXRlOyBpbmRleCA9IFwiICsgaW5kZXggICsgXCI7ICMgb2Ygc2hhcGVzID0gXCIgKyB0aGlzLnNoYXBlcy5zaXplKCkpO1xyXG4gICAgICAgIH0gICAgICAgIFxyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnVwZGF0ZVNoYXBlID0gZnVuY3Rpb24oaW5kZXgsIG5ld1NoYXBlKSB7XHJcbiAgICAgICAgLy9jYW5ub3QganVzdCBzaW1wbHkgZG8gaWYoaW5kZXgpIGJlY2F1c2UgaW5kZXggY2FuIGJlIDBcclxuICAgICAgICBpZiggaW5kZXggIT09IHVuZGVmaW5lZCAgJiYgaW5kZXggIT09IG51bGwgJiYgaW5kZXggPj0gMCAmJiBpbmRleCA8IHRoaXMuc2hhcGVzLnNpemUoKSl7XHJcbiAgICAgICAgICAgIHRoaXMuc2hhcGVzLnVwZGF0ZUJ5SW5kZXgoaW5kZXgsIG5ld1NoYXBlKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucmVuZGVyKCk7IFxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhcIlRyeWluZyB0byBkbyBpbGxlZ2FsIHNoYXBlIHVwZGF0ZTsgaW5kZXggPSBcIiArIGluZGV4KyBcIjsgIyBvZiBzaGFwZXMgPSBcIiArIHRoaXMuc2hhcGVzLnNpemUoKSk7XHJcbiAgICAgICAgfSAgICAgICAgXHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMudXBkYXRlU2hhcGVCeUluZGV4ID0gZnVuY3Rpb24ob2JqKSB7XHJcbiAgICAgICAgY29uc3Qgc2hhcGVJbmRleCA9IG9iai5zaGFwZUluZGV4O1xyXG5cclxuICAgICAgICB0aGlzLnNoYXBlcy51cGRhdGVCeUluZGV4KHNoYXBlSW5kZXgsIFNoYXBlRmFjdG9yeS5jcmVhdGVGcm9tSlNPTihvYmouc2hhcGUpKTtcclxuXHJcbiAgICAgICAgdGhpcy5yZW5kZXIoKTtcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuZW1pdFNoYXBlRGVsZXRlZCA9IGZ1bmN0aW9uKHNoYXBlSW5kZXgpIHtcclxuICAgICAgICB0aGlzLnNvY2tldHQuZW1pdCgnc2hhcGVEZWxldGVkJywge1xyXG4gICAgICAgICAgICAnZGVsZXRlZFNoYXBlSW5kZXgnIDogc2hhcGVJbmRleFxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmVtaXRTaGFwZVVwZGF0ZWQgPSBmdW5jdGlvbihzaGFwZUluZGV4KSB7XHJcbiAgICAgICAgdGhpcy5zb2NrZXR0LmVtaXQoJ3NoYXBlVXBkYXRlZCcsIHtcclxuICAgICAgICAgICAgJ3NoYXBlSW5kZXgnIDogc2hhcGVJbmRleCxcclxuICAgICAgICAgICAgJ3NoYXBlJzogdGhpcy5zaGFwZXMuZ2V0KHNoYXBlSW5kZXgpXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIC8qdGhpcy5zZXRTZWxlY3RlZFNoYXBlID0gZnVuY3Rpb24gKGluZGV4KSB7XHJcbiAgICAgICAgaWYgKGluZGV4ID49IDAgJiYgaW5kZXggPCB0aGlzLnNoYXBlcy5zaXplKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFNoYXBlSW5kZXggPSBpbmRleDtcclxuICAgICAgICAgICAgJC5wdWJsaXNoKFwic2hhcGVfc2VsZWN0ZWRcIiwge3NlbGVjdGVkU2hhcGVJbmRleDppbmRleH0pO1xyXG4gICAgICAgICAgICB0aGlzLnJlbmRlcigpO1xyXG4gICAgICAgIH1cclxuICAgIH07Ki9cclxuICAgIC8qXHJcbiAgICAgKiBwYXJhbSBvYmogOiB3aGVuIHNoYXBlIGlzIHNlbGVjdGVkIG5lZWQgdG8gc2V0IGl0cyBpbmRleCBoZXJlIHNvIHRoYXQgaXQgY2FuIGJlIGRyYXduIGFzIHNlbGVjdGVkXHJcbiAgICAgKiAoZG8gbm90IGRyYXcgaW4gdGhlIFNlbGVjdGVkU2hhcGVNb2RlbCBiZWNhdXNlIGl0IHdpbGwgYmUgYW5vdGhlciBzaGFwZSBvdmVybGF5ZWQgb24gdG9wIG9mIHRoZSBzaGFwZSBoZXJlKVxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZVNoYXBlU2VsZWN0ZWRFdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICBpZiAob2JqLnNlbGVjdGVkU2hhcGVJbmRleCA+PSAwICYmIG9iai5zZWxlY3RlZFNoYXBlSW5kZXggPCB0aGlzLnNoYXBlcy5zaXplKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFNoYXBlSW5kZXggPSBvYmouc2VsZWN0ZWRTaGFwZUluZGV4O1xyXG4gICAgICAgICAgICB0aGlzLnJlbmRlcigpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgLyp0aGlzLm1hcmtTZWxlY3RlZFNoYXBlSWZNb3VzZU92ZXJJdCA9IGZ1bmN0aW9uIChtb3VzZVgsIG1vdXNlWSkge1xyXG4gICAgICAgIHZhciBzZWxlY3RlZFNoYXBlSW5kZXggPSB0aGlzLmdldFNlbGVjdGVkU2hhcGVJbmRleChtb3VzZVgsIG1vdXNlWSk7XHJcbiAgICAgICAgdGhpcy5zZXRTZWxlY3RlZFNoYXBlKHNlbGVjdGVkU2hhcGVJbmRleCk7XHJcbiAgICB9OyovXHJcblxyXG4gICAgLyp0aGlzLmdldFNlbGVjdGVkU2hhcGVJbmRleCA9IGZ1bmN0aW9uIChtb3VzZVgsIG1vdXNlWSkge1xyXG4gICAgICAgIHZhciBsZW4gPSB0aGlzLnNoYXBlcy5zaXplKCk7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xyXG4gICAgICAgICAgICB2YXIgc2hhcGUgPSB0aGlzLnNoYXBlcy5nZXQoaSk7XHJcbiAgICAgICAgICAgIGlmIChzaGFwZS5pc1BvaW50T3Zlcih0aGlzLmNvbnRleHQsIG1vdXNlWCwgbW91c2VZKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIC0xO1xyXG4gICAgfTsqL1xyXG4gICAgXHJcbiAgICB0aGlzLmNsZWFyQ2FudmFzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdmFyIHRvcExlZnQgPSB7fTtcclxuICAgICAgICB0b3BMZWZ0LnggPSAoMCAtIHRoaXMucGFuRGlzcGxhY2VtZW50LngpL3RoaXMuZ2V0U2NhbGUoKTtcclxuICAgICAgICB0b3BMZWZ0LnkgPSAoMCAtIHRoaXMucGFuRGlzcGxhY2VtZW50LnkpL3RoaXMuZ2V0U2NhbGUoKTtcclxuICAgICAgICBcclxuICAgICAgICB2YXIgYm90dG9tUmlnaHQgPSB7fTtcclxuICAgICAgICBib3R0b21SaWdodC54ID0gKHRoaXMuY2FudmFzLndpZHRoKCkgKS90aGlzLmdldFNjYWxlKCk7XHJcbiAgICAgICAgYm90dG9tUmlnaHQueSA9ICh0aGlzLmNhbnZhcy5oZWlnaHQoKSApL3RoaXMuZ2V0U2NhbGUoKTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLmNvbnRleHQuY2xlYXJSZWN0KFxyXG4gICAgICAgICAgICAgICAgdG9wTGVmdC54LCB0b3BMZWZ0LnksIFxyXG4gICAgICAgICAgICAgICAgYm90dG9tUmlnaHQueCwgYm90dG9tUmlnaHQueVxyXG4gICAgICAgICk7XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLnJlbmRlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBcclxuICAgICAgICB2YXIgbGVuID0gdGhpcy5zaGFwZXMuc2l6ZSgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuY2xlYXJDYW52YXMoKTtcclxuICAgICAgICBcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XHJcbiAgICAgICAgICAgIHZhciBzaGFwZSA9IHRoaXMuc2hhcGVzLmdldChpKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChpID09PSB0aGlzLnNlbGVjdGVkU2hhcGVJbmRleCkge1xyXG4gICAgICAgICAgICAgICAgc2hhcGUuZHJhd1NlbGVjdGVkKHRoaXMuY29udGV4dCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBzaGFwZS5kcmF3KHRoaXMuY29udGV4dCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy90aGlzLmRyYXdHcmlkKCk7ICBcclxuICAgICAgICBcclxuICAgIH07XHJcbiAgICBcclxuICAgIC8qXHJcbiAgICAgKiBMaXR0bGUgdGVzdCBvZiBwZXJmb3JtYW5jZVxyXG4gICAgICovXHJcbiAgICBmdW5jdGlvbiBjcmVhdGVNYW55TGluZXMoKSB7XHJcbiAgICAgICAgdmFyIG51bUxpbmVzRm9yVGVzdCA9IDEwMDAwO1xyXG4gICAgICAgIC8vYXQgd29yayAxMCA8IHggPCAxNTgwXHJcbiAgICAgICAgLy8gMTAgPCB5IDwgNTMwXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBudW1MaW5lc0ZvclRlc3Q7IGkrKykge1xyXG4gICAgICAgICAgICB4MSA9IE1hdGguZmxvb3IoKE1hdGgucmFuZG9tKCkgKiAxNTgwKSArIDEpO1xyXG4gICAgICAgICAgICB4MiA9IE1hdGguZmxvb3IoKE1hdGgucmFuZG9tKCkgKiAxNTgwKSArIDEpO1xyXG5cclxuICAgICAgICAgICAgeTEgPSBNYXRoLmZsb29yKChNYXRoLnJhbmRvbSgpICogNTMwKSArIDEpO1xyXG4gICAgICAgICAgICB5MiA9IE1hdGguZmxvb3IoKE1hdGgucmFuZG9tKCkgKiA1MzApICsgMSk7XHJcblxyXG4gICAgICAgICAgICB2YXIgb2JqID0gbmV3IExpbmUoeDEsIHkxLCB4MiwgeTIpO1xyXG4gICAgICAgICAgICB0aGlzLnNoYXBlcy5hZGQob2JqKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5CYXNpY01vZGVsLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoQmFzZU1vZGVsLnByb3RvdHlwZSwge1xyXG4gICAgY29uc3RydWN0b3I6IHtcclxuICAgICAgICB2YWx1ZTogQmFzaWNNb2RlbFxyXG4gICAgfVxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQmFzaWNNb2RlbDsiLCIvKiBcclxuICogRHJhdyBncmlkIGxpbmVzOlxyXG4gKiAtIHR5cGljYWxseSBzbGlnaHRseSBibGVha2VyIGxpbmVzIGZvciB0aGUgZ3JpZFxyXG4gKiAtIGFuZCBicmlnaHRlciBsaW5lcyBhdCAwLDAgcG9pbnQgdG8gaGlnaGxpZ2h0IHRoZSBvcmlnaW4gcG9pbnRcclxuICovXHJcbnZhciBMYXllcnMgPSByZXF1aXJlKCcuLi9sYXllcnMvTGF5ZXJBUEknKTtcclxudmFyIExpbmUgPSByZXF1aXJlKCcuLi9zaGFwZXMvTGluZScpO1xyXG52YXIgQmFzZU1vZGVsID0gcmVxdWlyZSgnLi9CYXNlTW9kZWwnKTtcclxuXHJcbmZ1bmN0aW9uIENhbnZhc0JvcmRlck1vZGVsIChvcHRpb25zKSB7XHJcblxyXG4gICAgLy9jb25zdCBvcHRpb25zID0ge307XHJcblxyXG4gICAgLy9vcHRpb25zLmNhbnZhcyA9IGpRdWVyeShMYXllcnMub25lT2Yob3B0aW9ucy5jYW52YXMpKTsgXHJcbiAgICAvL29wdGlvbnMuc29ja2V0ID0gbnVsbDtcclxuICAgIEJhc2VNb2RlbC5jYWxsKHRoaXMsIG9wdGlvbnMpOyAvL2NhbGwgdGhlIHN1cGVyIGNvbnN0cnVjdG9yXHJcbiBcclxuICAgIC8qXHJcbiAgICAgKiBcclxuICAgICAqL1xyXG4gICAgdGhpcy5yZW5kZXIgPSBmdW5jdGlvbiAoY29udGV4dCwgb2JqKSB7XHJcbiAgICAgICAgdGhpcy5jbGVhckNhbnZhcygpO1xyXG4gICAgICAgIHRoaXMuY2FudmFzQm9yZGVyLmZvckVhY2goZnVuY3Rpb24oZWxlbWVudCl7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuZHJhdyh0aGlzLmNvbnRleHQsIDEvdGhpcy56b29tTGV2ZWwpO1xyXG4gICAgICAgIH0sIHRoaXMpO1xyXG4gICAgfTtcclxuXHJcbiAgICAvKlxyXG4gICAgICogT3ZlcnJpZGUgcGFyZW50J3Mgb2JqZWN0IGRlZmF1bHQgYmVoYXZpb3VyXHJcbiAgICAgKi9cclxuICAgIHRoaXMuaGFuZGxlWm9vbUV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG4gICAgICAgIC8vbmVlZCB0byBvdmVycmlkZSB0aGUgbWV0aG9kIG5vdCB0byB6b29tIGluIHRoaXMgY29udGV4dFxyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBPdmVycmlkZSBwYXJlbnQncyBvYmplY3QgZGVmYXVsdCBiZWhhdmlvdXJcclxuICAgICAqL1xyXG4gICAgdGhpcy5oYW5kbGVQYW5FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuXHJcbiAgICB9O1xyXG4gICAgLypcclxuICAgICAqIE92ZXJyaWRlIHBhcmVudCdzIG9iamVjdCBkZWZhdWx0IGJlaGF2aW91clxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZVNvY2tldFBhbkV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5jbGVhckNhbnZhcyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuY29udGV4dC5jbGVhclJlY3QoXHJcbiAgICAgICAgICAgICAgICAwLCAwLCBcclxuICAgICAgICAgICAgICAgIHRoaXMuY2FudmFzV2lkdGgsIHRoaXMuY2FudmFzSGVpZ2h0KTtcclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogRnVuY3Rpb24gc3RpbGwgZG9lcyBhIGZldyB0b28gbWFueSB0aGluZ3MgYnV0IGF0IGxlYXN0IGl0IGlzIG5vdyByZWZhY3RvcmVkIGludG8gaXRycyBvd24gZFxyXG4gICAgICogZGVkaWNhdGVkIGZ1bmN0aW9uIHRvIHNldCB0aGUgc2l6ZSAoYW5kIGNvbG9yIGZvciBub3cpIGZvciB0aGUgYm9yZGVyXHJcbiAgICAgKi9cclxuICAgIHRoaXMuc2V0Qm9yZGVyU2l6ZSA9IGZ1bmN0aW9uKGNhbnZhc1dpZHRoLCBjYW52YXNIZWlnaHQsIG9mZnNldFgpe1xyXG5cclxuICAgICAgICB0aGlzLmNhbnZhc0JvcmRlciA9IFtdOyAvLyBob2xkcyA0IGxpbmVzIHRvIGRyYXcgdGhlIGJvcmRlciBhcm91bmQgdGhlIGNhbnZhc1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vZHJhdyBhIFwiYm9yZGVyXCIgbGluZSB1bmRlciB0aGUgcnVsZXIgdGlja3NcclxuICAgICAgICBsZXQgYm9yZGVyID0gbmV3IExpbmUoMCwgb2Zmc2V0WCwgY2FudmFzV2lkdGgsIG9mZnNldFgpO1xyXG4gICAgICAgIGJvcmRlci5zZXRTdHJva2VTdHlsZSh0aGlzLmJvcmRlckxpbmVDb2xvcik7XHJcbiAgICAgICAgdGhpcy5jYW52YXNCb3JkZXIucHVzaChib3JkZXIpOyAvL3RvcCBob3Jpem9udGFsXHJcblxyXG4gICAgICAgIGJvcmRlciA9IG5ldyBMaW5lKGNhbnZhc1dpZHRoLCAwLCBjYW52YXNXaWR0aCwgY2FudmFzSGVpZ2h0KTtcclxuICAgICAgICBib3JkZXIuc2V0U3Ryb2tlU3R5bGUodGhpcy5ib3JkZXJMaW5lQ29sb3IpO1xyXG4gICAgICAgIHRoaXMuY2FudmFzQm9yZGVyLnB1c2goYm9yZGVyKTsgLy9yaWdodCB2ZXJ0aWNhbFxyXG5cclxuICAgICAgICBib3JkZXIgPSBuZXcgTGluZShjYW52YXNXaWR0aCwgY2FudmFzSGVpZ2h0LCAwLCBjYW52YXNIZWlnaHQpO1xyXG4gICAgICAgIGJvcmRlci5zZXRTdHJva2VTdHlsZSh0aGlzLmJvcmRlckxpbmVDb2xvcik7XHJcbiAgICAgICAgdGhpcy5jYW52YXNCb3JkZXIucHVzaChib3JkZXIpOyAvL2JvdHRvbSBob3Jpem9udGFsXHJcblxyXG4gICAgICAgIGJvcmRlciA9IG5ldyBMaW5lKG9mZnNldFgsIGNhbnZhc0hlaWdodCwgb2Zmc2V0WCwgMCk7XHJcbiAgICAgICAgYm9yZGVyLnNldFN0cm9rZVN0eWxlKHRoaXMuYm9yZGVyTGluZUNvbG9yKTtcclxuICAgICAgICB0aGlzLmNhbnZhc0JvcmRlci5wdXNoKGJvcmRlcik7IC8vbGVmdCB2ZXJ0aWNhbFxyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogRWFjaCBjYW52YXMgbW9kZWwgbmVlZHMgdG8gYmUgYWJsZSB0byByZWFjdCB0byBjYW52YXMgcmVzaXppbmcuXHJcbiAgICAgKiBJbiB0aGlzIGNhc2Ugd2UgbmVlZCB0byBlaXRoZXIgc2hvcnRlbiBvciBleHRlbmQgdGhlIHZlcnRpY2FsIHJ1bGVyIHRpY2tzXHJcbiAgICAgKiBkZXBlbmRpbmcgaWYgdGhlIGNhbnZhcyBzaHJ1bmsgb3IgZW5sYXJnZWRcclxuICAgICAqL1xyXG4gICAgIHRoaXMucmVhY3RUb0NhbnZhc1Jlc2l6ZUV2ZW50ID0gZnVuY3Rpb24oKXtcclxuICAgICAgICB0aGlzLmNhbnZhc1dpZHRoID0gdGhpcy5jYW52YXMud2lkdGgoKSAtIHRoaXMub2Zmc2V0V2lkdGg7IC8vIC0xIGlzIGZvciBsaW5lIHdpZHRoIChvdGhlcndpc2UgbGluZSB3aWxsIGJlIGRyYXduIG9uIHRoZSBvdXRzaWRlIG9mIHRoZSBjYW52YXMpXHJcbiAgICAgICAgdGhpcy5jYW52YXNIZWlnaHQgPSB0aGlzLmNhbnZhcy5oZWlnaHQoKSAtIHRoaXMub2Zmc2V0SGVpZ2h0O1xyXG4gICAgICAgIHRoaXMuc2V0Qm9yZGVyU2l6ZSh0aGlzLmNhbnZhc1dpZHRoLCB0aGlzLmNhbnZhc0hlaWdodCwgdGhpcy5vZmZzZXRYKTtcclxuICAgIH1cclxuICAgIC8qXHJcbiAgICAgKiBIYW5kbGUgdGhlIHJlc2l6ZSBldmVudCBmcm9tIGhlcmUgYW5kIGNhbGwgc3VwZXIncyBtZXRob2RcclxuICAgICAqL1xyXG4gICAgdGhpcy5oYW5kbGVXaW5kb3dSZXNpemVkRXZlbnQgPSBmdW5jdGlvbihvYmopIHtcclxuICAgICAgICB0aGlzLl9fcHJvdG9fXy5oYW5kbGVXaW5kb3dSZXNpemVkRXZlbnQuY2FsbCh0aGlzLCBvYmosIHRydWUpO1xyXG4gICAgICAgIHRoaXMucmVhY3RUb0NhbnZhc1Jlc2l6ZUV2ZW50KCk7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIob2JqKTtcclxuICAgIH1cclxuICAgIHRoaXMuaW5pdCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAvL3RoZSBlZGdlcyBvZiB0aGUgUnVsZXJMYXllciBhcmUgbm90IGZsdXNoIHdpdGggUGFwZXIgY2FudmFzIGVkZ2VzLCB0aGUgYXJlIG9mZnNldFxyXG4gICAgICAgIHRoaXMub2Zmc2V0WCA9IDIwO1xyXG4gICAgICAgIHRoaXMub2Zmc2V0V2lkdGggPSAxOyAvLzEgaXMgZm9yIGxpbmUgd2lkdGggKG90aGVyd2lzZSBsaW5lIHdpbGwgYmUgZHJhd24gb24gdGhlIG91dHNpZGUgb2YgdGhlIGNhbnZhcylcclxuICAgICAgICB0aGlzLm9mZnNldEhlaWdodCA9IDI7XHJcblxyXG4gICAgICAgIHRoaXMuYm9yZGVyTGluZUNvbG9yID0gXCIjMDAwMDAwXCI7XHJcblxyXG4gICAgICAgIHRoaXMuY2FudmFzV2lkdGggPSB0aGlzLmNhbnZhcy53aWR0aCgpIC0gdGhpcy5vZmZzZXRXaWR0aDsgLy8gLTEgaXMgZm9yIGxpbmUgd2lkdGggKG90aGVyd2lzZSBsaW5lIHdpbGwgYmUgZHJhd24gb24gdGhlIG91dHNpZGUgb2YgdGhlIGNhbnZhcylcclxuICAgICAgICB0aGlzLmNhbnZhc0hlaWdodCA9IHRoaXMuY2FudmFzLmhlaWdodCgpIC0gdGhpcy5vZmZzZXRIZWlnaHQ7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0Qm9yZGVyU2l6ZSh0aGlzLmNhbnZhc1dpZHRoLCB0aGlzLmNhbnZhc0hlaWdodCwgdGhpcy5vZmZzZXRYKTtcclxuXHJcbiAgICAgICAgdGhpcy5yZW5kZXIoKTtcclxuICAgIH07XHJcbiAgICB0aGlzLmluaXQoKTtcclxufVxyXG5cclxuQ2FudmFzQm9yZGVyTW9kZWwucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShCYXNlTW9kZWwucHJvdG90eXBlLCB7XHJcbiAgICBjb25zdHJ1Y3Rvcjoge1xyXG4gICAgICAgIHZhbHVlOiBDYW52YXNCb3JkZXJNb2RlbFxyXG4gICAgfVxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQ2FudmFzQm9yZGVyTW9kZWw7IiwiLyogXHJcbiAqIFRoaXMgaXMgYSBcInRlbXBvcmFyeVwiIGRyYXdpbmcgbW9kZWwuIEl0IGlzIHJlcHJlc2VudGF0aW9uIG9mIFxyXG4gKiBhIFwic2hhcGUgYmVpbmcgZHJhd25cIiBhbmQgbm90aGluZ3MgZWxzZSByZWFsbHkuIENvdWxkIGJlIGNhbGxlZCBcclxuICogQ3VycmVudGx5RHJhd25TaGFwZU1vZGVsIHRvIGJlIGNsZWFyZXIuXHJcbiAqXHJcbiAqIFdoYXRldmVyIHVzZXIgY3VycmVudGx5IGlzIGRyYXdpbmcgb24gdGhlIHNjcmVlbiBpcyBzdXBwb3J0ZWQgdmlhIHRoaXMgbW9kZWwuXHJcbiAqIFVubGlrZSBwZXJtYW5lbnQgbW9kZWwgd2hpY2ggc3RvcmVzIGFsbCBkcmF3biBzaGFwZXMuXHJcbiAqXHJcbiAqIFRoaXMgbW9kZWwgcGFzc2VzIGl0cyBjb250ZXh0IGludG8gc2hhcGVzIHdoZW4gdGhleSBuZWVkIHRvIGRyYXcgdGhlbXNlbHZlcy5cclxuICogVGhpcyBjb250ZXh0IG11c3QgXCJ0cmFuc2Zvcm1cImVkIGFjY29yZGluZyB0byB0aGUgY3VycmVudCBwYW5EaXNwbGFjZW1lbnQgYW5kIHpvb21MZXZlbFxyXG4gKiBpbiBvcmRlciBmb3Igc2hwZXMgdG8gYmUgZHJhd24gaW4gYXBwcm9wcmlhdGUgbG9jYXRpb25zXHJcbiAqXHJcbiAqL1xyXG5cclxudmFyIExpbmUgPSByZXF1aXJlKCcuLi9zaGFwZXMvTGluZScpO1xyXG52YXIgQ2lyY2xlID0gcmVxdWlyZSgnLi4vc2hhcGVzL0NpcmNsZScpO1xyXG52YXIgRnJlZUZvcm0gPSByZXF1aXJlKCcuLi9zaGFwZXMvRnJlZUZvcm0nKTtcclxudmFyIFNoYXBlRmFjdG9yeSA9IHJlcXVpcmUoJy4uL3NoYXBlcy9TaGFwZUZhY3RvcnknKTtcclxuXHJcbnZhciBUcmFuc2Zvcm0gPSByZXF1aXJlKCcuLi9tYXRyaXgvVHJhbnNmb3JtJyk7XHJcblxyXG52YXIgTGF5ZXJzID0gcmVxdWlyZSgnLi4vbGF5ZXJzL0xheWVyQVBJJyk7XHJcblxyXG52YXIgQmFzZU1vZGVsID0gcmVxdWlyZSgnLi9CYXNlTW9kZWwnKTtcclxuXHJcbmZ1bmN0aW9uIEN1cnJlbnREcmF3aW5nTW9kZWwgKG9wdGlvbnMpIHtcclxuICAgIFxyXG4gICAgQmFzZU1vZGVsLmNhbGwodGhpcywgb3B0aW9ucyk7IC8vIGNhbGwgdGhlIHN1cGVyIGNvbnN0cnVjdG9yXHJcblxyXG4gICAgdmFyIHRoYXQ7XHJcbiAgICB0aGlzLmluaXQgPSBmdW5jdGlvbiAob3B0aW9ucykge1xyXG5cclxuICAgICAgICB0aGF0ID0gdGhpcztcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNvY2tldHQgPSBvcHRpb25zLnNvY2tldDtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLmN1cnJlbnREcmF3aW5nTW9kZSA9IDA7IC8vIGxpbmUsIGNpcmNsZSwgZnJlZWZvcm0sIC4uLiAgb3IgMFxyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuY3VycmVudFNoYXBlID0gbnVsbDsgLy8gc2hhcGUgY3VycmVudGx5IGJlaW5nIGRyYXduXHJcblxyXG4gICAgICAgIHRoaXMuY29udGV4dCA9IG9wdGlvbnMuY29udGV4dDtcclxuXHJcbiAgICAgICAgdGhpcy5wdWJzdWIgPSBvcHRpb25zLnB1YnN1YjtcclxuICAgICAgICBcclxuXHJcbiAgICAgICAgLypcclxuICAgICAgICAgKiBTZXQgdGhlIG9yaWdpbiBmb3IgdGhlIHNoYXBlIHRvIGJlIGRyYXduXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgLyokLnN1YnNjcmliZSgnbW91c2VfbGVmdF9idXR0b25fZG93bicsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZUxlZnRCdXR0b25Eb3duRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgIH0pOyovXHJcblx0XHRcclxuXHRcdC8vIEhNTU1NTU1NLCBJIGFscmVhZHkgaGF2ZSBQVUJTVUIgaW4gdGhlIEJhc2VNb2RlbCBmb3IgdGhpc1xyXG5cdFx0Ly8gc2hvdWxkIG5vdCByZWFsbHkgZmlyZSB0aGUgc2FtZSBldmVudCB0aGUgc2Vjb25kIHRpbWVcclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdtb3VzZV9sZWZ0X2J1dHRvbl9kb3duJywgZnVuY3Rpb24ob2JqKSB7XHJcbiAgICAgICAgICAgICAgICAvL3RoYXQuaGFuZGxlTW91c2VMZWZ0QnV0dG9uRG93bkV2ZW50LmNhbGwodGhhdCwgb2JqKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKiQuc3Vic2NyaWJlKCdtb3VzZV9sZWZ0X2J1dHRvbl91cCcsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZUxlZnRCdXR0b25VcEV2ZW50LmNhbGwodGhhdCwgb2JqKTtcclxuICAgICAgICB9KTsqL1xyXG4gICAgICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgICAgdGhpcy5wdWJzdWIub24oJ21vdXNlX2xlZnRfYnV0dG9uX3VwJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICB0aGF0LmhhbmRsZU1vdXNlTGVmdEJ1dHRvblVwRXZlbnQuY2FsbCh0aGF0KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSAgICAgIFxyXG5cclxuICAgICAgICAvKlxyXG4gICAgICAgICAqIFRoaXMgaXMgaW4gcmVzcG9uc2UgdG8gc2VydmVyIHB1Ymxpc2hlZCBldmVudCB0byB1cGRhdGUgY2xpZW50c1xyXG4gICAgICAgICAqIHdpdGggY3VycmVudGx5IGRyYXduIHNoYXBlXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgLyokLnN1YnNjcmliZSgndXBkYXRlX2N1cnJlbnRfZHJhd2luZ19tb2RlbCcsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LnNldEN1cnJlbnRTaGFwZShTaGFwZUZhY3RvcnkuY3JlYXRlRnJvbUpTT04ob2JqKSk7XHJcbiAgICAgICAgICAgIHRoYXQucmVuZGVyKCk7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCd1cGRhdGVfY3VycmVudF9kcmF3aW5nX21vZGVsJywgZnVuY3Rpb24ob2JqKSB7XHJcbiAgICAgICAgICAgICAgICB0aGF0LnNldEN1cnJlbnRTaGFwZShTaGFwZUZhY3RvcnkuY3JlYXRlRnJvbUpTT04ob2JqKSk7XHJcbiAgICAgICAgICAgICAgICB0aGF0LnJlbmRlcigpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IFxyXG5cclxuICAgICAgICAvKiQuc3Vic2NyaWJlKCdzZWxlY3REcmF3aW5nTW9kZScsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LnNldERyYXdpbmdNb2RlKG9iai5rZXkpO1xyXG4gICAgICAgIH0pOyovXHJcblxyXG4gICAgICAgICQuc3Vic2NyaWJlKCdzdG9wRHJhd2luZycsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LnNldERyYXdpbmdNb2RlKDApO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvKiQuc3Vic2NyaWJlKCduZXdfc2hhcGVfY3JlYXRlZCcsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgICB0aGF0LmZsdXNoQnVmZmVyKCk7XHJcbiAgICAgICAgICAgIHRoYXQucmVuZGVyKCk7XHJcbiAgICAgICAgfSk7Ki9cclxuICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgIHRoaXMucHVic3ViLm9uKCduZXdfc2hhcGVfY3JlYXRlZCcsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgIHRoYXQuZmx1c2hCdWZmZXIoKTtcclxuICAgICAgICAgICAgICB0aGF0LnJlbmRlcigpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIFxyXG5cclxuICAgICAgICAvKlxyXG4gICAgICAgICAqIFJlYWN0aW5nIHRvIHRoZSBtb3VzZSBldmVudHNcclxuICAgICAgICAgKi9cclxuICAgICAgICAvKiQuc3Vic2NyaWJlKCdtb3VzZV9tb3ZlJywgZnVuY3Rpb24oIGV2ZW50LCBvYmogKSB7XHJcbiAgICAgICAgICAgIHRoYXQuaGFuZGxlTW91c2VNb3ZlRXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgIH0pOyovXHJcbiAgICAgICAgaWYodGhpcy5wdWJzdWIpe1xyXG4gICAgICAgICAgICB0aGlzLnB1YnN1Yi5vbignbW91c2VfbW92ZScsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZU1vdmVFdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuaW5pdChvcHRpb25zKTtcclxuICAgIFxyXG4gICAgLypcclxuICAgICAqIFRoaXMgZXZlbnQsIGV2ZW4gdGhvdWdoIGEgbW91c2UgbW92ZSBldmVudCwgaGFzIGEgcGFyYW1ldGVyXHJcbiAgICAgKiBpc0RyYXdpbmdzZXQgdG8gdHRydWUgaW4gb3JkZXIgbWFrZXMgc2Vuc2UgaW4gdGhpcyBjb250ZXh0LiBUaGUgY29udGV4dFxyXG4gICAgICogb2YgZHJhd2luZyBzaGFwZXMgdGhhdCBpc1xyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0ge3R5cGV9IG9ialxyXG4gICAgICogQHJldHVybnMge25vbmV9XHJcbiAgICAgKi9cclxuICAgIHRoaXMuaGFuZGxlTW91c2VNb3ZlRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcblxyXG4gICAgICAgIGlmKG9iai5pc0RyYXdpbmcgPT09IHRydWUpe1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUN1cnJlbnRTaGFwZShvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzQWRqdXN0ZWQueCwgb2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhc0FkanVzdGVkLnkpO1xyXG4gICAgICAgICAgICAvL2luaXRpYXRlIGV2ZW50IHRvIGJlIHNlbnQgdG8gYWxsIGNvbm5lY3RlZCBjbGllbnRzXHJcbiAgICAgICAgICAgIHRoaXMuZW1pdENoYW5nZVRvQ3VycmVudFNoYXBlKCk7XHJcbiAgICAgICAgICAgIHRoaXMucmVuZGVyKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH07XHJcbiAgICBcclxuICAgIC8qXHJcbiAgICAgKiBXaGVuIGxlZnQgbW91c2UgYnV0dG9uIGlzIHVwIGl0IG1lYW5zIHRoYXQgZHJhd2luZyBoYXMgZmluaXNoZWRcclxuICAgICAqIE5lZWQgdG8gZ2V0IHRoZSBsYXRlc3QgZHJhd24gc2hhcGUgYW5kIHBsYWNlIG9uIHRoZSBxdWV1ZSBmb3Igc3Vic2NyaWJlcnNcclxuICAgICAqIEBwYXJhbSB7dHlwZX0gb2JqXHJcbiAgICAgKiBAcmV0dXJucyB7bm9uZX1cclxuICAgICAqL1xyXG4gICAgdGhpcy5oYW5kbGVNb3VzZUxlZnRCdXR0b25VcEV2ZW50ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIFxyXG5cdFx0Ly8gUmVhbGx5IG5vdCBiaWcgZmFuIG9mIGRvaW5nIGl0IHRoYXQgd2F5LiBTcHJlYWRpbmcgdGhlIGtub3dsZWRnZSBhYm91dCB0aGUgdHlwZXMgb2YgU2hhcGVzXHJcblx0XHQvLyBhY3Jvc3MgdGhlIGZpbGUgaGVyZS4gTm90IHN1cmUgaWYgdGhlcmUgaXMgYSBiZXR0ZXIgd2F5LiBCdXQgcHJlZmVyIG5vdCBoYXZpbmcgdGhpc1xyXG5cdFx0Ly8gaWYgPT09IFwiUG9pbnRcIi4gSXQgbWlnaHQgbGVhZCB0byBleHBsb3Npb24gKGlmID09PSBTaGFwZVR5cGUpLiBJIGd1ZXNzIEkgd29vbmRlciBpZiBzaGFwZXNcclxuXHRcdC8vIHRoZW1zZWx2ZXMgc2hvdWxkIGJlIGF3YXJlIHRoYXQgdGhleSBhcmUgZmluaXNoZWQgdG8gYmUgZHJhd24gYW5kIGVtaXQgYXBwcm9wcmlhdGUgZXZlbnRzXHJcblx0XHQvLyBZZXMsIEkgdGhpbmsgdGhpcyBpcyBhIGJldHRlciB3YXkgdG8gZ28uXHJcblx0XHRcclxuXHRcdC8vIG1lYW53aGlsZSB0aGlzIG9uZSBpcyBuZWVkZWQgZm9yIGRlc2t0b3AgdmVyc2lvbiwgbW9iaWxlIGZpcmVzIG1vdXNlX2Rvd24gZXZlbnQgb24gdG91Y2hlbmRcclxuICAgICAgICBjb25zdCBzaGFwZSA9IHRoaXMuZ2V0Q3VycmVudFNoYXBlKCk7XHJcbiAgICAgICAgLy9cclxuICAgICAgICBpZiAoc2hhcGUgIT09IG51bGwgJiYgc2hhcGUuaXNWYWxpZCgpICYmIHNoYXBlLmNvbnN0cnVjdG9yLm5hbWUgPT09IFwiUG9pbnRcIikge1xyXG5cdFx0XHR0aGlzLmZsdXNoQnVmZmVyKCk7XHJcbiAgICAgICAgICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgICAgICB0aGlzLnB1YnN1Yi5lbWl0KCduZXdfc2hhcGVfY3JlYXRlZCcsIHtcIm5ld19zaGFwZVwiOiBzaGFwZS50b0pTT04oKX0gKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmVtaXROZXdTaGFwZUNyZWF0ZWQoe1wibmV3X3NoYXBlXCI6IHNoYXBlLnRvSlNPTigpfSk7XHJcbiAgICAgICAgfVxyXG5cdFx0XHJcbiAgICB9O1xyXG4gICAgLypcclxuICAgICAqIFdoZW4gbGVmdCBtb3VzZSBidXR0b24gaXMgZG93biBpdCBjYW4gYmUgZWl0aGVyIGEgc3RhcnQgb2YgcGFubmluZ1xyXG4gICAgICogb3IgYSBzdGFydCBvZiBhIG5ldyBzaGFwZVxyXG5cdCAqIFRoZSBldmVudCBpdHNlbGYgaXMgc3Vic2NyaWJlZCB0byBpbiBCYXNlTW9kZWwgYW5kIGhhbmRsZWQgdGhlcmUgYW5kIGhlcmUgXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHt0eXBlfSBvYmpcclxuICAgICAqIEByZXR1cm5zIHtub25lfVxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZU1vdXNlTGVmdEJ1dHRvbkRvd25FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuXHRcdFxyXG5cdFxyXG5cdFx0Y29uc3Qgc2hhcGUgPSB0aGlzLmdldEN1cnJlbnRTaGFwZSgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGlmIChzaGFwZSAhPT0gbnVsbCAmJiBzaGFwZS5pc1ZhbGlkKCkpIHtcclxuXHRcdFx0dGhpcy5mbHVzaEJ1ZmZlcigpO1xyXG4gICAgICAgICAgICAvLyQucHVibGlzaChcIm5ld19zaGFwZV9jcmVhdGVkXCIsIHtcIm5ld19zaGFwZVwiOiBzaGFwZS50b0pTT04oKX0pO1xyXG4gICAgICAgICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgICAgICAgdGhpcy5wdWJzdWIuZW1pdCgnbmV3X3NoYXBlX2NyZWF0ZWQnLCB7XCJuZXdfc2hhcGVcIjogc2hhcGUudG9KU09OKCl9ICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5lbWl0TmV3U2hhcGVDcmVhdGVkKHtcIm5ld19zaGFwZVwiOiBzaGFwZS50b0pTT04oKX0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiggb2JqID09PSBudWxsKXtcclxuXHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH1cclxuXHRcdFx0Ly8gdGhpcyBjYWxsIG5lZWRzIHRvIGJlIG1hZGUgaW4gb3JkZXIgdG8ga2VlcCBkcmF3aW5nIHNoYXBlcyBpbiB0aGUgY29ycmVjdCBsb2NhdGlvbiBhZnRlciBwYW5uaW5nXHJcblx0XHRcdEN1cnJlbnREcmF3aW5nTW9kZWwucHJvdG90eXBlLmhhbmRsZU1vdXNlTGVmdEJ1dHRvbkRvd25FdmVudC5jYWxsKHRoaXMsIG9iaik7XHJcblx0XHRcclxuXHRcdFx0Ly8gbG9va3MgbGlrZSB0aGlzIGlzIFN0YXJ0U2hhcGUgZXZlbnRcclxuXHRcdFx0aWYob2JqLmlzRHJhd2luZyA9PT0gdHJ1ZSl7XHJcblx0XHRcdFx0Y29uc3Qgc2hhcGVQYXJhbXMgPSB7XHJcblx0XHRcdFx0XHQnb3JpZ2luWCcgOiBvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzQWRqdXN0ZWQueCwgXHJcblx0XHRcdFx0XHQnb3JpZ2luWScgOiBvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzQWRqdXN0ZWQueVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHR0aGlzLnNldEN1cnJlbnRTaGFwZShTaGFwZUZhY3RvcnkuaW5pdGlhbGl6ZShvYmouY3VycmVudERyYXdpbmdNb2RlLCBzaGFwZVBhcmFtcykpO1xyXG5cdFx0XHRcdHRoaXMuZW1pdENoYW5nZVRvQ3VycmVudFNoYXBlKCk7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXIoKTtcclxuXHRcdFx0fVxyXG5cdFx0fVx0XHRcclxuICAgIH07XHJcblxyXG4gICAgLypcclxuICAgICAqIFNldHMgdGhlIGN1cnJlbnREcmF3aW5nTW9kZSB0byB0aGUgc3RyaW5nIG9mIHRoZSBzaGFwZSAobGluZSwgY2lyY2xlLCAuLi4pXHJcbiAgICAgKiBvciAwIHdoZW4gdXNlciBzdG9wcyBkcmF3aW5nXHJcbiAgICAgKi9cclxuICAgIHRoaXMuc2V0RHJhd2luZ01vZGUgPSBmdW5jdGlvbiAoZHJhd2luZ01vZGUpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnREcmF3aW5nTW9kZSA9IGRyYXdpbmdNb2RlO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmdldERyYXdpbmdNb2RlID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudERyYXdpbmdNb2RlO1xyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBQb29ybHkgbmFtZWQgZnVuY3Rpb24uIEl0IGRvZXMgMyB0aGluZ3Mgbm90IGp1c3QgZmx1c2hlcyBidWZmZXIhXHJcbiAgICAgKi9cclxuICAgIHRoaXMuZmx1c2hCdWZmZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5zZXRDdXJyZW50U2hhcGUobnVsbCk7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIoKTtcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogUmV0dXJucyB0aGUgcHJvcGVydHkgY3VycmVudFNoYXBlIG9mIGlmIGl0IGlzIHVuZGVmaW5lZCB0aGVuIG51bGxcclxuICAgICAqL1xyXG4gICAgdGhpcy5nZXRDdXJyZW50U2hhcGUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFNoYXBlIHx8IG51bGw7XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLmVtaXRDaGFuZ2VUb0N1cnJlbnRTaGFwZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmKHRoaXMuZ2V0Q3VycmVudFNoYXBlKCkgIT09IG51bGwpe1xyXG4gICAgICAgICAgICB0aGlzLnNvY2tldHQuZW1pdCgnc29ja2V0X3NoYXBlX2JlaW5nX2RyYXduX2NoYW5nZWQnLCB0aGlzLmdldEN1cnJlbnRTaGFwZSgpLnRvSlNPTigpKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuZW1pdE5ld1NoYXBlQ3JlYXRlZCA9IGZ1bmN0aW9uKHNoYXBlKSB7XHJcbiAgICAgICAgdGhpcy5zb2NrZXR0LmVtaXQoJ3NoYXBlQ3JlYXRlZCcsIHNoYXBlKTtcclxuICAgIH07XHJcblxyXG4gICAgXHJcbiAgICBcclxuICAgIHRoaXMudXBkYXRlQ3VycmVudFNoYXBlID0gZnVuY3Rpb24oIHBhZ2VYLCBwYWdlWSkge1xyXG4gICAgICAgIGlmKHRoaXMuZ2V0Q3VycmVudFNoYXBlKCkgIT09IG51bGwpe1xyXG4gICAgICAgICAgICB0aGlzLmdldEN1cnJlbnRTaGFwZSgpLnVwZGF0ZShwYWdlWCwgcGFnZVkpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuc2V0Q3VycmVudFNoYXBlID0gZnVuY3Rpb24gKHNoYXBlKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50U2hhcGUgPSBzaGFwZTtcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuY2xlYXJDYW52YXMgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICBcclxuICAgICAgICB2YXIgdG9wTGVmdCA9IHt9O1xyXG4gICAgICAgIHRvcExlZnQueCA9ICgwIC0gdGhpcy5wYW5EaXNwbGFjZW1lbnQueCkvdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgdG9wTGVmdC55ID0gKDAgLSB0aGlzLnBhbkRpc3BsYWNlbWVudC55KS90aGlzLnpvb21MZXZlbDtcclxuICAgICAgICBcclxuICAgICAgICB2YXIgYm90dG9tUmlnaHQgPSB7fTtcclxuICAgICAgICBib3R0b21SaWdodC54ID0gKHRoaXMuY2FudmFzLndpZHRoKCkgKS90aGlzLnpvb21MZXZlbDtcclxuICAgICAgICBib3R0b21SaWdodC55ID0gKHRoaXMuY2FudmFzLmhlaWdodCgpICkvdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5jb250ZXh0LmNsZWFyUmVjdChcclxuICAgICAgICAgICAgICAgIHRvcExlZnQueCwgdG9wTGVmdC55LCBcclxuICAgICAgICAgICAgICAgIGJvdHRvbVJpZ2h0LngsIGJvdHRvbVJpZ2h0LnkpO1xyXG4gICAgfTtcclxuICAgIFxyXG4gICAgLypcclxuICAgICAqIHRoaXMucGFuRGlzcGxhY2VtZW50IGFuZCB0aGlzLnpvb21MZXZlbCBhcmUgdmVyeSBpbXBvcnRhbnQgaW4gdGhpcyBtb2RlbFxyXG4gICAgICogYmVjYXVzZSB0aGUgY29udGV4dCBwYXNzZWQgaW50byBzaGFwZXMgZm9yIGRyYXdpbmcgbmVlZHMgdG8gYmUgdHJhbnNmb3JtZWQgYWNjb3JkaW5nbHlcclxuICAgICAqL1xyXG4gICAgdGhpcy5yZW5kZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5jbGVhckNhbnZhcygpO1xyXG4gICAgICAgIGlmKHRoaXMuZ2V0Q3VycmVudFNoYXBlKCkgIT09IG51bGwpe1xyXG4gICAgICAgICAgICB0aGlzLmdldEN1cnJlbnRTaGFwZSgpLmRyYXcodGhpcy5jb250ZXh0KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG59XHJcblxyXG5DdXJyZW50RHJhd2luZ01vZGVsLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoQmFzZU1vZGVsLnByb3RvdHlwZSwge1xyXG4gICAgY29uc3RydWN0b3I6IHtcclxuICAgICAgICB2YWx1ZTogQ3VycmVudERyYXdpbmdNb2RlbFxyXG4gICAgfVxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQ3VycmVudERyYXdpbmdNb2RlbDsiLCIvKiBcclxuICogVGhpcyBtb2RlbCBpcyByZXNwb25zaWJsZSBmb3IgbWFpbnRhaW5nIGRyYXdpbmcgc3RhdGVcclxuICogRm9yIGV4YW1wbGU6XHJcbiAqIC0gaXMgZHJhd2luZyBiZWluZyBwYW5uZWQgY3VycmVudGx5XHJcbiAqIC0gaXMgZHJhd2luZyBiZWluZyB6b29tZWRcclxuICogLSBpcyBkcmF3aW5nIGJlaW5nIGRyYXduIDopXHJcbiAqIFxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIERyYXdpbmdTdGF0ZU1vZGVsIChvcHRpb25zKSB7XHJcbiAgICB2YXIgdGhhdDtcclxuICAgIHRoaXMuaW5pdCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XHJcbiAgICAgIHRoYXQgPSB0aGlzO1xyXG4gICAgICBcclxuICAgICAgdGhpcy5kcmF3aW5nID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY3VycmVudERyYXdpbmdNb2RlID0gMDtcclxuICAgICAgdGhpcy5wYW5uaW5nID0gZmFsc2U7XHJcblxyXG4gICAgICB0aGlzLnB1YnN1YiA9IG9wdGlvbnMucHVic3ViO1xyXG5cclxuICAgICAgLyokLnN1YnNjcmliZSgnbW91c2VfbGVmdF9idXR0b25fdXAnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgIHRoYXQuaGFuZGxlTW91c2VMZWZ0QnV0dG9uVXBFdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgIH0pOyovXHJcbiAgICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICAgIHRoaXMucHVic3ViLm9uKCdtb3VzZV9sZWZ0X2J1dHRvbl91cCcsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgIHRoYXQuaGFuZGxlTW91c2VMZWZ0QnV0dG9uVXBFdmVudC5jYWxsKHRoYXQpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH0gIFxyXG4gICAgICBcclxuICAgICAgLypcclxuICAgICAgICogVXNlciBlaXRoZXIgd2FudHMgcGFuIG9yIGRyYXdcclxuICAgICAgICovXHJcbiAgICAgIC8qJC5zdWJzY3JpYmUoJ21vdXNlX2xlZnRfYnV0dG9uX2Rvd24nLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgaWYgKHRoYXQuZ2V0RHJhd2luZ01vZGUoKSkge1xyXG4gICAgICAgICAgICB0aGF0LnNldERyYXdpbmcodHJ1ZSk7XHJcbiAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoYXQuc2V0UGFubmluZyh0cnVlKTtcclxuICAgICAgICAgfVxyXG4gICAgICB9KTsqL1xyXG4gICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgdGhpcy5wdWJzdWIub24oJ21vdXNlX2xlZnRfYnV0dG9uX2Rvd24nLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIGlmICh0aGF0LmdldERyYXdpbmdNb2RlKCkpIHtcclxuICAgICAgICAgICAgdGhhdC5zZXREcmF3aW5nKHRydWUpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhhdC5zZXRQYW5uaW5nKHRydWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAvL3doZW4gbW91c2UgYnV0dG9uIGlzIHVwIG9yIGxlZnQgdGhlIGNhbnZhc1xyXG4gICAgICAvL25lZWQgdG8gcmVzZXQgdGhlIGRyYXdpbmcgbW9kZXNcclxuICAgICAgLyokLnN1YnNjcmliZSgnbW91c2VfbGVmdF9jYW52YXNfYm91bmRhcmllcycsIGZ1bmN0aW9uKCBldmVudCwgb2JqICkge1xyXG4gICAgICAgICAgdGhhdC5zZXREcmF3aW5nKGZhbHNlKTtcclxuICAgICAgICAgIHRoYXQuc2V0UGFubmluZyhmYWxzZSk7XHJcbiAgICAgICAgICAvL3RoYXQuc2V0RHJhd2luZ01vZGUoZmFsc2UpOyAvL2Nhbm5vdCBkbyB0aGF0IC4uLiBjb25uZWN0ZWQgY2xpZW50cyBnZXQgbWVzc2VkIHVwIC4uLiBvbmUgaGFzIGNpcmNsZSwgdGhlbiBhbm90aGVyIGxpbmUsIHRoZW4gdGhlIGZpcnN0IHdpbGwgaGF2ZSBsaW5lIGRyYXduIGlzbnRlYWQgb2YgY2lyY2xlIGFmdGVyIHN3aXRjaGluZ1xyXG4gICAgICB9KTsqL1xyXG4gICAgICBpZih0aGlzLnB1YnN1Yil7XHJcbiAgICAgICAgdGhpcy5wdWJzdWIub24oJ21vdXNlX2xlZnRfY2FudmFzX2JvdW5kYXJpZXMnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHRoYXQuc2V0RHJhd2luZyhmYWxzZSk7XHJcbiAgICAgICAgICB0aGF0LnNldFBhbm5pbmcoZmFsc2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkLnN1YnNjcmliZSgnc2VsZWN0RHJhd2luZ01vZGUnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICB0aGF0LmhhbmRsZVNlbGVjdERyYXdpbmdNb2RlRXZlbnQob2JqKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICAkLnN1YnNjcmliZSgnc3RvcERyYXdpbmcnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICB0aGF0LmhhbmRsZVN0b3BEcmF3aW5nRXZlbnQoKTtcclxuICAgICAgfSk7XHJcbiAgXHJcbiAgIH07XHJcbiAgIFxyXG4gICB0aGlzLmluaXQob3B0aW9ucyk7XHJcbiAgIC8qXHJcbiAgICAqIFdoZW4gbGVmdCBtb3VzZSBpcyB1cCBpdCBpcyBlaXRoZXIgZHJmYXdpbmcgaXMgZmluaXNoZWQgKHVzZXIgbmVlZHMgdG9cclxuICAgICogaG9sZCBtb3VzZSBkb3duIHRvIGRyYXcpIG9yIHBhbm5pbmcgaXMgZmluaXNoZWRcclxuICAgICovXHJcbiAgIHRoaXMuaGFuZGxlTW91c2VMZWZ0QnV0dG9uVXBFdmVudCA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgIC8vdGhpcy5zZXREcmF3aW5nKGZhbHNlKTtcclxuICAgICAgdGhpcy5zZXRQYW5uaW5nKGZhbHNlKTtcclxuICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBzZXQgZHJhd2luZyBtb2RlIHRvIHRoZSBuYW1lIG9mIHRoZSBzaGFwZSBiZWluZyBkcmF3blxyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0ge3R5cGV9IG9ialxyXG4gICAgICogQHJldHVybnMge25vbmV9XHJcbiAgICAgKi9cclxuICAgIHRoaXMuaGFuZGxlU2VsZWN0RHJhd2luZ01vZGVFdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICAvL3RoaXMuc2V0RHJhd2luZ01vZGUodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5zZXREcmF3aW5nTW9kZShvYmoua2V5KTtcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogc2V0IGRyYXdpbmcgbW9kZSB0byB0aGUgbmFtZSBvZiB0aGUgc2hhcGUgYmVpbmcgZHJhd25cclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIHt0eXBlfSBvYmpcclxuICAgICAqIEByZXR1cm5zIHtub25lfVxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZVN0b3BEcmF3aW5nRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcbiAgICAgICAgdGhpcy5zZXREcmF3aW5nTW9kZShmYWxzZSk7XHJcbiAgICB9OyAgIFxyXG4gICAgXHJcbiAgICB0aGlzLnNldERyYXdpbmcgPSBmdW5jdGlvbiggaXNEcmF3aW5nICl7XHJcbiAgICAgICAgdGhpcy5kcmF3aW5nID0gaXNEcmF3aW5nO1xyXG4gICAgfTtcclxuICAgIHRoaXMuaXNEcmF3aW5nID0gZnVuY3Rpb24oKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5kcmF3aW5nO1xyXG4gICAgfTtcclxuICAgIFxyXG4gICAgdGhpcy5zZXRQYW5uaW5nID0gZnVuY3Rpb24oIGlzUGFubmluZyApe1xyXG4gICAgICAgIHRoaXMucGFubmluZyA9IGlzUGFubmluZztcclxuICAgIH07XHJcbiAgICB0aGlzLmlzUGFubmluZyA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucGFubmluZztcclxuICAgIH07XHJcbiAgICBcclxuICAgIC8qXHJcbiAgICAgKiBXZSBlaXRoZXIgYXJlIGRyYXdpbmcgb3Igbm90XHJcbiAgICAgKiBcclxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gZHJhd2luZ01vZGVcclxuICAgICAqIEByZXR1cm5zIHtub25lfVxyXG4gICAgICovXHJcbiAgICB0aGlzLnNldERyYXdpbmdNb2RlPSBmdW5jdGlvbiggZHJhd2luZ01vZGUgKXtcclxuICAgICAgICB0aGlzLmN1cnJlbnREcmF3aW5nTW9kZSA9IGRyYXdpbmdNb2RlO1xyXG4gICAgfTtcclxuICAgIHRoaXMuZ2V0RHJhd2luZ01vZGUgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnREcmF3aW5nTW9kZTtcclxuICAgIH07XHJcbiAgICBcclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBEcmF3aW5nU3RhdGVNb2RlbDsiLCIndXNlIHN0cmljdCc7XHJcblxyXG5jbGFzcyBEcmF3aW5nVG9vbGJhck1vZGVsIHtcclxuXHJcblx0Y29uc3RydWN0b3IoKSB7XHJcblxyXG5cdFx0Y29uc3Qgc2VsZiA9IHRoaXM7XHJcblxyXG4gICBcdFx0JChcIiN0b29sYmFyX3N0cmFpZ2h0X2xpbmVcIikuY2hhbmdlKFxyXG4gICBcdFx0XHRmdW5jdGlvbigpIHtcclxuICAgXHRcdFx0XHQkLnB1Ymxpc2goXCJzZWxlY3REcmF3aW5nTW9kZVwiLCB7ICdrZXknIDogJ3N0cmFpZ2h0TGluZSd9ICk7XHJcbiAgIFx0XHRcdH1cclxuXHRcdCk7XHJcblxyXG5cdFx0JChcIiN0b29sYmFyX2NpcmNsZVwiKS5jaGFuZ2UoXHJcbiAgIFx0XHRcdGZ1bmN0aW9uKCkge1xyXG5cdFx0ICBcdFx0Ly8kKFwiI3BhcGVyXCIpLnRyaWdnZXIoXCJzZWxlY3REcmF3aW5nTW9kZVwiLCB7XCJwYXJhbVwiOiBcIm9uZVwiLCBcImtleVwiOiBcImNpcmNsZVwifSk7XHJcblx0XHQgIFx0XHQkLnB1Ymxpc2goXCJzZWxlY3REcmF3aW5nTW9kZVwiLCB7ICdrZXknIDogJ2NpcmNsZSd9ICk7XHJcblx0XHRcdH1cclxuXHRcdCk7XHJcblxyXG5cdFx0JChcIiN0b29sYmFyX2ZyZWVmb3JtXCIpLmNoYW5nZShcclxuICAgXHRcdFx0ZnVuY3Rpb24oKSB7XHJcblx0XHQgIFx0XHQvLyQoXCIjcGFwZXJcIikudHJpZ2dlcihcInNlbGVjdERyYXdpbmdNb2RlXCIsIHtcInBhcmFtXCI6IFwib25lXCIsIFwia2V5XCI6IFwiZnJlZUZvcm1cIn0pO1xyXG5cdFx0ICBcdFx0JC5wdWJsaXNoKFwic2VsZWN0RHJhd2luZ01vZGVcIiwgeyAna2V5JyA6ICdmcmVlRm9ybSd9ICk7XHJcblx0XHRcdH1cclxuXHRcdCk7XHJcblxyXG4gICAgJChcIiN0b29sYmFyX3BvaW50XCIpLmNoYW5nZShcclxuICAgICAgICBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIC8vJChcIiNwYXBlclwiKS50cmlnZ2VyKFwic2VsZWN0RHJhd2luZ01vZGVcIiwge1wicGFyYW1cIjogXCJvbmVcIiwgXCJrZXlcIjogXCJmcmVlRm9ybVwifSk7XHJcbiAgICAgICAgICAkLnB1Ymxpc2goXCJzZWxlY3REcmF3aW5nTW9kZVwiLCB7ICdrZXknIDogJ3BvaW50J30gKTtcclxuICAgICAgfVxyXG4gICAgKTtcclxuXHJcblx0XHQkKFwiI3Rvb2xiYXJfc3RvcF9kcmF3aW5nXCIpLmNoYW5nZShcclxuICAgXHRcdFx0ZnVuY3Rpb24oKSB7XHJcblx0XHQgIFx0XHQvLyQoXCIjcGFwZXJcIikudHJpZ2dlcihcInN0b3BEcmF3aW5nXCIsIHt9KTtcclxuXHRcdCAgXHRcdCQucHVibGlzaChcInN0b3BEcmF3aW5nXCIsIHsna2V5JyA6ICdzdG9wX2RyYXdpbmcnfSApO1xyXG5cdFx0XHR9XHJcblx0XHQpO1xyXG5cclxuXHJcbiAgICAkKFwiI3Rvb2xiYXJfZGVsZXRlX3NoYXBlXCIpLmNsaWNrKFxyXG4gICAgICAgIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgJC5wdWJsaXNoKFwiZGVsZXRlU2hhcGVcIiwge30gKTtcclxuICAgICAgICAgIFxyXG4gICAgICB9XHJcbiAgICApO1xyXG5cclxuXHRcdCQuc3Vic2NyaWJlKCdzZWxlY3REcmF3aW5nTW9kZScsIFxyXG5cdFx0XHRmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuXHRcdFx0XHRzZWxmLmhhbmRsZVNlbGVjdERyYXdpbmdNb2RlRXZlbnQob2JqKTtcclxuICAgICAgICBcdH1cclxuICAgICAgICApO1xyXG4gICAgICAgICQuc3Vic2NyaWJlKCdzdG9wRHJhd2luZycsIFxyXG5cdFx0XHRmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuXHRcdFx0XHRzZWxmLmhhbmRsZVN0b3BEcmF3aW5nRXZlbnQoKTtcclxuICAgICAgICBcdH1cclxuICAgICAgICApO1xyXG5cdFx0XHQvL3RoYXQuaGFuZGxlU2VsZWN0RHJhd2luZ01vZGUuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgXHJcblx0fVxyXG5cclxuXHQvKlxyXG5cdCAqIE5lZWQgdG8gaGlnaGxpZ2h0IGFwcHJvcHJpYXRlIGJ1dHRvbiBvbiB0aGUgdG9vbGJhciB3aGVuIGV2ZW50IGlzIGZpcmVkXHJcblx0ICovXHJcbiAgIFx0aGFuZGxlU2VsZWN0RHJhd2luZ01vZGVFdmVudCAob2JqKSB7XHJcbiAgIFx0XHRzd2l0Y2gob2JqLmtleSl7XHJcbiAgIFx0XHRcdGNhc2UgXCJzdHJhaWdodExpbmVcIjpcclxuICAgXHRcdFx0XHQkKFwiI3Rvb2xiYXJfc3RyYWlnaHRfbGluZVwiKS5wYXJlbnQoKS5hZGRDbGFzcyhcImFjdGl2ZVwiKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICBcdFx0XHRicmVhaztcclxuICAgXHRcdFx0Y2FzZSBcImNpcmNsZVwiOlxyXG4gICBcdFx0XHRcdCQoXCIjdG9vbGJhcl9jaXJjbGVcIikucGFyZW50KCkuYWRkQ2xhc3MoXCJhY3RpdmVcIikuc2libGluZ3MoKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgXHRcdFx0YnJlYWs7XHJcbiAgIFx0XHRcdGNhc2UgXCJmcmVlRm9ybVwiOlxyXG4gICBcdFx0XHRcdCQoXCIjdG9vbGJhcl9mcmVlZm9ybVwiKS5wYXJlbnQoKS5hZGRDbGFzcyhcImFjdGl2ZVwiKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICBcdFx0XHRicmVhaztcclxuICAgXHRcdFx0Y2FzZSBcInBvaW50XCI6XHJcbiAgIFx0XHRcdFx0JChcIiN0b29sYmFyX3BvaW50XCIpLnBhcmVudCgpLmFkZENsYXNzKFwiYWN0aXZlXCIpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgIFx0XHRcdGJyZWFrO1xyXG4gICAgICAgIGNhc2UgXCJmcmVlRm9ybVwiOlxyXG4gICAgICAgICAgJChcIiN0b29sYmFyX3N0b3BfZHJhd2luZ1wiKS5wYXJlbnQoKS5hZGRDbGFzcyhcImFjdGl2ZVwiKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgIGJyZWFrO1xyXG5cclxuICAgXHRcdFx0ZGVmYXVsdDogLy9kbyBub3RoaW5nXHJcbiAgIFx0XHR9XHJcbiAgICB9O1xyXG4gICAgLypcclxuICAgICAqXHJcbiAgICAgKi9cclxuICAgIGhhbmRsZVN0b3BEcmF3aW5nRXZlbnQgKCkge1xyXG4gICAgXHQkKFwiI3Rvb2xiYXJfc3RvcF9kcmF3aW5nXCIpLnBhcmVudCgpLmFkZENsYXNzKFwiYWN0aXZlXCIpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICB9O1xyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IERyYXdpbmdUb29sYmFyTW9kZWw7IiwiLyogXHJcbiAqIERyYXcgZ3JpZCBsaW5lczpcclxuICogLSB0eXBpY2FsbHkgc2xpZ2h0bHkgYmxlYWtlciBsaW5lcyBmb3IgdGhlIGdyaWRcclxuICogLSBhbmQgYnJpZ2h0ZXIgbGluZXMgYXQgMCwwIHBvaW50IHRvIGhpZ2hsaWdodCB0aGUgb3JpZ2luIHBvaW50XHJcbiAqL1xyXG52YXIgTGF5ZXJzID0gcmVxdWlyZSgnLi4vbGF5ZXJzL0xheWVyQVBJJyk7XHJcbnZhciBMaW5lID0gcmVxdWlyZSgnLi4vc2hhcGVzL0xpbmUnKTtcclxudmFyIEJhc2VNb2RlbCA9IHJlcXVpcmUoJy4vQmFzZU1vZGVsJyk7XHJcblxyXG5mdW5jdGlvbiBHcmlkTW9kZWwgKG9wdGlvbnMpIHtcclxuXHJcbiAgICAvL2NvbnN0IG9wdGlvbnMgPSB7fTtcclxuICAgIC8vY29uc3QgY2FudmFzID0gb3B0aW9ucy5jYW52YXM7XHJcbiAgICAvL29wdGlvbnMuY2FudmFzID0gJChMYXllcnMuY3JlYXRlKG9wdGlvbnMuY2FudmFzLCBcIkdyaWRMYXllclwiLCA1KSk7XHJcbiAgICAvL29wdGlvbnMuc29ja2V0ID0gc29ja2V0O1xyXG4gICAgQmFzZU1vZGVsLmNhbGwodGhpcywgb3B0aW9ucyk7IC8vY2FsbCB0aGUgc3VwZXIgY29uc3RydWN0b3JcclxuICAgIFxyXG4gICAgdmFyIHRoYXQ7XHJcbiAgICB0aGlzLmluaXQgPSBmdW5jdGlvbiAob3B0aW9ucykge1xyXG5cclxuICAgICAgICB0aGF0ID0gdGhpcztcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLm9mZnNldCA9IDE1O1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuZ3JpZExpbmVzID0gW107IC8vaG9sZHMgdmVydGljYWwgYW5kIGhvcml6b250YWwgbGluZXMgb2YgdGhlIGdyaWRcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLmdyaWRWaXNpYmxlID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdGhpcy5ncmlkTGluZXdpZHRoID0gMTtcclxuXHJcbiAgICAgICAgdGhpcy5ncmlkTGluZUNvbG9yID0gXCIjYmViZWJlXCI7XHJcbiAgICAgICAgdGhpcy56ZXJvdGhHcmlkTGluZUNvbG9yID0gXCIjMDAwMDAwXCI7XHJcblxyXG4gICAgICAgIHRoaXMuY2FudmFzID0gb3B0aW9ucy5jYW52YXM7XHJcblxyXG4gICAgICAgIHRoaXMuY29udGV4dCA9IG9wdGlvbnMuY29udGV4dDtcclxuICAgICAgICBcclxuICAgICAgICBqUXVlcnkuc3Vic2NyaWJlKCd0b2dnbGVHcmlkVmlzaWJpbGl0eScsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICB0aGF0LnRvZ2dsZUdyaWRWaXNpYmlsaXR5KCk7XHJcbiAgICAgICAgfSlcclxuXHJcbiAgICAgICAgdGhpcy5wdWJzdWIgPSBvcHRpb25zLnB1YnN1YjtcclxuXHJcbiAgICAgICAgLyokLnN1YnNjcmliZSgnbW91c2VfbGVmdF9idXR0b25fdXAnLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICAgIHRoYXQuaGFuZGxlTW91c2VMZWZ0QnV0dG9uVXBFdmVudC5jYWxsKHRoYXQsIG9iaik7XHJcbiAgICAgICAgfSk7Ki9cclxuXHJcbiAgICAgICAgdGhpcy5pbml0R3JpZCgpO1xyXG4gICAgICAgIHRoaXMucmVuZGVyKCk7XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLmluaXRHcmlkID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGNvbnN0IGZyZXF1ZW5jeSA9IDEwMDtcclxuICAgICAgICBjb25zdCB3aWR0aCA9IHRoaXMuY2FudmFzLndpZHRoKCk7XHJcbiAgICAgICAgY29uc3QgaGVpZ2h0ID0gdGhpcy5jYW52YXMuaGVpZ2h0KCk7XHJcbiAgICAgICAgLy9cclxuICAgICAgICB0aGlzLmdyaWRMaW5lcyA9IFtdO1xyXG4gICAgICAgIC8vdG8gZHJhdyB0aGUgZ3JpZCwgYWxsIEkgYW0gZG9pbmcgZm9yIG5vdyBpcyBkcmF3IGdyaWQgbGluZXMgMTAwICogd2lkdGggdG8gdGhlIGxlZnQgYW5kIHJpZ2h0XHJcbiAgICAgICAgLy9iYXNpY2FsbHkganVzdCBjcmVhdGluZyBzbyBtYW55IG9mIHRoZW0gdGhhdCBJIGFtIG5vdCBsaWtlbHkgdG8gcGFuIHRoYXQgZmFyLlxyXG4gICAgICAgIC8vVE9ETzogbWFrZSBpdCBtb3JlIGVmZmVjdGl2ZSBieSBkb2luZyBvbiBkZW1hbmQgZHJhd2luZyBcclxuICAgICAgICBmb3IodmFyIGkgPSAwIC0gd2lkdGgqMTAwOyBpIDwgd2lkdGggKjEwMDsgaSA9IGkrZnJlcXVlbmN5KXtcclxuICAgICAgICAgICAgdmFyIHZMaW5lID0gbmV3IExpbmUoaSwgMCAtIGhlaWdodCoxMDAsIGksIGhlaWdodCoxMDApO1xyXG4gICAgICAgICAgICB2TGluZS5zZXRTdHJva2VTdHlsZSh0aGlzLmdyaWRMaW5lQ29sb3IpO1xyXG4gICAgICAgICAgICBpZihpID09PSAwKXtcclxuICAgICAgICAgICAgICAgIHZMaW5lLnNldFN0cm9rZVN0eWxlKHRoaXMuemVyb3RoR3JpZExpbmVDb2xvcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5ncmlkTGluZXMucHVzaCh2TGluZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIGZvcih2YXIgaSA9IDAgLSBoZWlnaHQqMTAwOyBpIDwgaGVpZ2h0KjEwMDsgaSA9IGkrZnJlcXVlbmN5KXtcclxuICAgICAgICAgICAgdmFyIGhMaW5lID0gbmV3IExpbmUoMCAtIHdpZHRoKjEwMCwgaSwgd2lkdGgqMTAwLCBpKTtcclxuICAgICAgICAgICAgaExpbmUuc2V0U3Ryb2tlU3R5bGUodGhpcy5ncmlkTGluZUNvbG9yKTtcclxuICAgICAgICAgICAgaWYoaSA9PT0gMCl7XHJcbiAgICAgICAgICAgICAgICBoTGluZS5zZXRTdHJva2VTdHlsZSh0aGlzLnplcm90aEdyaWRMaW5lQ29sb3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuZ3JpZExpbmVzLnB1c2goaExpbmUpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMudG9nZ2xlR3JpZFZpc2liaWxpdHkgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICB0aGlzLmdyaWRWaXNpYmxlID0gIXRoaXMuZ3JpZFZpc2libGU7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIoKTtcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuY2xlYXJDYW52YXMgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgdG9wTGVmdCA9IHt9O1xyXG4gICAgICAgIHRvcExlZnQueCA9ICgwIC0gdGhpcy5wYW5EaXNwbGFjZW1lbnQueCkvdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgdG9wTGVmdC55ID0gKDAgLSB0aGlzLnBhbkRpc3BsYWNlbWVudC55KS90aGlzLnpvb21MZXZlbDtcclxuICAgICAgICBcclxuICAgICAgICB2YXIgYm90dG9tUmlnaHQgPSB7fTtcclxuICAgICAgICBib3R0b21SaWdodC54ID0gKHRoaXMuY2FudmFzLndpZHRoKCkgKS90aGlzLnpvb21MZXZlbDtcclxuICAgICAgICBib3R0b21SaWdodC55ID0gKHRoaXMuY2FudmFzLmhlaWdodCgpICkvdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5jb250ZXh0LmNsZWFyUmVjdChcclxuICAgICAgICAgICAgICAgIHRvcExlZnQueCwgdG9wTGVmdC55LCBcclxuICAgICAgICAgICAgICAgIGJvdHRvbVJpZ2h0LngsIGJvdHRvbVJpZ2h0LnkpO1xyXG4gICAgfTtcclxuICAgIFxyXG4gICAgdGhpcy5yZW5kZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5jbGVhckNhbnZhcygpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGlmICh0aGlzLmdyaWRWaXNpYmxlICkge1xyXG4gICAgICAgICAgICB2YXIgZ0xlbiA9IHRoaXMuZ3JpZExpbmVzLmxlbmd0aDtcclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBnTGVuOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHZhciBzaGFwZSA9IHRoaXMuZ3JpZExpbmVzW2ldO1xyXG4gICAgICAgICAgICAgICAgc2hhcGUuZHJhdyh0aGlzLmNvbnRleHQsIHRoaXMuZ3JpZExpbmV3aWR0aC90aGlzLnpvb21MZXZlbCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLmluaXQob3B0aW9ucyk7XHJcbn1cclxuXHJcbkdyaWRNb2RlbC5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKEJhc2VNb2RlbC5wcm90b3R5cGUsIHtcclxuICAgIGNvbnN0cnVjdG9yOiB7XHJcbiAgICAgICAgdmFsdWU6IEdyaWRNb2RlbFxyXG4gICAgfVxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gR3JpZE1vZGVsOyIsIi8qIFxyXG4gKiBUaGlzIG1vZGVsIGlzIG5vdGhpbmcgbW9yZSBqdXN0IGFuIGluZm9ybWF0aW9uIHN0b3JhZ2VcclxuICogYWJvdXQgdGhlIGN1cnJlbnQgcGFuIGFuZCB6b29tIHZhbHVlcy5cclxuICogRWFjaCBtb2RlbCBhY3R1YWxseSBpbmhlcml0cyB0aGlzIHZhbHVlcyBmcm9tIEJhc2VNb2RlbCByaWdodCBub3dcclxuICovXHJcblxyXG52YXIgQmFzZU1vZGVsID0gcmVxdWlyZSgnLi9CYXNlTW9kZWwnKTtcclxuXHJcbmZ1bmN0aW9uIFBhblpvb21Nb2RlbChvcHRpb25zKSB7XHJcblxyXG4gICAgQmFzZU1vZGVsLmNhbGwodGhpcywgb3B0aW9ucyk7IC8vY2FsbCB0aGUgc3VwZXIgY29uc3RydWN0b3JcclxuICAgIFxyXG4gICAgdmFyIHRoYXQ7XHJcbiAgICB0aGlzLmluaXQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhhdCA9IHRoaXM7ICAgICAgICBcclxuICAgICAgICBcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuaW5pdCgpO1xyXG5cclxuICAgIHRoaXMuZ2V0UGFuRGlzcGxhY2VtZW50ID0gZnVuY3Rpb24oICApIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wYW5EaXNwbGFjZW1lbnQ7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMucmVuZGVyID0gZnVuY3Rpb24oICApIHtcclxuICAgICAgICAvL2RvIG5vdGhpbmdcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5nZXRNb3VzZURvd25PcmlnaW4gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubW91c2VEb3duT3JpZ2luO1xyXG4gICAgfTtcclxuXHRcclxufVxyXG5cclxuUGFuWm9vbU1vZGVsLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoQmFzZU1vZGVsLnByb3RvdHlwZSwge1xyXG4gICAgY29uc3RydWN0b3I6IHtcclxuICAgICAgICB2YWx1ZTogUGFuWm9vbU1vZGVsXHJcbiAgICB9XHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBQYW5ab29tTW9kZWw7IiwidmFyIExpbmUgPSByZXF1aXJlKCcuLi8uLi9zaGFwZXMvTGluZScpO1xyXG52YXIgUnVsZXJHdWlkZSA9IHJlcXVpcmUoJy4vUnVsZXJHdWlkZScpO1xyXG5cclxuLyogXHJcbiAqIFJ1bGVyIEd1aWRlIHdoaWNoIHJ1bnMgaG9yaXpvbnRhbGx5IGZyb20gdGhlIGxlZnQgdmVydGljYWwgcnVsZXIgdG8gbW91c2UgcG9pbnRlclxyXG4gKiAodG8gdGhlIHJpZ2h0IGVkZ2Ugb2YgY2FudmFzLCBvcHRpb25hbGx5KVxyXG4gKi9cclxuZnVuY3Rpb24gSG9yaXpvbnRhbFJ1bGVyR3VpZGVNb2RlbChvcHRpb25zKSB7XHJcblxyXG4gICAgUnVsZXJHdWlkZS5jYWxsKHRoaXMsIG9wdGlvbnMpOyAvL2NhbGwgdGhlIHN1cGVyIGNvbnN0cnVjdG9yXHJcblxyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmluaXQoKTtcclxuICAgIFxyXG4gICAgLypcclxuICAgICAqIFxyXG4gICAgICovXHJcbiAgICB0aGlzLnJlbmRlciA9IGZ1bmN0aW9uIChjb250ZXh0LCBvYmopIHtcclxuICAgICAgICB0aGlzLmNsZWFyQ2FudmFzKCk7XHJcbiAgICAgICAgLy9zZWVlbXMgbGlrZSBub3QgbmVjZXNzYXJpbHkgdGhlIHJpZ2h0IHBsYWNlIGZvciB0aGlzIGNhbGN1bGF0aW9uXHJcbiAgICAgICAgaWYob2JqICYmIG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMpe1xyXG4gICAgICAgICAgICBjb25zdCB5ID0gb2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy55O1xyXG4gICAgICAgICAgICB2YXIgdkxpbmUgPSBuZXcgTGluZSgwLCB5LCB0aGlzLmNhbnZhc1dpZHRoLCB5KTtcclxuICAgICAgICAgICAgdkxpbmUuZHJhdyh0aGlzLmNvbnRleHQpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgLypcclxuICAgICAqIE92ZXJyaWRlIHBhcmVudCdzIG9iamVjdCBkZWZhdWx0IGJlaGF2aW91clxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZVpvb21FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICAvL25lZWQgdG8gb3ZlcnJpZGUgdGhlIG1ldGhvZCBub3QgdG8gem9vbSBpbiB0aGlzIGNvbnRleHRcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogT3ZlcnJpZGUgcGFyZW50J3Mgb2JqZWN0IGRlZmF1bHQgYmVoYXZpb3VyXHJcbiAgICAgKi9cclxuICAgIHRoaXMuaGFuZGxlUGFuRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcblxyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBPdmVycmlkZSBwYXJlbnQncyBvYmplY3QgZGVmYXVsdCBiZWhhdmlvdXJcclxuICAgICAqL1xyXG4gICAgdGhpcy5oYW5kbGVTb2NrZXRQYW5FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuXHJcbiAgICB9O1xyXG5cclxuXHJcblxyXG59XHJcbi8vIFNldHVwIHByb3RvdHlwZSBjaGFpbiBieSBzZXR0aW5nIHByb3RvdHlwZSBwcm9wZXJ0eSwgXHJcbi8vIFJ1bGVyR3VpZGUgaXMgcHJvdG90eXBlLlxyXG4vLyBBbmQgYSBzaW5nbGUgcHJvcGVydHkgY29uc3Ryb2N0b3IgPSBIb3Jpem9udGFsUnVsZXJHdWlkZU1vZGVsXHJcbkhvcml6b250YWxSdWxlckd1aWRlTW9kZWwucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShSdWxlckd1aWRlLnByb3RvdHlwZSwge1xyXG4gICAgY29uc3RydWN0b3I6IHtcclxuICAgICAgICB2YWx1ZTogSG9yaXpvbnRhbFJ1bGVyR3VpZGVNb2RlbFxyXG4gICAgfVxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSG9yaXpvbnRhbFJ1bGVyR3VpZGVNb2RlbDsiLCJ2YXIgTGF5ZXJzID0gcmVxdWlyZSgnLi4vLi4vbGF5ZXJzL0xheWVyQVBJJyk7XHJcbnZhciBCYXNlTW9kZWwgPSByZXF1aXJlKCcuLi9CYXNlTW9kZWwnKTtcclxuLyogXHJcbiAqIEFuIHBhcmVudCBjbGFzcyBmcm9tIHdoaWNoIGhvcml6b250YWwgYW5kIHZlcnRpY2FsIGd1aWRlcyB3aWxsIGluaGVyaXRcclxuICovXHJcbmZ1bmN0aW9uIFJ1bGVyR3VpZGUob3B0aW9ucykge1xyXG5cclxuICAgIC8vY29uc3Qgb3B0aW9ucyA9IHt9O1xyXG4gICAgLy9vcHRpb25zLmNhbnZhcyA9ICQoTGF5ZXJzLmNyZWF0ZShvcHRpb25zLmNhbnZhcywgXCJSdWxlckd1aWRlc0xheWVyXCIsIDUpKTsgXHJcbiAgICAvL29wdGlvbnMuc29ja2V0ID0gbnVsbDtcclxuICAgIEJhc2VNb2RlbC5jYWxsKHRoaXMsIG9wdGlvbnMpOyAvL2NhbGwgdGhlIHN1cGVyIGNvbnN0cnVjdG9yXHJcblxyXG5cdGNvbnN0IHRoYXQgPSB0aGlzO1xyXG5cdHRoaXMudG9nZ2xlUnVsZXJHdWlkZVZpc2liaWxpdHlTdGF0dXNWYWx1ZSA9IDA7XHJcblxyXG4gICAgLy90aGlzLmNhbnZhcyA9ICQoTGF5ZXJzLmNyZWF0ZShjYW52YXMsIFwiUnVsZXJHdWlkZXNMYXllclwiLCA1KSk7IFxyXG4gICAgLy90aGlzLmNvbnRleHQgPSB0aGlzLmNhbnZhc1swXS5nZXRDb250ZXh0KCcyZCcpOyAvLyBzZXQgdXAgY29udGV4dCBmb3IgcmVuZGVyIGZ1bmN0aW9uXHJcbiAgICAvL3RoaXMuY29udGV4dC50cmFuc2Zvcm0gPSBuZXcgVHJhbnNmb3JtKHRoaXMuY29udGV4dCk7XHJcblxyXG4gICAgdGhpcy5jYW52YXNXaWR0aCA9IHRoaXMuY2FudmFzLndpZHRoKCk7XHJcbiAgICB0aGlzLmNhbnZhc0hlaWdodCA9IHRoaXMuY2FudmFzLmhlaWdodCgpO1xyXG5cclxuXHQkLnN1YnNjcmliZSgndG9nZ2xlUnVsZXJHdWlkZVZpc2liaWxpdHknLCBmdW5jdGlvbiggZXZlbnQsIG9iaiApIHtcclxuICAgICAgICB0aGF0LmhhbmRsZVRvZ2dsZVJ1bGVyR3VpZGVWaXNpYmlsaXR5RXZlbnQuY2FsbCh0aGF0LCBvYmopO1xyXG4gICAgICAgIC8vbmVlZCB0aGlzIGNhbGwgdG8gY2xlYXIgY2FudmFzIGFmdGVyIEd1aWRlcyB3ZXJlIHR1cm5lZCBvZmZcclxuICAgICAgICB0aGF0LmNsZWFyQ2FudmFzLmNhbGwodGhhdCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvKlxyXG4gICAgICogUmVhY3RpbmcgdG8gdGhlIG1vdXNlIGV2ZW50c1xyXG4gICAgICovXHJcbiAgICAvKiQuc3Vic2NyaWJlKCdtb3VzZV9tb3ZlJywgZnVuY3Rpb24oIGV2ZW50LCBvYmogKSB7O1xyXG4gICAgXHRpZih0aGF0LnNob3VsZERyYXdSdWxlckd1aWRlcygpKXtcclxuICAgIFx0XHR0aGF0LnJlbmRlci5jYWxsKHRoYXQsIHRoYXQuY29udGV4dCwgb2JqKTtcclxuICAgIFx0fVxyXG4gICAgfSk7Ki9cclxuICAgIGlmKHRoaXMucHVic3ViKXtcclxuICAgICAgICB0aGlzLnB1YnN1Yi5vbignbW91c2VfbW92ZScsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICBpZih0aGF0LnNob3VsZERyYXdSdWxlckd1aWRlcygpKXtcclxuICAgICAgICAgICAgICAgIHRoYXQucmVuZGVyLmNhbGwodGhhdCwgdGhhdC5jb250ZXh0LCBvYmopO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn07XHJcblxyXG4vLyBUaGlzIGxpdGVyYWxseSBvdmVycmlkZXMgdGhlIHByb3RvdHlwZSwgc28gdG8gYXZvaWQgbG9vc2luZyBhbGwgdGhlIFJ1bGVyR3VpZGUucHJvdG90eXBlIGZ1bmN0aW9uc1xyXG4vL3RoaXMgcHJvdG90eXBlIGRlY2FscmF0aW9ucyBtdXN0IGJlIGJlZm9yZSB0aGUgZnVuY3Rpb25zIGFyZSBhZGRlZCB0byB0aGUgcHJvdG90eXBlXHJcblJ1bGVyR3VpZGUucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShCYXNlTW9kZWwucHJvdG90eXBlLCB7XHJcbiAgICBjb25zdHJ1Y3Rvcjoge1xyXG4gICAgICAgIHZhbHVlOiBSdWxlckd1aWRlXHJcbiAgICB9XHJcbn0pO1xyXG5cclxuXHJcbi8qXHJcbiAqIENoZWNrIGlmIFJ1bGVyIEd1aWRlcyBzaG91bGQgYmUgZHJhd24uXHJcbiAqXHJcbiAqIEByZXR1cm5zIHRydWUgaW4gY2FzZSBSdWxlciBHdWlkZXMgbmVlZCB0byBiZSBkcmF3blxyXG4gKi9cclxuUnVsZXJHdWlkZS5wcm90b3R5cGUuc2hvdWxkRHJhd1J1bGVyR3VpZGVzID0gZnVuY3Rpb24gKCkge1xyXG5cdGlmICh0aGlzLnRvZ2dsZVJ1bGVyR3VpZGVWaXNpYmlsaXR5U3RhdHVzVmFsdWUgPT09IDEpe1xyXG5cdFx0cmV0dXJuIHRydWU7XHJcblx0fVxyXG5cdHJldHVybiBmYWxzZTtcclxufTtcclxuXHJcblJ1bGVyR3VpZGUucHJvdG90eXBlLmhhbmRsZVRvZ2dsZVJ1bGVyR3VpZGVWaXNpYmlsaXR5RXZlbnQgPSBmdW5jdGlvbiAoKSB7XHJcblx0dGhpcy50b2dnbGVSdWxlckd1aWRlVmlzaWJpbGl0eVN0YXR1cygpO1xyXG59O1xyXG5cclxuUnVsZXJHdWlkZS5wcm90b3R5cGUudG9nZ2xlUnVsZXJHdWlkZVZpc2liaWxpdHlTdGF0dXMgPSBmdW5jdGlvbiAoY29udGV4dCkge1xyXG4gICAgdGhpcy50b2dnbGVSdWxlckd1aWRlVmlzaWJpbGl0eVN0YXR1c1ZhbHVlID0gdGhpcy50b2dnbGVSdWxlckd1aWRlVmlzaWJpbGl0eVN0YXR1c1ZhbHVlID09PSAwID8gMSA6IDA7XHJcbn07XHJcblxyXG5SdWxlckd1aWRlLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiAoY29udGV4dCkge1xyXG4gICAgY29uc29sZS5sb2coXCJyZW5kZXIgaXMgbm90IGltcGxlbWVudGVkIGluIFJ1bGVyR3VpZGVcIik7XHJcbn07XHJcblxyXG5SdWxlckd1aWRlLnByb3RvdHlwZS5jbGVhckNhbnZhcyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIFxyXG4gICAgICAgIHZhciB0b3BMZWZ0ID0ge307XHJcbiAgICAgICAgLy90b3BMZWZ0LnggPSAoMCAtIHRoaXMucGFuRGlzcGxhY2VtZW50LngpL3RoaXMuem9vbUxldmVsO1xyXG4gICAgICAgIC8vdG9wTGVmdC55ID0gKDAgLSB0aGlzLnBhbkRpc3BsYWNlbWVudC55KS90aGlzLnpvb21MZXZlbDtcclxuICAgICAgICBcclxuICAgICAgICB2YXIgYm90dG9tUmlnaHQgPSB7fTtcclxuICAgICAgICAvL2JvdHRvbVJpZ2h0LnggPSAodGhpcy5jYW52YXMud2lkdGgoKSApL3RoaXMuem9vbUxldmVsO1xyXG4gICAgICAgIC8vYm90dG9tUmlnaHQueSA9ICh0aGlzLmNhbnZhcy5oZWlnaHQoKSApL3RoaXMuem9vbUxldmVsO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8qdGhpcy5jb250ZXh0LmNsZWFyUmVjdChcclxuICAgICAgICAgICAgICAgIHRvcExlZnQueCwgdG9wTGVmdC55LCBcclxuICAgICAgICAgICAgICAgIGJvdHRvbVJpZ2h0LngsIGJvdHRvbVJpZ2h0LnkpOyovXHJcbiAgICAgICAgdGhpcy5jb250ZXh0LmNsZWFyUmVjdChcclxuICAgICAgICAgICAgICAgIDAsIDAsIFxyXG4gICAgICAgICAgICAgICAgdGhpcy5jYW52YXNXaWR0aCwgdGhpcy5jYW52YXNIZWlnaHQpO1xyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBSdWxlckd1aWRlOyIsInZhciBMaW5lID0gcmVxdWlyZSgnLi4vLi4vc2hhcGVzL0xpbmUnKTtcclxudmFyIFJ1bGVyR3VpZGUgPSByZXF1aXJlKCcuL1J1bGVyR3VpZGUnKTtcclxuXHJcbi8qIFxyXG4gKiBSdWxlciBHdWlkZSB3aGljaCBydW5zIGhvcml6b250YWxseSBmcm9tIHRoZSBsZWZ0IHZlcnRpY2FsIHJ1bGVyIHRvIG1vdXNlIHBvaW50ZXJcclxuICogKHRvIHRoZSByaWdodCBlZGdlIG9mIGNhbnZhcywgb3B0aW9uYWxseSlcclxuICovXHJcbmZ1bmN0aW9uIFZlcnRpY2FsUnVsZXJHdWlkZU1vZGVsKG9wdGlvbnMpIHtcclxuXHJcbiAgICBSdWxlckd1aWRlLmNhbGwodGhpcywgb3B0aW9ucyk7IC8vY2FsbCB0aGUgc3VwZXIgY29uc3RydWN0b3JcclxuICAgIFxyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmluaXQoKTtcclxuICAgIFxyXG4gICAgLypcclxuICAgICAqIFxyXG4gICAgICovXHJcbiAgICB0aGlzLnJlbmRlciA9IGZ1bmN0aW9uIChjb250ZXh0LCBvYmopIHtcclxuICAgICAgICB0aGlzLmNsZWFyQ2FudmFzKCk7XHJcbiAgICAgICAgLy9zZWVlbXMgbGlrZSBub3QgbmVjZXNzYXJpbHkgdGhlIHJpZ2h0IHBsYWNlIGZvciB0aGlzIGNhbGN1bGF0aW9uXHJcbiAgICAgICAgaWYob2JqICYmIG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMpe1xyXG4gICAgICAgICAgICBjb25zdCB4ID0gb2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy54O1xyXG4gICAgICAgICAgICB2YXIgdkxpbmUgPSBuZXcgTGluZSh4LCAwLCB4LCB0aGlzLmNhbnZhc0hlaWdodCk7XHJcbiAgICAgICAgICAgIHZMaW5lLmRyYXcodGhpcy5jb250ZXh0KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgLypcclxuICAgICAqIE92ZXJyaWRlIHBhcmVudCdzIG9iamVjdCBkZWZhdWx0IGJlaGF2aW91clxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZVpvb21FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICAvL25lZWQgdG8gb3ZlcnJpZGUgdGhlIG1ldGhvZCBub3QgdG8gem9vbSBpbiB0aGlzIGNvbnRleHRcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogT3ZlcnJpZGUgcGFyZW50J3Mgb2JqZWN0IGRlZmF1bHQgYmVoYXZpb3VyXHJcbiAgICAgKi9cclxuICAgIHRoaXMuaGFuZGxlUGFuRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcblxyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBPdmVycmlkZSBwYXJlbnQncyBvYmplY3QgZGVmYXVsdCBiZWhhdmlvdXJcclxuICAgICAqL1xyXG4gICAgdGhpcy5oYW5kbGVTb2NrZXRQYW5FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuXHJcbiAgICB9O1xyXG5cclxuXHJcbn1cclxuVmVydGljYWxSdWxlckd1aWRlTW9kZWwucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShSdWxlckd1aWRlLnByb3RvdHlwZSwge1xyXG4gICAgY29uc3RydWN0b3I6IHtcclxuICAgICAgICB2YWx1ZTogVmVydGljYWxSdWxlckd1aWRlTW9kZWxcclxuICAgIH1cclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFZlcnRpY2FsUnVsZXJHdWlkZU1vZGVsOyIsIi8qIFxyXG4gKiBEcmF3IHJ1bGVyXHJcbiAqL1xyXG5cclxudmFyIExpbmUgPSByZXF1aXJlKCcuLi8uLi9zaGFwZXMvTGluZScpO1xyXG52YXIgUnVsZXJNb2RlbCA9IHJlcXVpcmUoJy4vUnVsZXJNb2RlbCcpO1xyXG4vL3ZhciAkID0gZ2xvYmFsLmpRdWVyeSA9IHJlcXVpcmUoXCJqcXVlcnlcIik7IC8vIG5lZWRlZCBmb3IgdGVzdGluZywgdG8gYXZvaWQgXCIkIGlzIG5vdCBkZWZpbmVkXCJcclxuLy9jb25zb2xlLmluZm8oJCk7XHJcbmZ1bmN0aW9uIEhvcml6b250YWxSdWxlck1vZGVsIChvcHRpb25zKSB7XHJcblxyXG4gICAgUnVsZXJNb2RlbC5jYWxsKHRoaXMsIG9wdGlvbnMpOyAvL2NhbGwgdGhlIHN1cGVyIGNvbnN0cnVjdG9yXHJcblxyXG4gICAgdmFyIHRoYXQ7XHJcblxyXG4gICAgdGhpcy51cGRhdGVSdWxlckhvcml6b250YWxUaWNrc0FycmF5ID0gZnVuY3Rpb24gKHJ1bGVyVGlja3NBcnJheSwgbGltaXQsIHNpZ24sIG9mZnNldCkge1xyXG4gICAgICAgLy9ydWxlclRpY2tzQXJyYXkgPSBbXTtcclxuICAgICAgICBjb25zdCBsT2Zmc2V0ID0gb2Zmc2V0ID8gTWF0aC5jZWlsKG9mZnNldCAvIDEwKSAqIDEwIDogMDsgLy8gbmVlZHMgdG8gYmUgcm91bmRlZCB0byB0aGUgbmVhcmVzdCAxMCAoc2luY2UgcnVsZXIgaXMgaW4gMTBzKVxyXG4gICAgICAgIC8vY29uc29sZS5pbmZvKFwibE9mZnNldCBcIiArIG9mZnNldCk7XHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8obE9mZnNldCk7XHJcbiAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IGxpbWl0IDsgaSArPSAxMCl7XHJcbiAgICAgICAgICAgIGlmKGkgPCBsT2Zmc2V0KXtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHZhciBjb29yZCA9IHNpZ24gKiBpO1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUuaW5mbyhcImNvb3JkOlwiICsgY29vcmQgKyBcIiBsaW1pdDpcIiArIGxpbWl0KTtcclxuICAgICAgICAgICAgaWYoY29vcmQgPCAwICYmIGNvb3JkID4gbGltaXQpe1xyXG4gICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIHgwID0gY29vcmQgKyB0aGlzLm9mZnNldFg7XHJcbiAgICAgICAgICAgIHZhciB5MCA9IHRoaXMub2Zmc2V0WTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHZhciB4MSA9IHgwO1xyXG4gICAgICAgICAgICB2YXIgeTEgPSAoY29vcmQgLyAxMDAgPT09IHBhcnNlSW50KGNvb3JkIC8gMTAwKSkgPyAxMCA6IDE1O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgbGluZSA9IG5ldyBMaW5lKHgwLCB5MCwgeDEsIHkxKTtcclxuICAgICAgICAgICAgaWYoeTEgPT09IDEwKXtcclxuICAgICAgICAgICAgICAgIGxpbmUuc2V0TGFiZWwoY29vcmQpO1xyXG4gICAgICAgICAgICAgICAgbGluZS5zZXRMYWJlbE9yaWVudGF0aW9uKFwiaG9yaXpvbnRhbFwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBsaW5lLnNldE1vZGVsU2NhbGUodGhpcy5nZXRTY2FsZVgoKSk7XHJcbiAgICAgICAgICAgIGxpbmUuc2V0U3Ryb2tlU3R5bGUodGhpcy5ydWxlckxpbmVDb2xvcik7XHJcbiAgICAgICAgICAgIHJ1bGVyVGlja3NBcnJheS5wdXNoKGxpbmUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJ1bGVyVGlja3NBcnJheTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5jcmVhdGVSdWxlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgd2lkdGggPSB0aGlzLmNhbnZhcy53aWR0aCgpO1xyXG4gICAgICAgIHRoaXMucnVsZXJUaWNrcy5ob3Jpem9udGFsID0gW107XHJcbiAgICAgICAgdGhpcy5ydWxlclRpY2tzLmhvcml6b250YWwgPSB0aGlzLnVwZGF0ZVJ1bGVySG9yaXpvbnRhbFRpY2tzQXJyYXkodGhpcy5ydWxlclRpY2tzLmhvcml6b250YWwsIHdpZHRoLCAxKTtcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuY2xlYXJDYW52YXMgPSBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgdmFyIHRvcExlZnQgPSB7fTtcclxuICAgICAgICB0b3BMZWZ0LnggPSAoMCAtIHRoaXMucGFuRGlzcGxhY2VtZW50LngpL3RoaXMuem9vbUxldmVsO1xyXG4gICAgICAgIHRvcExlZnQueSA9ICgwIC0gdGhpcy5wYW5EaXNwbGFjZW1lbnQueSkvdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdmFyIGJvdHRvbVJpZ2h0ID0ge307XHJcbiAgICAgICAgYm90dG9tUmlnaHQueCA9ICh0aGlzLmNhbnZhcy53aWR0aCgpICkvdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgYm90dG9tUmlnaHQueSA9ICh0aGlzLmNhbnZhcy5oZWlnaHQoKSApL3RoaXMuem9vbUxldmVsO1xyXG5cclxuICAgICAgICBpZih0aGlzLmNvbnRleHQpe1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHQuY2xlYXJSZWN0KFxyXG4gICAgICAgICAgICAgICAgdG9wTGVmdC54LCB0b3BMZWZ0LnksIFxyXG4gICAgICAgICAgICAgICAgYm90dG9tUmlnaHQueCwgYm90dG9tUmlnaHQueSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICBcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogRWFjaCBjYW52YXMgbW9kZWwgbmVlZHMgdG8gYmUgYWJsZSB0byByZWFjdCB0byBjYW52YXMgcmVzaXppbmcuXHJcbiAgICAgKiBJbiB0aGlzIGNhc2Ugd2UgbmVlZCB0byBlaXRoZXIgc2hvcnRlbiBvciBleHRlbmQgdGhlIHZlcnRpY2FsIHJ1bGVyIHRpY2tzXHJcbiAgICAgKiBkZXBlbmRpbmcgaWYgdGhlIGNhbnZhcyBzaHJ1bmsgb3IgZW5sYXJnZWRcclxuICAgICAqL1xyXG4gICAgdGhpcy5yZWFjdFRvQ2FudmFzUmVzaXplRXZlbnQgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciB3aWR0aCA9IHRoaXMuY2FudmFzLndpZHRoKCk7XHJcbiAgICAgICAgdGhpcy51cGRhdGVSdWxlckhvcml6b250YWxUaWNrc0FycmF5KHRoaXMucnVsZXJUaWNrcy5ob3Jpem9udGFsLCB3aWR0aCwgMSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy51cGRhdGVSdWxlciA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICB0aGlzLnJ1bGVyVGlja3MuaG9yaXpvbnRhbCA9IFtdO1xyXG5cclxuICAgICAgICB2YXIgd2lkdGggPSB0aGlzLmNhbnZhcy53aWR0aCgpO1xyXG4gICAgICAgIC8vY29uc29sZS5pbmZvKHRoaXMucGFuRGlzcGxhY2VtZW50LngpO1xyXG4gICAgICAgIC8vcGFubmluZyB0byB0aGUgcmlnaHQgKGRyYWdnaW5nIDAgdG8gdGhlIHJpZ2h0KVxyXG4gICAgICAgIGlmKHRoaXMucGFuRGlzcGxhY2VtZW50LnggPj0gMCApe1xyXG4gICAgICAgICAgICAvL2JlY2F1c2UgdGhlIHRpY2tzIG11c3Qgc3RhcnQgZnJvbSAwIEkgbmVlZCBmaWxsIGFycmF5IHRvIHRoZSByaWdodCBhbmQgbGVmdCBvZiAwXHJcbiAgICAgICAgICAgIC8vdGh1cyAyIGNhbGxzIHRvIHRoaXMudXBkYXRlUnVsZXJWZXJ0aWNhbFRpY2tzQXJyYXlcclxuICAgICAgICAgICAgY29uc3QgbGVmdExpbWl0ID0gdGhpcy5wYW5EaXNwbGFjZW1lbnQueC90aGlzLmdldFNjYWxlWCgpO1xyXG4gICAgICAgICAgICAvL3RvIHRoZSBsZWZ0IGZyb20gMCB3aGVuIHBhbm5pbmcgcmlnaHQgKG1vdmluZyAwIHRvIHRoZSByaWdodClcclxuICAgICAgICAgICAgdGhpcy5ydWxlclRpY2tzLmhvcml6b250YWwgPSB0aGlzLnVwZGF0ZVJ1bGVySG9yaXpvbnRhbFRpY2tzQXJyYXkodGhpcy5ydWxlclRpY2tzLmhvcml6b250YWwsIGxlZnRMaW1pdCwgLTEpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcmlnaHRMaW1pdCA9IHdpZHRoL3RoaXMuZ2V0U2NhbGVYKCkgLSB0aGlzLnBhbkRpc3BsYWNlbWVudC54L3RoaXMuZ2V0U2NhbGVYKCk7XHJcbiAgICAgICAgICAgIC8vZnJvbSBsZWZ0IGJvcmRlciB0byAwIG1hcmsgd2hlbiBwYW5uaW5nIHJpZ2h0IChtb3ZpbmcgMCB0byB0aGUgcmlnaHQpXHJcbiAgICAgICAgICAgIHRoaXMucnVsZXJUaWNrcy5ob3Jpem9udGFsID0gdGhpcy51cGRhdGVSdWxlckhvcml6b250YWxUaWNrc0FycmF5KHRoaXMucnVsZXJUaWNrcy5ob3Jpem9udGFsLCByaWdodExpbWl0LCAxKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy9wYW5uaW5nIHRvIHRoZSBsZWZ0IChkcmFnZ2luZyAwIHRvIHRoZSBsZWZ0KVxyXG4gICAgICAgIGlmKHRoaXMucGFuRGlzcGxhY2VtZW50LngvdGhpcy5nZXRTY2FsZVgoKSA8IDAgKXtcclxuICAgICAgICAgICAgY29uc3QgcmlnaHRMaW1pdCA9IHdpZHRoL3RoaXMuZ2V0U2NhbGVYKCkgKyB0aGlzLnBhbkRpc3BsYWNlbWVudC54L3RoaXMuZ2V0U2NhbGVYKCkgKiAtMTtcclxuICAgICAgICAgICAgY29uc3QgbGVmdE9yaWdpbiA9IC0xLjAqdGhpcy5wYW5EaXNwbGFjZW1lbnQueC90aGlzLmdldFNjYWxlWCgpO1xyXG4gICAgICAgICAgICB0aGlzLnJ1bGVyVGlja3MuaG9yaXpvbnRhbCA9IHRoaXMudXBkYXRlUnVsZXJIb3Jpem9udGFsVGlja3NBcnJheSh0aGlzLnJ1bGVyVGlja3MuaG9yaXpvbnRhbCwgcmlnaHRMaW1pdCwgMSwgbGVmdE9yaWdpbik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnJlbmRlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGlzLmNsZWFyQ2FudmFzKCk7XHJcbiAgICAgICAgdGhpcy5ydWxlclRpY2tzLmhvcml6b250YWwuZm9yRWFjaChmdW5jdGlvbihlbGVtZW50KXtcclxuICAgICAgICAgICAgZWxlbWVudC5kcmF3KHRoaXMuY29udGV4dCwgMS90aGlzLnpvb21MZXZlbCk7XHJcbiAgICAgICAgfSwgdGhpcyk7XHJcbiAgICB9ICAgIFxyXG4gICAgLypcclxuICAgICAqIE92ZXJyaWRlIHBhcmVudCdzIG9iamVjdCBkZWZhdWx0IGJlaGF2aW91clxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZVpvb21FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICB0aGlzLnpvb21MZXZlbCAqPSBvYmouem9vbUw7XHJcbiAgICAgICAgLy8gbXVzdCBhZGQgdGhpcy5vZmZzZXRYIGJlY2F1c2UgdGhlIG9yaWdpbiBvZiB0aGUgY2FudmFzIGlzIG9mZnNldFxyXG4gICAgICAgIHRoaXMuY29udGV4dC50cmFuc2Zvcm0uem9vbU9uUG9pbnQoXHJcbiAgICAgICAgICAgIG9iai56b29tTCwgMSwgb2JqLm9yaWdpbi54IC0gdGhpcy5vZmZzZXRYLCAwXHJcbiAgICAgICAgKTtcclxuICAgICAgICAvL2NvbnN0IG1hdHJpeCA9IHRoaXMuY29udGV4dC50cmFuc2Zvcm0uZ2V0TWF0cml4KCk7XHJcbiAgICAgICAgdGhpcy5wYW5EaXNwbGFjZW1lbnQueCA9IHRoaXMuY29udGV4dC50cmFuc2Zvcm0uZ2V0WFRyYW5zbGF0aW9uKCk7XHJcbiAgICAgICAgdmFyIHRyYW5zID0gdGhpcy5vZmZzZXRYIC0gdGhpcy5vZmZzZXRYKnRoaXMuZ2V0U2NhbGUoKTtcclxuICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC54ICs9IHRyYW5zO1xyXG5cclxuICAgICAgICB0aGlzLnVwZGF0ZVJ1bGVyKG9iaik7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIodGhpcy5jb250ZXh0LCBvYmopO1xyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBPdmVycmlkZSBwYXJlbnQncyBvYmplY3QgZGVmYXVsdCBiZWhhdmlvdXJcclxuICAgICAqL1xyXG4gICAgdGhpcy5oYW5kbGVQYW5FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyh0aGlzKTtcclxuICAgICAgICAvL2NvbnNvbGUuaW5mbyh0aGlzLmNhbGN1bGF0ZURlbHRhRGlzcGxhY2VtZW50KG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMueCwgdGhpcy5wYW5EaXNwbGFjZW1lbnQueCwgdGhpcy5tb3VzZURvd25PcmlnaW4ueCkpO1xyXG4gICAgICAgIC8vY29uc29sZS5pbmZvKG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMueCArIFwiIC0gcnVsZXIgLSBcIiArIHRoaXMubW91c2VEb3duT3JpZ2luLnggKyBcIiAtIHJ1bGVyIC0gXCIgKyB0aGlzLnBhbkRpc3BsYWNlbWVudC54KTtcclxuICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC54ICs9IHRoaXMuY2FsY3VsYXRlRGVsdGFEaXNwbGFjZW1lbnQob2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy54LCB0aGlzLnBhbkRpc3BsYWNlbWVudC54LCB0aGlzLm1vdXNlRG93bk9yaWdpbi54KTtcclxuICAgICAgICAvLyB0cmFucyBoZXJlIGlzIGFsaXR0bGUgYml0IHRvIGFjY291bnQgZm9yIHRoZSBmYWN0IHRoYXQgb3JpZ2luIG9mIHRoZSBjYW52YXMgaXMgb2Zmc2V0XHJcbiAgICAgICAgLy92YXIgdHJhbnMgPSB0aGlzLmNvbnRleHQudHJhbnNmb3JtLmdldFhUcmFuc2xhdGlvbigpO1xyXG4gICAgICAgIHZhciB0cmFucyA9IHRoaXMub2Zmc2V0WCAtIHRoaXMub2Zmc2V0WCp0aGlzLmdldFNjYWxlKCk7XHJcbiAgICAgICAgdGhpcy5wYW5EaXNwbGFjZW1lbnQueCArPSB0cmFucztcclxuXHJcbiAgICAgICAgdGhpcy5jb250ZXh0LnRyYW5zZm9ybS50cmFuc2xhdGUoXHJcbiAgICAgICAgICAgIHRoaXMucGFuRGlzcGxhY2VtZW50LngsIDBcclxuICAgICAgICApO1xyXG4gICAgICAgIHRoaXMudXBkYXRlUnVsZXIob2JqKTtcclxuICAgICAgICB0aGlzLnJlbmRlcih0aGlzLmNvbnRleHQsIG9iaik7XHJcbiAgICB9O1xyXG4gICAgLypcclxuICAgICAqIE92ZXJyaWRlIHBhcmVudCdzIG9iamVjdCBkZWZhdWx0IGJlaGF2aW91clxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZVNvY2tldFBhbkV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG4gICAgICAgIHRoaXMubW91c2VEb3duT3JpZ2luID0gb2JqLm9yaWdpbjtcclxuICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC54ICs9IHRoaXMuY2FsY3VsYXRlRGVsdGFEaXNwbGFjZW1lbnQob2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy54LCB0aGlzLnBhbkRpc3BsYWNlbWVudC54LCB0aGlzLm1vdXNlRG93bk9yaWdpbi54KTtcclxuICAgICAgICAvL3RoaXMucGFuRGlzcGxhY2VtZW50LnkgKz0gdGhpcy5jYWxjdWxhdGVEZWx0YURpc3BsYWNlbWVudChvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzLnksIHRoaXMucGFuRGlzcGxhY2VtZW50LnksIHRoaXMubW91c2VEb3duT3JpZ2luLnkpO1xyXG4gICAgICAgIHRoaXMuY29udGV4dC50cmFuc2Zvcm0udHJhbnNsYXRlKFxyXG4gICAgICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC54LCB0aGlzLnBhbkRpc3BsYWNlbWVudC55XHJcbiAgICAgICAgKTtcclxuICAgICAgICB0aGlzLnVwZGF0ZVJ1bGVyKG9iaik7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIodGhpcy5jb250ZXh0LCBvYmopO1xyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBIYW5kbGUgdGhlIHJlc2l6ZSBldmVudCBmcm9tIGhlcmUgYW5kIGNhbGwgc3VwZXIncyBtZXRob2RcclxuICAgICAqL1xyXG4gICAgIHRoaXMuaGFuZGxlV2luZG93UmVzaXplZEV2ZW50ID0gZnVuY3Rpb24ob2JqKSB7XHJcbiAgICAgICAgdGhpcy5fX3Byb3RvX18uaGFuZGxlV2luZG93UmVzaXplZEV2ZW50LmNhbGwodGhpcywgb2JqLCB0cnVlKTtcclxuICAgICAgICB0aGlzLnVwZGF0ZVJ1bGVyKCk7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIob2JqKTtcclxuICAgICB9XHJcbiAgICAvKlxyXG4gICAgICogVGhpcyBpbml0KCkgZnVuY3Rpb24gbXVzdCBiZSBvbiB0aGUgYm90dG9tIG9mIHRoZSBkZWNsYXJhdGlvblxyXG4gICAgICogb3RoZXJ3aXNlIChiZWNhdXNlIHRoZXJlIGlzIG5vIGZ1bmN0aW9uIGhvaXN0aW5nKSBmdW5jdGlvbnMgY2FsbGVkIGZyb20gaW5pdCgpXHJcbiAgICAgKiB3aWxsIGJlIGZyb20gcGFyZW50cyBwcm90b3R5cGUgc2luY2UgdGhlIHRoaXMncyAgZnVuY3Rpb24gYXJlIG5vdCBkZWZpbmVkIHlldFxyXG4gICAgICogZm9yIGV4YW1wbGUgcmVuZGVyKClcclxuICAgICAqL1xyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoYXQgPSB0aGlzO1xyXG4gICAgICAgIHRoaXMucnVsZXJPcmlnaW5YID0gdGhpcy5vZmZzZXRYO1xyXG4gICAgICAgIHRoaXMucnVsZXJPcmlnaW5ZID0gdGhpcy5vZmZzZXRZO1xyXG4gICAgICAgIHRoaXMuY3JlYXRlUnVsZXIoKTtcclxuICAgICAgICB0aGlzLnJlbmRlcigpO1xyXG5cclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMuaW5pdCgpO1xyXG5cclxufVxyXG5cclxuLy8gVGhpcyBsaXRlcmFsbHkgb3ZlcnJpZGVzIHRoZSBwcm90b3R5cGUsIHNvIHRvIGF2b2lkIGxvb3NpbmcgYWxsIHRoZSBSdWxlckd1aWRlLnByb3RvdHlwZSBmdW5jdGlvbnNcclxuLy90aGlzIHByb3RvdHlwZSBkZWNhbHJhdGlvbnMgbXVzdCBiZSBiZWZvcmUgdGhlIGZ1bmN0aW9ucyBhcmUgYWRkZWQgdG8gdGhlIHByb3RvdHlwZVxyXG5Ib3Jpem9udGFsUnVsZXJNb2RlbC5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKFJ1bGVyTW9kZWwucHJvdG90eXBlLCB7XHJcbiAgICBjb25zdHJ1Y3Rvcjoge1xyXG4gICAgICAgIHZhbHVlOiBIb3Jpem9udGFsUnVsZXJNb2RlbFxyXG4gICAgfVxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSG9yaXpvbnRhbFJ1bGVyTW9kZWw7IiwiLyogXHJcbiAqIERyYXcgcnVsZXJcclxuICovXHJcbnZhciBMYXllcnMgPSByZXF1aXJlKCcuLi8uLi9sYXllcnMvTGF5ZXJBUEknKTtcclxudmFyIEJhc2VNb2RlbCA9IHJlcXVpcmUoJy4uL0Jhc2VNb2RlbCcpO1xyXG5cclxuZnVuY3Rpb24gUnVsZXJNb2RlbCAob3B0aW9ucykge1xyXG4gICAgXHJcbiAgICAvL29wdGlvbnMuY2FudmFzID0galF1ZXJ5KExheWVycy5vbmVPZihvcHRpb25zLmNhbnZhcykpOyBcclxuXHJcbiAgICBCYXNlTW9kZWwuY2FsbCh0aGlzLCBvcHRpb25zKTsgLy9jYWxsIHRoZSBzdXBlciBjb25zdHJ1Y3RvclxyXG5cclxuICAgIHZhciB0aGF0O1xyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vdGhlIGVkZ2VzIG9mIHRoZSBSdWxlckxheWVyIGFyZSBub3QgZmx1c2ggd2l0aCBQYXBlciBjYW52YXMgZWRnZXMsIHRoZSBhcmUgb2Zmc2V0XHJcbiAgICAgICAgdGhpcy5vZmZzZXRYID0gMjA7XHJcbiAgICAgICAgdGhpcy5vZmZzZXRZID0gMjE7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5ydWxlclRpY2tzID0ge307Ly8gb2JqZWN0IHRvIGhvbGQgYWxsIHRoZSBsaW5lcyByZXF1aXJlZCB0byBkcmF3IHJ1bGVyXHJcbiAgICAgICAgdGhpcy5ydWxlclRpY2tzLmhvcml6b250YWwgPSBbXTtcclxuICAgICAgICB0aGlzLnJ1bGVyVGlja3MudmVydGljYWwgPSBbXTtcclxuXHJcbiAgICAgICAgLy90aGlzLmNhbnZhc0JvcmRlciA9IFtdOyAvLyBob2xkcyA0IGxpbmVzIHRvIGRyYXcgdGhlIGJvcmRlciBhcm91bmQgdGhlIGNhbnZhc1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vdGhpcy5jYW52YXMgPSAkKExheWVycy5vbmVPZihjYW52YXMpKTsgXHJcbiAgICAgICAgLy90aGlzLmNvbnRleHQgPSB0aGlzLmNhbnZhc1swXS5nZXRDb250ZXh0KCcyZCcpOyAvLyBzZXQgdXAgY29udGV4dCBmb3IgcmVuZGVyIGZ1bmN0aW9uXHJcblxyXG4gICAgICAgIHRoaXMucnVsZXJMaW5lQ29sb3IgPSBcIiMwMDAwMDBcIjtcclxuICAgICAgIFxyXG4gICAgICAgIC8vdGhhdCA9IHRoaXM7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy90aGlzLmNyZWF0ZVJ1bGVyKCk7XHJcbiAgICAgICAgLy90aGlzLnJlbmRlcigpO1xyXG4gICAgfTtcclxuICAgIHRoaXMuaW5pdCgpO1xyXG4gICAgXHJcbiAgICB0aGlzLmNyZWF0ZVJ1bGVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgXHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLmNsZWFyQ2FudmFzID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnJlbmRlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjb25zb2xlLmluZm8oXCJ1cFwiKTtcclxuICAgICAgICAvKlxyXG4gICAgICAgIHRoaXMuY2xlYXJDYW52YXMoKTtcclxuXHJcbiAgICAgICAgdGhpcy5ydWxlclRpY2tzQW5kT3V0bGluZS5ob3Jpem9udGFsLmZvckVhY2goZnVuY3Rpb24oZWxlbWVudCl7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuZHJhdyh0aGlzLmNvbnRleHQpO1xyXG4gICAgICAgIH0sIHRoaXMpO1xyXG5cclxuICAgICAgICB0aGlzLnJ1bGVyVGlja3NBbmRPdXRsaW5lLnZlcnRpY2FsLmZvckVhY2goZnVuY3Rpb24oZWxlbWVudCl7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuZHJhdyh0aGlzLmNvbnRleHQpO1xyXG4gICAgICAgIH0sIHRoaXMpO1xyXG5cclxuICAgICAgICB0aGlzLmNhbnZhc0JvcmRlci5mb3JFYWNoKGZ1bmN0aW9uKGVsZW1lbnQpe1xyXG4gICAgICAgICAgICBlbGVtZW50LmRyYXcodGhpcy5jb250ZXh0KTtcclxuICAgICAgICB9LCB0aGlzKTtcclxuICAgICAgICAqL1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogT3ZlcnJpZGUgcGFyZW50J3Mgb2JqZWN0IGRlZmF1bHQgYmVoYXZpb3VyXHJcbiAgICAgKi9cclxuICAgIC8qdGhpcy5oYW5kbGVab29tRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcblxyXG4gICAgfTsqL1xyXG4gICAgLypcclxuICAgICAqIE92ZXJyaWRlIHBhcmVudCdzIG9iamVjdCBkZWZhdWx0IGJlaGF2aW91clxyXG4gICAgICovXHJcbiAgICAvKnRoaXMuaGFuZGxlUGFuRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5wYW5EaXNwbGFjZW1lbnQueCArPSB0aGlzLmNhbGN1bGF0ZURlbHRhRGlzcGxhY2VtZW50KG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMueCwgdGhpcy5wYW5EaXNwbGFjZW1lbnQueCwgdGhpcy5tb3VzZURvd25PcmlnaW4ueCk7XHJcbiAgICBcclxuICAgICAgICAvL3RoaXMucGFuRGlzcGxhY2VtZW50LnkgKz0gdGhpcy5jYWxjdWxhdGVEZWx0YURpc3BsYWNlbWVudChvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzLnksIHRoaXMucGFuRGlzcGxhY2VtZW50LnksIHRoaXMubW91c2VEb3duT3JpZ2luLnkpO1xyXG5cclxuICAgICAgICB0aGlzLmNvbnRleHQudHJhbnNmb3JtLnRyYW5zbGF0ZShcclxuICAgICAgICAgICAgdGhpcy5wYW5EaXNwbGFjZW1lbnQueCwgMFxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIHRoaXMucmVuZGVySG9yaXpvbnRhbFJ1bGVyKHRoaXMuY29udGV4dCwgb2JqKTtcclxuICAgICAgICBcclxuICAgIH07Ki9cclxuICAgIC8qXHJcbiAgICAgKiBPdmVycmlkZSBwYXJlbnQncyBvYmplY3QgZGVmYXVsdCBiZWhhdmlvdXJcclxuICAgICAqL1xyXG4gICAgLyp0aGlzLmhhbmRsZVNvY2tldFBhbkV2ZW50ID0gZnVuY3Rpb24gKG9iaikge1xyXG5cclxuICAgIH07Ki9cclxuICAgIFxyXG4gICAgXHJcbn1cclxuXHJcbi8vIFRoaXMgbGl0ZXJhbGx5IG92ZXJyaWRlcyB0aGUgcHJvdG90eXBlLCBzbyB0byBhdm9pZCBsb29zaW5nIGFsbCB0aGUgUnVsZXJHdWlkZS5wcm90b3R5cGUgZnVuY3Rpb25zXHJcbi8vdGhpcyBwcm90b3R5cGUgZGVjYWxyYXRpb25zIG11c3QgYmUgYmVmb3JlIHRoZSBmdW5jdGlvbnMgYXJlIGFkZGVkIHRvIHRoZSBwcm90b3R5cGVcclxuUnVsZXJNb2RlbC5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKEJhc2VNb2RlbC5wcm90b3R5cGUsIHtcclxuICAgIGNvbnN0cnVjdG9yOiB7XHJcbiAgICAgICAgdmFsdWU6IFJ1bGVyTW9kZWxcclxuICAgIH1cclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFJ1bGVyTW9kZWw7XHJcblxyXG4iLCIvKiBcclxuICogRHJhdyBydWxlclxyXG4gKiBUT0RPOiBzdWNreSBjb21tZW50cy4gTmVlZCB0byBpbXByb3ZlLlxyXG4gKi9cclxuXHJcbnZhciBMaW5lID0gcmVxdWlyZSgnLi4vLi4vc2hhcGVzL0xpbmUnKTtcclxudmFyIFJ1bGVyTW9kZWwgPSByZXF1aXJlKCcuL1J1bGVyTW9kZWwnKTtcclxuLy92YXIgJCA9IGdsb2JhbC5qUXVlcnkgPSByZXF1aXJlKFwianF1ZXJ5XCIpOyAvLyBuZWVkZWQgZm9yIHRlc3RpbmcsIHRvIGF2b2lkIFwiJCBpcyBub3QgZGVmaW5lZFwiXHJcbi8vY29uc29sZS5pbmZvKCQpO1xyXG5mdW5jdGlvbiBWZXJ0aWNhbFJ1bGVyTW9kZWwgKG9wdGlvbnMpIHtcclxuXHJcbiAgICBSdWxlck1vZGVsLmNhbGwodGhpcywgb3B0aW9ucyk7IC8vY2FsbCB0aGUgc3VwZXIgY29uc3RydWN0b3JcclxuXHJcbiAgICB2YXIgdGhhdDtcclxuXHJcbiAgICB0aGlzLnVwZGF0ZVJ1bGVyVmVydGljYWxUaWNrc0FycmF5ID0gZnVuY3Rpb24gKHJ1bGVyVGlja3NBcnJheSwgbGltaXQsIHNpZ24pIHtcclxuICAgICAgICAvL3J1bGVyVGlja3NBcnJheSA9IFtdO1xyXG4gICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCBsaW1pdCA7IGkgKz0gMTApe1xyXG4gICAgICAgICAgICB2YXIgY29vcmQgPSBzaWduICogaTtcclxuICAgICAgICAgICAgdmFyIHkwID0gY29vcmQgKyB0aGlzLm9mZnNldFg7XHJcbiAgICAgICAgICAgIHZhciB4MCA9IHRoaXMub2Zmc2V0WDtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHZhciB5MSA9IHkwO1xyXG4gICAgICAgICAgICB2YXIgeDEgPSAoY29vcmQgLyAxMDAgPT09IHBhcnNlSW50KGNvb3JkIC8gMTAwKSkgPyAxMCA6IDE1O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgbGluZSA9IG5ldyBMaW5lKHgwLCB5MCwgeDEsIHkxKTtcclxuICAgICAgICAgICAgaWYoeDEgPT09IDEwKXtcclxuICAgICAgICAgICAgICAgIGxpbmUuc2V0TGFiZWwoY29vcmQpO1xyXG4gICAgICAgICAgICAgICAgbGluZS5zZXRMYWJlbE9yaWVudGF0aW9uKFwidmVydGljYWxcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbGluZS5zZXRNb2RlbFNjYWxlKHRoaXMuZ2V0U2NhbGVZKCkpO1xyXG4gICAgICAgICAgICBsaW5lLnNldFN0cm9rZVN0eWxlKHRoaXMucnVsZXJMaW5lQ29sb3IpO1xyXG4gICAgICAgICAgICBydWxlclRpY2tzQXJyYXkucHVzaChsaW5lKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBydWxlclRpY2tzQXJyYXk7XHJcbiAgICB9O1xyXG5cclxuXHJcbiAgICB0aGlzLmNyZWF0ZVJ1bGVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBoZWlnaHQgPSB0aGlzLmNhbnZhcy5oZWlnaHQoKTtcclxuICAgICAgICB0aGlzLnJ1bGVyVGlja3MudmVydGljYWwgPSB0aGlzLnVwZGF0ZVJ1bGVyVmVydGljYWxUaWNrc0FycmF5KHRoaXMucnVsZXJUaWNrcy52ZXJ0aWNhbCwgaGVpZ2h0LCAxKTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5jbGVhckNhbnZhcyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciB0b3BMZWZ0ID0ge307XHJcbiAgICAgICAgdG9wTGVmdC54ID0gKDAgLSB0aGlzLnBhbkRpc3BsYWNlbWVudC54KS90aGlzLmdldFNjYWxlWCgpO1xyXG4gICAgICAgIHRvcExlZnQueSA9ICgwIC0gdGhpcy5wYW5EaXNwbGFjZW1lbnQueSkvdGhpcy5nZXRTY2FsZVkoKTtcclxuICAgICAgICBcclxuICAgICAgICB2YXIgYm90dG9tUmlnaHQgPSB7fTtcclxuICAgICAgICBib3R0b21SaWdodC54ID0gKHRoaXMuY2FudmFzLndpZHRoKCkgKS90aGlzLmdldFNjYWxlWCgpO1xyXG4gICAgICAgIGJvdHRvbVJpZ2h0LnkgPSAodGhpcy5jYW52YXMuaGVpZ2h0KCkgKS90aGlzLmdldFNjYWxlWSgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuY29udGV4dC5jbGVhclJlY3QoXHJcbiAgICAgICAgICAgICAgICB0b3BMZWZ0LngsIHRvcExlZnQueSwgXHJcbiAgICAgICAgICAgICAgICBib3R0b21SaWdodC54LCBib3R0b21SaWdodC55KTtcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogRWFjaCBjYW52YXMgbW9kZWwgbm5lZCB0byBiZSBhYmxlIHRvIHJlYWN0IHRvIGNhbnZhcyByZXNpemluZy5cclxuICAgICAqIEluIHRoaXMgY2FzZSB3ZSBuZWVkIHRvIGVpdGhlciBzaG9ydGVuIG9yIGV4dGVuZCB0aGUgdmVydGljYWwgcnVsZXIgdGlja3NcclxuICAgICAqIGRlcGVuZGluZyBpZiB0aGUgY2FudmFzIHNocnVuayBvciBlbmxhcmdlZFxyXG4gICAgICovXHJcbiAgICB0aGlzLnJlYWN0VG9DYW52YXNSZXNpemVFdmVudCA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyIGhlaWdodCA9IHRoaXMuY2FudmFzLmhlaWdodCgpO1xyXG4gICAgICAgIHRoaXMudXBkYXRlUnVsZXJWZXJ0aWNhbFRpY2tzQXJyYXkodGhpcy5ydWxlclRpY2tzLnZlcnRpY2FsLCBoZWlnaHQsIDEpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMudXBkYXRlUnVsZXIgPSBmdW5jdGlvbiAob2JqKSB7XHJcblxyXG4gICAgICAgIHRoaXMucnVsZXJUaWNrcy52ZXJ0aWNhbCA9IFtdO1xyXG5cclxuICAgICAgICB2YXIgaGVpZ2h0ID0gdGhpcy5jYW52YXMuaGVpZ2h0KCk7XHJcbiAgICAgICAgLy9wYW5uaW5nIGRvd24gKGRyYWdnaW5nIDAgZG93bilcclxuICAgICAgICBpZih0aGlzLnBhbkRpc3BsYWNlbWVudC55L3RoaXMuZ2V0U2NhbGVZKCkgPj0gMCApe1xyXG4gICAgICAgICAgICAvLyBiZWNhdXNlIHRoZSB0aWNrcyBtdXN0IHN0YXJ0IGZyb20gMCBJIG5lZWQgZmlsbCBhcnJheSB0byB0aGUgcmlnaHQgYW5kIGxlZnQgb2YgMFxyXG4gICAgICAgICAgICAvLyB0aHVzIDIgY2FsbHMgdG8gdGhpcy51cGRhdGVSdWxlclZlcnRpY2FsVGlja3NBcnJheVxyXG4gICAgICAgICAgICBjb25zdCB1cHBlckxpbWl0ID0gdGhpcy5wYW5EaXNwbGFjZW1lbnQueS90aGlzLmdldFNjYWxlWSgpO1xyXG4gICAgICAgICAgICB0aGlzLnJ1bGVyVGlja3MudmVydGljYWwgPSB0aGlzLnVwZGF0ZVJ1bGVyVmVydGljYWxUaWNrc0FycmF5KHRoaXMucnVsZXJUaWNrcy52ZXJ0aWNhbCwgdXBwZXJMaW1pdCwgLTEpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgbG93ZXJMaW1pdCA9IGhlaWdodC90aGlzLmdldFNjYWxlWSgpIC0gdGhpcy5wYW5EaXNwbGFjZW1lbnQueS90aGlzLmdldFNjYWxlWSgpO1xyXG4gICAgICAgICAgICB0aGlzLnJ1bGVyVGlja3MudmVydGljYWwgPSB0aGlzLnVwZGF0ZVJ1bGVyVmVydGljYWxUaWNrc0FycmF5KHRoaXMucnVsZXJUaWNrcy52ZXJ0aWNhbCwgbG93ZXJMaW1pdCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vcGFubmluZyB1cCAoZHJhZ2dpbmcgMCB1cClcclxuICAgICAgICBpZih0aGlzLnBhbkRpc3BsYWNlbWVudC55L3RoaXMuZ2V0U2NhbGVZKCkgPCAwICl7XHJcbiAgICAgICAgICAgIGNvbnN0IGxvd2VyTGltaXQgPSBoZWlnaHQvdGhpcy5nZXRTY2FsZVkoKSArIHRoaXMucGFuRGlzcGxhY2VtZW50LnkvdGhpcy5nZXRTY2FsZVkoKSAqIC0xO1xyXG4gICAgICAgICAgICB0aGlzLnJ1bGVyVGlja3MudmVydGljYWwgPSB0aGlzLnVwZGF0ZVJ1bGVyVmVydGljYWxUaWNrc0FycmF5KHRoaXMucnVsZXJUaWNrcy52ZXJ0aWNhbCwgbG93ZXJMaW1pdCwgMSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnJlbmRlciA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGlzLmNsZWFyQ2FudmFzKCk7XHJcbiAgICAgICAgdGhpcy5ydWxlclRpY2tzLnZlcnRpY2FsLmZvckVhY2goZnVuY3Rpb24oZWxlbWVudCl7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuZHJhdyh0aGlzLmNvbnRleHQsIDEvdGhpcy56b29tTGV2ZWwpO1xyXG4gICAgICAgIH0sIHRoaXMpO1xyXG4gICAgfSAgXHJcblxyXG4gICAgLypcclxuICAgICAqIE92ZXJyaWRlIHBhcmVudCdzIG9iamVjdCBkZWZhdWx0IGJlaGF2aW91clxyXG4gICAgICovXHJcbiAgICB0aGlzLmhhbmRsZVpvb21FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICB0aGlzLnpvb21MZXZlbCAqPSBvYmouem9vbUw7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8gbXVzdCBhZGQgdGhpcy5vZmZzZXRYIGJlY2F1c2UgdGhlIG9yaWdpbiBvZiB0aGUgY2FudmFzIGlzIG9mZnNldFxyXG4gICAgICAgIHRoaXMuY29udGV4dC50cmFuc2Zvcm0uem9vbU9uUG9pbnQoXHJcbiAgICAgICAgICAgIDEsIG9iai56b29tTCwgMCwgb2JqLm9yaWdpbi55ICsgdGhpcy5vZmZzZXRYXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgY29uc3QgbWF0cml4ID0gdGhpcy5jb250ZXh0LnRyYW5zZm9ybS5nZXRNYXRyaXgoKTtcclxuICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC55ID0gdGhpcy5jb250ZXh0LnRyYW5zZm9ybS5nZXRZVHJhbnNsYXRpb24oKTtcclxuXHJcbiAgICAgICAgdGhpcy51cGRhdGVSdWxlcihvYmopO1xyXG4gICAgICAgIHRoaXMucmVuZGVyKHRoaXMuY29udGV4dCwgb2JqKTtcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogT3ZlcnJpZGUgcGFyZW50J3Mgb2JqZWN0IGRlZmF1bHQgYmVoYXZpb3VyXHJcbiAgICAgKi9cclxuICAgIHRoaXMuaGFuZGxlUGFuRXZlbnQgPSBmdW5jdGlvbiAob2JqKSB7XHJcblxyXG4gICAgICAgIHRoaXMucGFuRGlzcGxhY2VtZW50LnkgKz0gdGhpcy5jYWxjdWxhdGVEZWx0YURpc3BsYWNlbWVudChvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzLnksIHRoaXMucGFuRGlzcGxhY2VtZW50LnksIHRoaXMubW91c2VEb3duT3JpZ2luLnkpO1xyXG5cclxuICAgICAgICAvLyB0cmFucyBoZXJlIGlzIGEgbGl0dGxlIGJpdCB0byBhY2NvdW50IGZvciB0aGUgZmFjdCB0aGF0IG9yaWdpbiBvZiB0aGUgY2FudmFzIGlzIG9mZnNldFxyXG4gICAgICAgIHZhciB0cmFucyA9IHRoaXMuY29udGV4dC50cmFuc2Zvcm0uZ2V0WVRyYW5zbGF0aW9uKCk7XHJcbiAgICAgICAgdHJhbnMgPSB0aGlzLm9mZnNldFggLSB0aGlzLm9mZnNldFgqdGhpcy5nZXRTY2FsZVkoKTtcclxuICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC55ICs9IHRyYW5zO1xyXG5cclxuICAgICAgICB0aGlzLmNvbnRleHQudHJhbnNmb3JtLnRyYW5zbGF0ZShcclxuICAgICAgICAgICAgMCwgdGhpcy5wYW5EaXNwbGFjZW1lbnQueVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIHRoaXMudXBkYXRlUnVsZXIob2JqKTtcclxuXHJcbiAgICAgICAgdGhpcy5yZW5kZXIodGhpcy5jb250ZXh0LCBvYmopO1xyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBPdmVycmlkZSBwYXJlbnQncyBvYmplY3QgZGVmYXVsdCBiZWhhdmlvdXJcclxuICAgICAqL1xyXG4gICAgdGhpcy5oYW5kbGVTb2NrZXRQYW5FdmVudCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgICAgICB0aGlzLm1vdXNlRG93bk9yaWdpbiA9IG9iai5vcmlnaW47XHJcblxyXG4gICAgICAgIC8vdGhpcy5wYW5EaXNwbGFjZW1lbnQueCArPSB0aGlzLmNhbGN1bGF0ZURlbHRhRGlzcGxhY2VtZW50KG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMueCwgdGhpcy5wYW5EaXNwbGFjZW1lbnQueCwgdGhpcy5tb3VzZURvd25PcmlnaW4ueCk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5wYW5EaXNwbGFjZW1lbnQueSArPSB0aGlzLmNhbGN1bGF0ZURlbHRhRGlzcGxhY2VtZW50KG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMueSwgdGhpcy5wYW5EaXNwbGFjZW1lbnQueSwgdGhpcy5tb3VzZURvd25PcmlnaW4ueSk7XHJcblxyXG4gICAgICAgIHRoaXMuY29udGV4dC50cmFuc2Zvcm0udHJhbnNsYXRlKFxyXG4gICAgICAgICAgICB0aGlzLnBhbkRpc3BsYWNlbWVudC54LCB0aGlzLnBhbkRpc3BsYWNlbWVudC55XHJcbiAgICAgICAgKTtcclxuICAgICAgICB0aGlzLnVwZGF0ZVJ1bGVyKG9iaik7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIodGhpcy5jb250ZXh0LCBvYmopO1xyXG5cclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogSGFuZGxlIHRoZSByZXNpemUgZXZlbnQgZnJvbSBoZXJlIGFuZCBjYWxsIHN1cGVyJ3MgbWV0aG9kXHJcbiAgICAgKi9cclxuICAgICB0aGlzLmhhbmRsZVdpbmRvd1Jlc2l6ZWRFdmVudCA9IGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgIHRoaXMuX19wcm90b19fLmhhbmRsZVdpbmRvd1Jlc2l6ZWRFdmVudC5jYWxsKHRoaXMsIG9iaiwgdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy51cGRhdGVSdWxlcigpO1xyXG4gICAgICAgIHRoaXMucmVuZGVyKG9iaik7XHJcbiAgICAgfVxyXG4gICAgLypcclxuICAgICAqIFRoaXMgaW5pdCgpIGZ1bmN0aW9uIG11c3QgYmUgb24gdGhlIGJvdHRvbSBvZiB0aGUgZGVjbGFyYXRpb25cclxuICAgICAqIG90aGVyd2lzZSAoYmVjYXVzZSB0aGVyZSBpcyBubyBmdW5jdGlvbiBob2lzdGluZykgZnVuY3Rpb25zIGNhbGxlZCBmcm9tIGluaXQoKVxyXG4gICAgICogd2lsbCBiZSBmcm9tIHBhcmVudHMgcHJvdG90eXBlIHNpbmNlIHRoZSB0aGlzJ3MgIGZ1bmN0aW9uIGFyZSBub3QgZGVmaW5lZCB5ZXRcclxuICAgICAqIGZvciBleGFtcGxlIHJlbmRlcigpXHJcbiAgICAgKi9cclxuICAgIHRoaXMuaW5pdCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGF0ID0gdGhpcztcclxuICAgICAgICB0aGlzLnJ1bGVyT3JpZ2luWCA9IHRoaXMub2Zmc2V0WDtcclxuICAgICAgICB0aGlzLnJ1bGVyT3JpZ2luWSA9IHRoaXMub2Zmc2V0WTtcclxuICAgICAgICB0aGlzLmNyZWF0ZVJ1bGVyKCk7XHJcbiAgICAgICAgdGhpcy5yZW5kZXIoKTtcclxuXHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLmluaXQoKTtcclxuICAgIFxyXG59XHJcblxyXG4vLyBUaGlzIGxpdGVyYWxseSBvdmVycmlkZXMgdGhlIHByb3RvdHlwZSwgc28gdG8gYXZvaWQgbG9vc2luZyBhbGwgdGhlIFJ1bGVyR3VpZGUucHJvdG90eXBlIGZ1bmN0aW9uc1xyXG4vL3RoaXMgcHJvdG90eXBlIGRlY2FscmF0aW9ucyBtdXN0IGJlIGJlZm9yZSB0aGUgZnVuY3Rpb25zIGFyZSBhZGRlZCB0byB0aGUgcHJvdG90eXBlXHJcblZlcnRpY2FsUnVsZXJNb2RlbC5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKFJ1bGVyTW9kZWwucHJvdG90eXBlLCB7XHJcbiAgICBjb25zdHJ1Y3Rvcjoge1xyXG4gICAgICAgIHZhbHVlOiBWZXJ0aWNhbFJ1bGVyTW9kZWxcclxuICAgIH1cclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFZlcnRpY2FsUnVsZXJNb2RlbDtcclxuXHJcbiIsIid1c2Ugc3RyaWN0JztcclxudmFyIEJhc2VNb2RlbCA9IHJlcXVpcmUoJy4vQmFzZU1vZGVsJyk7XHJcbmNsYXNzIFNlbGVjdGVkU2hhcGVNb2RlbCBleHRlbmRzIEJhc2VNb2RlbHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XHJcbiAgICAgICAgLy9CYXNlTW9kZWwuY2FsbCh0aGlzLCBvcHRpb25zKTsgLy9jYWxsIHRoZSBzdXBlciBjb25zdHJ1Y3RvclxyXG4gICAgICAgIHN1cGVyKG9wdGlvbnMpO1xyXG4gICAgICAgIHRoaXMuc29ja2V0dCA9IG9wdGlvbnMuc29ja2V0O1xyXG4gICAgICAgIHRoaXMuc2hhcGVzID0gb3B0aW9ucy5zaGFwZXNTdG9yYWdlO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRTaGFwZUluZGV4ID0gLTE7XHJcbiAgICAgICAgXHJcbiAgICAgICAgY29uc3QgdGhhdCA9IHRoaXM7XHJcblxyXG4gICAgICAgIC8qaWYodGhpcy5wdWJzdWIpe1xyXG4gICAgICAgICAgICB0aGlzLnB1YnN1Yi5vbignbW91c2VfbW92ZScsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZU1vdmVFdmVudChvYmopO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9Ki9cclxuICAgICAgICAvL0Jhc2VNb2RlbCBoYW5kbGVzICdtb3VzZV9sZWZ0X2J1dHRvbl9kb3duJyB0b28sIHNvIFwibG9jYWxcIiBoYW5kbGVNb3VzZUxlZnRCdXR0b25Eb3duRXZlbnQgd2lsbCBiZSBjYWxsZWQgdmlhXHJcbiAgICAgICAgLy9wb2x5bW9yaGlzbS4gSSBuZWVkIHRvIGNhbGwgc3VwZXIncyBtZXRob2QgaGVyZSB0aG91Z2guIFxyXG4gICAgICAgIC8qaWYodGhpcy5wdWJzdWIpe1xyXG4gICAgICAgICAgICB0aGlzLnB1YnN1Yi5vbignbW91c2VfbGVmdF9idXR0b25fZG93bicsIGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5oYW5kbGVNb3VzZUxlZnRCdXR0b25Eb3duRXZlbnQob2JqKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSovXHJcbiAgICB9XHJcbiAgICAvKlxyXG4gICAgICogVGhpcyBvbmUgc2ltcGx5IGhpZ2hsaWdodHMgc2hhcGVzIGlmIG1vdXNlIHBhc3NlcyBvdmVyIHRoZW1cclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIHt0eXBlfSBvYmpcclxuICAgICAqIEByZXR1cm5zIHtub25lfVxyXG4gICAgICovXHJcbiAgICAvKmhhbmRsZU1vdXNlTW92ZUV2ZW50IChvYmopIHtcclxuICAgICAgIGlmKG9iai5pc0RyYXdpbmcgPT09IGZhbHNlKXtcclxuICAgICAgICAgIHRoaXMubWFya1NlbGVjdGVkU2hhcGVJZk1vdXNlT3Zlckl0KG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXMueCwgb2JqLm1vdXNlQ29vcmRzT3ZlckNhbnZhcy55KTtcclxuICAgICAgIH0gICAgICAgXHJcbiAgICB9OyovXHJcblxyXG4gICAgbWFya1NlbGVjdGVkU2hhcGVJZk1vdXNlT3Zlckl0IChtb3VzZVgsIG1vdXNlWSkge1xyXG4gICAgICAgIHZhciBzZWxlY3RlZFNoYXBlSW5kZXggPSB0aGlzLmdldFNlbGVjdGVkU2hhcGVJbmRleChtb3VzZVgsIG1vdXNlWSk7XHJcbiAgICAgICAgdGhpcy5zZXRTZWxlY3RlZFNoYXBlKHNlbGVjdGVkU2hhcGVJbmRleCk7XHJcbiAgICB9O1xyXG5cclxuICAgIGdldFNlbGVjdGVkU2hhcGVJbmRleCAobW91c2VYLCBtb3VzZVkpIHtcclxuICAgICAgICB2YXIgbGVuID0gdGhpcy5zaGFwZXMuc2l6ZSgpO1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcclxuICAgICAgICAgICAgdmFyIHNoYXBlID0gdGhpcy5zaGFwZXMuZ2V0KGkpO1xyXG4gICAgICAgICAgICBpZiAoc2hhcGUuaXNQb2ludE92ZXIodGhpcy5jb250ZXh0LCBtb3VzZVgsIG1vdXNlWSkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAtMTtcclxuICAgIH07XHJcblxyXG4gICAgc2V0U2VsZWN0ZWRTaGFwZSAoaW5kZXgpIHtcclxuICAgICAgICBpZiAoaW5kZXggPj0gMCAmJiBpbmRleCA8IHRoaXMuc2hhcGVzLnNpemUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkU2hhcGVJbmRleCA9IGluZGV4O1xyXG4gICAgICAgICAgICAkLnB1Ymxpc2goXCJzaGFwZV9zZWxlY3RlZFwiLCB7c2VsZWN0ZWRTaGFwZUluZGV4OmluZGV4fSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBoYW5kbGVNb3VzZUxlZnRCdXR0b25Eb3duRXZlbnQgKG9iaikge1xyXG5cdFx0aWYoIG9iaiA9PT0gbnVsbCl7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuICAgICAgICBCYXNlTW9kZWwucHJvdG90eXBlLmhhbmRsZU1vdXNlTGVmdEJ1dHRvbkRvd25FdmVudChvYmopO1xyXG4gICAgICAgIHRoaXMubWFya1NlbGVjdGVkU2hhcGVJZk1vdXNlT3Zlckl0KG9iai5tb3VzZUNvb3Jkc092ZXJDYW52YXNBZGp1c3RlZC54LCBvYmoubW91c2VDb29yZHNPdmVyQ2FudmFzQWRqdXN0ZWQueSk7XHJcbiAgICB9XHJcbiAgICAvKlxyXG4gICAgICogcGxhY2Vob2xkZXIgaW4gb3JkZXIgbm90IHRvIGNydXNoIHdoZW4gcGFyZW50IEJhc2VNb2RlbCBjYWxscyBpdHMgY2hpbGRyZW4ncyByZW5kZXIgbWV0aG9kXHJcbiAgICAgKi9cclxuICAgIHJlbmRlcigpe1xyXG4gICAgICAgIC8vbmVlZCB0byBoYXZlIGl0IHRoZXJlIGJlY2F1c2UgQmFzZU1vZGVsIGNhbGxzIHJlbmRlcigpIGZvciBpdHMgY2hpbGRyZW4sIGkuZS4gd2hlbiBwYW5uaW5nXHJcbiAgICB9XHJcbn1cclxuLypcclxuU2VsZWN0ZWRTaGFwZU1vZGVsLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoQmFzZU1vZGVsLnByb3RvdHlwZSwge1xyXG4gICAgY29uc3RydWN0b3I6IHtcclxuICAgICAgICB2YWx1ZTogU2VsZWN0ZWRTaGFwZU1vZGVsXHJcbiAgICB9XHJcbn0pO1xyXG4qL1xyXG5tb2R1bGUuZXhwb3J0cyA9IFNlbGVjdGVkU2hhcGVNb2RlbDsiLCJ2YXIgRXZlbnRFbWl0dGVyID0gcmVxdWlyZSgnZXZlbnRzJykuRXZlbnRFbWl0dGVyXHJcbiAgLCBwdWJzdWIgPSBuZXcgRXZlbnRFbWl0dGVyKCkuc2V0TWF4TGlzdGVuZXJzKDEwMCk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9ICBwdWJzdWI7XHJcblxyXG5wdWJzdWIub24oJ2xvZ2dlZEluJywgZnVuY3Rpb24obXNnKSB7XHJcbiAgICBjb25zb2xlLmxvZyhtc2cpO1xyXG59KTsiLCIndXNlIHN0cmljdCc7XHJcblxyXG52YXIgSG9yaXpvbnRhbFJ1bGVyTW9kZWwgPSByZXF1aXJlKCcuLi9tb2RlbHMvUnVsZXJNb2RlbC9Ib3Jpem9udGFsUnVsZXJNb2RlbCcpO1xyXG52YXIgVmVydGljYWxSdWxlck1vZGVsID0gcmVxdWlyZSgnLi4vbW9kZWxzL1J1bGVyTW9kZWwvVmVydGljYWxSdWxlck1vZGVsJyk7XHJcbnZhciBHcmlkTW9kZWwgPSByZXF1aXJlKCcuLi9tb2RlbHMvR3JpZE1vZGVsJyk7XHJcbnZhciBEcmF3aW5nVG9vbGJhck1vZGVsID0gcmVxdWlyZSgnLi4vbW9kZWxzL0RyYXdpbmdUb29sYmFyTW9kZWwnKTtcclxuXHJcbnZhciBJbmZvQmxvY2sgPSByZXF1aXJlKCcuLi9pbmZvX2Jsb2NrL0luZm9CbG9jaycpO1xyXG52YXIgQmFzaWNNb2RlbCA9IHJlcXVpcmUoJy4uL21vZGVscy9CYXNpY01vZGVsJyk7XHJcbnZhciBTaGFwZVN0b3JlQXJyYXkgPSByZXF1aXJlKCcuLi9zdG9yYWdlL3NoYXBlcy9TaGFwZVN0b3JlQXJyYXknKTsgLy8gbmVlZHMgdG8gYmUgaW5qZWN0ZWQgaW50byBCYXNpY01vZGVsXHJcblxyXG52YXIgSG9yaXpvbnRhbFJ1bGVyR3VpZGVNb2RlbCA9IHJlcXVpcmUoJy4uL21vZGVscy9SdWxlckd1aWRlL0hvcml6b250YWxSdWxlckd1aWRlTW9kZWwnKTtcclxudmFyIFZlcnRpY2FsYWxSdWxlckd1aWRlTW9kZWwgPSByZXF1aXJlKCcuLi9tb2RlbHMvUnVsZXJHdWlkZS9WZXJ0aWNhbFJ1bGVyR3VpZGVNb2RlbCcpO1xyXG52YXIgQ3VycmVudERyYXdpbmdNb2RlbCA9IHJlcXVpcmUoJy4uL21vZGVscy9DdXJyZW50RHJhd2luZ01vZGVsJyk7XHJcbnZhciBTZWxlY3RlZFNoYXBlTW9kZWwgPSByZXF1aXJlKCcuLi9tb2RlbHMvU2VsZWN0ZWRTaGFwZU1vZGVsJyk7XHJcblxyXG52YXIgQ2FudmFzQm9yZGVyTW9kZWwgPSByZXF1aXJlKCcuLi9tb2RlbHMvQ2FudmFzQm9yZGVyTW9kZWwnKTtcclxuXHJcbnZhciBMYXllcnMgPSByZXF1aXJlKCcuLi9sYXllcnMvTGF5ZXJBUEknKTtcclxuXHJcbnZhciBUcmFuc2Zvcm0gPSByZXF1aXJlKCcuLi9tYXRyaXgvVHJhbnNmb3JtJyk7XHJcblxyXG5jbGFzcyBJbml0aWFsaXphdGlvbkZhY3Rvcnkge1xyXG4gICAgLypcclxuICAgIHBhcmFtOiB7fSBvcHRpb25zXHJcbiAgICAgICAgb3B0aW9ucy5jYW52YXMgPSBjYW52YXM7XHJcbiAgICAgICAgb3B0aW9ucy5zb2NrZXQgPSBzb2NrZXQ7XHJcbiAgICAgICAgb3B0aW9ucy5wdWJzdWIgPSBwdWJzdWI7XHJcbiAgICAgICAgLi4uXHJcbiAgICAgICAgb3B0aW9ucy5jb250ZXh0ID0gY29udGV4dDtcclxuICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XHJcblxyXG4gICAgICAgIC8vbGV0IG9yaWdpbmFsQ29udGV4dCA9IG9wdGlvbnMuY2FudmFzWzBdLmdldENvbnRleHQoJzJkJyk7XHJcbiAgICAgICAgLy9vcmlnaW5hbENvbnRleHQudHJhbnNmb3JtID0gbmV3IFRyYW5zZm9ybShvcmlnaW5hbENvbnRleHQpO1xyXG4gICAgICAgIGNvbnN0IG9yaWdpbmFsQ2FudmFzID0gb3B0aW9ucy5jYW52YXM7IC8vIG5lZWQgdG8gcHJlc2VydmUgdGhlIG9yaWdpbmFsXHJcblxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgb3B0aW9ucy5jYW52YXMgPSB0aGlzLmNyZWF0ZUNhbnZhcyhvcmlnaW5hbENhbnZhcywgXCJjdXJyZW50X2RyYXdpbmdfY2FudmFzXCIpO1xyXG4gICAgICAgIG9wdGlvbnMuY29udGV4dCA9IHRoaXMuY3JlYXRlQ2FudmFzQ29udGV4dChvcHRpb25zLmNhbnZhcyk7XHJcbiAgICAgICAgbmV3IEN1cnJlbnREcmF3aW5nTW9kZWwob3B0aW9ucyk7XHJcbiAgICAgICAgKi9cclxuICAgICAgICBuZXcgQ3VycmVudERyYXdpbmdNb2RlbCh0aGlzLnByZXBNb2RlbE9wdGlvbnMob3B0aW9ucywgb3JpZ2luYWxDYW52YXMsIFwiY3VycmVudF9kcmF3aW5nX2NhbnZhc1wiKSk7XHJcblxyXG4gICAgICAgIG9wdGlvbnMuc2hhcGVzU3RvcmFnZSA9IG5ldyBTaGFwZVN0b3JlQXJyYXkoKTtcclxuXHJcbiAgICAgICAgbmV3IFNlbGVjdGVkU2hhcGVNb2RlbCh0aGlzLnByZXBNb2RlbE9wdGlvbnMob3B0aW9ucywgb3JpZ2luYWxDYW52YXMsIFwic2VsZWN0ZWRfc2hhcGVfY2FudmFzXCIpKTsgLy9cclxuICAgICAgICAvL3QubWFya1NlbGVjdGVkU2hhcGVJZk1vdXNlT3Zlckl0KHt9KTtcclxuICAgICAgICAvKlxyXG4gICAgICAgIG9wdGlvbnMuY2FudmFzID0gdGhpcy5jcmVhdGVDYW52YXMob3JpZ2luYWxDYW52YXMsIFwiYmFzaWNfY2FudmFzXCIpO1xyXG4gICAgICAgIG9wdGlvbnMuY29udGV4dCA9IHRoaXMuY3JlYXRlQ2FudmFzQ29udGV4dChvcHRpb25zLmNhbnZhcyk7XHJcbiAgICAgICAgY29uc3QgYmFzaWNNb2RlbCA9IG5ldyBCYXNpY01vZGVsKG9wdGlvbnMpOyAvLyBuZWVkIHRvIGluamVjdCBpdCBpbnRvIEluZm9CbG9ja1xyXG4gICAgICAgICovXHJcbiAgICAgICAgY29uc3QgYmFzaWNNb2RlbCA9IG5ldyBCYXNpY01vZGVsKHRoaXMucHJlcE1vZGVsT3B0aW9ucyhvcHRpb25zLCBvcmlnaW5hbENhbnZhcywgXCJiYXNpY19jYW52YXNcIikpOyAvL25lZWQgdG8gaW5qZWN0IGl0IGludG8gSW5mb0Jsb2NrIHRoZXJlZm9yZSBjcmVhdGluZyB2YXJpYWJsZVxyXG4gICAgICAgIC8vc2hhcGVTdG9yYWdlIGlzIG9ubHkgbmVlZGVkIGZvciBCYXNpY01vZGVsIHNvIGNhbiBzZXQgaXQgdG8gbnVsbCBhdCB0aGlzIHBvaW50XHJcbiAgICAgICAgb3B0aW9ucy5zaGFwZXNTdG9yYWdlID0gbnVsbDtcclxuICAgICAgICAvL2RyYXdzIGxpdHRsZSBpbmZvIGJsb2NrIGFib3V0IHNlbGVjdGVkIHNoYXBlXHJcbiAgICAgICAgLy9vcHRpb25zLnNoYXBlc1N0b3JhZ2UgPSBudWxsO1xyXG4gICAgICAgIC8vb3B0aW9ucy5jYW52YXMgPSBudWxsO1xyXG4gICAgICAgIC8vb3B0aW9ucy5jb250ZXh0ID0gbnVsbDtcclxuICAgICAgICBvcHRpb25zLmJhc2ljTW9kZWwgPSBiYXNpY01vZGVsO1xyXG4gICAgICAgIG5ldyBJbmZvQmxvY2sob3B0aW9ucyk7XHJcblxyXG4gICAgICAgIC8vb3B0aW9ucy5jYW52YXMgPSAkKExheWVycy5jcmVhdGUob3JpZ2luYWxDYW52YXMsIFwiR3JpZExheWVyXCIsIDUpKTtcclxuICAgICAgICAvKlxyXG4gICAgICAgIG9wdGlvbnMuY2FudmFzID0gdGhpcy5jcmVhdGVDYW52YXMob3JpZ2luYWxDYW52YXMsIFwiZ3JpZF9jYW52YXNcIiwgNSk7XHJcbiAgICAgICAgb3B0aW9ucy5jb250ZXh0ID0gdGhpcy5jcmVhdGVDYW52YXNDb250ZXh0KG9wdGlvbnMuY2FudmFzKTtcclxuICAgICAgICBuZXcgR3JpZE1vZGVsKG9wdGlvbnMpO1xyXG4gICAgICAgICovXHJcbiAgICAgICAgbmV3IEdyaWRNb2RlbCh0aGlzLnByZXBNb2RlbE9wdGlvbnMob3B0aW9ucywgb3JpZ2luYWxDYW52YXMsIFwiZ3JpZF9jYW52YXNcIiwgNSkpO1xyXG5cclxuICAgICAgICBuZXcgRHJhd2luZ1Rvb2xiYXJNb2RlbCgpOyAvL2RyYXdzIHRvb2xiYXIgYW5kIGhhbmRsZXMgaXRzIGV2ZW50c1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgb3B0aW9ucy5jYW52YXMgPSB0aGlzLmNyZWF0ZUNhbnZhcyhvcmlnaW5hbENhbnZhcywgXCJydWxlcl9ndWlkZV9jYW52YXNfaG9yaXpvbnRhbFwiLCA1KTtcclxuICAgICAgICBvcHRpb25zLmNvbnRleHQgPSB0aGlzLmNyZWF0ZUNhbnZhc0NvbnRleHQob3B0aW9ucy5jYW52YXMpO1xyXG4gICAgICAgIG5ldyBIb3Jpem9udGFsUnVsZXJHdWlkZU1vZGVsKG9wdGlvbnMpOyAvL2RyYXdzIHJ1bGVyIGd1aWRlXHJcbiAgICAgICAgKi9cclxuICAgICAgICBuZXcgSG9yaXpvbnRhbFJ1bGVyR3VpZGVNb2RlbCh0aGlzLnByZXBNb2RlbE9wdGlvbnMob3B0aW9ucywgb3JpZ2luYWxDYW52YXMsIFwicnVsZXJfZ3VpZGVfY2FudmFzX2hvcml6b250YWxcIiwgNSkpOyAvL2RyYXdzIHJ1bGVyIGd1aWRlXHJcbiAgICAgICAgLypcclxuICAgICAgICBvcHRpb25zLmNhbnZhcyA9IHRoaXMuY3JlYXRlQ2FudmFzKG9yaWdpbmFsQ2FudmFzLCBcInJ1bGVyX2d1aWRlX2NhbnZhc192ZXJ0aWNhbFwiLCA1KTtcclxuICAgICAgICBvcHRpb25zLmNvbnRleHQgPSB0aGlzLmNyZWF0ZUNhbnZhc0NvbnRleHQob3B0aW9ucy5jYW52YXMpO1xyXG4gICAgICAgIG5ldyBWZXJ0aWNhbGFsUnVsZXJHdWlkZU1vZGVsKG9wdGlvbnMpOyAvL2RyYXdzIHJ1bGVyIGd1aWRlXHJcbiAgICAgICAgKi9cclxuICAgICAgICBuZXcgVmVydGljYWxhbFJ1bGVyR3VpZGVNb2RlbCh0aGlzLnByZXBNb2RlbE9wdGlvbnMob3B0aW9ucywgb3JpZ2luYWxDYW52YXMsIFwicnVsZXJfZ3VpZGVfY2FudmFzX3ZlcnRpY2FsXCIsIDUpKTsgLy9kcmF3cyBydWxlciBndWlkZVxyXG4gICAgICAgIC8vIEZvbGxvd2luZyBjYW52YXNlcyBuZWVkIHRvIGJlIG9mZnNldFxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgb3B0aW9ucy5jYW52YXMgPSB0aGlzLmNyZWF0ZU9mZnNldENhbnZhcyhvcmlnaW5hbENhbnZhcywgXCJydWxlcl9jYW52YXNfaG9yaXpvbnRhbFwiKTtcclxuICAgICAgICBvcHRpb25zLmNvbnRleHQgPSB0aGlzLmNyZWF0ZUNhbnZhc0NvbnRleHQob3B0aW9ucy5jYW52YXMpO1xyXG4gICAgICAgIG5ldyBIb3Jpem9udGFsUnVsZXJNb2RlbChvcHRpb25zKTsgLy9kcmF3cyBydWxlclxyXG4gICAgICAgICovXHJcbiAgICAgICAgbmV3IEhvcml6b250YWxSdWxlck1vZGVsKHRoaXMucHJlcE9mZnNldE1vZGVsT3B0aW9ucyhvcHRpb25zLCBvcmlnaW5hbENhbnZhcywgXCJydWxlcl9jYW52YXNfaG9yaXpvbnRhbFwiKSk7IC8vZHJhd3MgcnVsZXJcclxuICAgICAgICAvKlxyXG4gICAgICAgIG9wdGlvbnMuY2FudmFzID0gdGhpcy5jcmVhdGVPZmZzZXRDYW52YXMob3JpZ2luYWxDYW52YXMsIFwicnVsZXJfY2FudmFzX3ZlcnRpY2FsXCIpO1xyXG4gICAgICAgIG9wdGlvbnMuY29udGV4dCA9IHRoaXMuY3JlYXRlQ2FudmFzQ29udGV4dChvcHRpb25zLmNhbnZhcyk7XHJcbiAgICAgICAgbmV3IFZlcnRpY2FsUnVsZXJNb2RlbChvcHRpb25zKTsgLy9kcmF3cyBydWxlclxyXG4gICAgICAgICovXHJcbiAgICAgICAgbmV3IFZlcnRpY2FsUnVsZXJNb2RlbCh0aGlzLnByZXBPZmZzZXRNb2RlbE9wdGlvbnMob3B0aW9ucywgb3JpZ2luYWxDYW52YXMsIFwicnVsZXJfY2FudmFzX3ZlcnRpY2FsXCIpKTsgLy9kcmF3cyBydWxlclxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgb3B0aW9ucy5jYW52YXMgPSB0aGlzLmNyZWF0ZU9mZnNldENhbnZhcyhvcmlnaW5hbENhbnZhcywgXCJib3JkZXJfY2FudmFzXCIpO1xyXG4gICAgICAgIG9wdGlvbnMuY29udGV4dCA9IHRoaXMuY3JlYXRlQ2FudmFzQ29udGV4dChvcHRpb25zLmNhbnZhcyk7XHJcbiAgICAgICAgbmV3IENhbnZhc0JvcmRlck1vZGVsKG9wdGlvbnMpOyAvL2RyYXdzIGJvdW5kaW5nIGJveFxyXG4gICAgICAgICovXHJcbiAgICAgICAgbmV3IENhbnZhc0JvcmRlck1vZGVsKHRoaXMucHJlcE9mZnNldE1vZGVsT3B0aW9ucyhvcHRpb25zLCBvcmlnaW5hbENhbnZhcywgXCJib3JkZXJfY2FudmFzXCIpKTsgLy9kcmF3cyBib3VuZGluZyBib3hcclxuICAgIH1cclxuXHJcbiAgICBwcmVwTW9kZWxPcHRpb25zKG9wdGlvbnMsIGNhbnZhcywgY2FudmFzTmFtZSwgekluZGV4KXtcclxuICAgICAgICBpZih6SW5kZXgpe1xyXG4gICAgICAgICAgICBvcHRpb25zLmNhbnZhcyA9IHRoaXMuY3JlYXRlQ2FudmFzKGNhbnZhcywgY2FudmFzTmFtZSwgekluZGV4KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBvcHRpb25zLmNhbnZhcyA9IHRoaXMuY3JlYXRlQ2FudmFzKGNhbnZhcywgY2FudmFzTmFtZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIG9wdGlvbnMuY29udGV4dCA9IHRoaXMuY3JlYXRlQ2FudmFzQ29udGV4dChvcHRpb25zLmNhbnZhcyk7XHJcbiAgICAgICAgcmV0dXJuIG9wdGlvbnM7XHJcbiAgICB9XHJcbiAgICBwcmVwT2Zmc2V0TW9kZWxPcHRpb25zKG9wdGlvbnMsIGNhbnZhcywgY2FudmFzTmFtZSl7XHJcbiAgICAgICAgb3B0aW9ucy5jYW52YXMgPSB0aGlzLmNyZWF0ZU9mZnNldENhbnZhcyhjYW52YXMsIGNhbnZhc05hbWUpO1xyXG4gICAgICAgIG9wdGlvbnMuY29udGV4dCA9IHRoaXMuY3JlYXRlQ2FudmFzQ29udGV4dChvcHRpb25zLmNhbnZhcyk7XHJcbiAgICAgICAgcmV0dXJuIG9wdGlvbnM7XHJcbiAgICB9XHJcbiAgICBjcmVhdGVDYW52YXMob3JpZ2luYWxDYW52YXMsIGlkLCB6SW5kZXgpe1xyXG4gICAgICAgIGNvbnN0IG5ld0NhbnZhcyA9ICAkKExheWVycy5jcmVhdGUob3JpZ2luYWxDYW52YXMsIGlkLCB6SW5kZXgpKTtcclxuICAgICAgICByZXR1cm4gbmV3Q2FudmFzO1xyXG4gICAgfVxyXG4gICAgY3JlYXRlT2Zmc2V0Q2FudmFzIChvcmlnaW5hbENhbnZhcywgaWQpIHtcclxuICAgICAgICBjb25zdCBuZXdDYW52YXMgPSBqUXVlcnkoTGF5ZXJzLm9uZU9mKG9yaWdpbmFsQ2FudmFzLCBpZCkpO1xyXG4gICAgICAgIHJldHVybiBuZXdDYW52YXM7XHJcbiAgICB9XHJcbiAgICBjcmVhdGVDYW52YXNDb250ZXh0KGNhbnZhcyl7XHJcbiAgICAgICAgbGV0IGNvbnRleHQgPSBjYW52YXNbMF0uZ2V0Q29udGV4dCgnMmQnKTtcclxuICAgICAgICBjb250ZXh0LnRyYW5zZm9ybSA9IG5ldyBUcmFuc2Zvcm0oY29udGV4dCk7XHJcbiAgICAgICAgcmV0dXJuIGNvbnRleHQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSW5pdGlhbGl6YXRpb25GYWN0b3J5OyIsIi8qXHJcblRoaXMgaXMgYSBtYWluIGNvbnRyb2xsZXIgZmlsZS4gSXQgaW5pdGlhbGl6ZXMgZXZlcnl0aGluZyBhbmQgLi4uXHJcbi0gaGFuZGxlcyBzb2NrZXQub24oKSBldmVudHMgZm9yIGFsbCBicm93c2VyIENMSUVOVHNcclxuKi9cclxuXHJcbmNvbnN0IE1vdXNlSGFuZGxlciA9IHJlcXVpcmUoJy4vYWN0aW9ucy9Nb3VzZUhhbmRsZXInKTtcclxuXHJcbmNvbnN0IERyYXdpbmdTdGF0ZU1vZGVsID0gcmVxdWlyZSgnLi9tb2RlbHMvRHJhd2luZ1N0YXRlTW9kZWwnKTtcclxuXHJcbmNvbnN0IEV2ZW50RmFjdG9yeSA9IHJlcXVpcmUoJy4vZXZlbnRzL0V2ZW50RmFjdG9yeScpO1xyXG5cclxuY29uc3QgSW5pdGlhbGl6YXRpb25GYWN0b3J5ID0gcmVxdWlyZSgnLi9zY2FmZm9sZC9Jbml0aWFsaXphdGlvbkZhY3RvcnknKTtcclxuXHJcbmNvbnN0IFBhblpvb21Nb2RlbCA9IHJlcXVpcmUoJy4vbW9kZWxzL1Bhblpvb21Nb2RlbCcpO1xyXG5cclxuY29uc3QgTGluZSA9IHJlcXVpcmUoJy4vc2hhcGVzL0xpbmUnKTtcclxuXHJcbmNvbnN0IHB1YnN1YiA9IHJlcXVpcmUoJy4vcHVic3ViL1B1YlN1YicpO1xyXG5cclxuY29uc3QgVHJhbnNmb3JtID0gcmVxdWlyZSgnLi9tYXRyaXgvVHJhbnNmb3JtJyk7XHJcblxyXG5jb25zdCB1dWlkVjQgPSByZXF1aXJlKCd1dWlkL3Y0Jyk7XHJcblxyXG4vLyBBICQoIGRvY3VtZW50ICkucmVhZHkoKSBibG9jay5cclxuJChmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgdmFyIHNvY2tldCA9IGlvLmNvbm5lY3QoKTtcclxuXHJcbiAgICBjb25zdCBVVUlEID0gdXVpZFY0KCk7XHJcblxyXG4gICAgZnVuY3Rpb24gZ2V0Um9vbU5hbWUoKXtcclxuICAgICAgICBjb25zdCBwYXRobmFtZSA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZTtcclxuICAgICAgICBjb25zdCBmaWVsZHMgPSBwYXRobmFtZS5zcGxpdCgnLycpO1xyXG4gICAgICAgIC8vZ2V0IHRoZSBsYXN0IGVsZW1lbnRcclxuICAgICAgICBjb25zdCByb29tX25hbWUgPSBmaWVsZHNbZmllbGRzLmxlbmd0aC0xXTtcclxuICAgICAgICByZXR1cm4gcm9vbV9uYW1lIHx8ICdkZWZhdWx0JztcclxuICAgIH1cclxuICAgIC8vXHJcbiAgICBzb2NrZXQub24oJ2Nvbm5lY3QnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgIHNvY2tldC5lbWl0KCAnam9pbicsIHtcclxuICAgICAgICAgICAgbmFtZTogJ2Fub255bW91cycsXHJcbiAgICAgICAgICAgIHJvb21fbmFtZTogZ2V0Um9vbU5hbWUoKSxcclxuICAgICAgICAgICAgdXVpZDpVVUlEXHJcbiAgICAgICAgfSlcclxuICAgIH0pO1xyXG4gICAgLy9uZWVkIHRvIGZpcmUgdGhhdCB0byBnZXQgdGhlIHVzZXJzIHdobyBqb2luZWQgdGhlIHJvb20gYmVmb3JlIHRoZSB1c2VyIHdobyBpcyBqb2luaW5nIGluIG5vd1xyXG4gICAgLy9zb2NrZXQuZW1pdCgnZ2V0LXVzZXJzJywge3Jvb21fbmFtZTogZ2V0Um9vbU5hbWUoKX0pOyBcclxuXHJcbiAgICBzb2NrZXQub24oJ2FsbC11c2VycycsIGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgICAgIHB1YnN1Yi5lbWl0KCd1c2Vyc191cGRhdGVkJywge3VzZXJzOiBkYXRhLCBzZWxmVVVJRDogVVVJRH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdmFyIGNhbnZhcyA9ICQoJyNwYXBlcicpO1xyXG4gICAgLy9jb25zb2xlLmluZm8oY2FudmFzLnBhcmVudCgpLndpZHRoKCkpO1xyXG4gICAgLy9jb25zb2xlLmluZm8oY2FudmFzLnBhcmVudCgpLmhlaWdodCgpKTtcclxuICAgIC8vY29uc29sZS5pbmZvKGNhbnZhcy5wYXJlbnQoKS5vdXRlcldpZHRoKCkpO1xyXG4gICAgLy9jb25zb2xlLmluZm8oY2FudmFzLnBhcmVudCgpLm91dGVySGVpZ2h0KCkpO1xyXG5cclxuICAgIGNvbnN0IGFyYml0cmFyeU9mZnNldFdpZHRoID0gNTsgLy9vZmZzZXQgZnJvbSB0aGUgcmlnaHQgZWRnZVxyXG4gICAgY29uc3QgYXJiaXRyYXJ5T2Zmc2V0SGVpZ2h0ID0gNTsgLy8gb2Zmc2V0IGZyb20gdGhlIGJvdHRvbVxyXG5cclxuICAgIGNhbnZhcy53aWR0aChjYW52YXMucGFyZW50KCkud2lkdGgoKS1hcmJpdHJhcnlPZmZzZXRXaWR0aCk7XHJcbiAgICBjYW52YXMuaGVpZ2h0KGNhbnZhcy5wYXJlbnQoKS5oZWlnaHQoKS1hcmJpdHJhcnlPZmZzZXRIZWlnaHQpO1xyXG4gICAgLy9leHBlcmltZW50aW5nIHdpdGggY2FudmFzIHdpZHRoIGFuZCBoZWlnaHQgLi4uXHJcbiAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xyXG5cclxuICAgICAgICAkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgICAgICAgICB2YXIgY2FudmFzID0gJCgnI3BhcGVyJyk7XHJcbiAgICAgICAgICAgIGNhbnZhcy53aWR0aChjYW52YXMucGFyZW50KCkud2lkdGgoKS1hcmJpdHJhcnlPZmZzZXRXaWR0aCk7XHJcbiAgICAgICAgICAgIGNhbnZhcy5oZWlnaHQoY2FudmFzLnBhcmVudCgpLmhlaWdodCgpLWFyYml0cmFyeU9mZnNldEhlaWdodCk7XHJcbiAgICAgICAgICAgIHB1YnN1Yi5lbWl0KCd3aW5kb3dfcmVzaXplZCcsIHtjYW52YXM6IGNhbnZhc30pO1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUuaW5mbyhjYW52YXMuaGVpZ2h0KCkpO1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUuaW5mbyhjYW52YXMucGFyZW50KCkud2lkdGgoKSk7XHJcbiAgICAgICAgICAgIC8vY29uc29sZS5pbmZvKGNhbnZhcy5wYXJlbnQoKS5oZWlnaHQoKSk7XHJcbiAgICAgICAgICAgIC8vY2FudmFzLndpZHRoKGNhbnZhcy5wYXJlbnQoKS53aWR0aCgpLTMwKTtcclxuICAgICAgICAgICAgLy9jYW52YXMuaGVpZ2h0KGNhbnZhcy5wYXJlbnQoKS5oZWlnaHQoKS0zMCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHZhciBicmVha3BvaW50V2lkdGggPSA3Njg7Ly90aGlzIEJvb3RzdHJhcCdzIGJyZWFrcG9pbnQgdG8gc3dpdGNoIGZyb20gRGVza3RvcCB0byBNb2JpbGUgbW9kZVxyXG4gICAgICAgIC8vY2xpY2sgaGFuZGxlciBmb3IgYnV0dG9ucyBpbiB0aGUgbWVudVxyXG4gICAgICAgICQoXCJkaXZbZGF0YS10b2dnbGU9J2J1dHRvbnMnXSBsYWJlbFwiKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBpZigkKHdpbmRvdykud2lkdGgoKSA8IGJyZWFrcG9pbnRXaWR0aCApIHtcclxuICAgICAgICAgICAgICAgICQoJy5uYXZiYXItdG9nZ2xlJykuY2xpY2soKTsgLy9jb2xsYXBzZSB0aGUgbmF2YmFyXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy9jbGljayBoYW5kbGVyIGZvciBzaG93aW5nL2hpZGluZyBQcmVwZXJ0aWVzIFNpZGViZXIgZnJvbSB0aGUgbWVudVxyXG4gICAgICAgICQoJyNuYXYtYmFyLXRvZ2dsZS1zaWRlYmFyJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJCgnI3NpZGViYXItd3JhcHBlcicpLnRvZ2dsZUNsYXNzKFwidG9nZ2xlZFwiKTtcclxuICAgICAgICAgICAgaWYoJCh3aW5kb3cpLndpZHRoKCkgPCBicmVha3BvaW50V2lkdGggKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcubmF2YmFyLXRvZ2dsZScpLmNsaWNrKCk7IC8vdG9nZ2xlIGNvbGxhcHNlIG9mIHRoZSBuYXZiYXJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vbWFrZSBzaWRlYmFyIGRyYWdnYWJsZVxyXG4gICAgICAgIC8vJChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgJChcIiNzaWRlYmFyLXdyYXBwZXJcIikuZHJhZ2dhYmxlKCk7XHJcbiAgICAgICAgLy99KTtcclxuICAgIH0pO1xyXG4gICAgXHJcblxyXG4gICAgLy8gVGhpcyBkZW1vIGRlcGVuZHMgb24gdGhlIGNhbnZhcyBlbGVtZW50XHJcbiAgICBpZiAoISgnZ2V0Q29udGV4dCcgaW4gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJykpKSB7XHJcbiAgICAgICAgYWxlcnQoJ1NvcnJ5LCBpdCBsb29rcyBsaWtlIHlvdXIgYnJvd3NlciBkb2VzIG5vdCBzdXBwb3J0IGNhbnZhcyEnKTtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3Qgb3B0aW9ucyA9IHt9O1xyXG4gICAgb3B0aW9ucy5jYW52YXMgPSBjYW52YXM7XHJcbiAgICBvcHRpb25zLnNvY2tldCA9IHNvY2tldDtcclxuICAgIG9wdGlvbnMucHVic3ViID0gcHVic3ViO1xyXG5cclxuICAgIGxldCBjb250ZXh0ID0gb3B0aW9ucy5jYW52YXNbMF0uZ2V0Q29udGV4dCgnMmQnKTtcclxuICAgIC8vY29udGV4dC50cmFuc2Zvcm0gPSBuZXcgVHJhbnNmb3JtKCk7IC8vIGluaXRcclxuICAgIGNvbnRleHQudHJhbnNmb3JtID0gbmV3IFRyYW5zZm9ybSh0aGlzLmNvbnRleHQpO1xyXG4gICAgLy9jb25zb2xlLmluZm8oY29udGV4dCk7XHJcbiAgICBvcHRpb25zLmNvbnRleHQgPSBjb250ZXh0O1xyXG5cclxuXHJcbiAgICBjb25zdCBwYW5ab29tTW9kZWwgPSBuZXcgUGFuWm9vbU1vZGVsKG9wdGlvbnMpOyAvLyBqdXN0IGEgaG9sZGVyIG9mIGluZm9ybWFpdG9uXHJcbiAgICBcclxuICAgIHZhciBkcmF3aW5nU3RhdGVNb2RlbCA9IG5ldyBEcmF3aW5nU3RhdGVNb2RlbChvcHRpb25zKTtcclxuICAgXHJcbiAgICBuZXcgSW5pdGlhbGl6YXRpb25GYWN0b3J5KG9wdGlvbnMpOyAvLyBmdW5ueSBjb2RlOzsgVE9ETzogcmUtZmFjdG9yICwgaSBtZWFuIHdoeSBjcmVhdGUgbmV3IG9iamVjdCB3aGVuIGEgc3RhdGljIG1ldGhvZCB3aWxsIHN1ZmZpY2VcclxuICAgXHJcbiAgICB2YXIgbW91c2VIYW5kbGVyID0gbmV3IE1vdXNlSGFuZGxlcihjYW52YXMpO1xyXG4gICAgXHJcbiAgICB2YXIgbGFzdEVtaXQgPSAkLm5vdygpO1xyXG4gICAgXHJcblx0Ly9BbGwgb2YgdGhlc2Ugc29ja2V0Lm9uIGV2ZW50cyBoYW5kbGVycyBhcmUgbmVlZGVkIGFzIG9uZSBkb2Vzbid0IHN1YnNjcmliZVxyXG5cdC8vdG8gXCJzb2NrZXRcIiBldmVudHMgb24gdGhlIGNsaWVudCBmaWxlcy4gVGhlcmUgaXMgYSBkZWRpY2F0ZWQgcHViL3N1YiBhYnN0cmFjdGlvbi5cclxuXHRcclxuICAgIHNvY2tldC5vbignc29ja2V0X3Bhbm5pbmcnLCBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgIC8vJC5wdWJsaXNoKFwic29ja2V0X3Bhbm5pbmdcIiwgZGF0YSk7XHJcbiAgICAgICAgcHVic3ViLmVtaXQoJ3NvY2tldF9wYW5uaW5nJywgZGF0YSk7XHJcbiAgICB9KTtcclxuICAgIFxyXG4gICAgc29ja2V0Lm9uKCd6b29taW5nJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAvLyQucHVibGlzaChcInNvY2tldF96b29taW5nXCIsIGRhdGEpO1xyXG4gICAgICAgIHB1YnN1Yi5lbWl0KCdzb2NrZXRfem9vbWluZycsIGRhdGEpO1xyXG4gICAgfSk7XHJcbiAgICBcclxuICAgIHNvY2tldC5vbignc2hhcGVEZWxldGVkJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAvLyQucHVibGlzaChcInNoYXBlX2RlbGV0ZWRcIiwgZGF0YSk7XHJcbiAgICAgICAgcHVic3ViLmVtaXQoJ3NoYXBlX2RlbGV0ZWQnLCBkYXRhKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHNvY2tldC5vbignc2hhcGVVcGRhdGVkJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAvLyQucHVibGlzaChcInNoYXBlX3VwZGF0ZWRcIiwgZGF0YSk7XHJcbiAgICAgICAgcHVic3ViLmVtaXQoJ3NoYXBlX3VwZGF0ZWQnLCBkYXRhKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHNvY2tldC5vbignc2hhcGVDcmVhdGVkJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAvLyQucHVibGlzaChcIm5ld19zaGFwZV9jcmVhdGVkXCIsIGRhdGEpO1xyXG4gICAgICAgIHB1YnN1Yi5lbWl0KCduZXdfc2hhcGVfY3JlYXRlZCcsIGRhdGEpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLypcclxuICAgICAqIEN1cnJlbnRseSBkcmF3biBzaGFwZSBpcyBzZW50IHRvIGNvbm5lY3RlZCBjbGllbnRzXHJcbiAgICAgKi9cclxuICAgIHNvY2tldC5vbigndXBkYXRlQ3VycmVudERyYXdpbmdNb2RlbCcsIGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgLy8kLnB1Ymxpc2goXCJ1cGRhdGVfY3VycmVudF9kcmF3aW5nX21vZGVsXCIsIGRhdGEpO1xyXG4gICAgICAgIHB1YnN1Yi5lbWl0KCd1cGRhdGVfY3VycmVudF9kcmF3aW5nX21vZGVsJywgZGF0YSk7XHJcbiAgICB9KTtcclxuICAgIFxyXG4gICAgLypcclxuICAgICAqIE1vdXNlIHdoZWVsIHpvb21pbmcgZXZlbnRzIGhhbmRsZXJcclxuICAgICAqIE5vdCBzdXJlIGlmIHRoaXMgZXZlbnQgc2hvdWxkIGJlIGhlcmUgb3Igc29tZXdoZXJlIGVsc2UgLi4uXHJcbiAgICAgKi9cclxuICAgIGNhbnZhcy5vbignbW91c2V3aGVlbCBET01Nb3VzZVNjcm9sbCcsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIGNvbnN0IGxvY2FsRXZlbnQgPSB3aW5kb3cuZXZlbnQgfHwgZXZlbnQub3JpZ2luYWxFdmVudDsgLy8gb2xkIElFIHN1cHBvcnRcclxuXHJcbiAgICAgICAgY29uc3QgZGlyZWN0aW9uID0gKGxvY2FsRXZlbnQuZGV0YWlsIDwgMCB8fCBsb2NhbEV2ZW50LndoZWVsRGVsdGEgPiAwKSA/IDEgOiAtMTtcclxuICAgICAgICBjb25zdCB6b29tTGV2ZWwgPSBwYW5ab29tTW9kZWwuZ2V0U2NhbGUoKTtcclxuXHJcbiAgICAgICAgY29uc3QgdHJhbnNmb3JtTWF0cml4T2JqZWN0ID0gRXZlbnRGYWN0b3J5LmdldENoYW5nZVRyYW5zZm9ybU1hdHJpeE9iamVjdCgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRyYW5zZm9ybU1hdHJpeE9iamVjdC5wYW5EaXNwbGFjZW1lbnQgPSBwYW5ab29tTW9kZWwuZ2V0UGFuRGlzcGxhY2VtZW50KCk7XHJcbiAgICAgICAgdHJhbnNmb3JtTWF0cml4T2JqZWN0Lnpvb21MID0gZGlyZWN0aW9uIDwgMCA/IDAuOCA6IDEuMjU7XHJcblxyXG4gICAgICAgIHRyYW5zZm9ybU1hdHJpeE9iamVjdC56b29tTGV2ZWwgPSB6b29tTGV2ZWwgKiB0cmFuc2Zvcm1NYXRyaXhPYmplY3Quem9vbUw7XHJcblxyXG4gICAgICAgIGNvbnN0IG1Db29yZHMgPSBtb3VzZUhhbmRsZXIuY29udmVydFRvQ2FudmFzQ29vcmRzKGxvY2FsRXZlbnQucGFnZVgsIGxvY2FsRXZlbnQucGFnZVkpO1xyXG4gICAgICAgIGNvbnN0IHBhbkRpcyA9IHBhblpvb21Nb2RlbC5nZXRQYW5EaXNwbGFjZW1lbnQoKTtcclxuXHJcbiAgICAgICAgY29uc3Qgb3JpZ2luID0ge307XHJcbiAgICAgICAgb3JpZ2luLnggPSAobUNvb3Jkcy54IC0gcGFuRGlzLngpIC8gcGFuWm9vbU1vZGVsLmdldFNjYWxlKCk7XHJcbiAgICAgICAgb3JpZ2luLnkgPSAobUNvb3Jkcy55IC0gcGFuRGlzLnkpIC8gcGFuWm9vbU1vZGVsLmdldFNjYWxlKCk7XHJcblxyXG4gICAgICAgIHRyYW5zZm9ybU1hdHJpeE9iamVjdC5vcmlnaW4gPSBvcmlnaW47XHJcbiAgICAgICAgXHJcblx0XHQvL3B1Ymxpc2ggZXZlbnQgZm9yIGxvY2FsIGNsaWVudCBjb25zdW1wdGlvblxyXG5cdFx0Ly8kLnB1Ymxpc2goXCJjbGllbnRfem9vbWluZ1wiLCB0cmFuc2Zvcm1NYXRyaXhPYmplY3QpO1xyXG4gICAgICAgIHB1YnN1Yi5lbWl0KCdjbGllbnRfem9vbWluZycsIHRyYW5zZm9ybU1hdHJpeE9iamVjdCk7XHJcblx0XHRcclxuICAgICAgICAvL3NlbmQgZXZlbnQgdG8gdGhlIHNlcnZlclxyXG4gICAgICAgIC8vaWYgKCQubm93KCkgLSBsYXN0RW1pdCA+IDMwKSB7XHJcbiAgICAgICAgc29ja2V0LmVtaXQoJ3NvY2tldF96b29tJywgdHJhbnNmb3JtTWF0cml4T2JqZWN0KTtcclxuICAgICAgICAgICAgLy9sYXN0RW1pdCA9ICQubm93KCk7XHJcbiAgICAgICAgLy99XHJcbiAgICB9KTtcclxuXHJcbiAgICAvKlxyXG4gICAgICogVGhpcyBvYmplY3QgZXRzIGNyZWF0ZWQgd2hlbmVldnIgdXNlciBjbGlja3MgbW91c2UgZG93biBvciB0b3VjaGVzIHRoZSBzY3JlZW5cclxuICAgICAqL1xyXG4gICAgZnVuY3Rpb24gZ2V0TW91c2VEb3duT3JUb3VjaE9iamVjdChjb29yZHMpe1xyXG4gICAgICAgIGNvbnN0IG1vdXNlRG93bkV2ZW50T2JqZWN0ID0gRXZlbnRGYWN0b3J5LmdldE1vdXNlRG93bkV2ZW50T2JqZWN0KCk7XHJcbiAgICAgICAgY29uc3QgbUNvb3JkcyA9IG1vdXNlSGFuZGxlci5jb252ZXJ0VG9DYW52YXNDb29yZHMoY29vcmRzLnBhZ2VYLCBjb29yZHMucGFnZVkpO1xyXG4gICAgICAgIGNvbnN0IHBhbkRpcyA9IHBhblpvb21Nb2RlbC5nZXRQYW5EaXNwbGFjZW1lbnQoKTtcclxuXHJcbiAgICAgICAgY29uc3Qgb3JpZ2luID0ge307XHJcbiAgICAgICAgb3JpZ2luLnggPSAobUNvb3Jkcy54IC0gcGFuRGlzLngpIC8gcGFuWm9vbU1vZGVsLmdldFNjYWxlKCk7XHJcbiAgICAgICAgb3JpZ2luLnkgPSAobUNvb3Jkcy55IC0gcGFuRGlzLnkpIC8gcGFuWm9vbU1vZGVsLmdldFNjYWxlKCk7XHJcblxyXG4gICAgICAgIG1vdXNlRG93bkV2ZW50T2JqZWN0Lm1vdXNlQ29vcmRzT3ZlckNhbnZhc0FkanVzdGVkID0gb3JpZ2luO1xyXG4gICAgICAgIC8vXHJcbiAgICAgICAgaWYgKGRyYXdpbmdTdGF0ZU1vZGVsLmdldERyYXdpbmdNb2RlKCkgIT09IGZhbHNlICYmIGRyYXdpbmdTdGF0ZU1vZGVsLmdldERyYXdpbmdNb2RlKCkgIT09IDApIHtcclxuICAgICAgICAgICAgbW91c2VEb3duRXZlbnRPYmplY3QuaXNEcmF3aW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgbW91c2VEb3duRXZlbnRPYmplY3QuY3VycmVudERyYXdpbmdNb2RlID0gZHJhd2luZ1N0YXRlTW9kZWwuZ2V0RHJhd2luZ01vZGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG1vdXNlRG93bkV2ZW50T2JqZWN0O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogVXNlciB0b3VjaGVkIHNjcmVlbiBvbiBhIG1vYmlsZSBkZXZpY2VcclxuICAgICAqL1xyXG4gICAgY2FudmFzLm9uKCd0b3VjaHN0YXJ0JywgZnVuY3Rpb24gKGV2dCkge1xyXG4gICAgICAgIGNvbnN0IGV2ZW50ID0gZXZ0Lm9yaWdpbmFsRXZlbnQ7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCBtb3VzZURvd25FdmVudE9iamVjdCA9IGdldE1vdXNlRG93bk9yVG91Y2hPYmplY3QoZXZlbnQudGFyZ2V0VG91Y2hlc1swXSk7XHJcbiAgICAgICAgcHVic3ViLmVtaXQoJ21vdXNlX2xlZnRfYnV0dG9uX2Rvd24nLCBtb3VzZURvd25FdmVudE9iamVjdCk7XHJcbiAgICAgXHJcbiAgICB9KTtcclxuXHJcbiAgICAvKlxyXG4gICAgICogQW5vdGhlciBtb3VzZSBldmVudC5cclxuICAgICAqIFBlcmhhcHMgaXQgc2hvdWxkIGhhdmUgaXRzIG93biBcInJlcG9zaXRvcnlcIlxyXG4gICAgICovXHJcbiAgICBjYW52YXMub24oJ21vdXNlZG93bicsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHN3aXRjaCAoZS53aGljaCkge1xyXG4gICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtb3VzZURvd25FdmVudE9iamVjdCA9IGdldE1vdXNlRG93bk9yVG91Y2hPYmplY3QoZSk7XHJcbiAgICAgICAgICAgICAgICBwdWJzdWIuZW1pdCgnbW91c2VfbGVmdF9idXR0b25fZG93bicsIG1vdXNlRG93bkV2ZW50T2JqZWN0KTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIDI6XHJcbiAgICAgICAgICAgICAgICAvL01pZGRsZSBNb3VzZSBidXR0b24gcHJlc3NlZFxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgMzpcclxuICAgICAgICAgICAgICAgIC8vUmlnaHQgTW91c2UgYnV0dG9uIHByZXNzZWRcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgLy9Zb3UgaGF2ZSBhIHN0cmFuZ2UgTW91c2UhXHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgLypcclxuICAgICAqXHJcbiAgICAgKi9cclxuICAgIGZ1bmN0aW9uIHJlYWN0VG9Nb3VzZU1vdmVPclRvdWNoTW92ZUV2ZW50T2JqZWN0KGNvb3Jkcyl7XHJcblxyXG4gICAgICAgIGNvbnN0IG1vdXNlRXZlbnRPYmplY3QgPSBFdmVudEZhY3RvcnkuZ2V0TW91c2VNb3ZlRXZlbnRPYmplY3QoKTtcclxuICAgICAgICBtb3VzZUV2ZW50T2JqZWN0Lm1vdXNlQ29vcmRzT3ZlckNhbnZhcyA9IG1vdXNlSGFuZGxlci5jb252ZXJ0VG9DYW52YXNDb29yZHMoY29vcmRzLnBhZ2VYLCBjb29yZHMucGFnZVkpO1xyXG5cclxuICAgICAgICBjb25zdCBwYW5EaXNwbGFjZW1lbnQgPSBwYW5ab29tTW9kZWwuZ2V0UGFuRGlzcGxhY2VtZW50KCk7XHJcbiAgICAgICAgY29uc3QgYWRqdXN0ZWRNb3VzZUNvb3JkcyA9IHt9O1xyXG5cclxuICAgICAgICBhZGp1c3RlZE1vdXNlQ29vcmRzLnggPSAobW91c2VFdmVudE9iamVjdC5tb3VzZUNvb3Jkc092ZXJDYW52YXMueCAtIHBhbkRpc3BsYWNlbWVudC54KSAvIHBhblpvb21Nb2RlbC5nZXRTY2FsZSgpO1xyXG4gICAgICAgIGFkanVzdGVkTW91c2VDb29yZHMueSA9IChtb3VzZUV2ZW50T2JqZWN0Lm1vdXNlQ29vcmRzT3ZlckNhbnZhcy55IC0gcGFuRGlzcGxhY2VtZW50LnkpIC8gcGFuWm9vbU1vZGVsLmdldFNjYWxlKCk7XHJcblxyXG4gICAgICAgIG1vdXNlRXZlbnRPYmplY3QubW91c2VDb29yZHNPdmVyQ2FudmFzQWRqdXN0ZWQgPSBhZGp1c3RlZE1vdXNlQ29vcmRzO1xyXG4gICAgICAgIG1vdXNlRXZlbnRPYmplY3QucGFuRGlzcGxhY2VtZW50ID0gcGFuRGlzcGxhY2VtZW50O1xyXG4gICAgICAgIG1vdXNlRXZlbnRPYmplY3Quem9vbUxldmVsID0gcGFuWm9vbU1vZGVsLmdldFNjYWxlKCk7XHJcbiAgICAgICAgLy9cclxuICAgICAgICBpZiAoZHJhd2luZ1N0YXRlTW9kZWwuaXNEcmF3aW5nKCkgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgbW91c2VFdmVudE9iamVjdC5pc0RyYXdpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICBwdWJzdWIuZW1pdCgnbW91c2VfbW92ZScsIG1vdXNlRXZlbnRPYmplY3QpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZHJhd2luZ1N0YXRlTW9kZWwuaXNQYW5uaW5nKCkgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgbW91c2VFdmVudE9iamVjdC5vcmlnaW4gPSBwYW5ab29tTW9kZWwuZ2V0TW91c2VEb3duT3JpZ2luKCk7XHJcbiAgICAgICAgICAgIHB1YnN1Yi5lbWl0KCdjbGllbnRfcGFubmluZycsIG1vdXNlRXZlbnRPYmplY3QpO1xyXG4gICAgICAgICAgICBzb2NrZXQuZW1pdCgnc29ja2V0X3BhbicsIG1vdXNlRXZlbnRPYmplY3QpO1xyXG4gICAgICAgIH0gZWxzZSB7ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIG1vdXNlRXZlbnRPYmplY3QuaXNEcmF3aW5nID0gZmFsc2U7IC8vaXQgaXMgZGVmYXVsdCB2YWx1ZVxyXG4gICAgICAgICAgICBtb3VzZUV2ZW50T2JqZWN0LmlzUGFubmluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICBwdWJzdWIuZW1pdCgnbW91c2VfbW92ZScsIG1vdXNlRXZlbnRPYmplY3QpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8qXHJcbiAgICAgKiBVc2VyIG1vdmVzIGZpbmdlciBvdmVyIHRoZSBjYW52YXNcclxuICAgICAqL1xyXG4gICAgY2FudmFzLm9uKCd0b3VjaG1vdmUnLCBmdW5jdGlvbiAoZXZ0KSB7XHJcbiAgICAgICAgY29uc3QgZXZlbnQgPSBldnQub3JpZ2luYWxFdmVudDtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHJlYWN0VG9Nb3VzZU1vdmVPclRvdWNoTW92ZUV2ZW50T2JqZWN0KGV2ZW50LnRhcmdldFRvdWNoZXNbMF0pO1xyXG4gICAgfSk7XHJcbiAgICAvKlxyXG4gICAgICogWWV0IGFub3RoZXIgbW91c2UgZXZlbnQgaGFuZGxlciwgdGhlIG1vcmUgY29tbWVudHMgSSB3cml0ZSBhYm91dCB0aGVzZVxyXG4gICAgICogdGhlIG1vcmUgSSB0aGluayB0aGV5IHNob3VsZCBiZSByZWZhY3RvcmVkIGludG8gaXRzIG93biBjbGFzc1xyXG4gICAgICovXHJcbiAgICBjYW52YXMub24oJ21vdXNlbW92ZScsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgcmVhY3RUb01vdXNlTW92ZU9yVG91Y2hNb3ZlRXZlbnRPYmplY3QoZSk7XHJcbiAgICB9KTtcclxuICAgIFxyXG4gICAgLypcclxuICAgICAqIEFub3RoZXIgbW91c2UgZXZlbnQgaGFuZGxlciAuLi5cclxuICAgICAqL1xyXG4gICAgY2FudmFzLmJpbmQoJ21vdXNlbGVhdmUnLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIC8vJC5wdWJsaXNoKFwibW91c2VfbGVmdF9jYW52YXNfYm91bmRhcmllc1wiLCB7fSk7XHJcbiAgICAgICAgcHVic3ViLmVtaXQoJ21vdXNlX2xlZnRfY2FudmFzX2JvdW5kYXJpZXMnKTtcclxuICAgIH0pO1xyXG4gICAgLypcclxuICAgICAqIFVzZXIgbGlmdGVkIHRoZSBmaW5nZXIgb2ZmIHRoZSBjYW52YXNcclxuICAgICAqL1xyXG4gICAgY2FudmFzLm9uKCd0b3VjaGVuZCcsIGZ1bmN0aW9uIChldnQpIHtcclxuICAgICAgICBjb25zdCBldmVudCA9IGV2dC5vcmlnaW5hbEV2ZW50O1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgLy9wdWJzdWIuZW1pdCgnbW91c2VfbGVmdF9idXR0b25fdXAnKTtcclxuXHRcdFxyXG5cdFx0Ly9jb25zdCBtb3VzZURvd25FdmVudE9iamVjdCA9IGdldE1vdXNlRG93bk9yVG91Y2hPYmplY3QoZXZlbnQudGFyZ2V0VG91Y2hlc1swXSk7XHJcbiAgICAgICAgcHVic3ViLmVtaXQoJ21vdXNlX2xlZnRfYnV0dG9uX2Rvd24nLCBudWxsKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8qXHJcbiAgICAgKiBGYWlybHkgb2J2aW91cyBtb3VzZSBldmVudCBoYWRsZXJcclxuICAgICAqL1xyXG4gICAgY2FudmFzLm9uKCdtb3VzZXVwJywgZnVuY3Rpb24gKGUpIHtcclxuXHJcbiAgICAgICAgc3dpdGNoIChlLndoaWNoKSB7IC8vd2hpY2ggbW91c2UgYnV0dG9uXHJcbiAgICAgICAgICAgIGNhc2UgMTpcclxuICAgICAgICAgICAgICAgIC8vYnJvYWRjYXN0IHRoYXQgbGVmdCBtb3VzZSB3YXMgcmVsZWFzZWQgIFxyXG4gICAgICAgICAgICAgICAgcHVic3ViLmVtaXQoJ21vdXNlX2xlZnRfYnV0dG9uX3VwJyk7ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAyOlxyXG4gICAgICAgICAgICAgICAgLy9NaWRkbGUgTW91c2UgYnV0dG9uIHJlbGVhc2VkLlxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgMzpcclxuICAgICAgICAgICAgICAgIC8vUmlnaHQgTW91c2UgYnV0dG9uIHJlbGVhc2VkLlxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAvL1lvdSBoYXZlIGEgc3RyYW5nZSBNb3VzZSFcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBSZW1vdmUgaW5hY3RpdmUgY2xpZW50cyBhZnRlciAxMCBzZWNvbmRzIG9mIGluYWN0aXZpdHlcclxuICAgIC8qXHJcbiAgICBzZXRJbnRlcnZhbChmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgICAgIGZvciAoaWRlbnQgaW4gY2xpZW50cykge1xyXG4gICAgICAgICAgICBpZiAoJC5ub3coKSAtIGNsaWVudHNbaWRlbnRdLnVwZGF0ZWQgPiAxMDAwMCkge1xyXG4gICAgICAgICAgICAgICAgLy8gTGFzdCB1cGRhdGUgd2FzIG1vcmUgdGhhbiAxMCBzZWNvbmRzIGFnby4gXHJcbiAgICAgICAgICAgICAgICAvLyBUaGlzIHVzZXIgaGFzIHByb2JhYmx5IGNsb3NlZCB0aGUgcGFnZVxyXG4gICAgICAgICAgICAgICAgY3Vyc29yc1tpZGVudF0ucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgICAgICBkZWxldGUgY2xpZW50c1tpZGVudF07XHJcbiAgICAgICAgICAgICAgICBkZWxldGUgY3Vyc29yc1tpZGVudF07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfSwgMTAwMDApO1xyXG4gICAgKi9cclxuXHJcbn0pOyIsInZhciBTaGFwZSA9IHJlcXVpcmUoJy4vU2hhcGUnKTtcclxuXHJcbnZhciBjYWxjdWxhdGVEaXN0YW5jZSA9IHJlcXVpcmUoJy4uL3V0aWxzL3V0aWxzJykuY2FsY3VsYXRlRGlzdGFuY2U7XHJcbi8vdmFyIHV0aWxzID0gcmVxdWlyZSgnLi91dGlscycpO1xyXG4vL3ZhciBpc0NvbGxpbmVhciA9IHJlcXVpcmUoJy4vdXRpbHMnKS5pc0NvbGxpbmVhcjtcclxuXHJcbi8vdmFyIGlzUG9pbnRPbkxpbmUgPSByZXF1aXJlKCcuL3V0aWxzJykuaXNQb2ludE9uTGluZTtcclxuLyogXHJcbiAqIFRvIGNoYW5nZSB0aGlzIGxpY2Vuc2UgaGVhZGVyLCBjaG9vc2UgTGljZW5zZSBIZWFkZXJzIGluIFByb2plY3QgUHJvcGVydGllcy5cclxuICogVG8gY2hhbmdlIHRoaXMgdGVtcGxhdGUgZmlsZSwgY2hvb3NlIFRvb2xzIHwgVGVtcGxhdGVzXHJcbiAqIGFuZCBvcGVuIHRoZSB0ZW1wbGF0ZSBpbiB0aGUgZWRpdG9yLlxyXG4gKi9cclxuZnVuY3Rpb24gQ2lyY2xlKGNlbnRlclgsIGNlbnRlclksIHRvWCwgdG9ZKSB7XHJcblxyXG4gICAgU2hhcGUuY2FsbCh0aGlzKTsgLy9jYWxsIHN1cGVyIGNvbnN0cnVjdG9yXHJcblxyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKGNlbnRlclgsIGNlbnRlclksIHJhZGl1cykge1xyXG5cclxuICAgICAgICB0aGlzLmNlbnRlclggPSBjZW50ZXJYO1xyXG4gICAgICAgIHRoaXMuY2VudGVyWSA9IGNlbnRlclk7XHJcblxyXG4gICAgICAgIHRoaXMucmFkaXVzID0gcmFkaXVzO1xyXG5cclxuICAgICAgICBpZih0eXBlb2YgUGF0aDJEID09PSAnZnVuY3Rpb24nKXtcclxuICAgICAgICAgICAgdGhpcy5wYXRoID0gbmV3IFBhdGgyRCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGF0aDJEIGlzIG5vdCBkZWZpbmVkXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5wYXRoLmFyYyh0aGlzLmNlbnRlclgsIHRoaXMuY2VudGVyWSwgdGhpcy5yYWRpdXMsIDAsIDIgKiBNYXRoLlBJKTtcclxuICAgIH07XHJcbiAgICB0aGlzLmluaXQoY2VudGVyWCwgY2VudGVyWSwgdG9YLCB0b1kpO1xyXG5cclxuICAgIHRoaXMuZHJhdyA9IGZ1bmN0aW9uIChjb250ZXh0KSB7XHJcbiAgICAgICAgY29udGV4dC5iZWdpblBhdGgoKTtcclxuICAgICAgICBjb250ZXh0LmFyYyh0aGlzLmNlbnRlclgsIHRoaXMuY2VudGVyWSwgdGhpcy5yYWRpdXMsIDAsIDIgKiBNYXRoLlBJKTtcclxuXHJcbiAgICAgICAgY29udGV4dC5zdHJva2VTdHlsZSA9ICcjMDAwMDAwJztcclxuICAgICAgICBpZih0aGlzLmdldFN0cm9rZVN0eWxlKCkgIT09IG51bGwgKXtcclxuICAgICAgICAgICBjb250ZXh0LnN0cm9rZVN0eWxlID0gdGhpcy5nZXRTdHJva2VTdHlsZSgpOyBcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgY29udGV4dC5zdHJva2UoKTtcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogQmVmb3JlIGFueSBzaGFwZSBnZXRzIGNyZWF0ZWQgdGhpcyBmdW5jdGlvbiBpcyBjYWxsZWQgdG8gZGV0ZXJtaW5lIGlmIHRoZSBjdXJyZW50IHNoYXBlIGlzIHZhbGlkXHJcbiAgICAgKiBpLmUgaWYgbGVuZ3RoID4gMCBvciByYWRpdXMgPiAwXHJcbiAgICAgKi9cclxuICAgIHRoaXMuaXNWYWxpZCA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgaWYgKHRoaXMucmFkaXVzID4gMCl7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICB0aGlzLmlzUG9pbnRPdmVyID0gZnVuY3Rpb24gKGNvbnRleHQsIHgsIHkpIHtcclxuICAgICAgICAvLyBjaXJjbGUgZm9ybXVsYVxyXG4gICAgICAgIC8vICh4IC0gdGhpcy5jZW50ZXJYKV4yICsgKHkgLSB0aGlzLmNlbnRlclkpXjIgPSB0aGlzLnJhZGl1c14yOyAvLyBpZiB3ZSBhZGQgbWFyZ2luICstIG1hcmdpbjtcclxuICAgICAgICBjb25zdCBmaXJzdFBhcmFtID0gTWF0aC5wb3coICh4IC0gdGhpcy5jZW50ZXJYKSwgMik7XHJcbiAgICAgICAgY29uc3Qgc2Vjb25kUGFyYW0gPSBNYXRoLnBvdyggKHkgLSB0aGlzLmNlbnRlclkpLCAyKTtcclxuICAgICAgICBjb25zdCBsaHMgPSBmaXJzdFBhcmFtICsgc2Vjb25kUGFyYW07IC8vIGxlZnQgaGFuZCBzaWRlXHJcbiAgICAgICAgY29uc3QgcmhzID0gdGhpcy5yYWRpdXMgKiB0aGlzLnJhZGl1czsgLy8gcmlnaHQgaGFuZCBzaWRlXHJcbiAgICAgICAgLy9jb25zb2xlLmluZm8oIGxocyArIFwiIC0tIFwiICsgcmhzKVxyXG4gICAgICAgIGNvbnN0IGRlbHRhID0gTWF0aC5hYnMobGhzIC0gcmhzKTtcclxuICAgICAgICBjb25zdCBtYXJnaW4gPSA0MDA7XHJcbiAgICAgICAgaWYgKGRlbHRhIDwgbWFyZ2luKXtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAvL3JldHVybiBjb250ZXh0LmlzUG9pbnRJblN0cm9rZSh0aGlzLnBhdGgsIHgsIHkpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmRyYXdTZWxlY3RlZCA9IGZ1bmN0aW9uIChjb250ZXh0KSB7XHJcblxyXG4gICAgICAgIGRyYWdIYW5kbGVzID0gW107XHJcbiAgICAgICAgLy9pbWcub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGNvbnRleHQuZHJhd0ltYWdlKGltZ0RyYWdIYW5kbGUsIHRoaXMuY2VudGVyWCAtIDUsIHRoaXMuY2VudGVyWSAtIDUpO1xyXG4gICAgICAgIHZhciBoYW5kbGUgPSB7fTtcclxuICAgICAgICBoYW5kbGUub3JpZ2luID0ge307XHJcbiAgICAgICAgaGFuZGxlLm9yaWdpbi54ID0gdGhpcy5vcmlnaW5YO1xyXG4gICAgICAgIGhhbmRsZS5vcmlnaW4ueSA9IHRoaXMub3JpZ2luWTtcclxuICAgICAgICBkcmFnSGFuZGxlcy5wdXNoKGhhbmRsZSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy9kcmF3IGhhbmRsZXMgb24gY2lyY3VtZmVyZW5jZVxyXG4gICAgICAgIGZvcih2YXIgaT0wOyBpIDwgNDsgaT1pKzAuNSl7XHJcbiAgICAgICAgICAgIHZhciByZXN1bHQgPSB7fTtcclxuICAgICAgICAgICAgdmFyIGFuZ2xlID0gaSAqIE1hdGguUEk7XHJcbiAgICAgICAgICAgIHJlc3VsdC5ZID0gTWF0aC5yb3VuZCggdGhpcy5jZW50ZXJZICsgdGhpcy5yYWRpdXMgKiBNYXRoLnNpbiggYW5nbGUgKSApO1xyXG4gICAgICAgICAgICByZXN1bHQuWCA9IE1hdGgucm91bmQoIHRoaXMuY2VudGVyWCArIHRoaXMucmFkaXVzICogTWF0aC5jb3MoIGFuZ2xlICkgKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnRleHQuZHJhd0ltYWdlKGltZ0RyYWdIYW5kbGUsIHJlc3VsdC5YIC0gNSwgcmVzdWx0LlkgLSA1KTtcclxuICAgICAgICAgICAgaGFuZGxlID0ge307XHJcbiAgICAgICAgICAgIGhhbmRsZS5vcmlnaW4gPSB7fTtcclxuICAgICAgICAgICAgaGFuZGxlLm9yaWdpbi54ID0gcmVzdWx0Llg7XHJcbiAgICAgICAgICAgIGhhbmRsZS5vcmlnaW4ueSA9IHJlc3VsdC5ZO1xyXG4gICAgICAgICAgICBkcmFnSGFuZGxlcy5wdXNoKGhhbmRsZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbnRleHQuYmVnaW5QYXRoKCk7XHJcbiAgICAgICAgY29udGV4dC5hcmModGhpcy5jZW50ZXJYLCB0aGlzLmNlbnRlclksIHRoaXMucmFkaXVzLCAwLCAyICogTWF0aC5QSSk7XHJcblxyXG4gICAgICAgIGNvbnRleHQuc3Ryb2tlU3R5bGUgPSAnI2ZmMDAwMCc7XHJcbiAgICAgICAgY29udGV4dC5zdHJva2UoKTtcclxuICAgICAgICBjb250ZXh0LnN0cm9rZVN0eWxlID0gJyMwMDAwMDAnO1xyXG4gICAgfTtcclxuICAgIFxyXG4gICAgdGhpcy50b0pTT04gPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIC8vY2FsbGluZyBwYXJlbnQgY2xhc3MgdG8gZ2V0IGNvbW1vbiBwcm9wZXJ0aWVzIGxpa2Ugc3Ryb2tlU3R5bGUsIFVVSURcclxuICAgICAgICBjb25zdCBvYmogPSBDaXJjbGUucHJvdG90eXBlLnRvSlNPTi5jYWxsKHRoaXMpO1xyXG4gICAgICAgIG9iai50eXBlID0gXCJDaXJjbGVcIjtcclxuIFxyXG4gICAgICAgIG9iai5jZW50ZXJYID0gdGhpcy5jZW50ZXJYO1xyXG4gICAgICAgIG9iai5jZW50ZXJZID0gdGhpcy5jZW50ZXJZO1xyXG5cclxuICAgICAgICBvYmoucmFkaXVzID0gdGhpcy5yYWRpdXM7XHJcblxyXG4gICAgICAgIG9iai5zdHJva2VTdHlsZSA9IHRoaXMuZ2V0U3Ryb2tlU3R5bGUoKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG9iajtcclxuICAgICAgICBcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMudG9TdHJpbmcgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciBudW1iZXJPZkRlY2ltYWxQb2ludHMgPSAxMDA7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdmFyIHJldHZhbCA9IFwiPGRpdiBjbGFzcz0ncGFuZWwtaGVhZGluZyc+Q2lyY2xlPC9kaXY+XCIgK1xyXG4gICAgICAgICAgICAgICAgXCI8ZGl2IGNsYXNzPSdwYW5lbC1ib2R5Jz5cIitcclxuICAgICAgICAgICAgICAgIFwiY2VudGVyWDogXCIgKyBNYXRoLnJvdW5kKHRoaXMuY2VudGVyWCAqIG51bWJlck9mRGVjaW1hbFBvaW50cykgLyBudW1iZXJPZkRlY2ltYWxQb2ludHMgK1xyXG4gICAgICAgICAgICAgICAgXCI8YnIgLz5jZW50ZXJZOiBcIiArIE1hdGgucm91bmQodGhpcy5jZW50ZXJZICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICBcIjxiciAvPnJhZGl1czogXCIgKyBNYXRoLnJvdW5kKHRoaXMucmFkaXVzICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiByZXR2YWw7XHJcbiAgICB9O1xyXG4gICAgLypcclxuICAgICAqIFRoaXMgZ2V0IHMgZGlzcGxheWVkIGluIFByb3BlcnR5IHBvcHVwXHJcbiAgICAgKi9cclxuICAgIHRoaXMudG9IVE1MU3RyaW5nID0gZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgbnVtYmVyT2ZEZWNpbWFsUG9pbnRzID0gMTAwO1xyXG4gICAgICAgIC8vXCJYOiBcIiArIE1hdGgucm91bmQodGhpcy5vcmlnaW5YICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgdmFyIHJldHZhbCA9IFwiPGRpdiBjbGFzcz0ncGFuZWwtYm9keSc+XCIrXHJcbiAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsIHBhbmVsLWluZm8nPlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsLWhlYWRpbmcnPk9yaWdpbjwvZGl2PlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsLWJvZHknPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8ZGl2IGNsYXNzPSdpbnB1dC1ncm91cCBpbnB1dC1ncm91cC1zbSc+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI8c3BhbiBjbGFzcz0naW5wdXQtZ3JvdXAtYWRkb24nIGlkPSdzaXppbmctYWRkb24zJz5YPC9zcGFuPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiPGlucHV0IG5hbWU9J2NlbnRlclgnIHR5cGU9J3RleHQnIGNsYXNzPSdmb3JtLWNvbnRyb2wnIHBsYWNlaG9sZGVyPSdvcmlnaW4geCcgYXJpYS1kZXNjcmliZWRieT0nc2l6aW5nLWFkZG9uMycgdmFsdWU9J1wiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE1hdGgucm91bmQodGhpcy5jZW50ZXJYICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArIFwiJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiPC9kaXY+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J2lucHV0LWdyb3VwIGlucHV0LWdyb3VwLXNtJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIjxzcGFuIGNsYXNzPSdpbnB1dC1ncm91cC1hZGRvbicgaWQ9J3NpemluZy1hZGRvbjMnPlk8L3NwYW4+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI8aW5wdXQgbmFtZT0nY2VudGVyWScgdHlwZT0ndGV4dCcgY2xhc3M9J2Zvcm0tY29udHJvbCcgcGxhY2Vob2xkZXI9J29yaWdpbiB4JyBhcmlhLWRlc2NyaWJlZGJ5PSdzaXppbmctYWRkb24zJyB2YWx1ZT0nXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTWF0aC5yb3VuZCh0aGlzLmNlbnRlclkgKiBudW1iZXJPZkRlY2ltYWxQb2ludHMpIC8gbnVtYmVyT2ZEZWNpbWFsUG9pbnRzICsgXCInPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8L2Rpdj5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8L2Rpdj5cIiArXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiICtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgXCI8ZGl2IGNsYXNzPSdpbnB1dC1ncm91cCBpbnB1dC1ncm91cC1zbSc+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPHNwYW4gY2xhc3M9J2lucHV0LWdyb3VwLWFkZG9uJyBpZD0nc2l6aW5nLWFkZG9uMyc+UmFkaXVzPC9zcGFuPlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxpbnB1dCBuYW1lPSdyYWRpdXMnIHR5cGU9J3RleHQnIGNsYXNzPSdmb3JtLWNvbnRyb2wnIHBsYWNlaG9sZGVyPSdyYWRpdXMnIGFyaWEtZGVzY3JpYmVkYnk9J3NpemluZy1hZGRvbjMnIHZhbHVlPSdcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgTWF0aC5yb3VuZCh0aGlzLnJhZGl1cyAqIG51bWJlck9mRGVjaW1hbFBvaW50cykgLyBudW1iZXJPZkRlY2ltYWxQb2ludHMgKyBcIic+XCIgK1xyXG4gICAgICAgICAgICAgICAgXCI8L2Rpdj5cIiArXHJcblxyXG4gICAgICAgICAgICAgICAgXCI8ZGl2IGNsYXNzPSdpbnB1dC1ncm91cCBpbnB1dC1ncm91cC1zbSc+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPHNwYW4gY2xhc3M9J2lucHV0LWdyb3VwLWFkZG9uJyBpZD0nc2l6aW5nLWFkZG9uMyc+U3Ryb2tlIFN0eWxlPC9zcGFuPlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxpbnB1dCBuYW1lPSdzdHJva2VTdHlsZScgdHlwZT0ndGV4dCcgY2xhc3M9J2Zvcm0tY29udHJvbCcgcGxhY2Vob2xkZXI9J29yaWdpbiB4JyBhcmlhLWRlc2NyaWJlZGJ5PSdzaXppbmctYWRkb24zJyB2YWx1ZT0nXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0U3Ryb2tlU3R5bGUoKSArIFwiJz5cIiArXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiXHJcblxyXG4gICAgICAgICAgICAgICAgXCI8L2Rpdj5cIjtcclxuICAgICAgICBcclxuICAgICAgICByZXR1cm4gcmV0dmFsO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnVwZGF0ZSA9IGZ1bmN0aW9uIChuZXdFbmRwb2ludFgsIG5ld0VuZHBvaW50WSl7XHJcbiAgICAgICAgdGhpcy5yYWRpdXMgPSBjYWxjdWxhdGVEaXN0YW5jZSh0aGlzLmNlbnRlclgsIHRoaXMuY2VudGVyWSwgbmV3RW5kcG9pbnRYLCBuZXdFbmRwb2ludFkpO1xyXG4gICAgfTtcclxufVxyXG5DaXJjbGUucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShTaGFwZS5wcm90b3R5cGUsIHtcclxuICAgIGNvbnN0cnVjdG9yOiB7XHJcbiAgICAgICAgdmFsdWU6IENpcmNsZVxyXG4gICAgfVxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQ2lyY2xlOyIsInZhciBTaGFwZSA9IHJlcXVpcmUoJy4vU2hhcGUnKTtcclxuLyogXHJcbiAqIFRvIGNoYW5nZSB0aGlzIGxpY2Vuc2UgaGVhZGVyLCBjaG9vc2UgTGljZW5zZSBIZWFkZXJzIGluIFByb2plY3QgUHJvcGVydGllcy5cclxuICogVG8gY2hhbmdlIHRoaXMgdGVtcGxhdGUgZmlsZSwgY2hvb3NlIFRvb2xzIHwgVGVtcGxhdGVzXHJcbiAqIGFuZCBvcGVuIHRoZSB0ZW1wbGF0ZSBpbiB0aGUgZWRpdG9yLlxyXG4gKi9cclxuZnVuY3Rpb24gRnJlZUZvcm0ocG9pbnRzKSB7XHJcblxyXG4gICAgU2hhcGUuY2FsbCh0aGlzKTsgLy9jYWxsIHN1cGVyIGNvbnN0cnVjdG9yXHJcblxyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKHBvaW50cykge1xyXG5cclxuICAgICAgICB0aGlzLnBvaW50cyA9IHBvaW50cztcclxuICAgICAgICBpZih0eXBlb2YgUGF0aDJEID09PSAnZnVuY3Rpb24nKXtcclxuICAgICAgICAgICAgdGhpcy5wYXRoID0gbmV3IFBhdGgyRCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGF0aDJEIGlzIG5vdCBkZWZpbmVkXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMucG9pbnRzLmxlbmd0aCAtIDE7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLnBhdGgubW92ZVRvKHRoaXMucG9pbnRzW2ldLngsIHRoaXMucG9pbnRzW2ldLnkpO1xyXG4gICAgICAgICAgICB0aGlzLnBhdGgubGluZVRvKHRoaXMucG9pbnRzW2kgKyAxXS54LCB0aGlzLnBvaW50c1tpICsgMV0ueSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuICAgIHRoaXMuaW5pdChwb2ludHMpO1xyXG5cclxuICAgIHRoaXMuZHJhdyA9IGZ1bmN0aW9uIChjb250ZXh0KSB7XHJcblxyXG4gICAgICAgIGNvbnRleHQuYmVnaW5QYXRoKCk7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLnBvaW50cy5sZW5ndGggLSAxOyBpKyspIHtcclxuICAgICAgICAgICAgY29udGV4dC5tb3ZlVG8odGhpcy5wb2ludHNbaV0ueCwgdGhpcy5wb2ludHNbaV0ueSk7XHJcbiAgICAgICAgICAgIGNvbnRleHQubGluZVRvKHRoaXMucG9pbnRzW2kgKyAxXS54LCB0aGlzLnBvaW50c1tpICsgMV0ueSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnRleHQuc3Ryb2tlU3R5bGUgPSAnIzAwMDAwMCc7XHJcbiAgICAgICAgaWYodGhpcy5nZXRTdHJva2VTdHlsZSgpICE9PSBudWxsICl7XHJcbiAgICAgICAgICAgY29udGV4dC5zdHJva2VTdHlsZSA9IHRoaXMuZ2V0U3Ryb2tlU3R5bGUoKTsgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnRleHQuc3Ryb2tlKCk7XHJcblxyXG4gICAgfTtcclxuICAgIC8qXHJcbiAgICAgKiBCZWZvcmUgZnJlZWZvcm0gZ2V0cyBjcmVhdGVkIG1ha2Ugc3VyZSB0aGF0XHJcbiAgICAgKiAtIGVpdGhlciB0aGVyZSBpcyBtb3JlIHRoYW4gMiBwb2ludHNcclxuICAgICAqIC0gb3IgaWYgdGhlcmUgYXJlIDIgcG9pbnRzIG9ubHkgdGhlbiB0aGV5IGFyZSBub3QgdGhlIHNhbWVcclxuICAgICAqL1xyXG4gICAgdGhpcy5pc1ZhbGlkID0gZnVuY3Rpb24oKXtcclxuICAgICAgICBpZiAodGhpcy5wb2ludHMubGVuZ3RoID4gMil7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSBpZih0aGlzLnBvaW50cy5sZW5ndGggPT09IDIpe1xyXG4gICAgICAgICAgICBpZiAoTWF0aC5hYnModGhpcy5wb2ludHNbMF0ueCAtIHRoaXMucG9pbnRzWzFdLngpID4gMCB8fCBNYXRoLmFicyh0aGlzLnBvaW50c1swXS55IC0gdGhpcy5wb2ludHNbMV0ueSkgPiAwKXtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHRoaXMuaXNQb2ludE92ZXIgPSBmdW5jdGlvbiAoY29udGV4dCwgeCwgeSkge1xyXG4gICAgICAgIHJldHVybiBjb250ZXh0LmlzUG9pbnRJblN0cm9rZSh0aGlzLnBhdGgsIHgsIHkpO1xyXG4gICAgICAgIC8vcmV0dXJuIGNvbnRleHQuaXNQb2ludEluUGF0aCh0aGlzLnBhdGgsIHgsIHkpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmRyYXdTZWxlY3RlZCA9IGZ1bmN0aW9uIChjb250ZXh0KSB7XHJcblxyXG4gICAgICAgIGRyYWdIYW5kbGVzID0gW107XHJcblxyXG4gICAgICAgIGNvbnRleHQuZHJhd0ltYWdlKGltZ0RyYWdIYW5kbGUsIHRoaXMucG9pbnRzWzBdLnggLSA1LCB0aGlzLnBvaW50c1swXS55IC0gNSk7XHJcbiAgICAgICAgdmFyIGhhbmRsZSA9IHt9O1xyXG4gICAgICAgIGhhbmRsZS5vcmlnaW4gPSB7fTtcclxuICAgICAgICBoYW5kbGUub3JpZ2luLnggPSB0aGlzLm9yaWdpblg7XHJcbiAgICAgICAgaGFuZGxlLm9yaWdpbi55ID0gdGhpcy5vcmlnaW5ZO1xyXG4gICAgICAgIGRyYWdIYW5kbGVzLnB1c2goaGFuZGxlKTtcclxuXHJcbiAgICAgICAgY29udGV4dC5kcmF3SW1hZ2UoaW1nRHJhZ0hhbmRsZSwgdGhpcy5wb2ludHNbdGhpcy5wb2ludHMubGVuZ3RoLTFdLnggLSA1LCB0aGlzLnBvaW50c1t0aGlzLnBvaW50cy5sZW5ndGgtMV0ueSAtIDUpO1xyXG4gICAgICAgIGhhbmRsZSA9IHt9O1xyXG4gICAgICAgIGhhbmRsZS5vcmlnaW4gPSB7fTtcclxuICAgICAgICBoYW5kbGUub3JpZ2luLnggPSB0aGlzLmVuZFg7XHJcbiAgICAgICAgaGFuZGxlLm9yaWdpbi55ID0gdGhpcy5lbmRZO1xyXG4gICAgICAgIGRyYWdIYW5kbGVzLnB1c2goaGFuZGxlKTtcclxuXHJcbiAgICAgICAgY29udGV4dC5iZWdpblBhdGgoKTtcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMucG9pbnRzLmxlbmd0aCAtIDE7IGkrKykge1xyXG4gICAgICAgICAgICBjb250ZXh0Lm1vdmVUbyh0aGlzLnBvaW50c1tpXS54LCB0aGlzLnBvaW50c1tpXS55KTtcclxuICAgICAgICAgICAgY29udGV4dC5saW5lVG8odGhpcy5wb2ludHNbaSArIDFdLngsIHRoaXMucG9pbnRzW2kgKyAxXS55KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29udGV4dC5zdHJva2VTdHlsZSA9ICcjZmYwMDAwJztcclxuICAgICAgICBjb250ZXh0LnN0cm9rZSgpO1xyXG4gICAgICAgIGNvbnRleHQuc3Ryb2tlU3R5bGUgPSAnIzAwMDAwMCc7XHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLnRvSlNPTiA9IGZ1bmN0aW9uKCl7XHJcblxyXG4gICAgICAgIC8vY2FsbGluZyBwYXJlbnQgY2xhc3MgdG8gZ2V0IGNvbW1vbiBwcm9wZXJ0aWVzIGxpa2Ugc3Ryb2tlU3R5bGUsIFVVSURcclxuICAgICAgICBjb25zdCBvYmogPSBGcmVlRm9ybS5wcm90b3R5cGUudG9KU09OLmNhbGwodGhpcyk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgb2JqLnR5cGUgPSBcIkZyZWVGb3JtXCI7XHJcbiBcclxuICAgICAgICBvYmoucG9pbnRzID0gdGhpcy5wb2ludHM7XHJcblxyXG4gICAgICAgIG9iai5zdHJva2VTdHlsZSA9IHRoaXMuZ2V0U3Ryb2tlU3R5bGUoKTtcclxuICAgICAgICBcclxuICAgICAgICByZXR1cm4gb2JqOyAgICAgICAgXHJcbiAgICB9O1xyXG4gICAgdGhpcy50b1N0cmluZyA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyIG51bWJlck9mRGVjaW1hbFBvaW50cyA9IDEwMDtcclxuXHJcbiAgICAgICAgdmFyIHJldHZhbCA9IFwiPGRpdiBjbGFzcz0ncGFuZWwtaGVhZGluZyc+RnJlZWZvcm08L2Rpdj5cIiArXHJcbiAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsLWJvZHknPlwiK1xyXG4gICAgICAgICAgICAgICAgXCIjcG9pbnRzOiBcIiArIHRoaXMucG9pbnRzLmxlbmd0aCAgK1xyXG4gICAgICAgICAgICAgICAgXCI8YnIgLz5wc1swXVg6IFwiICsgTWF0aC5yb3VuZCh0aGlzLnBvaW50c1swXS54ICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICAgXCI8YnIgLz5wc1swXVk6IFwiICsgTWF0aC5yb3VuZCh0aGlzLnBvaW50c1swXS55ICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICBcIjxiciAvPnBzW2xhc3RdWDogXCIgKyBNYXRoLnJvdW5kKHRoaXMucG9pbnRzW3RoaXMucG9pbnRzLmxlbmd0aC0xXS54ICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICBcIjxiciAvPnBzW2xhc3RdWTogXCIgKyBNYXRoLnJvdW5kKHRoaXMucG9pbnRzW3RoaXMucG9pbnRzLmxlbmd0aC0xXS55ICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiByZXR2YWw7XHJcbiAgICAgICAgXHJcbiAgICB9O1xyXG5cclxuICAgIC8qXHJcbiAgICAgKiBUaGlzIGdldCBzIGRpc3BsYXllZCBpbiBQcm9wZXJ0eSBwb3B1cFxyXG4gICAgICovXHJcbiAgICB0aGlzLnRvSFRNTFN0cmluZyA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyIG51bWJlck9mRGVjaW1hbFBvaW50cyA9IDEwMDtcclxuICAgICAgICAvL1wiWDogXCIgKyBNYXRoLnJvdW5kKHRoaXMub3JpZ2luWCAqIG51bWJlck9mRGVjaW1hbFBvaW50cykgLyBudW1iZXJPZkRlY2ltYWxQb2ludHMgK1xyXG4gICAgICAgIHZhciByZXR2YWwgPSBcIjxpbnB1dCBuYW1lPSdwb2ludHMnIHR5cGU9J2hpZGRlbicgdmFsdWU9J1wiICsgSlNPTi5zdHJpbmdpZnkodGhpcy5wb2ludHMpICsgXCInIC8+XCIgK1xyXG5cclxuXHJcbiAgICAgICAgICAgICAgICBcIjxoMz48c3BhbiBjbGFzcz0nbGFiZWwgbGFiZWwtZGVmYXVsdCc+TmVlZCBzb21lIG1lY2hhbmlzbSB0byBhbGxvdyBlZGl0aW5nIG9mIFwiICsgdGhpcy5wb2ludHMubGVuZ3RoICsgXCIgcG9pbnRzIGluIEZyZWVmb3JtPC9zcGFuPjwvaDM+IDxiciAvPlwiICtcclxuIFxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J2lucHV0LWdyb3VwIGlucHV0LWdyb3VwLXNtJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8c3BhbiBjbGFzcz0naW5wdXQtZ3JvdXAtYWRkb24nIGlkPSdzaXppbmctYWRkb24zJz5TdHJva2UgU3R5bGU8L3NwYW4+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPGlucHV0IG5hbWU9J3N0cm9rZVN0eWxlJyB0eXBlPSd0ZXh0JyBjbGFzcz0nZm9ybS1jb250cm9sJyBwbGFjZWhvbGRlcj0nb3JpZ2luIHgnIGFyaWEtZGVzY3JpYmVkYnk9J3NpemluZy1hZGRvbjMnIHZhbHVlPSdcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRTdHJva2VTdHlsZSgpICsgXCInPlwiICtcclxuICAgICAgICAgICAgICAgIFwiPC9kaXY+XCJcclxuXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiByZXR2YWw7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMudXBkYXRlID0gZnVuY3Rpb24gKG5ld0VuZHBvaW50WCwgbmV3RW5kcG9pbnRZKXtcclxuICAgICAgICB2YXIgbmV3UG9pbnQgPSB7fTtcclxuICAgICAgICBuZXdQb2ludC54ID0gbmV3RW5kcG9pbnRYO1xyXG4gICAgICAgIG5ld1BvaW50LnkgPSBuZXdFbmRwb2ludFk7XHJcbiAgICAgICAgdGhpcy5wb2ludHMucHVzaChuZXdQb2ludCk7XHJcbiAgICB9O1xyXG59XHJcbi8vdGhlIHNlY29uZCBwYXJhbWV0ZXIgdG8gbWFrZSBpbnN0YW5jZW9mIHdvcmtcclxuRnJlZUZvcm0ucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShTaGFwZS5wcm90b3R5cGUsIHtcclxuICAgIGNvbnN0cnVjdG9yOiB7XHJcbiAgICAgICAgdmFsdWU6IEZyZWVGb3JtXHJcbiAgICB9XHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBGcmVlRm9ybTsiLCIndXNlIHN0cmljdCc7XHJcbnZhciBTaGFwZSA9IHJlcXVpcmUoJy4vU2hhcGUnKTtcclxuXHJcbi8vdmFyIHV0aWxzID0gcmVxdWlyZSgnLi91dGlscycpO1xyXG52YXIgaXNDb2xsaW5lYXIgPSByZXF1aXJlKCcuLi91dGlscy91dGlscycpLmlzQ29sbGluZWFyO1xyXG4vL3ZhciBjYWxjdWxhdGVEaXN0YW5jZSA9IHJlcXVpcmUoJy4vdXRpbHMnKS5jYWxjdWxhdGVEaXN0YW5jZTtcclxuLy92YXIgaXNQb2ludE9uTGluZSA9IHJlcXVpcmUoJy4vdXRpbHMnKS5pc1BvaW50T25MaW5lO1xyXG5cclxuLyogXHJcbiAqIFRvIGNoYW5nZSB0aGlzIGxpY2Vuc2UgaGVhZGVyLCBjaG9vc2UgTGljZW5zZSBIZWFkZXJzIGluIFByb2plY3QgUHJvcGVydGllcy5cclxuICogVG8gY2hhbmdlIHRoaXMgdGVtcGxhdGUgZmlsZSwgY2hvb3NlIFRvb2xzIHwgVGVtcGxhdGVzXHJcbiAqIGFuZCBvcGVuIHRoZSB0ZW1wbGF0ZSBpbiB0aGUgZWRpdG9yLlxyXG4gKi9cclxuY2xhc3MgTGluZSBleHRlbmRzIFNoYXBlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihzdGFydFgsIHN0YXJ0WSwgZW5kWCwgZW5kWSl7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5vcmlnaW5YID0gc3RhcnRYO1xyXG4gICAgICAgIHRoaXMub3JpZ2luWSA9IHN0YXJ0WTtcclxuICAgICAgICBpZih0eXBlb2YgUGF0aDJEID09PSAnZnVuY3Rpb24nKXtcclxuICAgICAgICAgICAgdGhpcy5wYXRoID0gbmV3IFBhdGgyRCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGF0aDJEIGlzIG5vdCBkZWZpbmVkXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICB0aGlzLmVuZFggPSBlbmRYO1xyXG4gICAgICAgIHRoaXMuZW5kWSA9IGVuZFk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5wYXRoLm1vdmVUbyh0aGlzLm9yaWdpblgsIHRoaXMub3JpZ2luWSk7XHJcbiAgICAgICAgdGhpcy5wYXRoLmxpbmVUbyh0aGlzLmVuZFgsIHRoaXMuZW5kWSk7XHJcblxyXG4gICAgfVxyXG4gICAgLypcclxuICAgICAqIEJlZm9yZSBhbnkgc2hhcGUgZ2V0cyBjcmVhdGVkIHRoaXMgZnVuY3Rpb24gaXMgY2FsbGVkIHRvIGRldGVybWluZSBpZiB0aGUgY3VycmVudCBzaGFwZSBpcyB2YWxpZFxyXG4gICAgICogaS5lIGlmIGxlbmd0aCA+IDAgb3IgcmFkaXVzID4gMFxyXG4gICAgICovXHJcbiAgICBpc1ZhbGlkICgpe1xyXG4gICAgICAgIGlmICggTWF0aC5hYnModGhpcy5vcmlnaW5YIC0gdGhpcy5lbmRYKSA+IDAgfHwgTWF0aC5hYnModGhpcy5vcmlnaW5ZIC0gdGhpcy5lbmRZKSA+IDApe1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgLypcclxuICAgICAqIGhvcml6b250YWwgYW5kIHZlcnRpY2FsIGxpbmVzIGxvb2sgZmFkZWQgb24gY2FudmFzXHJcbiAgICAgKiBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzEwMzczNjk1L2RyYXdpbmctbGluZXMtaW4tY2FudmFzLWJ1dC10aGUtbGFzdC1vbmVzLWFyZS1mYWRlZFxyXG4gICAgICovXHJcbiAgICBkcmF3IChjb250ZXh0LCBsaW5lV2lkdGgpIHtcclxuICAgICAgICBcclxuICAgICAgICBjb250ZXh0LnN0cm9rZVN0eWxlID0gJyMwMDAwMDAnO1xyXG5cclxuICAgICAgICBpZih0aGlzLmdldFN0cm9rZVN0eWxlKCkgIT09IG51bGwgKXtcclxuICAgICAgICAgICBjb250ZXh0LnN0cm9rZVN0eWxlID0gdGhpcy5nZXRTdHJva2VTdHlsZSgpOyBcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCB5RW5kMSA9IHRoaXMub3JpZ2luWTtcclxuICAgICAgICBsZXQgeUVuZDIgPSB0aGlzLmVuZFk7XHJcbiAgICAgICAgaWYoeUVuZDEgPT09IHlFbmQyKXsgLy9ob3Jpem9udGFsIGxpbmVcclxuICAgICAgICAgICAgeUVuZDEgPSBNYXRoLmZsb29yKHlFbmQxKSswLjU7XHJcbiAgICAgICAgICAgIHlFbmQyID0geUVuZDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCB4RW5kMSA9IHRoaXMub3JpZ2luWDtcclxuICAgICAgICBsZXQgeEVuZDIgPSB0aGlzLmVuZFg7XHJcblxyXG4gICAgICAgIGlmKHhFbmQxID09PSB4RW5kMil7IC8vaG9yaXpvbnRhbCBsaW5lXHJcbiAgICAgICAgICAgIHhFbmQxID0gTWF0aC5mbG9vcih4RW5kMSkrMC41O1xyXG4gICAgICAgICAgICB4RW5kMiA9IHhFbmQxO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29udGV4dC5iZWdpblBhdGgoKTtcclxuXHJcbiAgICAgICAgaWYobGluZVdpZHRoKXtcclxuICAgICAgICAgICBjb250ZXh0LmxpbmVXaWR0aCA9IGxpbmVXaWR0aDtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29udGV4dC5tb3ZlVG8oeEVuZDEsIHlFbmQxKTtcclxuICAgICAgICBjb250ZXh0LmxpbmVUbyh4RW5kMiwgeUVuZDIpO1xyXG5cclxuICAgICAgICBpZih0aGlzLmxhYmVsICE9PSBudWxsICYmIHR5cGVvZiB0aGlzLmxhYmVsICE9PSB1bmRlZmluZWQpe1xyXG4gICAgICAgICAgICBpZih0aGlzLmdldExhYmVsT3JpZW50YXRpb24oKSA9PT0gXCJ2ZXJ0aWNhbFwiKXtcclxuICAgICAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAgICBjb250ZXh0LnRyYW5zZm9ybS5zYXZlKCk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmluZm8oeEVuZDIgKyBcIiA6OiBcIiAreUVuZDIpO1xyXG4gICAgICAgICAgICAgICAgY29udGV4dC50cmFuc2Zvcm0udHJhbnNsYXRlKHhFbmQyLCB5RW5kMik7XHJcbiAgICAgICAgICAgICAgICBjb250ZXh0LnRyYW5zZm9ybS5yb3RhdGUoLU1hdGguUEkvMik7XHJcbiAgICAgICAgICAgICAgICBjb250ZXh0LnN0cm9rZVRleHQodGhpcy5sYWJlbCwgMCwgMyk7XHJcbiAgICAgICAgICAgICAgICBjb250ZXh0LnRyYW5zZm9ybS5yZXN0b3JlKCk7XHJcbiAgICAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAgICAgY29udGV4dC50cmFuc2Zvcm0uc2F2ZSgpO1xyXG4gICAgICAgICAgICAgICAgY29udGV4dC5mb250ID0gXCIxMHB4IEFyaWFsLCBzYW5zLXNlcmlmXCI7XHJcbiAgICAgICAgICAgICAgICBjb250ZXh0LnRyYW5zZm9ybS56b29tT25Qb2ludCgxLCAxL3RoaXMuZ2V0TW9kZWxTY2FsZSgpLCB4RW5kMiAsIHlFbmQyICk7XHJcbiAgICAgICAgICAgICAgICBjb250ZXh0LnN0cm9rZVN0eWxlID0gJ2JsYWNrJztcclxuICAgICAgICAgICAgICAgIGNvbnRleHQubGluZVdpZHRoID0gMTtcclxuICAgICAgICAgICAgICAgIGNvbnRleHQuZmlsbFRleHQodGhpcy5sYWJlbCwgeEVuZDIgLSA5LCB5RW5kMiArIDgpOyBcclxuICAgICAgICAgICAgICAgIGNvbnRleHQudHJhbnNmb3JtLnJlc3RvcmUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnRleHQudHJhbnNmb3JtLnNhdmUoKTtcclxuICAgICAgICAgICAgICAgIGNvbnRleHQuZm9udCA9IFwiMTBweCBBcmlhbCwgc2Fucy1zZXJpZlwiO1xyXG4gICAgICAgICAgICAgICAgY29udGV4dC50cmFuc2Zvcm0uem9vbU9uUG9pbnQoMS90aGlzLmdldE1vZGVsU2NhbGUoKSwgMSwgeEVuZDIsIHlFbmQyKTtcclxuICAgICAgICAgICAgICAgIGNvbnRleHQuc3Ryb2tlU3R5bGUgPSAnYmxhY2snO1xyXG4gICAgICAgICAgICAgICAgY29udGV4dC5saW5lV2lkdGggPSAxO1xyXG4gICAgICAgICAgICAgICAgY29udGV4dC5maWxsVGV4dCh0aGlzLmxhYmVsLCB4RW5kMiwgeUVuZDIgKyA0KTtcclxuICAgICAgICAgICAgICAgIGNvbnRleHQudHJhbnNmb3JtLnJlc3RvcmUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBjb250ZXh0LnN0cm9rZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGlzUG9pbnRPdmVyKGNvbnRleHQsIHgsIHkpIHtcclxuICAgICAgICAvKnJldHVybiBjb250ZXh0LmlzUG9pbnRJblN0cm9rZSh0aGlzLnBhdGgsIHgsIHkpOyovXHJcbiAgICAgICAgXHJcbiAgICAgICAgdmFyIGNyb3NzcHJvZHVjdCA9IGlzQ29sbGluZWFyKHgsIHksIHRoaXMub3JpZ2luWCwgdGhpcy5vcmlnaW5ZLCB0aGlzLmVuZFgsIHRoaXMuZW5kWSk7XHJcblxyXG4gICAgICAgIC8vIGNyb3NzcHJvZHVjdCA9PT0gMCBpbiB0aGUgcGVyZmV4dCB2ZWN0b3Igd29ybGQsIGhlcmUgd2UgbmVlZCB0aHJlc2hvbGRcclxuICAgICAgICBjb25zdCBtYXJnaW4gPSA0MDA7XHJcbiAgICAgICAgaWYgXHJcbiAgICAgICAgKFxyXG4gICAgICAgICAgICAoY3Jvc3Nwcm9kdWN0IDwgbWFyZ2luICYmIGNyb3NzcHJvZHVjdCA+IC1tYXJnaW4pICYmIFxyXG4gICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAodGhpcy5vcmlnaW5YIDw9IHggJiYgeCA8PSB0aGlzLmVuZFgpIHx8IFxyXG4gICAgICAgICAgICAgICAgKHRoaXMuZW5kWCA8PSB4ICYmIHggPD0gdGhpcy5vcmlnaW5YKVxyXG4gICAgICAgICAgICApICYmIFxyXG4gICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAodGhpcy5vcmlnaW5ZIDw9IHkgJiYgeSA8PSB0aGlzLmVuZFkpIHx8IFxyXG4gICAgICAgICAgICAgICAgKHRoaXMuZW5kWSA8PSB5ICYmIHkgPD0gdGhpcy5vcmlnaW5ZKVxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgKSBcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbiAgICBkcmF3U2VsZWN0ZWQgKGNvbnRleHQpIHtcclxuXHJcbiAgICAgICAgY29udGV4dC5iZWdpblBhdGgoKTtcclxuICAgICAgICBkcmFnSGFuZGxlcyA9IFtdO1xyXG4gICAgICAgIC8vaW1nLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjb250ZXh0LmRyYXdJbWFnZShpbWdEcmFnSGFuZGxlLCB0aGlzLm9yaWdpblggLSA1LCB0aGlzLm9yaWdpblkgLSA1KTtcclxuICAgICAgICB2YXIgaGFuZGxlID0ge307XHJcbiAgICAgICAgaGFuZGxlLm9yaWdpbiA9IHt9O1xyXG4gICAgICAgIGhhbmRsZS5vcmlnaW4ueCA9IHRoaXMub3JpZ2luWDtcclxuICAgICAgICBoYW5kbGUub3JpZ2luLnkgPSB0aGlzLm9yaWdpblk7XHJcbiAgICAgICAgZHJhZ0hhbmRsZXMucHVzaChoYW5kbGUpO1xyXG5cclxuICAgICAgICBjb250ZXh0LmRyYXdJbWFnZShpbWdEcmFnSGFuZGxlLCB0aGlzLmVuZFggLSA1LCB0aGlzLmVuZFkgLSA1KTtcclxuICAgICAgICBoYW5kbGUgPSB7fTtcclxuICAgICAgICBoYW5kbGUub3JpZ2luID0ge307XHJcbiAgICAgICAgaGFuZGxlLm9yaWdpbi54ID0gdGhpcy5lbmRYO1xyXG4gICAgICAgIGhhbmRsZS5vcmlnaW4ueSA9IHRoaXMuZW5kWTtcclxuICAgICAgICBkcmFnSGFuZGxlcy5wdXNoKGhhbmRsZSk7XHJcbiAgICAgICAgLy99O1xyXG4gICAgICAgIC8vc2VsZWN0ZWRTaGFwZUluZGV4ID0gaTtcclxuXHJcbiAgICAgICAgLy9zaGFwZXNQb2NbaV0udHJpZ2dlcihcIm1vdXNlSXNPdmVyXCIpO1xyXG4gICAgICAgIGNvbnRleHQubW92ZVRvKHRoaXMub3JpZ2luWCwgdGhpcy5vcmlnaW5ZKTtcclxuICAgICAgICBjb250ZXh0LmxpbmVUbyh0aGlzLmVuZFgsIHRoaXMuZW5kWSk7XHJcbiAgICAgICAgY29udGV4dC5zdHJva2VTdHlsZSA9ICcjZmYwMDAwJztcclxuICAgICAgICBjb250ZXh0LnN0cm9rZSgpO1xyXG4gICAgICAgIGNvbnRleHQuc3Ryb2tlU3R5bGUgPSAnIzAwMDAwMCc7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHRvSlNPTiAoKXtcclxuICAgICAgICAvL2NhbGxpbmcgcGFyZW50IGNsYXNzIHRvIGdldCBjb21tb24gcHJvcGVydGllcyBsaWtlIHN0cm9rZVN0eWxlLCBVVUlEXHJcbiAgICAgICAgLy9jb25zdCBvYmogPSBMaW5lLnByb3RvdHlwZS50b0pTT04uY2FsbCh0aGlzKTtcclxuICAgICAgICBjb25zdCBvYmogPSBzdXBlci50b0pTT04oKTtcclxuICAgICAgICBcclxuICAgICAgICBvYmoudHlwZSA9IFwiTGluZVwiO1xyXG4gXHJcbiAgICAgICAgb2JqLm9yaWdpblggPSB0aGlzLm9yaWdpblg7XHJcbiAgICAgICAgb2JqLm9yaWdpblkgPSB0aGlzLm9yaWdpblk7XHJcblxyXG4gICAgICAgIG9iai5lbmRYID0gdGhpcy5lbmRYO1xyXG4gICAgICAgIG9iai5lbmRZID0gdGhpcy5lbmRZO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiBvYmo7ICAgICAgICBcclxuICAgIH1cclxuICAgIFxyXG4gICAgdG9TdHJpbmcgKCl7XHJcbiAgICAgICAgdmFyIG51bWJlck9mRGVjaW1hbFBvaW50cyA9IDEwMDtcclxuXHJcbiAgICAgICAgdmFyIHJldHZhbCA9IFwiPGRpdiBjbGFzcz0ncGFuZWwtaGVhZGluZyc+TGluZTwvZGl2PlwiICtcclxuICAgICAgICAgICAgICAgIFwiPGRpdiBjbGFzcz0ncGFuZWwtYm9keSc+XCIrXHJcbiAgICAgICAgICAgICAgICBcIm9yaWdYOiBcIiArIE1hdGgucm91bmQodGhpcy5vcmlnaW5YICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICBcIjxiciAvPm9yaWdZOiBcIiArIE1hdGgucm91bmQodGhpcy5vcmlnaW5ZICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICBcIjxiciAvPmVuZFg6IFwiICsgTWF0aC5yb3VuZCh0aGlzLmVuZFggKiBudW1iZXJPZkRlY2ltYWxQb2ludHMpIC8gbnVtYmVyT2ZEZWNpbWFsUG9pbnRzICtcclxuICAgICAgICAgICAgICAgIFwiPGJyIC8+ZW5kWTogXCIgKyBNYXRoLnJvdW5kKHRoaXMuZW5kWSAqIG51bWJlck9mRGVjaW1hbFBvaW50cykgLyBudW1iZXJPZkRlY2ltYWxQb2ludHMgK1xyXG4gICAgICAgICAgICAgICAgXCI8L2Rpdj5cIjtcclxuICAgICAgICBcclxuICAgICAgICByZXR1cm4gcmV0dmFsO1xyXG4gICAgfVxyXG4gICAgLypcclxuICAgICAqIFRoaXMgZ2V0IHMgZGlzcGxheWVkIGluIFByb3BlcnR5IHBvcHVwXHJcbiAgICAgKi9cclxuICAgIHRvSFRNTFN0cmluZyAoKXtcclxuICAgICAgICB2YXIgbnVtYmVyT2ZEZWNpbWFsUG9pbnRzID0gMTAwO1xyXG4gICAgICAgIC8vXCJYOiBcIiArIE1hdGgucm91bmQodGhpcy5vcmlnaW5YICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgdmFyIHJldHZhbCA9IFwiPGRpdiBjbGFzcz0ncGFuZWwtYm9keSc+XCIrXHJcbiAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsIHBhbmVsLWluZm8nPlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsLWhlYWRpbmcnPk9yaWdpbjwvZGl2PlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsLWJvZHknPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8ZGl2IGNsYXNzPSdpbnB1dC1ncm91cCBpbnB1dC1ncm91cC1zbSc+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI8c3BhbiBjbGFzcz0naW5wdXQtZ3JvdXAtYWRkb24nIGlkPSdzaXppbmctYWRkb24zJz5YPC9zcGFuPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiPGlucHV0IG5hbWU9J29yaWdpblgnIHR5cGU9J3RleHQnIGNsYXNzPSdmb3JtLWNvbnRyb2wnIHBsYWNlaG9sZGVyPSdvcmlnaW4geCcgYXJpYS1kZXNjcmliZWRieT0nc2l6aW5nLWFkZG9uMycgdmFsdWU9J1wiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE1hdGgucm91bmQodGhpcy5vcmlnaW5YICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArIFwiJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiPC9kaXY+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J2lucHV0LWdyb3VwIGlucHV0LWdyb3VwLXNtJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIjxzcGFuIGNsYXNzPSdpbnB1dC1ncm91cC1hZGRvbicgaWQ9J3NpemluZy1hZGRvbjMnPlk8L3NwYW4+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI8aW5wdXQgbmFtZT0nb3JpZ2luWScgdHlwZT0ndGV4dCcgY2xhc3M9J2Zvcm0tY29udHJvbCcgcGxhY2Vob2xkZXI9J29yaWdpbiB4JyBhcmlhLWRlc2NyaWJlZGJ5PSdzaXppbmctYWRkb24zJyB2YWx1ZT0nXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTWF0aC5yb3VuZCh0aGlzLm9yaWdpblkgKiBudW1iZXJPZkRlY2ltYWxQb2ludHMpIC8gbnVtYmVyT2ZEZWNpbWFsUG9pbnRzICsgXCInPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8L2Rpdj5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8L2Rpdj5cIiArXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiICtcclxuXHJcblxyXG5cclxuICAgICAgICAgICAgICAgIFwiPGRpdiBjbGFzcz0ncGFuZWwgcGFuZWwtaW5mbyc+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPGRpdiBjbGFzcz0ncGFuZWwtaGVhZGluZyc+RW5kPC9kaXY+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPGRpdiBjbGFzcz0ncGFuZWwtYm9keSc+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J2lucHV0LWdyb3VwIGlucHV0LWdyb3VwLXNtJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIjxzcGFuIGNsYXNzPSdpbnB1dC1ncm91cC1hZGRvbicgaWQ9J3NpemluZy1hZGRvbjMnPlg8L3NwYW4+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI8aW5wdXQgbmFtZT0nZW5kWCcgdHlwZT0ndGV4dCcgY2xhc3M9J2Zvcm0tY29udHJvbCcgcGxhY2Vob2xkZXI9J29yaWdpbiB4JyBhcmlhLWRlc2NyaWJlZGJ5PSdzaXppbmctYWRkb24zJyB2YWx1ZT0nXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTWF0aC5yb3VuZCh0aGlzLmVuZFggKiBudW1iZXJPZkRlY2ltYWxQb2ludHMpIC8gbnVtYmVyT2ZEZWNpbWFsUG9pbnRzICsgXCInPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8L2Rpdj5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiPGRpdiBjbGFzcz0naW5wdXQtZ3JvdXAgaW5wdXQtZ3JvdXAtc20nPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiPHNwYW4gY2xhc3M9J2lucHV0LWdyb3VwLWFkZG9uJyBpZD0nc2l6aW5nLWFkZG9uMyc+WTwvc3Bhbj5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIjxpbnB1dCBuYW1lPSdlbmRZJyB0eXBlPSd0ZXh0JyBjbGFzcz0nZm9ybS1jb250cm9sJyBwbGFjZWhvbGRlcj0nb3JpZ2luIHgnIGFyaWEtZGVzY3JpYmVkYnk9J3NpemluZy1hZGRvbjMnIHZhbHVlPSdcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBNYXRoLnJvdW5kKHRoaXMuZW5kWSAqIG51bWJlck9mRGVjaW1hbFBvaW50cykgLyBudW1iZXJPZkRlY2ltYWxQb2ludHMgKyBcIic+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIjwvZGl2PlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjwvZGl2PlwiICtcclxuICAgICAgICAgICAgICAgIFwiPC9kaXY+XCIgK1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J2lucHV0LWdyb3VwIGlucHV0LWdyb3VwLXNtJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8c3BhbiBjbGFzcz0naW5wdXQtZ3JvdXAtYWRkb24nIGlkPSdzaXppbmctYWRkb24zJz5TdHJva2UgU3R5bGU8L3NwYW4+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPGlucHV0IG5hbWU9J3N0cm9rZVN0eWxlJyB0eXBlPSd0ZXh0JyBjbGFzcz0nZm9ybS1jb250cm9sJyBwbGFjZWhvbGRlcj0nb3JpZ2luIHgnIGFyaWEtZGVzY3JpYmVkYnk9J3NpemluZy1hZGRvbjMnIHZhbHVlPSdcIiArXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRTdHJva2VTdHlsZSgpICsgXCInPlwiICtcclxuICAgICAgICAgICAgICAgIFwiPC9kaXY+XCIgK1xyXG4gICAgICAgICAgICBcIjwvZGl2PlwiO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiByZXR2YWw7XHJcbiAgICB9XHJcblxyXG4gICAgLypcclxuICAgICAqIFdoZW4gc2hhcGUgaXMgYmVpbmcgZHJhd24gc29tZSBzb3J0IG9mIHBhcmFtZXRlciBjaGFuZ2VzXHJcbiAgICAgKiBpbiB0aGlzIGNhc2UgZW5kIG9mIHRoZSBsaW5lLiBTbyB0aGUgdG8gYmUgZHJhd24gdGhlIGVuZCBvZiB0aGUgbGluZSBuZWVkcyB0byBiZSB1cGRhdGVkXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZSAobmV3RW5kcG9pbnRYLCBuZXdFbmRwb2ludFkpe1xyXG4gICAgICAgIHRoaXMuZW5kWCA9IG5ld0VuZHBvaW50WDtcclxuICAgICAgICB0aGlzLmVuZFkgPSBuZXdFbmRwb2ludFk7XHJcbiAgICB9XHJcbn1cclxuLypcclxuTGluZS5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKFNoYXBlLnByb3RvdHlwZSwge1xyXG4gICAgY29uc3RydWN0b3I6IHtcclxuICAgICAgICB2YWx1ZTogTGluZVxyXG4gICAgfVxyXG59KTtcclxuKi9cclxuLy9PYmplY3Quc2V0UHJvdG90eXBlT2YoTGluZS5wcm90b3R5cGUsIFNoYXBlKTsvLyBJZiB5b3UgZG8gbm90IGRvIHRoaXMgeW91IHdpbGwgZ2V0IGEgVHlwZUVycm9yIHdoZW4geW91IGludm9rZSBzcGVha1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBMaW5lOyIsIid1c2Ugc3RyaWN0JztcclxudmFyIFNoYXBlID0gcmVxdWlyZSgnLi9TaGFwZScpO1xyXG5cclxuLyogXHJcbiAqIFBvaW50IGNsYXNzLiBcclxuICogUG9pbnQyRCB0aGF0IGlzIGNhdXNlIHRoZXJlIGlzIG5vIG5lZWQgZm9yIDNEIGluIHRoaXMgZnVuIGxpdHRsZSBhcHBcclxuICovXHJcbmZ1bmN0aW9uIFBvaW50KHN0YXJ0WCwgc3RhcnRZKSB7XHJcblxyXG4gICAgU2hhcGUuY2FsbCh0aGlzKTsgLy9jYWxsIHN1cGVyIGNvbnN0cnVjdG9yXHJcblxyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKHN0YXJ0WCwgc3RhcnRZKSB7XHJcbiAgICAgICAgdGhpcy5YID0gc3RhcnRYO1xyXG4gICAgICAgIHRoaXMuWSA9IHN0YXJ0WTtcclxuICAgICAgICBpZih0eXBlb2YgUGF0aDJEID09PSAnZnVuY3Rpb24nKXtcclxuICAgICAgICAgICAgdGhpcy5wYXRoID0gbmV3IFBhdGgyRCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGF0aDJEIGlzIG5vdCBkZWZpbmVkXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5EcmF3blJlY3RhbmdsZVNpemUgPSAzOyAvL2luIHBpeGVsczsgaW5jbHVkaW5nIG9uZSBwaXhlbCBmb3IgdGhlIHBvaW50IGl0c2VsZjtcclxuICAgICAgICB0aGlzLkRyYXduUmVjdGFuZ2xlU2l6ZVdoZW5TZWxlY3RlZCA9IDM7IC8vaW4gcGl4ZWxzOyBpbmNsdWRpbmcgb25lIHBpeGVsIGZvciB0aGUgcG9pbnQgaXRzZWxmO1xyXG4gICAgICAgIC8vIHdoZW4gbW91c2Ugb3ZlciB0aGUgcG9pbnQgaG93IG1hbnkgcGl4ZWxzIHdpbGwgYmUgcGFkZGVkIGFyb3VuZCB0aGUgcG9pbnRcclxuICAgICAgICAvLyB0byBkZXRlcm1pbmUgaWYgcG9pbnQgaXMgc2VsZWN0ZWRcclxuICAgICAgICB0aGlzLlNlbnNpdGl2aXR5UGFkZGluZyA9IDE7XHJcbiAgICAgICAgY29uc3QgYXJlYSA9IHRoaXMuU2Vuc2l0aXZpdHlQYWRkaW5nICogMiArIDE7Ly90b3RhbCBzcXVhcmUgc2lkZSBsZW5ndGggb2YgdGhlIHNlbnNpdGl2ZSBhcmVhIGFyb3VuZCBwaXhlbFxyXG4gICAgICAgIHRoaXMucGF0aC5yZWN0KHRoaXMuWCAtIHRoaXMuU2Vuc2l0aXZpdHlQYWRkaW5nLCB0aGlzLlkgLSB0aGlzLlNlbnNpdGl2aXR5UGFkZGluZywgYXJlYSwgYXJlYSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuaW5pdChzdGFydFgsIHN0YXJ0WSk7XHJcbiAgICBcclxuICAgIC8qXHJcbiAgICAgKiBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzc4MTI1MTQvZHJhd2luZy1hLWRvdC1vbi1odG1sNS1jYW52YXNcclxuICAgICAqL1xyXG4gICAgdGhpcy5kcmF3ID0gZnVuY3Rpb24gKGNvbnRleHQsICBsaW5lV2lkdGgpIHtcclxuICAgICAgICBcclxuICAgICAgICBjb250ZXh0LnN0cm9rZVN0eWxlID0gJyMwMDAwMDAnO1xyXG4gICAgICAgIGlmKHRoaXMuZ2V0U3Ryb2tlU3R5bGUoKSAhPT0gbnVsbCApe1xyXG4gICAgICAgICAgIGNvbnRleHQuc3Ryb2tlU3R5bGUgPSB0aGlzLmdldFN0cm9rZVN0eWxlKCk7IFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29udGV4dC5iZWdpblBhdGgoKTtcclxuXHJcbiAgICAgICAgaWYobGluZVdpZHRoKXtcclxuICAgICAgICAgICBjb250ZXh0LmxpbmVXaWR0aCA9IGxpbmVXaWR0aDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IG9mZnNldCA9IE1hdGguZmxvb3IodGhpcy5EcmF3blJlY3RhbmdsZVNpemUvMik7XHJcbiAgICAgICAgY29udGV4dC5maWxsUmVjdCh0aGlzLlggLSBvZmZzZXQsIHRoaXMuWSAtIG9mZnNldCwgdGhpcy5EcmF3blJlY3RhbmdsZVNpemUsIHRoaXMuRHJhd25SZWN0YW5nbGVTaXplKTtcclxuICAgICAgICBcclxuICAgICAgICBjb250ZXh0LnN0cm9rZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmlzUG9pbnRPdmVyID0gZnVuY3Rpb24gKGNvbnRleHQsIHgsIHkpIHtcclxuICAgICAgICBjb25zdCBkZWx0YVggPSBNYXRoLmFicyh4IC0gdGhpcy5YKTtcclxuICAgICAgICBjb25zdCBkZWx0YVkgPSBNYXRoLmFicyh5IC0gdGhpcy5ZKTtcclxuICAgICAgICBjb25zdCBtYXJnaW4gPSAzO1xyXG4gICAgICAgIC8vY29uc29sZS5pbmZvKGRlbHRhWCArIFwiIC0gXCIgKyBkZWx0YVkpO1xyXG4gICAgICAgIGlmIChkZWx0YVggPCBtYXJnaW4gJiYgZGVsdGFZIDwgbWFyZ2luKXtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgLy9yZXR1cm4gY29udGV4dC5pc1BvaW50SW5QYXRoKHRoaXMucGF0aCwgeCwgeSk7XHJcbiAgICB9O1xyXG4gICAgdGhpcy5kcmF3U2VsZWN0ZWQgPSBmdW5jdGlvbiAoY29udGV4dCkge1xyXG5cclxuICAgICAgICBjb250ZXh0LmJlZ2luUGF0aCgpO1xyXG4gICAgICAgIGRyYWdIYW5kbGVzID0gW107XHJcbiAgICAgICAgLy9pbWcub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGNvbnRleHQuZHJhd0ltYWdlKGltZ0RyYWdIYW5kbGUsIHRoaXMuWCAtIDQuNSwgdGhpcy5ZIC0gNC41KTtcclxuICAgICAgICB2YXIgaGFuZGxlID0ge307XHJcbiAgICAgICAgaGFuZGxlLm9yaWdpbiA9IHt9O1xyXG4gICAgICAgIGhhbmRsZS5vcmlnaW4ueCA9IHRoaXMub3JpZ2luWDtcclxuICAgICAgICBoYW5kbGUub3JpZ2luLnkgPSB0aGlzLm9yaWdpblk7XHJcbiAgICAgICAgZHJhZ0hhbmRsZXMucHVzaChoYW5kbGUpO1xyXG5cclxuXHJcbiAgICAgICAgY29udGV4dC5maWxsU3R5bGUgPSAnI2ZmMDAwMCc7XHJcblxyXG4gICAgICAgIGNvbnN0IG9mZnNldCA9IE1hdGguZmxvb3IodGhpcy5EcmF3blJlY3RhbmdsZVNpemVXaGVuU2VsZWN0ZWQvMik7XHJcbiAgICAgICAgY29udGV4dC5maWxsUmVjdCh0aGlzLlggLSBvZmZzZXQsIHRoaXMuWSAtIG9mZnNldCwgdGhpcy5EcmF3blJlY3RhbmdsZVNpemVXaGVuU2VsZWN0ZWQsIHRoaXMuRHJhd25SZWN0YW5nbGVTaXplV2hlblNlbGVjdGVkKTtcclxuICAgICAgICBcclxuICAgICAgICBjb250ZXh0LmZpbGxTdHlsZSA9ICcjMDAwMDAwJztcclxuXHJcbiAgICB9O1xyXG4gICAgXHJcbiAgICB0aGlzLnRvSlNPTiA9IGZ1bmN0aW9uKCl7XHJcblxyXG4gICAgICAgIC8vY2FsbGluZyBwYXJlbnQgY2xhc3MgdG8gZ2V0IGNvbW1vbiBwcm9wZXJ0aWVzIGxpa2Ugc3Ryb2tlU3R5bGUsIFVVSURcclxuICAgICAgICBjb25zdCBvYmogPSBQb2ludC5wcm90b3R5cGUudG9KU09OLmNhbGwodGhpcyk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgb2JqLnR5cGUgPSBcIlBvaW50XCI7XHJcbiBcclxuICAgICAgICBvYmouWCA9IHRoaXMuWDtcclxuICAgICAgICBvYmouWSA9IHRoaXMuWTtcclxuXHJcbiAgICAgICAgb2JqLnN0cm9rZVN0eWxlID0gdGhpcy5nZXRTdHJva2VTdHlsZSgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiBvYmo7ICAgICAgICBcclxuICAgIH07XHJcbiAgICBcclxuICAgIHRoaXMudG9TdHJpbmcgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciBudW1iZXJPZkRlY2ltYWxQb2ludHMgPSAxMDA7XHJcblxyXG4gICAgICAgIHZhciByZXR2YWwgPSBcIjxkaXYgY2xhc3M9J3BhbmVsLWhlYWRpbmcnPlBvaW50PC9kaXY+XCIgK1xyXG4gICAgICAgICAgICAgICAgXCI8ZGl2IGNsYXNzPSdwYW5lbC1ib2R5Jz5cIitcclxuICAgICAgICAgICAgICAgIFwiWDogXCIgKyBNYXRoLnJvdW5kKHRoaXMuWCAqIG51bWJlck9mRGVjaW1hbFBvaW50cykgLyBudW1iZXJPZkRlY2ltYWxQb2ludHMgK1xyXG4gICAgICAgICAgICAgICAgXCI8YnIgLz5ZOiBcIiArIE1hdGgucm91bmQodGhpcy5ZICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiByZXR2YWw7XHJcbiAgICB9O1xyXG4gICAgLypcclxuICAgICAqIFRoaXMgZ2V0IHMgZGlzcGxheWVkIGluIFByb3BlcnR5IHBvcHVwXHJcbiAgICAgKi9cclxuICAgIHRoaXMudG9IVE1MU3RyaW5nID0gZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgbnVtYmVyT2ZEZWNpbWFsUG9pbnRzID0gMTAwO1xyXG4gICAgICAgIC8vXCJYOiBcIiArIE1hdGgucm91bmQodGhpcy5vcmlnaW5YICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArXHJcbiAgICAgICAgdmFyIHJldHZhbCA9IFwiPGRpdiBjbGFzcz0ncGFuZWwtYm9keSc+XCIrXHJcbiAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsIHBhbmVsLWluZm8nPlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsLWhlYWRpbmcnPk9yaWdpbjwvZGl2PlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J3BhbmVsLWJvZHknPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8ZGl2IGNsYXNzPSdpbnB1dC1ncm91cCBpbnB1dC1ncm91cC1zbSc+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI8c3BhbiBjbGFzcz0naW5wdXQtZ3JvdXAtYWRkb24nIGlkPSdzaXppbmctYWRkb24zJz5YPC9zcGFuPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiPGlucHV0IG5hbWU9J1gnIHR5cGU9J3RleHQnIGNsYXNzPSdmb3JtLWNvbnRyb2wnIHBsYWNlaG9sZGVyPSdvcmlnaW4geCcgYXJpYS1kZXNjcmliZWRieT0nc2l6aW5nLWFkZG9uMycgdmFsdWU9J1wiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE1hdGgucm91bmQodGhpcy5YICogbnVtYmVyT2ZEZWNpbWFsUG9pbnRzKSAvIG51bWJlck9mRGVjaW1hbFBvaW50cyArIFwiJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiPC9kaXY+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIjxkaXYgY2xhc3M9J2lucHV0LWdyb3VwIGlucHV0LWdyb3VwLXNtJz5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIjxzcGFuIGNsYXNzPSdpbnB1dC1ncm91cC1hZGRvbicgaWQ9J3NpemluZy1hZGRvbjMnPlk8L3NwYW4+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCI8aW5wdXQgbmFtZT0nWScgdHlwZT0ndGV4dCcgY2xhc3M9J2Zvcm0tY29udHJvbCcgcGxhY2Vob2xkZXI9J29yaWdpbiB4JyBhcmlhLWRlc2NyaWJlZGJ5PSdzaXppbmctYWRkb24zJyB2YWx1ZT0nXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTWF0aC5yb3VuZCh0aGlzLlkgKiBudW1iZXJPZkRlY2ltYWxQb2ludHMpIC8gbnVtYmVyT2ZEZWNpbWFsUG9pbnRzICsgXCInPlwiICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCI8L2Rpdj5cIiArXHJcbiAgICAgICAgICAgICAgICAgICAgXCI8L2Rpdj5cIiArXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiICtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgXCI8ZGl2IGNsYXNzPSdpbnB1dC1ncm91cCBpbnB1dC1ncm91cC1zbSc+XCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIFwiPHNwYW4gY2xhc3M9J2lucHV0LWdyb3VwLWFkZG9uJyBpZD0nc2l6aW5nLWFkZG9uMyc+U3Ryb2tlIFN0eWxlPC9zcGFuPlwiICtcclxuICAgICAgICAgICAgICAgICAgICBcIjxpbnB1dCBuYW1lPSdzdHJva2VTdHlsZScgdHlwZT0ndGV4dCcgY2xhc3M9J2Zvcm0tY29udHJvbCcgcGxhY2Vob2xkZXI9J29yaWdpbiB4JyBhcmlhLWRlc2NyaWJlZGJ5PSdzaXppbmctYWRkb24zJyB2YWx1ZT0nXCIgK1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0U3Ryb2tlU3R5bGUoKSArIFwiJz5cIiArXHJcbiAgICAgICAgICAgICAgICBcIjwvZGl2PlwiICtcclxuICAgICAgICAgICAgXCI8L2Rpdj5cIjtcclxuICAgICAgICBcclxuICAgICAgICByZXR1cm4gcmV0dmFsO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnVwZGF0ZSA9IGZ1bmN0aW9uIChuZXdFbmRwb2ludFgsIG5ld0VuZHBvaW50WSl7XHJcbiAgICAgICAgdGhpcy5YID0gbmV3RW5kcG9pbnRYO1xyXG4gICAgICAgIHRoaXMuWSA9IG5ld0VuZHBvaW50WTtcclxuICAgIH07XHJcbn1cclxuUG9pbnQucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShTaGFwZS5wcm90b3R5cGUsIHtcclxuICAgIGNvbnN0cnVjdG9yOiB7XHJcbiAgICAgICAgdmFsdWU6IFBvaW50XHJcbiAgICB9XHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBQb2ludDsiLCIndXNlIHN0cmljdCc7XHJcbmNvbnN0IHV1aWRWNCA9IHJlcXVpcmUoJ3V1aWQvdjQnKTtcclxuLyogXHJcbiAqXHJcbiAqL1xyXG5mdW5jdGlvbiBTaGFwZSgpIHtcclxuXHRjb25zdCB0aGF0ID0gdGhpcztcclxuXHJcbiAgICB0aGlzLlVVSUQgPSB1dWlkVjQoKTtcclxuXHQvL2h0dHBzOi8vd3d3Lnczc2Nob29scy5jb20vdGFncy9yZWZfY2FudmFzLmFzcCBsb29rIGF0IGhlcmUgZm9yIHByb3BlcnRpZXNcclxuXHR0aGlzLnN0cm9rZVN0eWxlID0gXCIjMDAwMDAwXCI7XHJcblx0dGhpcy5saW5lV2lkdGggPSBcIjFcIjsgLy9zaGFwZSBsaW5lIHdpZHRoLCBpbiBwaXhlbHNcclxuXHJcbiAgICB0aGlzLmxhYmVsID0gbnVsbDtcclxuICAgIHRoaXMubGFiZWxPcmllbnRhdGlvbiA9IFwiaG9yaXpvbnRhbFwiO1xyXG4gICAgdGhpcy5tb2RlbFNjYWxlID0gMTtcclxuXHJcbn1cclxuXHJcblxyXG5TaGFwZS5wcm90b3R5cGUuc2V0U3Ryb2tlU3R5bGUgPSBmdW5jdGlvbiAoY29sb3IpIHtcclxuICAgIGlmKGNvbG9yKXtcclxuICAgICAgICB0aGlzLnN0cm9rZVN0eWxlID0gY29sb3I7IFxyXG4gICAgfVxyXG59O1xyXG5TaGFwZS5wcm90b3R5cGUuZ2V0U3Ryb2tlU3R5bGUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5zdHJva2VTdHlsZSB8fCBudWxsO1xyXG59O1xyXG4vKlxyXG4gKiBVc2VkIGZvciBjcmVhdGlvbiBvZiBvYmplY3RzIGZyb20gSlNPTiBcclxuICovXHJcblNoYXBlLnByb3RvdHlwZS5zZXRVVUlEID0gZnVuY3Rpb24gKFVVSUQpIHtcclxuICAgIGlmKFVVSUQpe1xyXG4gICAgICAgIHRoaXMuVVVJRCA9IFVVSUQ7IFxyXG4gICAgfVxyXG4gICAgXHJcbn07XHJcblNoYXBlLnByb3RvdHlwZS5nZXRVVUlEID0gZnVuY3Rpb24gKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuVVVJRCB8fCBudWxsO1xyXG59O1xyXG5cclxuU2hhcGUucHJvdG90eXBlLnNldExhYmVsID0gZnVuY3Rpb24gKGxhYmVsKSB7XHJcbiAgICBpZihsYWJlbCAhPT0gbnVsbCAmJiB0eXBlb2YgbGFiZWwgIT09IHVuZGVmaW5lZCl7XHJcbiAgICAgICAgdGhpcy5sYWJlbCA9IGxhYmVsOyBcclxuICAgIH1cclxufTtcclxuU2hhcGUucHJvdG90eXBlLmdldExhYmVsID0gZnVuY3Rpb24gKCkge1xyXG4gICAgcmV0dXJuIHRoaXMubGFiZWwgfHwgbnVsbDtcclxufTtcclxuXHJcblNoYXBlLnByb3RvdHlwZS5zZXRNb2RlbFNjYWxlID0gZnVuY3Rpb24gKG1vZGVsU2NhbGUpIHtcclxuICAgICAgICB0aGlzLm1vZGVsU2NhbGUgPSBtb2RlbFNjYWxlOyBcclxufTtcclxuU2hhcGUucHJvdG90eXBlLmdldE1vZGVsU2NhbGUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5tb2RlbFNjYWxlIHx8IDE7XHJcbn07XHJcblxyXG5TaGFwZS5wcm90b3R5cGUuc2V0TGFiZWxPcmllbnRhdGlvbiA9IGZ1bmN0aW9uIChsYWJlbE9yaWVudGF0aW9uKSB7XHJcbiAgICAgICAgdGhpcy5sYWJlbE9yaWVudGF0aW9uID0gbGFiZWxPcmllbnRhdGlvbjsgXHJcbn07XHJcblNoYXBlLnByb3RvdHlwZS5nZXRMYWJlbE9yaWVudGF0aW9uID0gZnVuY3Rpb24gKCkge1xyXG4gICAgcmV0dXJuIHRoaXMubGFiZWxPcmllbnRhdGlvbiB8fCBudWxsO1xyXG59O1xyXG5cclxuU2hhcGUucHJvdG90eXBlLmRyYXcgPSBmdW5jdGlvbiAoY29udGV4dCkge1xyXG4gICAgY29uc29sZS5sb2coXCJkcmF3IGlzIG5vdCBpbXBsZW1lbnRlZCBoZXJlXCIpO1xyXG59O1xyXG5TaGFwZS5wcm90b3R5cGUuaXNQb2ludE92ZXIgPSBmdW5jdGlvbiAoY3R4LCB4LCB5KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcImlzUG9pbnRPdmVyIGlzIG5vdCBpbXBsZW1lbnRlZCBoZXJlXCIpO1xyXG59O1xyXG5TaGFwZS5wcm90b3R5cGUuZHJhd1NlbGVjdGVkID0gZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiZHJhd1NlbGVjdGVkIGlzIG5vdCBpbXBsZW1lbnRlZCBoZXJlXCIpO1xyXG59O1xyXG5TaGFwZS5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgIGNvbnN0IG9iaj17fTtcclxuICAgIG9iai5zdHJva2VTdHlsZSA9IHRoaXMuZ2V0U3Ryb2tlU3R5bGUoKTtcclxuICAgIG9iai5VVUlEID0gdGhpcy5nZXRVVUlEKCk7XHJcbiAgICBvYmoubGFiZWwgPSB0aGlzLmdldExhYmVsKCk7XHJcbiAgICByZXR1cm4gb2JqO1xyXG59O1xyXG4vKlxyXG4gKiBCZWZvcmUgYW55IHNoYXBlIGdldHMgY3JlYXRlZCB0aGlzIGZ1bmN0aW9uIGlzIGNhbGxlZCB0byBkZXRlcm1pbmUgaWYgdGhlIGN1cnJlbnQgc2hhcGUgaXMgdmFsaWRcclxuICogaS5lIGlmIGxlbmd0aCA+IDAgb3IgcmFkaXVzID4gMFxyXG4gKi9cclxuU2hhcGUucHJvdG90eXBlLmlzVmFsaWQgPSBmdW5jdGlvbigpe1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbn07XHJcbi8qXHJcbiAqIFRoaXMgZ2V0IHMgZGlzcGxheWVkIGluIFByb3BlcnR5IHBvcHVwXHJcbiAqL1xyXG5TaGFwZS5wcm90b3R5cGUudG9IVE1MU3RyaW5nID0gZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgIGNvbnNvbGUubG9nKFwidG9IVE1MU3RyaW5nIGlzIG5vdCBpbXBsZW1lbnRlZCBoZXJlXCIpO1xyXG59O1xyXG5cclxuLypcclxuICogVXBkYXRlcyB0aGUgc2hhcGUgZm9yIGRyYXdpbmcuIE1ha2luZyBhc3N1bXB0aW9ucyB0aGF0IHRoaXMgaXMgYSBuZWVkZWRcclxuICogJ3VwZGF0ZScgbWV0aG9kLiBJIG1lYW4gdGhlIG5hbWluZyBpcyBwcmV0dHkgZ2VuZXJpYy4uLiBcclxuICovXHJcblNoYXBlLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgY29uc29sZS5sb2coXCJ1cGRhdGUgaXMgbm90IGltcGxlbWVudGVkIGhlcmVcIik7XHJcbn07XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFNoYXBlO1xyXG5cclxuIiwiLyogXHJcbiAqIEdlbmVyYXRlIG9iamVjdHMgYmFzZWQgb24gdHlwZSBpbiBKU09OIHN0cmluZ1xyXG4gKi9cclxuXHJcbnZhciBDaXJjbGUgPSByZXF1aXJlKCcuLi9zaGFwZXMvQ2lyY2xlJyk7XHJcbnZhciBMaW5lID0gcmVxdWlyZSgnLi4vc2hhcGVzL0xpbmUnKTtcclxudmFyIEZyZWVGb3JtID0gcmVxdWlyZSgnLi4vc2hhcGVzL0ZyZWVGb3JtJyk7XHJcbnZhciBQb2ludCA9IHJlcXVpcmUoJy4uL3NoYXBlcy9Qb2ludCcpO1xyXG5cclxuY2xhc3MgU2hhcGVGYWN0b3J5IHtcclxuXHJcbiAgICBzdGF0aWMgY3JlYXRlRnJvbUpTT04gKGpzb25SZXByZXNlbnRhdGlvbk9mU2hhcGUpIHtcclxuICAgICAgICByZXR1cm4gU2hhcGVGYWN0b3J5LmNyZWF0ZShqc29uUmVwcmVzZW50YXRpb25PZlNoYXBlLnR5cGUsIGpzb25SZXByZXNlbnRhdGlvbk9mU2hhcGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBjcmVhdGUgKHNoYXBlVHlwZSwgcGFyYW1zKSB7XHJcbiAgICAgICAgaWYoIFNoYXBlRmFjdG9yeS5pc0NpcmNsZShzaGFwZVR5cGUpICl7XHJcbiAgICAgICAgICAgIGNvbnN0IGNpcmNsZSA9ICBuZXcgQ2lyY2xlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXMuY2VudGVyWCwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtcy5jZW50ZXJZLCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zLnJhZGl1c1xyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGNpcmNsZS5zZXRTdHJva2VTdHlsZShwYXJhbXMuc3Ryb2tlU3R5bGUpO1xyXG4gICAgICAgICAgICBjaXJjbGUuc2V0VVVJRChwYXJhbXMuVVVJRCk7XHJcbiAgICAgICAgICAgIHJldHVybiBjaXJjbGU7XHJcbiAgICAgICAgfSBlbHNlIGlmKCBTaGFwZUZhY3RvcnkuaXNMaW5lKHNoYXBlVHlwZSkgKXtcclxuICAgICAgICAgICAgY29uc3QgbGluZSA9IG5ldyBMaW5lKFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLm9yaWdpblgsIFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLm9yaWdpblksIFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLmVuZFgsXHJcbiAgICAgICAgICAgICAgICBwYXJhbXMuZW5kWVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBsaW5lLnNldFN0cm9rZVN0eWxlKHBhcmFtcy5zdHJva2VTdHlsZSk7XHJcbiAgICAgICAgICAgIGxpbmUuc2V0VVVJRChwYXJhbXMuVVVJRCk7XHJcbiAgICAgICAgICAgIGxpbmUuc2V0TGFiZWwocGFyYW1zLmxhYmVsKTtcclxuICAgICAgICAgICAgcmV0dXJuIGxpbmU7XHJcbiAgICAgICAgfSBlbHNlIGlmKCBTaGFwZUZhY3RvcnkuaXNGcmVlZm9ybShzaGFwZVR5cGUpICl7XHJcbiAgICAgICAgICAgIGNvbnN0IGZyZWVmb3JtID0gbmV3IEZyZWVGb3JtKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXMucG9pbnRzXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGZyZWVmb3JtLnNldFN0cm9rZVN0eWxlKHBhcmFtcy5zdHJva2VTdHlsZSk7XHJcbiAgICAgICAgICAgIGZyZWVmb3JtLnNldFVVSUQocGFyYW1zLlVVSUQpO1xyXG4gICAgICAgICAgICByZXR1cm4gZnJlZWZvcm07XHJcbiAgICAgICAgfSBlbHNlIGlmKCBTaGFwZUZhY3RvcnkuaXNQb2ludChzaGFwZVR5cGUpICl7XHJcbiAgICAgICAgICAgIGNvbnN0IHBvaW50ID0gbmV3IFBvaW50KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXMuWCwgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtcy5ZXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHBvaW50LnNldFN0cm9rZVN0eWxlKHBhcmFtcy5zdHJva2VTdHlsZSk7XHJcbiAgICAgICAgICAgIHBvaW50LnNldFVVSUQocGFyYW1zLlVVSUQpO1xyXG4gICAgICAgICAgICByZXR1cm4gcG9pbnQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coc2hhcGVUeXBlICsgXCI6IHRoaXMgU2hhcGUgdHlwZSBpcyBub3QgaW1wbGVtZW50ZWQgaW4gdGhlIGZhY3RvcnlcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBUaGlzIG1ldGhvZCBvbmx5IHRha2VzIG9yaWdpbiBwYXJhbWV0ZXJzIGFuZCBwcm92aWRleCBkZWZhdWx0cyBmb3IgZW5kIHBvaW50IGFuZCByYWRpdXNcclxuICAgICAqL1xyXG4gICAgc3RhdGljIGluaXRpYWxpemUgKHNoYXBlVHlwZSwgcGFyYW1zKSB7XHJcbiAgICAgICAgaWYoIFNoYXBlRmFjdG9yeS5pc0NpcmNsZShzaGFwZVR5cGUpICl7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgQ2lyY2xlKFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLm9yaWdpblgsIFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLm9yaWdpblksIFxyXG4gICAgICAgICAgICAgICAgMFxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSBpZiggU2hhcGVGYWN0b3J5LmlzTGluZShzaGFwZVR5cGUpICl7XHJcbiAgICAgICAgICAgIGNvbnN0IGxpbmUgPSBuZXcgTGluZShcclxuICAgICAgICAgICAgICAgIHBhcmFtcy5vcmlnaW5YLCBcclxuICAgICAgICAgICAgICAgIHBhcmFtcy5vcmlnaW5ZLCBcclxuICAgICAgICAgICAgICAgIHBhcmFtcy5vcmlnaW5YLFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLm9yaWdpbllcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgLy9saW5lLnNldFN0cm9rZVN0eWxlKHN0cm9rZVN0eWxlKTtcclxuICAgICAgICAgICAgcmV0dXJuIGxpbmU7XHJcbiAgICAgICAgfSBlbHNlIGlmKCBTaGFwZUZhY3RvcnkuaXNGcmVlZm9ybShzaGFwZVR5cGUpICl7XHJcbiAgICAgICAgICAgIGNvbnN0IHBvaW50cyA9IFtdO1xyXG4gICAgICAgICAgICBjb25zdCBvcmlnaW4gPSB7fTtcclxuICAgICAgICAgICAgb3JpZ2luLnggPSBwYXJhbXMub3JpZ2luWDtcclxuICAgICAgICAgICAgb3JpZ2luLnkgPSBwYXJhbXMub3JpZ2luWTtcclxuICAgICAgICAgICAgcG9pbnRzLnB1c2gob3JpZ2luKTtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBGcmVlRm9ybShcclxuICAgICAgICAgICAgICAgIHBvaW50c1xyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSBpZiggU2hhcGVGYWN0b3J5LmlzUG9pbnQoc2hhcGVUeXBlKSApe1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IFBvaW50KFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLm9yaWdpblgsIFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLm9yaWdpbllcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhzaGFwZVR5cGUgKyBcIjogdGhpcyBTaGFwZSB0eXBlIGlzIG5vdCBpbXBsZW1lbnRlZCBpbiB0aGUgaW5pdGlhbGl6YXRpb24gZmFjdG9yeVwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAvKlxyXG4gICAgICogVXRpbGl0eSBmdW5jdGlvbiB0byBkZXRlcm1pbmUgaWYgcGFyYW0gKHN0cmluZykgaXMgYSBjaXJjbGVcclxuICAgICAqL1xyXG4gICAgc3RhdGljIGlzQ2lyY2xlIChwYXJhbSkge1xyXG4gICAgICAgIGlmKHBhcmFtID09PSBcIkNpcmNsZVwiIHx8IHBhcmFtID09PSBcImNpcmNsZVwiKXtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogVXRpbGl0eSBmdW5jdGlvbiB0byBkZXRlcm1pbmUgaWYgcGFyYW0gKHN0cmluZykgaXMgYSBsaW5lXHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBpc0xpbmUgKHBhcmFtKSB7XHJcbiAgICAgICAgaWYocGFyYW0gPT09IFwiTGluZVwiIHx8IHBhcmFtID09PSBcInN0cmFpZ2h0TGluZVwiKXtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgICogVXRpbGl0eSBmdW5jdGlvbiB0byBkZXRlcm1pbmUgaWYgcGFyYW0gKHN0cmluZykgaXMgYSBmcmVlZm9ybVxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgaXNGcmVlZm9ybSAocGFyYW0pIHtcclxuICAgICAgICBpZihwYXJhbSA9PT0gXCJGcmVlRm9ybVwiIHx8IHBhcmFtID09PSBcImZyZWVGb3JtXCIpe1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICAgKiBVdGlsaXR5IGZ1bmN0aW9uIHRvIGRldGVybWluZSBpZiBwYXJhbSAoc3RyaW5nKSBpcyBhIHBvaW50XHJcbiAgICAgKi9cclxuICAgIHN0YXRpYyBpc1BvaW50IChwYXJhbSkge1xyXG4gICAgICAgIGlmKHBhcmFtID09PSBcIlBvaW50XCIgfHwgcGFyYW0gPT09IFwicG9pbnRcIil7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gU2hhcGVGYWN0b3J5OyIsIid1c2Ugc3RyaWN0JztcclxuLypcclxuICogUGFyZW50IGNsYXNzIGFic3RyYWN0aW9uIGZvciBTaGFwZSBzdG9yYWdlLlxyXG4gKiBUaGUgaWRlYSBpcyB0aGF0IHJlZmVycmluZyB0byBzaGFwZXMgaW4gYW4gYXJyYXkgYnkgaW5kZXggbWlnaHQgYmUgYSBsaXR0bGUgdG9vIGJyaXR0bGVcclxuICogSXQgbWlnaHQgYmUgYSBtcmUgY29uc2lzdGVudCBhcHByb2FjaCB0byByZWZlciB0byBzaGFwZSBieSBVVUlEXHJcbiAqIGFuZCB0aHVzIGFuIGFzc29jaWF0aXZlIGFycmF5IChvYmplY3QpIGEgTWFwIG1pZ2h0IGJlIG5lZWRlZFxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIFNoYXBlU3RvcmUoKSB7XHJcblxyXG59XHJcblxyXG5TaGFwZVN0b3JlLnByb3RvdHlwZS5hZGQgPSBmdW5jdGlvbiAob2JqKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcImFkZCBpcyBub3QgaW1wbGVtZW50ZWQgaGVyZVwiKTtcclxuICAgIFxyXG59O1xyXG5cclxuU2hhcGVTdG9yZS5wcm90b3R5cGUuZ2V0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgY29uc29sZS5sb2coXCJnZXQgaXMgbm90IGltcGxlbWVudGVkIGhlcmVcIik7XHJcbn07XHJcblxyXG5TaGFwZVN0b3JlLnByb3RvdHlwZS5kZWxldGUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcImRlbGV0ZSBpcyBub3QgaW1wbGVtZW50ZWQgaGVyZVwiKTtcclxufTtcclxuXHJcblNoYXBlU3RvcmUucHJvdG90eXBlLnNpemUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcInNpemUgaXMgbm90IGltcGxlbWVudGVkIGhlcmVcIik7XHJcbn07XHJcblxyXG4vKlxyXG4gKiBVc2VmdWwgZm9yIGRlYnVnZ2luZ1xyXG4gKi9cclxuU2hhcGVTdG9yZS5wcm90b3R5cGUucmV0dXJuUmF3RGF0YSA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGNvbnNvbGUubG9nKFwic2l6ZSBpcyBub3QgaW1wbGVtZW50ZWQgaGVyZVwiKTtcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gU2hhcGVTdG9yZTtcclxuXHJcbiIsIid1c2Ugc3RyaWN0JztcclxudmFyIFNoYXBlU3RvcmUgPSByZXF1aXJlKCcuL1NoYXBlU3RvcmUnKTtcclxuXHJcbi8qXHJcbiAqIEltcGxlbWVudGF0aW9uIG9mIHRoZSBTaGFwZVN0b3JlIHRoYXQgdXJlbGllcyBvbiBbXSBmb3Igc3RvcmFnZVxyXG4gKi9cclxuZnVuY3Rpb24gU2hhcGVTdG9yZUFycmF5KCkge1xyXG5cclxuICAgIFNoYXBlU3RvcmUuY2FsbCh0aGlzKTsgLy9jYWxsIHN1cGVyIGNvbnN0cnVjdG9yXHJcblxyXG4gICAgdGhpcy5pbml0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuc2hhcGVTdG9yZSA9IFtdO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmluaXQoKTtcclxuICAgIFxyXG4gICAgdGhpcy5hZGQgPSBmdW5jdGlvbihvYmope1xyXG4gICAgICAgIHRoaXMuc2hhcGVTdG9yZS5wdXNoKG9iaik7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuZ2V0ID0gZnVuY3Rpb24oaW5kZXgpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNoYXBlU3RvcmVbaW5kZXhdO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmRlbGV0ZSA9IGZ1bmN0aW9uKGluZGV4KXtcclxuICAgICAgICB0aGlzLnNoYXBlU3RvcmUuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH07XHJcbiAgICAvKlxyXG4gICAgICogVkVyeSBzcGVjaWZpYyB0byBBcnJheXMgZnVuY3Rpb24gXHJcbiAgICAgKi9cclxuICAgIHRoaXMudXBkYXRlQnlJbmRleCA9IGZ1bmN0aW9uKGluZGV4LCBuZXdTaGFwZSl7XHJcbiAgICAgICAgdGhpcy5zaGFwZVN0b3JlW2luZGV4XSA9IG5ld1NoYXBlO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnNpemUgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNoYXBlU3RvcmUubGVuZ3RoO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnJldHVyblJhd0RhdGEgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNoYXBlU3RvcmU7XHJcbiAgICB9O1xyXG5cclxufVxyXG5cclxuXHJcblNoYXBlU3RvcmVBcnJheS5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKFNoYXBlU3RvcmUucHJvdG90eXBlLCB7XHJcbiAgICBjb25zdHJ1Y3Rvcjoge1xyXG4gICAgICAgIHZhbHVlOiBTaGFwZVN0b3JlQXJyYXlcclxuICAgIH1cclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFNoYXBlU3RvcmVBcnJheTsiLCIvKiBcclxuICogVG8gY2hhbmdlIHRoaXMgbGljZW5zZSBoZWFkZXIsIGNob29zZSBMaWNlbnNlIEhlYWRlcnMgaW4gUHJvamVjdCBQcm9wZXJ0aWVzLlxyXG4gKiBUbyBjaGFuZ2UgdGhpcyB0ZW1wbGF0ZSBmaWxlLCBjaG9vc2UgVG9vbHMgfCBUZW1wbGF0ZXNcclxuICogYW5kIG9wZW4gdGhlIHRlbXBsYXRlIGluIHRoZSBlZGl0b3IuXHJcbiAqL1xyXG5leHBvcnRzLmlzQ29sbGluZWFyID0gZnVuY3Rpb24gaXNDb2xsaW5lYXIoeDEsIHkxLCB4MiwgeTIsIHgzLCB5Mykge1xyXG4gICAgdmFyIGNyb3NzcHJvZHVjdCA9ICh5MSAtIHkyKSAqICh4MyAtIHgyKSAtICh4MSAtIHgyKSAqICh5MyAtIHkyKTtcclxuICAgIHJldHVybiBjcm9zc3Byb2R1Y3Q7XHJcbn07XHJcblxyXG5leHBvcnRzLmNhbGN1bGF0ZURpc3RhbmNlID0gZnVuY3Rpb24gY2FsY3VsYXRlRGlzdGFuY2UoeDEsIHkxLCB4MiwgeTIpIHtcclxuICAgIHZhciBkaXN0YW5jZSA9IE1hdGguc3FydCgoeDEgLSB4MikgKiAoeDEgLSB4MikgKyAoeTEgLSB5MikgKiAoeTEgLSB5MikpO1xyXG4gICAgcmV0dXJuIGRpc3RhbmNlO1xyXG59O1xyXG5cclxuZXhwb3J0cy5pc1BvaW50T25MaW5lID0gZnVuY3Rpb24gaXNQb2ludE9uTGluZSh4MSwgeTEsIHgyLCB5MiwgeDMsIHkzKSB7XHJcbiAgICB2YXIgY3Jvc3Nwcm9kdWN0ID0gaXNDb2xsaW5lYXIoeDEsIHkxLCB4MiwgeTIsIHgzLCB5Myk7XHJcbiAgICAvLyBjcm9zc3Byb2R1Y3QgPT09IDAgaW4gdGhlIHBlcmZleHQgdmVjdG9yIHdvcmxkXHJcblxyXG4gICAgaWYgKChjcm9zc3Byb2R1Y3QgPCAyMDAgJiYgY3Jvc3Nwcm9kdWN0ID4gLTIwMCkgJiYgKCh4MiA8PSB4MSAmJiB4MSA8PSB4MykgfHwgKHgzIDw9IHgxICYmIHgxIDw9IHgyKSkgJiYgKCh5MiA8PSB5MSAmJiB5MSA8PSB5MykgfHwgKHkzIDw9IHkxICYmIHkxIDw9IHkyKSkpIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxufTsiLCIvKiEgZGVjaW1hbC5qcyB2Ny41LjEgaHR0cHM6Ly9naXRodWIuY29tL01pa2VNY2wvZGVjaW1hbC5qcy9MSUNFTkNFICovXHJcbjsoZnVuY3Rpb24gKGdsb2JhbFNjb3BlKSB7XHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuXHJcbiAgLypcclxuICAgKiAgZGVjaW1hbC5qcyB2Ny41LjFcclxuICAgKiAgQW4gYXJiaXRyYXJ5LXByZWNpc2lvbiBEZWNpbWFsIHR5cGUgZm9yIEphdmFTY3JpcHQuXHJcbiAgICogIGh0dHBzOi8vZ2l0aHViLmNvbS9NaWtlTWNsL2RlY2ltYWwuanNcclxuICAgKiAgQ29weXJpZ2h0IChjKSAyMDE3IE1pY2hhZWwgTWNsYXVnaGxpbiA8TThjaDg4bEBnbWFpbC5jb20+XHJcbiAgICogIE1JVCBMaWNlbmNlXHJcbiAgICovXHJcblxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAgRURJVEFCTEUgREVGQVVMVFMgIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAvL1xyXG5cclxuXHJcbiAgICAvLyBUaGUgbWF4aW11bSBleHBvbmVudCBtYWduaXR1ZGUuXHJcbiAgICAvLyBUaGUgbGltaXQgb24gdGhlIHZhbHVlIG9mIGB0b0V4cE5lZ2AsIGB0b0V4cFBvc2AsIGBtaW5FYCBhbmQgYG1heEVgLlxyXG4gIHZhciBFWFBfTElNSVQgPSA5ZTE1LCAgICAgICAgICAgICAgICAgICAgICAvLyAwIHRvIDllMTVcclxuXHJcbiAgICAvLyBUaGUgbGltaXQgb24gdGhlIHZhbHVlIG9mIGBwcmVjaXNpb25gLCBhbmQgb24gdGhlIHZhbHVlIG9mIHRoZSBmaXJzdCBhcmd1bWVudCB0b1xyXG4gICAgLy8gYHRvRGVjaW1hbFBsYWNlc2AsIGB0b0V4cG9uZW50aWFsYCwgYHRvRml4ZWRgLCBgdG9QcmVjaXNpb25gIGFuZCBgdG9TaWduaWZpY2FudERpZ2l0c2AuXHJcbiAgICBNQVhfRElHSVRTID0gMWU5LCAgICAgICAgICAgICAgICAgICAgICAgIC8vIDAgdG8gMWU5XHJcblxyXG4gICAgLy8gQmFzZSBjb252ZXJzaW9uIGFscGhhYmV0LlxyXG4gICAgTlVNRVJBTFMgPSAnMDEyMzQ1Njc4OWFiY2RlZicsXHJcblxyXG4gICAgLy8gVGhlIG5hdHVyYWwgbG9nYXJpdGhtIG9mIDEwICgxMDI1IGRpZ2l0cykuXHJcbiAgICBMTjEwID0gJzIuMzAyNTg1MDkyOTk0MDQ1Njg0MDE3OTkxNDU0Njg0MzY0MjA3NjAxMTAxNDg4NjI4NzcyOTc2MDMzMzI3OTAwOTY3NTcyNjA5Njc3MzUyNDgwMjM1OTk3MjA1MDg5NTk4Mjk4MzQxOTY3Nzg0MDQyMjg2MjQ4NjMzNDA5NTI1NDY1MDgyODA2NzU2NjY2Mjg3MzY5MDk4NzgxNjg5NDgyOTA3MjA4MzI1NTU0NjgwODQzNzk5ODk0ODI2MjMzMTk4NTI4MzkzNTA1MzA4OTY1Mzc3NzMyNjI4ODQ2MTYzMzY2MjIyMjg3Njk4MjE5ODg2NzQ2NTQzNjY3NDc0NDA0MjQzMjc0MzY1MTU1MDQ4OTM0MzE0OTM5MzkxNDc5NjE5NDA0NDAwMjIyMTA1MTAxNzE0MTc0ODAwMzY4ODA4NDAxMjY0NzA4MDY4NTU2Nzc0MzIxNjIyODM1NTIyMDExNDgwNDY2MzcxNTY1OTEyMTM3MzQ1MDc0Nzg1Njk0NzY4MzQ2MzYxNjc5MjEwMTgwNjQ0NTA3MDY0ODAwMDI3NzUwMjY4NDkxNjc0NjU1MDU4Njg1NjkzNTY3MzQyMDY3MDU4MTEzNjQyOTIyNDU1NDQwNTc1ODkyNTcyNDIwODI0MTMxNDY5NTY4OTAxNjc1ODk0MDI1Njc3NjMxMTM1NjkxOTI5MjAzMzM3NjU4NzE0MTY2MDIzMDEwNTcwMzA4OTYzNDU3MjA3NTQ0MDM3MDg0NzQ2OTk0MDE2ODI2OTI4MjgwODQ4MTE4NDI4OTMxNDg0ODUyNDk0ODY0NDg3MTkyNzgwOTY3NjI3MTI3NTc3NTM5NzAyNzY2ODYwNTk1MjQ5NjcxNjY3NDE4MzQ4NTcwNDQyMjUwNzE5Nzk2NTAwNDcxNDk1MTA1MDQ5MjIxNDc3NjU2NzYzNjkzODY2Mjk3Njk3OTUyMjExMDcxODI2NDU0OTczNDc3MjY2MjQyNTcwOTQyOTMyMjU4Mjc5ODUwMjU4NTUwOTc4NTI2NTM4MzIwNzYwNjcyNjMxNzE2NDMwOTUwNTk5NTA4NzgwNzUyMzcxMDMzMzEwMTE5Nzg1NzU0NzMzMTU0MTQyMTgwODQyNzU0Mzg2MzU5MTc3ODExNzA1NDMwOTgyNzQ4MjM4NTA0NTY0ODAxOTA5NTYxMDI5OTI5MTgyNDMxODIzNzUyNTM1NzcwOTc1MDUzOTU2NTE4NzY5NzUxMDM3NDk3MDg4ODY5MjE4MDIwNTE4OTMzOTUwNzIzODUzOTIwNTE0NDYzNDE5NzI2NTI4NzI4Njk2NTExMDg2MjU3MTQ5MjE5ODg0OTk3ODc0ODg3Mzc3MTM0NTY4NjIwOTE2NzA1OCcsXHJcblxyXG4gICAgLy8gUGkgKDEwMjUgZGlnaXRzKS5cclxuICAgIFBJID0gJzMuMTQxNTkyNjUzNTg5NzkzMjM4NDYyNjQzMzgzMjc5NTAyODg0MTk3MTY5Mzk5Mzc1MTA1ODIwOTc0OTQ0NTkyMzA3ODE2NDA2Mjg2MjA4OTk4NjI4MDM0ODI1MzQyMTE3MDY3OTgyMTQ4MDg2NTEzMjgyMzA2NjQ3MDkzODQ0NjA5NTUwNTgyMjMxNzI1MzU5NDA4MTI4NDgxMTE3NDUwMjg0MTAyNzAxOTM4NTIxMTA1NTU5NjQ0NjIyOTQ4OTU0OTMwMzgxOTY0NDI4ODEwOTc1NjY1OTMzNDQ2MTI4NDc1NjQ4MjMzNzg2NzgzMTY1MjcxMjAxOTA5MTQ1NjQ4NTY2OTIzNDYwMzQ4NjEwNDU0MzI2NjQ4MjEzMzkzNjA3MjYwMjQ5MTQxMjczNzI0NTg3MDA2NjA2MzE1NTg4MTc0ODgxNTIwOTIwOTYyODI5MjU0MDkxNzE1MzY0MzY3ODkyNTkwMzYwMDExMzMwNTMwNTQ4ODIwNDY2NTIxMzg0MTQ2OTUxOTQxNTExNjA5NDMzMDU3MjcwMzY1NzU5NTkxOTUzMDkyMTg2MTE3MzgxOTMyNjExNzkzMTA1MTE4NTQ4MDc0NDYyMzc5OTYyNzQ5NTY3MzUxODg1NzUyNzI0ODkxMjI3OTM4MTgzMDExOTQ5MTI5ODMzNjczMzYyNDQwNjU2NjQzMDg2MDIxMzk0OTQ2Mzk1MjI0NzM3MTkwNzAyMTc5ODYwOTQzNzAyNzcwNTM5MjE3MTc2MjkzMTc2NzUyMzg0Njc0ODE4NDY3NjY5NDA1MTMyMDAwNTY4MTI3MTQ1MjYzNTYwODI3Nzg1NzcxMzQyNzU3Nzg5NjA5MTczNjM3MTc4NzIxNDY4NDQwOTAxMjI0OTUzNDMwMTQ2NTQ5NTg1MzcxMDUwNzkyMjc5Njg5MjU4OTIzNTQyMDE5OTU2MTEyMTI5MDIxOTYwODY0MDM0NDE4MTU5ODEzNjI5Nzc0NzcxMzA5OTYwNTE4NzA3MjExMzQ5OTk5OTk4MzcyOTc4MDQ5OTUxMDU5NzMxNzMyODE2MDk2MzE4NTk1MDI0NDU5NDU1MzQ2OTA4MzAyNjQyNTIyMzA4MjUzMzQ0Njg1MDM1MjYxOTMxMTg4MTcxMDEwMDAzMTM3ODM4NzUyODg2NTg3NTMzMjA4MzgxNDIwNjE3MTc3NjY5MTQ3MzAzNTk4MjUzNDkwNDI4NzU1NDY4NzMxMTU5NTYyODYzODgyMzUzNzg3NTkzNzUxOTU3NzgxODU3NzgwNTMyMTcxMjI2ODA2NjEzMDAxOTI3ODc2NjExMTk1OTA5MjE2NDIwMTk4OTM4MDk1MjU3MjAxMDY1NDg1ODYzMjc4OScsXHJcblxyXG5cclxuICAgIC8vIFRoZSBpbml0aWFsIGNvbmZpZ3VyYXRpb24gcHJvcGVydGllcyBvZiB0aGUgRGVjaW1hbCBjb25zdHJ1Y3Rvci5cclxuICAgIERlY2ltYWwgPSB7XHJcblxyXG4gICAgICAvLyBUaGVzZSB2YWx1ZXMgbXVzdCBiZSBpbnRlZ2VycyB3aXRoaW4gdGhlIHN0YXRlZCByYW5nZXMgKGluY2x1c2l2ZSkuXHJcbiAgICAgIC8vIE1vc3Qgb2YgdGhlc2UgdmFsdWVzIGNhbiBiZSBjaGFuZ2VkIGF0IHJ1bi10aW1lIHVzaW5nIHRoZSBgRGVjaW1hbC5jb25maWdgIG1ldGhvZC5cclxuXHJcbiAgICAgIC8vIFRoZSBtYXhpbXVtIG51bWJlciBvZiBzaWduaWZpY2FudCBkaWdpdHMgb2YgdGhlIHJlc3VsdCBvZiBhIGNhbGN1bGF0aW9uIG9yIGJhc2UgY29udmVyc2lvbi5cclxuICAgICAgLy8gRS5nLiBgRGVjaW1hbC5jb25maWcoeyBwcmVjaXNpb246IDIwIH0pO2BcclxuICAgICAgcHJlY2lzaW9uOiAyMCwgICAgICAgICAgICAgICAgICAgICAgICAgLy8gMSB0byBNQVhfRElHSVRTXHJcblxyXG4gICAgICAvLyBUaGUgcm91bmRpbmcgbW9kZSB1c2VkIHdoZW4gcm91bmRpbmcgdG8gYHByZWNpc2lvbmAuXHJcbiAgICAgIC8vXHJcbiAgICAgIC8vIFJPVU5EX1VQICAgICAgICAgMCBBd2F5IGZyb20gemVyby5cclxuICAgICAgLy8gUk9VTkRfRE9XTiAgICAgICAxIFRvd2FyZHMgemVyby5cclxuICAgICAgLy8gUk9VTkRfQ0VJTCAgICAgICAyIFRvd2FyZHMgK0luZmluaXR5LlxyXG4gICAgICAvLyBST1VORF9GTE9PUiAgICAgIDMgVG93YXJkcyAtSW5maW5pdHkuXHJcbiAgICAgIC8vIFJPVU5EX0hBTEZfVVAgICAgNCBUb3dhcmRzIG5lYXJlc3QgbmVpZ2hib3VyLiBJZiBlcXVpZGlzdGFudCwgdXAuXHJcbiAgICAgIC8vIFJPVU5EX0hBTEZfRE9XTiAgNSBUb3dhcmRzIG5lYXJlc3QgbmVpZ2hib3VyLiBJZiBlcXVpZGlzdGFudCwgZG93bi5cclxuICAgICAgLy8gUk9VTkRfSEFMRl9FVkVOICA2IFRvd2FyZHMgbmVhcmVzdCBuZWlnaGJvdXIuIElmIGVxdWlkaXN0YW50LCB0b3dhcmRzIGV2ZW4gbmVpZ2hib3VyLlxyXG4gICAgICAvLyBST1VORF9IQUxGX0NFSUwgIDcgVG93YXJkcyBuZWFyZXN0IG5laWdoYm91ci4gSWYgZXF1aWRpc3RhbnQsIHRvd2FyZHMgK0luZmluaXR5LlxyXG4gICAgICAvLyBST1VORF9IQUxGX0ZMT09SIDggVG93YXJkcyBuZWFyZXN0IG5laWdoYm91ci4gSWYgZXF1aWRpc3RhbnQsIHRvd2FyZHMgLUluZmluaXR5LlxyXG4gICAgICAvL1xyXG4gICAgICAvLyBFLmcuXHJcbiAgICAgIC8vIGBEZWNpbWFsLnJvdW5kaW5nID0gNDtgXHJcbiAgICAgIC8vIGBEZWNpbWFsLnJvdW5kaW5nID0gRGVjaW1hbC5ST1VORF9IQUxGX1VQO2BcclxuICAgICAgcm91bmRpbmc6IDQsICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gMCB0byA4XHJcblxyXG4gICAgICAvLyBUaGUgbW9kdWxvIG1vZGUgdXNlZCB3aGVuIGNhbGN1bGF0aW5nIHRoZSBtb2R1bHVzOiBhIG1vZCBuLlxyXG4gICAgICAvLyBUaGUgcXVvdGllbnQgKHEgPSBhIC8gbikgaXMgY2FsY3VsYXRlZCBhY2NvcmRpbmcgdG8gdGhlIGNvcnJlc3BvbmRpbmcgcm91bmRpbmcgbW9kZS5cclxuICAgICAgLy8gVGhlIHJlbWFpbmRlciAocikgaXMgY2FsY3VsYXRlZCBhczogciA9IGEgLSBuICogcS5cclxuICAgICAgLy9cclxuICAgICAgLy8gVVAgICAgICAgICAwIFRoZSByZW1haW5kZXIgaXMgcG9zaXRpdmUgaWYgdGhlIGRpdmlkZW5kIGlzIG5lZ2F0aXZlLCBlbHNlIGlzIG5lZ2F0aXZlLlxyXG4gICAgICAvLyBET1dOICAgICAgIDEgVGhlIHJlbWFpbmRlciBoYXMgdGhlIHNhbWUgc2lnbiBhcyB0aGUgZGl2aWRlbmQgKEphdmFTY3JpcHQgJSkuXHJcbiAgICAgIC8vIEZMT09SICAgICAgMyBUaGUgcmVtYWluZGVyIGhhcyB0aGUgc2FtZSBzaWduIGFzIHRoZSBkaXZpc29yIChQeXRob24gJSkuXHJcbiAgICAgIC8vIEhBTEZfRVZFTiAgNiBUaGUgSUVFRSA3NTQgcmVtYWluZGVyIGZ1bmN0aW9uLlxyXG4gICAgICAvLyBFVUNMSUQgICAgIDkgRXVjbGlkaWFuIGRpdmlzaW9uLiBxID0gc2lnbihuKSAqIGZsb29yKGEgLyBhYnMobikpLiBBbHdheXMgcG9zaXRpdmUuXHJcbiAgICAgIC8vXHJcbiAgICAgIC8vIFRydW5jYXRlZCBkaXZpc2lvbiAoMSksIGZsb29yZWQgZGl2aXNpb24gKDMpLCB0aGUgSUVFRSA3NTQgcmVtYWluZGVyICg2KSwgYW5kIEV1Y2xpZGlhblxyXG4gICAgICAvLyBkaXZpc2lvbiAoOSkgYXJlIGNvbW1vbmx5IHVzZWQgZm9yIHRoZSBtb2R1bHVzIG9wZXJhdGlvbi4gVGhlIG90aGVyIHJvdW5kaW5nIG1vZGVzIGNhbiBhbHNvXHJcbiAgICAgIC8vIGJlIHVzZWQsIGJ1dCB0aGV5IG1heSBub3QgZ2l2ZSB1c2VmdWwgcmVzdWx0cy5cclxuICAgICAgbW9kdWxvOiAxLCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gMCB0byA5XHJcblxyXG4gICAgICAvLyBUaGUgZXhwb25lbnQgdmFsdWUgYXQgYW5kIGJlbmVhdGggd2hpY2ggYHRvU3RyaW5nYCByZXR1cm5zIGV4cG9uZW50aWFsIG5vdGF0aW9uLlxyXG4gICAgICAvLyBKYXZhU2NyaXB0IG51bWJlcnM6IC03XHJcbiAgICAgIHRvRXhwTmVnOiAtNywgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIDAgdG8gLUVYUF9MSU1JVFxyXG5cclxuICAgICAgLy8gVGhlIGV4cG9uZW50IHZhbHVlIGF0IGFuZCBhYm92ZSB3aGljaCBgdG9TdHJpbmdgIHJldHVybnMgZXhwb25lbnRpYWwgbm90YXRpb24uXHJcbiAgICAgIC8vIEphdmFTY3JpcHQgbnVtYmVyczogMjFcclxuICAgICAgdG9FeHBQb3M6ICAyMSwgICAgICAgICAgICAgICAgICAgICAgICAgLy8gMCB0byBFWFBfTElNSVRcclxuXHJcbiAgICAgIC8vIFRoZSBtaW5pbXVtIGV4cG9uZW50IHZhbHVlLCBiZW5lYXRoIHdoaWNoIHVuZGVyZmxvdyB0byB6ZXJvIG9jY3Vycy5cclxuICAgICAgLy8gSmF2YVNjcmlwdCBudW1iZXJzOiAtMzI0ICAoNWUtMzI0KVxyXG4gICAgICBtaW5FOiAtRVhQX0xJTUlULCAgICAgICAgICAgICAgICAgICAgICAvLyAtMSB0byAtRVhQX0xJTUlUXHJcblxyXG4gICAgICAvLyBUaGUgbWF4aW11bSBleHBvbmVudCB2YWx1ZSwgYWJvdmUgd2hpY2ggb3ZlcmZsb3cgdG8gSW5maW5pdHkgb2NjdXJzLlxyXG4gICAgICAvLyBKYXZhU2NyaXB0IG51bWJlcnM6IDMwOCAgKDEuNzk3NjkzMTM0ODYyMzE1N2UrMzA4KVxyXG4gICAgICBtYXhFOiBFWFBfTElNSVQsICAgICAgICAgICAgICAgICAgICAgICAvLyAxIHRvIEVYUF9MSU1JVFxyXG5cclxuICAgICAgLy8gV2hldGhlciB0byB1c2UgY3J5cHRvZ3JhcGhpY2FsbHktc2VjdXJlIHJhbmRvbSBudW1iZXIgZ2VuZXJhdGlvbiwgaWYgYXZhaWxhYmxlLlxyXG4gICAgICBjcnlwdG86IGZhbHNlICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0cnVlL2ZhbHNlXHJcbiAgICB9LFxyXG5cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gRU5EIE9GIEVESVRBQkxFIERFRkFVTFRTIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gLy9cclxuXHJcblxyXG4gICAgaW5leGFjdCwgbm9Db25mbGljdCwgcXVhZHJhbnQsXHJcbiAgICBleHRlcm5hbCA9IHRydWUsXHJcblxyXG4gICAgZGVjaW1hbEVycm9yID0gJ1tEZWNpbWFsRXJyb3JdICcsXHJcbiAgICBpbnZhbGlkQXJndW1lbnQgPSBkZWNpbWFsRXJyb3IgKyAnSW52YWxpZCBhcmd1bWVudDogJyxcclxuICAgIHByZWNpc2lvbkxpbWl0RXhjZWVkZWQgPSBkZWNpbWFsRXJyb3IgKyAnUHJlY2lzaW9uIGxpbWl0IGV4Y2VlZGVkJyxcclxuICAgIGNyeXB0b1VuYXZhaWxhYmxlID0gZGVjaW1hbEVycm9yICsgJ2NyeXB0byB1bmF2YWlsYWJsZScsXHJcblxyXG4gICAgbWF0aGZsb29yID0gTWF0aC5mbG9vcixcclxuICAgIG1hdGhwb3cgPSBNYXRoLnBvdyxcclxuXHJcbiAgICBpc0JpbmFyeSA9IC9eMGIoWzAxXSsoXFwuWzAxXSopP3xcXC5bMDFdKykocFsrLV0/XFxkKyk/JC9pLFxyXG4gICAgaXNIZXggPSAvXjB4KFswLTlhLWZdKyhcXC5bMC05YS1mXSopP3xcXC5bMC05YS1mXSspKHBbKy1dP1xcZCspPyQvaSxcclxuICAgIGlzT2N0YWwgPSAvXjBvKFswLTddKyhcXC5bMC03XSopP3xcXC5bMC03XSspKHBbKy1dP1xcZCspPyQvaSxcclxuICAgIGlzRGVjaW1hbCA9IC9eKFxcZCsoXFwuXFxkKik/fFxcLlxcZCspKGVbKy1dP1xcZCspPyQvaSxcclxuXHJcbiAgICBCQVNFID0gMWU3LFxyXG4gICAgTE9HX0JBU0UgPSA3LFxyXG4gICAgTUFYX1NBRkVfSU5URUdFUiA9IDkwMDcxOTkyNTQ3NDA5OTEsXHJcblxyXG4gICAgTE4xMF9QUkVDSVNJT04gPSBMTjEwLmxlbmd0aCAtIDEsXHJcbiAgICBQSV9QUkVDSVNJT04gPSBQSS5sZW5ndGggLSAxLFxyXG5cclxuICAgIC8vIERlY2ltYWwucHJvdG90eXBlIG9iamVjdFxyXG4gICAgUCA9IHt9O1xyXG5cclxuXHJcbiAgLy8gRGVjaW1hbCBwcm90b3R5cGUgbWV0aG9kc1xyXG5cclxuXHJcbiAgLypcclxuICAgKiAgYWJzb2x1dGVWYWx1ZSAgICAgICAgICAgICBhYnNcclxuICAgKiAgY2VpbFxyXG4gICAqICBjb21wYXJlZFRvICAgICAgICAgICAgICAgIGNtcFxyXG4gICAqICBjb3NpbmUgICAgICAgICAgICAgICAgICAgIGNvc1xyXG4gICAqICBjdWJlUm9vdCAgICAgICAgICAgICAgICAgIGNicnRcclxuICAgKiAgZGVjaW1hbFBsYWNlcyAgICAgICAgICAgICBkcFxyXG4gICAqICBkaXZpZGVkQnkgICAgICAgICAgICAgICAgIGRpdlxyXG4gICAqICBkaXZpZGVkVG9JbnRlZ2VyQnkgICAgICAgIGRpdlRvSW50XHJcbiAgICogIGVxdWFscyAgICAgICAgICAgICAgICAgICAgZXFcclxuICAgKiAgZmxvb3JcclxuICAgKiAgZ3JlYXRlclRoYW4gICAgICAgICAgICAgICBndFxyXG4gICAqICBncmVhdGVyVGhhbk9yRXF1YWxUbyAgICAgIGd0ZVxyXG4gICAqICBoeXBlcmJvbGljQ29zaW5lICAgICAgICAgIGNvc2hcclxuICAgKiAgaHlwZXJib2xpY1NpbmUgICAgICAgICAgICBzaW5oXHJcbiAgICogIGh5cGVyYm9saWNUYW5nZW50ICAgICAgICAgdGFuaFxyXG4gICAqICBpbnZlcnNlQ29zaW5lICAgICAgICAgICAgIGFjb3NcclxuICAgKiAgaW52ZXJzZUh5cGVyYm9saWNDb3NpbmUgICBhY29zaFxyXG4gICAqICBpbnZlcnNlSHlwZXJib2xpY1NpbmUgICAgIGFzaW5oXHJcbiAgICogIGludmVyc2VIeXBlcmJvbGljVGFuZ2VudCAgYXRhbmhcclxuICAgKiAgaW52ZXJzZVNpbmUgICAgICAgICAgICAgICBhc2luXHJcbiAgICogIGludmVyc2VUYW5nZW50ICAgICAgICAgICAgYXRhblxyXG4gICAqICBpc0Zpbml0ZVxyXG4gICAqICBpc0ludGVnZXIgICAgICAgICAgICAgICAgIGlzSW50XHJcbiAgICogIGlzTmFOXHJcbiAgICogIGlzTmVnYXRpdmUgICAgICAgICAgICAgICAgaXNOZWdcclxuICAgKiAgaXNQb3NpdGl2ZSAgICAgICAgICAgICAgICBpc1Bvc1xyXG4gICAqICBpc1plcm9cclxuICAgKiAgbGVzc1RoYW4gICAgICAgICAgICAgICAgICBsdFxyXG4gICAqICBsZXNzVGhhbk9yRXF1YWxUbyAgICAgICAgIGx0ZVxyXG4gICAqICBsb2dhcml0aG0gICAgICAgICAgICAgICAgIGxvZ1xyXG4gICAqICBbbWF4aW11bV0gICAgICAgICAgICAgICAgIFttYXhdXHJcbiAgICogIFttaW5pbXVtXSAgICAgICAgICAgICAgICAgW21pbl1cclxuICAgKiAgbWludXMgICAgICAgICAgICAgICAgICAgICBzdWJcclxuICAgKiAgbW9kdWxvICAgICAgICAgICAgICAgICAgICBtb2RcclxuICAgKiAgbmF0dXJhbEV4cG9uZW50aWFsICAgICAgICBleHBcclxuICAgKiAgbmF0dXJhbExvZ2FyaXRobSAgICAgICAgICBsblxyXG4gICAqICBuZWdhdGVkICAgICAgICAgICAgICAgICAgIG5lZ1xyXG4gICAqICBwbHVzICAgICAgICAgICAgICAgICAgICAgIGFkZFxyXG4gICAqICBwcmVjaXNpb24gICAgICAgICAgICAgICAgIHNkXHJcbiAgICogIHJvdW5kXHJcbiAgICogIHNpbmUgICAgICAgICAgICAgICAgICAgICAgc2luXHJcbiAgICogIHNxdWFyZVJvb3QgICAgICAgICAgICAgICAgc3FydFxyXG4gICAqICB0YW5nZW50ICAgICAgICAgICAgICAgICAgIHRhblxyXG4gICAqICB0aW1lcyAgICAgICAgICAgICAgICAgICAgIG11bFxyXG4gICAqICB0b0JpbmFyeVxyXG4gICAqICB0b0RlY2ltYWxQbGFjZXMgICAgICAgICAgIHRvRFBcclxuICAgKiAgdG9FeHBvbmVudGlhbFxyXG4gICAqICB0b0ZpeGVkXHJcbiAgICogIHRvRnJhY3Rpb25cclxuICAgKiAgdG9IZXhhZGVjaW1hbCAgICAgICAgICAgICB0b0hleFxyXG4gICAqICB0b05lYXJlc3RcclxuICAgKiAgdG9OdW1iZXJcclxuICAgKiAgdG9PY3RhbFxyXG4gICAqICB0b1Bvd2VyICAgICAgICAgICAgICAgICAgIHBvd1xyXG4gICAqICB0b1ByZWNpc2lvblxyXG4gICAqICB0b1NpZ25pZmljYW50RGlnaXRzICAgICAgIHRvU0RcclxuICAgKiAgdG9TdHJpbmdcclxuICAgKiAgdHJ1bmNhdGVkICAgICAgICAgICAgICAgICB0cnVuY1xyXG4gICAqICB2YWx1ZU9mICAgICAgICAgICAgICAgICAgIHRvSlNPTlxyXG4gICAqL1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgYWJzb2x1dGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5hYnNvbHV0ZVZhbHVlID0gUC5hYnMgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgeCA9IG5ldyB0aGlzLmNvbnN0cnVjdG9yKHRoaXMpO1xyXG4gICAgaWYgKHgucyA8IDApIHgucyA9IDE7XHJcbiAgICByZXR1cm4gZmluYWxpc2UoeCk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCByb3VuZGVkIHRvIGEgd2hvbGUgbnVtYmVyIGluIHRoZVxyXG4gICAqIGRpcmVjdGlvbiBvZiBwb3NpdGl2ZSBJbmZpbml0eS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuY2VpbCA9IGZ1bmN0aW9uICgpIHtcclxuICAgIHJldHVybiBmaW5hbGlzZShuZXcgdGhpcy5jb25zdHJ1Y3Rvcih0aGlzKSwgdGhpcy5lICsgMSwgMik7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuXHJcbiAgICogICAxICAgIGlmIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgaXMgZ3JlYXRlciB0aGFuIHRoZSB2YWx1ZSBvZiBgeWAsXHJcbiAgICogIC0xICAgIGlmIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgaXMgbGVzcyB0aGFuIHRoZSB2YWx1ZSBvZiBgeWAsXHJcbiAgICogICAwICAgIGlmIHRoZXkgaGF2ZSB0aGUgc2FtZSB2YWx1ZSxcclxuICAgKiAgIE5hTiAgaWYgdGhlIHZhbHVlIG9mIGVpdGhlciBEZWNpbWFsIGlzIE5hTi5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuY29tcGFyZWRUbyA9IFAuY21wID0gZnVuY3Rpb24gKHkpIHtcclxuICAgIHZhciBpLCBqLCB4ZEwsIHlkTCxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIHhkID0geC5kLFxyXG4gICAgICB5ZCA9ICh5ID0gbmV3IHguY29uc3RydWN0b3IoeSkpLmQsXHJcbiAgICAgIHhzID0geC5zLFxyXG4gICAgICB5cyA9IHkucztcclxuXHJcbiAgICAvLyBFaXRoZXIgTmFOIG9yIMKxSW5maW5pdHk/XHJcbiAgICBpZiAoIXhkIHx8ICF5ZCkge1xyXG4gICAgICByZXR1cm4gIXhzIHx8ICF5cyA/IE5hTiA6IHhzICE9PSB5cyA/IHhzIDogeGQgPT09IHlkID8gMCA6ICF4ZCBeIHhzIDwgMCA/IDEgOiAtMTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBFaXRoZXIgemVybz9cclxuICAgIGlmICgheGRbMF0gfHwgIXlkWzBdKSByZXR1cm4geGRbMF0gPyB4cyA6IHlkWzBdID8gLXlzIDogMDtcclxuXHJcbiAgICAvLyBTaWducyBkaWZmZXI/XHJcbiAgICBpZiAoeHMgIT09IHlzKSByZXR1cm4geHM7XHJcblxyXG4gICAgLy8gQ29tcGFyZSBleHBvbmVudHMuXHJcbiAgICBpZiAoeC5lICE9PSB5LmUpIHJldHVybiB4LmUgPiB5LmUgXiB4cyA8IDAgPyAxIDogLTE7XHJcblxyXG4gICAgeGRMID0geGQubGVuZ3RoO1xyXG4gICAgeWRMID0geWQubGVuZ3RoO1xyXG5cclxuICAgIC8vIENvbXBhcmUgZGlnaXQgYnkgZGlnaXQuXHJcbiAgICBmb3IgKGkgPSAwLCBqID0geGRMIDwgeWRMID8geGRMIDogeWRMOyBpIDwgajsgKytpKSB7XHJcbiAgICAgIGlmICh4ZFtpXSAhPT0geWRbaV0pIHJldHVybiB4ZFtpXSA+IHlkW2ldIF4geHMgPCAwID8gMSA6IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENvbXBhcmUgbGVuZ3Rocy5cclxuICAgIHJldHVybiB4ZEwgPT09IHlkTCA/IDAgOiB4ZEwgPiB5ZEwgXiB4cyA8IDAgPyAxIDogLTE7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGNvc2luZSBvZiB0aGUgdmFsdWUgaW4gcmFkaWFucyBvZiB0aGlzIERlY2ltYWwuXHJcbiAgICpcclxuICAgKiBEb21haW46IFstSW5maW5pdHksIEluZmluaXR5XVxyXG4gICAqIFJhbmdlOiBbLTEsIDFdXHJcbiAgICpcclxuICAgKiBjb3MoMCkgICAgICAgICA9IDFcclxuICAgKiBjb3MoLTApICAgICAgICA9IDFcclxuICAgKiBjb3MoSW5maW5pdHkpICA9IE5hTlxyXG4gICAqIGNvcygtSW5maW5pdHkpID0gTmFOXHJcbiAgICogY29zKE5hTikgICAgICAgPSBOYU5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuY29zaW5lID0gUC5jb3MgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgcHIsIHJtLFxyXG4gICAgICB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcblxyXG4gICAgaWYgKCF4LmQpIHJldHVybiBuZXcgQ3RvcihOYU4pO1xyXG5cclxuICAgIC8vIGNvcygwKSA9IGNvcygtMCkgPSAxXHJcbiAgICBpZiAoIXguZFswXSkgcmV0dXJuIG5ldyBDdG9yKDEpO1xyXG5cclxuICAgIHByID0gQ3Rvci5wcmVjaXNpb247XHJcbiAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcbiAgICBDdG9yLnByZWNpc2lvbiA9IHByICsgTWF0aC5tYXgoeC5lLCB4LnNkKCkpICsgTE9HX0JBU0U7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gMTtcclxuXHJcbiAgICB4ID0gY29zaW5lKEN0b3IsIHRvTGVzc1RoYW5IYWxmUGkoQ3RvciwgeCkpO1xyXG5cclxuICAgIEN0b3IucHJlY2lzaW9uID0gcHI7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gcm07XHJcblxyXG4gICAgcmV0dXJuIGZpbmFsaXNlKHF1YWRyYW50ID09IDIgfHwgcXVhZHJhbnQgPT0gMyA/IHgubmVnKCkgOiB4LCBwciwgcm0sIHRydWUpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGN1YmUgcm9vdCBvZiB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsLCByb3VuZGVkIHRvXHJcbiAgICogYHByZWNpc2lvbmAgc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqICBjYnJ0KDApICA9ICAwXHJcbiAgICogIGNicnQoLTApID0gLTBcclxuICAgKiAgY2JydCgxKSAgPSAgMVxyXG4gICAqICBjYnJ0KC0xKSA9IC0xXHJcbiAgICogIGNicnQoTikgID0gIE5cclxuICAgKiAgY2JydCgtSSkgPSAtSVxyXG4gICAqICBjYnJ0KEkpICA9ICBJXHJcbiAgICpcclxuICAgKiBNYXRoLmNicnQoeCkgPSAoeCA8IDAgPyAtTWF0aC5wb3coLXgsIDEvMykgOiBNYXRoLnBvdyh4LCAxLzMpKVxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5jdWJlUm9vdCA9IFAuY2JydCA9IGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBlLCBtLCBuLCByLCByZXAsIHMsIHNkLCB0LCB0MywgdDNwbHVzeCxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIGlmICgheC5pc0Zpbml0ZSgpIHx8IHguaXNaZXJvKCkpIHJldHVybiBuZXcgQ3Rvcih4KTtcclxuICAgIGV4dGVybmFsID0gZmFsc2U7XHJcblxyXG4gICAgLy8gSW5pdGlhbCBlc3RpbWF0ZS5cclxuICAgIHMgPSB4LnMgKiBNYXRoLnBvdyh4LnMgKiB4LCAxIC8gMyk7XHJcblxyXG4gICAgIC8vIE1hdGguY2JydCB1bmRlcmZsb3cvb3ZlcmZsb3c/XHJcbiAgICAgLy8gUGFzcyB4IHRvIE1hdGgucG93IGFzIGludGVnZXIsIHRoZW4gYWRqdXN0IHRoZSBleHBvbmVudCBvZiB0aGUgcmVzdWx0LlxyXG4gICAgaWYgKCFzIHx8IE1hdGguYWJzKHMpID09IDEgLyAwKSB7XHJcbiAgICAgIG4gPSBkaWdpdHNUb1N0cmluZyh4LmQpO1xyXG4gICAgICBlID0geC5lO1xyXG5cclxuICAgICAgLy8gQWRqdXN0IG4gZXhwb25lbnQgc28gaXQgaXMgYSBtdWx0aXBsZSBvZiAzIGF3YXkgZnJvbSB4IGV4cG9uZW50LlxyXG4gICAgICBpZiAocyA9IChlIC0gbi5sZW5ndGggKyAxKSAlIDMpIG4gKz0gKHMgPT0gMSB8fCBzID09IC0yID8gJzAnIDogJzAwJyk7XHJcbiAgICAgIHMgPSBNYXRoLnBvdyhuLCAxIC8gMyk7XHJcblxyXG4gICAgICAvLyBSYXJlbHksIGUgbWF5IGJlIG9uZSBsZXNzIHRoYW4gdGhlIHJlc3VsdCBleHBvbmVudCB2YWx1ZS5cclxuICAgICAgZSA9IG1hdGhmbG9vcigoZSArIDEpIC8gMykgLSAoZSAlIDMgPT0gKGUgPCAwID8gLTEgOiAyKSk7XHJcblxyXG4gICAgICBpZiAocyA9PSAxIC8gMCkge1xyXG4gICAgICAgIG4gPSAnNWUnICsgZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBuID0gcy50b0V4cG9uZW50aWFsKCk7XHJcbiAgICAgICAgbiA9IG4uc2xpY2UoMCwgbi5pbmRleE9mKCdlJykgKyAxKSArIGU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHIgPSBuZXcgQ3RvcihuKTtcclxuICAgICAgci5zID0geC5zO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgciA9IG5ldyBDdG9yKHMudG9TdHJpbmcoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2QgPSAoZSA9IEN0b3IucHJlY2lzaW9uKSArIDM7XHJcblxyXG4gICAgLy8gSGFsbGV5J3MgbWV0aG9kLlxyXG4gICAgLy8gVE9ETz8gQ29tcGFyZSBOZXd0b24ncyBtZXRob2QuXHJcbiAgICBmb3IgKDs7KSB7XHJcbiAgICAgIHQgPSByO1xyXG4gICAgICB0MyA9IHQudGltZXModCkudGltZXModCk7XHJcbiAgICAgIHQzcGx1c3ggPSB0My5wbHVzKHgpO1xyXG4gICAgICByID0gZGl2aWRlKHQzcGx1c3gucGx1cyh4KS50aW1lcyh0KSwgdDNwbHVzeC5wbHVzKHQzKSwgc2QgKyAyLCAxKTtcclxuXHJcbiAgICAgIC8vIFRPRE8/IFJlcGxhY2Ugd2l0aCBmb3ItbG9vcCBhbmQgY2hlY2tSb3VuZGluZ0RpZ2l0cy5cclxuICAgICAgaWYgKGRpZ2l0c1RvU3RyaW5nKHQuZCkuc2xpY2UoMCwgc2QpID09PSAobiA9IGRpZ2l0c1RvU3RyaW5nKHIuZCkpLnNsaWNlKDAsIHNkKSkge1xyXG4gICAgICAgIG4gPSBuLnNsaWNlKHNkIC0gMywgc2QgKyAxKTtcclxuXHJcbiAgICAgICAgLy8gVGhlIDR0aCByb3VuZGluZyBkaWdpdCBtYXkgYmUgaW4gZXJyb3IgYnkgLTEgc28gaWYgdGhlIDQgcm91bmRpbmcgZGlnaXRzIGFyZSA5OTk5IG9yIDQ5OTlcclxuICAgICAgICAvLyAsIGkuZS4gYXBwcm9hY2hpbmcgYSByb3VuZGluZyBib3VuZGFyeSwgY29udGludWUgdGhlIGl0ZXJhdGlvbi5cclxuICAgICAgICBpZiAobiA9PSAnOTk5OScgfHwgIXJlcCAmJiBuID09ICc0OTk5Jykge1xyXG5cclxuICAgICAgICAgIC8vIE9uIHRoZSBmaXJzdCBpdGVyYXRpb24gb25seSwgY2hlY2sgdG8gc2VlIGlmIHJvdW5kaW5nIHVwIGdpdmVzIHRoZSBleGFjdCByZXN1bHQgYXMgdGhlXHJcbiAgICAgICAgICAvLyBuaW5lcyBtYXkgaW5maW5pdGVseSByZXBlYXQuXHJcbiAgICAgICAgICBpZiAoIXJlcCkge1xyXG4gICAgICAgICAgICBmaW5hbGlzZSh0LCBlICsgMSwgMCk7XHJcblxyXG4gICAgICAgICAgICBpZiAodC50aW1lcyh0KS50aW1lcyh0KS5lcSh4KSkge1xyXG4gICAgICAgICAgICAgIHIgPSB0O1xyXG4gICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgc2QgKz0gNDtcclxuICAgICAgICAgIHJlcCA9IDE7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAvLyBJZiB0aGUgcm91bmRpbmcgZGlnaXRzIGFyZSBudWxsLCAwezAsNH0gb3IgNTB7MCwzfSwgY2hlY2sgZm9yIGFuIGV4YWN0IHJlc3VsdC5cclxuICAgICAgICAgIC8vIElmIG5vdCwgdGhlbiB0aGVyZSBhcmUgZnVydGhlciBkaWdpdHMgYW5kIG0gd2lsbCBiZSB0cnV0aHkuXHJcbiAgICAgICAgICBpZiAoIStuIHx8ICErbi5zbGljZSgxKSAmJiBuLmNoYXJBdCgwKSA9PSAnNScpIHtcclxuXHJcbiAgICAgICAgICAgIC8vIFRydW5jYXRlIHRvIHRoZSBmaXJzdCByb3VuZGluZyBkaWdpdC5cclxuICAgICAgICAgICAgZmluYWxpc2UociwgZSArIDEsIDEpO1xyXG4gICAgICAgICAgICBtID0gIXIudGltZXMocikudGltZXMocikuZXEoeCk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZXh0ZXJuYWwgPSB0cnVlO1xyXG5cclxuICAgIHJldHVybiBmaW5hbGlzZShyLCBlLCBDdG9yLnJvdW5kaW5nLCBtKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gdGhlIG51bWJlciBvZiBkZWNpbWFsIHBsYWNlcyBvZiB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5kZWNpbWFsUGxhY2VzID0gUC5kcCA9IGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciB3LFxyXG4gICAgICBkID0gdGhpcy5kLFxyXG4gICAgICBuID0gTmFOO1xyXG5cclxuICAgIGlmIChkKSB7XHJcbiAgICAgIHcgPSBkLmxlbmd0aCAtIDE7XHJcbiAgICAgIG4gPSAodyAtIG1hdGhmbG9vcih0aGlzLmUgLyBMT0dfQkFTRSkpICogTE9HX0JBU0U7XHJcblxyXG4gICAgICAvLyBTdWJ0cmFjdCB0aGUgbnVtYmVyIG9mIHRyYWlsaW5nIHplcm9zIG9mIHRoZSBsYXN0IHdvcmQuXHJcbiAgICAgIHcgPSBkW3ddO1xyXG4gICAgICBpZiAodykgZm9yICg7IHcgJSAxMCA9PSAwOyB3IC89IDEwKSBuLS07XHJcbiAgICAgIGlmIChuIDwgMCkgbiA9IDA7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG47XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogIG4gLyAwID0gSVxyXG4gICAqICBuIC8gTiA9IE5cclxuICAgKiAgbiAvIEkgPSAwXHJcbiAgICogIDAgLyBuID0gMFxyXG4gICAqICAwIC8gMCA9IE5cclxuICAgKiAgMCAvIE4gPSBOXHJcbiAgICogIDAgLyBJID0gMFxyXG4gICAqICBOIC8gbiA9IE5cclxuICAgKiAgTiAvIDAgPSBOXHJcbiAgICogIE4gLyBOID0gTlxyXG4gICAqICBOIC8gSSA9IE5cclxuICAgKiAgSSAvIG4gPSBJXHJcbiAgICogIEkgLyAwID0gSVxyXG4gICAqICBJIC8gTiA9IE5cclxuICAgKiAgSSAvIEkgPSBOXHJcbiAgICpcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIGRpdmlkZWQgYnkgYHlgLCByb3VuZGVkIHRvXHJcbiAgICogYHByZWNpc2lvbmAgc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuZGl2aWRlZEJ5ID0gUC5kaXYgPSBmdW5jdGlvbiAoeSkge1xyXG4gICAgcmV0dXJuIGRpdmlkZSh0aGlzLCBuZXcgdGhpcy5jb25zdHJ1Y3Rvcih5KSk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGludGVnZXIgcGFydCBvZiBkaXZpZGluZyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsXHJcbiAgICogYnkgdGhlIHZhbHVlIG9mIGB5YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYCBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5kaXZpZGVkVG9JbnRlZ2VyQnkgPSBQLmRpdlRvSW50ID0gZnVuY3Rpb24gKHkpIHtcclxuICAgIHZhciB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcbiAgICByZXR1cm4gZmluYWxpc2UoZGl2aWRlKHgsIG5ldyBDdG9yKHkpLCAwLCAxLCAxKSwgQ3Rvci5wcmVjaXNpb24sIEN0b3Iucm91bmRpbmcpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiB0cnVlIGlmIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgaXMgZXF1YWwgdG8gdGhlIHZhbHVlIG9mIGB5YCwgb3RoZXJ3aXNlIHJldHVybiBmYWxzZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuZXF1YWxzID0gUC5lcSA9IGZ1bmN0aW9uICh5KSB7XHJcbiAgICByZXR1cm4gdGhpcy5jbXAoeSkgPT09IDA7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCByb3VuZGVkIHRvIGEgd2hvbGUgbnVtYmVyIGluIHRoZVxyXG4gICAqIGRpcmVjdGlvbiBvZiBuZWdhdGl2ZSBJbmZpbml0eS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuZmxvb3IgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gZmluYWxpc2UobmV3IHRoaXMuY29uc3RydWN0b3IodGhpcyksIHRoaXMuZSArIDEsIDMpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiB0cnVlIGlmIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgaXMgZ3JlYXRlciB0aGFuIHRoZSB2YWx1ZSBvZiBgeWAsIG90aGVyd2lzZSByZXR1cm5cclxuICAgKiBmYWxzZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuZ3JlYXRlclRoYW4gPSBQLmd0ID0gZnVuY3Rpb24gKHkpIHtcclxuICAgIHJldHVybiB0aGlzLmNtcCh5KSA+IDA7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIHRydWUgaWYgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBpcyBncmVhdGVyIHRoYW4gb3IgZXF1YWwgdG8gdGhlIHZhbHVlIG9mIGB5YCxcclxuICAgKiBvdGhlcndpc2UgcmV0dXJuIGZhbHNlLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5ncmVhdGVyVGhhbk9yRXF1YWxUbyA9IFAuZ3RlID0gZnVuY3Rpb24gKHkpIHtcclxuICAgIHZhciBrID0gdGhpcy5jbXAoeSk7XHJcbiAgICByZXR1cm4gayA9PSAxIHx8IGsgPT09IDA7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGh5cGVyYm9saWMgY29zaW5lIG9mIHRoZSB2YWx1ZSBpbiByYWRpYW5zIG9mIHRoaXNcclxuICAgKiBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICogRG9tYWluOiBbLUluZmluaXR5LCBJbmZpbml0eV1cclxuICAgKiBSYW5nZTogWzEsIEluZmluaXR5XVxyXG4gICAqXHJcbiAgICogY29zaCh4KSA9IDEgKyB4XjIvMiEgKyB4XjQvNCEgKyB4XjYvNiEgKyAuLi5cclxuICAgKlxyXG4gICAqIGNvc2goMCkgICAgICAgICA9IDFcclxuICAgKiBjb3NoKC0wKSAgICAgICAgPSAxXHJcbiAgICogY29zaChJbmZpbml0eSkgID0gSW5maW5pdHlcclxuICAgKiBjb3NoKC1JbmZpbml0eSkgPSBJbmZpbml0eVxyXG4gICAqIGNvc2goTmFOKSAgICAgICA9IE5hTlxyXG4gICAqXHJcbiAgICogIHggICAgICAgIHRpbWUgdGFrZW4gKG1zKSAgIHJlc3VsdFxyXG4gICAqIDEwMDAgICAgICA5ICAgICAgICAgICAgICAgICA5Ljg1MDM1NTU3MDA4NTIzNDk2OTRlKzQzM1xyXG4gICAqIDEwMDAwICAgICAyNSAgICAgICAgICAgICAgICA0LjQwMzQwOTExMjgzMTQ2MDc5MzZlKzQzNDJcclxuICAgKiAxMDAwMDAgICAgMTcxICAgICAgICAgICAgICAgMS40MDMzMzE2ODAyMTMwNjE1ODk3ZSs0MzQyOVxyXG4gICAqIDEwMDAwMDAgICAzODE3ICAgICAgICAgICAgICAxLjUxNjYwNzY5ODQwMTA0Mzc3MjVlKzQzNDI5NFxyXG4gICAqIDEwMDAwMDAwICBhYmFuZG9uZWQgYWZ0ZXIgMiBtaW51dGUgd2FpdFxyXG4gICAqXHJcbiAgICogVE9ETz8gQ29tcGFyZSBwZXJmb3JtYW5jZSBvZiBjb3NoKHgpID0gMC41ICogKGV4cCh4KSArIGV4cCgteCkpXHJcbiAgICpcclxuICAgKi9cclxuICBQLmh5cGVyYm9saWNDb3NpbmUgPSBQLmNvc2ggPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgaywgbiwgcHIsIHJtLCBsZW4sXHJcbiAgICAgIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcixcclxuICAgICAgb25lID0gbmV3IEN0b3IoMSk7XHJcblxyXG4gICAgaWYgKCF4LmlzRmluaXRlKCkpIHJldHVybiBuZXcgQ3Rvcih4LnMgPyAxIC8gMCA6IE5hTik7XHJcbiAgICBpZiAoeC5pc1plcm8oKSkgcmV0dXJuIG9uZTtcclxuXHJcbiAgICBwciA9IEN0b3IucHJlY2lzaW9uO1xyXG4gICAgcm0gPSBDdG9yLnJvdW5kaW5nO1xyXG4gICAgQ3Rvci5wcmVjaXNpb24gPSBwciArIE1hdGgubWF4KHguZSwgeC5zZCgpKSArIDQ7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gMTtcclxuICAgIGxlbiA9IHguZC5sZW5ndGg7XHJcblxyXG4gICAgLy8gQXJndW1lbnQgcmVkdWN0aW9uOiBjb3MoNHgpID0gMSAtIDhjb3NeMih4KSArIDhjb3NeNCh4KSArIDFcclxuICAgIC8vIGkuZS4gY29zKHgpID0gMSAtIGNvc14yKHgvNCkoOCAtIDhjb3NeMih4LzQpKVxyXG5cclxuICAgIC8vIEVzdGltYXRlIHRoZSBvcHRpbXVtIG51bWJlciBvZiB0aW1lcyB0byB1c2UgdGhlIGFyZ3VtZW50IHJlZHVjdGlvbi5cclxuICAgIC8vIFRPRE8/IEVzdGltYXRpb24gcmV1c2VkIGZyb20gY29zaW5lKCkgYW5kIG1heSBub3QgYmUgb3B0aW1hbCBoZXJlLlxyXG4gICAgaWYgKGxlbiA8IDMyKSB7XHJcbiAgICAgIGsgPSBNYXRoLmNlaWwobGVuIC8gMyk7XHJcbiAgICAgIG4gPSBNYXRoLnBvdyg0LCAtaykudG9TdHJpbmcoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGsgPSAxNjtcclxuICAgICAgbiA9ICcyLjMyODMwNjQzNjUzODY5NjI4OTA2MjVlLTEwJztcclxuICAgIH1cclxuXHJcbiAgICB4ID0gdGF5bG9yU2VyaWVzKEN0b3IsIDEsIHgudGltZXMobiksIG5ldyBDdG9yKDEpLCB0cnVlKTtcclxuXHJcbiAgICAvLyBSZXZlcnNlIGFyZ3VtZW50IHJlZHVjdGlvblxyXG4gICAgdmFyIGNvc2gyX3gsXHJcbiAgICAgIGkgPSBrLFxyXG4gICAgICBkOCA9IG5ldyBDdG9yKDgpO1xyXG4gICAgZm9yICg7IGktLTspIHtcclxuICAgICAgY29zaDJfeCA9IHgudGltZXMoeCk7XHJcbiAgICAgIHggPSBvbmUubWludXMoY29zaDJfeC50aW1lcyhkOC5taW51cyhjb3NoMl94LnRpbWVzKGQ4KSkpKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZmluYWxpc2UoeCwgQ3Rvci5wcmVjaXNpb24gPSBwciwgQ3Rvci5yb3VuZGluZyA9IHJtLCB0cnVlKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgaHlwZXJib2xpYyBzaW5lIG9mIHRoZSB2YWx1ZSBpbiByYWRpYW5zIG9mIHRoaXNcclxuICAgKiBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICogRG9tYWluOiBbLUluZmluaXR5LCBJbmZpbml0eV1cclxuICAgKiBSYW5nZTogWy1JbmZpbml0eSwgSW5maW5pdHldXHJcbiAgICpcclxuICAgKiBzaW5oKHgpID0geCArIHheMy8zISArIHheNS81ISArIHheNy83ISArIC4uLlxyXG4gICAqXHJcbiAgICogc2luaCgwKSAgICAgICAgID0gMFxyXG4gICAqIHNpbmgoLTApICAgICAgICA9IC0wXHJcbiAgICogc2luaChJbmZpbml0eSkgID0gSW5maW5pdHlcclxuICAgKiBzaW5oKC1JbmZpbml0eSkgPSAtSW5maW5pdHlcclxuICAgKiBzaW5oKE5hTikgICAgICAgPSBOYU5cclxuICAgKlxyXG4gICAqIHggICAgICAgIHRpbWUgdGFrZW4gKG1zKVxyXG4gICAqIDEwICAgICAgIDIgbXNcclxuICAgKiAxMDAgICAgICA1IG1zXHJcbiAgICogMTAwMCAgICAgMTQgbXNcclxuICAgKiAxMDAwMCAgICA4MiBtc1xyXG4gICAqIDEwMDAwMCAgIDg4NiBtcyAgICAgICAgICAgIDEuNDAzMzMxNjgwMjEzMDYxNTg5N2UrNDM0MjlcclxuICAgKiAyMDAwMDAgICAyNjEzIG1zXHJcbiAgICogMzAwMDAwICAgNTQwNyBtc1xyXG4gICAqIDQwMDAwMCAgIDg4MjQgbXNcclxuICAgKiA1MDAwMDAgICAxMzAyNiBtcyAgICAgICAgICA4LjcwODA2NDM2MTI3MTgwODQxMjllKzIxNzE0NlxyXG4gICAqIDEwMDAwMDAgIDQ4NTQzIG1zXHJcbiAgICpcclxuICAgKiBUT0RPPyBDb21wYXJlIHBlcmZvcm1hbmNlIG9mIHNpbmgoeCkgPSAwLjUgKiAoZXhwKHgpIC0gZXhwKC14KSlcclxuICAgKlxyXG4gICAqL1xyXG4gIFAuaHlwZXJib2xpY1NpbmUgPSBQLnNpbmggPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgaywgcHIsIHJtLCBsZW4sXHJcbiAgICAgIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcjtcclxuXHJcbiAgICBpZiAoIXguaXNGaW5pdGUoKSB8fCB4LmlzWmVybygpKSByZXR1cm4gbmV3IEN0b3IoeCk7XHJcblxyXG4gICAgcHIgPSBDdG9yLnByZWNpc2lvbjtcclxuICAgIHJtID0gQ3Rvci5yb3VuZGluZztcclxuICAgIEN0b3IucHJlY2lzaW9uID0gcHIgKyBNYXRoLm1heCh4LmUsIHguc2QoKSkgKyA0O1xyXG4gICAgQ3Rvci5yb3VuZGluZyA9IDE7XHJcbiAgICBsZW4gPSB4LmQubGVuZ3RoO1xyXG5cclxuICAgIGlmIChsZW4gPCAzKSB7XHJcbiAgICAgIHggPSB0YXlsb3JTZXJpZXMoQ3RvciwgMiwgeCwgeCwgdHJ1ZSk7XHJcbiAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgLy8gQWx0ZXJuYXRpdmUgYXJndW1lbnQgcmVkdWN0aW9uOiBzaW5oKDN4KSA9IHNpbmgoeCkoMyArIDRzaW5oXjIoeCkpXHJcbiAgICAgIC8vIGkuZS4gc2luaCh4KSA9IHNpbmgoeC8zKSgzICsgNHNpbmheMih4LzMpKVxyXG4gICAgICAvLyAzIG11bHRpcGxpY2F0aW9ucyBhbmQgMSBhZGRpdGlvblxyXG5cclxuICAgICAgLy8gQXJndW1lbnQgcmVkdWN0aW9uOiBzaW5oKDV4KSA9IHNpbmgoeCkoNSArIHNpbmheMih4KSgyMCArIDE2c2luaF4yKHgpKSlcclxuICAgICAgLy8gaS5lLiBzaW5oKHgpID0gc2luaCh4LzUpKDUgKyBzaW5oXjIoeC81KSgyMCArIDE2c2luaF4yKHgvNSkpKVxyXG4gICAgICAvLyA0IG11bHRpcGxpY2F0aW9ucyBhbmQgMiBhZGRpdGlvbnNcclxuXHJcbiAgICAgIC8vIEVzdGltYXRlIHRoZSBvcHRpbXVtIG51bWJlciBvZiB0aW1lcyB0byB1c2UgdGhlIGFyZ3VtZW50IHJlZHVjdGlvbi5cclxuICAgICAgayA9IDEuNCAqIE1hdGguc3FydChsZW4pO1xyXG4gICAgICBrID0gayA+IDE2ID8gMTYgOiBrIHwgMDtcclxuXHJcbiAgICAgIHggPSB4LnRpbWVzKE1hdGgucG93KDUsIC1rKSk7XHJcblxyXG4gICAgICB4ID0gdGF5bG9yU2VyaWVzKEN0b3IsIDIsIHgsIHgsIHRydWUpO1xyXG5cclxuICAgICAgLy8gUmV2ZXJzZSBhcmd1bWVudCByZWR1Y3Rpb25cclxuICAgICAgdmFyIHNpbmgyX3gsXHJcbiAgICAgICAgZDUgPSBuZXcgQ3Rvcig1KSxcclxuICAgICAgICBkMTYgPSBuZXcgQ3RvcigxNiksXHJcbiAgICAgICAgZDIwID0gbmV3IEN0b3IoMjApO1xyXG4gICAgICBmb3IgKDsgay0tOykge1xyXG4gICAgICAgIHNpbmgyX3ggPSB4LnRpbWVzKHgpO1xyXG4gICAgICAgIHggPSB4LnRpbWVzKGQ1LnBsdXMoc2luaDJfeC50aW1lcyhkMTYudGltZXMoc2luaDJfeCkucGx1cyhkMjApKSkpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQ3Rvci5wcmVjaXNpb24gPSBwcjtcclxuICAgIEN0b3Iucm91bmRpbmcgPSBybTtcclxuXHJcbiAgICByZXR1cm4gZmluYWxpc2UoeCwgcHIsIHJtLCB0cnVlKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgaHlwZXJib2xpYyB0YW5nZW50IG9mIHRoZSB2YWx1ZSBpbiByYWRpYW5zIG9mIHRoaXNcclxuICAgKiBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICogRG9tYWluOiBbLUluZmluaXR5LCBJbmZpbml0eV1cclxuICAgKiBSYW5nZTogWy0xLCAxXVxyXG4gICAqXHJcbiAgICogdGFuaCh4KSA9IHNpbmgoeCkgLyBjb3NoKHgpXHJcbiAgICpcclxuICAgKiB0YW5oKDApICAgICAgICAgPSAwXHJcbiAgICogdGFuaCgtMCkgICAgICAgID0gLTBcclxuICAgKiB0YW5oKEluZmluaXR5KSAgPSAxXHJcbiAgICogdGFuaCgtSW5maW5pdHkpID0gLTFcclxuICAgKiB0YW5oKE5hTikgICAgICAgPSBOYU5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuaHlwZXJib2xpY1RhbmdlbnQgPSBQLnRhbmggPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgcHIsIHJtLFxyXG4gICAgICB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcblxyXG4gICAgaWYgKCF4LmlzRmluaXRlKCkpIHJldHVybiBuZXcgQ3Rvcih4LnMpO1xyXG4gICAgaWYgKHguaXNaZXJvKCkpIHJldHVybiBuZXcgQ3Rvcih4KTtcclxuXHJcbiAgICBwciA9IEN0b3IucHJlY2lzaW9uO1xyXG4gICAgcm0gPSBDdG9yLnJvdW5kaW5nO1xyXG4gICAgQ3Rvci5wcmVjaXNpb24gPSBwciArIDc7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gMTtcclxuXHJcbiAgICByZXR1cm4gZGl2aWRlKHguc2luaCgpLCB4LmNvc2goKSwgQ3Rvci5wcmVjaXNpb24gPSBwciwgQ3Rvci5yb3VuZGluZyA9IHJtKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgYXJjY29zaW5lIChpbnZlcnNlIGNvc2luZSkgaW4gcmFkaWFucyBvZiB0aGUgdmFsdWUgb2ZcclxuICAgKiB0aGlzIERlY2ltYWwuXHJcbiAgICpcclxuICAgKiBEb21haW46IFstMSwgMV1cclxuICAgKiBSYW5nZTogWzAsIHBpXVxyXG4gICAqXHJcbiAgICogYWNvcyh4KSA9IHBpLzIgLSBhc2luKHgpXHJcbiAgICpcclxuICAgKiBhY29zKDApICAgICAgID0gcGkvMlxyXG4gICAqIGFjb3MoLTApICAgICAgPSBwaS8yXHJcbiAgICogYWNvcygxKSAgICAgICA9IDBcclxuICAgKiBhY29zKC0xKSAgICAgID0gcGlcclxuICAgKiBhY29zKDEvMikgICAgID0gcGkvM1xyXG4gICAqIGFjb3MoLTEvMikgICAgPSAyKnBpLzNcclxuICAgKiBhY29zKHx4fCA+IDEpID0gTmFOXHJcbiAgICogYWNvcyhOYU4pICAgICA9IE5hTlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5pbnZlcnNlQ29zaW5lID0gUC5hY29zID0gZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIGhhbGZQaSxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yLFxyXG4gICAgICBrID0geC5hYnMoKS5jbXAoMSksXHJcbiAgICAgIHByID0gQ3Rvci5wcmVjaXNpb24sXHJcbiAgICAgIHJtID0gQ3Rvci5yb3VuZGluZztcclxuXHJcbiAgICBpZiAoayAhPT0gLTEpIHtcclxuICAgICAgcmV0dXJuIGsgPT09IDBcclxuICAgICAgICAvLyB8eHwgaXMgMVxyXG4gICAgICAgID8geC5pc05lZygpID8gZ2V0UGkoQ3RvciwgcHIsIHJtKSA6IG5ldyBDdG9yKDApXHJcbiAgICAgICAgLy8gfHh8ID4gMSBvciB4IGlzIE5hTlxyXG4gICAgICAgIDogbmV3IEN0b3IoTmFOKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoeC5pc1plcm8oKSkgcmV0dXJuIGdldFBpKEN0b3IsIHByICsgNCwgcm0pLnRpbWVzKDAuNSk7XHJcblxyXG4gICAgLy8gVE9ETz8gU3BlY2lhbCBjYXNlIGFjb3MoMC41KSA9IHBpLzMgYW5kIGFjb3MoLTAuNSkgPSAyKnBpLzNcclxuXHJcbiAgICBDdG9yLnByZWNpc2lvbiA9IHByICsgNjtcclxuICAgIEN0b3Iucm91bmRpbmcgPSAxO1xyXG5cclxuICAgIHggPSB4LmFzaW4oKTtcclxuICAgIGhhbGZQaSA9IGdldFBpKEN0b3IsIHByICsgNCwgcm0pLnRpbWVzKDAuNSk7XHJcblxyXG4gICAgQ3Rvci5wcmVjaXNpb24gPSBwcjtcclxuICAgIEN0b3Iucm91bmRpbmcgPSBybTtcclxuXHJcbiAgICByZXR1cm4gaGFsZlBpLm1pbnVzKHgpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBpbnZlcnNlIG9mIHRoZSBoeXBlcmJvbGljIGNvc2luZSBpbiByYWRpYW5zIG9mIHRoZVxyXG4gICAqIHZhbHVlIG9mIHRoaXMgRGVjaW1hbC5cclxuICAgKlxyXG4gICAqIERvbWFpbjogWzEsIEluZmluaXR5XVxyXG4gICAqIFJhbmdlOiBbMCwgSW5maW5pdHldXHJcbiAgICpcclxuICAgKiBhY29zaCh4KSA9IGxuKHggKyBzcXJ0KHheMiAtIDEpKVxyXG4gICAqXHJcbiAgICogYWNvc2goeCA8IDEpICAgICA9IE5hTlxyXG4gICAqIGFjb3NoKE5hTikgICAgICAgPSBOYU5cclxuICAgKiBhY29zaChJbmZpbml0eSkgID0gSW5maW5pdHlcclxuICAgKiBhY29zaCgtSW5maW5pdHkpID0gTmFOXHJcbiAgICogYWNvc2goMCkgICAgICAgICA9IE5hTlxyXG4gICAqIGFjb3NoKC0wKSAgICAgICAgPSBOYU5cclxuICAgKiBhY29zaCgxKSAgICAgICAgID0gMFxyXG4gICAqIGFjb3NoKC0xKSAgICAgICAgPSBOYU5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuaW52ZXJzZUh5cGVyYm9saWNDb3NpbmUgPSBQLmFjb3NoID0gZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIHByLCBybSxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIGlmICh4Lmx0ZSgxKSkgcmV0dXJuIG5ldyBDdG9yKHguZXEoMSkgPyAwIDogTmFOKTtcclxuICAgIGlmICgheC5pc0Zpbml0ZSgpKSByZXR1cm4gbmV3IEN0b3IoeCk7XHJcblxyXG4gICAgcHIgPSBDdG9yLnByZWNpc2lvbjtcclxuICAgIHJtID0gQ3Rvci5yb3VuZGluZztcclxuICAgIEN0b3IucHJlY2lzaW9uID0gcHIgKyBNYXRoLm1heChNYXRoLmFicyh4LmUpLCB4LnNkKCkpICsgNDtcclxuICAgIEN0b3Iucm91bmRpbmcgPSAxO1xyXG4gICAgZXh0ZXJuYWwgPSBmYWxzZTtcclxuXHJcbiAgICB4ID0geC50aW1lcyh4KS5taW51cygxKS5zcXJ0KCkucGx1cyh4KTtcclxuXHJcbiAgICBleHRlcm5hbCA9IHRydWU7XHJcbiAgICBDdG9yLnByZWNpc2lvbiA9IHByO1xyXG4gICAgQ3Rvci5yb3VuZGluZyA9IHJtO1xyXG5cclxuICAgIHJldHVybiB4LmxuKCk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGludmVyc2Ugb2YgdGhlIGh5cGVyYm9saWMgc2luZSBpbiByYWRpYW5zIG9mIHRoZSB2YWx1ZVxyXG4gICAqIG9mIHRoaXMgRGVjaW1hbC5cclxuICAgKlxyXG4gICAqIERvbWFpbjogWy1JbmZpbml0eSwgSW5maW5pdHldXHJcbiAgICogUmFuZ2U6IFstSW5maW5pdHksIEluZmluaXR5XVxyXG4gICAqXHJcbiAgICogYXNpbmgoeCkgPSBsbih4ICsgc3FydCh4XjIgKyAxKSlcclxuICAgKlxyXG4gICAqIGFzaW5oKE5hTikgICAgICAgPSBOYU5cclxuICAgKiBhc2luaChJbmZpbml0eSkgID0gSW5maW5pdHlcclxuICAgKiBhc2luaCgtSW5maW5pdHkpID0gLUluZmluaXR5XHJcbiAgICogYXNpbmgoMCkgICAgICAgICA9IDBcclxuICAgKiBhc2luaCgtMCkgICAgICAgID0gLTBcclxuICAgKlxyXG4gICAqL1xyXG4gIFAuaW52ZXJzZUh5cGVyYm9saWNTaW5lID0gUC5hc2luaCA9IGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBwciwgcm0sXHJcbiAgICAgIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcjtcclxuXHJcbiAgICBpZiAoIXguaXNGaW5pdGUoKSB8fCB4LmlzWmVybygpKSByZXR1cm4gbmV3IEN0b3IoeCk7XHJcblxyXG4gICAgcHIgPSBDdG9yLnByZWNpc2lvbjtcclxuICAgIHJtID0gQ3Rvci5yb3VuZGluZztcclxuICAgIEN0b3IucHJlY2lzaW9uID0gcHIgKyAyICogTWF0aC5tYXgoTWF0aC5hYnMoeC5lKSwgeC5zZCgpKSArIDY7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gMTtcclxuICAgIGV4dGVybmFsID0gZmFsc2U7XHJcblxyXG4gICAgeCA9IHgudGltZXMoeCkucGx1cygxKS5zcXJ0KCkucGx1cyh4KTtcclxuXHJcbiAgICBleHRlcm5hbCA9IHRydWU7XHJcbiAgICBDdG9yLnByZWNpc2lvbiA9IHByO1xyXG4gICAgQ3Rvci5yb3VuZGluZyA9IHJtO1xyXG5cclxuICAgIHJldHVybiB4LmxuKCk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGludmVyc2Ugb2YgdGhlIGh5cGVyYm9saWMgdGFuZ2VudCBpbiByYWRpYW5zIG9mIHRoZVxyXG4gICAqIHZhbHVlIG9mIHRoaXMgRGVjaW1hbC5cclxuICAgKlxyXG4gICAqIERvbWFpbjogWy0xLCAxXVxyXG4gICAqIFJhbmdlOiBbLUluZmluaXR5LCBJbmZpbml0eV1cclxuICAgKlxyXG4gICAqIGF0YW5oKHgpID0gMC41ICogbG4oKDEgKyB4KSAvICgxIC0geCkpXHJcbiAgICpcclxuICAgKiBhdGFuaCh8eHwgPiAxKSAgID0gTmFOXHJcbiAgICogYXRhbmgoTmFOKSAgICAgICA9IE5hTlxyXG4gICAqIGF0YW5oKEluZmluaXR5KSAgPSBOYU5cclxuICAgKiBhdGFuaCgtSW5maW5pdHkpID0gTmFOXHJcbiAgICogYXRhbmgoMCkgICAgICAgICA9IDBcclxuICAgKiBhdGFuaCgtMCkgICAgICAgID0gLTBcclxuICAgKiBhdGFuaCgxKSAgICAgICAgID0gSW5maW5pdHlcclxuICAgKiBhdGFuaCgtMSkgICAgICAgID0gLUluZmluaXR5XHJcbiAgICpcclxuICAgKi9cclxuICBQLmludmVyc2VIeXBlcmJvbGljVGFuZ2VudCA9IFAuYXRhbmggPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgcHIsIHJtLCB3cHIsIHhzZCxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIGlmICgheC5pc0Zpbml0ZSgpKSByZXR1cm4gbmV3IEN0b3IoTmFOKTtcclxuICAgIGlmICh4LmUgPj0gMCkgcmV0dXJuIG5ldyBDdG9yKHguYWJzKCkuZXEoMSkgPyB4LnMgLyAwIDogeC5pc1plcm8oKSA/IHggOiBOYU4pO1xyXG5cclxuICAgIHByID0gQ3Rvci5wcmVjaXNpb247XHJcbiAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcbiAgICB4c2QgPSB4LnNkKCk7XHJcblxyXG4gICAgaWYgKE1hdGgubWF4KHhzZCwgcHIpIDwgMiAqIC14LmUgLSAxKSByZXR1cm4gZmluYWxpc2UobmV3IEN0b3IoeCksIHByLCBybSwgdHJ1ZSk7XHJcblxyXG4gICAgQ3Rvci5wcmVjaXNpb24gPSB3cHIgPSB4c2QgLSB4LmU7XHJcblxyXG4gICAgeCA9IGRpdmlkZSh4LnBsdXMoMSksIG5ldyBDdG9yKDEpLm1pbnVzKHgpLCB3cHIgKyBwciwgMSk7XHJcblxyXG4gICAgQ3Rvci5wcmVjaXNpb24gPSBwciArIDQ7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gMTtcclxuXHJcbiAgICB4ID0geC5sbigpO1xyXG5cclxuICAgIEN0b3IucHJlY2lzaW9uID0gcHI7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gcm07XHJcblxyXG4gICAgcmV0dXJuIHgudGltZXMoMC41KTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgYXJjc2luZSAoaW52ZXJzZSBzaW5lKSBpbiByYWRpYW5zIG9mIHRoZSB2YWx1ZSBvZiB0aGlzXHJcbiAgICogRGVjaW1hbC5cclxuICAgKlxyXG4gICAqIERvbWFpbjogWy1JbmZpbml0eSwgSW5maW5pdHldXHJcbiAgICogUmFuZ2U6IFstcGkvMiwgcGkvMl1cclxuICAgKlxyXG4gICAqIGFzaW4oeCkgPSAyKmF0YW4oeC8oMSArIHNxcnQoMSAtIHheMikpKVxyXG4gICAqXHJcbiAgICogYXNpbigwKSAgICAgICA9IDBcclxuICAgKiBhc2luKC0wKSAgICAgID0gLTBcclxuICAgKiBhc2luKDEvMikgICAgID0gcGkvNlxyXG4gICAqIGFzaW4oLTEvMikgICAgPSAtcGkvNlxyXG4gICAqIGFzaW4oMSkgICAgICAgPSBwaS8yXHJcbiAgICogYXNpbigtMSkgICAgICA9IC1waS8yXHJcbiAgICogYXNpbih8eHwgPiAxKSA9IE5hTlxyXG4gICAqIGFzaW4oTmFOKSAgICAgPSBOYU5cclxuICAgKlxyXG4gICAqIFRPRE8/IENvbXBhcmUgcGVyZm9ybWFuY2Ugb2YgVGF5bG9yIHNlcmllcy5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuaW52ZXJzZVNpbmUgPSBQLmFzaW4gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgaGFsZlBpLCBrLFxyXG4gICAgICBwciwgcm0sXHJcbiAgICAgIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcjtcclxuXHJcbiAgICBpZiAoeC5pc1plcm8oKSkgcmV0dXJuIG5ldyBDdG9yKHgpO1xyXG5cclxuICAgIGsgPSB4LmFicygpLmNtcCgxKTtcclxuICAgIHByID0gQ3Rvci5wcmVjaXNpb247XHJcbiAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcblxyXG4gICAgaWYgKGsgIT09IC0xKSB7XHJcblxyXG4gICAgICAvLyB8eHwgaXMgMVxyXG4gICAgICBpZiAoayA9PT0gMCkge1xyXG4gICAgICAgIGhhbGZQaSA9IGdldFBpKEN0b3IsIHByICsgNCwgcm0pLnRpbWVzKDAuNSk7XHJcbiAgICAgICAgaGFsZlBpLnMgPSB4LnM7XHJcbiAgICAgICAgcmV0dXJuIGhhbGZQaTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gfHh8ID4gMSBvciB4IGlzIE5hTlxyXG4gICAgICByZXR1cm4gbmV3IEN0b3IoTmFOKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUT0RPPyBTcGVjaWFsIGNhc2UgYXNpbigxLzIpID0gcGkvNiBhbmQgYXNpbigtMS8yKSA9IC1waS82XHJcblxyXG4gICAgQ3Rvci5wcmVjaXNpb24gPSBwciArIDY7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gMTtcclxuXHJcbiAgICB4ID0geC5kaXYobmV3IEN0b3IoMSkubWludXMoeC50aW1lcyh4KSkuc3FydCgpLnBsdXMoMSkpLmF0YW4oKTtcclxuXHJcbiAgICBDdG9yLnByZWNpc2lvbiA9IHByO1xyXG4gICAgQ3Rvci5yb3VuZGluZyA9IHJtO1xyXG5cclxuICAgIHJldHVybiB4LnRpbWVzKDIpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBhcmN0YW5nZW50IChpbnZlcnNlIHRhbmdlbnQpIGluIHJhZGlhbnMgb2YgdGhlIHZhbHVlXHJcbiAgICogb2YgdGhpcyBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICogRG9tYWluOiBbLUluZmluaXR5LCBJbmZpbml0eV1cclxuICAgKiBSYW5nZTogWy1waS8yLCBwaS8yXVxyXG4gICAqXHJcbiAgICogYXRhbih4KSA9IHggLSB4XjMvMyArIHheNS81IC0geF43LzcgKyAuLi5cclxuICAgKlxyXG4gICAqIGF0YW4oMCkgICAgICAgICA9IDBcclxuICAgKiBhdGFuKC0wKSAgICAgICAgPSAtMFxyXG4gICAqIGF0YW4oMSkgICAgICAgICA9IHBpLzRcclxuICAgKiBhdGFuKC0xKSAgICAgICAgPSAtcGkvNFxyXG4gICAqIGF0YW4oSW5maW5pdHkpICA9IHBpLzJcclxuICAgKiBhdGFuKC1JbmZpbml0eSkgPSAtcGkvMlxyXG4gICAqIGF0YW4oTmFOKSAgICAgICA9IE5hTlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5pbnZlcnNlVGFuZ2VudCA9IFAuYXRhbiA9IGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBpLCBqLCBrLCBuLCBweCwgdCwgciwgd3ByLCB4MixcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yLFxyXG4gICAgICBwciA9IEN0b3IucHJlY2lzaW9uLFxyXG4gICAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcblxyXG4gICAgaWYgKCF4LmlzRmluaXRlKCkpIHtcclxuICAgICAgaWYgKCF4LnMpIHJldHVybiBuZXcgQ3RvcihOYU4pO1xyXG4gICAgICBpZiAocHIgKyA0IDw9IFBJX1BSRUNJU0lPTikge1xyXG4gICAgICAgIHIgPSBnZXRQaShDdG9yLCBwciArIDQsIHJtKS50aW1lcygwLjUpO1xyXG4gICAgICAgIHIucyA9IHgucztcclxuICAgICAgICByZXR1cm4gcjtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIGlmICh4LmlzWmVybygpKSB7XHJcbiAgICAgIHJldHVybiBuZXcgQ3Rvcih4KTtcclxuICAgIH0gZWxzZSBpZiAoeC5hYnMoKS5lcSgxKSAmJiBwciArIDQgPD0gUElfUFJFQ0lTSU9OKSB7XHJcbiAgICAgIHIgPSBnZXRQaShDdG9yLCBwciArIDQsIHJtKS50aW1lcygwLjI1KTtcclxuICAgICAgci5zID0geC5zO1xyXG4gICAgICByZXR1cm4gcjtcclxuICAgIH1cclxuXHJcbiAgICBDdG9yLnByZWNpc2lvbiA9IHdwciA9IHByICsgMTA7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gMTtcclxuXHJcbiAgICAvLyBUT0RPPyBpZiAoeCA+PSAxICYmIHByIDw9IFBJX1BSRUNJU0lPTikgYXRhbih4KSA9IGhhbGZQaSAqIHgucyAtIGF0YW4oMSAvIHgpO1xyXG5cclxuICAgIC8vIEFyZ3VtZW50IHJlZHVjdGlvblxyXG4gICAgLy8gRW5zdXJlIHx4fCA8IDAuNDJcclxuICAgIC8vIGF0YW4oeCkgPSAyICogYXRhbih4IC8gKDEgKyBzcXJ0KDEgKyB4XjIpKSlcclxuXHJcbiAgICBrID0gTWF0aC5taW4oMjgsIHdwciAvIExPR19CQVNFICsgMiB8IDApO1xyXG5cclxuICAgIGZvciAoaSA9IGs7IGk7IC0taSkgeCA9IHguZGl2KHgudGltZXMoeCkucGx1cygxKS5zcXJ0KCkucGx1cygxKSk7XHJcblxyXG4gICAgZXh0ZXJuYWwgPSBmYWxzZTtcclxuXHJcbiAgICBqID0gTWF0aC5jZWlsKHdwciAvIExPR19CQVNFKTtcclxuICAgIG4gPSAxO1xyXG4gICAgeDIgPSB4LnRpbWVzKHgpO1xyXG4gICAgciA9IG5ldyBDdG9yKHgpO1xyXG4gICAgcHggPSB4O1xyXG5cclxuICAgIC8vIGF0YW4oeCkgPSB4IC0geF4zLzMgKyB4XjUvNSAtIHheNy83ICsgLi4uXHJcbiAgICBmb3IgKDsgaSAhPT0gLTE7KSB7XHJcbiAgICAgIHB4ID0gcHgudGltZXMoeDIpO1xyXG4gICAgICB0ID0gci5taW51cyhweC5kaXYobiArPSAyKSk7XHJcblxyXG4gICAgICBweCA9IHB4LnRpbWVzKHgyKTtcclxuICAgICAgciA9IHQucGx1cyhweC5kaXYobiArPSAyKSk7XHJcblxyXG4gICAgICBpZiAoci5kW2pdICE9PSB2b2lkIDApIGZvciAoaSA9IGo7IHIuZFtpXSA9PT0gdC5kW2ldICYmIGktLTspO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChrKSByID0gci50aW1lcygyIDw8IChrIC0gMSkpO1xyXG5cclxuICAgIGV4dGVybmFsID0gdHJ1ZTtcclxuXHJcbiAgICByZXR1cm4gZmluYWxpc2UociwgQ3Rvci5wcmVjaXNpb24gPSBwciwgQ3Rvci5yb3VuZGluZyA9IHJtLCB0cnVlKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gdHJ1ZSBpZiB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIGlzIGEgZmluaXRlIG51bWJlciwgb3RoZXJ3aXNlIHJldHVybiBmYWxzZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuaXNGaW5pdGUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gISF0aGlzLmQ7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIHRydWUgaWYgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBpcyBhbiBpbnRlZ2VyLCBvdGhlcndpc2UgcmV0dXJuIGZhbHNlLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5pc0ludGVnZXIgPSBQLmlzSW50ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgcmV0dXJuICEhdGhpcy5kICYmIG1hdGhmbG9vcih0aGlzLmUgLyBMT0dfQkFTRSkgPiB0aGlzLmQubGVuZ3RoIC0gMjtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gdHJ1ZSBpZiB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIGlzIE5hTiwgb3RoZXJ3aXNlIHJldHVybiBmYWxzZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuaXNOYU4gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gIXRoaXMucztcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gdHJ1ZSBpZiB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIGlzIG5lZ2F0aXZlLCBvdGhlcndpc2UgcmV0dXJuIGZhbHNlLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5pc05lZ2F0aXZlID0gUC5pc05lZyA9IGZ1bmN0aW9uICgpIHtcclxuICAgIHJldHVybiB0aGlzLnMgPCAwO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiB0cnVlIGlmIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgaXMgcG9zaXRpdmUsIG90aGVyd2lzZSByZXR1cm4gZmFsc2UuXHJcbiAgICpcclxuICAgKi9cclxuICBQLmlzUG9zaXRpdmUgPSBQLmlzUG9zID0gZnVuY3Rpb24gKCkge1xyXG4gICAgcmV0dXJuIHRoaXMucyA+IDA7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIHRydWUgaWYgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBpcyAwIG9yIC0wLCBvdGhlcndpc2UgcmV0dXJuIGZhbHNlLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5pc1plcm8gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gISF0aGlzLmQgJiYgdGhpcy5kWzBdID09PSAwO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiB0cnVlIGlmIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgaXMgbGVzcyB0aGFuIGB5YCwgb3RoZXJ3aXNlIHJldHVybiBmYWxzZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAubGVzc1RoYW4gPSBQLmx0ID0gZnVuY3Rpb24gKHkpIHtcclxuICAgIHJldHVybiB0aGlzLmNtcCh5KSA8IDA7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIHRydWUgaWYgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBpcyBsZXNzIHRoYW4gb3IgZXF1YWwgdG8gYHlgLCBvdGhlcndpc2UgcmV0dXJuIGZhbHNlLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5sZXNzVGhhbk9yRXF1YWxUbyA9IFAubHRlID0gZnVuY3Rpb24gKHkpIHtcclxuICAgIHJldHVybiB0aGlzLmNtcCh5KSA8IDE7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIHRoZSBsb2dhcml0aG0gb2YgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCB0byB0aGUgc3BlY2lmaWVkIGJhc2UsIHJvdW5kZWQgdG8gYHByZWNpc2lvbmBcclxuICAgKiBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogSWYgbm8gYmFzZSBpcyBzcGVjaWZpZWQsIHJldHVybiBsb2dbMTBdKGFyZykuXHJcbiAgICpcclxuICAgKiBsb2dbYmFzZV0oYXJnKSA9IGxuKGFyZykgLyBsbihiYXNlKVxyXG4gICAqXHJcbiAgICogVGhlIHJlc3VsdCB3aWxsIGFsd2F5cyBiZSBjb3JyZWN0bHkgcm91bmRlZCBpZiB0aGUgYmFzZSBvZiB0aGUgbG9nIGlzIDEwLCBhbmQgJ2FsbW9zdCBhbHdheXMnXHJcbiAgICogb3RoZXJ3aXNlOlxyXG4gICAqXHJcbiAgICogRGVwZW5kaW5nIG9uIHRoZSByb3VuZGluZyBtb2RlLCB0aGUgcmVzdWx0IG1heSBiZSBpbmNvcnJlY3RseSByb3VuZGVkIGlmIHRoZSBmaXJzdCBmaWZ0ZWVuXHJcbiAgICogcm91bmRpbmcgZGlnaXRzIGFyZSBbNDldOTk5OTk5OTk5OTk5OTkgb3IgWzUwXTAwMDAwMDAwMDAwMDAwLiBJbiB0aGF0IGNhc2UsIHRoZSBtYXhpbXVtIGVycm9yXHJcbiAgICogYmV0d2VlbiB0aGUgcmVzdWx0IGFuZCB0aGUgY29ycmVjdGx5IHJvdW5kZWQgcmVzdWx0IHdpbGwgYmUgb25lIHVscCAodW5pdCBpbiB0aGUgbGFzdCBwbGFjZSkuXHJcbiAgICpcclxuICAgKiBsb2dbLWJdKGEpICAgICAgID0gTmFOXHJcbiAgICogbG9nWzBdKGEpICAgICAgICA9IE5hTlxyXG4gICAqIGxvZ1sxXShhKSAgICAgICAgPSBOYU5cclxuICAgKiBsb2dbTmFOXShhKSAgICAgID0gTmFOXHJcbiAgICogbG9nW0luZmluaXR5XShhKSA9IE5hTlxyXG4gICAqIGxvZ1tiXSgwKSAgICAgICAgPSAtSW5maW5pdHlcclxuICAgKiBsb2dbYl0oLTApICAgICAgID0gLUluZmluaXR5XHJcbiAgICogbG9nW2JdKC1hKSAgICAgICA9IE5hTlxyXG4gICAqIGxvZ1tiXSgxKSAgICAgICAgPSAwXHJcbiAgICogbG9nW2JdKEluZmluaXR5KSA9IEluZmluaXR5XHJcbiAgICogbG9nW2JdKE5hTikgICAgICA9IE5hTlxyXG4gICAqXHJcbiAgICogW2Jhc2VdIHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IFRoZSBiYXNlIG9mIHRoZSBsb2dhcml0aG0uXHJcbiAgICpcclxuICAgKi9cclxuICBQLmxvZ2FyaXRobSA9IFAubG9nID0gZnVuY3Rpb24gKGJhc2UpIHtcclxuICAgIHZhciBpc0Jhc2UxMCwgZCwgZGVub21pbmF0b3IsIGssIGluZiwgbnVtLCBzZCwgcixcclxuICAgICAgYXJnID0gdGhpcyxcclxuICAgICAgQ3RvciA9IGFyZy5jb25zdHJ1Y3RvcixcclxuICAgICAgcHIgPSBDdG9yLnByZWNpc2lvbixcclxuICAgICAgcm0gPSBDdG9yLnJvdW5kaW5nLFxyXG4gICAgICBndWFyZCA9IDU7XHJcblxyXG4gICAgLy8gRGVmYXVsdCBiYXNlIGlzIDEwLlxyXG4gICAgaWYgKGJhc2UgPT0gbnVsbCkge1xyXG4gICAgICBiYXNlID0gbmV3IEN0b3IoMTApO1xyXG4gICAgICBpc0Jhc2UxMCA9IHRydWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBiYXNlID0gbmV3IEN0b3IoYmFzZSk7XHJcbiAgICAgIGQgPSBiYXNlLmQ7XHJcblxyXG4gICAgICAvLyBSZXR1cm4gTmFOIGlmIGJhc2UgaXMgbmVnYXRpdmUsIG9yIG5vbi1maW5pdGUsIG9yIGlzIDAgb3IgMS5cclxuICAgICAgaWYgKGJhc2UucyA8IDAgfHwgIWQgfHwgIWRbMF0gfHwgYmFzZS5lcSgxKSkgcmV0dXJuIG5ldyBDdG9yKE5hTik7XHJcblxyXG4gICAgICBpc0Jhc2UxMCA9IGJhc2UuZXEoMTApO1xyXG4gICAgfVxyXG5cclxuICAgIGQgPSBhcmcuZDtcclxuXHJcbiAgICAvLyBJcyBhcmcgbmVnYXRpdmUsIG5vbi1maW5pdGUsIDAgb3IgMT9cclxuICAgIGlmIChhcmcucyA8IDAgfHwgIWQgfHwgIWRbMF0gfHwgYXJnLmVxKDEpKSB7XHJcbiAgICAgIHJldHVybiBuZXcgQ3RvcihkICYmICFkWzBdID8gLTEgLyAwIDogYXJnLnMgIT0gMSA/IE5hTiA6IGQgPyAwIDogMSAvIDApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRoZSByZXN1bHQgd2lsbCBoYXZlIGEgbm9uLXRlcm1pbmF0aW5nIGRlY2ltYWwgZXhwYW5zaW9uIGlmIGJhc2UgaXMgMTAgYW5kIGFyZyBpcyBub3QgYW5cclxuICAgIC8vIGludGVnZXIgcG93ZXIgb2YgMTAuXHJcbiAgICBpZiAoaXNCYXNlMTApIHtcclxuICAgICAgaWYgKGQubGVuZ3RoID4gMSkge1xyXG4gICAgICAgIGluZiA9IHRydWU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZm9yIChrID0gZFswXTsgayAlIDEwID09PSAwOykgayAvPSAxMDtcclxuICAgICAgICBpbmYgPSBrICE9PSAxO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZXh0ZXJuYWwgPSBmYWxzZTtcclxuICAgIHNkID0gcHIgKyBndWFyZDtcclxuICAgIG51bSA9IG5hdHVyYWxMb2dhcml0aG0oYXJnLCBzZCk7XHJcbiAgICBkZW5vbWluYXRvciA9IGlzQmFzZTEwID8gZ2V0TG4xMChDdG9yLCBzZCArIDEwKSA6IG5hdHVyYWxMb2dhcml0aG0oYmFzZSwgc2QpO1xyXG5cclxuICAgIC8vIFRoZSByZXN1bHQgd2lsbCBoYXZlIDUgcm91bmRpbmcgZGlnaXRzLlxyXG4gICAgciA9IGRpdmlkZShudW0sIGRlbm9taW5hdG9yLCBzZCwgMSk7XHJcblxyXG4gICAgLy8gSWYgYXQgYSByb3VuZGluZyBib3VuZGFyeSwgaS5lLiB0aGUgcmVzdWx0J3Mgcm91bmRpbmcgZGlnaXRzIGFyZSBbNDldOTk5OSBvciBbNTBdMDAwMCxcclxuICAgIC8vIGNhbGN1bGF0ZSAxMCBmdXJ0aGVyIGRpZ2l0cy5cclxuICAgIC8vXHJcbiAgICAvLyBJZiB0aGUgcmVzdWx0IGlzIGtub3duIHRvIGhhdmUgYW4gaW5maW5pdGUgZGVjaW1hbCBleHBhbnNpb24sIHJlcGVhdCB0aGlzIHVudGlsIGl0IGlzIGNsZWFyXHJcbiAgICAvLyB0aGF0IHRoZSByZXN1bHQgaXMgYWJvdmUgb3IgYmVsb3cgdGhlIGJvdW5kYXJ5LiBPdGhlcndpc2UsIGlmIGFmdGVyIGNhbGN1bGF0aW5nIHRoZSAxMFxyXG4gICAgLy8gZnVydGhlciBkaWdpdHMsIHRoZSBsYXN0IDE0IGFyZSBuaW5lcywgcm91bmQgdXAgYW5kIGFzc3VtZSB0aGUgcmVzdWx0IGlzIGV4YWN0LlxyXG4gICAgLy8gQWxzbyBhc3N1bWUgdGhlIHJlc3VsdCBpcyBleGFjdCBpZiB0aGUgbGFzdCAxNCBhcmUgemVyby5cclxuICAgIC8vXHJcbiAgICAvLyBFeGFtcGxlIG9mIGEgcmVzdWx0IHRoYXQgd2lsbCBiZSBpbmNvcnJlY3RseSByb3VuZGVkOlxyXG4gICAgLy8gbG9nWzEwNDg1NzZdKDQ1MDM1OTk2MjczNzA1MDIpID0gMi42MDAwMDAwMDAwMDAwMDAwOTYxMDI3OTUxMTQ0NDc0Ni4uLlxyXG4gICAgLy8gVGhlIGFib3ZlIHJlc3VsdCBjb3JyZWN0bHkgcm91bmRlZCB1c2luZyBST1VORF9DRUlMIHRvIDEgZGVjaW1hbCBwbGFjZSBzaG91bGQgYmUgMi43LCBidXQgaXRcclxuICAgIC8vIHdpbGwgYmUgZ2l2ZW4gYXMgMi42IGFzIHRoZXJlIGFyZSAxNSB6ZXJvcyBpbW1lZGlhdGVseSBhZnRlciB0aGUgcmVxdWVzdGVkIGRlY2ltYWwgcGxhY2UsIHNvXHJcbiAgICAvLyB0aGUgZXhhY3QgcmVzdWx0IHdvdWxkIGJlIGFzc3VtZWQgdG8gYmUgMi42LCB3aGljaCByb3VuZGVkIHVzaW5nIFJPVU5EX0NFSUwgdG8gMSBkZWNpbWFsXHJcbiAgICAvLyBwbGFjZSBpcyBzdGlsbCAyLjYuXHJcbiAgICBpZiAoY2hlY2tSb3VuZGluZ0RpZ2l0cyhyLmQsIGsgPSBwciwgcm0pKSB7XHJcblxyXG4gICAgICBkbyB7XHJcbiAgICAgICAgc2QgKz0gMTA7XHJcbiAgICAgICAgbnVtID0gbmF0dXJhbExvZ2FyaXRobShhcmcsIHNkKTtcclxuICAgICAgICBkZW5vbWluYXRvciA9IGlzQmFzZTEwID8gZ2V0TG4xMChDdG9yLCBzZCArIDEwKSA6IG5hdHVyYWxMb2dhcml0aG0oYmFzZSwgc2QpO1xyXG4gICAgICAgIHIgPSBkaXZpZGUobnVtLCBkZW5vbWluYXRvciwgc2QsIDEpO1xyXG5cclxuICAgICAgICBpZiAoIWluZikge1xyXG5cclxuICAgICAgICAgIC8vIENoZWNrIGZvciAxNCBuaW5lcyBmcm9tIHRoZSAybmQgcm91bmRpbmcgZGlnaXQsIGFzIHRoZSBmaXJzdCBtYXkgYmUgNC5cclxuICAgICAgICAgIGlmICgrZGlnaXRzVG9TdHJpbmcoci5kKS5zbGljZShrICsgMSwgayArIDE1KSArIDEgPT0gMWUxNCkge1xyXG4gICAgICAgICAgICByID0gZmluYWxpc2UociwgcHIgKyAxLCAwKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgIH0gd2hpbGUgKGNoZWNrUm91bmRpbmdEaWdpdHMoci5kLCBrICs9IDEwLCBybSkpO1xyXG4gICAgfVxyXG5cclxuICAgIGV4dGVybmFsID0gdHJ1ZTtcclxuXHJcbiAgICByZXR1cm4gZmluYWxpc2UociwgcHIsIHJtKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgbWF4aW11bSBvZiB0aGUgYXJndW1lbnRzIGFuZCB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICogYXJndW1lbnRzIHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICpcclxuICBQLm1heCA9IGZ1bmN0aW9uICgpIHtcclxuICAgIEFycmF5LnByb3RvdHlwZS5wdXNoLmNhbGwoYXJndW1lbnRzLCB0aGlzKTtcclxuICAgIHJldHVybiBtYXhPck1pbih0aGlzLmNvbnN0cnVjdG9yLCBhcmd1bWVudHMsICdsdCcpO1xyXG4gIH07XHJcbiAgICovXHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBtaW5pbXVtIG9mIHRoZSBhcmd1bWVudHMgYW5kIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwuXHJcbiAgICpcclxuICAgKiBhcmd1bWVudHMge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gIFAubWluID0gZnVuY3Rpb24gKCkge1xyXG4gICAgQXJyYXkucHJvdG90eXBlLnB1c2guY2FsbChhcmd1bWVudHMsIHRoaXMpO1xyXG4gICAgcmV0dXJuIG1heE9yTWluKHRoaXMuY29uc3RydWN0b3IsIGFyZ3VtZW50cywgJ2d0Jyk7XHJcbiAgfTtcclxuICAgKi9cclxuXHJcblxyXG4gIC8qXHJcbiAgICogIG4gLSAwID0gblxyXG4gICAqICBuIC0gTiA9IE5cclxuICAgKiAgbiAtIEkgPSAtSVxyXG4gICAqICAwIC0gbiA9IC1uXHJcbiAgICogIDAgLSAwID0gMFxyXG4gICAqICAwIC0gTiA9IE5cclxuICAgKiAgMCAtIEkgPSAtSVxyXG4gICAqICBOIC0gbiA9IE5cclxuICAgKiAgTiAtIDAgPSBOXHJcbiAgICogIE4gLSBOID0gTlxyXG4gICAqICBOIC0gSSA9IE5cclxuICAgKiAgSSAtIG4gPSBJXHJcbiAgICogIEkgLSAwID0gSVxyXG4gICAqICBJIC0gTiA9IE5cclxuICAgKiAgSSAtIEkgPSBOXHJcbiAgICpcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIG1pbnVzIGB5YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYFxyXG4gICAqIHNpZ25pZmljYW50IGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKi9cclxuICBQLm1pbnVzID0gUC5zdWIgPSBmdW5jdGlvbiAoeSkge1xyXG4gICAgdmFyIGQsIGUsIGksIGosIGssIGxlbiwgcHIsIHJtLCB4ZCwgeGUsIHhMVHksIHlkLFxyXG4gICAgICB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcblxyXG4gICAgeSA9IG5ldyBDdG9yKHkpO1xyXG5cclxuICAgIC8vIElmIGVpdGhlciBpcyBub3QgZmluaXRlLi4uXHJcbiAgICBpZiAoIXguZCB8fCAheS5kKSB7XHJcblxyXG4gICAgICAvLyBSZXR1cm4gTmFOIGlmIGVpdGhlciBpcyBOYU4uXHJcbiAgICAgIGlmICgheC5zIHx8ICF5LnMpIHkgPSBuZXcgQ3RvcihOYU4pO1xyXG5cclxuICAgICAgLy8gUmV0dXJuIHkgbmVnYXRlZCBpZiB4IGlzIGZpbml0ZSBhbmQgeSBpcyDCsUluZmluaXR5LlxyXG4gICAgICBlbHNlIGlmICh4LmQpIHkucyA9IC15LnM7XHJcblxyXG4gICAgICAvLyBSZXR1cm4geCBpZiB5IGlzIGZpbml0ZSBhbmQgeCBpcyDCsUluZmluaXR5LlxyXG4gICAgICAvLyBSZXR1cm4geCBpZiBib3RoIGFyZSDCsUluZmluaXR5IHdpdGggZGlmZmVyZW50IHNpZ25zLlxyXG4gICAgICAvLyBSZXR1cm4gTmFOIGlmIGJvdGggYXJlIMKxSW5maW5pdHkgd2l0aCB0aGUgc2FtZSBzaWduLlxyXG4gICAgICBlbHNlIHkgPSBuZXcgQ3Rvcih5LmQgfHwgeC5zICE9PSB5LnMgPyB4IDogTmFOKTtcclxuXHJcbiAgICAgIHJldHVybiB5O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIElmIHNpZ25zIGRpZmZlci4uLlxyXG4gICAgaWYgKHgucyAhPSB5LnMpIHtcclxuICAgICAgeS5zID0gLXkucztcclxuICAgICAgcmV0dXJuIHgucGx1cyh5KTtcclxuICAgIH1cclxuXHJcbiAgICB4ZCA9IHguZDtcclxuICAgIHlkID0geS5kO1xyXG4gICAgcHIgPSBDdG9yLnByZWNpc2lvbjtcclxuICAgIHJtID0gQ3Rvci5yb3VuZGluZztcclxuXHJcbiAgICAvLyBJZiBlaXRoZXIgaXMgemVyby4uLlxyXG4gICAgaWYgKCF4ZFswXSB8fCAheWRbMF0pIHtcclxuXHJcbiAgICAgIC8vIFJldHVybiB5IG5lZ2F0ZWQgaWYgeCBpcyB6ZXJvIGFuZCB5IGlzIG5vbi16ZXJvLlxyXG4gICAgICBpZiAoeWRbMF0pIHkucyA9IC15LnM7XHJcblxyXG4gICAgICAvLyBSZXR1cm4geCBpZiB5IGlzIHplcm8gYW5kIHggaXMgbm9uLXplcm8uXHJcbiAgICAgIGVsc2UgaWYgKHhkWzBdKSB5ID0gbmV3IEN0b3IoeCk7XHJcblxyXG4gICAgICAvLyBSZXR1cm4gemVybyBpZiBib3RoIGFyZSB6ZXJvLlxyXG4gICAgICAvLyBGcm9tIElFRUUgNzU0ICgyMDA4KSA2LjM6IDAgLSAwID0gLTAgLSAtMCA9IC0wIHdoZW4gcm91bmRpbmcgdG8gLUluZmluaXR5LlxyXG4gICAgICBlbHNlIHJldHVybiBuZXcgQ3RvcihybSA9PT0gMyA/IC0wIDogMCk7XHJcblxyXG4gICAgICByZXR1cm4gZXh0ZXJuYWwgPyBmaW5hbGlzZSh5LCBwciwgcm0pIDogeTtcclxuICAgIH1cclxuXHJcbiAgICAvLyB4IGFuZCB5IGFyZSBmaW5pdGUsIG5vbi16ZXJvIG51bWJlcnMgd2l0aCB0aGUgc2FtZSBzaWduLlxyXG5cclxuICAgIC8vIENhbGN1bGF0ZSBiYXNlIDFlNyBleHBvbmVudHMuXHJcbiAgICBlID0gbWF0aGZsb29yKHkuZSAvIExPR19CQVNFKTtcclxuICAgIHhlID0gbWF0aGZsb29yKHguZSAvIExPR19CQVNFKTtcclxuXHJcbiAgICB4ZCA9IHhkLnNsaWNlKCk7XHJcbiAgICBrID0geGUgLSBlO1xyXG5cclxuICAgIC8vIElmIGJhc2UgMWU3IGV4cG9uZW50cyBkaWZmZXIuLi5cclxuICAgIGlmIChrKSB7XHJcbiAgICAgIHhMVHkgPSBrIDwgMDtcclxuXHJcbiAgICAgIGlmICh4TFR5KSB7XHJcbiAgICAgICAgZCA9IHhkO1xyXG4gICAgICAgIGsgPSAtaztcclxuICAgICAgICBsZW4gPSB5ZC5sZW5ndGg7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZCA9IHlkO1xyXG4gICAgICAgIGUgPSB4ZTtcclxuICAgICAgICBsZW4gPSB4ZC5sZW5ndGg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIE51bWJlcnMgd2l0aCBtYXNzaXZlbHkgZGlmZmVyZW50IGV4cG9uZW50cyB3b3VsZCByZXN1bHQgaW4gYSB2ZXJ5IGhpZ2ggbnVtYmVyIG9mXHJcbiAgICAgIC8vIHplcm9zIG5lZWRpbmcgdG8gYmUgcHJlcGVuZGVkLCBidXQgdGhpcyBjYW4gYmUgYXZvaWRlZCB3aGlsZSBzdGlsbCBlbnN1cmluZyBjb3JyZWN0XHJcbiAgICAgIC8vIHJvdW5kaW5nIGJ5IGxpbWl0aW5nIHRoZSBudW1iZXIgb2YgemVyb3MgdG8gYE1hdGguY2VpbChwciAvIExPR19CQVNFKSArIDJgLlxyXG4gICAgICBpID0gTWF0aC5tYXgoTWF0aC5jZWlsKHByIC8gTE9HX0JBU0UpLCBsZW4pICsgMjtcclxuXHJcbiAgICAgIGlmIChrID4gaSkge1xyXG4gICAgICAgIGsgPSBpO1xyXG4gICAgICAgIGQubGVuZ3RoID0gMTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gUHJlcGVuZCB6ZXJvcyB0byBlcXVhbGlzZSBleHBvbmVudHMuXHJcbiAgICAgIGQucmV2ZXJzZSgpO1xyXG4gICAgICBmb3IgKGkgPSBrOyBpLS07KSBkLnB1c2goMCk7XHJcbiAgICAgIGQucmV2ZXJzZSgpO1xyXG5cclxuICAgIC8vIEJhc2UgMWU3IGV4cG9uZW50cyBlcXVhbC5cclxuICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAvLyBDaGVjayBkaWdpdHMgdG8gZGV0ZXJtaW5lIHdoaWNoIGlzIHRoZSBiaWdnZXIgbnVtYmVyLlxyXG5cclxuICAgICAgaSA9IHhkLmxlbmd0aDtcclxuICAgICAgbGVuID0geWQubGVuZ3RoO1xyXG4gICAgICB4TFR5ID0gaSA8IGxlbjtcclxuICAgICAgaWYgKHhMVHkpIGxlbiA9IGk7XHJcblxyXG4gICAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcclxuICAgICAgICBpZiAoeGRbaV0gIT0geWRbaV0pIHtcclxuICAgICAgICAgIHhMVHkgPSB4ZFtpXSA8IHlkW2ldO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBrID0gMDtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoeExUeSkge1xyXG4gICAgICBkID0geGQ7XHJcbiAgICAgIHhkID0geWQ7XHJcbiAgICAgIHlkID0gZDtcclxuICAgICAgeS5zID0gLXkucztcclxuICAgIH1cclxuXHJcbiAgICBsZW4gPSB4ZC5sZW5ndGg7XHJcblxyXG4gICAgLy8gQXBwZW5kIHplcm9zIHRvIGB4ZGAgaWYgc2hvcnRlci5cclxuICAgIC8vIERvbid0IGFkZCB6ZXJvcyB0byBgeWRgIGlmIHNob3J0ZXIgYXMgc3VidHJhY3Rpb24gb25seSBuZWVkcyB0byBzdGFydCBhdCBgeWRgIGxlbmd0aC5cclxuICAgIGZvciAoaSA9IHlkLmxlbmd0aCAtIGxlbjsgaSA+IDA7IC0taSkgeGRbbGVuKytdID0gMDtcclxuXHJcbiAgICAvLyBTdWJ0cmFjdCB5ZCBmcm9tIHhkLlxyXG4gICAgZm9yIChpID0geWQubGVuZ3RoOyBpID4gazspIHtcclxuXHJcbiAgICAgIGlmICh4ZFstLWldIDwgeWRbaV0pIHtcclxuICAgICAgICBmb3IgKGogPSBpOyBqICYmIHhkWy0tal0gPT09IDA7KSB4ZFtqXSA9IEJBU0UgLSAxO1xyXG4gICAgICAgIC0teGRbal07XHJcbiAgICAgICAgeGRbaV0gKz0gQkFTRTtcclxuICAgICAgfVxyXG5cclxuICAgICAgeGRbaV0gLT0geWRbaV07XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUmVtb3ZlIHRyYWlsaW5nIHplcm9zLlxyXG4gICAgZm9yICg7IHhkWy0tbGVuXSA9PT0gMDspIHhkLnBvcCgpO1xyXG5cclxuICAgIC8vIFJlbW92ZSBsZWFkaW5nIHplcm9zIGFuZCBhZGp1c3QgZXhwb25lbnQgYWNjb3JkaW5nbHkuXHJcbiAgICBmb3IgKDsgeGRbMF0gPT09IDA7IHhkLnNoaWZ0KCkpIC0tZTtcclxuXHJcbiAgICAvLyBaZXJvP1xyXG4gICAgaWYgKCF4ZFswXSkgcmV0dXJuIG5ldyBDdG9yKHJtID09PSAzID8gLTAgOiAwKTtcclxuXHJcbiAgICB5LmQgPSB4ZDtcclxuICAgIHkuZSA9IGdldEJhc2UxMEV4cG9uZW50KHhkLCBlKTtcclxuXHJcbiAgICByZXR1cm4gZXh0ZXJuYWwgPyBmaW5hbGlzZSh5LCBwciwgcm0pIDogeTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiAgIG4gJSAwID0gIE5cclxuICAgKiAgIG4gJSBOID0gIE5cclxuICAgKiAgIG4gJSBJID0gIG5cclxuICAgKiAgIDAgJSBuID0gIDBcclxuICAgKiAgLTAgJSBuID0gLTBcclxuICAgKiAgIDAgJSAwID0gIE5cclxuICAgKiAgIDAgJSBOID0gIE5cclxuICAgKiAgIDAgJSBJID0gIDBcclxuICAgKiAgIE4gJSBuID0gIE5cclxuICAgKiAgIE4gJSAwID0gIE5cclxuICAgKiAgIE4gJSBOID0gIE5cclxuICAgKiAgIE4gJSBJID0gIE5cclxuICAgKiAgIEkgJSBuID0gIE5cclxuICAgKiAgIEkgJSAwID0gIE5cclxuICAgKiAgIEkgJSBOID0gIE5cclxuICAgKiAgIEkgJSBJID0gIE5cclxuICAgKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgbW9kdWxvIGB5YCwgcm91bmRlZCB0b1xyXG4gICAqIGBwcmVjaXNpb25gIHNpZ25pZmljYW50IGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiBUaGUgcmVzdWx0IGRlcGVuZHMgb24gdGhlIG1vZHVsbyBtb2RlLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5tb2R1bG8gPSBQLm1vZCA9IGZ1bmN0aW9uICh5KSB7XHJcbiAgICB2YXIgcSxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIHkgPSBuZXcgQ3Rvcih5KTtcclxuXHJcbiAgICAvLyBSZXR1cm4gTmFOIGlmIHggaXMgwrFJbmZpbml0eSBvciBOYU4sIG9yIHkgaXMgTmFOIG9yIMKxMC5cclxuICAgIGlmICgheC5kIHx8ICF5LnMgfHwgeS5kICYmICF5LmRbMF0pIHJldHVybiBuZXcgQ3RvcihOYU4pO1xyXG5cclxuICAgIC8vIFJldHVybiB4IGlmIHkgaXMgwrFJbmZpbml0eSBvciB4IGlzIMKxMC5cclxuICAgIGlmICgheS5kIHx8IHguZCAmJiAheC5kWzBdKSB7XHJcbiAgICAgIHJldHVybiBmaW5hbGlzZShuZXcgQ3Rvcih4KSwgQ3Rvci5wcmVjaXNpb24sIEN0b3Iucm91bmRpbmcpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFByZXZlbnQgcm91bmRpbmcgb2YgaW50ZXJtZWRpYXRlIGNhbGN1bGF0aW9ucy5cclxuICAgIGV4dGVybmFsID0gZmFsc2U7XHJcblxyXG4gICAgaWYgKEN0b3IubW9kdWxvID09IDkpIHtcclxuXHJcbiAgICAgIC8vIEV1Y2xpZGlhbiBkaXZpc2lvbjogcSA9IHNpZ24oeSkgKiBmbG9vcih4IC8gYWJzKHkpKVxyXG4gICAgICAvLyByZXN1bHQgPSB4IC0gcSAqIHkgICAgd2hlcmUgIDAgPD0gcmVzdWx0IDwgYWJzKHkpXHJcbiAgICAgIHEgPSBkaXZpZGUoeCwgeS5hYnMoKSwgMCwgMywgMSk7XHJcbiAgICAgIHEucyAqPSB5LnM7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBxID0gZGl2aWRlKHgsIHksIDAsIEN0b3IubW9kdWxvLCAxKTtcclxuICAgIH1cclxuXHJcbiAgICBxID0gcS50aW1lcyh5KTtcclxuXHJcbiAgICBleHRlcm5hbCA9IHRydWU7XHJcblxyXG4gICAgcmV0dXJuIHgubWludXMocSk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIG5hdHVyYWwgZXhwb25lbnRpYWwgb2YgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCxcclxuICAgKiBpLmUuIHRoZSBiYXNlIGUgcmFpc2VkIHRvIHRoZSBwb3dlciB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gXHJcbiAgICogc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAubmF0dXJhbEV4cG9uZW50aWFsID0gUC5leHAgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gbmF0dXJhbEV4cG9uZW50aWFsKHRoaXMpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBuYXR1cmFsIGxvZ2FyaXRobSBvZiB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsLFxyXG4gICAqIHJvdW5kZWQgdG8gYHByZWNpc2lvbmAgc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAubmF0dXJhbExvZ2FyaXRobSA9IFAubG4gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gbmF0dXJhbExvZ2FyaXRobSh0aGlzKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIG5lZ2F0ZWQsIGkuZS4gYXMgaWYgbXVsdGlwbGllZCBieVxyXG4gICAqIC0xLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5uZWdhdGVkID0gUC5uZWcgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgeCA9IG5ldyB0aGlzLmNvbnN0cnVjdG9yKHRoaXMpO1xyXG4gICAgeC5zID0gLXgucztcclxuICAgIHJldHVybiBmaW5hbGlzZSh4KTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiAgbiArIDAgPSBuXHJcbiAgICogIG4gKyBOID0gTlxyXG4gICAqICBuICsgSSA9IElcclxuICAgKiAgMCArIG4gPSBuXHJcbiAgICogIDAgKyAwID0gMFxyXG4gICAqICAwICsgTiA9IE5cclxuICAgKiAgMCArIEkgPSBJXHJcbiAgICogIE4gKyBuID0gTlxyXG4gICAqICBOICsgMCA9IE5cclxuICAgKiAgTiArIE4gPSBOXHJcbiAgICogIE4gKyBJID0gTlxyXG4gICAqICBJICsgbiA9IElcclxuICAgKiAgSSArIDAgPSBJXHJcbiAgICogIEkgKyBOID0gTlxyXG4gICAqICBJICsgSSA9IElcclxuICAgKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgcGx1cyBgeWAsIHJvdW5kZWQgdG8gYHByZWNpc2lvbmBcclxuICAgKiBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC5wbHVzID0gUC5hZGQgPSBmdW5jdGlvbiAoeSkge1xyXG4gICAgdmFyIGNhcnJ5LCBkLCBlLCBpLCBrLCBsZW4sIHByLCBybSwgeGQsIHlkLFxyXG4gICAgICB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcblxyXG4gICAgeSA9IG5ldyBDdG9yKHkpO1xyXG5cclxuICAgIC8vIElmIGVpdGhlciBpcyBub3QgZmluaXRlLi4uXHJcbiAgICBpZiAoIXguZCB8fCAheS5kKSB7XHJcblxyXG4gICAgICAvLyBSZXR1cm4gTmFOIGlmIGVpdGhlciBpcyBOYU4uXHJcbiAgICAgIGlmICgheC5zIHx8ICF5LnMpIHkgPSBuZXcgQ3RvcihOYU4pO1xyXG5cclxuICAgICAgLy8gUmV0dXJuIHggaWYgeSBpcyBmaW5pdGUgYW5kIHggaXMgwrFJbmZpbml0eS5cclxuICAgICAgLy8gUmV0dXJuIHggaWYgYm90aCBhcmUgwrFJbmZpbml0eSB3aXRoIHRoZSBzYW1lIHNpZ24uXHJcbiAgICAgIC8vIFJldHVybiBOYU4gaWYgYm90aCBhcmUgwrFJbmZpbml0eSB3aXRoIGRpZmZlcmVudCBzaWducy5cclxuICAgICAgLy8gUmV0dXJuIHkgaWYgeCBpcyBmaW5pdGUgYW5kIHkgaXMgwrFJbmZpbml0eS5cclxuICAgICAgZWxzZSBpZiAoIXguZCkgeSA9IG5ldyBDdG9yKHkuZCB8fCB4LnMgPT09IHkucyA/IHggOiBOYU4pO1xyXG5cclxuICAgICAgcmV0dXJuIHk7XHJcbiAgICB9XHJcblxyXG4gICAgIC8vIElmIHNpZ25zIGRpZmZlci4uLlxyXG4gICAgaWYgKHgucyAhPSB5LnMpIHtcclxuICAgICAgeS5zID0gLXkucztcclxuICAgICAgcmV0dXJuIHgubWludXMoeSk7XHJcbiAgICB9XHJcblxyXG4gICAgeGQgPSB4LmQ7XHJcbiAgICB5ZCA9IHkuZDtcclxuICAgIHByID0gQ3Rvci5wcmVjaXNpb247XHJcbiAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcblxyXG4gICAgLy8gSWYgZWl0aGVyIGlzIHplcm8uLi5cclxuICAgIGlmICgheGRbMF0gfHwgIXlkWzBdKSB7XHJcblxyXG4gICAgICAvLyBSZXR1cm4geCBpZiB5IGlzIHplcm8uXHJcbiAgICAgIC8vIFJldHVybiB5IGlmIHkgaXMgbm9uLXplcm8uXHJcbiAgICAgIGlmICgheWRbMF0pIHkgPSBuZXcgQ3Rvcih4KTtcclxuXHJcbiAgICAgIHJldHVybiBleHRlcm5hbCA/IGZpbmFsaXNlKHksIHByLCBybSkgOiB5O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHggYW5kIHkgYXJlIGZpbml0ZSwgbm9uLXplcm8gbnVtYmVycyB3aXRoIHRoZSBzYW1lIHNpZ24uXHJcblxyXG4gICAgLy8gQ2FsY3VsYXRlIGJhc2UgMWU3IGV4cG9uZW50cy5cclxuICAgIGsgPSBtYXRoZmxvb3IoeC5lIC8gTE9HX0JBU0UpO1xyXG4gICAgZSA9IG1hdGhmbG9vcih5LmUgLyBMT0dfQkFTRSk7XHJcblxyXG4gICAgeGQgPSB4ZC5zbGljZSgpO1xyXG4gICAgaSA9IGsgLSBlO1xyXG5cclxuICAgIC8vIElmIGJhc2UgMWU3IGV4cG9uZW50cyBkaWZmZXIuLi5cclxuICAgIGlmIChpKSB7XHJcblxyXG4gICAgICBpZiAoaSA8IDApIHtcclxuICAgICAgICBkID0geGQ7XHJcbiAgICAgICAgaSA9IC1pO1xyXG4gICAgICAgIGxlbiA9IHlkLmxlbmd0aDtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBkID0geWQ7XHJcbiAgICAgICAgZSA9IGs7XHJcbiAgICAgICAgbGVuID0geGQubGVuZ3RoO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBMaW1pdCBudW1iZXIgb2YgemVyb3MgcHJlcGVuZGVkIHRvIG1heChjZWlsKHByIC8gTE9HX0JBU0UpLCBsZW4pICsgMS5cclxuICAgICAgayA9IE1hdGguY2VpbChwciAvIExPR19CQVNFKTtcclxuICAgICAgbGVuID0gayA+IGxlbiA/IGsgKyAxIDogbGVuICsgMTtcclxuXHJcbiAgICAgIGlmIChpID4gbGVuKSB7XHJcbiAgICAgICAgaSA9IGxlbjtcclxuICAgICAgICBkLmxlbmd0aCA9IDE7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIFByZXBlbmQgemVyb3MgdG8gZXF1YWxpc2UgZXhwb25lbnRzLiBOb3RlOiBGYXN0ZXIgdG8gdXNlIHJldmVyc2UgdGhlbiBkbyB1bnNoaWZ0cy5cclxuICAgICAgZC5yZXZlcnNlKCk7XHJcbiAgICAgIGZvciAoOyBpLS07KSBkLnB1c2goMCk7XHJcbiAgICAgIGQucmV2ZXJzZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGxlbiA9IHhkLmxlbmd0aDtcclxuICAgIGkgPSB5ZC5sZW5ndGg7XHJcblxyXG4gICAgLy8gSWYgeWQgaXMgbG9uZ2VyIHRoYW4geGQsIHN3YXAgeGQgYW5kIHlkIHNvIHhkIHBvaW50cyB0byB0aGUgbG9uZ2VyIGFycmF5LlxyXG4gICAgaWYgKGxlbiAtIGkgPCAwKSB7XHJcbiAgICAgIGkgPSBsZW47XHJcbiAgICAgIGQgPSB5ZDtcclxuICAgICAgeWQgPSB4ZDtcclxuICAgICAgeGQgPSBkO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE9ubHkgc3RhcnQgYWRkaW5nIGF0IHlkLmxlbmd0aCAtIDEgYXMgdGhlIGZ1cnRoZXIgZGlnaXRzIG9mIHhkIGNhbiBiZSBsZWZ0IGFzIHRoZXkgYXJlLlxyXG4gICAgZm9yIChjYXJyeSA9IDA7IGk7KSB7XHJcbiAgICAgIGNhcnJ5ID0gKHhkWy0taV0gPSB4ZFtpXSArIHlkW2ldICsgY2FycnkpIC8gQkFTRSB8IDA7XHJcbiAgICAgIHhkW2ldICU9IEJBU0U7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNhcnJ5KSB7XHJcbiAgICAgIHhkLnVuc2hpZnQoY2FycnkpO1xyXG4gICAgICArK2U7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUmVtb3ZlIHRyYWlsaW5nIHplcm9zLlxyXG4gICAgLy8gTm8gbmVlZCB0byBjaGVjayBmb3IgemVybywgYXMgK3ggKyAreSAhPSAwICYmIC14ICsgLXkgIT0gMFxyXG4gICAgZm9yIChsZW4gPSB4ZC5sZW5ndGg7IHhkWy0tbGVuXSA9PSAwOykgeGQucG9wKCk7XHJcblxyXG4gICAgeS5kID0geGQ7XHJcbiAgICB5LmUgPSBnZXRCYXNlMTBFeHBvbmVudCh4ZCwgZSk7XHJcblxyXG4gICAgcmV0dXJuIGV4dGVybmFsID8gZmluYWxpc2UoeSwgcHIsIHJtKSA6IHk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIHRoZSBudW1iZXIgb2Ygc2lnbmlmaWNhbnQgZGlnaXRzIG9mIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwuXHJcbiAgICpcclxuICAgKiBbel0ge2Jvb2xlYW58bnVtYmVyfSBXaGV0aGVyIHRvIGNvdW50IGludGVnZXItcGFydCB0cmFpbGluZyB6ZXJvczogdHJ1ZSwgZmFsc2UsIDEgb3IgMC5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAucHJlY2lzaW9uID0gUC5zZCA9IGZ1bmN0aW9uICh6KSB7XHJcbiAgICB2YXIgayxcclxuICAgICAgeCA9IHRoaXM7XHJcblxyXG4gICAgaWYgKHogIT09IHZvaWQgMCAmJiB6ICE9PSAhIXogJiYgeiAhPT0gMSAmJiB6ICE9PSAwKSB0aHJvdyBFcnJvcihpbnZhbGlkQXJndW1lbnQgKyB6KTtcclxuXHJcbiAgICBpZiAoeC5kKSB7XHJcbiAgICAgIGsgPSBnZXRQcmVjaXNpb24oeC5kKTtcclxuICAgICAgaWYgKHogJiYgeC5lICsgMSA+IGspIGsgPSB4LmUgKyAxO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgayA9IE5hTjtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gaztcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIHJvdW5kZWQgdG8gYSB3aG9sZSBudW1iZXIgdXNpbmdcclxuICAgKiByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKi9cclxuICBQLnJvdW5kID0gZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcjtcclxuXHJcbiAgICByZXR1cm4gZmluYWxpc2UobmV3IEN0b3IoeCksIHguZSArIDEsIEN0b3Iucm91bmRpbmcpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBzaW5lIG9mIHRoZSB2YWx1ZSBpbiByYWRpYW5zIG9mIHRoaXMgRGVjaW1hbC5cclxuICAgKlxyXG4gICAqIERvbWFpbjogWy1JbmZpbml0eSwgSW5maW5pdHldXHJcbiAgICogUmFuZ2U6IFstMSwgMV1cclxuICAgKlxyXG4gICAqIHNpbih4KSA9IHggLSB4XjMvMyEgKyB4XjUvNSEgLSAuLi5cclxuICAgKlxyXG4gICAqIHNpbigwKSAgICAgICAgID0gMFxyXG4gICAqIHNpbigtMCkgICAgICAgID0gLTBcclxuICAgKiBzaW4oSW5maW5pdHkpICA9IE5hTlxyXG4gICAqIHNpbigtSW5maW5pdHkpID0gTmFOXHJcbiAgICogc2luKE5hTikgICAgICAgPSBOYU5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAuc2luZSA9IFAuc2luID0gZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIHByLCBybSxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIGlmICgheC5pc0Zpbml0ZSgpKSByZXR1cm4gbmV3IEN0b3IoTmFOKTtcclxuICAgIGlmICh4LmlzWmVybygpKSByZXR1cm4gbmV3IEN0b3IoeCk7XHJcblxyXG4gICAgcHIgPSBDdG9yLnByZWNpc2lvbjtcclxuICAgIHJtID0gQ3Rvci5yb3VuZGluZztcclxuICAgIEN0b3IucHJlY2lzaW9uID0gcHIgKyBNYXRoLm1heCh4LmUsIHguc2QoKSkgKyBMT0dfQkFTRTtcclxuICAgIEN0b3Iucm91bmRpbmcgPSAxO1xyXG5cclxuICAgIHggPSBzaW5lKEN0b3IsIHRvTGVzc1RoYW5IYWxmUGkoQ3RvciwgeCkpO1xyXG5cclxuICAgIEN0b3IucHJlY2lzaW9uID0gcHI7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gcm07XHJcblxyXG4gICAgcmV0dXJuIGZpbmFsaXNlKHF1YWRyYW50ID4gMiA/IHgubmVnKCkgOiB4LCBwciwgcm0sIHRydWUpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBzcXVhcmUgcm9vdCBvZiB0aGlzIERlY2ltYWwsIHJvdW5kZWQgdG8gYHByZWNpc2lvbmBcclxuICAgKiBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogIHNxcnQoLW4pID0gIE5cclxuICAgKiAgc3FydChOKSAgPSAgTlxyXG4gICAqICBzcXJ0KC1JKSA9ICBOXHJcbiAgICogIHNxcnQoSSkgID0gIElcclxuICAgKiAgc3FydCgwKSAgPSAgMFxyXG4gICAqICBzcXJ0KC0wKSA9IC0wXHJcbiAgICpcclxuICAgKi9cclxuICBQLnNxdWFyZVJvb3QgPSBQLnNxcnQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgbSwgbiwgc2QsIHIsIHJlcCwgdCxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIGQgPSB4LmQsXHJcbiAgICAgIGUgPSB4LmUsXHJcbiAgICAgIHMgPSB4LnMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIC8vIE5lZ2F0aXZlL05hTi9JbmZpbml0eS96ZXJvP1xyXG4gICAgaWYgKHMgIT09IDEgfHwgIWQgfHwgIWRbMF0pIHtcclxuICAgICAgcmV0dXJuIG5ldyBDdG9yKCFzIHx8IHMgPCAwICYmICghZCB8fCBkWzBdKSA/IE5hTiA6IGQgPyB4IDogMSAvIDApO1xyXG4gICAgfVxyXG5cclxuICAgIGV4dGVybmFsID0gZmFsc2U7XHJcblxyXG4gICAgLy8gSW5pdGlhbCBlc3RpbWF0ZS5cclxuICAgIHMgPSBNYXRoLnNxcnQoK3gpO1xyXG5cclxuICAgIC8vIE1hdGguc3FydCB1bmRlcmZsb3cvb3ZlcmZsb3c/XHJcbiAgICAvLyBQYXNzIHggdG8gTWF0aC5zcXJ0IGFzIGludGVnZXIsIHRoZW4gYWRqdXN0IHRoZSBleHBvbmVudCBvZiB0aGUgcmVzdWx0LlxyXG4gICAgaWYgKHMgPT0gMCB8fCBzID09IDEgLyAwKSB7XHJcbiAgICAgIG4gPSBkaWdpdHNUb1N0cmluZyhkKTtcclxuXHJcbiAgICAgIGlmICgobi5sZW5ndGggKyBlKSAlIDIgPT0gMCkgbiArPSAnMCc7XHJcbiAgICAgIHMgPSBNYXRoLnNxcnQobik7XHJcbiAgICAgIGUgPSBtYXRoZmxvb3IoKGUgKyAxKSAvIDIpIC0gKGUgPCAwIHx8IGUgJSAyKTtcclxuXHJcbiAgICAgIGlmIChzID09IDEgLyAwKSB7XHJcbiAgICAgICAgbiA9ICcxZScgKyBlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIG4gPSBzLnRvRXhwb25lbnRpYWwoKTtcclxuICAgICAgICBuID0gbi5zbGljZSgwLCBuLmluZGV4T2YoJ2UnKSArIDEpICsgZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgciA9IG5ldyBDdG9yKG4pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgciA9IG5ldyBDdG9yKHMudG9TdHJpbmcoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2QgPSAoZSA9IEN0b3IucHJlY2lzaW9uKSArIDM7XHJcblxyXG4gICAgLy8gTmV3dG9uLVJhcGhzb24gaXRlcmF0aW9uLlxyXG4gICAgZm9yICg7Oykge1xyXG4gICAgICB0ID0gcjtcclxuICAgICAgciA9IHQucGx1cyhkaXZpZGUoeCwgdCwgc2QgKyAyLCAxKSkudGltZXMoMC41KTtcclxuXHJcbiAgICAgIC8vIFRPRE8/IFJlcGxhY2Ugd2l0aCBmb3ItbG9vcCBhbmQgY2hlY2tSb3VuZGluZ0RpZ2l0cy5cclxuICAgICAgaWYgKGRpZ2l0c1RvU3RyaW5nKHQuZCkuc2xpY2UoMCwgc2QpID09PSAobiA9IGRpZ2l0c1RvU3RyaW5nKHIuZCkpLnNsaWNlKDAsIHNkKSkge1xyXG4gICAgICAgIG4gPSBuLnNsaWNlKHNkIC0gMywgc2QgKyAxKTtcclxuXHJcbiAgICAgICAgLy8gVGhlIDR0aCByb3VuZGluZyBkaWdpdCBtYXkgYmUgaW4gZXJyb3IgYnkgLTEgc28gaWYgdGhlIDQgcm91bmRpbmcgZGlnaXRzIGFyZSA5OTk5IG9yXHJcbiAgICAgICAgLy8gNDk5OSwgaS5lLiBhcHByb2FjaGluZyBhIHJvdW5kaW5nIGJvdW5kYXJ5LCBjb250aW51ZSB0aGUgaXRlcmF0aW9uLlxyXG4gICAgICAgIGlmIChuID09ICc5OTk5JyB8fCAhcmVwICYmIG4gPT0gJzQ5OTknKSB7XHJcblxyXG4gICAgICAgICAgLy8gT24gdGhlIGZpcnN0IGl0ZXJhdGlvbiBvbmx5LCBjaGVjayB0byBzZWUgaWYgcm91bmRpbmcgdXAgZ2l2ZXMgdGhlIGV4YWN0IHJlc3VsdCBhcyB0aGVcclxuICAgICAgICAgIC8vIG5pbmVzIG1heSBpbmZpbml0ZWx5IHJlcGVhdC5cclxuICAgICAgICAgIGlmICghcmVwKSB7XHJcbiAgICAgICAgICAgIGZpbmFsaXNlKHQsIGUgKyAxLCAwKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0LnRpbWVzKHQpLmVxKHgpKSB7XHJcbiAgICAgICAgICAgICAgciA9IHQ7XHJcbiAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBzZCArPSA0O1xyXG4gICAgICAgICAgcmVwID0gMTtcclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgIC8vIElmIHRoZSByb3VuZGluZyBkaWdpdHMgYXJlIG51bGwsIDB7MCw0fSBvciA1MHswLDN9LCBjaGVjayBmb3IgYW4gZXhhY3QgcmVzdWx0LlxyXG4gICAgICAgICAgLy8gSWYgbm90LCB0aGVuIHRoZXJlIGFyZSBmdXJ0aGVyIGRpZ2l0cyBhbmQgbSB3aWxsIGJlIHRydXRoeS5cclxuICAgICAgICAgIGlmICghK24gfHwgIStuLnNsaWNlKDEpICYmIG4uY2hhckF0KDApID09ICc1Jykge1xyXG5cclxuICAgICAgICAgICAgLy8gVHJ1bmNhdGUgdG8gdGhlIGZpcnN0IHJvdW5kaW5nIGRpZ2l0LlxyXG4gICAgICAgICAgICBmaW5hbGlzZShyLCBlICsgMSwgMSk7XHJcbiAgICAgICAgICAgIG0gPSAhci50aW1lcyhyKS5lcSh4KTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBleHRlcm5hbCA9IHRydWU7XHJcblxyXG4gICAgcmV0dXJuIGZpbmFsaXNlKHIsIGUsIEN0b3Iucm91bmRpbmcsIG0pO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSB0YW5nZW50IG9mIHRoZSB2YWx1ZSBpbiByYWRpYW5zIG9mIHRoaXMgRGVjaW1hbC5cclxuICAgKlxyXG4gICAqIERvbWFpbjogWy1JbmZpbml0eSwgSW5maW5pdHldXHJcbiAgICogUmFuZ2U6IFstSW5maW5pdHksIEluZmluaXR5XVxyXG4gICAqXHJcbiAgICogdGFuKDApICAgICAgICAgPSAwXHJcbiAgICogdGFuKC0wKSAgICAgICAgPSAtMFxyXG4gICAqIHRhbihJbmZpbml0eSkgID0gTmFOXHJcbiAgICogdGFuKC1JbmZpbml0eSkgPSBOYU5cclxuICAgKiB0YW4oTmFOKSAgICAgICA9IE5hTlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC50YW5nZW50ID0gUC50YW4gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgcHIsIHJtLFxyXG4gICAgICB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcblxyXG4gICAgaWYgKCF4LmlzRmluaXRlKCkpIHJldHVybiBuZXcgQ3RvcihOYU4pO1xyXG4gICAgaWYgKHguaXNaZXJvKCkpIHJldHVybiBuZXcgQ3Rvcih4KTtcclxuXHJcbiAgICBwciA9IEN0b3IucHJlY2lzaW9uO1xyXG4gICAgcm0gPSBDdG9yLnJvdW5kaW5nO1xyXG4gICAgQ3Rvci5wcmVjaXNpb24gPSBwciArIDEwO1xyXG4gICAgQ3Rvci5yb3VuZGluZyA9IDE7XHJcblxyXG4gICAgeCA9IHguc2luKCk7XHJcbiAgICB4LnMgPSAxO1xyXG4gICAgeCA9IGRpdmlkZSh4LCBuZXcgQ3RvcigxKS5taW51cyh4LnRpbWVzKHgpKS5zcXJ0KCksIHByICsgMTAsIDApO1xyXG5cclxuICAgIEN0b3IucHJlY2lzaW9uID0gcHI7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gcm07XHJcblxyXG4gICAgcmV0dXJuIGZpbmFsaXNlKHF1YWRyYW50ID09IDIgfHwgcXVhZHJhbnQgPT0gNCA/IHgubmVnKCkgOiB4LCBwciwgcm0sIHRydWUpO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqICBuICogMCA9IDBcclxuICAgKiAgbiAqIE4gPSBOXHJcbiAgICogIG4gKiBJID0gSVxyXG4gICAqICAwICogbiA9IDBcclxuICAgKiAgMCAqIDAgPSAwXHJcbiAgICogIDAgKiBOID0gTlxyXG4gICAqICAwICogSSA9IE5cclxuICAgKiAgTiAqIG4gPSBOXHJcbiAgICogIE4gKiAwID0gTlxyXG4gICAqICBOICogTiA9IE5cclxuICAgKiAgTiAqIEkgPSBOXHJcbiAgICogIEkgKiBuID0gSVxyXG4gICAqICBJICogMCA9IE5cclxuICAgKiAgSSAqIE4gPSBOXHJcbiAgICogIEkgKiBJID0gSVxyXG4gICAqXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhpcyBEZWNpbWFsIHRpbWVzIGB5YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYCBzaWduaWZpY2FudFxyXG4gICAqIGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKi9cclxuICBQLnRpbWVzID0gUC5tdWwgPSBmdW5jdGlvbiAoeSkge1xyXG4gICAgdmFyIGNhcnJ5LCBlLCBpLCBrLCByLCByTCwgdCwgeGRMLCB5ZEwsXHJcbiAgICAgIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcixcclxuICAgICAgeGQgPSB4LmQsXHJcbiAgICAgIHlkID0gKHkgPSBuZXcgQ3Rvcih5KSkuZDtcclxuXHJcbiAgICB5LnMgKj0geC5zO1xyXG5cclxuICAgICAvLyBJZiBlaXRoZXIgaXMgTmFOLCDCsUluZmluaXR5IG9yIMKxMC4uLlxyXG4gICAgaWYgKCF4ZCB8fCAheGRbMF0gfHwgIXlkIHx8ICF5ZFswXSkge1xyXG5cclxuICAgICAgcmV0dXJuIG5ldyBDdG9yKCF5LnMgfHwgeGQgJiYgIXhkWzBdICYmICF5ZCB8fCB5ZCAmJiAheWRbMF0gJiYgIXhkXHJcblxyXG4gICAgICAgIC8vIFJldHVybiBOYU4gaWYgZWl0aGVyIGlzIE5hTi5cclxuICAgICAgICAvLyBSZXR1cm4gTmFOIGlmIHggaXMgwrEwIGFuZCB5IGlzIMKxSW5maW5pdHksIG9yIHkgaXMgwrEwIGFuZCB4IGlzIMKxSW5maW5pdHkuXHJcbiAgICAgICAgPyBOYU5cclxuXHJcbiAgICAgICAgLy8gUmV0dXJuIMKxSW5maW5pdHkgaWYgZWl0aGVyIGlzIMKxSW5maW5pdHkuXHJcbiAgICAgICAgLy8gUmV0dXJuIMKxMCBpZiBlaXRoZXIgaXMgwrEwLlxyXG4gICAgICAgIDogIXhkIHx8ICF5ZCA/IHkucyAvIDAgOiB5LnMgKiAwKTtcclxuICAgIH1cclxuXHJcbiAgICBlID0gbWF0aGZsb29yKHguZSAvIExPR19CQVNFKSArIG1hdGhmbG9vcih5LmUgLyBMT0dfQkFTRSk7XHJcbiAgICB4ZEwgPSB4ZC5sZW5ndGg7XHJcbiAgICB5ZEwgPSB5ZC5sZW5ndGg7XHJcblxyXG4gICAgLy8gRW5zdXJlIHhkIHBvaW50cyB0byB0aGUgbG9uZ2VyIGFycmF5LlxyXG4gICAgaWYgKHhkTCA8IHlkTCkge1xyXG4gICAgICByID0geGQ7XHJcbiAgICAgIHhkID0geWQ7XHJcbiAgICAgIHlkID0gcjtcclxuICAgICAgckwgPSB4ZEw7XHJcbiAgICAgIHhkTCA9IHlkTDtcclxuICAgICAgeWRMID0gckw7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gSW5pdGlhbGlzZSB0aGUgcmVzdWx0IGFycmF5IHdpdGggemVyb3MuXHJcbiAgICByID0gW107XHJcbiAgICByTCA9IHhkTCArIHlkTDtcclxuICAgIGZvciAoaSA9IHJMOyBpLS07KSByLnB1c2goMCk7XHJcblxyXG4gICAgLy8gTXVsdGlwbHkhXHJcbiAgICBmb3IgKGkgPSB5ZEw7IC0taSA+PSAwOykge1xyXG4gICAgICBjYXJyeSA9IDA7XHJcbiAgICAgIGZvciAoayA9IHhkTCArIGk7IGsgPiBpOykge1xyXG4gICAgICAgIHQgPSByW2tdICsgeWRbaV0gKiB4ZFtrIC0gaSAtIDFdICsgY2Fycnk7XHJcbiAgICAgICAgcltrLS1dID0gdCAlIEJBU0UgfCAwO1xyXG4gICAgICAgIGNhcnJ5ID0gdCAvIEJBU0UgfCAwO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByW2tdID0gKHJba10gKyBjYXJyeSkgJSBCQVNFIHwgMDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBSZW1vdmUgdHJhaWxpbmcgemVyb3MuXHJcbiAgICBmb3IgKDsgIXJbLS1yTF07KSByLnBvcCgpO1xyXG5cclxuICAgIGlmIChjYXJyeSkgKytlO1xyXG4gICAgZWxzZSByLnNoaWZ0KCk7XHJcblxyXG4gICAgeS5kID0gcjtcclxuICAgIHkuZSA9IGdldEJhc2UxMEV4cG9uZW50KHIsIGUpO1xyXG5cclxuICAgIHJldHVybiBleHRlcm5hbCA/IGZpbmFsaXNlKHksIEN0b3IucHJlY2lzaW9uLCBDdG9yLnJvdW5kaW5nKSA6IHk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgc3RyaW5nIHJlcHJlc2VudGluZyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIGluIGJhc2UgMiwgcm91bmQgdG8gYHNkYCBzaWduaWZpY2FudFxyXG4gICAqIGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGBybWAuXHJcbiAgICpcclxuICAgKiBJZiB0aGUgb3B0aW9uYWwgYHNkYCBhcmd1bWVudCBpcyBwcmVzZW50IHRoZW4gcmV0dXJuIGJpbmFyeSBleHBvbmVudGlhbCBub3RhdGlvbi5cclxuICAgKlxyXG4gICAqIFtzZF0ge251bWJlcn0gU2lnbmlmaWNhbnQgZGlnaXRzLiBJbnRlZ2VyLCAxIHRvIE1BWF9ESUdJVFMgaW5jbHVzaXZlLlxyXG4gICAqIFtybV0ge251bWJlcn0gUm91bmRpbmcgbW9kZS4gSW50ZWdlciwgMCB0byA4IGluY2x1c2l2ZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAudG9CaW5hcnkgPSBmdW5jdGlvbiAoc2QsIHJtKSB7XHJcbiAgICByZXR1cm4gdG9TdHJpbmdCaW5hcnkodGhpcywgMiwgc2QsIHJtKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIHJvdW5kZWQgdG8gYSBtYXhpbXVtIG9mIGBkcGBcclxuICAgKiBkZWNpbWFsIHBsYWNlcyB1c2luZyByb3VuZGluZyBtb2RlIGBybWAgb3IgYHJvdW5kaW5nYCBpZiBgcm1gIGlzIG9taXR0ZWQuXHJcbiAgICpcclxuICAgKiBJZiBgZHBgIGlzIG9taXR0ZWQsIHJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwuXHJcbiAgICpcclxuICAgKiBbZHBdIHtudW1iZXJ9IERlY2ltYWwgcGxhY2VzLiBJbnRlZ2VyLCAwIHRvIE1BWF9ESUdJVFMgaW5jbHVzaXZlLlxyXG4gICAqIFtybV0ge251bWJlcn0gUm91bmRpbmcgbW9kZS4gSW50ZWdlciwgMCB0byA4IGluY2x1c2l2ZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAudG9EZWNpbWFsUGxhY2VzID0gUC50b0RQID0gZnVuY3Rpb24gKGRwLCBybSkge1xyXG4gICAgdmFyIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcjtcclxuXHJcbiAgICB4ID0gbmV3IEN0b3IoeCk7XHJcbiAgICBpZiAoZHAgPT09IHZvaWQgMCkgcmV0dXJuIHg7XHJcblxyXG4gICAgY2hlY2tJbnQzMihkcCwgMCwgTUFYX0RJR0lUUyk7XHJcblxyXG4gICAgaWYgKHJtID09PSB2b2lkIDApIHJtID0gQ3Rvci5yb3VuZGluZztcclxuICAgIGVsc2UgY2hlY2tJbnQzMihybSwgMCwgOCk7XHJcblxyXG4gICAgcmV0dXJuIGZpbmFsaXNlKHgsIGRwICsgeC5lICsgMSwgcm0pO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIHN0cmluZyByZXByZXNlbnRpbmcgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBpbiBleHBvbmVudGlhbCBub3RhdGlvbiByb3VuZGVkIHRvXHJcbiAgICogYGRwYCBmaXhlZCBkZWNpbWFsIHBsYWNlcyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiBbZHBdIHtudW1iZXJ9IERlY2ltYWwgcGxhY2VzLiBJbnRlZ2VyLCAwIHRvIE1BWF9ESUdJVFMgaW5jbHVzaXZlLlxyXG4gICAqIFtybV0ge251bWJlcn0gUm91bmRpbmcgbW9kZS4gSW50ZWdlciwgMCB0byA4IGluY2x1c2l2ZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAudG9FeHBvbmVudGlhbCA9IGZ1bmN0aW9uIChkcCwgcm0pIHtcclxuICAgIHZhciBzdHIsXHJcbiAgICAgIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcjtcclxuXHJcbiAgICBpZiAoZHAgPT09IHZvaWQgMCkge1xyXG4gICAgICBzdHIgPSBmaW5pdGVUb1N0cmluZyh4LCB0cnVlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNoZWNrSW50MzIoZHAsIDAsIE1BWF9ESUdJVFMpO1xyXG5cclxuICAgICAgaWYgKHJtID09PSB2b2lkIDApIHJtID0gQ3Rvci5yb3VuZGluZztcclxuICAgICAgZWxzZSBjaGVja0ludDMyKHJtLCAwLCA4KTtcclxuXHJcbiAgICAgIHggPSBmaW5hbGlzZShuZXcgQ3Rvcih4KSwgZHAgKyAxLCBybSk7XHJcbiAgICAgIHN0ciA9IGZpbml0ZVRvU3RyaW5nKHgsIHRydWUsIGRwICsgMSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHguaXNOZWcoKSAmJiAheC5pc1plcm8oKSA/ICctJyArIHN0ciA6IHN0cjtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBzdHJpbmcgcmVwcmVzZW50aW5nIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgaW4gbm9ybWFsIChmaXhlZC1wb2ludCkgbm90YXRpb24gdG9cclxuICAgKiBgZHBgIGZpeGVkIGRlY2ltYWwgcGxhY2VzIGFuZCByb3VuZGVkIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJtYCBvciBgcm91bmRpbmdgIGlmIGBybWAgaXNcclxuICAgKiBvbWl0dGVkLlxyXG4gICAqXHJcbiAgICogQXMgd2l0aCBKYXZhU2NyaXB0IG51bWJlcnMsICgtMCkudG9GaXhlZCgwKSBpcyAnMCcsIGJ1dCBlLmcuICgtMC4wMDAwMSkudG9GaXhlZCgwKSBpcyAnLTAnLlxyXG4gICAqXHJcbiAgICogW2RwXSB7bnVtYmVyfSBEZWNpbWFsIHBsYWNlcy4gSW50ZWdlciwgMCB0byBNQVhfRElHSVRTIGluY2x1c2l2ZS5cclxuICAgKiBbcm1dIHtudW1iZXJ9IFJvdW5kaW5nIG1vZGUuIEludGVnZXIsIDAgdG8gOCBpbmNsdXNpdmUuXHJcbiAgICpcclxuICAgKiAoLTApLnRvRml4ZWQoMCkgaXMgJzAnLCBidXQgKC0wLjEpLnRvRml4ZWQoMCkgaXMgJy0wJy5cclxuICAgKiAoLTApLnRvRml4ZWQoMSkgaXMgJzAuMCcsIGJ1dCAoLTAuMDEpLnRvRml4ZWQoMSkgaXMgJy0wLjAnLlxyXG4gICAqICgtMCkudG9GaXhlZCgzKSBpcyAnMC4wMDAnLlxyXG4gICAqICgtMC41KS50b0ZpeGVkKDApIGlzICctMCcuXHJcbiAgICpcclxuICAgKi9cclxuICBQLnRvRml4ZWQgPSBmdW5jdGlvbiAoZHAsIHJtKSB7XHJcbiAgICB2YXIgc3RyLCB5LFxyXG4gICAgICB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcblxyXG4gICAgaWYgKGRwID09PSB2b2lkIDApIHtcclxuICAgICAgc3RyID0gZmluaXRlVG9TdHJpbmcoeCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjaGVja0ludDMyKGRwLCAwLCBNQVhfRElHSVRTKTtcclxuXHJcbiAgICAgIGlmIChybSA9PT0gdm9pZCAwKSBybSA9IEN0b3Iucm91bmRpbmc7XHJcbiAgICAgIGVsc2UgY2hlY2tJbnQzMihybSwgMCwgOCk7XHJcblxyXG4gICAgICB5ID0gZmluYWxpc2UobmV3IEN0b3IoeCksIGRwICsgeC5lICsgMSwgcm0pO1xyXG4gICAgICBzdHIgPSBmaW5pdGVUb1N0cmluZyh5LCBmYWxzZSwgZHAgKyB5LmUgKyAxKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUbyBkZXRlcm1pbmUgd2hldGhlciB0byBhZGQgdGhlIG1pbnVzIHNpZ24gbG9vayBhdCB0aGUgdmFsdWUgYmVmb3JlIGl0IHdhcyByb3VuZGVkLFxyXG4gICAgLy8gaS5lLiBsb29rIGF0IGB4YCByYXRoZXIgdGhhbiBgeWAuXHJcbiAgICByZXR1cm4geC5pc05lZygpICYmICF4LmlzWmVybygpID8gJy0nICsgc3RyIDogc3RyO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhbiBhcnJheSByZXByZXNlbnRpbmcgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBhcyBhIHNpbXBsZSBmcmFjdGlvbiB3aXRoIGFuIGludGVnZXJcclxuICAgKiBudW1lcmF0b3IgYW5kIGFuIGludGVnZXIgZGVub21pbmF0b3IuXHJcbiAgICpcclxuICAgKiBUaGUgZGVub21pbmF0b3Igd2lsbCBiZSBhIHBvc2l0aXZlIG5vbi16ZXJvIHZhbHVlIGxlc3MgdGhhbiBvciBlcXVhbCB0byB0aGUgc3BlY2lmaWVkIG1heGltdW1cclxuICAgKiBkZW5vbWluYXRvci4gSWYgYSBtYXhpbXVtIGRlbm9taW5hdG9yIGlzIG5vdCBzcGVjaWZpZWQsIHRoZSBkZW5vbWluYXRvciB3aWxsIGJlIHRoZSBsb3dlc3RcclxuICAgKiB2YWx1ZSBuZWNlc3NhcnkgdG8gcmVwcmVzZW50IHRoZSBudW1iZXIgZXhhY3RseS5cclxuICAgKlxyXG4gICAqIFttYXhEXSB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfSBNYXhpbXVtIGRlbm9taW5hdG9yLiBJbnRlZ2VyID49IDEgYW5kIDwgSW5maW5pdHkuXHJcbiAgICpcclxuICAgKi9cclxuICBQLnRvRnJhY3Rpb24gPSBmdW5jdGlvbiAobWF4RCkge1xyXG4gICAgdmFyIGQsIGQwLCBkMSwgZDIsIGUsIGssIG4sIG4wLCBuMSwgcHIsIHEsIHIsXHJcbiAgICAgIHggPSB0aGlzLFxyXG4gICAgICB4ZCA9IHguZCxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcblxyXG4gICAgaWYgKCF4ZCkgcmV0dXJuIG5ldyBDdG9yKHgpO1xyXG5cclxuICAgIG4xID0gZDAgPSBuZXcgQ3RvcigxKTtcclxuICAgIGQxID0gbjAgPSBuZXcgQ3RvcigwKTtcclxuXHJcbiAgICBkID0gbmV3IEN0b3IoZDEpO1xyXG4gICAgZSA9IGQuZSA9IGdldFByZWNpc2lvbih4ZCkgLSB4LmUgLSAxO1xyXG4gICAgayA9IGUgJSBMT0dfQkFTRTtcclxuICAgIGQuZFswXSA9IG1hdGhwb3coMTAsIGsgPCAwID8gTE9HX0JBU0UgKyBrIDogayk7XHJcblxyXG4gICAgaWYgKG1heEQgPT0gbnVsbCkge1xyXG5cclxuICAgICAgLy8gZCBpcyAxMCoqZSwgdGhlIG1pbmltdW0gbWF4LWRlbm9taW5hdG9yIG5lZWRlZC5cclxuICAgICAgbWF4RCA9IGUgPiAwID8gZCA6IG4xO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgbiA9IG5ldyBDdG9yKG1heEQpO1xyXG4gICAgICBpZiAoIW4uaXNJbnQoKSB8fCBuLmx0KG4xKSkgdGhyb3cgRXJyb3IoaW52YWxpZEFyZ3VtZW50ICsgbik7XHJcbiAgICAgIG1heEQgPSBuLmd0KGQpID8gKGUgPiAwID8gZCA6IG4xKSA6IG47XHJcbiAgICB9XHJcblxyXG4gICAgZXh0ZXJuYWwgPSBmYWxzZTtcclxuICAgIG4gPSBuZXcgQ3RvcihkaWdpdHNUb1N0cmluZyh4ZCkpO1xyXG4gICAgcHIgPSBDdG9yLnByZWNpc2lvbjtcclxuICAgIEN0b3IucHJlY2lzaW9uID0gZSA9IHhkLmxlbmd0aCAqIExPR19CQVNFICogMjtcclxuXHJcbiAgICBmb3IgKDs7KSAge1xyXG4gICAgICBxID0gZGl2aWRlKG4sIGQsIDAsIDEsIDEpO1xyXG4gICAgICBkMiA9IGQwLnBsdXMocS50aW1lcyhkMSkpO1xyXG4gICAgICBpZiAoZDIuY21wKG1heEQpID09IDEpIGJyZWFrO1xyXG4gICAgICBkMCA9IGQxO1xyXG4gICAgICBkMSA9IGQyO1xyXG4gICAgICBkMiA9IG4xO1xyXG4gICAgICBuMSA9IG4wLnBsdXMocS50aW1lcyhkMikpO1xyXG4gICAgICBuMCA9IGQyO1xyXG4gICAgICBkMiA9IGQ7XHJcbiAgICAgIGQgPSBuLm1pbnVzKHEudGltZXMoZDIpKTtcclxuICAgICAgbiA9IGQyO1xyXG4gICAgfVxyXG5cclxuICAgIGQyID0gZGl2aWRlKG1heEQubWludXMoZDApLCBkMSwgMCwgMSwgMSk7XHJcbiAgICBuMCA9IG4wLnBsdXMoZDIudGltZXMobjEpKTtcclxuICAgIGQwID0gZDAucGx1cyhkMi50aW1lcyhkMSkpO1xyXG4gICAgbjAucyA9IG4xLnMgPSB4LnM7XHJcblxyXG4gICAgLy8gRGV0ZXJtaW5lIHdoaWNoIGZyYWN0aW9uIGlzIGNsb3NlciB0byB4LCBuMC9kMCBvciBuMS9kMT9cclxuICAgIHIgPSBkaXZpZGUobjEsIGQxLCBlLCAxKS5taW51cyh4KS5hYnMoKS5jbXAoZGl2aWRlKG4wLCBkMCwgZSwgMSkubWludXMoeCkuYWJzKCkpIDwgMVxyXG4gICAgICAgID8gW24xLCBkMV0gOiBbbjAsIGQwXTtcclxuXHJcbiAgICBDdG9yLnByZWNpc2lvbiA9IHByO1xyXG4gICAgZXh0ZXJuYWwgPSB0cnVlO1xyXG5cclxuICAgIHJldHVybiByO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIHN0cmluZyByZXByZXNlbnRpbmcgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBpbiBiYXNlIDE2LCByb3VuZCB0byBgc2RgIHNpZ25pZmljYW50XHJcbiAgICogZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJtYC5cclxuICAgKlxyXG4gICAqIElmIHRoZSBvcHRpb25hbCBgc2RgIGFyZ3VtZW50IGlzIHByZXNlbnQgdGhlbiByZXR1cm4gYmluYXJ5IGV4cG9uZW50aWFsIG5vdGF0aW9uLlxyXG4gICAqXHJcbiAgICogW3NkXSB7bnVtYmVyfSBTaWduaWZpY2FudCBkaWdpdHMuIEludGVnZXIsIDEgdG8gTUFYX0RJR0lUUyBpbmNsdXNpdmUuXHJcbiAgICogW3JtXSB7bnVtYmVyfSBSb3VuZGluZyBtb2RlLiBJbnRlZ2VyLCAwIHRvIDggaW5jbHVzaXZlLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC50b0hleGFkZWNpbWFsID0gUC50b0hleCA9IGZ1bmN0aW9uIChzZCwgcm0pIHtcclxuICAgIHJldHVybiB0b1N0cmluZ0JpbmFyeSh0aGlzLCAxNiwgc2QsIHJtKTtcclxuICB9O1xyXG5cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJucyBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBuZWFyZXN0IG11bHRpcGxlIG9mIHRoZSBtYWduaXR1ZGUgb2YgYHlgIHRvIHRoZSB2YWx1ZVxyXG4gICAqIG9mIHRoaXMgRGVjaW1hbC5cclxuICAgKlxyXG4gICAqIElmIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgaXMgZXF1aWRpc3RhbnQgZnJvbSB0d28gbXVsdGlwbGVzIG9mIGB5YCwgdGhlIHJvdW5kaW5nIG1vZGUgYHJtYCxcclxuICAgKiBvciBgRGVjaW1hbC5yb3VuZGluZ2AgaWYgYHJtYCBpcyBvbWl0dGVkLCBkZXRlcm1pbmVzIHRoZSBkaXJlY3Rpb24gb2YgdGhlIG5lYXJlc3QgbXVsdGlwbGUuXHJcbiAgICpcclxuICAgKiBJbiB0aGUgY29udGV4dCBvZiB0aGlzIG1ldGhvZCwgcm91bmRpbmcgbW9kZSA0IChST1VORF9IQUxGX1VQKSBpcyB0aGUgc2FtZSBhcyByb3VuZGluZyBtb2RlIDBcclxuICAgKiAoUk9VTkRfVVApLCBhbmQgc28gb24uXHJcbiAgICpcclxuICAgKiBUaGUgcmV0dXJuIHZhbHVlIHdpbGwgYWx3YXlzIGhhdmUgdGhlIHNhbWUgc2lnbiBhcyB0aGlzIERlY2ltYWwsIHVubGVzcyBlaXRoZXIgdGhpcyBEZWNpbWFsXHJcbiAgICogb3IgYHlgIGlzIE5hTiwgaW4gd2hpY2ggY2FzZSB0aGUgcmV0dXJuIHZhbHVlIHdpbGwgYmUgYWxzbyBiZSBOYU4uXHJcbiAgICpcclxuICAgKiBUaGUgcmV0dXJuIHZhbHVlIGlzIG5vdCBhZmZlY3RlZCBieSB0aGUgdmFsdWUgb2YgYHByZWNpc2lvbmAuXHJcbiAgICpcclxuICAgKiB5IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IFRoZSBtYWduaXR1ZGUgdG8gcm91bmQgdG8gYSBtdWx0aXBsZSBvZi5cclxuICAgKiBbcm1dIHtudW1iZXJ9IFJvdW5kaW5nIG1vZGUuIEludGVnZXIsIDAgdG8gOCBpbmNsdXNpdmUuXHJcbiAgICpcclxuICAgKiAndG9OZWFyZXN0KCkgcm91bmRpbmcgbW9kZSBub3QgYW4gaW50ZWdlcjoge3JtfSdcclxuICAgKiAndG9OZWFyZXN0KCkgcm91bmRpbmcgbW9kZSBvdXQgb2YgcmFuZ2U6IHtybX0nXHJcbiAgICpcclxuICAgKi9cclxuICBQLnRvTmVhcmVzdCA9IGZ1bmN0aW9uICh5LCBybSkge1xyXG4gICAgdmFyIHggPSB0aGlzLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcjtcclxuXHJcbiAgICB4ID0gbmV3IEN0b3IoeCk7XHJcblxyXG4gICAgaWYgKHkgPT0gbnVsbCkge1xyXG5cclxuICAgICAgLy8gSWYgeCBpcyBub3QgZmluaXRlLCByZXR1cm4geC5cclxuICAgICAgaWYgKCF4LmQpIHJldHVybiB4O1xyXG5cclxuICAgICAgeSA9IG5ldyBDdG9yKDEpO1xyXG4gICAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB5ID0gbmV3IEN0b3IoeSk7XHJcbiAgICAgIGlmIChybSAhPT0gdm9pZCAwKSBjaGVja0ludDMyKHJtLCAwLCA4KTtcclxuXHJcbiAgICAgIC8vIElmIHggaXMgbm90IGZpbml0ZSwgcmV0dXJuIHggaWYgeSBpcyBub3QgTmFOLCBlbHNlIE5hTi5cclxuICAgICAgaWYgKCF4LmQpIHJldHVybiB5LnMgPyB4IDogeTtcclxuXHJcbiAgICAgIC8vIElmIHkgaXMgbm90IGZpbml0ZSwgcmV0dXJuIEluZmluaXR5IHdpdGggdGhlIHNpZ24gb2YgeCBpZiB5IGlzIEluZmluaXR5LCBlbHNlIE5hTi5cclxuICAgICAgaWYgKCF5LmQpIHtcclxuICAgICAgICBpZiAoeS5zKSB5LnMgPSB4LnM7XHJcbiAgICAgICAgcmV0dXJuIHk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBJZiB5IGlzIG5vdCB6ZXJvLCBjYWxjdWxhdGUgdGhlIG5lYXJlc3QgbXVsdGlwbGUgb2YgeSB0byB4LlxyXG4gICAgaWYgKHkuZFswXSkge1xyXG4gICAgICBleHRlcm5hbCA9IGZhbHNlO1xyXG4gICAgICBpZiAocm0gPCA0KSBybSA9IFs0LCA1LCA3LCA4XVtybV07XHJcbiAgICAgIHggPSBkaXZpZGUoeCwgeSwgMCwgcm0sIDEpLnRpbWVzKHkpO1xyXG4gICAgICBleHRlcm5hbCA9IHRydWU7XHJcbiAgICAgIGZpbmFsaXNlKHgpO1xyXG5cclxuICAgIC8vIElmIHkgaXMgemVybywgcmV0dXJuIHplcm8gd2l0aCB0aGUgc2lnbiBvZiB4LlxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgeS5zID0geC5zO1xyXG4gICAgICB4ID0geTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4geDtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBjb252ZXJ0ZWQgdG8gYSBudW1iZXIgcHJpbWl0aXZlLlxyXG4gICAqIFplcm8ga2VlcHMgaXRzIHNpZ24uXHJcbiAgICpcclxuICAgKi9cclxuICBQLnRvTnVtYmVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgcmV0dXJuICt0aGlzO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIHN0cmluZyByZXByZXNlbnRpbmcgdGhlIHZhbHVlIG9mIHRoaXMgRGVjaW1hbCBpbiBiYXNlIDgsIHJvdW5kIHRvIGBzZGAgc2lnbmlmaWNhbnRcclxuICAgKiBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm1gLlxyXG4gICAqXHJcbiAgICogSWYgdGhlIG9wdGlvbmFsIGBzZGAgYXJndW1lbnQgaXMgcHJlc2VudCB0aGVuIHJldHVybiBiaW5hcnkgZXhwb25lbnRpYWwgbm90YXRpb24uXHJcbiAgICpcclxuICAgKiBbc2RdIHtudW1iZXJ9IFNpZ25pZmljYW50IGRpZ2l0cy4gSW50ZWdlciwgMSB0byBNQVhfRElHSVRTIGluY2x1c2l2ZS5cclxuICAgKiBbcm1dIHtudW1iZXJ9IFJvdW5kaW5nIG1vZGUuIEludGVnZXIsIDAgdG8gOCBpbmNsdXNpdmUuXHJcbiAgICpcclxuICAgKi9cclxuICBQLnRvT2N0YWwgPSBmdW5jdGlvbiAoc2QsIHJtKSB7XHJcbiAgICByZXR1cm4gdG9TdHJpbmdCaW5hcnkodGhpcywgOCwgc2QsIHJtKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIHJhaXNlZCB0byB0aGUgcG93ZXIgYHlgLCByb3VuZGVkXHJcbiAgICogdG8gYHByZWNpc2lvbmAgc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIEVDTUFTY3JpcHQgY29tcGxpYW50LlxyXG4gICAqXHJcbiAgICogICBwb3coeCwgTmFOKSAgICAgICAgICAgICAgICAgICAgICAgICAgID0gTmFOXHJcbiAgICogICBwb3coeCwgwrEwKSAgICAgICAgICAgICAgICAgICAgICAgICAgICA9IDFcclxuXHJcbiAgICogICBwb3coTmFOLCBub24temVybykgICAgICAgICAgICAgICAgICAgID0gTmFOXHJcbiAgICogICBwb3coYWJzKHgpID4gMSwgK0luZmluaXR5KSAgICAgICAgICAgID0gK0luZmluaXR5XHJcbiAgICogICBwb3coYWJzKHgpID4gMSwgLUluZmluaXR5KSAgICAgICAgICAgID0gKzBcclxuICAgKiAgIHBvdyhhYnMoeCkgPT0gMSwgwrFJbmZpbml0eSkgICAgICAgICAgID0gTmFOXHJcbiAgICogICBwb3coYWJzKHgpIDwgMSwgK0luZmluaXR5KSAgICAgICAgICAgID0gKzBcclxuICAgKiAgIHBvdyhhYnMoeCkgPCAxLCAtSW5maW5pdHkpICAgICAgICAgICAgPSArSW5maW5pdHlcclxuICAgKiAgIHBvdygrSW5maW5pdHksIHkgPiAwKSAgICAgICAgICAgICAgICAgPSArSW5maW5pdHlcclxuICAgKiAgIHBvdygrSW5maW5pdHksIHkgPCAwKSAgICAgICAgICAgICAgICAgPSArMFxyXG4gICAqICAgcG93KC1JbmZpbml0eSwgb2RkIGludGVnZXIgPiAwKSAgICAgICA9IC1JbmZpbml0eVxyXG4gICAqICAgcG93KC1JbmZpbml0eSwgZXZlbiBpbnRlZ2VyID4gMCkgICAgICA9ICtJbmZpbml0eVxyXG4gICAqICAgcG93KC1JbmZpbml0eSwgb2RkIGludGVnZXIgPCAwKSAgICAgICA9IC0wXHJcbiAgICogICBwb3coLUluZmluaXR5LCBldmVuIGludGVnZXIgPCAwKSAgICAgID0gKzBcclxuICAgKiAgIHBvdygrMCwgeSA+IDApICAgICAgICAgICAgICAgICAgICAgICAgPSArMFxyXG4gICAqICAgcG93KCswLCB5IDwgMCkgICAgICAgICAgICAgICAgICAgICAgICA9ICtJbmZpbml0eVxyXG4gICAqICAgcG93KC0wLCBvZGQgaW50ZWdlciA+IDApICAgICAgICAgICAgICA9IC0wXHJcbiAgICogICBwb3coLTAsIGV2ZW4gaW50ZWdlciA+IDApICAgICAgICAgICAgID0gKzBcclxuICAgKiAgIHBvdygtMCwgb2RkIGludGVnZXIgPCAwKSAgICAgICAgICAgICAgPSAtSW5maW5pdHlcclxuICAgKiAgIHBvdygtMCwgZXZlbiBpbnRlZ2VyIDwgMCkgICAgICAgICAgICAgPSArSW5maW5pdHlcclxuICAgKiAgIHBvdyhmaW5pdGUgeCA8IDAsIGZpbml0ZSBub24taW50ZWdlcikgPSBOYU5cclxuICAgKlxyXG4gICAqIEZvciBub24taW50ZWdlciBvciB2ZXJ5IGxhcmdlIGV4cG9uZW50cyBwb3coeCwgeSkgaXMgY2FsY3VsYXRlZCB1c2luZ1xyXG4gICAqXHJcbiAgICogICB4XnkgPSBleHAoeSpsbih4KSlcclxuICAgKlxyXG4gICAqIEFzc3VtaW5nIHRoZSBmaXJzdCAxNSByb3VuZGluZyBkaWdpdHMgYXJlIGVhY2ggZXF1YWxseSBsaWtlbHkgdG8gYmUgYW55IGRpZ2l0IDAtOSwgdGhlXHJcbiAgICogcHJvYmFiaWxpdHkgb2YgYW4gaW5jb3JyZWN0bHkgcm91bmRlZCByZXN1bHRcclxuICAgKiBQKFs0OV05ezE0fSB8IFs1MF0wezE0fSkgPSAyICogMC4yICogMTBeLTE0ID0gNGUtMTUgPSAxLzIuNWUrMTRcclxuICAgKiBpLmUuIDEgaW4gMjUwLDAwMCwwMDAsMDAwLDAwMFxyXG4gICAqXHJcbiAgICogSWYgYSByZXN1bHQgaXMgaW5jb3JyZWN0bHkgcm91bmRlZCB0aGUgbWF4aW11bSBlcnJvciB3aWxsIGJlIDEgdWxwICh1bml0IGluIGxhc3QgcGxhY2UpLlxyXG4gICAqXHJcbiAgICogeSB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfSBUaGUgcG93ZXIgdG8gd2hpY2ggdG8gcmFpc2UgdGhpcyBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC50b1Bvd2VyID0gUC5wb3cgPSBmdW5jdGlvbiAoeSkge1xyXG4gICAgdmFyIGUsIGssIHByLCByLCBybSwgcyxcclxuICAgICAgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yLFxyXG4gICAgICB5biA9ICsoeSA9IG5ldyBDdG9yKHkpKTtcclxuXHJcbiAgICAvLyBFaXRoZXIgwrFJbmZpbml0eSwgTmFOIG9yIMKxMD9cclxuICAgIGlmICgheC5kIHx8ICF5LmQgfHwgIXguZFswXSB8fCAheS5kWzBdKSByZXR1cm4gbmV3IEN0b3IobWF0aHBvdygreCwgeW4pKTtcclxuXHJcbiAgICB4ID0gbmV3IEN0b3IoeCk7XHJcblxyXG4gICAgaWYgKHguZXEoMSkpIHJldHVybiB4O1xyXG5cclxuICAgIHByID0gQ3Rvci5wcmVjaXNpb247XHJcbiAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcblxyXG4gICAgaWYgKHkuZXEoMSkpIHJldHVybiBmaW5hbGlzZSh4LCBwciwgcm0pO1xyXG5cclxuICAgIC8vIHkgZXhwb25lbnRcclxuICAgIGUgPSBtYXRoZmxvb3IoeS5lIC8gTE9HX0JBU0UpO1xyXG5cclxuICAgIC8vIElmIHkgaXMgYSBzbWFsbCBpbnRlZ2VyIHVzZSB0aGUgJ2V4cG9uZW50aWF0aW9uIGJ5IHNxdWFyaW5nJyBhbGdvcml0aG0uXHJcbiAgICBpZiAoZSA+PSB5LmQubGVuZ3RoIC0gMSAmJiAoayA9IHluIDwgMCA/IC15biA6IHluKSA8PSBNQVhfU0FGRV9JTlRFR0VSKSB7XHJcbiAgICAgIHIgPSBpbnRQb3coQ3RvciwgeCwgaywgcHIpO1xyXG4gICAgICByZXR1cm4geS5zIDwgMCA/IG5ldyBDdG9yKDEpLmRpdihyKSA6IGZpbmFsaXNlKHIsIHByLCBybSk7XHJcbiAgICB9XHJcblxyXG4gICAgcyA9IHgucztcclxuXHJcbiAgICAvLyBpZiB4IGlzIG5lZ2F0aXZlXHJcbiAgICBpZiAocyA8IDApIHtcclxuXHJcbiAgICAgIC8vIGlmIHkgaXMgbm90IGFuIGludGVnZXJcclxuICAgICAgaWYgKGUgPCB5LmQubGVuZ3RoIC0gMSkgcmV0dXJuIG5ldyBDdG9yKE5hTik7XHJcblxyXG4gICAgICAvLyBSZXN1bHQgaXMgcG9zaXRpdmUgaWYgeCBpcyBuZWdhdGl2ZSBhbmQgdGhlIGxhc3QgZGlnaXQgb2YgaW50ZWdlciB5IGlzIGV2ZW4uXHJcbiAgICAgIGlmICgoeS5kW2VdICYgMSkgPT0gMCkgcyA9IDE7XHJcblxyXG4gICAgICAvLyBpZiB4LmVxKC0xKVxyXG4gICAgICBpZiAoeC5lID09IDAgJiYgeC5kWzBdID09IDEgJiYgeC5kLmxlbmd0aCA9PSAxKSB7XHJcbiAgICAgICAgeC5zID0gcztcclxuICAgICAgICByZXR1cm4geDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIEVzdGltYXRlIHJlc3VsdCBleHBvbmVudC5cclxuICAgIC8vIHheeSA9IDEwXmUsICB3aGVyZSBlID0geSAqIGxvZzEwKHgpXHJcbiAgICAvLyBsb2cxMCh4KSA9IGxvZzEwKHhfc2lnbmlmaWNhbmQpICsgeF9leHBvbmVudFxyXG4gICAgLy8gbG9nMTAoeF9zaWduaWZpY2FuZCkgPSBsbih4X3NpZ25pZmljYW5kKSAvIGxuKDEwKVxyXG4gICAgayA9IG1hdGhwb3coK3gsIHluKTtcclxuICAgIGUgPSBrID09IDAgfHwgIWlzRmluaXRlKGspXHJcbiAgICAgID8gbWF0aGZsb29yKHluICogKE1hdGgubG9nKCcwLicgKyBkaWdpdHNUb1N0cmluZyh4LmQpKSAvIE1hdGguTE4xMCArIHguZSArIDEpKVxyXG4gICAgICA6IG5ldyBDdG9yKGsgKyAnJykuZTtcclxuXHJcbiAgICAvLyBFeHBvbmVudCBlc3RpbWF0ZSBtYXkgYmUgaW5jb3JyZWN0IGUuZy4geDogMC45OTk5OTk5OTk5OTk5OTk5OTksIHk6IDIuMjksIGU6IDAsIHIuZTogLTEuXHJcblxyXG4gICAgLy8gT3ZlcmZsb3cvdW5kZXJmbG93P1xyXG4gICAgaWYgKGUgPiBDdG9yLm1heEUgKyAxIHx8IGUgPCBDdG9yLm1pbkUgLSAxKSByZXR1cm4gbmV3IEN0b3IoZSA+IDAgPyBzIC8gMCA6IDApO1xyXG5cclxuICAgIGV4dGVybmFsID0gZmFsc2U7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0geC5zID0gMTtcclxuXHJcbiAgICAvLyBFc3RpbWF0ZSB0aGUgZXh0cmEgZ3VhcmQgZGlnaXRzIG5lZWRlZCB0byBlbnN1cmUgZml2ZSBjb3JyZWN0IHJvdW5kaW5nIGRpZ2l0cyBmcm9tXHJcbiAgICAvLyBuYXR1cmFsTG9nYXJpdGhtKHgpLiBFeGFtcGxlIG9mIGZhaWx1cmUgd2l0aG91dCB0aGVzZSBleHRyYSBkaWdpdHMgKHByZWNpc2lvbjogMTApOlxyXG4gICAgLy8gbmV3IERlY2ltYWwoMi4zMjQ1NikucG93KCcyMDg3OTg3NDM2NTM0NTY2LjQ2NDExJylcclxuICAgIC8vIHNob3VsZCBiZSAxLjE2MjM3NzgyM2UrNzY0OTE0OTA1MTczODE1LCBidXQgaXMgMS4xNjIzNTU4MjNlKzc2NDkxNDkwNTE3MzgxNVxyXG4gICAgayA9IE1hdGgubWluKDEyLCAoZSArICcnKS5sZW5ndGgpO1xyXG5cclxuICAgIC8vIHIgPSB4XnkgPSBleHAoeSpsbih4KSlcclxuICAgIHIgPSBuYXR1cmFsRXhwb25lbnRpYWwoeS50aW1lcyhuYXR1cmFsTG9nYXJpdGhtKHgsIHByICsgaykpLCBwcik7XHJcblxyXG4gICAgLy8gciBtYXkgYmUgSW5maW5pdHksIGUuZy4gKDAuOTk5OTk5OTk5OTk5OTk5OSkucG93KC0xZSs0MClcclxuICAgIGlmIChyLmQpIHtcclxuXHJcbiAgICAgIC8vIFRydW5jYXRlIHRvIHRoZSByZXF1aXJlZCBwcmVjaXNpb24gcGx1cyBmaXZlIHJvdW5kaW5nIGRpZ2l0cy5cclxuICAgICAgciA9IGZpbmFsaXNlKHIsIHByICsgNSwgMSk7XHJcblxyXG4gICAgICAvLyBJZiB0aGUgcm91bmRpbmcgZGlnaXRzIGFyZSBbNDldOTk5OSBvciBbNTBdMDAwMCBpbmNyZWFzZSB0aGUgcHJlY2lzaW9uIGJ5IDEwIGFuZCByZWNhbGN1bGF0ZVxyXG4gICAgICAvLyB0aGUgcmVzdWx0LlxyXG4gICAgICBpZiAoY2hlY2tSb3VuZGluZ0RpZ2l0cyhyLmQsIHByLCBybSkpIHtcclxuICAgICAgICBlID0gcHIgKyAxMDtcclxuXHJcbiAgICAgICAgLy8gVHJ1bmNhdGUgdG8gdGhlIGluY3JlYXNlZCBwcmVjaXNpb24gcGx1cyBmaXZlIHJvdW5kaW5nIGRpZ2l0cy5cclxuICAgICAgICByID0gZmluYWxpc2UobmF0dXJhbEV4cG9uZW50aWFsKHkudGltZXMobmF0dXJhbExvZ2FyaXRobSh4LCBlICsgaykpLCBlKSwgZSArIDUsIDEpO1xyXG5cclxuICAgICAgICAvLyBDaGVjayBmb3IgMTQgbmluZXMgZnJvbSB0aGUgMm5kIHJvdW5kaW5nIGRpZ2l0ICh0aGUgZmlyc3Qgcm91bmRpbmcgZGlnaXQgbWF5IGJlIDQgb3IgOSkuXHJcbiAgICAgICAgaWYgKCtkaWdpdHNUb1N0cmluZyhyLmQpLnNsaWNlKHByICsgMSwgcHIgKyAxNSkgKyAxID09IDFlMTQpIHtcclxuICAgICAgICAgIHIgPSBmaW5hbGlzZShyLCBwciArIDEsIDApO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHIucyA9IHM7XHJcbiAgICBleHRlcm5hbCA9IHRydWU7XHJcbiAgICBDdG9yLnJvdW5kaW5nID0gcm07XHJcblxyXG4gICAgcmV0dXJuIGZpbmFsaXNlKHIsIHByLCBybSk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgc3RyaW5nIHJlcHJlc2VudGluZyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsIHJvdW5kZWQgdG8gYHNkYCBzaWduaWZpY2FudCBkaWdpdHNcclxuICAgKiB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiBSZXR1cm4gZXhwb25lbnRpYWwgbm90YXRpb24gaWYgYHNkYCBpcyBsZXNzIHRoYW4gdGhlIG51bWJlciBvZiBkaWdpdHMgbmVjZXNzYXJ5IHRvIHJlcHJlc2VudFxyXG4gICAqIHRoZSBpbnRlZ2VyIHBhcnQgb2YgdGhlIHZhbHVlIGluIG5vcm1hbCBub3RhdGlvbi5cclxuICAgKlxyXG4gICAqIFtzZF0ge251bWJlcn0gU2lnbmlmaWNhbnQgZGlnaXRzLiBJbnRlZ2VyLCAxIHRvIE1BWF9ESUdJVFMgaW5jbHVzaXZlLlxyXG4gICAqIFtybV0ge251bWJlcn0gUm91bmRpbmcgbW9kZS4gSW50ZWdlciwgMCB0byA4IGluY2x1c2l2ZS5cclxuICAgKlxyXG4gICAqL1xyXG4gIFAudG9QcmVjaXNpb24gPSBmdW5jdGlvbiAoc2QsIHJtKSB7XHJcbiAgICB2YXIgc3RyLFxyXG4gICAgICB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3I7XHJcblxyXG4gICAgaWYgKHNkID09PSB2b2lkIDApIHtcclxuICAgICAgc3RyID0gZmluaXRlVG9TdHJpbmcoeCwgeC5lIDw9IEN0b3IudG9FeHBOZWcgfHwgeC5lID49IEN0b3IudG9FeHBQb3MpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY2hlY2tJbnQzMihzZCwgMSwgTUFYX0RJR0lUUyk7XHJcblxyXG4gICAgICBpZiAocm0gPT09IHZvaWQgMCkgcm0gPSBDdG9yLnJvdW5kaW5nO1xyXG4gICAgICBlbHNlIGNoZWNrSW50MzIocm0sIDAsIDgpO1xyXG5cclxuICAgICAgeCA9IGZpbmFsaXNlKG5ldyBDdG9yKHgpLCBzZCwgcm0pO1xyXG4gICAgICBzdHIgPSBmaW5pdGVUb1N0cmluZyh4LCBzZCA8PSB4LmUgfHwgeC5lIDw9IEN0b3IudG9FeHBOZWcsIHNkKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4geC5pc05lZygpICYmICF4LmlzWmVybygpID8gJy0nICsgc3RyIDogc3RyO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgcm91bmRlZCB0byBhIG1heGltdW0gb2YgYHNkYFxyXG4gICAqIHNpZ25pZmljYW50IGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGBybWAsIG9yIHRvIGBwcmVjaXNpb25gIGFuZCBgcm91bmRpbmdgIHJlc3BlY3RpdmVseSBpZlxyXG4gICAqIG9taXR0ZWQuXHJcbiAgICpcclxuICAgKiBbc2RdIHtudW1iZXJ9IFNpZ25pZmljYW50IGRpZ2l0cy4gSW50ZWdlciwgMSB0byBNQVhfRElHSVRTIGluY2x1c2l2ZS5cclxuICAgKiBbcm1dIHtudW1iZXJ9IFJvdW5kaW5nIG1vZGUuIEludGVnZXIsIDAgdG8gOCBpbmNsdXNpdmUuXHJcbiAgICpcclxuICAgKiAndG9TRCgpIGRpZ2l0cyBvdXQgb2YgcmFuZ2U6IHtzZH0nXHJcbiAgICogJ3RvU0QoKSBkaWdpdHMgbm90IGFuIGludGVnZXI6IHtzZH0nXHJcbiAgICogJ3RvU0QoKSByb3VuZGluZyBtb2RlIG5vdCBhbiBpbnRlZ2VyOiB7cm19J1xyXG4gICAqICd0b1NEKCkgcm91bmRpbmcgbW9kZSBvdXQgb2YgcmFuZ2U6IHtybX0nXHJcbiAgICpcclxuICAgKi9cclxuICBQLnRvU2lnbmlmaWNhbnREaWdpdHMgPSBQLnRvU0QgPSBmdW5jdGlvbiAoc2QsIHJtKSB7XHJcbiAgICB2YXIgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIGlmIChzZCA9PT0gdm9pZCAwKSB7XHJcbiAgICAgIHNkID0gQ3Rvci5wcmVjaXNpb247XHJcbiAgICAgIHJtID0gQ3Rvci5yb3VuZGluZztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNoZWNrSW50MzIoc2QsIDEsIE1BWF9ESUdJVFMpO1xyXG5cclxuICAgICAgaWYgKHJtID09PSB2b2lkIDApIHJtID0gQ3Rvci5yb3VuZGluZztcclxuICAgICAgZWxzZSBjaGVja0ludDMyKHJtLCAwLCA4KTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZmluYWxpc2UobmV3IEN0b3IoeCksIHNkLCBybSk7XHJcbiAgfTtcclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgc3RyaW5nIHJlcHJlc2VudGluZyB0aGUgdmFsdWUgb2YgdGhpcyBEZWNpbWFsLlxyXG4gICAqXHJcbiAgICogUmV0dXJuIGV4cG9uZW50aWFsIG5vdGF0aW9uIGlmIHRoaXMgRGVjaW1hbCBoYXMgYSBwb3NpdGl2ZSBleHBvbmVudCBlcXVhbCB0byBvciBncmVhdGVyIHRoYW5cclxuICAgKiBgdG9FeHBQb3NgLCBvciBhIG5lZ2F0aXZlIGV4cG9uZW50IGVxdWFsIHRvIG9yIGxlc3MgdGhhbiBgdG9FeHBOZWdgLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC50b1N0cmluZyA9IGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciB4ID0gdGhpcyxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3IsXHJcbiAgICAgIHN0ciA9IGZpbml0ZVRvU3RyaW5nKHgsIHguZSA8PSBDdG9yLnRvRXhwTmVnIHx8IHguZSA+PSBDdG9yLnRvRXhwUG9zKTtcclxuXHJcbiAgICByZXR1cm4geC5pc05lZygpICYmICF4LmlzWmVybygpID8gJy0nICsgc3RyIDogc3RyO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwgdHJ1bmNhdGVkIHRvIGEgd2hvbGUgbnVtYmVyLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC50cnVuY2F0ZWQgPSBQLnRydW5jID0gZnVuY3Rpb24gKCkge1xyXG4gICAgcmV0dXJuIGZpbmFsaXNlKG5ldyB0aGlzLmNvbnN0cnVjdG9yKHRoaXMpLCB0aGlzLmUgKyAxLCAxKTtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBzdHJpbmcgcmVwcmVzZW50aW5nIHRoZSB2YWx1ZSBvZiB0aGlzIERlY2ltYWwuXHJcbiAgICogVW5saWtlIGB0b1N0cmluZ2AsIG5lZ2F0aXZlIHplcm8gd2lsbCBpbmNsdWRlIHRoZSBtaW51cyBzaWduLlxyXG4gICAqXHJcbiAgICovXHJcbiAgUC52YWx1ZU9mID0gUC50b0pTT04gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgeCA9IHRoaXMsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yLFxyXG4gICAgICBzdHIgPSBmaW5pdGVUb1N0cmluZyh4LCB4LmUgPD0gQ3Rvci50b0V4cE5lZyB8fCB4LmUgPj0gQ3Rvci50b0V4cFBvcyk7XHJcblxyXG4gICAgcmV0dXJuIHguaXNOZWcoKSA/ICctJyArIHN0ciA6IHN0cjtcclxuICB9O1xyXG5cclxuXHJcbiAgLypcclxuICAvLyBBZGQgYWxpYXNlcyB0byBtYXRjaCBCaWdEZWNpbWFsIG1ldGhvZCBuYW1lcy5cclxuICAvLyBQLmFkZCA9IFAucGx1cztcclxuICBQLnN1YnRyYWN0ID0gUC5taW51cztcclxuICBQLm11bHRpcGx5ID0gUC50aW1lcztcclxuICBQLmRpdmlkZSA9IFAuZGl2O1xyXG4gIFAucmVtYWluZGVyID0gUC5tb2Q7XHJcbiAgUC5jb21wYXJlVG8gPSBQLmNtcDtcclxuICBQLm5lZ2F0ZSA9IFAubmVnO1xyXG4gICAqL1xyXG5cclxuXHJcbiAgLy8gSGVscGVyIGZ1bmN0aW9ucyBmb3IgRGVjaW1hbC5wcm90b3R5cGUgKFApIGFuZC9vciBEZWNpbWFsIG1ldGhvZHMsIGFuZCB0aGVpciBjYWxsZXJzLlxyXG5cclxuXHJcbiAgLypcclxuICAgKiAgZGlnaXRzVG9TdHJpbmcgICAgICAgICAgIFAuY3ViZVJvb3QsIFAubG9nYXJpdGhtLCBQLnNxdWFyZVJvb3QsIFAudG9GcmFjdGlvbiwgUC50b1Bvd2VyLFxyXG4gICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgZmluaXRlVG9TdHJpbmcsIG5hdHVyYWxFeHBvbmVudGlhbCwgbmF0dXJhbExvZ2FyaXRobVxyXG4gICAqICBjaGVja0ludDMyICAgICAgICAgICAgICAgUC50b0RlY2ltYWxQbGFjZXMsIFAudG9FeHBvbmVudGlhbCwgUC50b0ZpeGVkLCBQLnRvTmVhcmVzdCxcclxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgIFAudG9QcmVjaXNpb24sIFAudG9TaWduaWZpY2FudERpZ2l0cywgdG9TdHJpbmdCaW5hcnksIHJhbmRvbVxyXG4gICAqICBjaGVja1JvdW5kaW5nRGlnaXRzICAgICAgUC5sb2dhcml0aG0sIFAudG9Qb3dlciwgbmF0dXJhbEV4cG9uZW50aWFsLCBuYXR1cmFsTG9nYXJpdGhtXHJcbiAgICogIGNvbnZlcnRCYXNlICAgICAgICAgICAgICB0b1N0cmluZ0JpbmFyeSwgcGFyc2VPdGhlclxyXG4gICAqICBjb3MgICAgICAgICAgICAgICAgICAgICAgUC5jb3NcclxuICAgKiAgZGl2aWRlICAgICAgICAgICAgICAgICAgIFAuYXRhbmgsIFAuY3ViZVJvb3QsIFAuZGl2aWRlZEJ5LCBQLmRpdmlkZWRUb0ludGVnZXJCeSxcclxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgIFAubG9nYXJpdGhtLCBQLm1vZHVsbywgUC5zcXVhcmVSb290LCBQLnRhbiwgUC50YW5oLCBQLnRvRnJhY3Rpb24sXHJcbiAgICogICAgICAgICAgICAgICAgICAgICAgICAgICBQLnRvTmVhcmVzdCwgdG9TdHJpbmdCaW5hcnksIG5hdHVyYWxFeHBvbmVudGlhbCwgbmF0dXJhbExvZ2FyaXRobSxcclxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRheWxvclNlcmllcywgYXRhbjIsIHBhcnNlT3RoZXJcclxuICAgKiAgZmluYWxpc2UgICAgICAgICAgICAgICAgIFAuYWJzb2x1dGVWYWx1ZSwgUC5hdGFuLCBQLmF0YW5oLCBQLmNlaWwsIFAuY29zLCBQLmNvc2gsXHJcbiAgICogICAgICAgICAgICAgICAgICAgICAgICAgICBQLmN1YmVSb290LCBQLmRpdmlkZWRUb0ludGVnZXJCeSwgUC5mbG9vciwgUC5sb2dhcml0aG0sIFAubWludXMsXHJcbiAgICogICAgICAgICAgICAgICAgICAgICAgICAgICBQLm1vZHVsbywgUC5uZWdhdGVkLCBQLnBsdXMsIFAucm91bmQsIFAuc2luLCBQLnNpbmgsIFAuc3F1YXJlUm9vdCxcclxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgIFAudGFuLCBQLnRpbWVzLCBQLnRvRGVjaW1hbFBsYWNlcywgUC50b0V4cG9uZW50aWFsLCBQLnRvRml4ZWQsXHJcbiAgICogICAgICAgICAgICAgICAgICAgICAgICAgICBQLnRvTmVhcmVzdCwgUC50b1Bvd2VyLCBQLnRvUHJlY2lzaW9uLCBQLnRvU2lnbmlmaWNhbnREaWdpdHMsXHJcbiAgICogICAgICAgICAgICAgICAgICAgICAgICAgICBQLnRydW5jYXRlZCwgZGl2aWRlLCBnZXRMbjEwLCBnZXRQaSwgbmF0dXJhbEV4cG9uZW50aWFsLFxyXG4gICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgbmF0dXJhbExvZ2FyaXRobSwgY2VpbCwgZmxvb3IsIHJvdW5kLCB0cnVuY1xyXG4gICAqICBmaW5pdGVUb1N0cmluZyAgICAgICAgICAgUC50b0V4cG9uZW50aWFsLCBQLnRvRml4ZWQsIFAudG9QcmVjaXNpb24sIFAudG9TdHJpbmcsIFAudmFsdWVPZixcclxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvU3RyaW5nQmluYXJ5XHJcbiAgICogIGdldEJhc2UxMEV4cG9uZW50ICAgICAgICBQLm1pbnVzLCBQLnBsdXMsIFAudGltZXMsIHBhcnNlT3RoZXJcclxuICAgKiAgZ2V0TG4xMCAgICAgICAgICAgICAgICAgIFAubG9nYXJpdGhtLCBuYXR1cmFsTG9nYXJpdGhtXHJcbiAgICogIGdldFBpICAgICAgICAgICAgICAgICAgICBQLmFjb3MsIFAuYXNpbiwgUC5hdGFuLCB0b0xlc3NUaGFuSGFsZlBpLCBhdGFuMlxyXG4gICAqICBnZXRQcmVjaXNpb24gICAgICAgICAgICAgUC5wcmVjaXNpb24sIFAudG9GcmFjdGlvblxyXG4gICAqICBnZXRaZXJvU3RyaW5nICAgICAgICAgICAgZGlnaXRzVG9TdHJpbmcsIGZpbml0ZVRvU3RyaW5nXHJcbiAgICogIGludFBvdyAgICAgICAgICAgICAgICAgICBQLnRvUG93ZXIsIHBhcnNlT3RoZXJcclxuICAgKiAgaXNPZGQgICAgICAgICAgICAgICAgICAgIHRvTGVzc1RoYW5IYWxmUGlcclxuICAgKiAgbWF4T3JNaW4gICAgICAgICAgICAgICAgIG1heCwgbWluXHJcbiAgICogIG5hdHVyYWxFeHBvbmVudGlhbCAgICAgICBQLm5hdHVyYWxFeHBvbmVudGlhbCwgUC50b1Bvd2VyXHJcbiAgICogIG5hdHVyYWxMb2dhcml0aG0gICAgICAgICBQLmFjb3NoLCBQLmFzaW5oLCBQLmF0YW5oLCBQLmxvZ2FyaXRobSwgUC5uYXR1cmFsTG9nYXJpdGhtLFxyXG4gICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgUC50b1Bvd2VyLCBuYXR1cmFsRXhwb25lbnRpYWxcclxuICAgKiAgbm9uRmluaXRlVG9TdHJpbmcgICAgICAgIGZpbml0ZVRvU3RyaW5nLCB0b1N0cmluZ0JpbmFyeVxyXG4gICAqICBwYXJzZURlY2ltYWwgICAgICAgICAgICAgRGVjaW1hbFxyXG4gICAqICBwYXJzZU90aGVyICAgICAgICAgICAgICAgRGVjaW1hbFxyXG4gICAqICBzaW4gICAgICAgICAgICAgICAgICAgICAgUC5zaW5cclxuICAgKiAgdGF5bG9yU2VyaWVzICAgICAgICAgICAgIFAuY29zaCwgUC5zaW5oLCBjb3MsIHNpblxyXG4gICAqICB0b0xlc3NUaGFuSGFsZlBpICAgICAgICAgUC5jb3MsIFAuc2luXHJcbiAgICogIHRvU3RyaW5nQmluYXJ5ICAgICAgICAgICBQLnRvQmluYXJ5LCBQLnRvSGV4YWRlY2ltYWwsIFAudG9PY3RhbFxyXG4gICAqICB0cnVuY2F0ZSAgICAgICAgICAgICAgICAgaW50UG93XHJcbiAgICpcclxuICAgKiAgVGhyb3dzOiAgICAgICAgICAgICAgICAgIFAubG9nYXJpdGhtLCBQLnByZWNpc2lvbiwgUC50b0ZyYWN0aW9uLCBjaGVja0ludDMyLCBnZXRMbjEwLCBnZXRQaSxcclxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hdHVyYWxMb2dhcml0aG0sIGNvbmZpZywgcGFyc2VPdGhlciwgcmFuZG9tLCBEZWNpbWFsXHJcbiAgICovXHJcblxyXG5cclxuICBmdW5jdGlvbiBkaWdpdHNUb1N0cmluZyhkKSB7XHJcbiAgICB2YXIgaSwgaywgd3MsXHJcbiAgICAgIGluZGV4T2ZMYXN0V29yZCA9IGQubGVuZ3RoIC0gMSxcclxuICAgICAgc3RyID0gJycsXHJcbiAgICAgIHcgPSBkWzBdO1xyXG5cclxuICAgIGlmIChpbmRleE9mTGFzdFdvcmQgPiAwKSB7XHJcbiAgICAgIHN0ciArPSB3O1xyXG4gICAgICBmb3IgKGkgPSAxOyBpIDwgaW5kZXhPZkxhc3RXb3JkOyBpKyspIHtcclxuICAgICAgICB3cyA9IGRbaV0gKyAnJztcclxuICAgICAgICBrID0gTE9HX0JBU0UgLSB3cy5sZW5ndGg7XHJcbiAgICAgICAgaWYgKGspIHN0ciArPSBnZXRaZXJvU3RyaW5nKGspO1xyXG4gICAgICAgIHN0ciArPSB3cztcclxuICAgICAgfVxyXG5cclxuICAgICAgdyA9IGRbaV07XHJcbiAgICAgIHdzID0gdyArICcnO1xyXG4gICAgICBrID0gTE9HX0JBU0UgLSB3cy5sZW5ndGg7XHJcbiAgICAgIGlmIChrKSBzdHIgKz0gZ2V0WmVyb1N0cmluZyhrKTtcclxuICAgIH0gZWxzZSBpZiAodyA9PT0gMCkge1xyXG4gICAgICByZXR1cm4gJzAnO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJlbW92ZSB0cmFpbGluZyB6ZXJvcyBvZiBsYXN0IHcuXHJcbiAgICBmb3IgKDsgdyAlIDEwID09PSAwOykgdyAvPSAxMDtcclxuXHJcbiAgICByZXR1cm4gc3RyICsgdztcclxuICB9XHJcblxyXG5cclxuICBmdW5jdGlvbiBjaGVja0ludDMyKGksIG1pbiwgbWF4KSB7XHJcbiAgICBpZiAoaSAhPT0gfn5pIHx8IGkgPCBtaW4gfHwgaSA+IG1heCkge1xyXG4gICAgICB0aHJvdyBFcnJvcihpbnZhbGlkQXJndW1lbnQgKyBpKTtcclxuICAgIH1cclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIENoZWNrIDUgcm91bmRpbmcgZGlnaXRzIGlmIGByZXBlYXRpbmdgIGlzIG51bGwsIDQgb3RoZXJ3aXNlLlxyXG4gICAqIGByZXBlYXRpbmcgPT0gbnVsbGAgaWYgY2FsbGVyIGlzIGBsb2dgIG9yIGBwb3dgLFxyXG4gICAqIGByZXBlYXRpbmcgIT0gbnVsbGAgaWYgY2FsbGVyIGlzIGBuYXR1cmFsTG9nYXJpdGhtYCBvciBgbmF0dXJhbEV4cG9uZW50aWFsYC5cclxuICAgKi9cclxuICBmdW5jdGlvbiBjaGVja1JvdW5kaW5nRGlnaXRzKGQsIGksIHJtLCByZXBlYXRpbmcpIHtcclxuICAgIHZhciBkaSwgaywgciwgcmQ7XHJcblxyXG4gICAgLy8gR2V0IHRoZSBsZW5ndGggb2YgdGhlIGZpcnN0IHdvcmQgb2YgdGhlIGFycmF5IGQuXHJcbiAgICBmb3IgKGsgPSBkWzBdOyBrID49IDEwOyBrIC89IDEwKSAtLWk7XHJcblxyXG4gICAgLy8gSXMgdGhlIHJvdW5kaW5nIGRpZ2l0IGluIHRoZSBmaXJzdCB3b3JkIG9mIGQ/XHJcbiAgICBpZiAoLS1pIDwgMCkge1xyXG4gICAgICBpICs9IExPR19CQVNFO1xyXG4gICAgICBkaSA9IDA7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBkaSA9IE1hdGguY2VpbCgoaSArIDEpIC8gTE9HX0JBU0UpO1xyXG4gICAgICBpICU9IExPR19CQVNFO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGkgaXMgdGhlIGluZGV4ICgwIC0gNikgb2YgdGhlIHJvdW5kaW5nIGRpZ2l0LlxyXG4gICAgLy8gRS5nLiBpZiB3aXRoaW4gdGhlIHdvcmQgMzQ4NzU2MyB0aGUgZmlyc3Qgcm91bmRpbmcgZGlnaXQgaXMgNSxcclxuICAgIC8vIHRoZW4gaSA9IDQsIGsgPSAxMDAwLCByZCA9IDM0ODc1NjMgJSAxMDAwID0gNTYzXHJcbiAgICBrID0gbWF0aHBvdygxMCwgTE9HX0JBU0UgLSBpKTtcclxuICAgIHJkID0gZFtkaV0gJSBrIHwgMDtcclxuXHJcbiAgICBpZiAocmVwZWF0aW5nID09IG51bGwpIHtcclxuICAgICAgaWYgKGkgPCAzKSB7XHJcbiAgICAgICAgaWYgKGkgPT0gMCkgcmQgPSByZCAvIDEwMCB8IDA7XHJcbiAgICAgICAgZWxzZSBpZiAoaSA9PSAxKSByZCA9IHJkIC8gMTAgfCAwO1xyXG4gICAgICAgIHIgPSBybSA8IDQgJiYgcmQgPT0gOTk5OTkgfHwgcm0gPiAzICYmIHJkID09IDQ5OTk5IHx8IHJkID09IDUwMDAwIHx8IHJkID09IDA7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgciA9IChybSA8IDQgJiYgcmQgKyAxID09IGsgfHwgcm0gPiAzICYmIHJkICsgMSA9PSBrIC8gMikgJiZcclxuICAgICAgICAgIChkW2RpICsgMV0gLyBrIC8gMTAwIHwgMCkgPT0gbWF0aHBvdygxMCwgaSAtIDIpIC0gMSB8fFxyXG4gICAgICAgICAgICAocmQgPT0gayAvIDIgfHwgcmQgPT0gMCkgJiYgKGRbZGkgKyAxXSAvIGsgLyAxMDAgfCAwKSA9PSAwO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoaSA8IDQpIHtcclxuICAgICAgICBpZiAoaSA9PSAwKSByZCA9IHJkIC8gMTAwMCB8IDA7XHJcbiAgICAgICAgZWxzZSBpZiAoaSA9PSAxKSByZCA9IHJkIC8gMTAwIHwgMDtcclxuICAgICAgICBlbHNlIGlmIChpID09IDIpIHJkID0gcmQgLyAxMCB8IDA7XHJcbiAgICAgICAgciA9IChyZXBlYXRpbmcgfHwgcm0gPCA0KSAmJiByZCA9PSA5OTk5IHx8ICFyZXBlYXRpbmcgJiYgcm0gPiAzICYmIHJkID09IDQ5OTk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgciA9ICgocmVwZWF0aW5nIHx8IHJtIDwgNCkgJiYgcmQgKyAxID09IGsgfHxcclxuICAgICAgICAoIXJlcGVhdGluZyAmJiBybSA+IDMpICYmIHJkICsgMSA9PSBrIC8gMikgJiZcclxuICAgICAgICAgIChkW2RpICsgMV0gLyBrIC8gMTAwMCB8IDApID09IG1hdGhwb3coMTAsIGkgLSAzKSAtIDE7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gcjtcclxuICB9XHJcblxyXG5cclxuICAvLyBDb252ZXJ0IHN0cmluZyBvZiBgYmFzZUluYCB0byBhbiBhcnJheSBvZiBudW1iZXJzIG9mIGBiYXNlT3V0YC5cclxuICAvLyBFZy4gY29udmVydEJhc2UoJzI1NScsIDEwLCAxNikgcmV0dXJucyBbMTUsIDE1XS5cclxuICAvLyBFZy4gY29udmVydEJhc2UoJ2ZmJywgMTYsIDEwKSByZXR1cm5zIFsyLCA1LCA1XS5cclxuICBmdW5jdGlvbiBjb252ZXJ0QmFzZShzdHIsIGJhc2VJbiwgYmFzZU91dCkge1xyXG4gICAgdmFyIGosXHJcbiAgICAgIGFyciA9IFswXSxcclxuICAgICAgYXJyTCxcclxuICAgICAgaSA9IDAsXHJcbiAgICAgIHN0ckwgPSBzdHIubGVuZ3RoO1xyXG5cclxuICAgIGZvciAoOyBpIDwgc3RyTDspIHtcclxuICAgICAgZm9yIChhcnJMID0gYXJyLmxlbmd0aDsgYXJyTC0tOykgYXJyW2FyckxdICo9IGJhc2VJbjtcclxuICAgICAgYXJyWzBdICs9IE5VTUVSQUxTLmluZGV4T2Yoc3RyLmNoYXJBdChpKyspKTtcclxuICAgICAgZm9yIChqID0gMDsgaiA8IGFyci5sZW5ndGg7IGorKykge1xyXG4gICAgICAgIGlmIChhcnJbal0gPiBiYXNlT3V0IC0gMSkge1xyXG4gICAgICAgICAgaWYgKGFycltqICsgMV0gPT09IHZvaWQgMCkgYXJyW2ogKyAxXSA9IDA7XHJcbiAgICAgICAgICBhcnJbaiArIDFdICs9IGFycltqXSAvIGJhc2VPdXQgfCAwO1xyXG4gICAgICAgICAgYXJyW2pdICU9IGJhc2VPdXQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGFyci5yZXZlcnNlKCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBjb3MoeCkgPSAxIC0geF4yLzIhICsgeF40LzQhIC0gLi4uXHJcbiAgICogfHh8IDwgcGkvMlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gY29zaW5lKEN0b3IsIHgpIHtcclxuICAgIHZhciBrLCB5LFxyXG4gICAgICBsZW4gPSB4LmQubGVuZ3RoO1xyXG5cclxuICAgIC8vIEFyZ3VtZW50IHJlZHVjdGlvbjogY29zKDR4KSA9IDgqKGNvc140KHgpIC0gY29zXjIoeCkpICsgMVxyXG4gICAgLy8gaS5lLiBjb3MoeCkgPSA4Kihjb3NeNCh4LzQpIC0gY29zXjIoeC80KSkgKyAxXHJcblxyXG4gICAgLy8gRXN0aW1hdGUgdGhlIG9wdGltdW0gbnVtYmVyIG9mIHRpbWVzIHRvIHVzZSB0aGUgYXJndW1lbnQgcmVkdWN0aW9uLlxyXG4gICAgaWYgKGxlbiA8IDMyKSB7XHJcbiAgICAgIGsgPSBNYXRoLmNlaWwobGVuIC8gMyk7XHJcbiAgICAgIHkgPSBNYXRoLnBvdyg0LCAtaykudG9TdHJpbmcoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGsgPSAxNjtcclxuICAgICAgeSA9ICcyLjMyODMwNjQzNjUzODY5NjI4OTA2MjVlLTEwJztcclxuICAgIH1cclxuXHJcbiAgICBDdG9yLnByZWNpc2lvbiArPSBrO1xyXG5cclxuICAgIHggPSB0YXlsb3JTZXJpZXMoQ3RvciwgMSwgeC50aW1lcyh5KSwgbmV3IEN0b3IoMSkpO1xyXG5cclxuICAgIC8vIFJldmVyc2UgYXJndW1lbnQgcmVkdWN0aW9uXHJcbiAgICBmb3IgKHZhciBpID0gazsgaS0tOykge1xyXG4gICAgICB2YXIgY29zMnggPSB4LnRpbWVzKHgpO1xyXG4gICAgICB4ID0gY29zMngudGltZXMoY29zMngpLm1pbnVzKGNvczJ4KS50aW1lcyg4KS5wbHVzKDEpO1xyXG4gICAgfVxyXG5cclxuICAgIEN0b3IucHJlY2lzaW9uIC09IGs7XHJcblxyXG4gICAgcmV0dXJuIHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBQZXJmb3JtIGRpdmlzaW9uIGluIHRoZSBzcGVjaWZpZWQgYmFzZS5cclxuICAgKi9cclxuICB2YXIgZGl2aWRlID0gKGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAvLyBBc3N1bWVzIG5vbi16ZXJvIHggYW5kIGssIGFuZCBoZW5jZSBub24temVybyByZXN1bHQuXHJcbiAgICBmdW5jdGlvbiBtdWx0aXBseUludGVnZXIoeCwgaywgYmFzZSkge1xyXG4gICAgICB2YXIgdGVtcCxcclxuICAgICAgICBjYXJyeSA9IDAsXHJcbiAgICAgICAgaSA9IHgubGVuZ3RoO1xyXG5cclxuICAgICAgZm9yICh4ID0geC5zbGljZSgpOyBpLS07KSB7XHJcbiAgICAgICAgdGVtcCA9IHhbaV0gKiBrICsgY2Fycnk7XHJcbiAgICAgICAgeFtpXSA9IHRlbXAgJSBiYXNlIHwgMDtcclxuICAgICAgICBjYXJyeSA9IHRlbXAgLyBiYXNlIHwgMDtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGNhcnJ5KSB4LnVuc2hpZnQoY2FycnkpO1xyXG5cclxuICAgICAgcmV0dXJuIHg7XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gY29tcGFyZShhLCBiLCBhTCwgYkwpIHtcclxuICAgICAgdmFyIGksIHI7XHJcblxyXG4gICAgICBpZiAoYUwgIT0gYkwpIHtcclxuICAgICAgICByID0gYUwgPiBiTCA/IDEgOiAtMTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBmb3IgKGkgPSByID0gMDsgaSA8IGFMOyBpKyspIHtcclxuICAgICAgICAgIGlmIChhW2ldICE9IGJbaV0pIHtcclxuICAgICAgICAgICAgciA9IGFbaV0gPiBiW2ldID8gMSA6IC0xO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiByO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIHN1YnRyYWN0KGEsIGIsIGFMLCBiYXNlKSB7XHJcbiAgICAgIHZhciBpID0gMDtcclxuXHJcbiAgICAgIC8vIFN1YnRyYWN0IGIgZnJvbSBhLlxyXG4gICAgICBmb3IgKDsgYUwtLTspIHtcclxuICAgICAgICBhW2FMXSAtPSBpO1xyXG4gICAgICAgIGkgPSBhW2FMXSA8IGJbYUxdID8gMSA6IDA7XHJcbiAgICAgICAgYVthTF0gPSBpICogYmFzZSArIGFbYUxdIC0gYlthTF07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIFJlbW92ZSBsZWFkaW5nIHplcm9zLlxyXG4gICAgICBmb3IgKDsgIWFbMF0gJiYgYS5sZW5ndGggPiAxOykgYS5zaGlmdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBmdW5jdGlvbiAoeCwgeSwgcHIsIHJtLCBkcCwgYmFzZSkge1xyXG4gICAgICB2YXIgY21wLCBlLCBpLCBrLCBsb2dCYXNlLCBtb3JlLCBwcm9kLCBwcm9kTCwgcSwgcWQsIHJlbSwgcmVtTCwgcmVtMCwgc2QsIHQsIHhpLCB4TCwgeWQwLFxyXG4gICAgICAgIHlMLCB5eixcclxuICAgICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcixcclxuICAgICAgICBzaWduID0geC5zID09IHkucyA/IDEgOiAtMSxcclxuICAgICAgICB4ZCA9IHguZCxcclxuICAgICAgICB5ZCA9IHkuZDtcclxuXHJcbiAgICAgIC8vIEVpdGhlciBOYU4sIEluZmluaXR5IG9yIDA/XHJcbiAgICAgIGlmICgheGQgfHwgIXhkWzBdIHx8ICF5ZCB8fCAheWRbMF0pIHtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBDdG9yKC8vIFJldHVybiBOYU4gaWYgZWl0aGVyIE5hTiwgb3IgYm90aCBJbmZpbml0eSBvciAwLlxyXG4gICAgICAgICAgIXgucyB8fCAheS5zIHx8ICh4ZCA/IHlkICYmIHhkWzBdID09IHlkWzBdIDogIXlkKSA/IE5hTiA6XHJcblxyXG4gICAgICAgICAgLy8gUmV0dXJuIMKxMCBpZiB4IGlzIDAgb3IgeSBpcyDCsUluZmluaXR5LCBvciByZXR1cm4gwrFJbmZpbml0eSBhcyB5IGlzIDAuXHJcbiAgICAgICAgICB4ZCAmJiB4ZFswXSA9PSAwIHx8ICF5ZCA/IHNpZ24gKiAwIDogc2lnbiAvIDApO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoYmFzZSkge1xyXG4gICAgICAgIGxvZ0Jhc2UgPSAxO1xyXG4gICAgICAgIGUgPSB4LmUgLSB5LmU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgYmFzZSA9IEJBU0U7XHJcbiAgICAgICAgbG9nQmFzZSA9IExPR19CQVNFO1xyXG4gICAgICAgIGUgPSBtYXRoZmxvb3IoeC5lIC8gbG9nQmFzZSkgLSBtYXRoZmxvb3IoeS5lIC8gbG9nQmFzZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHlMID0geWQubGVuZ3RoO1xyXG4gICAgICB4TCA9IHhkLmxlbmd0aDtcclxuICAgICAgcSA9IG5ldyBDdG9yKHNpZ24pO1xyXG4gICAgICBxZCA9IHEuZCA9IFtdO1xyXG5cclxuICAgICAgLy8gUmVzdWx0IGV4cG9uZW50IG1heSBiZSBvbmUgbGVzcyB0aGFuIGUuXHJcbiAgICAgIC8vIFRoZSBkaWdpdCBhcnJheSBvZiBhIERlY2ltYWwgZnJvbSB0b1N0cmluZ0JpbmFyeSBtYXkgaGF2ZSB0cmFpbGluZyB6ZXJvcy5cclxuICAgICAgZm9yIChpID0gMDsgeWRbaV0gPT0gKHhkW2ldIHx8IDApOyBpKyspO1xyXG5cclxuICAgICAgaWYgKHlkW2ldID4gKHhkW2ldIHx8IDApKSBlLS07XHJcblxyXG4gICAgICBpZiAocHIgPT0gbnVsbCkge1xyXG4gICAgICAgIHNkID0gcHIgPSBDdG9yLnByZWNpc2lvbjtcclxuICAgICAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcbiAgICAgIH0gZWxzZSBpZiAoZHApIHtcclxuICAgICAgICBzZCA9IHByICsgKHguZSAtIHkuZSkgKyAxO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNkID0gcHI7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChzZCA8IDApIHtcclxuICAgICAgICBxZC5wdXNoKDEpO1xyXG4gICAgICAgIG1vcmUgPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAvLyBDb252ZXJ0IHByZWNpc2lvbiBpbiBudW1iZXIgb2YgYmFzZSAxMCBkaWdpdHMgdG8gYmFzZSAxZTcgZGlnaXRzLlxyXG4gICAgICAgIHNkID0gc2QgLyBsb2dCYXNlICsgMiB8IDA7XHJcbiAgICAgICAgaSA9IDA7XHJcblxyXG4gICAgICAgIC8vIGRpdmlzb3IgPCAxZTdcclxuICAgICAgICBpZiAoeUwgPT0gMSkge1xyXG4gICAgICAgICAgayA9IDA7XHJcbiAgICAgICAgICB5ZCA9IHlkWzBdO1xyXG4gICAgICAgICAgc2QrKztcclxuXHJcbiAgICAgICAgICAvLyBrIGlzIHRoZSBjYXJyeS5cclxuICAgICAgICAgIGZvciAoOyAoaSA8IHhMIHx8IGspICYmIHNkLS07IGkrKykge1xyXG4gICAgICAgICAgICB0ID0gayAqIGJhc2UgKyAoeGRbaV0gfHwgMCk7XHJcbiAgICAgICAgICAgIHFkW2ldID0gdCAvIHlkIHwgMDtcclxuICAgICAgICAgICAgayA9IHQgJSB5ZCB8IDA7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgbW9yZSA9IGsgfHwgaSA8IHhMO1xyXG5cclxuICAgICAgICAvLyBkaXZpc29yID49IDFlN1xyXG4gICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgLy8gTm9ybWFsaXNlIHhkIGFuZCB5ZCBzbyBoaWdoZXN0IG9yZGVyIGRpZ2l0IG9mIHlkIGlzID49IGJhc2UvMlxyXG4gICAgICAgICAgayA9IGJhc2UgLyAoeWRbMF0gKyAxKSB8IDA7XHJcblxyXG4gICAgICAgICAgaWYgKGsgPiAxKSB7XHJcbiAgICAgICAgICAgIHlkID0gbXVsdGlwbHlJbnRlZ2VyKHlkLCBrLCBiYXNlKTtcclxuICAgICAgICAgICAgeGQgPSBtdWx0aXBseUludGVnZXIoeGQsIGssIGJhc2UpO1xyXG4gICAgICAgICAgICB5TCA9IHlkLmxlbmd0aDtcclxuICAgICAgICAgICAgeEwgPSB4ZC5sZW5ndGg7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgeGkgPSB5TDtcclxuICAgICAgICAgIHJlbSA9IHhkLnNsaWNlKDAsIHlMKTtcclxuICAgICAgICAgIHJlbUwgPSByZW0ubGVuZ3RoO1xyXG5cclxuICAgICAgICAgIC8vIEFkZCB6ZXJvcyB0byBtYWtlIHJlbWFpbmRlciBhcyBsb25nIGFzIGRpdmlzb3IuXHJcbiAgICAgICAgICBmb3IgKDsgcmVtTCA8IHlMOykgcmVtW3JlbUwrK10gPSAwO1xyXG5cclxuICAgICAgICAgIHl6ID0geWQuc2xpY2UoKTtcclxuICAgICAgICAgIHl6LnVuc2hpZnQoMCk7XHJcbiAgICAgICAgICB5ZDAgPSB5ZFswXTtcclxuXHJcbiAgICAgICAgICBpZiAoeWRbMV0gPj0gYmFzZSAvIDIpICsreWQwO1xyXG5cclxuICAgICAgICAgIGRvIHtcclxuICAgICAgICAgICAgayA9IDA7XHJcblxyXG4gICAgICAgICAgICAvLyBDb21wYXJlIGRpdmlzb3IgYW5kIHJlbWFpbmRlci5cclxuICAgICAgICAgICAgY21wID0gY29tcGFyZSh5ZCwgcmVtLCB5TCwgcmVtTCk7XHJcblxyXG4gICAgICAgICAgICAvLyBJZiBkaXZpc29yIDwgcmVtYWluZGVyLlxyXG4gICAgICAgICAgICBpZiAoY21wIDwgMCkge1xyXG5cclxuICAgICAgICAgICAgICAvLyBDYWxjdWxhdGUgdHJpYWwgZGlnaXQsIGsuXHJcbiAgICAgICAgICAgICAgcmVtMCA9IHJlbVswXTtcclxuICAgICAgICAgICAgICBpZiAoeUwgIT0gcmVtTCkgcmVtMCA9IHJlbTAgKiBiYXNlICsgKHJlbVsxXSB8fCAwKTtcclxuXHJcbiAgICAgICAgICAgICAgLy8gayB3aWxsIGJlIGhvdyBtYW55IHRpbWVzIHRoZSBkaXZpc29yIGdvZXMgaW50byB0aGUgY3VycmVudCByZW1haW5kZXIuXHJcbiAgICAgICAgICAgICAgayA9IHJlbTAgLyB5ZDAgfCAwO1xyXG5cclxuICAgICAgICAgICAgICAvLyAgQWxnb3JpdGhtOlxyXG4gICAgICAgICAgICAgIC8vICAxLiBwcm9kdWN0ID0gZGl2aXNvciAqIHRyaWFsIGRpZ2l0IChrKVxyXG4gICAgICAgICAgICAgIC8vICAyLiBpZiBwcm9kdWN0ID4gcmVtYWluZGVyOiBwcm9kdWN0IC09IGRpdmlzb3IsIGstLVxyXG4gICAgICAgICAgICAgIC8vICAzLiByZW1haW5kZXIgLT0gcHJvZHVjdFxyXG4gICAgICAgICAgICAgIC8vICA0LiBpZiBwcm9kdWN0IHdhcyA8IHJlbWFpbmRlciBhdCAyOlxyXG4gICAgICAgICAgICAgIC8vICAgIDUuIGNvbXBhcmUgbmV3IHJlbWFpbmRlciBhbmQgZGl2aXNvclxyXG4gICAgICAgICAgICAgIC8vICAgIDYuIElmIHJlbWFpbmRlciA+IGRpdmlzb3I6IHJlbWFpbmRlciAtPSBkaXZpc29yLCBrKytcclxuXHJcbiAgICAgICAgICAgICAgaWYgKGsgPiAxKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoayA+PSBiYXNlKSBrID0gYmFzZSAtIDE7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gcHJvZHVjdCA9IGRpdmlzb3IgKiB0cmlhbCBkaWdpdC5cclxuICAgICAgICAgICAgICAgIHByb2QgPSBtdWx0aXBseUludGVnZXIoeWQsIGssIGJhc2UpO1xyXG4gICAgICAgICAgICAgICAgcHJvZEwgPSBwcm9kLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgIHJlbUwgPSByZW0ubGVuZ3RoO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIENvbXBhcmUgcHJvZHVjdCBhbmQgcmVtYWluZGVyLlxyXG4gICAgICAgICAgICAgICAgY21wID0gY29tcGFyZShwcm9kLCByZW0sIHByb2RMLCByZW1MKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBwcm9kdWN0ID4gcmVtYWluZGVyLlxyXG4gICAgICAgICAgICAgICAgaWYgKGNtcCA9PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgIGstLTtcclxuXHJcbiAgICAgICAgICAgICAgICAgIC8vIFN1YnRyYWN0IGRpdmlzb3IgZnJvbSBwcm9kdWN0LlxyXG4gICAgICAgICAgICAgICAgICBzdWJ0cmFjdChwcm9kLCB5TCA8IHByb2RMID8geXogOiB5ZCwgcHJvZEwsIGJhc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gY21wIGlzIC0xLlxyXG4gICAgICAgICAgICAgICAgLy8gSWYgayBpcyAwLCB0aGVyZSBpcyBubyBuZWVkIHRvIGNvbXBhcmUgeWQgYW5kIHJlbSBhZ2FpbiBiZWxvdywgc28gY2hhbmdlIGNtcCB0byAxXHJcbiAgICAgICAgICAgICAgICAvLyB0byBhdm9pZCBpdC4gSWYgayBpcyAxIHRoZXJlIGlzIGEgbmVlZCB0byBjb21wYXJlIHlkIGFuZCByZW0gYWdhaW4gYmVsb3cuXHJcbiAgICAgICAgICAgICAgICBpZiAoayA9PSAwKSBjbXAgPSBrID0gMTtcclxuICAgICAgICAgICAgICAgIHByb2QgPSB5ZC5zbGljZSgpO1xyXG4gICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgcHJvZEwgPSBwcm9kLmxlbmd0aDtcclxuICAgICAgICAgICAgICBpZiAocHJvZEwgPCByZW1MKSBwcm9kLnVuc2hpZnQoMCk7XHJcblxyXG4gICAgICAgICAgICAgIC8vIFN1YnRyYWN0IHByb2R1Y3QgZnJvbSByZW1haW5kZXIuXHJcbiAgICAgICAgICAgICAgc3VidHJhY3QocmVtLCBwcm9kLCByZW1MLCBiYXNlKTtcclxuXHJcbiAgICAgICAgICAgICAgLy8gSWYgcHJvZHVjdCB3YXMgPCBwcmV2aW91cyByZW1haW5kZXIuXHJcbiAgICAgICAgICAgICAgaWYgKGNtcCA9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgcmVtTCA9IHJlbS5sZW5ndGg7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gQ29tcGFyZSBkaXZpc29yIGFuZCBuZXcgcmVtYWluZGVyLlxyXG4gICAgICAgICAgICAgICAgY21wID0gY29tcGFyZSh5ZCwgcmVtLCB5TCwgcmVtTCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gSWYgZGl2aXNvciA8IG5ldyByZW1haW5kZXIsIHN1YnRyYWN0IGRpdmlzb3IgZnJvbSByZW1haW5kZXIuXHJcbiAgICAgICAgICAgICAgICBpZiAoY21wIDwgMSkge1xyXG4gICAgICAgICAgICAgICAgICBrKys7XHJcblxyXG4gICAgICAgICAgICAgICAgICAvLyBTdWJ0cmFjdCBkaXZpc29yIGZyb20gcmVtYWluZGVyLlxyXG4gICAgICAgICAgICAgICAgICBzdWJ0cmFjdChyZW0sIHlMIDwgcmVtTCA/IHl6IDogeWQsIHJlbUwsIGJhc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgcmVtTCA9IHJlbS5sZW5ndGg7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoY21wID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgaysrO1xyXG4gICAgICAgICAgICAgIHJlbSA9IFswXTtcclxuICAgICAgICAgICAgfSAgICAvLyBpZiBjbXAgPT09IDEsIGsgd2lsbCBiZSAwXHJcblxyXG4gICAgICAgICAgICAvLyBBZGQgdGhlIG5leHQgZGlnaXQsIGssIHRvIHRoZSByZXN1bHQgYXJyYXkuXHJcbiAgICAgICAgICAgIHFkW2krK10gPSBrO1xyXG5cclxuICAgICAgICAgICAgLy8gVXBkYXRlIHRoZSByZW1haW5kZXIuXHJcbiAgICAgICAgICAgIGlmIChjbXAgJiYgcmVtWzBdKSB7XHJcbiAgICAgICAgICAgICAgcmVtW3JlbUwrK10gPSB4ZFt4aV0gfHwgMDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICByZW0gPSBbeGRbeGldXTtcclxuICAgICAgICAgICAgICByZW1MID0gMTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIH0gd2hpbGUgKCh4aSsrIDwgeEwgfHwgcmVtWzBdICE9PSB2b2lkIDApICYmIHNkLS0pO1xyXG5cclxuICAgICAgICAgIG1vcmUgPSByZW1bMF0gIT09IHZvaWQgMDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIExlYWRpbmcgemVybz9cclxuICAgICAgICBpZiAoIXFkWzBdKSBxZC5zaGlmdCgpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBsb2dCYXNlIGlzIDEgd2hlbiBkaXZpZGUgaXMgYmVpbmcgdXNlZCBmb3IgYmFzZSBjb252ZXJzaW9uLlxyXG4gICAgICBpZiAobG9nQmFzZSA9PSAxKSB7XHJcbiAgICAgICAgcS5lID0gZTtcclxuICAgICAgICBpbmV4YWN0ID0gbW9yZTtcclxuICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgLy8gVG8gY2FsY3VsYXRlIHEuZSwgZmlyc3QgZ2V0IHRoZSBudW1iZXIgb2YgZGlnaXRzIG9mIHFkWzBdLlxyXG4gICAgICAgIGZvciAoaSA9IDEsIGsgPSBxZFswXTsgayA+PSAxMDsgayAvPSAxMCkgaSsrO1xyXG4gICAgICAgIHEuZSA9IGkgKyBlICogbG9nQmFzZSAtIDE7XHJcblxyXG4gICAgICAgIGZpbmFsaXNlKHEsIGRwID8gcHIgKyBxLmUgKyAxIDogcHIsIHJtLCBtb3JlKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHE7XHJcbiAgICB9O1xyXG4gIH0pKCk7XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJvdW5kIGB4YCB0byBgc2RgIHNpZ25pZmljYW50IGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGBybWAuXHJcbiAgICogQ2hlY2sgZm9yIG92ZXIvdW5kZXItZmxvdy5cclxuICAgKi9cclxuICAgZnVuY3Rpb24gZmluYWxpc2UoeCwgc2QsIHJtLCBpc1RydW5jYXRlZCkge1xyXG4gICAgdmFyIGRpZ2l0cywgaSwgaiwgaywgcmQsIHJvdW5kVXAsIHcsIHhkLCB4ZGksXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIC8vIERvbid0IHJvdW5kIGlmIHNkIGlzIG51bGwgb3IgdW5kZWZpbmVkLlxyXG4gICAgb3V0OiBpZiAoc2QgIT0gbnVsbCkge1xyXG4gICAgICB4ZCA9IHguZDtcclxuXHJcbiAgICAgIC8vIEluZmluaXR5L05hTi5cclxuICAgICAgaWYgKCF4ZCkgcmV0dXJuIHg7XHJcblxyXG4gICAgICAvLyByZDogdGhlIHJvdW5kaW5nIGRpZ2l0LCBpLmUuIHRoZSBkaWdpdCBhZnRlciB0aGUgZGlnaXQgdGhhdCBtYXkgYmUgcm91bmRlZCB1cC5cclxuICAgICAgLy8gdzogdGhlIHdvcmQgb2YgeGQgY29udGFpbmluZyByZCwgYSBiYXNlIDFlNyBudW1iZXIuXHJcbiAgICAgIC8vIHhkaTogdGhlIGluZGV4IG9mIHcgd2l0aGluIHhkLlxyXG4gICAgICAvLyBkaWdpdHM6IHRoZSBudW1iZXIgb2YgZGlnaXRzIG9mIHcuXHJcbiAgICAgIC8vIGk6IHdoYXQgd291bGQgYmUgdGhlIGluZGV4IG9mIHJkIHdpdGhpbiB3IGlmIGFsbCB0aGUgbnVtYmVycyB3ZXJlIDcgZGlnaXRzIGxvbmcgKGkuZS4gaWZcclxuICAgICAgLy8gdGhleSBoYWQgbGVhZGluZyB6ZXJvcylcclxuICAgICAgLy8gajogaWYgPiAwLCB0aGUgYWN0dWFsIGluZGV4IG9mIHJkIHdpdGhpbiB3IChpZiA8IDAsIHJkIGlzIGEgbGVhZGluZyB6ZXJvKS5cclxuXHJcbiAgICAgIC8vIEdldCB0aGUgbGVuZ3RoIG9mIHRoZSBmaXJzdCB3b3JkIG9mIHRoZSBkaWdpdHMgYXJyYXkgeGQuXHJcbiAgICAgIGZvciAoZGlnaXRzID0gMSwgayA9IHhkWzBdOyBrID49IDEwOyBrIC89IDEwKSBkaWdpdHMrKztcclxuICAgICAgaSA9IHNkIC0gZGlnaXRzO1xyXG5cclxuICAgICAgLy8gSXMgdGhlIHJvdW5kaW5nIGRpZ2l0IGluIHRoZSBmaXJzdCB3b3JkIG9mIHhkP1xyXG4gICAgICBpZiAoaSA8IDApIHtcclxuICAgICAgICBpICs9IExPR19CQVNFO1xyXG4gICAgICAgIGogPSBzZDtcclxuICAgICAgICB3ID0geGRbeGRpID0gMF07XHJcblxyXG4gICAgICAgIC8vIEdldCB0aGUgcm91bmRpbmcgZGlnaXQgYXQgaW5kZXggaiBvZiB3LlxyXG4gICAgICAgIHJkID0gdyAvIG1hdGhwb3coMTAsIGRpZ2l0cyAtIGogLSAxKSAlIDEwIHwgMDtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB4ZGkgPSBNYXRoLmNlaWwoKGkgKyAxKSAvIExPR19CQVNFKTtcclxuICAgICAgICBrID0geGQubGVuZ3RoO1xyXG4gICAgICAgIGlmICh4ZGkgPj0gaykge1xyXG4gICAgICAgICAgaWYgKGlzVHJ1bmNhdGVkKSB7XHJcblxyXG4gICAgICAgICAgICAvLyBOZWVkZWQgYnkgYG5hdHVyYWxFeHBvbmVudGlhbGAsIGBuYXR1cmFsTG9nYXJpdGhtYCBhbmQgYHNxdWFyZVJvb3RgLlxyXG4gICAgICAgICAgICBmb3IgKDsgaysrIDw9IHhkaTspIHhkLnB1c2goMCk7XHJcbiAgICAgICAgICAgIHcgPSByZCA9IDA7XHJcbiAgICAgICAgICAgIGRpZ2l0cyA9IDE7XHJcbiAgICAgICAgICAgIGkgJT0gTE9HX0JBU0U7XHJcbiAgICAgICAgICAgIGogPSBpIC0gTE9HX0JBU0UgKyAxO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgYnJlYWsgb3V0O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB3ID0gayA9IHhkW3hkaV07XHJcblxyXG4gICAgICAgICAgLy8gR2V0IHRoZSBudW1iZXIgb2YgZGlnaXRzIG9mIHcuXHJcbiAgICAgICAgICBmb3IgKGRpZ2l0cyA9IDE7IGsgPj0gMTA7IGsgLz0gMTApIGRpZ2l0cysrO1xyXG5cclxuICAgICAgICAgIC8vIEdldCB0aGUgaW5kZXggb2YgcmQgd2l0aGluIHcuXHJcbiAgICAgICAgICBpICU9IExPR19CQVNFO1xyXG5cclxuICAgICAgICAgIC8vIEdldCB0aGUgaW5kZXggb2YgcmQgd2l0aGluIHcsIGFkanVzdGVkIGZvciBsZWFkaW5nIHplcm9zLlxyXG4gICAgICAgICAgLy8gVGhlIG51bWJlciBvZiBsZWFkaW5nIHplcm9zIG9mIHcgaXMgZ2l2ZW4gYnkgTE9HX0JBU0UgLSBkaWdpdHMuXHJcbiAgICAgICAgICBqID0gaSAtIExPR19CQVNFICsgZGlnaXRzO1xyXG5cclxuICAgICAgICAgIC8vIEdldCB0aGUgcm91bmRpbmcgZGlnaXQgYXQgaW5kZXggaiBvZiB3LlxyXG4gICAgICAgICAgcmQgPSBqIDwgMCA/IDAgOiB3IC8gbWF0aHBvdygxMCwgZGlnaXRzIC0gaiAtIDEpICUgMTAgfCAwO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gQXJlIHRoZXJlIGFueSBub24temVybyBkaWdpdHMgYWZ0ZXIgdGhlIHJvdW5kaW5nIGRpZ2l0P1xyXG4gICAgICBpc1RydW5jYXRlZCA9IGlzVHJ1bmNhdGVkIHx8IHNkIDwgMCB8fFxyXG4gICAgICAgIHhkW3hkaSArIDFdICE9PSB2b2lkIDAgfHwgKGogPCAwID8gdyA6IHcgJSBtYXRocG93KDEwLCBkaWdpdHMgLSBqIC0gMSkpO1xyXG5cclxuICAgICAgLy8gVGhlIGV4cHJlc3Npb24gYHcgJSBtYXRocG93KDEwLCBkaWdpdHMgLSBqIC0gMSlgIHJldHVybnMgYWxsIHRoZSBkaWdpdHMgb2YgdyB0byB0aGUgcmlnaHRcclxuICAgICAgLy8gb2YgdGhlIGRpZ2l0IGF0IChsZWZ0LXRvLXJpZ2h0KSBpbmRleCBqLCBlLmcuIGlmIHcgaXMgOTA4NzE0IGFuZCBqIGlzIDIsIHRoZSBleHByZXNzaW9uXHJcbiAgICAgIC8vIHdpbGwgZ2l2ZSA3MTQuXHJcblxyXG4gICAgICByb3VuZFVwID0gcm0gPCA0XHJcbiAgICAgICAgPyAocmQgfHwgaXNUcnVuY2F0ZWQpICYmIChybSA9PSAwIHx8IHJtID09ICh4LnMgPCAwID8gMyA6IDIpKVxyXG4gICAgICAgIDogcmQgPiA1IHx8IHJkID09IDUgJiYgKHJtID09IDQgfHwgaXNUcnVuY2F0ZWQgfHwgcm0gPT0gNiAmJlxyXG5cclxuICAgICAgICAgIC8vIENoZWNrIHdoZXRoZXIgdGhlIGRpZ2l0IHRvIHRoZSBsZWZ0IG9mIHRoZSByb3VuZGluZyBkaWdpdCBpcyBvZGQuXHJcbiAgICAgICAgICAoKGkgPiAwID8gaiA+IDAgPyB3IC8gbWF0aHBvdygxMCwgZGlnaXRzIC0gaikgOiAwIDogeGRbeGRpIC0gMV0pICUgMTApICYgMSB8fFxyXG4gICAgICAgICAgICBybSA9PSAoeC5zIDwgMCA/IDggOiA3KSk7XHJcblxyXG4gICAgICBpZiAoc2QgPCAxIHx8ICF4ZFswXSkge1xyXG4gICAgICAgIHhkLmxlbmd0aCA9IDA7XHJcbiAgICAgICAgaWYgKHJvdW5kVXApIHtcclxuXHJcbiAgICAgICAgICAvLyBDb252ZXJ0IHNkIHRvIGRlY2ltYWwgcGxhY2VzLlxyXG4gICAgICAgICAgc2QgLT0geC5lICsgMTtcclxuXHJcbiAgICAgICAgICAvLyAxLCAwLjEsIDAuMDEsIDAuMDAxLCAwLjAwMDEgZXRjLlxyXG4gICAgICAgICAgeGRbMF0gPSBtYXRocG93KDEwLCAoTE9HX0JBU0UgLSBzZCAlIExPR19CQVNFKSAlIExPR19CQVNFKTtcclxuICAgICAgICAgIHguZSA9IC1zZCB8fCAwO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgLy8gWmVyby5cclxuICAgICAgICAgIHhkWzBdID0geC5lID0gMDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBSZW1vdmUgZXhjZXNzIGRpZ2l0cy5cclxuICAgICAgaWYgKGkgPT0gMCkge1xyXG4gICAgICAgIHhkLmxlbmd0aCA9IHhkaTtcclxuICAgICAgICBrID0gMTtcclxuICAgICAgICB4ZGktLTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB4ZC5sZW5ndGggPSB4ZGkgKyAxO1xyXG4gICAgICAgIGsgPSBtYXRocG93KDEwLCBMT0dfQkFTRSAtIGkpO1xyXG5cclxuICAgICAgICAvLyBFLmcuIDU2NzAwIGJlY29tZXMgNTYwMDAgaWYgNyBpcyB0aGUgcm91bmRpbmcgZGlnaXQuXHJcbiAgICAgICAgLy8gaiA+IDAgbWVhbnMgaSA+IG51bWJlciBvZiBsZWFkaW5nIHplcm9zIG9mIHcuXHJcbiAgICAgICAgeGRbeGRpXSA9IGogPiAwID8gKHcgLyBtYXRocG93KDEwLCBkaWdpdHMgLSBqKSAlIG1hdGhwb3coMTAsIGopIHwgMCkgKiBrIDogMDtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHJvdW5kVXApIHtcclxuICAgICAgICBmb3IgKDs7KSB7XHJcblxyXG4gICAgICAgICAgLy8gSXMgdGhlIGRpZ2l0IHRvIGJlIHJvdW5kZWQgdXAgaW4gdGhlIGZpcnN0IHdvcmQgb2YgeGQ/XHJcbiAgICAgICAgICBpZiAoeGRpID09IDApIHtcclxuXHJcbiAgICAgICAgICAgIC8vIGkgd2lsbCBiZSB0aGUgbGVuZ3RoIG9mIHhkWzBdIGJlZm9yZSBrIGlzIGFkZGVkLlxyXG4gICAgICAgICAgICBmb3IgKGkgPSAxLCBqID0geGRbMF07IGogPj0gMTA7IGogLz0gMTApIGkrKztcclxuICAgICAgICAgICAgaiA9IHhkWzBdICs9IGs7XHJcbiAgICAgICAgICAgIGZvciAoayA9IDE7IGogPj0gMTA7IGogLz0gMTApIGsrKztcclxuXHJcbiAgICAgICAgICAgIC8vIGlmIGkgIT0gayB0aGUgbGVuZ3RoIGhhcyBpbmNyZWFzZWQuXHJcbiAgICAgICAgICAgIGlmIChpICE9IGspIHtcclxuICAgICAgICAgICAgICB4LmUrKztcclxuICAgICAgICAgICAgICBpZiAoeGRbMF0gPT0gQkFTRSkgeGRbMF0gPSAxO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHhkW3hkaV0gKz0gaztcclxuICAgICAgICAgICAgaWYgKHhkW3hkaV0gIT0gQkFTRSkgYnJlYWs7XHJcbiAgICAgICAgICAgIHhkW3hkaS0tXSA9IDA7XHJcbiAgICAgICAgICAgIGsgPSAxO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gUmVtb3ZlIHRyYWlsaW5nIHplcm9zLlxyXG4gICAgICBmb3IgKGkgPSB4ZC5sZW5ndGg7IHhkWy0taV0gPT09IDA7KSB4ZC5wb3AoKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoZXh0ZXJuYWwpIHtcclxuXHJcbiAgICAgIC8vIE92ZXJmbG93P1xyXG4gICAgICBpZiAoeC5lID4gQ3Rvci5tYXhFKSB7XHJcblxyXG4gICAgICAgIC8vIEluZmluaXR5LlxyXG4gICAgICAgIHguZCA9IG51bGw7XHJcbiAgICAgICAgeC5lID0gTmFOO1xyXG5cclxuICAgICAgLy8gVW5kZXJmbG93P1xyXG4gICAgICB9IGVsc2UgaWYgKHguZSA8IEN0b3IubWluRSkge1xyXG5cclxuICAgICAgICAvLyBaZXJvLlxyXG4gICAgICAgIHguZSA9IDA7XHJcbiAgICAgICAgeC5kID0gWzBdO1xyXG4gICAgICAgIC8vIEN0b3IudW5kZXJmbG93ID0gdHJ1ZTtcclxuICAgICAgfSAvLyBlbHNlIEN0b3IudW5kZXJmbG93ID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgZnVuY3Rpb24gZmluaXRlVG9TdHJpbmcoeCwgaXNFeHAsIHNkKSB7XHJcbiAgICBpZiAoIXguaXNGaW5pdGUoKSkgcmV0dXJuIG5vbkZpbml0ZVRvU3RyaW5nKHgpO1xyXG4gICAgdmFyIGssXHJcbiAgICAgIGUgPSB4LmUsXHJcbiAgICAgIHN0ciA9IGRpZ2l0c1RvU3RyaW5nKHguZCksXHJcbiAgICAgIGxlbiA9IHN0ci5sZW5ndGg7XHJcblxyXG4gICAgaWYgKGlzRXhwKSB7XHJcbiAgICAgIGlmIChzZCAmJiAoayA9IHNkIC0gbGVuKSA+IDApIHtcclxuICAgICAgICBzdHIgPSBzdHIuY2hhckF0KDApICsgJy4nICsgc3RyLnNsaWNlKDEpICsgZ2V0WmVyb1N0cmluZyhrKTtcclxuICAgICAgfSBlbHNlIGlmIChsZW4gPiAxKSB7XHJcbiAgICAgICAgc3RyID0gc3RyLmNoYXJBdCgwKSArICcuJyArIHN0ci5zbGljZSgxKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgc3RyID0gc3RyICsgKHguZSA8IDAgPyAnZScgOiAnZSsnKSArIHguZTtcclxuICAgIH0gZWxzZSBpZiAoZSA8IDApIHtcclxuICAgICAgc3RyID0gJzAuJyArIGdldFplcm9TdHJpbmcoLWUgLSAxKSArIHN0cjtcclxuICAgICAgaWYgKHNkICYmIChrID0gc2QgLSBsZW4pID4gMCkgc3RyICs9IGdldFplcm9TdHJpbmcoayk7XHJcbiAgICB9IGVsc2UgaWYgKGUgPj0gbGVuKSB7XHJcbiAgICAgIHN0ciArPSBnZXRaZXJvU3RyaW5nKGUgKyAxIC0gbGVuKTtcclxuICAgICAgaWYgKHNkICYmIChrID0gc2QgLSBlIC0gMSkgPiAwKSBzdHIgPSBzdHIgKyAnLicgKyBnZXRaZXJvU3RyaW5nKGspO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKChrID0gZSArIDEpIDwgbGVuKSBzdHIgPSBzdHIuc2xpY2UoMCwgaykgKyAnLicgKyBzdHIuc2xpY2Uoayk7XHJcbiAgICAgIGlmIChzZCAmJiAoayA9IHNkIC0gbGVuKSA+IDApIHtcclxuICAgICAgICBpZiAoZSArIDEgPT09IGxlbikgc3RyICs9ICcuJztcclxuICAgICAgICBzdHIgKz0gZ2V0WmVyb1N0cmluZyhrKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBzdHI7XHJcbiAgfVxyXG5cclxuXHJcbiAgLy8gQ2FsY3VsYXRlIHRoZSBiYXNlIDEwIGV4cG9uZW50IGZyb20gdGhlIGJhc2UgMWU3IGV4cG9uZW50LlxyXG4gIGZ1bmN0aW9uIGdldEJhc2UxMEV4cG9uZW50KGRpZ2l0cywgZSkge1xyXG4gICAgdmFyIHcgPSBkaWdpdHNbMF07XHJcblxyXG4gICAgLy8gQWRkIHRoZSBudW1iZXIgb2YgZGlnaXRzIG9mIHRoZSBmaXJzdCB3b3JkIG9mIHRoZSBkaWdpdHMgYXJyYXkuXHJcbiAgICBmb3IgKCBlICo9IExPR19CQVNFOyB3ID49IDEwOyB3IC89IDEwKSBlKys7XHJcbiAgICByZXR1cm4gZTtcclxuICB9XHJcblxyXG5cclxuICBmdW5jdGlvbiBnZXRMbjEwKEN0b3IsIHNkLCBwcikge1xyXG4gICAgaWYgKHNkID4gTE4xMF9QUkVDSVNJT04pIHtcclxuXHJcbiAgICAgIC8vIFJlc2V0IGdsb2JhbCBzdGF0ZSBpbiBjYXNlIHRoZSBleGNlcHRpb24gaXMgY2F1Z2h0LlxyXG4gICAgICBleHRlcm5hbCA9IHRydWU7XHJcbiAgICAgIGlmIChwcikgQ3Rvci5wcmVjaXNpb24gPSBwcjtcclxuICAgICAgdGhyb3cgRXJyb3IocHJlY2lzaW9uTGltaXRFeGNlZWRlZCk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmluYWxpc2UobmV3IEN0b3IoTE4xMCksIHNkLCAxLCB0cnVlKTtcclxuICB9XHJcblxyXG5cclxuICBmdW5jdGlvbiBnZXRQaShDdG9yLCBzZCwgcm0pIHtcclxuICAgIGlmIChzZCA+IFBJX1BSRUNJU0lPTikgdGhyb3cgRXJyb3IocHJlY2lzaW9uTGltaXRFeGNlZWRlZCk7XHJcbiAgICByZXR1cm4gZmluYWxpc2UobmV3IEN0b3IoUEkpLCBzZCwgcm0sIHRydWUpO1xyXG4gIH1cclxuXHJcblxyXG4gIGZ1bmN0aW9uIGdldFByZWNpc2lvbihkaWdpdHMpIHtcclxuICAgIHZhciB3ID0gZGlnaXRzLmxlbmd0aCAtIDEsXHJcbiAgICAgIGxlbiA9IHcgKiBMT0dfQkFTRSArIDE7XHJcblxyXG4gICAgdyA9IGRpZ2l0c1t3XTtcclxuXHJcbiAgICAvLyBJZiBub24temVyby4uLlxyXG4gICAgaWYgKHcpIHtcclxuXHJcbiAgICAgIC8vIFN1YnRyYWN0IHRoZSBudW1iZXIgb2YgdHJhaWxpbmcgemVyb3Mgb2YgdGhlIGxhc3Qgd29yZC5cclxuICAgICAgZm9yICg7IHcgJSAxMCA9PSAwOyB3IC89IDEwKSBsZW4tLTtcclxuXHJcbiAgICAgIC8vIEFkZCB0aGUgbnVtYmVyIG9mIGRpZ2l0cyBvZiB0aGUgZmlyc3Qgd29yZC5cclxuICAgICAgZm9yICh3ID0gZGlnaXRzWzBdOyB3ID49IDEwOyB3IC89IDEwKSBsZW4rKztcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gbGVuO1xyXG4gIH1cclxuXHJcblxyXG4gIGZ1bmN0aW9uIGdldFplcm9TdHJpbmcoaykge1xyXG4gICAgdmFyIHpzID0gJyc7XHJcbiAgICBmb3IgKDsgay0tOykgenMgKz0gJzAnO1xyXG4gICAgcmV0dXJuIHpzO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIHZhbHVlIG9mIERlY2ltYWwgYHhgIHRvIHRoZSBwb3dlciBgbmAsIHdoZXJlIGBuYCBpcyBhblxyXG4gICAqIGludGVnZXIgb2YgdHlwZSBudW1iZXIuXHJcbiAgICpcclxuICAgKiBJbXBsZW1lbnRzICdleHBvbmVudGlhdGlvbiBieSBzcXVhcmluZycuIENhbGxlZCBieSBgcG93YCBhbmQgYHBhcnNlT3RoZXJgLlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gaW50UG93KEN0b3IsIHgsIG4sIHByKSB7XHJcbiAgICB2YXIgaXNUcnVuY2F0ZWQsXHJcbiAgICAgIHIgPSBuZXcgQ3RvcigxKSxcclxuXHJcbiAgICAgIC8vIE1heCBuIG9mIDkwMDcxOTkyNTQ3NDA5OTEgdGFrZXMgNTMgbG9vcCBpdGVyYXRpb25zLlxyXG4gICAgICAvLyBNYXhpbXVtIGRpZ2l0cyBhcnJheSBsZW5ndGg7IGxlYXZlcyBbMjgsIDM0XSBndWFyZCBkaWdpdHMuXHJcbiAgICAgIGsgPSBNYXRoLmNlaWwocHIgLyBMT0dfQkFTRSArIDQpO1xyXG5cclxuICAgIGV4dGVybmFsID0gZmFsc2U7XHJcblxyXG4gICAgZm9yICg7Oykge1xyXG4gICAgICBpZiAobiAlIDIpIHtcclxuICAgICAgICByID0gci50aW1lcyh4KTtcclxuICAgICAgICBpZiAodHJ1bmNhdGUoci5kLCBrKSkgaXNUcnVuY2F0ZWQgPSB0cnVlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBuID0gbWF0aGZsb29yKG4gLyAyKTtcclxuICAgICAgaWYgKG4gPT09IDApIHtcclxuXHJcbiAgICAgICAgLy8gVG8gZW5zdXJlIGNvcnJlY3Qgcm91bmRpbmcgd2hlbiByLmQgaXMgdHJ1bmNhdGVkLCBpbmNyZW1lbnQgdGhlIGxhc3Qgd29yZCBpZiBpdCBpcyB6ZXJvLlxyXG4gICAgICAgIG4gPSByLmQubGVuZ3RoIC0gMTtcclxuICAgICAgICBpZiAoaXNUcnVuY2F0ZWQgJiYgci5kW25dID09PSAwKSArK3IuZFtuXTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG5cclxuICAgICAgeCA9IHgudGltZXMoeCk7XHJcbiAgICAgIHRydW5jYXRlKHguZCwgayk7XHJcbiAgICB9XHJcblxyXG4gICAgZXh0ZXJuYWwgPSB0cnVlO1xyXG5cclxuICAgIHJldHVybiByO1xyXG4gIH1cclxuXHJcblxyXG4gIGZ1bmN0aW9uIGlzT2RkKG4pIHtcclxuICAgIHJldHVybiBuLmRbbi5kLmxlbmd0aCAtIDFdICYgMTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIEhhbmRsZSBgbWF4YCBhbmQgYG1pbmAuIGBsdGd0YCBpcyAnbHQnIG9yICdndCcuXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gbWF4T3JNaW4oQ3RvciwgYXJncywgbHRndCkge1xyXG4gICAgdmFyIHksXHJcbiAgICAgIHggPSBuZXcgQ3RvcihhcmdzWzBdKSxcclxuICAgICAgaSA9IDA7XHJcblxyXG4gICAgZm9yICg7ICsraSA8IGFyZ3MubGVuZ3RoOykge1xyXG4gICAgICB5ID0gbmV3IEN0b3IoYXJnc1tpXSk7XHJcbiAgICAgIGlmICgheS5zKSB7XHJcbiAgICAgICAgeCA9IHk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIH0gZWxzZSBpZiAoeFtsdGd0XSh5KSkge1xyXG4gICAgICAgIHggPSB5O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgbmF0dXJhbCBleHBvbmVudGlhbCBvZiBgeGAgcm91bmRlZCB0byBgc2RgIHNpZ25pZmljYW50XHJcbiAgICogZGlnaXRzLlxyXG4gICAqXHJcbiAgICogVGF5bG9yL01hY2xhdXJpbiBzZXJpZXMuXHJcbiAgICpcclxuICAgKiBleHAoeCkgPSB4XjAvMCEgKyB4XjEvMSEgKyB4XjIvMiEgKyB4XjMvMyEgKyAuLi5cclxuICAgKlxyXG4gICAqIEFyZ3VtZW50IHJlZHVjdGlvbjpcclxuICAgKiAgIFJlcGVhdCB4ID0geCAvIDMyLCBrICs9IDUsIHVudGlsIHx4fCA8IDAuMVxyXG4gICAqICAgZXhwKHgpID0gZXhwKHggLyAyXmspXigyXmspXHJcbiAgICpcclxuICAgKiBQcmV2aW91c2x5LCB0aGUgYXJndW1lbnQgd2FzIGluaXRpYWxseSByZWR1Y2VkIGJ5XHJcbiAgICogZXhwKHgpID0gZXhwKHIpICogMTBeayAgd2hlcmUgciA9IHggLSBrICogbG4xMCwgayA9IGZsb29yKHggLyBsbjEwKVxyXG4gICAqIHRvIGZpcnN0IHB1dCByIGluIHRoZSByYW5nZSBbMCwgbG4xMF0sIGJlZm9yZSBkaXZpZGluZyBieSAzMiB1bnRpbCB8eHwgPCAwLjEsIGJ1dCB0aGlzIHdhc1xyXG4gICAqIGZvdW5kIHRvIGJlIHNsb3dlciB0aGFuIGp1c3QgZGl2aWRpbmcgcmVwZWF0ZWRseSBieSAzMiBhcyBhYm92ZS5cclxuICAgKlxyXG4gICAqIE1heCBpbnRlZ2VyIGFyZ3VtZW50OiBleHAoJzIwNzIzMjY1ODM2OTQ2NDEzJykgPSA2LjNlKzkwMDAwMDAwMDAwMDAwMDBcclxuICAgKiBNaW4gaW50ZWdlciBhcmd1bWVudDogZXhwKCctMjA3MjMyNjU4MzY5NDY0MTEnKSA9IDEuMmUtOTAwMDAwMDAwMDAwMDAwMFxyXG4gICAqIChNYXRoIG9iamVjdCBpbnRlZ2VyIG1pbi9tYXg6IE1hdGguZXhwKDcwOSkgPSA4LjJlKzMwNywgTWF0aC5leHAoLTc0NSkgPSA1ZS0zMjQpXHJcbiAgICpcclxuICAgKiAgZXhwKEluZmluaXR5KSAgPSBJbmZpbml0eVxyXG4gICAqICBleHAoLUluZmluaXR5KSA9IDBcclxuICAgKiAgZXhwKE5hTikgICAgICAgPSBOYU5cclxuICAgKiAgZXhwKMKxMCkgICAgICAgID0gMVxyXG4gICAqXHJcbiAgICogIGV4cCh4KSBpcyBub24tdGVybWluYXRpbmcgZm9yIGFueSBmaW5pdGUsIG5vbi16ZXJvIHguXHJcbiAgICpcclxuICAgKiAgVGhlIHJlc3VsdCB3aWxsIGFsd2F5cyBiZSBjb3JyZWN0bHkgcm91bmRlZC5cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIG5hdHVyYWxFeHBvbmVudGlhbCh4LCBzZCkge1xyXG4gICAgdmFyIGRlbm9taW5hdG9yLCBndWFyZCwgaiwgcG93LCBzdW0sIHQsIHdwcixcclxuICAgICAgcmVwID0gMCxcclxuICAgICAgaSA9IDAsXHJcbiAgICAgIGsgPSAwLFxyXG4gICAgICBDdG9yID0geC5jb25zdHJ1Y3RvcixcclxuICAgICAgcm0gPSBDdG9yLnJvdW5kaW5nLFxyXG4gICAgICBwciA9IEN0b3IucHJlY2lzaW9uO1xyXG5cclxuICAgIC8vIDAvTmFOL0luZmluaXR5P1xyXG4gICAgaWYgKCF4LmQgfHwgIXguZFswXSB8fCB4LmUgPiAxNykge1xyXG5cclxuICAgICAgcmV0dXJuIG5ldyBDdG9yKHguZFxyXG4gICAgICAgID8gIXguZFswXSA/IDEgOiB4LnMgPCAwID8gMCA6IDEgLyAwXHJcbiAgICAgICAgOiB4LnMgPyB4LnMgPCAwID8gMCA6IHggOiAwIC8gMCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHNkID09IG51bGwpIHtcclxuICAgICAgZXh0ZXJuYWwgPSBmYWxzZTtcclxuICAgICAgd3ByID0gcHI7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB3cHIgPSBzZDtcclxuICAgIH1cclxuXHJcbiAgICB0ID0gbmV3IEN0b3IoMC4wMzEyNSk7XHJcblxyXG4gICAgLy8gd2hpbGUgYWJzKHgpID49IDAuMVxyXG4gICAgd2hpbGUgKHguZSA+IC0yKSB7XHJcblxyXG4gICAgICAvLyB4ID0geCAvIDJeNVxyXG4gICAgICB4ID0geC50aW1lcyh0KTtcclxuICAgICAgayArPSA1O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFVzZSAyICogbG9nMTAoMl5rKSArIDUgKGVtcGlyaWNhbGx5IGRlcml2ZWQpIHRvIGVzdGltYXRlIHRoZSBpbmNyZWFzZSBpbiBwcmVjaXNpb25cclxuICAgIC8vIG5lY2Vzc2FyeSB0byBlbnN1cmUgdGhlIGZpcnN0IDQgcm91bmRpbmcgZGlnaXRzIGFyZSBjb3JyZWN0LlxyXG4gICAgZ3VhcmQgPSBNYXRoLmxvZyhtYXRocG93KDIsIGspKSAvIE1hdGguTE4xMCAqIDIgKyA1IHwgMDtcclxuICAgIHdwciArPSBndWFyZDtcclxuICAgIGRlbm9taW5hdG9yID0gcG93ID0gc3VtID0gbmV3IEN0b3IoMSk7XHJcbiAgICBDdG9yLnByZWNpc2lvbiA9IHdwcjtcclxuXHJcbiAgICBmb3IgKDs7KSB7XHJcbiAgICAgIHBvdyA9IGZpbmFsaXNlKHBvdy50aW1lcyh4KSwgd3ByLCAxKTtcclxuICAgICAgZGVub21pbmF0b3IgPSBkZW5vbWluYXRvci50aW1lcygrK2kpO1xyXG4gICAgICB0ID0gc3VtLnBsdXMoZGl2aWRlKHBvdywgZGVub21pbmF0b3IsIHdwciwgMSkpO1xyXG5cclxuICAgICAgaWYgKGRpZ2l0c1RvU3RyaW5nKHQuZCkuc2xpY2UoMCwgd3ByKSA9PT0gZGlnaXRzVG9TdHJpbmcoc3VtLmQpLnNsaWNlKDAsIHdwcikpIHtcclxuICAgICAgICBqID0gaztcclxuICAgICAgICB3aGlsZSAoai0tKSBzdW0gPSBmaW5hbGlzZShzdW0udGltZXMoc3VtKSwgd3ByLCAxKTtcclxuXHJcbiAgICAgICAgLy8gQ2hlY2sgdG8gc2VlIGlmIHRoZSBmaXJzdCA0IHJvdW5kaW5nIGRpZ2l0cyBhcmUgWzQ5XTk5OS5cclxuICAgICAgICAvLyBJZiBzbywgcmVwZWF0IHRoZSBzdW1tYXRpb24gd2l0aCBhIGhpZ2hlciBwcmVjaXNpb24sIG90aGVyd2lzZVxyXG4gICAgICAgIC8vIGUuZy4gd2l0aCBwcmVjaXNpb246IDE4LCByb3VuZGluZzogMVxyXG4gICAgICAgIC8vIGV4cCgxOC40MDQyNzI0NjI1OTUwMzQwODM1Njc3OTM5MTk4NDM3NjEpID0gOTgzNzI1NjAuMTIyOTk5OTk5OSAoc2hvdWxkIGJlIDk4MzcyNTYwLjEyMylcclxuICAgICAgICAvLyBgd3ByIC0gZ3VhcmRgIGlzIHRoZSBpbmRleCBvZiBmaXJzdCByb3VuZGluZyBkaWdpdC5cclxuICAgICAgICBpZiAoc2QgPT0gbnVsbCkge1xyXG5cclxuICAgICAgICAgIGlmIChyZXAgPCAzICYmIGNoZWNrUm91bmRpbmdEaWdpdHMoc3VtLmQsIHdwciAtIGd1YXJkLCBybSwgcmVwKSkge1xyXG4gICAgICAgICAgICBDdG9yLnByZWNpc2lvbiA9IHdwciArPSAxMDtcclxuICAgICAgICAgICAgZGVub21pbmF0b3IgPSBwb3cgPSB0ID0gbmV3IEN0b3IoMSk7XHJcbiAgICAgICAgICAgIGkgPSAwO1xyXG4gICAgICAgICAgICByZXArKztcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmaW5hbGlzZShzdW0sIEN0b3IucHJlY2lzaW9uID0gcHIsIHJtLCBleHRlcm5hbCA9IHRydWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBDdG9yLnByZWNpc2lvbiA9IHByO1xyXG4gICAgICAgICAgcmV0dXJuIHN1bTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHN1bSA9IHQ7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgbmF0dXJhbCBsb2dhcml0aG0gb2YgYHhgIHJvdW5kZWQgdG8gYHNkYCBzaWduaWZpY2FudFxyXG4gICAqIGRpZ2l0cy5cclxuICAgKlxyXG4gICAqICBsbigtbikgICAgICAgID0gTmFOXHJcbiAgICogIGxuKDApICAgICAgICAgPSAtSW5maW5pdHlcclxuICAgKiAgbG4oLTApICAgICAgICA9IC1JbmZpbml0eVxyXG4gICAqICBsbigxKSAgICAgICAgID0gMFxyXG4gICAqICBsbihJbmZpbml0eSkgID0gSW5maW5pdHlcclxuICAgKiAgbG4oLUluZmluaXR5KSA9IE5hTlxyXG4gICAqICBsbihOYU4pICAgICAgID0gTmFOXHJcbiAgICpcclxuICAgKiAgbG4obikgKG4gIT0gMSkgaXMgbm9uLXRlcm1pbmF0aW5nLlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gbmF0dXJhbExvZ2FyaXRobSh5LCBzZCkge1xyXG4gICAgdmFyIGMsIGMwLCBkZW5vbWluYXRvciwgZSwgbnVtZXJhdG9yLCByZXAsIHN1bSwgdCwgd3ByLCB4MSwgeDIsXHJcbiAgICAgIG4gPSAxLFxyXG4gICAgICBndWFyZCA9IDEwLFxyXG4gICAgICB4ID0geSxcclxuICAgICAgeGQgPSB4LmQsXHJcbiAgICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yLFxyXG4gICAgICBybSA9IEN0b3Iucm91bmRpbmcsXHJcbiAgICAgIHByID0gQ3Rvci5wcmVjaXNpb247XHJcblxyXG4gICAgLy8gSXMgeCBuZWdhdGl2ZSBvciBJbmZpbml0eSwgTmFOLCAwIG9yIDE/XHJcbiAgICBpZiAoeC5zIDwgMCB8fCAheGQgfHwgIXhkWzBdIHx8ICF4LmUgJiYgeGRbMF0gPT0gMSAmJiB4ZC5sZW5ndGggPT0gMSkge1xyXG4gICAgICByZXR1cm4gbmV3IEN0b3IoeGQgJiYgIXhkWzBdID8gLTEgLyAwIDogeC5zICE9IDEgPyBOYU4gOiB4ZCA/IDAgOiB4KTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoc2QgPT0gbnVsbCkge1xyXG4gICAgICBleHRlcm5hbCA9IGZhbHNlO1xyXG4gICAgICB3cHIgPSBwcjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHdwciA9IHNkO1xyXG4gICAgfVxyXG5cclxuICAgIEN0b3IucHJlY2lzaW9uID0gd3ByICs9IGd1YXJkO1xyXG4gICAgYyA9IGRpZ2l0c1RvU3RyaW5nKHhkKTtcclxuICAgIGMwID0gYy5jaGFyQXQoMCk7XHJcblxyXG4gICAgaWYgKE1hdGguYWJzKGUgPSB4LmUpIDwgMS41ZTE1KSB7XHJcblxyXG4gICAgICAvLyBBcmd1bWVudCByZWR1Y3Rpb24uXHJcbiAgICAgIC8vIFRoZSBzZXJpZXMgY29udmVyZ2VzIGZhc3RlciB0aGUgY2xvc2VyIHRoZSBhcmd1bWVudCBpcyB0byAxLCBzbyB1c2luZ1xyXG4gICAgICAvLyBsbihhXmIpID0gYiAqIGxuKGEpLCAgIGxuKGEpID0gbG4oYV5iKSAvIGJcclxuICAgICAgLy8gbXVsdGlwbHkgdGhlIGFyZ3VtZW50IGJ5IGl0c2VsZiB1bnRpbCB0aGUgbGVhZGluZyBkaWdpdHMgb2YgdGhlIHNpZ25pZmljYW5kIGFyZSA3LCA4LCA5LFxyXG4gICAgICAvLyAxMCwgMTEsIDEyIG9yIDEzLCByZWNvcmRpbmcgdGhlIG51bWJlciBvZiBtdWx0aXBsaWNhdGlvbnMgc28gdGhlIHN1bSBvZiB0aGUgc2VyaWVzIGNhblxyXG4gICAgICAvLyBsYXRlciBiZSBkaXZpZGVkIGJ5IHRoaXMgbnVtYmVyLCB0aGVuIHNlcGFyYXRlIG91dCB0aGUgcG93ZXIgb2YgMTAgdXNpbmdcclxuICAgICAgLy8gbG4oYSoxMF5iKSA9IGxuKGEpICsgYipsbigxMCkuXHJcblxyXG4gICAgICAvLyBtYXggbiBpcyAyMSAoZ2l2ZXMgMC45LCAxLjAgb3IgMS4xKSAoOWUxNSAvIDIxID0gNC4yZTE0KS5cclxuICAgICAgLy93aGlsZSAoYzAgPCA5ICYmIGMwICE9IDEgfHwgYzAgPT0gMSAmJiBjLmNoYXJBdCgxKSA+IDEpIHtcclxuICAgICAgLy8gbWF4IG4gaXMgNiAoZ2l2ZXMgMC43IC0gMS4zKVxyXG4gICAgICB3aGlsZSAoYzAgPCA3ICYmIGMwICE9IDEgfHwgYzAgPT0gMSAmJiBjLmNoYXJBdCgxKSA+IDMpIHtcclxuICAgICAgICB4ID0geC50aW1lcyh5KTtcclxuICAgICAgICBjID0gZGlnaXRzVG9TdHJpbmcoeC5kKTtcclxuICAgICAgICBjMCA9IGMuY2hhckF0KDApO1xyXG4gICAgICAgIG4rKztcclxuICAgICAgfVxyXG5cclxuICAgICAgZSA9IHguZTtcclxuXHJcbiAgICAgIGlmIChjMCA+IDEpIHtcclxuICAgICAgICB4ID0gbmV3IEN0b3IoJzAuJyArIGMpO1xyXG4gICAgICAgIGUrKztcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB4ID0gbmV3IEN0b3IoYzAgKyAnLicgKyBjLnNsaWNlKDEpKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuXHJcbiAgICAgIC8vIFRoZSBhcmd1bWVudCByZWR1Y3Rpb24gbWV0aG9kIGFib3ZlIG1heSByZXN1bHQgaW4gb3ZlcmZsb3cgaWYgdGhlIGFyZ3VtZW50IHkgaXMgYSBtYXNzaXZlXHJcbiAgICAgIC8vIG51bWJlciB3aXRoIGV4cG9uZW50ID49IDE1MDAwMDAwMDAwMDAwMDAgKDllMTUgLyA2ID0gMS41ZTE1KSwgc28gaW5zdGVhZCByZWNhbGwgdGhpc1xyXG4gICAgICAvLyBmdW5jdGlvbiB1c2luZyBsbih4KjEwXmUpID0gbG4oeCkgKyBlKmxuKDEwKS5cclxuICAgICAgdCA9IGdldExuMTAoQ3Rvciwgd3ByICsgMiwgcHIpLnRpbWVzKGUgKyAnJyk7XHJcbiAgICAgIHggPSBuYXR1cmFsTG9nYXJpdGhtKG5ldyBDdG9yKGMwICsgJy4nICsgYy5zbGljZSgxKSksIHdwciAtIGd1YXJkKS5wbHVzKHQpO1xyXG4gICAgICBDdG9yLnByZWNpc2lvbiA9IHByO1xyXG5cclxuICAgICAgcmV0dXJuIHNkID09IG51bGwgPyBmaW5hbGlzZSh4LCBwciwgcm0sIGV4dGVybmFsID0gdHJ1ZSkgOiB4O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHgxIGlzIHggcmVkdWNlZCB0byBhIHZhbHVlIG5lYXIgMS5cclxuICAgIHgxID0geDtcclxuXHJcbiAgICAvLyBUYXlsb3Igc2VyaWVzLlxyXG4gICAgLy8gbG4oeSkgPSBsbigoMSArIHgpLygxIC0geCkpID0gMih4ICsgeF4zLzMgKyB4XjUvNSArIHheNy83ICsgLi4uKVxyXG4gICAgLy8gd2hlcmUgeCA9ICh5IC0gMSkvKHkgKyAxKSAgICAofHh8IDwgMSlcclxuICAgIHN1bSA9IG51bWVyYXRvciA9IHggPSBkaXZpZGUoeC5taW51cygxKSwgeC5wbHVzKDEpLCB3cHIsIDEpO1xyXG4gICAgeDIgPSBmaW5hbGlzZSh4LnRpbWVzKHgpLCB3cHIsIDEpO1xyXG4gICAgZGVub21pbmF0b3IgPSAzO1xyXG5cclxuICAgIGZvciAoOzspIHtcclxuICAgICAgbnVtZXJhdG9yID0gZmluYWxpc2UobnVtZXJhdG9yLnRpbWVzKHgyKSwgd3ByLCAxKTtcclxuICAgICAgdCA9IHN1bS5wbHVzKGRpdmlkZShudW1lcmF0b3IsIG5ldyBDdG9yKGRlbm9taW5hdG9yKSwgd3ByLCAxKSk7XHJcblxyXG4gICAgICBpZiAoZGlnaXRzVG9TdHJpbmcodC5kKS5zbGljZSgwLCB3cHIpID09PSBkaWdpdHNUb1N0cmluZyhzdW0uZCkuc2xpY2UoMCwgd3ByKSkge1xyXG4gICAgICAgIHN1bSA9IHN1bS50aW1lcygyKTtcclxuXHJcbiAgICAgICAgLy8gUmV2ZXJzZSB0aGUgYXJndW1lbnQgcmVkdWN0aW9uLiBDaGVjayB0aGF0IGUgaXMgbm90IDAgYmVjYXVzZSwgYmVzaWRlcyBwcmV2ZW50aW5nIGFuXHJcbiAgICAgICAgLy8gdW5uZWNlc3NhcnkgY2FsY3VsYXRpb24sIC0wICsgMCA9ICswIGFuZCB0byBlbnN1cmUgY29ycmVjdCByb3VuZGluZyAtMCBuZWVkcyB0byBzdGF5IC0wLlxyXG4gICAgICAgIGlmIChlICE9PSAwKSBzdW0gPSBzdW0ucGx1cyhnZXRMbjEwKEN0b3IsIHdwciArIDIsIHByKS50aW1lcyhlICsgJycpKTtcclxuICAgICAgICBzdW0gPSBkaXZpZGUoc3VtLCBuZXcgQ3RvcihuKSwgd3ByLCAxKTtcclxuXHJcbiAgICAgICAgLy8gSXMgcm0gPiAzIGFuZCB0aGUgZmlyc3QgNCByb3VuZGluZyBkaWdpdHMgNDk5OSwgb3Igcm0gPCA0IChvciB0aGUgc3VtbWF0aW9uIGhhc1xyXG4gICAgICAgIC8vIGJlZW4gcmVwZWF0ZWQgcHJldmlvdXNseSkgYW5kIHRoZSBmaXJzdCA0IHJvdW5kaW5nIGRpZ2l0cyA5OTk5P1xyXG4gICAgICAgIC8vIElmIHNvLCByZXN0YXJ0IHRoZSBzdW1tYXRpb24gd2l0aCBhIGhpZ2hlciBwcmVjaXNpb24sIG90aGVyd2lzZVxyXG4gICAgICAgIC8vIGUuZy4gd2l0aCBwcmVjaXNpb246IDEyLCByb3VuZGluZzogMVxyXG4gICAgICAgIC8vIGxuKDEzNTUyMDAyOC42MTI2MDkxNzE0MjY1MzgxNTMzKSA9IDE4LjcyNDYyOTk5OTkgd2hlbiBpdCBzaG91bGQgYmUgMTguNzI0NjMuXHJcbiAgICAgICAgLy8gYHdwciAtIGd1YXJkYCBpcyB0aGUgaW5kZXggb2YgZmlyc3Qgcm91bmRpbmcgZGlnaXQuXHJcbiAgICAgICAgaWYgKHNkID09IG51bGwpIHtcclxuICAgICAgICAgIGlmIChjaGVja1JvdW5kaW5nRGlnaXRzKHN1bS5kLCB3cHIgLSBndWFyZCwgcm0sIHJlcCkpIHtcclxuICAgICAgICAgICAgQ3Rvci5wcmVjaXNpb24gPSB3cHIgKz0gZ3VhcmQ7XHJcbiAgICAgICAgICAgIHQgPSBudW1lcmF0b3IgPSB4ID0gZGl2aWRlKHgxLm1pbnVzKDEpLCB4MS5wbHVzKDEpLCB3cHIsIDEpO1xyXG4gICAgICAgICAgICB4MiA9IGZpbmFsaXNlKHgudGltZXMoeCksIHdwciwgMSk7XHJcbiAgICAgICAgICAgIGRlbm9taW5hdG9yID0gcmVwID0gMTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmaW5hbGlzZShzdW0sIEN0b3IucHJlY2lzaW9uID0gcHIsIHJtLCBleHRlcm5hbCA9IHRydWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBDdG9yLnByZWNpc2lvbiA9IHByO1xyXG4gICAgICAgICAgcmV0dXJuIHN1bTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHN1bSA9IHQ7XHJcbiAgICAgIGRlbm9taW5hdG9yICs9IDI7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuXHJcbiAgLy8gwrFJbmZpbml0eSwgTmFOLlxyXG4gIGZ1bmN0aW9uIG5vbkZpbml0ZVRvU3RyaW5nKHgpIHtcclxuICAgIC8vIFVuc2lnbmVkLlxyXG4gICAgcmV0dXJuIFN0cmluZyh4LnMgKiB4LnMgLyAwKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFBhcnNlIHRoZSB2YWx1ZSBvZiBhIG5ldyBEZWNpbWFsIGB4YCBmcm9tIHN0cmluZyBgc3RyYC5cclxuICAgKi9cclxuICBmdW5jdGlvbiBwYXJzZURlY2ltYWwoeCwgc3RyKSB7XHJcbiAgICB2YXIgZSwgaSwgbGVuO1xyXG5cclxuICAgIC8vIERlY2ltYWwgcG9pbnQ/XHJcbiAgICBpZiAoKGUgPSBzdHIuaW5kZXhPZignLicpKSA+IC0xKSBzdHIgPSBzdHIucmVwbGFjZSgnLicsICcnKTtcclxuXHJcbiAgICAvLyBFeHBvbmVudGlhbCBmb3JtP1xyXG4gICAgaWYgKChpID0gc3RyLnNlYXJjaCgvZS9pKSkgPiAwKSB7XHJcblxyXG4gICAgICAvLyBEZXRlcm1pbmUgZXhwb25lbnQuXHJcbiAgICAgIGlmIChlIDwgMCkgZSA9IGk7XHJcbiAgICAgIGUgKz0gK3N0ci5zbGljZShpICsgMSk7XHJcbiAgICAgIHN0ciA9IHN0ci5zdWJzdHJpbmcoMCwgaSk7XHJcbiAgICB9IGVsc2UgaWYgKGUgPCAwKSB7XHJcblxyXG4gICAgICAvLyBJbnRlZ2VyLlxyXG4gICAgICBlID0gc3RyLmxlbmd0aDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBEZXRlcm1pbmUgbGVhZGluZyB6ZXJvcy5cclxuICAgIGZvciAoaSA9IDA7IHN0ci5jaGFyQ29kZUF0KGkpID09PSA0ODsgaSsrKTtcclxuXHJcbiAgICAvLyBEZXRlcm1pbmUgdHJhaWxpbmcgemVyb3MuXHJcbiAgICBmb3IgKGxlbiA9IHN0ci5sZW5ndGg7IHN0ci5jaGFyQ29kZUF0KGxlbiAtIDEpID09PSA0ODsgLS1sZW4pO1xyXG4gICAgc3RyID0gc3RyLnNsaWNlKGksIGxlbik7XHJcblxyXG4gICAgaWYgKHN0cikge1xyXG4gICAgICBsZW4gLT0gaTtcclxuICAgICAgeC5lID0gZSA9IGUgLSBpIC0gMTtcclxuICAgICAgeC5kID0gW107XHJcblxyXG4gICAgICAvLyBUcmFuc2Zvcm0gYmFzZVxyXG5cclxuICAgICAgLy8gZSBpcyB0aGUgYmFzZSAxMCBleHBvbmVudC5cclxuICAgICAgLy8gaSBpcyB3aGVyZSB0byBzbGljZSBzdHIgdG8gZ2V0IHRoZSBmaXJzdCB3b3JkIG9mIHRoZSBkaWdpdHMgYXJyYXkuXHJcbiAgICAgIGkgPSAoZSArIDEpICUgTE9HX0JBU0U7XHJcbiAgICAgIGlmIChlIDwgMCkgaSArPSBMT0dfQkFTRTtcclxuXHJcbiAgICAgIGlmIChpIDwgbGVuKSB7XHJcbiAgICAgICAgaWYgKGkpIHguZC5wdXNoKCtzdHIuc2xpY2UoMCwgaSkpO1xyXG4gICAgICAgIGZvciAobGVuIC09IExPR19CQVNFOyBpIDwgbGVuOykgeC5kLnB1c2goK3N0ci5zbGljZShpLCBpICs9IExPR19CQVNFKSk7XHJcbiAgICAgICAgc3RyID0gc3RyLnNsaWNlKGkpO1xyXG4gICAgICAgIGkgPSBMT0dfQkFTRSAtIHN0ci5sZW5ndGg7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaSAtPSBsZW47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGZvciAoOyBpLS07KSBzdHIgKz0gJzAnO1xyXG4gICAgICB4LmQucHVzaCgrc3RyKTtcclxuXHJcbiAgICAgIGlmIChleHRlcm5hbCkge1xyXG5cclxuICAgICAgICAvLyBPdmVyZmxvdz9cclxuICAgICAgICBpZiAoeC5lID4geC5jb25zdHJ1Y3Rvci5tYXhFKSB7XHJcblxyXG4gICAgICAgICAgLy8gSW5maW5pdHkuXHJcbiAgICAgICAgICB4LmQgPSBudWxsO1xyXG4gICAgICAgICAgeC5lID0gTmFOO1xyXG5cclxuICAgICAgICAvLyBVbmRlcmZsb3c/XHJcbiAgICAgICAgfSBlbHNlIGlmICh4LmUgPCB4LmNvbnN0cnVjdG9yLm1pbkUpIHtcclxuXHJcbiAgICAgICAgICAvLyBaZXJvLlxyXG4gICAgICAgICAgeC5lID0gMDtcclxuICAgICAgICAgIHguZCA9IFswXTtcclxuICAgICAgICAgIC8vIHguY29uc3RydWN0b3IudW5kZXJmbG93ID0gdHJ1ZTtcclxuICAgICAgICB9IC8vIGVsc2UgeC5jb25zdHJ1Y3Rvci51bmRlcmZsb3cgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuXHJcbiAgICAgIC8vIFplcm8uXHJcbiAgICAgIHguZSA9IDA7XHJcbiAgICAgIHguZCA9IFswXTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4geDtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFBhcnNlIHRoZSB2YWx1ZSBvZiBhIG5ldyBEZWNpbWFsIGB4YCBmcm9tIGEgc3RyaW5nIGBzdHJgLCB3aGljaCBpcyBub3QgYSBkZWNpbWFsIHZhbHVlLlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHBhcnNlT3RoZXIoeCwgc3RyKSB7XHJcbiAgICB2YXIgYmFzZSwgQ3RvciwgZGl2aXNvciwgaSwgaXNGbG9hdCwgbGVuLCBwLCB4ZCwgeGU7XHJcblxyXG4gICAgaWYgKHN0ciA9PT0gJ0luZmluaXR5JyB8fCBzdHIgPT09ICdOYU4nKSB7XHJcbiAgICAgIGlmICghK3N0cikgeC5zID0gTmFOO1xyXG4gICAgICB4LmUgPSBOYU47XHJcbiAgICAgIHguZCA9IG51bGw7XHJcbiAgICAgIHJldHVybiB4O1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChpc0hleC50ZXN0KHN0cikpICB7XHJcbiAgICAgIGJhc2UgPSAxNjtcclxuICAgICAgc3RyID0gc3RyLnRvTG93ZXJDYXNlKCk7XHJcbiAgICB9IGVsc2UgaWYgKGlzQmluYXJ5LnRlc3Qoc3RyKSkgIHtcclxuICAgICAgYmFzZSA9IDI7XHJcbiAgICB9IGVsc2UgaWYgKGlzT2N0YWwudGVzdChzdHIpKSAge1xyXG4gICAgICBiYXNlID0gODtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRocm93IEVycm9yKGludmFsaWRBcmd1bWVudCArIHN0cik7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gSXMgdGhlcmUgYSBiaW5hcnkgZXhwb25lbnQgcGFydD9cclxuICAgIGkgPSBzdHIuc2VhcmNoKC9wL2kpO1xyXG5cclxuICAgIGlmIChpID4gMCkge1xyXG4gICAgICBwID0gK3N0ci5zbGljZShpICsgMSk7XHJcbiAgICAgIHN0ciA9IHN0ci5zdWJzdHJpbmcoMiwgaSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzdHIgPSBzdHIuc2xpY2UoMik7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ29udmVydCBgc3RyYCBhcyBhbiBpbnRlZ2VyIHRoZW4gZGl2aWRlIHRoZSByZXN1bHQgYnkgYGJhc2VgIHJhaXNlZCB0byBhIHBvd2VyIHN1Y2ggdGhhdCB0aGVcclxuICAgIC8vIGZyYWN0aW9uIHBhcnQgd2lsbCBiZSByZXN0b3JlZC5cclxuICAgIGkgPSBzdHIuaW5kZXhPZignLicpO1xyXG4gICAgaXNGbG9hdCA9IGkgPj0gMDtcclxuICAgIEN0b3IgPSB4LmNvbnN0cnVjdG9yO1xyXG5cclxuICAgIGlmIChpc0Zsb2F0KSB7XHJcbiAgICAgIHN0ciA9IHN0ci5yZXBsYWNlKCcuJywgJycpO1xyXG4gICAgICBsZW4gPSBzdHIubGVuZ3RoO1xyXG4gICAgICBpID0gbGVuIC0gaTtcclxuXHJcbiAgICAgIC8vIGxvZ1sxMF0oMTYpID0gMS4yMDQxLi4uICwgbG9nWzEwXSg4OCkgPSAxLjk0NDQuLi4uXHJcbiAgICAgIGRpdmlzb3IgPSBpbnRQb3coQ3RvciwgbmV3IEN0b3IoYmFzZSksIGksIGkgKiAyKTtcclxuICAgIH1cclxuXHJcbiAgICB4ZCA9IGNvbnZlcnRCYXNlKHN0ciwgYmFzZSwgQkFTRSk7XHJcbiAgICB4ZSA9IHhkLmxlbmd0aCAtIDE7XHJcblxyXG4gICAgLy8gUmVtb3ZlIHRyYWlsaW5nIHplcm9zLlxyXG4gICAgZm9yIChpID0geGU7IHhkW2ldID09PSAwOyAtLWkpIHhkLnBvcCgpO1xyXG4gICAgaWYgKGkgPCAwKSByZXR1cm4gbmV3IEN0b3IoeC5zICogMCk7XHJcbiAgICB4LmUgPSBnZXRCYXNlMTBFeHBvbmVudCh4ZCwgeGUpO1xyXG4gICAgeC5kID0geGQ7XHJcbiAgICBleHRlcm5hbCA9IGZhbHNlO1xyXG5cclxuICAgIC8vIEF0IHdoYXQgcHJlY2lzaW9uIHRvIHBlcmZvcm0gdGhlIGRpdmlzaW9uIHRvIGVuc3VyZSBleGFjdCBjb252ZXJzaW9uP1xyXG4gICAgLy8gbWF4RGVjaW1hbEludGVnZXJQYXJ0RGlnaXRDb3VudCA9IGNlaWwobG9nWzEwXShiKSAqIG90aGVyQmFzZUludGVnZXJQYXJ0RGlnaXRDb3VudClcclxuICAgIC8vIGxvZ1sxMF0oMikgPSAwLjMwMTAzLCBsb2dbMTBdKDgpID0gMC45MDMwOSwgbG9nWzEwXSgxNikgPSAxLjIwNDEyXHJcbiAgICAvLyBFLmcuIGNlaWwoMS4yICogMykgPSA0LCBzbyB1cCB0byA0IGRlY2ltYWwgZGlnaXRzIGFyZSBuZWVkZWQgdG8gcmVwcmVzZW50IDMgaGV4IGludCBkaWdpdHMuXHJcbiAgICAvLyBtYXhEZWNpbWFsRnJhY3Rpb25QYXJ0RGlnaXRDb3VudCA9IHtIZXg6NHxPY3Q6M3xCaW46MX0gKiBvdGhlckJhc2VGcmFjdGlvblBhcnREaWdpdENvdW50XHJcbiAgICAvLyBUaGVyZWZvcmUgdXNpbmcgNCAqIHRoZSBudW1iZXIgb2YgZGlnaXRzIG9mIHN0ciB3aWxsIGFsd2F5cyBiZSBlbm91Z2guXHJcbiAgICBpZiAoaXNGbG9hdCkgeCA9IGRpdmlkZSh4LCBkaXZpc29yLCBsZW4gKiA0KTtcclxuXHJcbiAgICAvLyBNdWx0aXBseSBieSB0aGUgYmluYXJ5IGV4cG9uZW50IHBhcnQgaWYgcHJlc2VudC5cclxuICAgIGlmIChwKSB4ID0geC50aW1lcyhNYXRoLmFicyhwKSA8IDU0ID8gTWF0aC5wb3coMiwgcCkgOiBEZWNpbWFsLnBvdygyLCBwKSk7XHJcbiAgICBleHRlcm5hbCA9IHRydWU7XHJcblxyXG4gICAgcmV0dXJuIHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBzaW4oeCkgPSB4IC0geF4zLzMhICsgeF41LzUhIC0gLi4uXHJcbiAgICogfHh8IDwgcGkvMlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gc2luZShDdG9yLCB4KSB7XHJcbiAgICB2YXIgayxcclxuICAgICAgbGVuID0geC5kLmxlbmd0aDtcclxuXHJcbiAgICBpZiAobGVuIDwgMykgcmV0dXJuIHRheWxvclNlcmllcyhDdG9yLCAyLCB4LCB4KTtcclxuXHJcbiAgICAvLyBBcmd1bWVudCByZWR1Y3Rpb246IHNpbig1eCkgPSAxNipzaW5eNSh4KSAtIDIwKnNpbl4zKHgpICsgNSpzaW4oeClcclxuICAgIC8vIGkuZS4gc2luKHgpID0gMTYqc2luXjUoeC81KSAtIDIwKnNpbl4zKHgvNSkgKyA1KnNpbih4LzUpXHJcbiAgICAvLyBhbmQgIHNpbih4KSA9IHNpbih4LzUpKDUgKyBzaW5eMih4LzUpKDE2c2luXjIoeC81KSAtIDIwKSlcclxuXHJcbiAgICAvLyBFc3RpbWF0ZSB0aGUgb3B0aW11bSBudW1iZXIgb2YgdGltZXMgdG8gdXNlIHRoZSBhcmd1bWVudCByZWR1Y3Rpb24uXHJcbiAgICBrID0gMS40ICogTWF0aC5zcXJ0KGxlbik7XHJcbiAgICBrID0gayA+IDE2ID8gMTYgOiBrIHwgMDtcclxuXHJcbiAgICAvLyBNYXggayBiZWZvcmUgTWF0aC5wb3cgcHJlY2lzaW9uIGxvc3MgaXMgMjJcclxuICAgIHggPSB4LnRpbWVzKE1hdGgucG93KDUsIC1rKSk7XHJcbiAgICB4ID0gdGF5bG9yU2VyaWVzKEN0b3IsIDIsIHgsIHgpO1xyXG5cclxuICAgIC8vIFJldmVyc2UgYXJndW1lbnQgcmVkdWN0aW9uXHJcbiAgICB2YXIgc2luMl94LFxyXG4gICAgICBkNSA9IG5ldyBDdG9yKDUpLFxyXG4gICAgICBkMTYgPSBuZXcgQ3RvcigxNiksXHJcbiAgICAgIGQyMCA9IG5ldyBDdG9yKDIwKTtcclxuICAgIGZvciAoOyBrLS07KSB7XHJcbiAgICAgIHNpbjJfeCA9IHgudGltZXMoeCk7XHJcbiAgICAgIHggPSB4LnRpbWVzKGQ1LnBsdXMoc2luMl94LnRpbWVzKGQxNi50aW1lcyhzaW4yX3gpLm1pbnVzKGQyMCkpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLy8gQ2FsY3VsYXRlIFRheWxvciBzZXJpZXMgZm9yIGBjb3NgLCBgY29zaGAsIGBzaW5gIGFuZCBgc2luaGAuXHJcbiAgZnVuY3Rpb24gdGF5bG9yU2VyaWVzKEN0b3IsIG4sIHgsIHksIGlzSHlwZXJib2xpYykge1xyXG4gICAgdmFyIGosIHQsIHUsIHgyLFxyXG4gICAgICBpID0gMSxcclxuICAgICAgcHIgPSBDdG9yLnByZWNpc2lvbixcclxuICAgICAgayA9IE1hdGguY2VpbChwciAvIExPR19CQVNFKTtcclxuXHJcbiAgICBleHRlcm5hbCA9IGZhbHNlO1xyXG4gICAgeDIgPSB4LnRpbWVzKHgpO1xyXG4gICAgdSA9IG5ldyBDdG9yKHkpO1xyXG5cclxuICAgIGZvciAoOzspIHtcclxuICAgICAgdCA9IGRpdmlkZSh1LnRpbWVzKHgyKSwgbmV3IEN0b3IobisrICogbisrKSwgcHIsIDEpO1xyXG4gICAgICB1ID0gaXNIeXBlcmJvbGljID8geS5wbHVzKHQpIDogeS5taW51cyh0KTtcclxuICAgICAgeSA9IGRpdmlkZSh0LnRpbWVzKHgyKSwgbmV3IEN0b3IobisrICogbisrKSwgcHIsIDEpO1xyXG4gICAgICB0ID0gdS5wbHVzKHkpO1xyXG5cclxuICAgICAgaWYgKHQuZFtrXSAhPT0gdm9pZCAwKSB7XHJcbiAgICAgICAgZm9yIChqID0gazsgdC5kW2pdID09PSB1LmRbal0gJiYgai0tOyk7XHJcbiAgICAgICAgaWYgKGogPT0gLTEpIGJyZWFrO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBqID0gdTtcclxuICAgICAgdSA9IHk7XHJcbiAgICAgIHkgPSB0O1xyXG4gICAgICB0ID0gajtcclxuICAgICAgaSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGV4dGVybmFsID0gdHJ1ZTtcclxuICAgIHQuZC5sZW5ndGggPSBrICsgMTtcclxuXHJcbiAgICByZXR1cm4gdDtcclxuICB9XHJcblxyXG5cclxuICAvLyBSZXR1cm4gdGhlIGFic29sdXRlIHZhbHVlIG9mIGB4YCByZWR1Y2VkIHRvIGxlc3MgdGhhbiBvciBlcXVhbCB0byBoYWxmIHBpLlxyXG4gIGZ1bmN0aW9uIHRvTGVzc1RoYW5IYWxmUGkoQ3RvciwgeCkge1xyXG4gICAgdmFyIHQsXHJcbiAgICAgIGlzTmVnID0geC5zIDwgMCxcclxuICAgICAgcGkgPSBnZXRQaShDdG9yLCBDdG9yLnByZWNpc2lvbiwgMSksXHJcbiAgICAgIGhhbGZQaSA9IHBpLnRpbWVzKDAuNSk7XHJcblxyXG4gICAgeCA9IHguYWJzKCk7XHJcblxyXG4gICAgaWYgKHgubHRlKGhhbGZQaSkpIHtcclxuICAgICAgcXVhZHJhbnQgPSBpc05lZyA/IDQgOiAxO1xyXG4gICAgICByZXR1cm4geDtcclxuICAgIH1cclxuXHJcbiAgICB0ID0geC5kaXZUb0ludChwaSk7XHJcblxyXG4gICAgaWYgKHQuaXNaZXJvKCkpIHtcclxuICAgICAgcXVhZHJhbnQgPSBpc05lZyA/IDMgOiAyO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgeCA9IHgubWludXModC50aW1lcyhwaSkpO1xyXG5cclxuICAgICAgLy8gMCA8PSB4IDwgcGlcclxuICAgICAgaWYgKHgubHRlKGhhbGZQaSkpIHtcclxuICAgICAgICBxdWFkcmFudCA9IGlzT2RkKHQpID8gKGlzTmVnID8gMiA6IDMpIDogKGlzTmVnID8gNCA6IDEpO1xyXG4gICAgICAgIHJldHVybiB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICBxdWFkcmFudCA9IGlzT2RkKHQpID8gKGlzTmVnID8gMSA6IDQpIDogKGlzTmVnID8gMyA6IDIpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB4Lm1pbnVzKHBpKS5hYnMoKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiB0aGUgdmFsdWUgb2YgRGVjaW1hbCBgeGAgYXMgYSBzdHJpbmcgaW4gYmFzZSBgYmFzZU91dGAuXHJcbiAgICpcclxuICAgKiBJZiB0aGUgb3B0aW9uYWwgYHNkYCBhcmd1bWVudCBpcyBwcmVzZW50IGluY2x1ZGUgYSBiaW5hcnkgZXhwb25lbnQgc3VmZml4LlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHRvU3RyaW5nQmluYXJ5KHgsIGJhc2VPdXQsIHNkLCBybSkge1xyXG4gICAgdmFyIGJhc2UsIGUsIGksIGssIGxlbiwgcm91bmRVcCwgc3RyLCB4ZCwgeSxcclxuICAgICAgQ3RvciA9IHguY29uc3RydWN0b3IsXHJcbiAgICAgIGlzRXhwID0gc2QgIT09IHZvaWQgMDtcclxuXHJcbiAgICBpZiAoaXNFeHApIHtcclxuICAgICAgY2hlY2tJbnQzMihzZCwgMSwgTUFYX0RJR0lUUyk7XHJcbiAgICAgIGlmIChybSA9PT0gdm9pZCAwKSBybSA9IEN0b3Iucm91bmRpbmc7XHJcbiAgICAgIGVsc2UgY2hlY2tJbnQzMihybSwgMCwgOCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZCA9IEN0b3IucHJlY2lzaW9uO1xyXG4gICAgICBybSA9IEN0b3Iucm91bmRpbmc7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCF4LmlzRmluaXRlKCkpIHtcclxuICAgICAgc3RyID0gbm9uRmluaXRlVG9TdHJpbmcoeCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzdHIgPSBmaW5pdGVUb1N0cmluZyh4KTtcclxuICAgICAgaSA9IHN0ci5pbmRleE9mKCcuJyk7XHJcblxyXG4gICAgICAvLyBVc2UgZXhwb25lbnRpYWwgbm90YXRpb24gYWNjb3JkaW5nIHRvIGB0b0V4cFBvc2AgYW5kIGB0b0V4cE5lZ2A/IE5vLCBidXQgaWYgcmVxdWlyZWQ6XHJcbiAgICAgIC8vIG1heEJpbmFyeUV4cG9uZW50ID0gZmxvb3IoKGRlY2ltYWxFeHBvbmVudCArIDEpICogbG9nWzJdKDEwKSlcclxuICAgICAgLy8gbWluQmluYXJ5RXhwb25lbnQgPSBmbG9vcihkZWNpbWFsRXhwb25lbnQgKiBsb2dbMl0oMTApKVxyXG4gICAgICAvLyBsb2dbMl0oMTApID0gMy4zMjE5MjgwOTQ4ODczNjIzNDc4NzAzMTk0Mjk0ODkzOTAxNzU4NjRcclxuXHJcbiAgICAgIGlmIChpc0V4cCkge1xyXG4gICAgICAgIGJhc2UgPSAyO1xyXG4gICAgICAgIGlmIChiYXNlT3V0ID09IDE2KSB7XHJcbiAgICAgICAgICBzZCA9IHNkICogNCAtIDM7XHJcbiAgICAgICAgfSBlbHNlIGlmIChiYXNlT3V0ID09IDgpIHtcclxuICAgICAgICAgIHNkID0gc2QgKiAzIC0gMjtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgYmFzZSA9IGJhc2VPdXQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIENvbnZlcnQgdGhlIG51bWJlciBhcyBhbiBpbnRlZ2VyIHRoZW4gZGl2aWRlIHRoZSByZXN1bHQgYnkgaXRzIGJhc2UgcmFpc2VkIHRvIGEgcG93ZXIgc3VjaFxyXG4gICAgICAvLyB0aGF0IHRoZSBmcmFjdGlvbiBwYXJ0IHdpbGwgYmUgcmVzdG9yZWQuXHJcblxyXG4gICAgICAvLyBOb24taW50ZWdlci5cclxuICAgICAgaWYgKGkgPj0gMCkge1xyXG4gICAgICAgIHN0ciA9IHN0ci5yZXBsYWNlKCcuJywgJycpO1xyXG4gICAgICAgIHkgPSBuZXcgQ3RvcigxKTtcclxuICAgICAgICB5LmUgPSBzdHIubGVuZ3RoIC0gaTtcclxuICAgICAgICB5LmQgPSBjb252ZXJ0QmFzZShmaW5pdGVUb1N0cmluZyh5KSwgMTAsIGJhc2UpO1xyXG4gICAgICAgIHkuZSA9IHkuZC5sZW5ndGg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHhkID0gY29udmVydEJhc2Uoc3RyLCAxMCwgYmFzZSk7XHJcbiAgICAgIGUgPSBsZW4gPSB4ZC5sZW5ndGg7XHJcblxyXG4gICAgICAvLyBSZW1vdmUgdHJhaWxpbmcgemVyb3MuXHJcbiAgICAgIGZvciAoOyB4ZFstLWxlbl0gPT0gMDspIHhkLnBvcCgpO1xyXG5cclxuICAgICAgaWYgKCF4ZFswXSkge1xyXG4gICAgICAgIHN0ciA9IGlzRXhwID8gJzBwKzAnIDogJzAnO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmIChpIDwgMCkge1xyXG4gICAgICAgICAgZS0tO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB4ID0gbmV3IEN0b3IoeCk7XHJcbiAgICAgICAgICB4LmQgPSB4ZDtcclxuICAgICAgICAgIHguZSA9IGU7XHJcbiAgICAgICAgICB4ID0gZGl2aWRlKHgsIHksIHNkLCBybSwgMCwgYmFzZSk7XHJcbiAgICAgICAgICB4ZCA9IHguZDtcclxuICAgICAgICAgIGUgPSB4LmU7XHJcbiAgICAgICAgICByb3VuZFVwID0gaW5leGFjdDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFRoZSByb3VuZGluZyBkaWdpdCwgaS5lLiB0aGUgZGlnaXQgYWZ0ZXIgdGhlIGRpZ2l0IHRoYXQgbWF5IGJlIHJvdW5kZWQgdXAuXHJcbiAgICAgICAgaSA9IHhkW3NkXTtcclxuICAgICAgICBrID0gYmFzZSAvIDI7XHJcbiAgICAgICAgcm91bmRVcCA9IHJvdW5kVXAgfHwgeGRbc2QgKyAxXSAhPT0gdm9pZCAwO1xyXG5cclxuICAgICAgICByb3VuZFVwID0gcm0gPCA0XHJcbiAgICAgICAgICA/IChpICE9PSB2b2lkIDAgfHwgcm91bmRVcCkgJiYgKHJtID09PSAwIHx8IHJtID09PSAoeC5zIDwgMCA/IDMgOiAyKSlcclxuICAgICAgICAgIDogaSA+IGsgfHwgaSA9PT0gayAmJiAocm0gPT09IDQgfHwgcm91bmRVcCB8fCBybSA9PT0gNiAmJiB4ZFtzZCAtIDFdICYgMSB8fFxyXG4gICAgICAgICAgICBybSA9PT0gKHgucyA8IDAgPyA4IDogNykpO1xyXG5cclxuICAgICAgICB4ZC5sZW5ndGggPSBzZDtcclxuXHJcbiAgICAgICAgaWYgKHJvdW5kVXApIHtcclxuXHJcbiAgICAgICAgICAvLyBSb3VuZGluZyB1cCBtYXkgbWVhbiB0aGUgcHJldmlvdXMgZGlnaXQgaGFzIHRvIGJlIHJvdW5kZWQgdXAgYW5kIHNvIG9uLlxyXG4gICAgICAgICAgZm9yICg7ICsreGRbLS1zZF0gPiBiYXNlIC0gMTspIHtcclxuICAgICAgICAgICAgeGRbc2RdID0gMDtcclxuICAgICAgICAgICAgaWYgKCFzZCkge1xyXG4gICAgICAgICAgICAgICsrZTtcclxuICAgICAgICAgICAgICB4ZC51bnNoaWZ0KDEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBEZXRlcm1pbmUgdHJhaWxpbmcgemVyb3MuXHJcbiAgICAgICAgZm9yIChsZW4gPSB4ZC5sZW5ndGg7ICF4ZFtsZW4gLSAxXTsgLS1sZW4pO1xyXG5cclxuICAgICAgICAvLyBFLmcuIFs0LCAxMSwgMTVdIGJlY29tZXMgNGJmLlxyXG4gICAgICAgIGZvciAoaSA9IDAsIHN0ciA9ICcnOyBpIDwgbGVuOyBpKyspIHN0ciArPSBOVU1FUkFMUy5jaGFyQXQoeGRbaV0pO1xyXG5cclxuICAgICAgICAvLyBBZGQgYmluYXJ5IGV4cG9uZW50IHN1ZmZpeD9cclxuICAgICAgICBpZiAoaXNFeHApIHtcclxuICAgICAgICAgIGlmIChsZW4gPiAxKSB7XHJcbiAgICAgICAgICAgIGlmIChiYXNlT3V0ID09IDE2IHx8IGJhc2VPdXQgPT0gOCkge1xyXG4gICAgICAgICAgICAgIGkgPSBiYXNlT3V0ID09IDE2ID8gNCA6IDM7XHJcbiAgICAgICAgICAgICAgZm9yICgtLWxlbjsgbGVuICUgaTsgbGVuKyspIHN0ciArPSAnMCc7XHJcbiAgICAgICAgICAgICAgeGQgPSBjb252ZXJ0QmFzZShzdHIsIGJhc2UsIGJhc2VPdXQpO1xyXG4gICAgICAgICAgICAgIGZvciAobGVuID0geGQubGVuZ3RoOyAheGRbbGVuIC0gMV07IC0tbGVuKTtcclxuXHJcbiAgICAgICAgICAgICAgLy8geGRbMF0gd2lsbCBhbHdheXMgYmUgYmUgMVxyXG4gICAgICAgICAgICAgIGZvciAoaSA9IDEsIHN0ciA9ICcxLic7IGkgPCBsZW47IGkrKykgc3RyICs9IE5VTUVSQUxTLmNoYXJBdCh4ZFtpXSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgc3RyID0gc3RyLmNoYXJBdCgwKSArICcuJyArIHN0ci5zbGljZSgxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHN0ciA9ICBzdHIgKyAoZSA8IDAgPyAncCcgOiAncCsnKSArIGU7XHJcbiAgICAgICAgfSBlbHNlIGlmIChlIDwgMCkge1xyXG4gICAgICAgICAgZm9yICg7ICsrZTspIHN0ciA9ICcwJyArIHN0cjtcclxuICAgICAgICAgIHN0ciA9ICcwLicgKyBzdHI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmICgrK2UgPiBsZW4pIGZvciAoZSAtPSBsZW47IGUtLSA7KSBzdHIgKz0gJzAnO1xyXG4gICAgICAgICAgZWxzZSBpZiAoZSA8IGxlbikgc3RyID0gc3RyLnNsaWNlKDAsIGUpICsgJy4nICsgc3RyLnNsaWNlKGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgc3RyID0gKGJhc2VPdXQgPT0gMTYgPyAnMHgnIDogYmFzZU91dCA9PSAyID8gJzBiJyA6IGJhc2VPdXQgPT0gOCA/ICcwbycgOiAnJykgKyBzdHI7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHgucyA8IDAgPyAnLScgKyBzdHIgOiBzdHI7XHJcbiAgfVxyXG5cclxuXHJcbiAgLy8gRG9lcyBub3Qgc3RyaXAgdHJhaWxpbmcgemVyb3MuXHJcbiAgZnVuY3Rpb24gdHJ1bmNhdGUoYXJyLCBsZW4pIHtcclxuICAgIGlmIChhcnIubGVuZ3RoID4gbGVuKSB7XHJcbiAgICAgIGFyci5sZW5ndGggPSBsZW47XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcblxyXG4gIC8vIERlY2ltYWwgbWV0aG9kc1xyXG5cclxuXHJcbiAgLypcclxuICAgKiAgYWJzXHJcbiAgICogIGFjb3NcclxuICAgKiAgYWNvc2hcclxuICAgKiAgYWRkXHJcbiAgICogIGFzaW5cclxuICAgKiAgYXNpbmhcclxuICAgKiAgYXRhblxyXG4gICAqICBhdGFuaFxyXG4gICAqICBhdGFuMlxyXG4gICAqICBjYnJ0XHJcbiAgICogIGNlaWxcclxuICAgKiAgY2xvbmVcclxuICAgKiAgY29uZmlnXHJcbiAgICogIGNvc1xyXG4gICAqICBjb3NoXHJcbiAgICogIGRpdlxyXG4gICAqICBleHBcclxuICAgKiAgZmxvb3JcclxuICAgKiAgaHlwb3RcclxuICAgKiAgbG5cclxuICAgKiAgbG9nXHJcbiAgICogIGxvZzJcclxuICAgKiAgbG9nMTBcclxuICAgKiAgbWF4XHJcbiAgICogIG1pblxyXG4gICAqICBtb2RcclxuICAgKiAgbXVsXHJcbiAgICogIHBvd1xyXG4gICAqICByYW5kb21cclxuICAgKiAgcm91bmRcclxuICAgKiAgc2V0XHJcbiAgICogIHNpZ25cclxuICAgKiAgc2luXHJcbiAgICogIHNpbmhcclxuICAgKiAgc3FydFxyXG4gICAqICBzdWJcclxuICAgKiAgdGFuXHJcbiAgICogIHRhbmhcclxuICAgKiAgdHJ1bmNcclxuICAgKi9cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGFic29sdXRlIHZhbHVlIG9mIGB4YC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGFicyh4KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkuYWJzKCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgYXJjY29zaW5lIGluIHJhZGlhbnMgb2YgYHhgLlxyXG4gICAqXHJcbiAgICogeCB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfVxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gYWNvcyh4KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkuYWNvcygpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGludmVyc2Ugb2YgdGhlIGh5cGVyYm9saWMgY29zaW5lIG9mIGB4YCwgcm91bmRlZCB0b1xyXG4gICAqIGBwcmVjaXNpb25gIHNpZ25pZmljYW50IGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IEEgdmFsdWUgaW4gcmFkaWFucy5cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGFjb3NoKHgpIHtcclxuICAgIHJldHVybiBuZXcgdGhpcyh4KS5hY29zaCgpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIHN1bSBvZiBgeGAgYW5kIGB5YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYCBzaWduaWZpY2FudFxyXG4gICAqIGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICogeSB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfVxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gYWRkKHgsIHkpIHtcclxuICAgIHJldHVybiBuZXcgdGhpcyh4KS5wbHVzKHkpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGFyY3NpbmUgaW4gcmFkaWFucyBvZiBgeGAsIHJvdW5kZWQgdG8gYHByZWNpc2lvbmBcclxuICAgKiBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogeCB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfVxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gYXNpbih4KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkuYXNpbigpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGludmVyc2Ugb2YgdGhlIGh5cGVyYm9saWMgc2luZSBvZiBgeGAsIHJvdW5kZWQgdG9cclxuICAgKiBgcHJlY2lzaW9uYCBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogeCB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfSBBIHZhbHVlIGluIHJhZGlhbnMuXHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBhc2luaCh4KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkuYXNpbmgoKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBhcmN0YW5nZW50IGluIHJhZGlhbnMgb2YgYHhgLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gXHJcbiAgICogc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGF0YW4oeCkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLmF0YW4oKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBpbnZlcnNlIG9mIHRoZSBoeXBlcmJvbGljIHRhbmdlbnQgb2YgYHhgLCByb3VuZGVkIHRvXHJcbiAgICogYHByZWNpc2lvbmAgc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH0gQSB2YWx1ZSBpbiByYWRpYW5zLlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gYXRhbmgoeCkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLmF0YW5oKCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgYXJjdGFuZ2VudCBpbiByYWRpYW5zIG9mIGB5L3hgIGluIHRoZSByYW5nZSAtcGkgdG8gcGlcclxuICAgKiAoaW5jbHVzaXZlKSwgcm91bmRlZCB0byBgcHJlY2lzaW9uYCBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogRG9tYWluOiBbLUluZmluaXR5LCBJbmZpbml0eV1cclxuICAgKiBSYW5nZTogWy1waSwgcGldXHJcbiAgICpcclxuICAgKiB5IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IFRoZSB5LWNvb3JkaW5hdGUuXHJcbiAgICogeCB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfSBUaGUgeC1jb29yZGluYXRlLlxyXG4gICAqXHJcbiAgICogYXRhbjIowrEwLCAtMCkgICAgICAgICAgICAgICA9IMKxcGlcclxuICAgKiBhdGFuMijCsTAsICswKSAgICAgICAgICAgICAgID0gwrEwXHJcbiAgICogYXRhbjIowrEwLCAteCkgICAgICAgICAgICAgICA9IMKxcGkgZm9yIHggPiAwXHJcbiAgICogYXRhbjIowrEwLCB4KSAgICAgICAgICAgICAgICA9IMKxMCBmb3IgeCA+IDBcclxuICAgKiBhdGFuMigteSwgwrEwKSAgICAgICAgICAgICAgID0gLXBpLzIgZm9yIHkgPiAwXHJcbiAgICogYXRhbjIoeSwgwrEwKSAgICAgICAgICAgICAgICA9IHBpLzIgZm9yIHkgPiAwXHJcbiAgICogYXRhbjIowrF5LCAtSW5maW5pdHkpICAgICAgICA9IMKxcGkgZm9yIGZpbml0ZSB5ID4gMFxyXG4gICAqIGF0YW4yKMKxeSwgK0luZmluaXR5KSAgICAgICAgPSDCsTAgZm9yIGZpbml0ZSB5ID4gMFxyXG4gICAqIGF0YW4yKMKxSW5maW5pdHksIHgpICAgICAgICAgPSDCsXBpLzIgZm9yIGZpbml0ZSB4XHJcbiAgICogYXRhbjIowrFJbmZpbml0eSwgLUluZmluaXR5KSA9IMKxMypwaS80XHJcbiAgICogYXRhbjIowrFJbmZpbml0eSwgK0luZmluaXR5KSA9IMKxcGkvNFxyXG4gICAqIGF0YW4yKE5hTiwgeCkgPSBOYU5cclxuICAgKiBhdGFuMih5LCBOYU4pID0gTmFOXHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBhdGFuMih5LCB4KSB7XHJcbiAgICB5ID0gbmV3IHRoaXMoeSk7XHJcbiAgICB4ID0gbmV3IHRoaXMoeCk7XHJcbiAgICB2YXIgcixcclxuICAgICAgcHIgPSB0aGlzLnByZWNpc2lvbixcclxuICAgICAgcm0gPSB0aGlzLnJvdW5kaW5nLFxyXG4gICAgICB3cHIgPSBwciArIDQ7XHJcblxyXG4gICAgLy8gRWl0aGVyIE5hTlxyXG4gICAgaWYgKCF5LnMgfHwgIXgucykge1xyXG4gICAgICByID0gbmV3IHRoaXMoTmFOKTtcclxuXHJcbiAgICAvLyBCb3RoIMKxSW5maW5pdHlcclxuICAgIH0gZWxzZSBpZiAoIXkuZCAmJiAheC5kKSB7XHJcbiAgICAgIHIgPSBnZXRQaSh0aGlzLCB3cHIsIDEpLnRpbWVzKHgucyA+IDAgPyAwLjI1IDogMC43NSk7XHJcbiAgICAgIHIucyA9IHkucztcclxuXHJcbiAgICAvLyB4IGlzIMKxSW5maW5pdHkgb3IgeSBpcyDCsTBcclxuICAgIH0gZWxzZSBpZiAoIXguZCB8fCB5LmlzWmVybygpKSB7XHJcbiAgICAgIHIgPSB4LnMgPCAwID8gZ2V0UGkodGhpcywgcHIsIHJtKSA6IG5ldyB0aGlzKDApO1xyXG4gICAgICByLnMgPSB5LnM7XHJcblxyXG4gICAgLy8geSBpcyDCsUluZmluaXR5IG9yIHggaXMgwrEwXHJcbiAgICB9IGVsc2UgaWYgKCF5LmQgfHwgeC5pc1plcm8oKSkge1xyXG4gICAgICByID0gZ2V0UGkodGhpcywgd3ByLCAxKS50aW1lcygwLjUpO1xyXG4gICAgICByLnMgPSB5LnM7XHJcblxyXG4gICAgLy8gQm90aCBub24temVybyBhbmQgZmluaXRlXHJcbiAgICB9IGVsc2UgaWYgKHgucyA8IDApIHtcclxuICAgICAgdGhpcy5wcmVjaXNpb24gPSB3cHI7XHJcbiAgICAgIHRoaXMucm91bmRpbmcgPSAxO1xyXG4gICAgICByID0gdGhpcy5hdGFuKGRpdmlkZSh5LCB4LCB3cHIsIDEpKTtcclxuICAgICAgeCA9IGdldFBpKHRoaXMsIHdwciwgMSk7XHJcbiAgICAgIHRoaXMucHJlY2lzaW9uID0gcHI7XHJcbiAgICAgIHRoaXMucm91bmRpbmcgPSBybTtcclxuICAgICAgciA9IHkucyA8IDAgPyByLm1pbnVzKHgpIDogci5wbHVzKHgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgciA9IHRoaXMuYXRhbihkaXZpZGUoeSwgeCwgd3ByLCAxKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHI7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgY3ViZSByb290IG9mIGB4YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYCBzaWduaWZpY2FudFxyXG4gICAqIGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBjYnJ0KHgpIHtcclxuICAgIHJldHVybiBuZXcgdGhpcyh4KS5jYnJ0KCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyBgeGAgcm91bmRlZCB0byBhbiBpbnRlZ2VyIHVzaW5nIGBST1VORF9DRUlMYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGNlaWwoeCkge1xyXG4gICAgcmV0dXJuIGZpbmFsaXNlKHggPSBuZXcgdGhpcyh4KSwgeC5lICsgMSwgMik7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBDb25maWd1cmUgZ2xvYmFsIHNldHRpbmdzIGZvciBhIERlY2ltYWwgY29uc3RydWN0b3IuXHJcbiAgICpcclxuICAgKiBgb2JqYCBpcyBhbiBvYmplY3Qgd2l0aCBvbmUgb3IgbW9yZSBvZiB0aGUgZm9sbG93aW5nIHByb3BlcnRpZXMsXHJcbiAgICpcclxuICAgKiAgIHByZWNpc2lvbiAge251bWJlcn1cclxuICAgKiAgIHJvdW5kaW5nICAge251bWJlcn1cclxuICAgKiAgIHRvRXhwTmVnICAge251bWJlcn1cclxuICAgKiAgIHRvRXhwUG9zICAge251bWJlcn1cclxuICAgKiAgIG1heEUgICAgICAge251bWJlcn1cclxuICAgKiAgIG1pbkUgICAgICAge251bWJlcn1cclxuICAgKiAgIG1vZHVsbyAgICAge251bWJlcn1cclxuICAgKiAgIGNyeXB0byAgICAge2Jvb2xlYW58bnVtYmVyfVxyXG4gICAqXHJcbiAgICogRS5nLiBEZWNpbWFsLmNvbmZpZyh7IHByZWNpc2lvbjogMjAsIHJvdW5kaW5nOiA0IH0pXHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBjb25maWcob2JqKSB7XHJcbiAgICBpZiAoIW9iaiB8fCB0eXBlb2Ygb2JqICE9PSAnb2JqZWN0JykgdGhyb3cgRXJyb3IoZGVjaW1hbEVycm9yICsgJ09iamVjdCBleHBlY3RlZCcpO1xyXG4gICAgdmFyIGksIHAsIHYsXHJcbiAgICAgIHBzID0gW1xyXG4gICAgICAgICdwcmVjaXNpb24nLCAxLCBNQVhfRElHSVRTLFxyXG4gICAgICAgICdyb3VuZGluZycsIDAsIDgsXHJcbiAgICAgICAgJ3RvRXhwTmVnJywgLUVYUF9MSU1JVCwgMCxcclxuICAgICAgICAndG9FeHBQb3MnLCAwLCBFWFBfTElNSVQsXHJcbiAgICAgICAgJ21heEUnLCAwLCBFWFBfTElNSVQsXHJcbiAgICAgICAgJ21pbkUnLCAtRVhQX0xJTUlULCAwLFxyXG4gICAgICAgICdtb2R1bG8nLCAwLCA5XHJcbiAgICAgIF07XHJcblxyXG4gICAgZm9yIChpID0gMDsgaSA8IHBzLmxlbmd0aDsgaSArPSAzKSB7XHJcbiAgICAgIGlmICgodiA9IG9ialtwID0gcHNbaV1dKSAhPT0gdm9pZCAwKSB7XHJcbiAgICAgICAgaWYgKG1hdGhmbG9vcih2KSA9PT0gdiAmJiB2ID49IHBzW2kgKyAxXSAmJiB2IDw9IHBzW2kgKyAyXSkgdGhpc1twXSA9IHY7XHJcbiAgICAgICAgZWxzZSB0aHJvdyBFcnJvcihpbnZhbGlkQXJndW1lbnQgKyBwICsgJzogJyArIHYpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCh2ID0gb2JqW3AgPSAnY3J5cHRvJ10pICE9PSB2b2lkIDApIHtcclxuICAgICAgaWYgKHYgPT09IHRydWUgfHwgdiA9PT0gZmFsc2UgfHwgdiA9PT0gMCB8fCB2ID09PSAxKSB7XHJcbiAgICAgICAgaWYgKHYpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgY3J5cHRvICE9ICd1bmRlZmluZWQnICYmIGNyeXB0byAmJlxyXG4gICAgICAgICAgICAoY3J5cHRvLmdldFJhbmRvbVZhbHVlcyB8fCBjcnlwdG8ucmFuZG9tQnl0ZXMpKSB7XHJcbiAgICAgICAgICAgIHRoaXNbcF0gPSB0cnVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgRXJyb3IoY3J5cHRvVW5hdmFpbGFibGUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzW3BdID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRocm93IEVycm9yKGludmFsaWRBcmd1bWVudCArIHAgKyAnOiAnICsgdik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBjb3NpbmUgb2YgYHhgLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gIHNpZ25pZmljYW50XHJcbiAgICogZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH0gQSB2YWx1ZSBpbiByYWRpYW5zLlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gY29zKHgpIHtcclxuICAgIHJldHVybiBuZXcgdGhpcyh4KS5jb3MoKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBoeXBlcmJvbGljIGNvc2luZSBvZiBgeGAsIHJvdW5kZWQgdG8gcHJlY2lzaW9uXHJcbiAgICogc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH0gQSB2YWx1ZSBpbiByYWRpYW5zLlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gY29zaCh4KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkuY29zaCgpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogQ3JlYXRlIGFuZCByZXR1cm4gYSBEZWNpbWFsIGNvbnN0cnVjdG9yIHdpdGggdGhlIHNhbWUgY29uZmlndXJhdGlvbiBwcm9wZXJ0aWVzIGFzIHRoaXMgRGVjaW1hbFxyXG4gICAqIGNvbnN0cnVjdG9yLlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gY2xvbmUob2JqKSB7XHJcbiAgICB2YXIgaSwgcCwgcHM7XHJcblxyXG4gICAgLypcclxuICAgICAqIFRoZSBEZWNpbWFsIGNvbnN0cnVjdG9yIGFuZCBleHBvcnRlZCBmdW5jdGlvbi5cclxuICAgICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIGluc3RhbmNlLlxyXG4gICAgICpcclxuICAgICAqIHYge251bWJlcnxzdHJpbmd8RGVjaW1hbH0gQSBudW1lcmljIHZhbHVlLlxyXG4gICAgICpcclxuICAgICAqL1xyXG4gICAgZnVuY3Rpb24gRGVjaW1hbCh2KSB7XHJcbiAgICAgIHZhciBlLCBpLCB0LFxyXG4gICAgICAgIHggPSB0aGlzO1xyXG5cclxuICAgICAgLy8gRGVjaW1hbCBjYWxsZWQgd2l0aG91dCBuZXcuXHJcbiAgICAgIGlmICghKHggaW5zdGFuY2VvZiBEZWNpbWFsKSkgcmV0dXJuIG5ldyBEZWNpbWFsKHYpO1xyXG5cclxuICAgICAgLy8gUmV0YWluIGEgcmVmZXJlbmNlIHRvIHRoaXMgRGVjaW1hbCBjb25zdHJ1Y3RvciwgYW5kIHNoYWRvdyBEZWNpbWFsLnByb3RvdHlwZS5jb25zdHJ1Y3RvclxyXG4gICAgICAvLyB3aGljaCBwb2ludHMgdG8gT2JqZWN0LlxyXG4gICAgICB4LmNvbnN0cnVjdG9yID0gRGVjaW1hbDtcclxuXHJcbiAgICAgIC8vIER1cGxpY2F0ZS5cclxuICAgICAgaWYgKHYgaW5zdGFuY2VvZiBEZWNpbWFsKSB7XHJcbiAgICAgICAgeC5zID0gdi5zO1xyXG4gICAgICAgIHguZSA9IHYuZTtcclxuICAgICAgICB4LmQgPSAodiA9IHYuZCkgPyB2LnNsaWNlKCkgOiB2O1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdCA9IHR5cGVvZiB2O1xyXG5cclxuICAgICAgaWYgKHQgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgaWYgKHYgPT09IDApIHtcclxuICAgICAgICAgIHgucyA9IDEgLyB2IDwgMCA/IC0xIDogMTtcclxuICAgICAgICAgIHguZSA9IDA7XHJcbiAgICAgICAgICB4LmQgPSBbMF07XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodiA8IDApIHtcclxuICAgICAgICAgIHYgPSAtdjtcclxuICAgICAgICAgIHgucyA9IC0xO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB4LnMgPSAxO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gRmFzdCBwYXRoIGZvciBzbWFsbCBpbnRlZ2Vycy5cclxuICAgICAgICBpZiAodiA9PT0gfn52ICYmIHYgPCAxZTcpIHtcclxuICAgICAgICAgIGZvciAoZSA9IDAsIGkgPSB2OyBpID49IDEwOyBpIC89IDEwKSBlKys7XHJcbiAgICAgICAgICB4LmUgPSBlO1xyXG4gICAgICAgICAgeC5kID0gW3ZdO1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICAvLyBJbmZpbml0eSwgTmFOLlxyXG4gICAgICAgIH0gZWxzZSBpZiAodiAqIDAgIT09IDApIHtcclxuICAgICAgICAgIGlmICghdikgeC5zID0gTmFOO1xyXG4gICAgICAgICAgeC5lID0gTmFOO1xyXG4gICAgICAgICAgeC5kID0gbnVsbDtcclxuICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBwYXJzZURlY2ltYWwoeCwgdi50b1N0cmluZygpKTtcclxuXHJcbiAgICAgIH0gZWxzZSBpZiAodCAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgICB0aHJvdyBFcnJvcihpbnZhbGlkQXJndW1lbnQgKyB2KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gTWludXMgc2lnbj9cclxuICAgICAgaWYgKHYuY2hhckNvZGVBdCgwKSA9PT0gNDUpIHtcclxuICAgICAgICB2ID0gdi5zbGljZSgxKTtcclxuICAgICAgICB4LnMgPSAtMTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB4LnMgPSAxO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gaXNEZWNpbWFsLnRlc3QodikgPyBwYXJzZURlY2ltYWwoeCwgdikgOiBwYXJzZU90aGVyKHgsIHYpO1xyXG4gICAgfVxyXG5cclxuICAgIERlY2ltYWwucHJvdG90eXBlID0gUDtcclxuXHJcbiAgICBEZWNpbWFsLlJPVU5EX1VQID0gMDtcclxuICAgIERlY2ltYWwuUk9VTkRfRE9XTiA9IDE7XHJcbiAgICBEZWNpbWFsLlJPVU5EX0NFSUwgPSAyO1xyXG4gICAgRGVjaW1hbC5ST1VORF9GTE9PUiA9IDM7XHJcbiAgICBEZWNpbWFsLlJPVU5EX0hBTEZfVVAgPSA0O1xyXG4gICAgRGVjaW1hbC5ST1VORF9IQUxGX0RPV04gPSA1O1xyXG4gICAgRGVjaW1hbC5ST1VORF9IQUxGX0VWRU4gPSA2O1xyXG4gICAgRGVjaW1hbC5ST1VORF9IQUxGX0NFSUwgPSA3O1xyXG4gICAgRGVjaW1hbC5ST1VORF9IQUxGX0ZMT09SID0gODtcclxuICAgIERlY2ltYWwuRVVDTElEID0gOTtcclxuXHJcbiAgICBEZWNpbWFsLmNvbmZpZyA9IERlY2ltYWwuc2V0ID0gY29uZmlnO1xyXG4gICAgRGVjaW1hbC5jbG9uZSA9IGNsb25lO1xyXG5cclxuICAgIERlY2ltYWwuYWJzID0gYWJzO1xyXG4gICAgRGVjaW1hbC5hY29zID0gYWNvcztcclxuICAgIERlY2ltYWwuYWNvc2ggPSBhY29zaDsgICAgICAgIC8vIEVTNlxyXG4gICAgRGVjaW1hbC5hZGQgPSBhZGQ7XHJcbiAgICBEZWNpbWFsLmFzaW4gPSBhc2luO1xyXG4gICAgRGVjaW1hbC5hc2luaCA9IGFzaW5oOyAgICAgICAgLy8gRVM2XHJcbiAgICBEZWNpbWFsLmF0YW4gPSBhdGFuO1xyXG4gICAgRGVjaW1hbC5hdGFuaCA9IGF0YW5oOyAgICAgICAgLy8gRVM2XHJcbiAgICBEZWNpbWFsLmF0YW4yID0gYXRhbjI7XHJcbiAgICBEZWNpbWFsLmNicnQgPSBjYnJ0OyAgICAgICAgICAvLyBFUzZcclxuICAgIERlY2ltYWwuY2VpbCA9IGNlaWw7XHJcbiAgICBEZWNpbWFsLmNvcyA9IGNvcztcclxuICAgIERlY2ltYWwuY29zaCA9IGNvc2g7ICAgICAgICAgIC8vIEVTNlxyXG4gICAgRGVjaW1hbC5kaXYgPSBkaXY7XHJcbiAgICBEZWNpbWFsLmV4cCA9IGV4cDtcclxuICAgIERlY2ltYWwuZmxvb3IgPSBmbG9vcjtcclxuICAgIERlY2ltYWwuaHlwb3QgPSBoeXBvdDsgICAgICAgIC8vIEVTNlxyXG4gICAgRGVjaW1hbC5sbiA9IGxuO1xyXG4gICAgRGVjaW1hbC5sb2cgPSBsb2c7XHJcbiAgICBEZWNpbWFsLmxvZzEwID0gbG9nMTA7ICAgICAgICAvLyBFUzZcclxuICAgIERlY2ltYWwubG9nMiA9IGxvZzI7ICAgICAgICAgIC8vIEVTNlxyXG4gICAgRGVjaW1hbC5tYXggPSBtYXg7XHJcbiAgICBEZWNpbWFsLm1pbiA9IG1pbjtcclxuICAgIERlY2ltYWwubW9kID0gbW9kO1xyXG4gICAgRGVjaW1hbC5tdWwgPSBtdWw7XHJcbiAgICBEZWNpbWFsLnBvdyA9IHBvdztcclxuICAgIERlY2ltYWwucmFuZG9tID0gcmFuZG9tO1xyXG4gICAgRGVjaW1hbC5yb3VuZCA9IHJvdW5kO1xyXG4gICAgRGVjaW1hbC5zaWduID0gc2lnbjsgICAgICAgICAgLy8gRVM2XHJcbiAgICBEZWNpbWFsLnNpbiA9IHNpbjtcclxuICAgIERlY2ltYWwuc2luaCA9IHNpbmg7ICAgICAgICAgIC8vIEVTNlxyXG4gICAgRGVjaW1hbC5zcXJ0ID0gc3FydDtcclxuICAgIERlY2ltYWwuc3ViID0gc3ViO1xyXG4gICAgRGVjaW1hbC50YW4gPSB0YW47XHJcbiAgICBEZWNpbWFsLnRhbmggPSB0YW5oOyAgICAgICAgICAvLyBFUzZcclxuICAgIERlY2ltYWwudHJ1bmMgPSB0cnVuYzsgICAgICAgIC8vIEVTNlxyXG5cclxuICAgIGlmIChvYmogPT09IHZvaWQgMCkgb2JqID0ge307XHJcbiAgICBpZiAob2JqKSB7XHJcbiAgICAgIHBzID0gWydwcmVjaXNpb24nLCAncm91bmRpbmcnLCAndG9FeHBOZWcnLCAndG9FeHBQb3MnLCAnbWF4RScsICdtaW5FJywgJ21vZHVsbycsICdjcnlwdG8nXTtcclxuICAgICAgZm9yIChpID0gMDsgaSA8IHBzLmxlbmd0aDspIGlmICghb2JqLmhhc093blByb3BlcnR5KHAgPSBwc1tpKytdKSkgb2JqW3BdID0gdGhpc1twXTtcclxuICAgIH1cclxuXHJcbiAgICBEZWNpbWFsLmNvbmZpZyhvYmopO1xyXG5cclxuICAgIHJldHVybiBEZWNpbWFsO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgYHhgIGRpdmlkZWQgYnkgYHlgLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gIHNpZ25pZmljYW50XHJcbiAgICogZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKiB5IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBkaXYoeCwgeSkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLmRpdih5KTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBuYXR1cmFsIGV4cG9uZW50aWFsIG9mIGB4YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYFxyXG4gICAqIHNpZ25pZmljYW50IGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IFRoZSBwb3dlciB0byB3aGljaCB0byByYWlzZSB0aGUgYmFzZSBvZiB0aGUgbmF0dXJhbCBsb2cuXHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBleHAoeCkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLmV4cCgpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgYHhgIHJvdW5kIHRvIGFuIGludGVnZXIgdXNpbmcgYFJPVU5EX0ZMT09SYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGZsb29yKHgpIHtcclxuICAgIHJldHVybiBmaW5hbGlzZSh4ID0gbmV3IHRoaXMoeCksIHguZSArIDEsIDMpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIHNxdWFyZSByb290IG9mIHRoZSBzdW0gb2YgdGhlIHNxdWFyZXMgb2YgdGhlIGFyZ3VtZW50cyxcclxuICAgKiByb3VuZGVkIHRvIGBwcmVjaXNpb25gIHNpZ25pZmljYW50IGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiBoeXBvdChhLCBiLCAuLi4pID0gc3FydChhXjIgKyBiXjIgKyAuLi4pXHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBoeXBvdCgpIHtcclxuICAgIHZhciBpLCBuLFxyXG4gICAgICB0ID0gbmV3IHRoaXMoMCk7XHJcblxyXG4gICAgZXh0ZXJuYWwgPSBmYWxzZTtcclxuXHJcbiAgICBmb3IgKGkgPSAwOyBpIDwgYXJndW1lbnRzLmxlbmd0aDspIHtcclxuICAgICAgbiA9IG5ldyB0aGlzKGFyZ3VtZW50c1tpKytdKTtcclxuICAgICAgaWYgKCFuLmQpIHtcclxuICAgICAgICBpZiAobi5zKSB7XHJcbiAgICAgICAgICBleHRlcm5hbCA9IHRydWU7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IHRoaXMoMSAvIDApO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0ID0gbjtcclxuICAgICAgfSBlbHNlIGlmICh0LmQpIHtcclxuICAgICAgICB0ID0gdC5wbHVzKG4udGltZXMobikpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZXh0ZXJuYWwgPSB0cnVlO1xyXG5cclxuICAgIHJldHVybiB0LnNxcnQoKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBuYXR1cmFsIGxvZ2FyaXRobSBvZiBgeGAsIHJvdW5kZWQgdG8gYHByZWNpc2lvbmBcclxuICAgKiBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogeCB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfVxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gbG4oeCkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLmxuKCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgbG9nIG9mIGB4YCB0byB0aGUgYmFzZSBgeWAsIG9yIHRvIGJhc2UgMTAgaWYgbm8gYmFzZVxyXG4gICAqIGlzIHNwZWNpZmllZCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYCBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogbG9nW3ldKHgpXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IFRoZSBhcmd1bWVudCBvZiB0aGUgbG9nYXJpdGhtLlxyXG4gICAqIHkge251bWJlcnxzdHJpbmd8RGVjaW1hbH0gVGhlIGJhc2Ugb2YgdGhlIGxvZ2FyaXRobS5cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGxvZyh4LCB5KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkubG9nKHkpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIGJhc2UgMiBsb2dhcml0aG0gb2YgYHhgLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gXHJcbiAgICogc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGxvZzIoeCkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLmxvZygyKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBiYXNlIDEwIGxvZ2FyaXRobSBvZiBgeGAsIHJvdW5kZWQgdG8gYHByZWNpc2lvbmBcclxuICAgKiBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogeCB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfVxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gbG9nMTAoeCkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLmxvZygxMCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgbWF4aW11bSBvZiB0aGUgYXJndW1lbnRzLlxyXG4gICAqXHJcbiAgICogYXJndW1lbnRzIHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBtYXgoKSB7XHJcbiAgICByZXR1cm4gbWF4T3JNaW4odGhpcywgYXJndW1lbnRzLCAnbHQnKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBtaW5pbXVtIG9mIHRoZSBhcmd1bWVudHMuXHJcbiAgICpcclxuICAgKiBhcmd1bWVudHMge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIG1pbigpIHtcclxuICAgIHJldHVybiBtYXhPck1pbih0aGlzLCBhcmd1bWVudHMsICdndCcpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgYHhgIG1vZHVsbyBgeWAsIHJvdW5kZWQgdG8gYHByZWNpc2lvbmAgc2lnbmlmaWNhbnQgZGlnaXRzXHJcbiAgICogdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogeCB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfVxyXG4gICAqIHkge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIG1vZCh4LCB5KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkubW9kKHkpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgYHhgIG11bHRpcGxpZWQgYnkgYHlgLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gIHNpZ25pZmljYW50XHJcbiAgICogZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKiB5IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBtdWwoeCwgeSkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLm11bCh5KTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIGB4YCByYWlzZWQgdG8gdGhlIHBvd2VyIGB5YCwgcm91bmRlZCB0byBwcmVjaXNpb25cclxuICAgKiBzaWduaWZpY2FudCBkaWdpdHMgdXNpbmcgcm91bmRpbmcgbW9kZSBgcm91bmRpbmdgLlxyXG4gICAqXHJcbiAgICogeCB7bnVtYmVyfHN0cmluZ3xEZWNpbWFsfSBUaGUgYmFzZS5cclxuICAgKiB5IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IFRoZSBleHBvbmVudC5cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHBvdyh4LCB5KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkucG93KHkpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJucyBhIG5ldyBEZWNpbWFsIHdpdGggYSByYW5kb20gdmFsdWUgZXF1YWwgdG8gb3IgZ3JlYXRlciB0aGFuIDAgYW5kIGxlc3MgdGhhbiAxLCBhbmQgd2l0aFxyXG4gICAqIGBzZGAsIG9yIGBEZWNpbWFsLnByZWNpc2lvbmAgaWYgYHNkYCBpcyBvbWl0dGVkLCBzaWduaWZpY2FudCBkaWdpdHMgKG9yIGxlc3MgaWYgdHJhaWxpbmcgemVyb3NcclxuICAgKiBhcmUgcHJvZHVjZWQpLlxyXG4gICAqXHJcbiAgICogW3NkXSB7bnVtYmVyfSBTaWduaWZpY2FudCBkaWdpdHMuIEludGVnZXIsIDAgdG8gTUFYX0RJR0lUUyBpbmNsdXNpdmUuXHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiByYW5kb20oc2QpIHtcclxuICAgIHZhciBkLCBlLCBrLCBuLFxyXG4gICAgICBpID0gMCxcclxuICAgICAgciA9IG5ldyB0aGlzKDEpLFxyXG4gICAgICByZCA9IFtdO1xyXG5cclxuICAgIGlmIChzZCA9PT0gdm9pZCAwKSBzZCA9IHRoaXMucHJlY2lzaW9uO1xyXG4gICAgZWxzZSBjaGVja0ludDMyKHNkLCAxLCBNQVhfRElHSVRTKTtcclxuXHJcbiAgICBrID0gTWF0aC5jZWlsKHNkIC8gTE9HX0JBU0UpO1xyXG5cclxuICAgIGlmICghdGhpcy5jcnlwdG8pIHtcclxuICAgICAgZm9yICg7IGkgPCBrOykgcmRbaSsrXSA9IE1hdGgucmFuZG9tKCkgKiAxZTcgfCAwO1xyXG5cclxuICAgIC8vIEJyb3dzZXJzIHN1cHBvcnRpbmcgY3J5cHRvLmdldFJhbmRvbVZhbHVlcy5cclxuICAgIH0gZWxzZSBpZiAoY3J5cHRvLmdldFJhbmRvbVZhbHVlcykge1xyXG4gICAgICBkID0gY3J5cHRvLmdldFJhbmRvbVZhbHVlcyhuZXcgVWludDMyQXJyYXkoaykpO1xyXG5cclxuICAgICAgZm9yICg7IGkgPCBrOykge1xyXG4gICAgICAgIG4gPSBkW2ldO1xyXG5cclxuICAgICAgICAvLyAwIDw9IG4gPCA0Mjk0OTY3Mjk2XHJcbiAgICAgICAgLy8gUHJvYmFiaWxpdHkgbiA+PSA0LjI5ZTksIGlzIDQ5NjcyOTYgLyA0Mjk0OTY3Mjk2ID0gMC4wMDExNiAoMSBpbiA4NjUpLlxyXG4gICAgICAgIGlmIChuID49IDQuMjllOSkge1xyXG4gICAgICAgICAgZFtpXSA9IGNyeXB0by5nZXRSYW5kb21WYWx1ZXMobmV3IFVpbnQzMkFycmF5KDEpKVswXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgIC8vIDAgPD0gbiA8PSA0Mjg5OTk5OTk5XHJcbiAgICAgICAgICAvLyAwIDw9IChuICUgMWU3KSA8PSA5OTk5OTk5XHJcbiAgICAgICAgICByZFtpKytdID0gbiAlIDFlNztcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAvLyBOb2RlLmpzIHN1cHBvcnRpbmcgY3J5cHRvLnJhbmRvbUJ5dGVzLlxyXG4gICAgfSBlbHNlIGlmIChjcnlwdG8ucmFuZG9tQnl0ZXMpIHtcclxuXHJcbiAgICAgIC8vIGJ1ZmZlclxyXG4gICAgICBkID0gY3J5cHRvLnJhbmRvbUJ5dGVzKGsgKj0gNCk7XHJcblxyXG4gICAgICBmb3IgKDsgaSA8IGs7KSB7XHJcblxyXG4gICAgICAgIC8vIDAgPD0gbiA8IDIxNDc0ODM2NDhcclxuICAgICAgICBuID0gZFtpXSArIChkW2kgKyAxXSA8PCA4KSArIChkW2kgKyAyXSA8PCAxNikgKyAoKGRbaSArIDNdICYgMHg3ZikgPDwgMjQpO1xyXG5cclxuICAgICAgICAvLyBQcm9iYWJpbGl0eSBuID49IDIuMTRlOSwgaXMgNzQ4MzY0OCAvIDIxNDc0ODM2NDggPSAwLjAwMzUgKDEgaW4gMjg2KS5cclxuICAgICAgICBpZiAobiA+PSAyLjE0ZTkpIHtcclxuICAgICAgICAgIGNyeXB0by5yYW5kb21CeXRlcyg0KS5jb3B5KGQsIGkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgLy8gMCA8PSBuIDw9IDIxMzk5OTk5OTlcclxuICAgICAgICAgIC8vIDAgPD0gKG4gJSAxZTcpIDw9IDk5OTk5OTlcclxuICAgICAgICAgIHJkLnB1c2gobiAlIDFlNyk7XHJcbiAgICAgICAgICBpICs9IDQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpID0gayAvIDQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aHJvdyBFcnJvcihjcnlwdG9VbmF2YWlsYWJsZSk7XHJcbiAgICB9XHJcblxyXG4gICAgayA9IHJkWy0taV07XHJcbiAgICBzZCAlPSBMT0dfQkFTRTtcclxuXHJcbiAgICAvLyBDb252ZXJ0IHRyYWlsaW5nIGRpZ2l0cyB0byB6ZXJvcyBhY2NvcmRpbmcgdG8gc2QuXHJcbiAgICBpZiAoayAmJiBzZCkge1xyXG4gICAgICBuID0gbWF0aHBvdygxMCwgTE9HX0JBU0UgLSBzZCk7XHJcbiAgICAgIHJkW2ldID0gKGsgLyBuIHwgMCkgKiBuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJlbW92ZSB0cmFpbGluZyB3b3JkcyB3aGljaCBhcmUgemVyby5cclxuICAgIGZvciAoOyByZFtpXSA9PT0gMDsgaS0tKSByZC5wb3AoKTtcclxuXHJcbiAgICAvLyBaZXJvP1xyXG4gICAgaWYgKGkgPCAwKSB7XHJcbiAgICAgIGUgPSAwO1xyXG4gICAgICByZCA9IFswXTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGUgPSAtMTtcclxuXHJcbiAgICAgIC8vIFJlbW92ZSBsZWFkaW5nIHdvcmRzIHdoaWNoIGFyZSB6ZXJvIGFuZCBhZGp1c3QgZXhwb25lbnQgYWNjb3JkaW5nbHkuXHJcbiAgICAgIGZvciAoOyByZFswXSA9PT0gMDsgZSAtPSBMT0dfQkFTRSkgcmQuc2hpZnQoKTtcclxuXHJcbiAgICAgIC8vIENvdW50IHRoZSBkaWdpdHMgb2YgdGhlIGZpcnN0IHdvcmQgb2YgcmQgdG8gZGV0ZXJtaW5lIGxlYWRpbmcgemVyb3MuXHJcbiAgICAgIGZvciAoayA9IDEsIG4gPSByZFswXTsgbiA+PSAxMDsgbiAvPSAxMCkgaysrO1xyXG5cclxuICAgICAgLy8gQWRqdXN0IHRoZSBleHBvbmVudCBmb3IgbGVhZGluZyB6ZXJvcyBvZiB0aGUgZmlyc3Qgd29yZCBvZiByZC5cclxuICAgICAgaWYgKGsgPCBMT0dfQkFTRSkgZSAtPSBMT0dfQkFTRSAtIGs7XHJcbiAgICB9XHJcblxyXG4gICAgci5lID0gZTtcclxuICAgIHIuZCA9IHJkO1xyXG5cclxuICAgIHJldHVybiByO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgYHhgIHJvdW5kZWQgdG8gYW4gaW50ZWdlciB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiBUbyBlbXVsYXRlIGBNYXRoLnJvdW5kYCwgc2V0IHJvdW5kaW5nIHRvIDcgKFJPVU5EX0hBTEZfQ0VJTCkuXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiByb3VuZCh4KSB7XHJcbiAgICByZXR1cm4gZmluYWxpc2UoeCA9IG5ldyB0aGlzKHgpLCB4LmUgKyAxLCB0aGlzLnJvdW5kaW5nKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVyblxyXG4gICAqICAgMSAgICBpZiB4ID4gMCxcclxuICAgKiAgLTEgICAgaWYgeCA8IDAsXHJcbiAgICogICAwICAgIGlmIHggaXMgMCxcclxuICAgKiAgLTAgICAgaWYgeCBpcyAtMCxcclxuICAgKiAgIE5hTiAgb3RoZXJ3aXNlXHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBzaWduKHgpIHtcclxuICAgIHggPSBuZXcgdGhpcyh4KTtcclxuICAgIHJldHVybiB4LmQgPyAoeC5kWzBdID8geC5zIDogMCAqIHgucykgOiB4LnMgfHwgTmFOO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIHNpbmUgb2YgYHhgLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gIHNpZ25pZmljYW50IGRpZ2l0c1xyXG4gICAqIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH0gQSB2YWx1ZSBpbiByYWRpYW5zLlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gc2luKHgpIHtcclxuICAgIHJldHVybiBuZXcgdGhpcyh4KS5zaW4oKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSBoeXBlcmJvbGljIHNpbmUgb2YgYHhgLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gXHJcbiAgICogc2lnbmlmaWNhbnQgZGlnaXRzIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH0gQSB2YWx1ZSBpbiByYWRpYW5zLlxyXG4gICAqXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gc2luaCh4KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkuc2luaCgpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8qXHJcbiAgICogUmV0dXJuIGEgbmV3IERlY2ltYWwgd2hvc2UgdmFsdWUgaXMgdGhlIHNxdWFyZSByb290IG9mIGB4YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYCBzaWduaWZpY2FudFxyXG4gICAqIGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBzcXJ0KHgpIHtcclxuICAgIHJldHVybiBuZXcgdGhpcyh4KS5zcXJ0KCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyBgeGAgbWludXMgYHlgLCByb3VuZGVkIHRvIGBwcmVjaXNpb25gIHNpZ25pZmljYW50IGRpZ2l0c1xyXG4gICAqIHVzaW5nIHJvdW5kaW5nIG1vZGUgYHJvdW5kaW5nYC5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKiB5IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9XHJcbiAgICpcclxuICAgKi9cclxuICBmdW5jdGlvbiBzdWIoeCwgeSkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLnN1Yih5KTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIHRoZSB0YW5nZW50IG9mIGB4YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYCBzaWduaWZpY2FudFxyXG4gICAqIGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IEEgdmFsdWUgaW4gcmFkaWFucy5cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHRhbih4KSB7XHJcbiAgICByZXR1cm4gbmV3IHRoaXMoeCkudGFuKCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLypcclxuICAgKiBSZXR1cm4gYSBuZXcgRGVjaW1hbCB3aG9zZSB2YWx1ZSBpcyB0aGUgaHlwZXJib2xpYyB0YW5nZW50IG9mIGB4YCwgcm91bmRlZCB0byBgcHJlY2lzaW9uYFxyXG4gICAqIHNpZ25pZmljYW50IGRpZ2l0cyB1c2luZyByb3VuZGluZyBtb2RlIGByb3VuZGluZ2AuXHJcbiAgICpcclxuICAgKiB4IHtudW1iZXJ8c3RyaW5nfERlY2ltYWx9IEEgdmFsdWUgaW4gcmFkaWFucy5cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHRhbmgoeCkge1xyXG4gICAgcmV0dXJuIG5ldyB0aGlzKHgpLnRhbmgoKTtcclxuICB9XHJcblxyXG5cclxuICAvKlxyXG4gICAqIFJldHVybiBhIG5ldyBEZWNpbWFsIHdob3NlIHZhbHVlIGlzIGB4YCB0cnVuY2F0ZWQgdG8gYW4gaW50ZWdlci5cclxuICAgKlxyXG4gICAqIHgge251bWJlcnxzdHJpbmd8RGVjaW1hbH1cclxuICAgKlxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHRydW5jKHgpIHtcclxuICAgIHJldHVybiBmaW5hbGlzZSh4ID0gbmV3IHRoaXMoeCksIHguZSArIDEsIDEpO1xyXG4gIH1cclxuXHJcblxyXG4gIC8vIENyZWF0ZSBhbmQgY29uZmlndXJlIGluaXRpYWwgRGVjaW1hbCBjb25zdHJ1Y3Rvci5cclxuICBEZWNpbWFsID0gY2xvbmUoRGVjaW1hbCk7XHJcblxyXG4gIERlY2ltYWxbJ2RlZmF1bHQnXSA9IERlY2ltYWwuRGVjaW1hbCA9IERlY2ltYWw7XHJcblxyXG4gIC8vIENyZWF0ZSB0aGUgaW50ZXJuYWwgY29uc3RhbnRzIGZyb20gdGhlaXIgc3RyaW5nIHZhbHVlcy5cclxuICBMTjEwID0gbmV3IERlY2ltYWwoTE4xMCk7XHJcbiAgUEkgPSBuZXcgRGVjaW1hbChQSSk7XHJcblxyXG5cclxuICAvLyBFeHBvcnQuXHJcblxyXG5cclxuICAvLyBBTUQuXHJcbiAgaWYgKHR5cGVvZiBkZWZpbmUgPT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XHJcbiAgICBkZWZpbmUoZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gRGVjaW1hbDtcclxuICAgIH0pO1xyXG5cclxuICAvLyBOb2RlIGFuZCBvdGhlciBlbnZpcm9ubWVudHMgdGhhdCBzdXBwb3J0IG1vZHVsZS5leHBvcnRzLlxyXG4gIH0gZWxzZSBpZiAodHlwZW9mIG1vZHVsZSAhPSAndW5kZWZpbmVkJyAmJiBtb2R1bGUuZXhwb3J0cykge1xyXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBEZWNpbWFsO1xyXG5cclxuICAvLyBCcm93c2VyLlxyXG4gIH0gZWxzZSB7XHJcbiAgICBpZiAoIWdsb2JhbFNjb3BlKSB7XHJcbiAgICAgIGdsb2JhbFNjb3BlID0gdHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZiAmJiBzZWxmLnNlbGYgPT0gc2VsZlxyXG4gICAgICAgID8gc2VsZiA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbm9Db25mbGljdCA9IGdsb2JhbFNjb3BlLkRlY2ltYWw7XHJcbiAgICBEZWNpbWFsLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGdsb2JhbFNjb3BlLkRlY2ltYWwgPSBub0NvbmZsaWN0O1xyXG4gICAgICByZXR1cm4gRGVjaW1hbDtcclxuICAgIH07XHJcblxyXG4gICAgZ2xvYmFsU2NvcGUuRGVjaW1hbCA9IERlY2ltYWw7XHJcbiAgfVxyXG59KSh0aGlzKTtcclxuIiwiLyoqXG4gKiBDb252ZXJ0IGFycmF5IG9mIDE2IGJ5dGUgdmFsdWVzIHRvIFVVSUQgc3RyaW5nIGZvcm1hdCBvZiB0aGUgZm9ybTpcbiAqIFhYWFhYWFhYLVhYWFgtWFhYWC1YWFhYLVhYWFhYWFhYWFhYWFxuICovXG52YXIgYnl0ZVRvSGV4ID0gW107XG5mb3IgKHZhciBpID0gMDsgaSA8IDI1NjsgKytpKSB7XG4gIGJ5dGVUb0hleFtpXSA9IChpICsgMHgxMDApLnRvU3RyaW5nKDE2KS5zdWJzdHIoMSk7XG59XG5cbmZ1bmN0aW9uIGJ5dGVzVG9VdWlkKGJ1Ziwgb2Zmc2V0KSB7XG4gIHZhciBpID0gb2Zmc2V0IHx8IDA7XG4gIHZhciBidGggPSBieXRlVG9IZXg7XG4gIC8vIGpvaW4gdXNlZCB0byBmaXggbWVtb3J5IGlzc3VlIGNhdXNlZCBieSBjb25jYXRlbmF0aW9uOiBodHRwczovL2J1Z3MuY2hyb21pdW0ub3JnL3AvdjgvaXNzdWVzL2RldGFpbD9pZD0zMTc1I2M0XG4gIHJldHVybiAoW1xuICAgIGJ0aFtidWZbaSsrXV0sIGJ0aFtidWZbaSsrXV0sXG4gICAgYnRoW2J1ZltpKytdXSwgYnRoW2J1ZltpKytdXSwgJy0nLFxuICAgIGJ0aFtidWZbaSsrXV0sIGJ0aFtidWZbaSsrXV0sICctJyxcbiAgICBidGhbYnVmW2krK11dLCBidGhbYnVmW2krK11dLCAnLScsXG4gICAgYnRoW2J1ZltpKytdXSwgYnRoW2J1ZltpKytdXSwgJy0nLFxuICAgIGJ0aFtidWZbaSsrXV0sIGJ0aFtidWZbaSsrXV0sXG4gICAgYnRoW2J1ZltpKytdXSwgYnRoW2J1ZltpKytdXSxcbiAgICBidGhbYnVmW2krK11dLCBidGhbYnVmW2krK11dXG4gIF0pLmpvaW4oJycpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGJ5dGVzVG9VdWlkO1xuIiwiLy8gVW5pcXVlIElEIGNyZWF0aW9uIHJlcXVpcmVzIGEgaGlnaCBxdWFsaXR5IHJhbmRvbSAjIGdlbmVyYXRvci4gIEluIHRoZVxuLy8gYnJvd3NlciB0aGlzIGlzIGEgbGl0dGxlIGNvbXBsaWNhdGVkIGR1ZSB0byB1bmtub3duIHF1YWxpdHkgb2YgTWF0aC5yYW5kb20oKVxuLy8gYW5kIGluY29uc2lzdGVudCBzdXBwb3J0IGZvciB0aGUgYGNyeXB0b2AgQVBJLiAgV2UgZG8gdGhlIGJlc3Qgd2UgY2FuIHZpYVxuLy8gZmVhdHVyZS1kZXRlY3Rpb25cblxuLy8gZ2V0UmFuZG9tVmFsdWVzIG5lZWRzIHRvIGJlIGludm9rZWQgaW4gYSBjb250ZXh0IHdoZXJlIFwidGhpc1wiIGlzIGEgQ3J5cHRvXG4vLyBpbXBsZW1lbnRhdGlvbi4gQWxzbywgZmluZCB0aGUgY29tcGxldGUgaW1wbGVtZW50YXRpb24gb2YgY3J5cHRvIG9uIElFMTEuXG52YXIgZ2V0UmFuZG9tVmFsdWVzID0gKHR5cGVvZihjcnlwdG8pICE9ICd1bmRlZmluZWQnICYmIGNyeXB0by5nZXRSYW5kb21WYWx1ZXMgJiYgY3J5cHRvLmdldFJhbmRvbVZhbHVlcy5iaW5kKGNyeXB0bykpIHx8XG4gICAgICAgICAgICAgICAgICAgICAgKHR5cGVvZihtc0NyeXB0bykgIT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIHdpbmRvdy5tc0NyeXB0by5nZXRSYW5kb21WYWx1ZXMgPT0gJ2Z1bmN0aW9uJyAmJiBtc0NyeXB0by5nZXRSYW5kb21WYWx1ZXMuYmluZChtc0NyeXB0bykpO1xuXG5pZiAoZ2V0UmFuZG9tVmFsdWVzKSB7XG4gIC8vIFdIQVRXRyBjcnlwdG8gUk5HIC0gaHR0cDovL3dpa2kud2hhdHdnLm9yZy93aWtpL0NyeXB0b1xuICB2YXIgcm5kczggPSBuZXcgVWludDhBcnJheSgxNik7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW5kZWZcblxuICBtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHdoYXR3Z1JORygpIHtcbiAgICBnZXRSYW5kb21WYWx1ZXMocm5kczgpO1xuICAgIHJldHVybiBybmRzODtcbiAgfTtcbn0gZWxzZSB7XG4gIC8vIE1hdGgucmFuZG9tKCktYmFzZWQgKFJORylcbiAgLy9cbiAgLy8gSWYgYWxsIGVsc2UgZmFpbHMsIHVzZSBNYXRoLnJhbmRvbSgpLiAgSXQncyBmYXN0LCBidXQgaXMgb2YgdW5zcGVjaWZpZWRcbiAgLy8gcXVhbGl0eS5cbiAgdmFyIHJuZHMgPSBuZXcgQXJyYXkoMTYpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbWF0aFJORygpIHtcbiAgICBmb3IgKHZhciBpID0gMCwgcjsgaSA8IDE2OyBpKyspIHtcbiAgICAgIGlmICgoaSAmIDB4MDMpID09PSAwKSByID0gTWF0aC5yYW5kb20oKSAqIDB4MTAwMDAwMDAwO1xuICAgICAgcm5kc1tpXSA9IHIgPj4+ICgoaSAmIDB4MDMpIDw8IDMpICYgMHhmZjtcbiAgICB9XG5cbiAgICByZXR1cm4gcm5kcztcbiAgfTtcbn1cbiIsInZhciBybmcgPSByZXF1aXJlKCcuL2xpYi9ybmcnKTtcbnZhciBieXRlc1RvVXVpZCA9IHJlcXVpcmUoJy4vbGliL2J5dGVzVG9VdWlkJyk7XG5cbmZ1bmN0aW9uIHY0KG9wdGlvbnMsIGJ1Ziwgb2Zmc2V0KSB7XG4gIHZhciBpID0gYnVmICYmIG9mZnNldCB8fCAwO1xuXG4gIGlmICh0eXBlb2Yob3B0aW9ucykgPT0gJ3N0cmluZycpIHtcbiAgICBidWYgPSBvcHRpb25zID09PSAnYmluYXJ5JyA/IG5ldyBBcnJheSgxNikgOiBudWxsO1xuICAgIG9wdGlvbnMgPSBudWxsO1xuICB9XG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuXG4gIHZhciBybmRzID0gb3B0aW9ucy5yYW5kb20gfHwgKG9wdGlvbnMucm5nIHx8IHJuZykoKTtcblxuICAvLyBQZXIgNC40LCBzZXQgYml0cyBmb3IgdmVyc2lvbiBhbmQgYGNsb2NrX3NlcV9oaV9hbmRfcmVzZXJ2ZWRgXG4gIHJuZHNbNl0gPSAocm5kc1s2XSAmIDB4MGYpIHwgMHg0MDtcbiAgcm5kc1s4XSA9IChybmRzWzhdICYgMHgzZikgfCAweDgwO1xuXG4gIC8vIENvcHkgYnl0ZXMgdG8gYnVmZmVyLCBpZiBwcm92aWRlZFxuICBpZiAoYnVmKSB7XG4gICAgZm9yICh2YXIgaWkgPSAwOyBpaSA8IDE2OyArK2lpKSB7XG4gICAgICBidWZbaSArIGlpXSA9IHJuZHNbaWldO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBidWYgfHwgYnl0ZXNUb1V1aWQocm5kcyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gdjQ7XG4iXX0=
