/* 
 * Idea is to keep drawing settings such as pencil width ...int his class
 * and then when user zoom these can be aware of the zoom level 
 * and scale accordingly
 * 
 * https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Transformations
 */
/*
function DrawingSettings() {

    this.init = function () {
        //this.penWidth = 1;
        
    };
    this.init();

    this.draw = function (context, zoomLevel) {
        context.lineWidth = 1/zoomLevel;
    };
 
}
*/
DrawingSettings = {};

DrawingSettings.draw = function (context, zoomLevel) {
    context.lineWidth = 1/zoomLevel;
};
module.exports = DrawingSettings;