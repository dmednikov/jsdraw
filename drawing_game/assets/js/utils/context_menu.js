$(function () {
   //console.info(pubsub);
   $.contextMenu({
      selector: '.context-menu-one',
      callback: function (key, options) {
         var m = "clicked: " + key;
         //consdeleteSelectedShapeole.info(options);
         switch (key) {
            case "delete":
               $.publish("deleteShape");
               break;
            case "straight line":
                $.publish("selectDrawingMode", {"key": "straightLine"});
               break;
            case "free form":
               //$("#paper").trigger("selectDrawingMode", {"param": "one", "key": "freeForm"});
               $.publish("selectDrawingMode", {"key": "freeForm"});
               break;
            case "circle":
               //$("#paper").trigger("selectDrawingMode", {"param": "one", "key": "circle"});
               $.publish("selectDrawingMode", {"key": "circle"});
               break;
            case "stopDrawing":
               //$("#paper").trigger("stopDrawing", {});
               $.publish("stopDrawing", {"key": "stop_drawing"});
               break;
            case "toggleGridVisibility":
               //$("#paper").trigger("toggleGridVisibility", {});
               $.publish("toggleGridVisibility");
               break;
            case "toggleRulerGuideVisibility":
               $.publish("toggleRulerGuideVisibility", {});
               break;
  
            default:
               // do nothing
         }
         
         //window.console && console.log(m) || alert(m);
      },
		items: {
			"straight line": {
				name: "straight line", 
				icon: function(opt, $itemElement, itemKey, item){
					
					// Set the content to the menu trigger selector and add an bootstrap icon to the item.
					$itemElement.html('<span> ' + itemKey +'</span> ');

					// Add the context-menu-icon-updated class to the item
					return 'context-menu-icon context-menu-icon-straight_line';
				}
			},
			"free form": {
				name: "free form", 
				icon: function(opt, $itemElement, itemKey, item){
	
					// Set the content to the menu trigger selector and add an bootstrap icon to the item.
					$itemElement.html('<span> ' + itemKey +'</span> ');

					// Add the context-menu-icon-updated class to the item
					return 'context-menu-icon context-menu-icon-free_form';
				}
			},
			"circle": {
				name: "circle", 
				icon: function(opt, $itemElement, itemKey, item){
	
					// Set the content to the menu trigger selector and add an bootstrap icon to the item.
					$itemElement.html('<span> ' + itemKey +'</span> ');

					// Add the context-menu-icon-updated class to the item
					return 'context-menu-icon context-menu-icon-circle';
				}
			},
			"sep1": "---------",
         "stopDrawing": {name: "stop drawing", icon: "delete"},
         "toggleGridVisibility": {name: "toggle grid", icon: "delete"},
         "toggleRulerGuideVisibility": {name: "toggle ruler guide", icon: "delete"},
         "delete": {name: "delete", icon: "delete"},
         "sep2": "---------",
         "quit": {name: "quit", icon: function () {
               return 'context-menu-icon context-menu-icon-quit';
            }}
      },
	  zIndex: 8
   });

   $('.context-menu-one').on('click', function (e) {

   });
});