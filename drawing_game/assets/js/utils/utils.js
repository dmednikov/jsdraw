/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
exports.isCollinear = function isCollinear(x1, y1, x2, y2, x3, y3) {
    var crossproduct = (y1 - y2) * (x3 - x2) - (x1 - x2) * (y3 - y2);
    return crossproduct;
};

exports.calculateDistance = function calculateDistance(x1, y1, x2, y2) {
    var distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    return distance;
};

exports.isPointOnLine = function isPointOnLine(x1, y1, x2, y2, x3, y3) {
    var crossproduct = isCollinear(x1, y1, x2, y2, x3, y3);
    // crossproduct === 0 in the perfext vector world

    if ((crossproduct < 200 && crossproduct > -200) && ((x2 <= x1 && x1 <= x3) || (x3 <= x1 && x1 <= x2)) && ((y2 <= y1 && y1 <= y3) || (y3 <= y1 && y1 <= y2))) {
        return true;
    }
    return false;
};