/*
 * share vars between server and client:  
 * http://stackoverflow.com/questions/3225251/how-can-i-share-code-between-node-js-and-the-browser
 */
(function(exports){

    const room_names = [
    	['Tokyo/Yokohama', 'Tokyo/Yokohama-Japan-33,200,000'],
    	['New York Metro', 'New York Metro-USA-17,800,000'],
    	['Sao Paulo', 'Sao Paulo-Brazil-17,700,000'],
    	['Seoul/Incheon', 'Seoul/Incheon-South Korea-17,500,000'],
    	['Mexico City','Mexico City-Mexico-17,400,000'],
    	['Osaka/Kobe/Kyoto','Osaka/Kobe/Kyoto-Japan-16,425,000'],
    	['Manila','Manila-Philippines-14,750,000'],
    	['Mumbai','Mumbai-India-14,350,000'],
    	['Delhi','Delhi-India-14,300,000'],
    	['Jakarta','Jakarta-Indonesia-14,250,00']
	];


   	exports.getLargestCities = function(){
        return room_names;
    };

})(typeof exports === 'undefined'? this.static_vars={}: exports);