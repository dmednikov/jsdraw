var EventEmitter = require('events').EventEmitter
  , pubsub = new EventEmitter().setMaxListeners(100);

module.exports =  pubsub;

pubsub.on('loggedIn', function(msg) {
    console.log(msg);
});