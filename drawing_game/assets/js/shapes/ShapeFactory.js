/* 
 * Generate objects based on type in JSON string
 */

var Circle = require('../shapes/Circle');
var Line = require('../shapes/Line');
var FreeForm = require('../shapes/FreeForm');
var Point = require('../shapes/Point');

class ShapeFactory {

    static createFromJSON (jsonRepresentationOfShape) {
        return ShapeFactory.create(jsonRepresentationOfShape.type, jsonRepresentationOfShape);
    }

    static create (shapeType, params) {
        if( ShapeFactory.isCircle(shapeType) ){
            const circle =  new Circle(
                        params.centerX, 
                        params.centerY, 
                        params.radius
                    );
            circle.setStrokeStyle(params.strokeStyle);
            circle.setUUID(params.UUID);
            return circle;
        } else if( ShapeFactory.isLine(shapeType) ){
            const line = new Line(
                params.originX, 
                params.originY, 
                params.endX,
                params.endY
            );
            line.setStrokeStyle(params.strokeStyle);
            line.setUUID(params.UUID);
            line.setLabel(params.label);
            return line;
        } else if( ShapeFactory.isFreeform(shapeType) ){
            const freeform = new FreeForm(
                        params.points
            );
            freeform.setStrokeStyle(params.strokeStyle);
            freeform.setUUID(params.UUID);
            return freeform;
        } else if( ShapeFactory.isPoint(shapeType) ){
            const point = new Point(
                        params.X, 
                        params.Y
            );
            point.setStrokeStyle(params.strokeStyle);
            point.setUUID(params.UUID);
            return point;
        } else {
            console.log(shapeType + ": this Shape type is not implemented in the factory");
        }
    }

    /*
     * This method only takes origin parameters and providex defaults for end point and radius
     */
    static initialize (shapeType, params) {
        if( ShapeFactory.isCircle(shapeType) ){
            return new Circle(
                params.originX, 
                params.originY, 
                0
            );
        } else if( ShapeFactory.isLine(shapeType) ){
            const line = new Line(
                params.originX, 
                params.originY, 
                params.originX,
                params.originY
            );
            //line.setStrokeStyle(strokeStyle);
            return line;
        } else if( ShapeFactory.isFreeform(shapeType) ){
            const points = [];
            const origin = {};
            origin.x = params.originX;
            origin.y = params.originY;
            points.push(origin);
            return new FreeForm(
                points
            );
        } else if( ShapeFactory.isPoint(shapeType) ){
            return new Point(
                params.originX, 
                params.originY
            );
        } else {
            console.log(shapeType + ": this Shape type is not implemented in the initialization factory");
        }
    }
    /*
     * Utility function to determine if param (string) is a circle
     */
    static isCircle (param) {
        if(param === "Circle" || param === "circle"){
            return true;
        }
        return false;
    }

    /*
     * Utility function to determine if param (string) is a line
     */
    static isLine (param) {
        if(param === "Line" || param === "straightLine"){
            return true;
        }
        return false;
    }

    /*
     * Utility function to determine if param (string) is a freeform
     */
    static isFreeform (param) {
        if(param === "FreeForm" || param === "freeForm"){
            return true;
        }
        return false;
    }

    /*
     * Utility function to determine if param (string) is a point
     */
    static isPoint (param) {
        if(param === "Point" || param === "point"){
            return true;
        }
        return false;
    }
}

module.exports = ShapeFactory;