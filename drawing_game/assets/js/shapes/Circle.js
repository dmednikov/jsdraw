var Shape = require('./Shape');

var calculateDistance = require('../utils/utils').calculateDistance;
//var utils = require('./utils');
//var isCollinear = require('./utils').isCollinear;

//var isPointOnLine = require('./utils').isPointOnLine;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Circle(centerX, centerY, toX, toY) {

    Shape.call(this); //call super constructor

    this.init = function (centerX, centerY, radius) {

        this.centerX = centerX;
        this.centerY = centerY;

        this.radius = radius;

        if(typeof Path2D === 'function'){
            this.path = new Path2D();
        } else {
            console.log("Path2D is not defined");
        }

        this.path.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI);
    };
    this.init(centerX, centerY, toX, toY);

    this.draw = function (context) {
        context.beginPath();
        context.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI);

        context.strokeStyle = '#000000';
        if(this.getStrokeStyle() !== null ){
           context.strokeStyle = this.getStrokeStyle(); 
        }
        
        context.stroke();
    };
    /*
     * Before any shape gets created this function is called to determine if the current shape is valid
     * i.e if length > 0 or radius > 0
     */
    this.isValid = function(){
        if (this.radius > 0){
            return true;
        }
        return false;
    }
    this.isPointOver = function (context, x, y) {
        // circle formula
        // (x - this.centerX)^2 + (y - this.centerY)^2 = this.radius^2; // if we add margin +- margin;
        const firstParam = Math.pow( (x - this.centerX), 2);
        const secondParam = Math.pow( (y - this.centerY), 2);
        const lhs = firstParam + secondParam; // left hand side
        const rhs = this.radius * this.radius; // right hand side
        //console.info( lhs + " -- " + rhs)
        const delta = Math.abs(lhs - rhs);
        const margin = 400;
        if (delta < margin){
            return true;
        }
        return false;
        //return context.isPointInStroke(this.path, x, y);
    };

    this.drawSelected = function (context) {

        dragHandles = [];
        //img.onload = function () {
        context.drawImage(imgDragHandle, this.centerX - 5, this.centerY - 5);
        var handle = {};
        handle.origin = {};
        handle.origin.x = this.originX;
        handle.origin.y = this.originY;
        dragHandles.push(handle);
        
        //draw handles on circumference
        for(var i=0; i < 4; i=i+0.5){
            var result = {};
            var angle = i * Math.PI;
            result.Y = Math.round( this.centerY + this.radius * Math.sin( angle ) );
            result.X = Math.round( this.centerX + this.radius * Math.cos( angle ) );

            context.drawImage(imgDragHandle, result.X - 5, result.Y - 5);
            handle = {};
            handle.origin = {};
            handle.origin.x = result.X;
            handle.origin.y = result.Y;
            dragHandles.push(handle);
        }
        
        
        context.beginPath();
        context.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI);

        context.strokeStyle = '#ff0000';
        context.stroke();
        context.strokeStyle = '#000000';
    };
    
    this.toJSON = function(){
        //calling parent class to get common properties like strokeStyle, UUID
        const obj = Circle.prototype.toJSON.call(this);
        obj.type = "Circle";
 
        obj.centerX = this.centerX;
        obj.centerY = this.centerY;

        obj.radius = this.radius;

        obj.strokeStyle = this.getStrokeStyle();

        return obj;
        
    };
    
    this.toString = function(){
        var numberOfDecimalPoints = 100;
        
        var retval = "<div class='panel-heading'>Circle</div>" +
                "<div class='panel-body'>"+
                "centerX: " + Math.round(this.centerX * numberOfDecimalPoints) / numberOfDecimalPoints +
                "<br />centerY: " + Math.round(this.centerY * numberOfDecimalPoints) / numberOfDecimalPoints +
                "<br />radius: " + Math.round(this.radius * numberOfDecimalPoints) / numberOfDecimalPoints +
                "</div>";
        
        return retval;
    };
    /*
     * This get s displayed in Property popup
     */
    this.toHTMLString = function(){
        var numberOfDecimalPoints = 100;
        //"X: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +
        var retval = "<div class='panel-body'>"+
                "<div class='panel panel-info'>" +
                    "<div class='panel-heading'>Origin</div>" +
                    "<div class='panel-body'>" +
                        "<div class='input-group input-group-sm'>" +
                            "<span class='input-group-addon' id='sizing-addon3'>X</span>" +
                            "<input name='centerX' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                            Math.round(this.centerX * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                        "</div>" +
                        "<div class='input-group input-group-sm'>" +
                            "<span class='input-group-addon' id='sizing-addon3'>Y</span>" +
                            "<input name='centerY' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                            Math.round(this.centerY * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
                
                "<div class='input-group input-group-sm'>" +
                    "<span class='input-group-addon' id='sizing-addon3'>Radius</span>" +
                    "<input name='radius' type='text' class='form-control' placeholder='radius' aria-describedby='sizing-addon3' value='" +
                    Math.round(this.radius * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                "</div>" +

                "<div class='input-group input-group-sm'>" +
                    "<span class='input-group-addon' id='sizing-addon3'>Stroke Style</span>" +
                    "<input name='strokeStyle' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                    this.getStrokeStyle() + "'>" +
                "</div>"

                "</div>";
        
        return retval;
    };

    this.update = function (newEndpointX, newEndpointY){
        this.radius = calculateDistance(this.centerX, this.centerY, newEndpointX, newEndpointY);
    };
}
Circle.prototype = Object.create(Shape.prototype, {
    constructor: {
        value: Circle
    }
});

module.exports = Circle;