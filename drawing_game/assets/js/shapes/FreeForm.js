var Shape = require('./Shape');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function FreeForm(points) {

    Shape.call(this); //call super constructor

    this.init = function (points) {

        this.points = points;
        if(typeof Path2D === 'function'){
            this.path = new Path2D();
        } else {
            console.log("Path2D is not defined");
        }
        for (var i = 0; i < this.points.length - 1; i++) {
            this.path.moveTo(this.points[i].x, this.points[i].y);
            this.path.lineTo(this.points[i + 1].x, this.points[i + 1].y);
        }
    };
    this.init(points);

    this.draw = function (context) {

        context.beginPath();
        for (var i = 0; i < this.points.length - 1; i++) {
            context.moveTo(this.points[i].x, this.points[i].y);
            context.lineTo(this.points[i + 1].x, this.points[i + 1].y);
        }
        context.strokeStyle = '#000000';
        if(this.getStrokeStyle() !== null ){
           context.strokeStyle = this.getStrokeStyle(); 
        }
        context.stroke();

    };
    /*
     * Before freeform gets created make sure that
     * - either there is more than 2 points
     * - or if there are 2 points only then they are not the same
     */
    this.isValid = function(){
        if (this.points.length > 2){
            return true;
        } else if(this.points.length === 2){
            if (Math.abs(this.points[0].x - this.points[1].x) > 0 || Math.abs(this.points[0].y - this.points[1].y) > 0){
                return true;
            }
        }
        return false;
    }
    this.isPointOver = function (context, x, y) {
        return context.isPointInStroke(this.path, x, y);
        //return context.isPointInPath(this.path, x, y);
    };

    this.drawSelected = function (context) {

        dragHandles = [];

        context.drawImage(imgDragHandle, this.points[0].x - 5, this.points[0].y - 5);
        var handle = {};
        handle.origin = {};
        handle.origin.x = this.originX;
        handle.origin.y = this.originY;
        dragHandles.push(handle);

        context.drawImage(imgDragHandle, this.points[this.points.length-1].x - 5, this.points[this.points.length-1].y - 5);
        handle = {};
        handle.origin = {};
        handle.origin.x = this.endX;
        handle.origin.y = this.endY;
        dragHandles.push(handle);

        context.beginPath();
        for (var i = 0; i < this.points.length - 1; i++) {
            context.moveTo(this.points[i].x, this.points[i].y);
            context.lineTo(this.points[i + 1].x, this.points[i + 1].y);
        }
        context.strokeStyle = '#ff0000';
        context.stroke();
        context.strokeStyle = '#000000';
    };
    
    this.toJSON = function(){

        //calling parent class to get common properties like strokeStyle, UUID
        const obj = FreeForm.prototype.toJSON.call(this);
        
        obj.type = "FreeForm";
 
        obj.points = this.points;

        obj.strokeStyle = this.getStrokeStyle();
        
        return obj;        
    };
    this.toString = function(){
        var numberOfDecimalPoints = 100;

        var retval = "<div class='panel-heading'>Freeform</div>" +
                "<div class='panel-body'>"+
                "#points: " + this.points.length  +
                "<br />ps[0]X: " + Math.round(this.points[0].x * numberOfDecimalPoints) / numberOfDecimalPoints +
                 "<br />ps[0]Y: " + Math.round(this.points[0].y * numberOfDecimalPoints) / numberOfDecimalPoints +
                "<br />ps[last]X: " + Math.round(this.points[this.points.length-1].x * numberOfDecimalPoints) / numberOfDecimalPoints +
                "<br />ps[last]Y: " + Math.round(this.points[this.points.length-1].y * numberOfDecimalPoints) / numberOfDecimalPoints +
                "</div>";
        
        return retval;
        
    };

    /*
     * This get s displayed in Property popup
     */
    this.toHTMLString = function(){
        var numberOfDecimalPoints = 100;
        //"X: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +
        var retval = "<input name='points' type='hidden' value='" + JSON.stringify(this.points) + "' />" +


                "<h3><span class='label label-default'>Need some mechanism to allow editing of " + this.points.length + " points in Freeform</span></h3> <br />" +
 
                
                "<div class='input-group input-group-sm'>" +
                    "<span class='input-group-addon' id='sizing-addon3'>Stroke Style</span>" +
                    "<input name='strokeStyle' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                    this.getStrokeStyle() + "'>" +
                "</div>"

                "</div>";
        
        return retval;
    };

    this.update = function (newEndpointX, newEndpointY){
        var newPoint = {};
        newPoint.x = newEndpointX;
        newPoint.y = newEndpointY;
        this.points.push(newPoint);
    };
}
//the second parameter to make instanceof work
FreeForm.prototype = Object.create(Shape.prototype, {
    constructor: {
        value: FreeForm
    }
});

module.exports = FreeForm;