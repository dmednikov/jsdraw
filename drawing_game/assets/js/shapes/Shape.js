'use strict';
const uuidV4 = require('uuid/v4');
/* 
 *
 */
function Shape() {
	const that = this;

    this.UUID = uuidV4();
	//https://www.w3schools.com/tags/ref_canvas.asp look at here for properties
	this.strokeStyle = "#000000";
	this.lineWidth = "1"; //shape line width, in pixels

    this.label = null;
    this.labelOrientation = "horizontal";
    this.modelScale = 1;

}


Shape.prototype.setStrokeStyle = function (color) {
    if(color){
        this.strokeStyle = color; 
    }
};
Shape.prototype.getStrokeStyle = function () {
    return this.strokeStyle || null;
};
/*
 * Used for creation of objects from JSON 
 */
Shape.prototype.setUUID = function (UUID) {
    if(UUID){
        this.UUID = UUID; 
    }
    
};
Shape.prototype.getUUID = function () {
    return this.UUID || null;
};

Shape.prototype.setLabel = function (label) {
    if(label !== null && typeof label !== undefined){
        this.label = label; 
    }
};
Shape.prototype.getLabel = function () {
    return this.label || null;
};

Shape.prototype.setModelScale = function (modelScale) {
        this.modelScale = modelScale; 
};
Shape.prototype.getModelScale = function () {
    return this.modelScale || 1;
};

Shape.prototype.setLabelOrientation = function (labelOrientation) {
        this.labelOrientation = labelOrientation; 
};
Shape.prototype.getLabelOrientation = function () {
    return this.labelOrientation || null;
};

Shape.prototype.draw = function (context) {
    console.log("draw is not implemented here");
};
Shape.prototype.isPointOver = function (ctx, x, y) {
    console.log("isPointOver is not implemented here");
};
Shape.prototype.drawSelected = function (x, y) {
    console.log("drawSelected is not implemented here");
};
Shape.prototype.toJSON = function (x, y) {
    const obj={};
    obj.strokeStyle = this.getStrokeStyle();
    obj.UUID = this.getUUID();
    obj.label = this.getLabel();
    return obj;
};
/*
 * Before any shape gets created this function is called to determine if the current shape is valid
 * i.e if length > 0 or radius > 0
 */
Shape.prototype.isValid = function(){
    return true;
};
/*
 * This get s displayed in Property popup
 */
Shape.prototype.toHTMLString = function (x, y) {
    console.log("toHTMLString is not implemented here");
};

/*
 * Updates the shape for drawing. Making assumptions that this is a needed
 * 'update' method. I mean the naming is pretty generic... 
 */
Shape.prototype.update = function (x, y) {
    console.log("update is not implemented here");
};

module.exports = Shape;

