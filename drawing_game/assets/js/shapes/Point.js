'use strict';
var Shape = require('./Shape');

/* 
 * Point class. 
 * Point2D that is cause there is no need for 3D in this fun little app
 */
function Point(startX, startY) {

    Shape.call(this); //call super constructor

    this.init = function (startX, startY) {
        this.X = startX;
        this.Y = startY;
        if(typeof Path2D === 'function'){
            this.path = new Path2D();
        } else {
            console.log("Path2D is not defined");
        }

        this.DrawnRectangleSize = 3; //in pixels; including one pixel for the point itself;
        this.DrawnRectangleSizeWhenSelected = 3; //in pixels; including one pixel for the point itself;
        // when mouse over the point how many pixels will be padded around the point
        // to determine if point is selected
        this.SensitivityPadding = 1;
        const area = this.SensitivityPadding * 2 + 1;//total square side length of the sensitive area around pixel
        this.path.rect(this.X - this.SensitivityPadding, this.Y - this.SensitivityPadding, area, area);
    };

    this.init(startX, startY);
    
    /*
     * http://stackoverflow.com/questions/7812514/drawing-a-dot-on-html5-canvas
     */
    this.draw = function (context,  lineWidth) {
        
        context.strokeStyle = '#000000';
        if(this.getStrokeStyle() !== null ){
           context.strokeStyle = this.getStrokeStyle(); 
        }

        context.beginPath();

        if(lineWidth){
           context.lineWidth = lineWidth;
        }

        const offset = Math.floor(this.DrawnRectangleSize/2);
        context.fillRect(this.X - offset, this.Y - offset, this.DrawnRectangleSize, this.DrawnRectangleSize);
        
        context.stroke();
    };

    this.isPointOver = function (context, x, y) {
        const deltaX = Math.abs(x - this.X);
        const deltaY = Math.abs(y - this.Y);
        const margin = 3;
        //console.info(deltaX + " - " + deltaY);
        if (deltaX < margin && deltaY < margin){
            return true;
        }
        return false;

        //return context.isPointInPath(this.path, x, y);
    };
    this.drawSelected = function (context) {

        context.beginPath();
        dragHandles = [];
        //img.onload = function () {
        context.drawImage(imgDragHandle, this.X - 4.5, this.Y - 4.5);
        var handle = {};
        handle.origin = {};
        handle.origin.x = this.originX;
        handle.origin.y = this.originY;
        dragHandles.push(handle);


        context.fillStyle = '#ff0000';

        const offset = Math.floor(this.DrawnRectangleSizeWhenSelected/2);
        context.fillRect(this.X - offset, this.Y - offset, this.DrawnRectangleSizeWhenSelected, this.DrawnRectangleSizeWhenSelected);
        
        context.fillStyle = '#000000';

    };
    
    this.toJSON = function(){

        //calling parent class to get common properties like strokeStyle, UUID
        const obj = Point.prototype.toJSON.call(this);
        
        obj.type = "Point";
 
        obj.X = this.X;
        obj.Y = this.Y;

        obj.strokeStyle = this.getStrokeStyle();
        
        return obj;        
    };
    
    this.toString = function(){
        var numberOfDecimalPoints = 100;

        var retval = "<div class='panel-heading'>Point</div>" +
                "<div class='panel-body'>"+
                "X: " + Math.round(this.X * numberOfDecimalPoints) / numberOfDecimalPoints +
                "<br />Y: " + Math.round(this.Y * numberOfDecimalPoints) / numberOfDecimalPoints +
                "</div>";
        
        return retval;
    };
    /*
     * This get s displayed in Property popup
     */
    this.toHTMLString = function(){
        var numberOfDecimalPoints = 100;
        //"X: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +
        var retval = "<div class='panel-body'>"+
                "<div class='panel panel-info'>" +
                    "<div class='panel-heading'>Origin</div>" +
                    "<div class='panel-body'>" +
                        "<div class='input-group input-group-sm'>" +
                            "<span class='input-group-addon' id='sizing-addon3'>X</span>" +
                            "<input name='X' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                            Math.round(this.X * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                        "</div>" +
                        "<div class='input-group input-group-sm'>" +
                            "<span class='input-group-addon' id='sizing-addon3'>Y</span>" +
                            "<input name='Y' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                            Math.round(this.Y * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
                
                "<div class='input-group input-group-sm'>" +
                    "<span class='input-group-addon' id='sizing-addon3'>Stroke Style</span>" +
                    "<input name='strokeStyle' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                    this.getStrokeStyle() + "'>" +
                "</div>" +
            "</div>";
        
        return retval;
    };

    this.update = function (newEndpointX, newEndpointY){
        this.X = newEndpointX;
        this.Y = newEndpointY;
    };
}
Point.prototype = Object.create(Shape.prototype, {
    constructor: {
        value: Point
    }
});

module.exports = Point;