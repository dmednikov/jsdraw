'use strict';
var Shape = require('./Shape');

//var utils = require('./utils');
var isCollinear = require('../utils/utils').isCollinear;
//var calculateDistance = require('./utils').calculateDistance;
//var isPointOnLine = require('./utils').isPointOnLine;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Line extends Shape {

    constructor(startX, startY, endX, endY){
        super();

        this.originX = startX;
        this.originY = startY;
        if(typeof Path2D === 'function'){
            this.path = new Path2D();
        } else {
            console.log("Path2D is not defined");
        }
        
        this.endX = endX;
        this.endY = endY;
        
        this.path.moveTo(this.originX, this.originY);
        this.path.lineTo(this.endX, this.endY);

    }
    /*
     * Before any shape gets created this function is called to determine if the current shape is valid
     * i.e if length > 0 or radius > 0
     */
    isValid (){
        if ( Math.abs(this.originX - this.endX) > 0 || Math.abs(this.originY - this.endY) > 0){
            return true;
        }
        return false;
    }
    /*
     * horizontal and vertical lines look faded on canvas
     * http://stackoverflow.com/questions/10373695/drawing-lines-in-canvas-but-the-last-ones-are-faded
     */
    draw (context, lineWidth) {
        
        context.strokeStyle = '#000000';

        if(this.getStrokeStyle() !== null ){
           context.strokeStyle = this.getStrokeStyle(); 
        }

        let yEnd1 = this.originY;
        let yEnd2 = this.endY;
        if(yEnd1 === yEnd2){ //horizontal line
            yEnd1 = Math.floor(yEnd1)+0.5;
            yEnd2 = yEnd1;
        }
        let xEnd1 = this.originX;
        let xEnd2 = this.endX;

        if(xEnd1 === xEnd2){ //horizontal line
            xEnd1 = Math.floor(xEnd1)+0.5;
            xEnd2 = xEnd1;
        }

        context.beginPath();

        if(lineWidth){
           context.lineWidth = lineWidth;
        }
        context.moveTo(xEnd1, yEnd1);
        context.lineTo(xEnd2, yEnd2);

        if(this.label !== null && typeof this.label !== undefined){
            if(this.getLabelOrientation() === "vertical"){
                /*
                context.transform.save();
                console.info(xEnd2 + " :: " +yEnd2);
                context.transform.translate(xEnd2, yEnd2);
                context.transform.rotate(-Math.PI/2);
                context.strokeText(this.label, 0, 3);
                context.transform.restore();
                */
                context.transform.save();
                context.font = "10px Arial, sans-serif";
                context.transform.zoomOnPoint(1, 1/this.getModelScale(), xEnd2 , yEnd2 );
                context.strokeStyle = 'black';
                context.lineWidth = 1;
                context.fillText(this.label, xEnd2 - 9, yEnd2 + 8); 
                context.transform.restore();
            } else {
                context.transform.save();
                context.font = "10px Arial, sans-serif";
                context.transform.zoomOnPoint(1/this.getModelScale(), 1, xEnd2, yEnd2);
                context.strokeStyle = 'black';
                context.lineWidth = 1;
                context.fillText(this.label, xEnd2, yEnd2 + 4);
                context.transform.restore();
            }
        }
        context.stroke();
    }

    isPointOver(context, x, y) {
        /*return context.isPointInStroke(this.path, x, y);*/
        
        var crossproduct = isCollinear(x, y, this.originX, this.originY, this.endX, this.endY);

        // crossproduct === 0 in the perfext vector world, here we need threshold
        const margin = 400;
        if 
        (
            (crossproduct < margin && crossproduct > -margin) && 
            (
                (this.originX <= x && x <= this.endX) || 
                (this.endX <= x && x <= this.originX)
            ) && 
            (
                (this.originY <= y && y <= this.endY) || 
                (this.endY <= y && y <= this.originY)
            )
        ) 
        {
            return true;
        }
        return false;
        
    }
    drawSelected (context) {

        context.beginPath();
        dragHandles = [];
        //img.onload = function () {
        context.drawImage(imgDragHandle, this.originX - 5, this.originY - 5);
        var handle = {};
        handle.origin = {};
        handle.origin.x = this.originX;
        handle.origin.y = this.originY;
        dragHandles.push(handle);

        context.drawImage(imgDragHandle, this.endX - 5, this.endY - 5);
        handle = {};
        handle.origin = {};
        handle.origin.x = this.endX;
        handle.origin.y = this.endY;
        dragHandles.push(handle);
        //};
        //selectedShapeIndex = i;

        //shapesPoc[i].trigger("mouseIsOver");
        context.moveTo(this.originX, this.originY);
        context.lineTo(this.endX, this.endY);
        context.strokeStyle = '#ff0000';
        context.stroke();
        context.strokeStyle = '#000000';
    }
    
    toJSON (){
        //calling parent class to get common properties like strokeStyle, UUID
        //const obj = Line.prototype.toJSON.call(this);
        const obj = super.toJSON();
        
        obj.type = "Line";
 
        obj.originX = this.originX;
        obj.originY = this.originY;

        obj.endX = this.endX;
        obj.endY = this.endY;
        
        return obj;        
    }
    
    toString (){
        var numberOfDecimalPoints = 100;

        var retval = "<div class='panel-heading'>Line</div>" +
                "<div class='panel-body'>"+
                "origX: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +
                "<br />origY: " + Math.round(this.originY * numberOfDecimalPoints) / numberOfDecimalPoints +
                "<br />endX: " + Math.round(this.endX * numberOfDecimalPoints) / numberOfDecimalPoints +
                "<br />endY: " + Math.round(this.endY * numberOfDecimalPoints) / numberOfDecimalPoints +
                "</div>";
        
        return retval;
    }
    /*
     * This get s displayed in Property popup
     */
    toHTMLString (){
        var numberOfDecimalPoints = 100;
        //"X: " + Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints +
        var retval = "<div class='panel-body'>"+
                "<div class='panel panel-info'>" +
                    "<div class='panel-heading'>Origin</div>" +
                    "<div class='panel-body'>" +
                        "<div class='input-group input-group-sm'>" +
                            "<span class='input-group-addon' id='sizing-addon3'>X</span>" +
                            "<input name='originX' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                            Math.round(this.originX * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                        "</div>" +
                        "<div class='input-group input-group-sm'>" +
                            "<span class='input-group-addon' id='sizing-addon3'>Y</span>" +
                            "<input name='originY' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                            Math.round(this.originY * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                        "</div>" +
                    "</div>" +
                "</div>" +



                "<div class='panel panel-info'>" +
                    "<div class='panel-heading'>End</div>" +
                    "<div class='panel-body'>" +
                        "<div class='input-group input-group-sm'>" +
                            "<span class='input-group-addon' id='sizing-addon3'>X</span>" +
                            "<input name='endX' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                            Math.round(this.endX * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                        "</div>" +
                        "<div class='input-group input-group-sm'>" +
                            "<span class='input-group-addon' id='sizing-addon3'>Y</span>" +
                            "<input name='endY' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                            Math.round(this.endY * numberOfDecimalPoints) / numberOfDecimalPoints + "'>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
                
                "<div class='input-group input-group-sm'>" +
                    "<span class='input-group-addon' id='sizing-addon3'>Stroke Style</span>" +
                    "<input name='strokeStyle' type='text' class='form-control' placeholder='origin x' aria-describedby='sizing-addon3' value='" +
                    this.getStrokeStyle() + "'>" +
                "</div>" +
            "</div>";
        
        return retval;
    }

    /*
     * When shape is being drawn some sort of parameter changes
     * in this case end of the line. So the to be drawn the end of the line needs to be updated
     */
    update (newEndpointX, newEndpointY){
        this.endX = newEndpointX;
        this.endY = newEndpointY;
    }
}
/*
Line.prototype = Object.create(Shape.prototype, {
    constructor: {
        value: Line
    }
});
*/
//Object.setPrototypeOf(Line.prototype, Shape);// If you do not do this you will get a TypeError when you invoke speak

module.exports = Line;