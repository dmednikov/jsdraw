'use strict';

var HorizontalRulerModel = require('../models/RulerModel/HorizontalRulerModel');
var VerticalRulerModel = require('../models/RulerModel/VerticalRulerModel');
var GridModel = require('../models/GridModel');
var DrawingToolbarModel = require('../models/DrawingToolbarModel');

var InfoBlock = require('../info_block/InfoBlock');
var BasicModel = require('../models/BasicModel');
var ShapeStoreArray = require('../storage/shapes/ShapeStoreArray'); // needs to be injected into BasicModel

var HorizontalRulerGuideModel = require('../models/RulerGuide/HorizontalRulerGuideModel');
var VerticalalRulerGuideModel = require('../models/RulerGuide/VerticalRulerGuideModel');
var CurrentDrawingModel = require('../models/CurrentDrawingModel');
var SelectedShapeModel = require('../models/SelectedShapeModel');

var CanvasBorderModel = require('../models/CanvasBorderModel');

var Layers = require('../layers/LayerAPI');

var Transform = require('../matrix/Transform');

class InitializationFactory {
    /*
    param: {} options
        options.canvas = canvas;
        options.socket = socket;
        options.pubsub = pubsub;
        ...
        options.context = context;
    */
    constructor(options) {

        //let originalContext = options.canvas[0].getContext('2d');
        //originalContext.transform = new Transform(originalContext);
        const originalCanvas = options.canvas; // need to preserve the original

        /*
        options.canvas = this.createCanvas(originalCanvas, "current_drawing_canvas");
        options.context = this.createCanvasContext(options.canvas);
        new CurrentDrawingModel(options);
        */
        new CurrentDrawingModel(this.prepModelOptions(options, originalCanvas, "current_drawing_canvas"));

        options.shapesStorage = new ShapeStoreArray();

        new SelectedShapeModel(this.prepModelOptions(options, originalCanvas, "selected_shape_canvas")); //
        //t.markSelectedShapeIfMouseOverIt({});
        /*
        options.canvas = this.createCanvas(originalCanvas, "basic_canvas");
        options.context = this.createCanvasContext(options.canvas);
        const basicModel = new BasicModel(options); // need to inject it into InfoBlock
        */
        const basicModel = new BasicModel(this.prepModelOptions(options, originalCanvas, "basic_canvas")); //need to inject it into InfoBlock therefore creating variable
        //shapeStorage is only needed for BasicModel so can set it to null at this point
        options.shapesStorage = null;
        //draws little info block about selected shape
        //options.shapesStorage = null;
        //options.canvas = null;
        //options.context = null;
        options.basicModel = basicModel;
        new InfoBlock(options);

        //options.canvas = $(Layers.create(originalCanvas, "GridLayer", 5));
        /*
        options.canvas = this.createCanvas(originalCanvas, "grid_canvas", 5);
        options.context = this.createCanvasContext(options.canvas);
        new GridModel(options);
        */
        new GridModel(this.prepModelOptions(options, originalCanvas, "grid_canvas", 5));

        new DrawingToolbarModel(); //draws toolbar and handles its events
        /*
        options.canvas = this.createCanvas(originalCanvas, "ruler_guide_canvas_horizontal", 5);
        options.context = this.createCanvasContext(options.canvas);
        new HorizontalRulerGuideModel(options); //draws ruler guide
        */
        new HorizontalRulerGuideModel(this.prepModelOptions(options, originalCanvas, "ruler_guide_canvas_horizontal", 5)); //draws ruler guide
        /*
        options.canvas = this.createCanvas(originalCanvas, "ruler_guide_canvas_vertical", 5);
        options.context = this.createCanvasContext(options.canvas);
        new VerticalalRulerGuideModel(options); //draws ruler guide
        */
        new VerticalalRulerGuideModel(this.prepModelOptions(options, originalCanvas, "ruler_guide_canvas_vertical", 5)); //draws ruler guide
        // Following canvases need to be offset
        /*
        options.canvas = this.createOffsetCanvas(originalCanvas, "ruler_canvas_horizontal");
        options.context = this.createCanvasContext(options.canvas);
        new HorizontalRulerModel(options); //draws ruler
        */
        new HorizontalRulerModel(this.prepOffsetModelOptions(options, originalCanvas, "ruler_canvas_horizontal")); //draws ruler
        /*
        options.canvas = this.createOffsetCanvas(originalCanvas, "ruler_canvas_vertical");
        options.context = this.createCanvasContext(options.canvas);
        new VerticalRulerModel(options); //draws ruler
        */
        new VerticalRulerModel(this.prepOffsetModelOptions(options, originalCanvas, "ruler_canvas_vertical")); //draws ruler
        /*
        options.canvas = this.createOffsetCanvas(originalCanvas, "border_canvas");
        options.context = this.createCanvasContext(options.canvas);
        new CanvasBorderModel(options); //draws bounding box
        */
        new CanvasBorderModel(this.prepOffsetModelOptions(options, originalCanvas, "border_canvas")); //draws bounding box
    }

    prepModelOptions(options, canvas, canvasName, zIndex){
        if(zIndex){
            options.canvas = this.createCanvas(canvas, canvasName, zIndex);
        } else {
            options.canvas = this.createCanvas(canvas, canvasName);
        }
        options.context = this.createCanvasContext(options.canvas);
        return options;
    }
    prepOffsetModelOptions(options, canvas, canvasName){
        options.canvas = this.createOffsetCanvas(canvas, canvasName);
        options.context = this.createCanvasContext(options.canvas);
        return options;
    }
    createCanvas(originalCanvas, id, zIndex){
        const newCanvas =  $(Layers.create(originalCanvas, id, zIndex));
        return newCanvas;
    }
    createOffsetCanvas (originalCanvas, id) {
        const newCanvas = jQuery(Layers.oneOf(originalCanvas, id));
        return newCanvas;
    }
    createCanvasContext(canvas){
        let context = canvas[0].getContext('2d');
        context.transform = new Transform(context);
        return context;
    }
}

module.exports = InitializationFactory;