var ShapeFactory = require('../shapes/ShapeFactory');
/* 
 * This is a model supports information (block) display
 * for a currently selected shape
 */

function InfoBlock(options) {
    
    /*
     * @param {type} canvas
     * @returns {undefined}canvas - jQuery object
     */
    var that = this;
    this.init = function (options) {

        this.basicModel = options.basicModel;

        this.pubsub = options.pubsub;

        //$.subscribe('client_zooming', function( event, obj ) {
            //that.handleZoomEvent.call(that, obj);
        //});
        if(this.pubsub){
            this.pubsub.on('client_zooming', function(obj) {
                that.handleZoomEvent.call(that, obj);
            });
        }

        if(this.pubsub){
            this.pubsub.on('users_updated', function(obj) {
                that.handleUsersUpdated.call(that, obj);
            });
        }

        /*$.subscribe('socket_zooming', function( event, obj ) {
            that.handleZoomEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('socket_zooming', function(obj) {
                that.handleZoomEvent.call(that, obj);
            });
        }
        /*$.subscribe('client_panning', function( event, obj ) {
            that.handlePanEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('client_panning', function(obj) {
                that.handlePanEvent.call(that, obj);
            });
        }

        /*$.subscribe('socket_panning', function( event, obj ) {
            that.handlePanEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('socket_panning', function(obj) {
                that.handlePanEvent.call(that, obj);
            });
        }
        /*$.subscribe('mouse_move', function( event, obj ) {
            that.handleMouseMoveEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('mouse_move', function(obj) {
                that.handleMouseMoveEvent.call(that, obj);
            });
        }
        $.subscribe('shape_selected', function( event, obj ) {
            that.handleShapeSelectedEvent.call(that, obj);
        });
        $.subscribe('debug_show_shapes', function( event ) {
            console.info(that.basicModel.getShapesRawForDebugging());
        });
        $.subscribe('properties_form_submission', function( event, obj ) {
            
            //jQuery("#propertiesForm").hide();
            jQuery("#myModal").remove();

            var form_data = obj.split('&');
            var input     = {};

            $.each(form_data, function(key, value) {
                var data = value.split('=');
                if(data[0] === "points"){
                    input[data[0]] = JSON.parse(decodeURIComponent(data[1]));
                    return true; // it is not 'continue' because we are in 'each' loop not regular 'for loop'
                }
                input[data[0]] = decodeURIComponent(data[1]);
            });
            let newShape = null;
            try{
                newShape = ShapeFactory.createFromJSON(input);
            } catch(ex){
                //should may be popup user message about an attempt to create invalid shape ...
            }
            
            if (newShape !== null && newShape.isValid()){
                that.basicModel.updateShape(input.selectedIndex, newShape);

                that.basicModel.emitShapeUpdated(input.selectedIndex);
            }
        });

        $.subscribe('show_properties_dialog', function( event, index ) {

            const shape = that.basicModel.getShape(index);
            //default, if not hsape is selected popup informative message
            let html = '<!-- Modal --> ' +
                    '<div id="myModal" class="modal fade" role="dialog">' +
                      '<div class="modal-dialog">' +

                        '<!-- Modal content-->' +
                        '<div class="modal-content">' +
                          '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                            '<h4 class="modal-title">No shape selected!</h4>' +
                          '</div>' +
                          '<div class="modal-body">' +
                            '<form id="propertiesForm">' +
                            '<span class="input-group-addon" id="sizing-addon3">Please select a shape</span>' +
                            '</form>' +
                          '</div>' +
                          '<div class="modal-footer">'+
                            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                          '</div>' +
                        '</div>' +

                      '</div>' +
                    '</div>';
            //if there is a selected shape, overwrite default then
            if (shape != null ){
                html = '<!-- Modal --> ' +
                    '<div id="myModal" class="modal fade" role="dialog">' +
                      '<div class="modal-dialog">' +

                        '<!-- Modal content-->' +
                        '<div class="modal-content">' +
                          '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                            '<h4 class="modal-title">' + shape.constructor.name + '</h4>' +
                          '</div>' +
                          '<div class="modal-body">' +
                            '<form id="propertiesForm">'+ shape.toHTMLString() +
                            '<input name="type" type="hidden" value="' + shape.constructor.name + '"/>' +
                            '<input name="selectedIndex" type="hidden" value="' + index + '"/>' +
                            '</form>' +
                          '</div>' +
                          '<div class="modal-footer">' +
                            '<button type="button" class="btn btn-default" onClick="$.publish(\'properties_form_submission\', $(\'#propertiesForm\').serialize())">Submit</button>'+
                            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                          '</div>' +
                        '</div>' +

                      '</div>' +
                    '</div>';
            }
            
            //$("#myModal").modal();
            var modal = $(html);
            modal.modal({backdrop: false});
        });
        
    };

    this.init(options);
    
    this.handleUsersUpdated = function (obj) {

        const users = obj.users.filter( function(item){
            return item.uuid !== obj.selfUUID;
        })
        $("#number_of_room_visitors").html((users.length +1));

    };

    this.handleZoomEvent = function (obj) {
        $("#zoom_value").html(obj.zoomLevel);
    };
    
    this.handlePanEvent = function (obj) {
        $("#pan_x").html(obj.panDisplacement.x);        
        $("#pan_y").html(obj.panDisplacement.y);
    };
    this.handleMouseMoveEvent = function (obj) {

        $("#mouse_x").html(obj.mouseCoordsOverCanvas.x);        
        $("#mouse_y").html(obj.mouseCoordsOverCanvas.y); 

        $("#mouse_adjusted_x").html(obj.mouseCoordsOverCanvasAdjusted.x);        
        $("#mouse_adjusted_y").html(obj.mouseCoordsOverCanvasAdjusted.y); 
    };
    /*
     * param obj : 
     */
    this.handleShapeSelectedEvent = function (obj) {

        const inputGroupStart = '<div class="panel panel-default" style="margin:4px;">';
        const inputGroupEnd = '</div>';
        //const button = '<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Properties</button>';
        const button = '<button type="button" class="btn btn-primary btn-sm" onClick="$.publish(\'show_properties_dialog\', '+ obj.selectedShapeIndex +')" >Properties</button>';

        $("#shape_properties").html(inputGroupStart + that.basicModel.getShape(obj.selectedShapeIndex).toString() + "<br />" + button + inputGroupEnd);
    };
    
}

module.exports = InfoBlock;