'use strict';
var ShapeStore = require('./ShapeStore');

/*
 * Implementation of the ShapeStore that urelies on [] for storage
 */
function ShapeStoreArray() {

    ShapeStore.call(this); //call super constructor

    this.init = function () {
        this.shapeStore = [];
    };

    this.init();
    
    this.add = function(obj){
        this.shapeStore.push(obj);
    };

    this.get = function(index){
        return this.shapeStore[index];
    };

    this.delete = function(index){
        this.shapeStore.splice(index, 1);
    };
    /*
     * VEry specific to Arrays function 
     */
    this.updateByIndex = function(index, newShape){
        this.shapeStore[index] = newShape;
    };

    this.size = function(){
        return this.shapeStore.length;
    };

    this.returnRawData = function(){
        return this.shapeStore;
    };

}


ShapeStoreArray.prototype = Object.create(ShapeStore.prototype, {
    constructor: {
        value: ShapeStoreArray
    }
});

module.exports = ShapeStoreArray;