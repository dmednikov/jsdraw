'use strict';
/*
 * Parent class abstraction for Shape storage.
 * The idea is that referring to shapes in an array by index might be a little too brittle
 * It might be a mre consistent approach to refer to shape by UUID
 * and thus an associative array (object) a Map might be needed
 */

function ShapeStore() {

}

ShapeStore.prototype.add = function (obj) {
    console.log("add is not implemented here");
    
};

ShapeStore.prototype.get = function () {
    console.log("get is not implemented here");
};

ShapeStore.prototype.delete = function () {
    console.log("delete is not implemented here");
};

ShapeStore.prototype.size = function () {
    console.log("size is not implemented here");
};

/*
 * Useful for debugging
 */
ShapeStore.prototype.returnRawData = function () {
    console.log("size is not implemented here");
};

module.exports = ShapeStore;

