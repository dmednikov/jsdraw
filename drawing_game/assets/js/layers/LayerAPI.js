//Should it be a Singleton perhaps?

// interesting discussion:
// http://stackoverflow.com/questions/3680429/click-through-a-div-to-underlying-elements
// there is an option there 
/*
     $('#elementontop).click(function (e) {
        $('#elementontop).hide();
        $(document.elementFromPoint(e.clientX, e.clientY)).trigger("click");
        $('#elementontop').show();
    });
 */
class LayerAPI {
    static create (originalCanvas, id, zIndex) {
        var originalCanvasOffset = originalCanvas.offset();
        var canvas = document.createElement('canvas');
        canvas.id     = id;
        canvas.style.top =  originalCanvasOffset.top + "px";
        canvas.style.left =  originalCanvasOffset.left + "px";
        
        canvas.style.pointerEvents = "none";

        canvas.width  = originalCanvas.width();
        canvas.height = originalCanvas.height();
        canvas.style.zIndex   = 8;
        if(zIndex){
            canvas.style.zIndex   = zIndex;
        }
        canvas.style.position = "absolute";
        canvas.style.border   = "none";
        document.body.appendChild(canvas);
        return canvas;
    }

    static oneOf (originalCanvas, id) {
        if(originalCanvas){
            var offset = 20;
            var originalCanvasOffset = originalCanvas.offset();
            var canvas = document.createElement('canvas');
            canvas.id     = id;
            canvas.customOffset = offset; // used when the cnavas is re-drawn TODO: look into why this is needed.
            canvas.style.top =  (originalCanvasOffset.top - offset) + "px";
            canvas.style.left =  (originalCanvasOffset.left - offset) + "px";
            
            canvas.style.pointerEvents = "none";

            canvas.width  = originalCanvas.width() + offset;
            canvas.height = originalCanvas.height() + offset;
            canvas.style.zIndex   = 9;
            canvas.style.position = "absolute";
            canvas.style.border   = "none";
            document.body.appendChild(canvas);
            return canvas;
        }
        return null;
    }
}

module.exports = LayerAPI;