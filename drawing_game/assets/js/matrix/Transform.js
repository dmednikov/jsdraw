/*
 * Transform tracker
 *
 * @author Kevin Moot <kevin.moot@gmail.com>
 * Based on a class created by Simon Sarris - www.simonsarris.com - sarris@acm.org
 * 
 * https://github.com/simonsarris/Canvas-tutorials/blob/master/transform.js
 * 
 * It uses setTransform() HTML Canvas function to reset the current transformation to the identety matrix
 * After every translate, scale ... setTransform is called which means that all consequtive transformation
 * will only affect drawings made after the setTransform method is called. Which allows for zoom to simply
 * go up. 1.1 ... 1.2 ... 1.3 and then ... 1.2 willa ctually scale down. In usual context (without this transform 
 * library) going to ... 1.2 will actually still scale up  by a factor of 1.2 instead of scaling down by 0.1 
 */

var Decimal = require('decimal.js');

function Transform(context) {

    // Set the precision and rounding of the default Decimal constructor
    Decimal.set({ precision: 10, rounding: 6 })
    
    this.context = context;
    this.matrix = [new Decimal(1), new Decimal(0), new Decimal(0), new Decimal(1), new Decimal(0), new Decimal(0)]; //initialize with the identity matrix
    this.stack = [];
    
    //==========================================
    // Constructor, getter/setter
    //==========================================    
    
    this.setContext = function(context) {
        this.context = context;
    };

    this.getMatrix = function() {

        const clone = [];
        for(var i=0; i<this.matrix.length; i++){
            clone.push(this.matrix[i].toNumber());
        }
        return clone;
    };

    this.getXTranslation = function() {
        return this.matrix[4].toNumber();
    };

    this.getYTranslation = function() {
        return this.matrix[5].toNumber();
    };

    this.getScaleX = function() {
        return this.matrix[0].toNumber();
    };

    this.getScaleY = function() {
        return this.matrix[3].toNumber();
    };

    
    this.setMatrix = function(m) {
        //this.matrix = [m[0],m[1],m[2],m[3],m[4],m[5]];
        this.matrix = [new Decimal(m[0]), new Decimal(m[1]), new Decimal(m[2]), new Decimal(m[3]), new Decimal(m[4]), new Decimal(m[5])];
        this.setTransform();
    };
    
    this.cloneMatrix = function(m) {
        return [m[0],m[1],m[2],m[3],m[4],m[5]];
    };
    
    //==========================================
    // Stack
    //==========================================
    
    this.save = function() {
        var matrix = this.cloneMatrix(this.getMatrix());
        this.stack.push(matrix);
        
        if (this.context) this.context.save();
    };

    this.restore = function() {
        if (this.stack.length > 0) {
            var matrix = this.stack.pop();
            this.setMatrix(matrix);
        }
        
        if (this.context) this.context.restore();
    };

    //==========================================
    // Matrix
    //==========================================

    this.setTransform = function() {
        if (this.context) {
            this.context.setTransform(
                this.matrix[0], //Horizontal scaling
                this.matrix[1], //Horizontal skewing
                this.matrix[2], //Vertical skewing
                this.matrix[3], //Vertical scaling
                this.matrix[4], //Horizontal moving
                this.matrix[5]  //Vertical moving
            );
        }
    };
    
    /*
    this.translate = function(x, y) {
        this.matrix[4] += this.matrix[0] * x + this.matrix[2] * y;
        this.matrix[5] += this.matrix[1] * x + this.matrix[3] * y;
        
        this.setTransform();
    };
    */
    this.translateDelta = function(x, y) {
        this.matrix[4] = this.matrix[4].plus(this.matrix[0].times(x).plus(this.matrix[2].times(y)));
        this.matrix[5] = this.matrix[5].plus(this.matrix[1].times(x).plus(this.matrix[3].times(y)));
        
        this.setTransform();
    };
    this.translateDeltaLight = function(x, y) {
        //console.info(                              this.matrix[0].toNumber()  );
        //console.info(                              this.matrix[2].times(y).toNumber()  );
        //console.info( this.matrix[0].times(x).plus(this.matrix[2].times(y)).toNumber() );
        this.matrix[4] = this.matrix[4].plus(this.matrix[0].times(x).plus(this.matrix[2].times(y)));
        this.matrix[5] = this.matrix[5].plus(this.matrix[1].times(x).plus(this.matrix[3].times(y)));
    };

    this.translate = function(x, y) {
        this.matrix[4] = new Decimal(x);
        this.matrix[5] = new Decimal(y);
        
        this.setTransform();
    };
    this.translateLight = function(x, y) {
        this.matrix[4] = new Decimal(x);
        this.matrix[5] = new Decimal(y);
    };
    this.rotate = function(rad) {
        var c = Math.cos(rad);
        var s = Math.sin(rad);
        var m11 = this.matrix[0] * c + this.matrix[2] * s;
        var m12 = this.matrix[1] * c + this.matrix[3] * s;
        var m21 = this.matrix[0] * -s + this.matrix[2] * c;
        var m22 = this.matrix[1] * -s + this.matrix[3] * c;
        this.matrix[0] = m11;
        this.matrix[1] = m12;
        this.matrix[2] = m21;
        this.matrix[3] = m22;
        
        this.setTransform();
    };
    
    this.scale = function(sx, sy) {
        this.matrix[0] = this.matrix[0].times(sx);
        this.matrix[1] = this.matrix[1].times(sx);
        this.matrix[2] = this.matrix[2].times(sy);
        this.matrix[3] = this.matrix[3].times(sy);
        
        this.setTransform();
    };

    this.setScale = function(x, y) {
        this.matrix[0] = new Decimal(x);
        this.matrix[1] = new Decimal(x);
        this.matrix[2] = new Decimal(y);
        this.matrix[3] = new Decimal(y);
        
        this.setTransform();
    };

    this.scaleLight = function(sx, sy) {
        this.matrix[0] = this.matrix[0].times(sx);
        this.matrix[1] = this.matrix[1].times(sx);
        this.matrix[2] = this.matrix[2].times(sy);
        this.matrix[3] = this.matrix[3].times(sy);
        
        //this.setTransform();
    };
    /* Assuming uniform scaling 
     *
     */
    this.getScaleFactor = function(){
        return this.matrix[0];
    }
    /*
    this.scale = function(sx, sy) {
        this.matrix[0] = sx;
        this.matrix[3] = sy;
        
        this.setTransform();
        //this.context.translate(x, y);
    };*/

    this.zoomOnPoint = function(sx, sy, x, y) {

        //console.info(sx + " :: " + sy);
        //console.info(x + " :: " + y);
        sx = new Decimal(sx);
        sy = new Decimal(sy);

        x = new Decimal(x);
        y = new Decimal(y);

        //this.matrix[4] = new Decimal(x);
        //this.matrix[5] = new Decimal(y);
        
        //move coordinate system origin to the x,y (mouse position over the canvas)
        //console.info(this.matrix[4].toNumber() + " <-4 0 5-> " + this.matrix[5].toNumber());
 
        //console.info(x + " - " + y);
        this.translateDeltaLight(x, y);
        //console.info(this.matrix[4].toNumber() + " <-4 1 5-> " + this.matrix[5].toNumber());
        //scale 
        this.scaleLight(sx, sy);
        //const scaleFactor = this.getScaleFactor();
        //
       //console.info("scale :" +scaleFactor);
        //console.info("Back  X:" + x.minus(x.times(scaleFactor)).toNumber());
        // move the coordinate system back to where it was
        //this.translateDeltaLight(x.minus(x.times(scaleFactor)), y.minus(y.times(scaleFactor)));
        //originx -= mousex/(scale*zoom) - mousex/scale;

        //originx = originx - originx/zoom -originx

        this.translateDeltaLight(-x, -y);
        //console.info(this.matrix[4].toNumber() + " <-4 2 5-> " + this.matrix[5].toNumber());

        //this.restore();
        
        this.setTransform();

        //console.info(this.getMatrix());
    };
    
    //==========================================
    // Matrix extensions
    //==========================================

    this.rotateDegrees = function(deg) {
        var rad = deg * Math.PI / 180;
        this.rotate(rad);
    };

    this.rotateAbout = function(rad, x, y) {
        this.translate(x, y);
        this.rotate(rad);
        this.translate(-x, -y);
        this.setTransform();
    }

    this.rotateDegreesAbout = function(deg, x, y) {
        this.translate(x, y);
        this.rotateDegrees(deg);
        this.translate(-x, -y);
        this.setTransform();
    }
    
    this.identity = function() {
        this.m = [1,0,0,1,0,0];
        this.setTransform();
    };

    this.multiply = function(matrix) {
        var m11 = this.matrix[0] * matrix.m[0] + this.matrix[2] * matrix.m[1];
        var m12 = this.matrix[1] * matrix.m[0] + this.matrix[3] * matrix.m[1];

        var m21 = this.matrix[0] * matrix.m[2] + this.matrix[2] * matrix.m[3];
        var m22 = this.matrix[1] * matrix.m[2] + this.matrix[3] * matrix.m[3];

        var dx = this.matrix[0] * matrix.m[4] + this.matrix[2] * matrix.m[5] + this.matrix[4];
        var dy = this.matrix[1] * matrix.m[4] + this.matrix[3] * matrix.m[5] + this.matrix[5];

        this.matrix[0] = m11;
        this.matrix[1] = m12;
        this.matrix[2] = m21;
        this.matrix[3] = m22;
        this.matrix[4] = dx;
        this.matrix[5] = dy;
        this.setTransform();
    };

    this.invert = function() {
        var d = 1 / (this.matrix[0] * this.matrix[3] - this.matrix[1] * this.matrix[2]);
        var m0 = this.matrix[3] * d;
        var m1 = -this.matrix[1] * d;
        var m2 = -this.matrix[2] * d;
        var m3 = this.matrix[0] * d;
        var m4 = d * (this.matrix[2] * this.matrix[5] - this.matrix[3] * this.matrix[4]);
        var m5 = d * (this.matrix[1] * this.matrix[4] - this.matrix[0] * this.matrix[5]);
        this.matrix[0] = m0;
        this.matrix[1] = m1;
        this.matrix[2] = m2;
        this.matrix[3] = m3;
        this.matrix[4] = m4;
        this.matrix[5] = m5;
        this.setTransform();
    };
    
     //==========================================
    // Helpers
    //==========================================

    this.transformPoint = function(x, y) {
        return {
            x: x * this.matrix[0] + y * this.matrix[2] + this.matrix[4], 
            y: x * this.matrix[1] + y * this.matrix[3] + this.matrix[5]
        };
    };
}

module.exports = Transform;