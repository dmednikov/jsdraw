'use strict';
//https://socket.io/docs/emit-cheatsheet/
class UserEventManager {
    constructor(userEventManagerParams) {
        const socket = userEventManagerParams.socket;
        const usersService = userEventManagerParams.usersService;
        //const usersModel = userEventManagerParams.usersModel;
        const io = userEventManagerParams.io;

        socket.on('join', function(data) {
            const room_name = usersService.getRoomName(data);
            usersService.addUser(data, socket.id);
            socket.uuid = data.uuid;
            socket.room_name = room_name;
            socket.join(room_name);
            const allRoomUsers = usersService.getAllUsersInRoom(room_name);
            //sending to all clients in 'room_name' room, including sender
            io.in(room_name).emit('all-users', allRoomUsers);
        });

        /*
        socket.on('get-users', function(data) {
            const room_name = usersService.getRoomName(data);
            const allRoomUsers = usersModel.getAllUsersInRoom(room_name);
            //sending to all clients in 'room_name' room, including sender
            io.in(room_name).emit('all-users', allRoomUsers);
        });
        */

        // apparently there is no "data" in "disconnect" event
        socket.on('disconnect', function() {
            usersService.removeUser(socket.uuid);
            //
            const room_name = usersService.getRoomName(socket);
            const allRoomUsers = usersService.getAllUsersInRoom(room_name);
            //sending to all clients in 'room_name' room
            io.in(room_name).emit('all-users', allRoomUsers);
        });
    }
}
module.exports = UserEventManager;