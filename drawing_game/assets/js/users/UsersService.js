'use strict';
var staticVars = require('../utils/static_vars.js');
class UsersService {
    constructor(usersModel) {
        const self = this;
        this.usersModel = usersModel;
    }
    /*
     *
     */
    addUser(data, socketId) {
        const room_name = this.getRoomName(data);
        console.info("Adding socket and user: uuid " + data.uuid + ' room name: ' + room_name);
        const userObject = {
            uuid: data.uuid,
            socketid: socketId,
            room_name: room_name
        };
        this.usersModel.addUser(userObject);
    }
    /*
     *
     */
    removeUser(uuid) {
        //console.info("Removing socket and user: uuid " + uuid + ' room name: ' + room_name);
        this.usersModel.removeUser(uuid);
    }
    /*
     *
     */
    getAllUsersInRoom(room_name) {   
        return this.usersModel.getAllUsersInRoom(room_name);
    }
    /*
     * param data object with "room_name field set"
     */
    getRoomName(data) {
        const largestCities = staticVars.getLargestCities();
        const needle = decodeURIComponent(data.room_name);
        let index = 0,
            len = largestCities.length;
        for (; index < len; index++) {
            if (largestCities[index][0] === needle) {
                break;
            }
        }
        let room_name = 'default'; // room name some funky attempt to join invalid room, join 'default' instead
        if (index !== len) {
            // then the room_name was found and valid join it
            room_name = needle;
        }
        return room_name;
    }
}
module.exports = UsersService;