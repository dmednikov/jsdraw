'use strict';
var staticVars = require('../utils/static_vars.js');
class UsersModel {
    constructor() {
        const self = this;
        this.users = [];
    }
    addUser(user) {
        this.users.push(user);
    }
    removeUser(uuid) {
        this.users = this.users.filter(function(item) {
            return (item.uuid !== uuid);
        });
    }
    getAllUsersInRoom(room_name) {
        const allUsersInRoom = this.users.filter(function(item) {
            return (item.room_name === room_name);
        });
        return allUsersInRoom;
    }
}
module.exports = UsersModel;