var Line = require('../../shapes/Line');
var RulerGuide = require('./RulerGuide');

/* 
 * Ruler Guide which runs horizontally from the left vertical ruler to mouse pointer
 * (to the right edge of canvas, optionally)
 */
function HorizontalRulerGuideModel(options) {

    RulerGuide.call(this, options); //call the super constructor

    this.init = function () {
    };

    this.init();
    
    /*
     * 
     */
    this.render = function (context, obj) {
        this.clearCanvas();
        //seeems like not necessarily the right place for this calculation
        if(obj && obj.mouseCoordsOverCanvas){
            const y = obj.mouseCoordsOverCanvas.y;
            var vLine = new Line(0, y, this.canvasWidth, y);
            vLine.draw(this.context);
        }
    };

    /*
     * Override parent's object default behaviour
     */
    this.handleZoomEvent = function (obj) {
        //need to override the method not to zoom in this context
    };
    /*
     * Override parent's object default behaviour
     */
    this.handlePanEvent = function (obj) {

    };
    /*
     * Override parent's object default behaviour
     */
    this.handleSocketPanEvent = function (obj) {

    };



}
// Setup prototype chain by setting prototype property, 
// RulerGuide is prototype.
// And a single property constroctor = HorizontalRulerGuideModel
HorizontalRulerGuideModel.prototype = Object.create(RulerGuide.prototype, {
    constructor: {
        value: HorizontalRulerGuideModel
    }
});

module.exports = HorizontalRulerGuideModel;