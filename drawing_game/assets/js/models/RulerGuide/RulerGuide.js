var Layers = require('../../layers/LayerAPI');
var BaseModel = require('../BaseModel');
/* 
 * An parent class from which horizontal and vertical guides will inherit
 */
function RulerGuide(options) {

    //const options = {};
    //options.canvas = $(Layers.create(options.canvas, "RulerGuidesLayer", 5)); 
    //options.socket = null;
    BaseModel.call(this, options); //call the super constructor

	const that = this;
	this.toggleRulerGuideVisibilityStatusValue = 0;

    //this.canvas = $(Layers.create(canvas, "RulerGuidesLayer", 5)); 
    //this.context = this.canvas[0].getContext('2d'); // set up context for render function
    //this.context.transform = new Transform(this.context);

    this.canvasWidth = this.canvas.width();
    this.canvasHeight = this.canvas.height();

	$.subscribe('toggleRulerGuideVisibility', function( event, obj ) {
        that.handleToggleRulerGuideVisibilityEvent.call(that, obj);
        //need this call to clear canvas after Guides were turned off
        that.clearCanvas.call(that);
    });

    /*
     * Reacting to the mouse events
     */
    /*$.subscribe('mouse_move', function( event, obj ) {;
    	if(that.shouldDrawRulerGuides()){
    		that.render.call(that, that.context, obj);
    	}
    });*/
    if(this.pubsub){
        this.pubsub.on('mouse_move', function(obj) {
            if(that.shouldDrawRulerGuides()){
                that.render.call(that, that.context, obj);
            }
        });
    }
};

// This literally overrides the prototype, so to avoid loosing all the RulerGuide.prototype functions
//this prototype decalrations must be before the functions are added to the prototype
RulerGuide.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: RulerGuide
    }
});


/*
 * Check if Ruler Guides should be drawn.
 *
 * @returns true in case Ruler Guides need to be drawn
 */
RulerGuide.prototype.shouldDrawRulerGuides = function () {
	if (this.toggleRulerGuideVisibilityStatusValue === 1){
		return true;
	}
	return false;
};

RulerGuide.prototype.handleToggleRulerGuideVisibilityEvent = function () {
	this.toggleRulerGuideVisibilityStatus();
};

RulerGuide.prototype.toggleRulerGuideVisibilityStatus = function (context) {
    this.toggleRulerGuideVisibilityStatusValue = this.toggleRulerGuideVisibilityStatusValue === 0 ? 1 : 0;
};

RulerGuide.prototype.render = function (context) {
    console.log("render is not implemented in RulerGuide");
};

RulerGuide.prototype.clearCanvas = function() {
        
        var topLeft = {};
        //topLeft.x = (0 - this.panDisplacement.x)/this.zoomLevel;
        //topLeft.y = (0 - this.panDisplacement.y)/this.zoomLevel;
        
        var bottomRight = {};
        //bottomRight.x = (this.canvas.width() )/this.zoomLevel;
        //bottomRight.y = (this.canvas.height() )/this.zoomLevel;
        
        /*this.context.clearRect(
                topLeft.x, topLeft.y, 
                bottomRight.x, bottomRight.y);*/
        this.context.clearRect(
                0, 0, 
                this.canvasWidth, this.canvasHeight);
};

module.exports = RulerGuide;