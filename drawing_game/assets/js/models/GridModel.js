/* 
 * Draw grid lines:
 * - typically slightly bleaker lines for the grid
 * - and brighter lines at 0,0 point to highlight the origin point
 */
var Layers = require('../layers/LayerAPI');
var Line = require('../shapes/Line');
var BaseModel = require('./BaseModel');

function GridModel (options) {

    //const options = {};
    //const canvas = options.canvas;
    //options.canvas = $(Layers.create(options.canvas, "GridLayer", 5));
    //options.socket = socket;
    BaseModel.call(this, options); //call the super constructor
    
    var that;
    this.init = function (options) {

        that = this;
        
        this.offset = 15;
        
        this.gridLines = []; //holds vertical and horizontal lines of the grid
        
        this.gridVisible = true;

        this.gridLinewidth = 1;

        this.gridLineColor = "#bebebe";
        this.zerothGridLineColor = "#000000";

        this.canvas = options.canvas;

        this.context = options.context;
        
        jQuery.subscribe('toggleGridVisibility', function() {
            that.toggleGridVisibility();
        })

        this.pubsub = options.pubsub;

        /*$.subscribe('mouse_left_button_up', function( event, obj ) {
          that.handleMouseLeftButtonUpEvent.call(that, obj);
        });*/

        this.initGrid();
        this.render();
    };
    
    this.initGrid = function () {
        const frequency = 100;
        const width = this.canvas.width();
        const height = this.canvas.height();
        //
        this.gridLines = [];
        //to draw the grid, all I am doing for now is draw grid lines 100 * width to the left and right
        //basically just creating so many of them that I am not likely to pan that far.
        //TODO: make it more effective by doing on demand drawing 
        for(var i = 0 - width*100; i < width *100; i = i+frequency){
            var vLine = new Line(i, 0 - height*100, i, height*100);
            vLine.setStrokeStyle(this.gridLineColor);
            if(i === 0){
                vLine.setStrokeStyle(this.zerothGridLineColor);
            }
            this.gridLines.push(vLine);
        }
        
        for(var i = 0 - height*100; i < height*100; i = i+frequency){
            var hLine = new Line(0 - width*100, i, width*100, i);
            hLine.setStrokeStyle(this.gridLineColor);
            if(i === 0){
                hLine.setStrokeStyle(this.zerothGridLineColor);
            }
            this.gridLines.push(hLine);
        }
    };
    
    this.toggleGridVisibility = function() {
        this.gridVisible = !this.gridVisible;
        this.render();
    };
    
    this.clearCanvas = function() {
        var topLeft = {};
        topLeft.x = (0 - this.panDisplacement.x)/this.zoomLevel;
        topLeft.y = (0 - this.panDisplacement.y)/this.zoomLevel;
        
        var bottomRight = {};
        bottomRight.x = (this.canvas.width() )/this.zoomLevel;
        bottomRight.y = (this.canvas.height() )/this.zoomLevel;
        
        this.context.clearRect(
                topLeft.x, topLeft.y, 
                bottomRight.x, bottomRight.y);
    };
    
    this.render = function () {
        this.clearCanvas();
        
        if (this.gridVisible ) {
            var gLen = this.gridLines.length;
            for (var i = 0; i < gLen; i++) {
                var shape = this.gridLines[i];
                shape.draw(this.context, this.gridLinewidth/this.zoomLevel);
            }
        }
    };
    
    this.init(options);
}

GridModel.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: GridModel
    }
});

module.exports = GridModel;