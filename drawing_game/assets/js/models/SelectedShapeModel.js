'use strict';
var BaseModel = require('./BaseModel');
class SelectedShapeModel extends BaseModel{

    constructor(options) {
        //BaseModel.call(this, options); //call the super constructor
        super(options);
        this.sockett = options.socket;
        this.shapes = options.shapesStorage;
        this.selectedShapeIndex = -1;
        
        const that = this;

        /*if(this.pubsub){
            this.pubsub.on('mouse_move', function(obj) {
                that.handleMouseMoveEvent(obj);
            });
        }*/
        //BaseModel handles 'mouse_left_button_down' too, so "local" handleMouseLeftButtonDownEvent will be called via
        //polymorhism. I need to call super's method here though. 
        /*if(this.pubsub){
            this.pubsub.on('mouse_left_button_down', function(obj) {
                that.handleMouseLeftButtonDownEvent(obj);
            });
        }*/
    }
    /*
     * This one simply highlights shapes if mouse passes over them
     * 
     * @param {type} obj
     * @returns {none}
     */
    /*handleMouseMoveEvent (obj) {
       if(obj.isDrawing === false){
          this.markSelectedShapeIfMouseOverIt(obj.mouseCoordsOverCanvas.x, obj.mouseCoordsOverCanvas.y);
       }       
    };*/

    markSelectedShapeIfMouseOverIt (mouseX, mouseY) {
        var selectedShapeIndex = this.getSelectedShapeIndex(mouseX, mouseY);
        this.setSelectedShape(selectedShapeIndex);
    };

    getSelectedShapeIndex (mouseX, mouseY) {
        var len = this.shapes.size();
        for (var i = 0; i < len; i++) {
            var shape = this.shapes.get(i);
            if (shape.isPointOver(this.context, mouseX, mouseY)) {
                return i;
            }
        }
        return -1;
    };

    setSelectedShape (index) {
        if (index >= 0 && index < this.shapes.size()) {
            this.selectedShapeIndex = index;
            $.publish("shape_selected", {selectedShapeIndex:index});
        }
    };

    handleMouseLeftButtonDownEvent (obj) {
		if( obj === null){
			return;
		}
        BaseModel.prototype.handleMouseLeftButtonDownEvent(obj);
        this.markSelectedShapeIfMouseOverIt(obj.mouseCoordsOverCanvasAdjusted.x, obj.mouseCoordsOverCanvasAdjusted.y);
    }
    /*
     * placeholder in order not to crush when parent BaseModel calls its children's render method
     */
    render(){
        //need to have it there because BaseModel calls render() for its children, i.e. when panning
    }
}
/*
SelectedShapeModel.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: SelectedShapeModel
    }
});
*/
module.exports = SelectedShapeModel;