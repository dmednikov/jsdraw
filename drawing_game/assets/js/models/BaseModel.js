/* 
 * This is BASE model from which many other models should inherit
 * It provides common functionality for:
 * - zooming
 * - panning
 * - clearing canvas

 * 
 */

var Transform = require('../matrix/Transform');
//var ShapeStoreArray = require('../storage/shapes/ShapeStoreArray');

function BaseModel(options) {

    var that;

    this.init = function (options) {
        
        if(options.socket){
            this.sockett = options.socket;
        }

        this.zoomLevel = 1;

        this.panDisplacement = {};
        this.panDisplacement.x = 0;
        this.panDisplacement.y = 0;

        this.mouseDownOrigin = {};//mouseDownOrigin - start for pan
        this.mouseDownOrigin.x = 0;
        this.mouseDownOrigin.y = 0;

        this.pubsub = options.pubsub;
        
        if(options.canvas){
            this.canvas = options.canvas;
        }

        this.context = options.context;
 
        that = this;

        /*
         * Zoom on the client
         */
        /*jQuery.subscribe('client_zooming', function( event, obj ) {
            that.handleZoomEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('client_zooming', function(obj) {
                that.handleZoomEvent.call(that, obj);
            });
        }
        /*
         * 
         */
        /*jQuery.subscribe('socket_zooming', function( event, obj ) {
            that.handleZoomEvent.call(that, obj);

        });*/
        if(this.pubsub){
            this.pubsub.on('socket_zooming', function(obj) {
                that.handleZoomEvent.call(that, obj);
            });
        }

        /*jQuery.subscribe('client_panning', function( event, obj ) {
            that.handlePanEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('client_panning', function(obj) {
                that.handlePanEvent.call(that, obj);
            });
        }

        /*jQuery.subscribe('socket_panning', function( event, obj ) {
            that.handleSocketPanEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('socket_panning', function(obj) {
                that.handleSocketPanEvent.call(that, obj);
            });
        }
        /*
         * Set the origin for pan start position
         */
        /*jQuery.subscribe('mouse_left_button_down', function( event, obj ) {
          that.handleMouseLeftButtonDownEvent.call(that, obj);
        });*/
        //pubsub is not guaranteed at all, some models do not need it
        if(this.pubsub){
            this.pubsub.on('mouse_left_button_down', function(obj) {				
                that.handleMouseLeftButtonDownEvent.call(that, obj);
            });
        }

        if(this.pubsub){
            this.pubsub.on('window_resized', function(obj) {
                that.handleWindowResizedEvent.call(that, obj);
            });
        } 

    };
    
    this.init(options);
}

BaseModel.prototype.getScale = function (obj) {
    return this.context.transform.getScaleX();
};

BaseModel.prototype.getScaleX = function (obj) {
    return this.context.transform.getScaleX();
};
BaseModel.prototype.getScaleY = function (obj) {
    return this.context.transform.getScaleY();
};
/*
 * Need to set the origin when left mouse button is pressed down
 * as it is going to be a new shape origin or start of pan
 * 
 * @param {type} obj
 * @param {type} bool indicates if this function should call render function
 * @returns {none}
 */
BaseModel.prototype.handleWindowResizedEvent = function (obj, doNotRender) {
    let offset = 0;
    if(this.canvas[0].customOffset !== undefined){
        offset = this.canvas[0].customOffset;
    }
    //
    this.canvas.width(obj.canvas.width()+offset);
    this.canvas.height(obj.canvas.height()+offset);
    //
    this.context.canvas.width = obj.canvas.width()+offset;
    this.context.canvas.height = obj.canvas.height()+offset;  
    //
    this.context.transform.translate(
        this.panDisplacement.x, this.panDisplacement.y
    );
    /*if (typeof this.reactToCanvasResizeEvent === "function") { 
        this.reactToCanvasResizeEvent();
    }*/
    //
    if (doNotRender) { 
        return;
    }
    this.render.call(this, obj);
};
/*
 * Need to set the origin when left mouse button is pressed down
 * as it is going to be a new shape origin or start of pan
 * 
 * @param {type} obj
 * @returns {none}
 */
BaseModel.prototype.handleMouseLeftButtonDownEvent = function (obj) {
	if( obj === null){
		return;
	}
	//this call makes a huge difference to panning and zooming
    this.mouseDownOrigin = obj.mouseCoordsOverCanvasAdjusted;
};

BaseModel.prototype.handleZoomEvent = function (obj) {
    this.zoomLevel *= obj.zoomL;
    
    this.context.transform.zoomOnPoint(
        obj.zoomL, obj.zoomL, obj.origin.x, obj.origin.y
    );

    //const matrix = this.context.transform.getMatrix();
    this.panDisplacement.x = this.context.transform.getXTranslation();
    this.panDisplacement.y = this.context.transform.getYTranslation();

    this.render(this.context, obj);
};

BaseModel.prototype.handlePanEvent = function (obj) {
    //console.info(this);
    //console.info(this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x));
    //console.info(obj.mouseCoordsOverCanvas.x + " - basemodel - " + this.mouseDownOrigin.x + " - basemodel - " + this.panDisplacement.x);
    //console.info(this.panDisplacement);
    this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
    
    this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);

    this.context.transform.translate(
        this.panDisplacement.x, this.panDisplacement.y
    );

    this.render(this.context, obj);
};

/*
 * There is some specific to socket panning because
 * there was no mouse_down event to set the origin
 * And this is nothing to calculate here, just values to the ones passed in the event
 */
BaseModel.prototype.handleSocketPanEvent = function (obj) {
    this.mouseDownOrigin = obj.origin;

    this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
    
    this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);

    this.context.transform.translate(
        this.panDisplacement.x, this.panDisplacement.y
    );

    this.render(this.context, obj);
};

/*
 * Calculates dispalcement on one axis based on the mouseCoordsOverCanvas (passed in)
 * and previously svaed MouseDownOrigin
 *
 * @param mouseCoordsOverCanvasOnXorY is current mouse position over the canvas either over X or Y axis
 * @param panDisplacementOnXorY current pan displacement
 * @param mouseDownOriginOnXorY mouse position when pan started
 */
BaseModel.prototype.calculateDeltaDisplacement = function (mouseCoordsOverCanvasOnXorY, panDisplacementOnXorY, mouseDownOriginOnXorY) {

        // obj.mouseCoordsOverCanvas is current mouse position over the canvas; It doesn't account 
        // for zoom or previous dicplacement
        //console.info("calculateDeltaDisplacement parameters = " + mouseCoordsOverCanvasOnXorY + " : " + panDisplacementOnXorY + " : " + mouseDownOriginOnXorY + " : " + this.zoomLevel);
        var current = (mouseCoordsOverCanvasOnXorY - panDisplacementOnXorY)/this.zoomLevel;
        //console.info("calculateDeltaDisplacement current = " + current);
        //need to multiply by this.zoomLevel to convert displacement back to screen coords
        var diffX = (mouseDownOriginOnXorY - current) * this.zoomLevel;
        //console.info("calculateDeltaDisplacement diffX = " + diffX);
        var directionX = (diffX < 0) ? 1 : -1;
        diffX = Math.abs(diffX);
        //console.info("calculateDeltaDisplacement diffX * directionX = " + (diffX * directionX));
        return (diffX * directionX);
};

module.exports = BaseModel;