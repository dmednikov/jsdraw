/* 
 * Draw ruler
 */

var Line = require('../../shapes/Line');
var RulerModel = require('./RulerModel');
//var $ = global.jQuery = require("jquery"); // needed for testing, to avoid "$ is not defined"
//console.info($);
function HorizontalRulerModel (options) {

    RulerModel.call(this, options); //call the super constructor

    var that;

    this.updateRulerHorizontalTicksArray = function (rulerTicksArray, limit, sign, offset) {
       //rulerTicksArray = [];
        const lOffset = offset ? Math.ceil(offset / 10) * 10 : 0; // needs to be rounded to the nearest 10 (since ruler is in 10s)
        //console.info("lOffset " + offset);
        //console.info(lOffset);
        for(var i = 0; i < limit ; i += 10){
            if(i < lOffset){
                continue;
            }
            var coord = sign * i;
            //console.info("coord:" + coord + " limit:" + limit);
            if(coord < 0 && coord > limit){
                continue;
            }
            var x0 = coord + this.offsetX;
            var y0 = this.offsetY;
            
            var x1 = x0;
            var y1 = (coord / 100 === parseInt(coord / 100)) ? 10 : 15;

            const line = new Line(x0, y0, x1, y1);
            if(y1 === 10){
                line.setLabel(coord);
                line.setLabelOrientation("horizontal");
            }
            line.setModelScale(this.getScaleX());
            line.setStrokeStyle(this.rulerLineColor);
            rulerTicksArray.push(line);
        }

        return rulerTicksArray;
    };

    this.createRuler = function () {
        var width = this.canvas.width();
        this.rulerTicks.horizontal = [];
        this.rulerTicks.horizontal = this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, width, 1);
    };
    
    this.clearCanvas = function() {

        var topLeft = {};
        topLeft.x = (0 - this.panDisplacement.x)/this.zoomLevel;
        topLeft.y = (0 - this.panDisplacement.y)/this.zoomLevel;
        
        var bottomRight = {};
        bottomRight.x = (this.canvas.width() )/this.zoomLevel;
        bottomRight.y = (this.canvas.height() )/this.zoomLevel;

        if(this.context){
            this.context.clearRect(
                topLeft.x, topLeft.y, 
                bottomRight.x, bottomRight.y);
        }
        
      
    };
    /*
     * Each canvas model needs to be able to react to canvas resizing.
     * In this case we need to either shorten or extend the vertical ruler ticks
     * depending if the canvas shrunk or enlarged
     */
    this.reactToCanvasResizeEvent = function(){
        var width = this.canvas.width();
        this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, width, 1);
    }

    this.updateRuler = function (obj) {
        this.rulerTicks.horizontal = [];

        var width = this.canvas.width();
        //console.info(this.panDisplacement.x);
        //panning to the right (dragging 0 to the right)
        if(this.panDisplacement.x >= 0 ){
            //because the ticks must start from 0 I need fill array to the right and left of 0
            //thus 2 calls to this.updateRulerVerticalTicksArray
            const leftLimit = this.panDisplacement.x/this.getScaleX();
            //to the left from 0 when panning right (moving 0 to the right)
            this.rulerTicks.horizontal = this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, leftLimit, -1);

            const rightLimit = width/this.getScaleX() - this.panDisplacement.x/this.getScaleX();
            //from left border to 0 mark when panning right (moving 0 to the right)
            this.rulerTicks.horizontal = this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, rightLimit, 1);
        }
        //panning to the left (dragging 0 to the left)
        if(this.panDisplacement.x/this.getScaleX() < 0 ){
            const rightLimit = width/this.getScaleX() + this.panDisplacement.x/this.getScaleX() * -1;
            const leftOrigin = -1.0*this.panDisplacement.x/this.getScaleX();
            this.rulerTicks.horizontal = this.updateRulerHorizontalTicksArray(this.rulerTicks.horizontal, rightLimit, 1, leftOrigin);
        }

    }

    this.render = function () {
        this.clearCanvas();
        this.rulerTicks.horizontal.forEach(function(element){
            element.draw(this.context, 1/this.zoomLevel);
        }, this);
    }    
    /*
     * Override parent's object default behaviour
     */
    this.handleZoomEvent = function (obj) {
        this.zoomLevel *= obj.zoomL;
        // must add this.offsetX because the origin of the canvas is offset
        this.context.transform.zoomOnPoint(
            obj.zoomL, 1, obj.origin.x - this.offsetX, 0
        );
        //const matrix = this.context.transform.getMatrix();
        this.panDisplacement.x = this.context.transform.getXTranslation();
        var trans = this.offsetX - this.offsetX*this.getScale();
        this.panDisplacement.x += trans;

        this.updateRuler(obj);
        this.render(this.context, obj);
    };
    /*
     * Override parent's object default behaviour
     */
    this.handlePanEvent = function (obj) {
        //console.info(this);
        //console.info(this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x));
        //console.info(obj.mouseCoordsOverCanvas.x + " - ruler - " + this.mouseDownOrigin.x + " - ruler - " + this.panDisplacement.x);
        this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
        // trans here is alittle bit to account for the fact that origin of the canvas is offset
        //var trans = this.context.transform.getXTranslation();
        var trans = this.offsetX - this.offsetX*this.getScale();
        this.panDisplacement.x += trans;

        this.context.transform.translate(
            this.panDisplacement.x, 0
        );
        this.updateRuler(obj);
        this.render(this.context, obj);
    };
    /*
     * Override parent's object default behaviour
     */
    this.handleSocketPanEvent = function (obj) {
        this.mouseDownOrigin = obj.origin;
        this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
        //this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);
        this.context.transform.translate(
            this.panDisplacement.x, this.panDisplacement.y
        );
        this.updateRuler(obj);
        this.render(this.context, obj);
    };
    /*
     * Handle the resize event from here and call super's method
     */
     this.handleWindowResizedEvent = function(obj) {
        this.__proto__.handleWindowResizedEvent.call(this, obj, true);
        this.updateRuler();
        this.render(obj);
     }
    /*
     * This init() function must be on the bottom of the declaration
     * otherwise (because there is no function hoisting) functions called from init()
     * will be from parents prototype since the this's  function are not defined yet
     * for example render()
     */
    this.init = function () {
        that = this;
        this.rulerOriginX = this.offsetX;
        this.rulerOriginY = this.offsetY;
        this.createRuler();
        this.render();

    };
    
    this.init();

}

// This literally overrides the prototype, so to avoid loosing all the RulerGuide.prototype functions
//this prototype decalrations must be before the functions are added to the prototype
HorizontalRulerModel.prototype = Object.create(RulerModel.prototype, {
    constructor: {
        value: HorizontalRulerModel
    }
});

module.exports = HorizontalRulerModel;