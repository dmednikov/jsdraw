/* 
 * Draw ruler
 */
var Layers = require('../../layers/LayerAPI');
var BaseModel = require('../BaseModel');

function RulerModel (options) {
    
    //options.canvas = jQuery(Layers.oneOf(options.canvas)); 

    BaseModel.call(this, options); //call the super constructor

    var that;
    this.init = function () {
        
        //the edges of the RulerLayer are not flush with Paper canvas edges, the are offset
        this.offsetX = 20;
        this.offsetY = 21;
        
        this.rulerTicks = {};// object to hold all the lines required to draw ruler
        this.rulerTicks.horizontal = [];
        this.rulerTicks.vertical = [];

        //this.canvasBorder = []; // holds 4 lines to draw the border around the canvas
        
        //this.canvas = $(Layers.oneOf(canvas)); 
        //this.context = this.canvas[0].getContext('2d'); // set up context for render function

        this.rulerLineColor = "#000000";
       
        //that = this;
        
        //this.createRuler();
        //this.render();
    };
    this.init();
    
    this.createRuler = function () {
          
    };
    
    this.clearCanvas = function() {

    };

    this.render = function () {
        console.info("up");
        /*
        this.clearCanvas();

        this.rulerTicksAndOutline.horizontal.forEach(function(element){
            element.draw(this.context);
        }, this);

        this.rulerTicksAndOutline.vertical.forEach(function(element){
            element.draw(this.context);
        }, this);

        this.canvasBorder.forEach(function(element){
            element.draw(this.context);
        }, this);
        */

    }

    /*
     * Override parent's object default behaviour
     */
    /*this.handleZoomEvent = function (obj) {

    };*/
    /*
     * Override parent's object default behaviour
     */
    /*this.handlePanEvent = function (obj) {
        
        this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
    
        //this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);

        this.context.transform.translate(
            this.panDisplacement.x, 0
        );

        this.renderHorizontalRuler(this.context, obj);
        
    };*/
    /*
     * Override parent's object default behaviour
     */
    /*this.handleSocketPanEvent = function (obj) {

    };*/
    
    
}

// This literally overrides the prototype, so to avoid loosing all the RulerGuide.prototype functions
//this prototype decalrations must be before the functions are added to the prototype
RulerModel.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: RulerModel
    }
});

module.exports = RulerModel;

