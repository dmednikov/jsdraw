/* 
 * Draw ruler
 * TODO: sucky comments. Need to improve.
 */

var Line = require('../../shapes/Line');
var RulerModel = require('./RulerModel');
//var $ = global.jQuery = require("jquery"); // needed for testing, to avoid "$ is not defined"
//console.info($);
function VerticalRulerModel (options) {

    RulerModel.call(this, options); //call the super constructor

    var that;

    this.updateRulerVerticalTicksArray = function (rulerTicksArray, limit, sign) {
        //rulerTicksArray = [];
        for(var i = 0; i < limit ; i += 10){
            var coord = sign * i;
            var y0 = coord + this.offsetX;
            var x0 = this.offsetX;
            
            var y1 = y0;
            var x1 = (coord / 100 === parseInt(coord / 100)) ? 10 : 15;

            const line = new Line(x0, y0, x1, y1);
            if(x1 === 10){
                line.setLabel(coord);
                line.setLabelOrientation("vertical");
            }
            line.setModelScale(this.getScaleY());
            line.setStrokeStyle(this.rulerLineColor);
            rulerTicksArray.push(line);
        }

        return rulerTicksArray;
    };


    this.createRuler = function () {
        var height = this.canvas.height();
        this.rulerTicks.vertical = this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, height, 1);
    };

    this.clearCanvas = function() {
        var topLeft = {};
        topLeft.x = (0 - this.panDisplacement.x)/this.getScaleX();
        topLeft.y = (0 - this.panDisplacement.y)/this.getScaleY();
        
        var bottomRight = {};
        bottomRight.x = (this.canvas.width() )/this.getScaleX();
        bottomRight.y = (this.canvas.height() )/this.getScaleY();
        
        this.context.clearRect(
                topLeft.x, topLeft.y, 
                bottomRight.x, bottomRight.y);
    };
    /*
     * Each canvas model nned to be able to react to canvas resizing.
     * In this case we need to either shorten or extend the vertical ruler ticks
     * depending if the canvas shrunk or enlarged
     */
    this.reactToCanvasResizeEvent = function(){
        var height = this.canvas.height();
        this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, height, 1);
    }

    this.updateRuler = function (obj) {

        this.rulerTicks.vertical = [];

        var height = this.canvas.height();
        //panning down (dragging 0 down)
        if(this.panDisplacement.y/this.getScaleY() >= 0 ){
            // because the ticks must start from 0 I need fill array to the right and left of 0
            // thus 2 calls to this.updateRulerVerticalTicksArray
            const upperLimit = this.panDisplacement.y/this.getScaleY();
            this.rulerTicks.vertical = this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, upperLimit, -1);

            const lowerLimit = height/this.getScaleY() - this.panDisplacement.y/this.getScaleY();
            this.rulerTicks.vertical = this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, lowerLimit, 1);
        }
        //panning up (dragging 0 up)
        if(this.panDisplacement.y/this.getScaleY() < 0 ){
            const lowerLimit = height/this.getScaleY() + this.panDisplacement.y/this.getScaleY() * -1;
            this.rulerTicks.vertical = this.updateRulerVerticalTicksArray(this.rulerTicks.vertical, lowerLimit, 1);
        }

    }

    this.render = function () {
        this.clearCanvas();
        this.rulerTicks.vertical.forEach(function(element){
            element.draw(this.context, 1/this.zoomLevel);
        }, this);
    }  

    /*
     * Override parent's object default behaviour
     */
    this.handleZoomEvent = function (obj) {
        this.zoomLevel *= obj.zoomL;
        
        // must add this.offsetX because the origin of the canvas is offset
        this.context.transform.zoomOnPoint(
            1, obj.zoomL, 0, obj.origin.y + this.offsetX
        );

        const matrix = this.context.transform.getMatrix();
        this.panDisplacement.y = this.context.transform.getYTranslation();

        this.updateRuler(obj);
        this.render(this.context, obj);
    };
    /*
     * Override parent's object default behaviour
     */
    this.handlePanEvent = function (obj) {

        this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);

        // trans here is a little bit to account for the fact that origin of the canvas is offset
        var trans = this.context.transform.getYTranslation();
        trans = this.offsetX - this.offsetX*this.getScaleY();
        this.panDisplacement.y += trans;

        this.context.transform.translate(
            0, this.panDisplacement.y
        );

        this.updateRuler(obj);

        this.render(this.context, obj);
    };
    /*
     * Override parent's object default behaviour
     */
    this.handleSocketPanEvent = function (obj) {
        this.mouseDownOrigin = obj.origin;

        //this.panDisplacement.x += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.x, this.panDisplacement.x, this.mouseDownOrigin.x);
        
        this.panDisplacement.y += this.calculateDeltaDisplacement(obj.mouseCoordsOverCanvas.y, this.panDisplacement.y, this.mouseDownOrigin.y);

        this.context.transform.translate(
            this.panDisplacement.x, this.panDisplacement.y
        );
        this.updateRuler(obj);
        this.render(this.context, obj);

    };
    /*
     * Handle the resize event from here and call super's method
     */
     this.handleWindowResizedEvent = function(obj) {
        this.__proto__.handleWindowResizedEvent.call(this, obj, true);
        this.updateRuler();
        this.render(obj);
     }
    /*
     * This init() function must be on the bottom of the declaration
     * otherwise (because there is no function hoisting) functions called from init()
     * will be from parents prototype since the this's  function are not defined yet
     * for example render()
     */
    this.init = function () {
        that = this;
        this.rulerOriginX = this.offsetX;
        this.rulerOriginY = this.offsetY;
        this.createRuler();
        this.render();

    };
    
    this.init();
    
}

// This literally overrides the prototype, so to avoid loosing all the RulerGuide.prototype functions
//this prototype decalrations must be before the functions are added to the prototype
VerticalRulerModel.prototype = Object.create(RulerModel.prototype, {
    constructor: {
        value: VerticalRulerModel
    }
});

module.exports = VerticalRulerModel;

