/* 
 * This model is nothing more just an information storage
 * about the current pan and zoom values.
 * Each model actually inherits this values from BaseModel right now
 */

var BaseModel = require('./BaseModel');

function PanZoomModel(options) {

    BaseModel.call(this, options); //call the super constructor
    
    var that;
    this.init = function () {
        
        that = this;        
        
    };
    
    this.init();

    this.getPanDisplacement = function(  ) {
        return this.panDisplacement;
    };

    this.render = function(  ) {
        //do nothing
    };

    this.getMouseDownOrigin = function () {
        return this.mouseDownOrigin;
    };
	
}

PanZoomModel.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: PanZoomModel
    }
});

module.exports = PanZoomModel;