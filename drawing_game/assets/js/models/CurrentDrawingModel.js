/* 
 * This is a "temporary" drawing model. It is representation of 
 * a "shape being drawn" and nothings else really. Could be called 
 * CurrentlyDrawnShapeModel to be clearer.
 *
 * Whatever user currently is drawing on the screen is supported via this model.
 * Unlike permanent model which stores all drawn shapes.
 *
 * This model passes its context into shapes when they need to draw themselves.
 * This context must "transform"ed according to the current panDisplacement and zoomLevel
 * in order for shpes to be drawn in appropriate locations
 *
 */

var Line = require('../shapes/Line');
var Circle = require('../shapes/Circle');
var FreeForm = require('../shapes/FreeForm');
var ShapeFactory = require('../shapes/ShapeFactory');

var Transform = require('../matrix/Transform');

var Layers = require('../layers/LayerAPI');

var BaseModel = require('./BaseModel');

function CurrentDrawingModel (options) {
    
    BaseModel.call(this, options); // call the super constructor

    var that;
    this.init = function (options) {

        that = this;
        
        this.sockett = options.socket;
        
        this.currentDrawingMode = 0; // line, circle, freeform, ...  or 0
        
        this.currentShape = null; // shape currently being drawn

        this.context = options.context;

        this.pubsub = options.pubsub;
        

        /*
         * Set the origin for the shape to be drawn
         */
        /*$.subscribe('mouse_left_button_down', function( event, obj ) {
          that.handleMouseLeftButtonDownEvent.call(that, obj);
        });*/
		
		// HMMMMMMM, I already have PUBSUB in the BaseModel for this
		// should not really fire the same event the second time
        if(this.pubsub){
            this.pubsub.on('mouse_left_button_down', function(obj) {
                //that.handleMouseLeftButtonDownEvent.call(that, obj);
            });
        }

        /*$.subscribe('mouse_left_button_up', function( event, obj ) {
          that.handleMouseLeftButtonUpEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('mouse_left_button_up', function() {
                that.handleMouseLeftButtonUpEvent.call(that);
            });
        }      

        /*
         * This is in response to server published event to update clients
         * with currently drawn shape
         */
        /*$.subscribe('update_current_drawing_model', function( event, obj ) {
            that.setCurrentShape(ShapeFactory.createFromJSON(obj));
            that.render();
        });*/
        if(this.pubsub){
            this.pubsub.on('update_current_drawing_model', function(obj) {
                that.setCurrentShape(ShapeFactory.createFromJSON(obj));
                that.render();
            });
        } 

        /*$.subscribe('selectDrawingMode', function( event, obj ) {
            that.setDrawingMode(obj.key);
        });*/

        $.subscribe('stopDrawing', function( event, obj ) {
            that.setDrawingMode(0);
        });

        /*$.subscribe('new_shape_created', function( event, obj ) {
            that.flushBuffer();
            that.render();
        });*/
        if(this.pubsub){
            this.pubsub.on('new_shape_created', function(obj) {
              that.flushBuffer();
              that.render();
            });
        }

        

        /*
         * Reacting to the mouse events
         */
        /*$.subscribe('mouse_move', function( event, obj ) {
            that.handleMouseMoveEvent.call(that, obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('mouse_move', function(obj) {
                that.handleMouseMoveEvent.call(that, obj);
            });
        }
    };
    
    this.init(options);
    
    /*
     * This event, even though a mouse move event, has a parameter
     * isDrawingset to ttrue in order makes sense in this context. The context
     * of drawing shapes that is
     * 
     * @param {type} obj
     * @returns {none}
     */
    this.handleMouseMoveEvent = function (obj) {

        if(obj.isDrawing === true){
            this.updateCurrentShape(obj.mouseCoordsOverCanvasAdjusted.x, obj.mouseCoordsOverCanvasAdjusted.y);
            //initiate event to be sent to all connected clients
            this.emitChangeToCurrentShape();
            this.render();
        }

    };
    
    /*
     * When left mouse button is up it means that drawing has finished
     * Need to get the latest drawn shape and place on the queue for subscribers
     * @param {type} obj
     * @returns {none}
     */
    this.handleMouseLeftButtonUpEvent = function () {
        
		// Really not big fan of doing it that way. Spreading the knowledge about the types of Shapes
		// across the file here. Not sure if there is a better way. But prefer not having this
		// if === "Point". It might lead to explosion (if === ShapeType). I guess I woonder if shapes
		// themselves should be aware that they are finished to be drawn and emit appropriate events
		// Yes, I think this is a better way to go.
		
		// meanwhile this one is needed for desktop version, mobile fires mouse_down event on touchend
        const shape = this.getCurrentShape();
        //
        if (shape !== null && shape.isValid() && shape.constructor.name === "Point") {
			this.flushBuffer();
            if(this.pubsub){
              this.pubsub.emit('new_shape_created', {"new_shape": shape.toJSON()} );
            }
            this.emitNewShapeCreated({"new_shape": shape.toJSON()});
        }
		
    };
    /*
     * When left mouse button is down it can be either a start of panning
     * or a start of a new shape
	 * The event itself is subscribed to in BaseModel and handled there and here 
     *
     * @param {type} obj
     * @returns {none}
     */
    this.handleMouseLeftButtonDownEvent = function (obj) {
		
	
		const shape = this.getCurrentShape();
        
        if (shape !== null && shape.isValid()) {
			this.flushBuffer();
            //$.publish("new_shape_created", {"new_shape": shape.toJSON()});
            if(this.pubsub){
              this.pubsub.emit('new_shape_created', {"new_shape": shape.toJSON()} );
            }
            this.emitNewShapeCreated({"new_shape": shape.toJSON()});
        } else {
			
			if( obj === null){
				return;
			}
			// this call needs to be made in order to keep drawing shapes in the correct location after panning
			CurrentDrawingModel.prototype.handleMouseLeftButtonDownEvent.call(this, obj);
		
			// looks like this is StartShape event
			if(obj.isDrawing === true){
				const shapeParams = {
					'originX' : obj.mouseCoordsOverCanvasAdjusted.x, 
					'originY' : obj.mouseCoordsOverCanvasAdjusted.y
				}
				this.setCurrentShape(ShapeFactory.initialize(obj.currentDrawingMode, shapeParams));
				this.emitChangeToCurrentShape();
				this.render();
			}
		}		
    };

    /*
     * Sets the currentDrawingMode to the string of the shape (line, circle, ...)
     * or 0 when user stops drawing
     */
    this.setDrawingMode = function (drawingMode) {
        this.currentDrawingMode = drawingMode;
    };

    this.getDrawingMode = function() {
        return this.currentDrawingMode;
    };
    /*
     * Poorly named function. It does 3 things not just flushes buffer!
     */
    this.flushBuffer = function () {
        this.setCurrentShape(null);
        this.render();
    };
    /*
     * Returns the property currentShape of if it is undefined then null
     */
    this.getCurrentShape = function () {
        return this.currentShape || null;
    };
    
    this.emitChangeToCurrentShape = function() {
        if(this.getCurrentShape() !== null){
            this.sockett.emit('socket_shape_being_drawn_changed', this.getCurrentShape().toJSON());
        }
    };

    this.emitNewShapeCreated = function(shape) {
        this.sockett.emit('shapeCreated', shape);
    };

    
    
    this.updateCurrentShape = function( pageX, pageY) {
        if(this.getCurrentShape() !== null){
            this.getCurrentShape().update(pageX, pageY);
        }
    };
    
    this.setCurrentShape = function (shape) {
        this.currentShape = shape;
    };
    
    this.clearCanvas = function() {
        
        var topLeft = {};
        topLeft.x = (0 - this.panDisplacement.x)/this.zoomLevel;
        topLeft.y = (0 - this.panDisplacement.y)/this.zoomLevel;
        
        var bottomRight = {};
        bottomRight.x = (this.canvas.width() )/this.zoomLevel;
        bottomRight.y = (this.canvas.height() )/this.zoomLevel;
        
        this.context.clearRect(
                topLeft.x, topLeft.y, 
                bottomRight.x, bottomRight.y);
    };
    
    /*
     * this.panDisplacement and this.zoomLevel are very important in this model
     * because the context passed into shapes for drawing needs to be transformed accordingly
     */
    this.render = function () {
        this.clearCanvas();
        if(this.getCurrentShape() !== null){
            this.getCurrentShape().draw(this.context);
        }
    };
}

CurrentDrawingModel.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: CurrentDrawingModel
    }
});

module.exports = CurrentDrawingModel;