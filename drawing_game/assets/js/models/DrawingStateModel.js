/* 
 * This model is responsible for maintaing drawing state
 * For example:
 * - is drawing being panned currently
 * - is drawing being zoomed
 * - is drawing being drawn :)
 * 
 */

function DrawingStateModel (options) {
    var that;
    this.init = function (options) {
      that = this;
      
      this.drawing = false;
      this.currentDrawingMode = 0;
      this.panning = false;

      this.pubsub = options.pubsub;

      /*$.subscribe('mouse_left_button_up', function( event, obj ) {
          that.handleMouseLeftButtonUpEvent.call(that, obj);
      });*/
      if(this.pubsub){
          this.pubsub.on('mouse_left_button_up', function(obj) {
              that.handleMouseLeftButtonUpEvent.call(that);
          });
      }  
      
      /*
       * User either wants pan or draw
       */
      /*$.subscribe('mouse_left_button_down', function( event, obj ) {
         if (that.getDrawingMode()) {
            that.setDrawing(true);
         } else {
            that.setPanning(true);
         }
      });*/
      if(this.pubsub){
        this.pubsub.on('mouse_left_button_down', function() {
          if (that.getDrawingMode()) {
            that.setDrawing(true);
          } else {
            that.setPanning(true);
          }
        });
      }
      
      //when mouse button is up or left the canvas
      //need to reset the drawing modes
      /*$.subscribe('mouse_left_canvas_boundaries', function( event, obj ) {
          that.setDrawing(false);
          that.setPanning(false);
          //that.setDrawingMode(false); //cannot do that ... connected clients get messed up ... one has circle, then another line, then the first will have line drawn isntead of circle after switching
      });*/
      if(this.pubsub){
        this.pubsub.on('mouse_left_canvas_boundaries', function() {
          that.setDrawing(false);
          that.setPanning(false);
        });
      }

      $.subscribe('selectDrawingMode', function( event, obj ) {
        that.handleSelectDrawingModeEvent(obj);
      });

      $.subscribe('stopDrawing', function( event, obj ) {
        that.handleStopDrawingEvent();
      });
  
   };
   
   this.init(options);
   /*
    * When left mouse is up it is either drfawing is finished (user needs to
    * hold mouse down to draw) or panning is finished
    */
   this.handleMouseLeftButtonUpEvent = function(){
      //this.setDrawing(false);
      this.setPanning(false);
   }

    /*
     * set drawing mode to the name of the shape being drawn
     * 
     * @param {type} obj
     * @returns {none}
     */
    this.handleSelectDrawingModeEvent = function (obj) {
        //this.setDrawingMode(true);
        this.setDrawingMode(obj.key);
    };
    /*
     * set drawing mode to the name of the shape being drawn
     * 
     * @param {type} obj
     * @returns {none}
     */
    this.handleStopDrawingEvent = function (obj) {
        this.setDrawingMode(false);
    };   
    
    this.setDrawing = function( isDrawing ){
        this.drawing = isDrawing;
    };
    this.isDrawing = function(){
        return this.drawing;
    };
    
    this.setPanning = function( isPanning ){
        this.panning = isPanning;
    };
    this.isPanning = function(){
        return this.panning;
    };
    
    /*
     * We either are drawing or not
     * 
     * @param {boolean} drawingMode
     * @returns {none}
     */
    this.setDrawingMode= function( drawingMode ){
        this.currentDrawingMode = drawingMode;
    };
    this.getDrawingMode = function(){
        return this.currentDrawingMode;
    };
    
}

module.exports = DrawingStateModel;