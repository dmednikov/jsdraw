/* 
 * This model maintains an array of all shapes drawn (for redrawing on canvas
 * and any manipulation)
 * It also draws these shapes on the canvas (clears canvas as well). However, it only
 * "draws" the shapes from "shapes" array. There is a difference between
 * those and a shape being drawn (this one happens in its own model) and later
 * added to "shapes" array.
 * It also handles "pan" event and "init"s a grid
 * Makes shapes selected
 * 
 * TODO: Too many responsibilities and it should be refactored
 */

var Line = require('../shapes/Line');
var ShapeFactory = require('../shapes/ShapeFactory');
var Transform = require('../matrix/Transform');
var BaseModel = require('./BaseModel');
//var ShapeStoreArray = require('../storage/shapes/ShapeStoreArray');

function BasicModel(options) {

    //const options = {};
    //options.canvas = canvas;
    //options.socket = socket;
    BaseModel.call(this, options); //call the super constructor
    
    var that;
    this.init = function (options) {
        
        this.sockett = options.socket;

        this.lastEmit = 0;

        this.shapes = options.shapesStorage;
        //layers are to keep various ... static? layers in the model
        //this.layers = {};
        //this.layers.grid = [];
        
        this.selectedShapeIndex = -1;
        
        that = this;
        
        /*
         * Reacting to the mouse events
         */
        /*$.subscribe('mouse_move', function( event, obj ) {
            that.handleMouseMoveEvent.call(that, obj);
        });*/
        /*if(this.pubsub){
            this.pubsub.on('mouse_move', function(obj) {
                that.handleMouseMoveEvent.call(that, obj);
            });
        }*/
        $.subscribe('shape_selected', function( event, obj ) {
            that.handleShapeSelectedEvent.call(that, obj);
        });

        /*.subscribe('new_shape_created', function( event, obj ) {
            that.addShape(ShapeFactory.createFromJSON(obj.new_shape));
        });*/
        if(this.pubsub){
            this.pubsub.on('new_shape_created', function(obj) {
                that.addShape(ShapeFactory.createFromJSON(obj.new_shape));
            });
        }

        //
        /*$.subscribe('shape_deleted', function( event, obj ) {
            that.deleteShapeByIndex(obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('shape_deleted', function(obj) {
                that.deleteShapeByIndex(obj);
            });
        }

        //
        /*$.subscribe('shape_updated', function( event, obj ) {
            that.updateShapeByIndex(obj);
        });*/
        if(this.pubsub){
            this.pubsub.on('shape_updated', function(obj) {
                that.updateShapeByIndex(obj);
            });
        }

        //from toolbar
        $.subscribe('deleteShape', function( event, obj ) {
            that.deleteShape();
        });
        
        //initGrid(this, this.canvas.height(), this.canvas.width());
        
        this.canvas.on('fromContextMenu', {}, function (e, param) {
            switch (param.key) {
                case "delete":
                    that.deleteShape();
            }
        });
    
        
        
    };
    
    this.init(options);
    
    
    
    /*
     * This one simply highlights shapes if mouse passes over them
     * 
     * @param {type} obj
     * @returns {none}
     */
    /*this.handleMouseMoveEvent = function (obj) {
       if(obj.isDrawing === false){
          this.markSelectedShapeIfMouseOverIt(obj.mouseCoordsOverCanvas.x, obj.mouseCoordsOverCanvas.y);
       }       
    };*/

    /*
     * Calculates zoom level based on a direction of the mouse wheel 
     */
    this.caclculateZoomLevel = function (direction) {
        return this.zoomLevel;
    };

    this.getZoomLevel = function() {
        return this.getScale();
    };

    this.setPanDisplacement = function( panDisplacement ) {
        this.panDisplacement = panDisplacement;
        this.render();
    };
    
    this.getPanDisplacement = function(  ) {
        return this.panDisplacement;
    };
    
    this.addShape = function (shape) {
        this.shapes.add(shape);
        this.render();
    };
    /*
     * I am wondering if I should return a copy of the shapesStorage instead of real thing ?
     * To prevent anyone from outside BasicModel changing it.
     */
    this.getShapes = function () {
        return this.shapes;
    };
    /*
     * 
     */
    this.getShapesRawForDebugging = function () {
        return this.shapes.returnRawData();
    };
    /*
     * 
     */
    this.getShape = function (index) {
        if( index !== undefined  && index !== null && index >= 0 && index < this.shapes.size()){
            return this.shapes.get(index);
        } else {
            console.info("Trying to get a shape illegally; index = " + index  + "; # of shapes = " + this.shapes.size());
            return null;
        } 
    };

    /*
     * 
     */
    /*this.getMouseDownOrigin = function () {
        return this.mouseDownOrigin;
    };*/
    
    
    //TODO refactor as this function does more than deletign shape
    // need to refactor out the code for selection
    this.deleteShape = function() {
        if (this.selectedShapeIndex !== -1) {
            const shapeIndex = this.selectedShapeIndex;
            this.shapes.delete(this.selectedShapeIndex);
            this.selectedShapeIndex = -1;

            this.render();
            
            this.emitShapeDeleted(shapeIndex);
        }
    };

    this.deleteShapeByIndex = function(obj) {

        const index = obj.deletedShapeIndex;
        //cannot just simply do if(index) because index can be 0
        if( index !== undefined  && index !== null && index >= 0 && index < this.shapes.size()){
            this.shapes.delete(index);

            this.render();
        } else {
            console.info("Trying to do illegal shape delete; index = " + index  + "; # of shapes = " + this.shapes.size());
        }        
    };

    this.updateShape = function(index, newShape) {
        //cannot just simply do if(index) because index can be 0
        if( index !== undefined  && index !== null && index >= 0 && index < this.shapes.size()){
            this.shapes.updateByIndex(index, newShape);

            this.render(); 
        } else {
            console.info("Trying to do illegal shape update; index = " + index+ "; # of shapes = " + this.shapes.size());
        }        
    };

    this.updateShapeByIndex = function(obj) {
        const shapeIndex = obj.shapeIndex;

        this.shapes.updateByIndex(shapeIndex, ShapeFactory.createFromJSON(obj.shape));

        this.render();
    };
    
    this.emitShapeDeleted = function(shapeIndex) {
        this.sockett.emit('shapeDeleted', {
            'deletedShapeIndex' : shapeIndex
        });
    };

    this.emitShapeUpdated = function(shapeIndex) {
        this.sockett.emit('shapeUpdated', {
            'shapeIndex' : shapeIndex,
            'shape': this.shapes.get(shapeIndex)
        });
    };

    /*this.setSelectedShape = function (index) {
        if (index >= 0 && index < this.shapes.size()) {
            this.selectedShapeIndex = index;
            $.publish("shape_selected", {selectedShapeIndex:index});
            this.render();
        }
    };*/
    /*
     * param obj : when shape is selected need to set its index here so that it can be drawn as selected
     * (do not draw in the SelectedShapeModel because it will be another shape overlayed on top of the shape here)
     */
    this.handleShapeSelectedEvent = function (obj) {
        if (obj.selectedShapeIndex >= 0 && obj.selectedShapeIndex < this.shapes.size()) {
            this.selectedShapeIndex = obj.selectedShapeIndex;
            this.render();
        }
    };

    /*this.markSelectedShapeIfMouseOverIt = function (mouseX, mouseY) {
        var selectedShapeIndex = this.getSelectedShapeIndex(mouseX, mouseY);
        this.setSelectedShape(selectedShapeIndex);
    };*/

    /*this.getSelectedShapeIndex = function (mouseX, mouseY) {
        var len = this.shapes.size();
        for (var i = 0; i < len; i++) {
            var shape = this.shapes.get(i);
            if (shape.isPointOver(this.context, mouseX, mouseY)) {
                return i;
            }
        }
        return -1;
    };*/
    
    this.clearCanvas = function() {
        
        var topLeft = {};
        topLeft.x = (0 - this.panDisplacement.x)/this.getScale();
        topLeft.y = (0 - this.panDisplacement.y)/this.getScale();
        
        var bottomRight = {};
        bottomRight.x = (this.canvas.width() )/this.getScale();
        bottomRight.y = (this.canvas.height() )/this.getScale();
        
        this.context.clearRect(
                topLeft.x, topLeft.y, 
                bottomRight.x, bottomRight.y
        );
    };
    
    this.render = function () {
        
        var len = this.shapes.size();
        
        this.clearCanvas();
        
        for (var i = 0; i < len; i++) {
            var shape = this.shapes.get(i);

            if (i === this.selectedShapeIndex) {
                shape.drawSelected(this.context);
            } else {
                shape.draw(this.context);
            }
        }
        
        //this.drawGrid();  
        
    };
    
    /*
     * Little test of performance
     */
    function createManyLines() {
        var numLinesForTest = 10000;
        //at work 10 < x < 1580
        // 10 < y < 530
        for (var i = 0; i < numLinesForTest; i++) {
            x1 = Math.floor((Math.random() * 1580) + 1);
            x2 = Math.floor((Math.random() * 1580) + 1);

            y1 = Math.floor((Math.random() * 530) + 1);
            y2 = Math.floor((Math.random() * 530) + 1);

            var obj = new Line(x1, y1, x2, y2);
            this.shapes.add(obj);
        }
    }

}

BasicModel.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: BasicModel
    }
});

module.exports = BasicModel;