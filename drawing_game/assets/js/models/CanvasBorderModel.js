/* 
 * Draw grid lines:
 * - typically slightly bleaker lines for the grid
 * - and brighter lines at 0,0 point to highlight the origin point
 */
var Layers = require('../layers/LayerAPI');
var Line = require('../shapes/Line');
var BaseModel = require('./BaseModel');

function CanvasBorderModel (options) {

    //const options = {};

    //options.canvas = jQuery(Layers.oneOf(options.canvas)); 
    //options.socket = null;
    BaseModel.call(this, options); //call the super constructor
 
    /*
     * 
     */
    this.render = function (context, obj) {
        this.clearCanvas();
        this.canvasBorder.forEach(function(element){
            element.draw(this.context, 1/this.zoomLevel);
        }, this);
    };

    /*
     * Override parent's object default behaviour
     */
    this.handleZoomEvent = function (obj) {
        //need to override the method not to zoom in this context
    };
    /*
     * Override parent's object default behaviour
     */
    this.handlePanEvent = function (obj) {

    };
    /*
     * Override parent's object default behaviour
     */
    this.handleSocketPanEvent = function (obj) {

    };

    this.clearCanvas = function() {
        this.context.clearRect(
                0, 0, 
                this.canvasWidth, this.canvasHeight);
    }

    /*
     * Function still does a few too many things but at least it is now refactored into itrs own d
     * dedicated function to set the size (and color for now) for the border
     */
    this.setBorderSize = function(canvasWidth, canvasHeight, offsetX){

        this.canvasBorder = []; // holds 4 lines to draw the border around the canvas
        
        //draw a "border" line under the ruler ticks
        let border = new Line(0, offsetX, canvasWidth, offsetX);
        border.setStrokeStyle(this.borderLineColor);
        this.canvasBorder.push(border); //top horizontal

        border = new Line(canvasWidth, 0, canvasWidth, canvasHeight);
        border.setStrokeStyle(this.borderLineColor);
        this.canvasBorder.push(border); //right vertical

        border = new Line(canvasWidth, canvasHeight, 0, canvasHeight);
        border.setStrokeStyle(this.borderLineColor);
        this.canvasBorder.push(border); //bottom horizontal

        border = new Line(offsetX, canvasHeight, offsetX, 0);
        border.setStrokeStyle(this.borderLineColor);
        this.canvasBorder.push(border); //left vertical

    }

    /*
     * Each canvas model needs to be able to react to canvas resizing.
     * In this case we need to either shorten or extend the vertical ruler ticks
     * depending if the canvas shrunk or enlarged
     */
     this.reactToCanvasResizeEvent = function(){
        this.canvasWidth = this.canvas.width() - this.offsetWidth; // -1 is for line width (otherwise line will be drawn on the outside of the canvas)
        this.canvasHeight = this.canvas.height() - this.offsetHeight;
        this.setBorderSize(this.canvasWidth, this.canvasHeight, this.offsetX);
    }
    /*
     * Handle the resize event from here and call super's method
     */
    this.handleWindowResizedEvent = function(obj) {
        this.__proto__.handleWindowResizedEvent.call(this, obj, true);
        this.reactToCanvasResizeEvent();
        this.render(obj);
    }
    this.init = function () {
        //the edges of the RulerLayer are not flush with Paper canvas edges, the are offset
        this.offsetX = 20;
        this.offsetWidth = 1; //1 is for line width (otherwise line will be drawn on the outside of the canvas)
        this.offsetHeight = 2;

        this.borderLineColor = "#000000";

        this.canvasWidth = this.canvas.width() - this.offsetWidth; // -1 is for line width (otherwise line will be drawn on the outside of the canvas)
        this.canvasHeight = this.canvas.height() - this.offsetHeight;

        this.setBorderSize(this.canvasWidth, this.canvasHeight, this.offsetX);

        this.render();
    };
    this.init();
}

CanvasBorderModel.prototype = Object.create(BaseModel.prototype, {
    constructor: {
        value: CanvasBorderModel
    }
});

module.exports = CanvasBorderModel;