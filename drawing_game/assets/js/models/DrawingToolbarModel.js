'use strict';

class DrawingToolbarModel {

	constructor() {

		const self = this;

   		$("#toolbar_straight_line").change(
   			function() {
   				$.publish("selectDrawingMode", { 'key' : 'straightLine'} );
   			}
		);

		$("#toolbar_circle").change(
   			function() {
		  		//$("#paper").trigger("selectDrawingMode", {"param": "one", "key": "circle"});
		  		$.publish("selectDrawingMode", { 'key' : 'circle'} );
			}
		);

		$("#toolbar_freeform").change(
   			function() {
		  		//$("#paper").trigger("selectDrawingMode", {"param": "one", "key": "freeForm"});
		  		$.publish("selectDrawingMode", { 'key' : 'freeForm'} );
			}
		);

    $("#toolbar_point").change(
        function() {
          //$("#paper").trigger("selectDrawingMode", {"param": "one", "key": "freeForm"});
          $.publish("selectDrawingMode", { 'key' : 'point'} );
      }
    );

		$("#toolbar_stop_drawing").change(
   			function() {
		  		//$("#paper").trigger("stopDrawing", {});
		  		$.publish("stopDrawing", {'key' : 'stop_drawing'} );
			}
		);


    $("#toolbar_delete_shape").click(
        function() {
          $.publish("deleteShape", {} );
          
      }
    );

		$.subscribe('selectDrawingMode', 
			function( event, obj ) {
				self.handleSelectDrawingModeEvent(obj);
        	}
        );
        $.subscribe('stopDrawing', 
			function( event, obj ) {
				self.handleStopDrawingEvent();
        	}
        );
			//that.handleSelectDrawingMode.call(that, obj);
    
	}

	/*
	 * Need to highlight appropriate button on the toolbar when event is fired
	 */
   	handleSelectDrawingModeEvent (obj) {
   		switch(obj.key){
   			case "straightLine":
   				$("#toolbar_straight_line").parent().addClass("active").siblings().removeClass("active");
   			break;
   			case "circle":
   				$("#toolbar_circle").parent().addClass("active").siblings().removeClass("active");
   			break;
   			case "freeForm":
   				$("#toolbar_freeform").parent().addClass("active").siblings().removeClass("active");
   			break;
   			case "point":
   				$("#toolbar_point").parent().addClass("active").siblings().removeClass("active");
   			break;
        case "freeForm":
          $("#toolbar_stop_drawing").parent().addClass("active").siblings().removeClass("active");
        break;

   			default: //do nothing
   		}
    };
    /*
     *
     */
    handleStopDrawingEvent () {
    	$("#toolbar_stop_drawing").parent().addClass("active").siblings().removeClass("active");
    };
}

module.exports = DrawingToolbarModel;