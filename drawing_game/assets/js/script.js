/*
This is a main controller file. It initializes everything and ...
- handles socket.on() events for all browser CLIENTs
*/

const MouseHandler = require('./actions/MouseHandler');

const DrawingStateModel = require('./models/DrawingStateModel');

const EventFactory = require('./events/EventFactory');

const InitializationFactory = require('./scaffold/InitializationFactory');

const PanZoomModel = require('./models/PanZoomModel');

const Line = require('./shapes/Line');

const pubsub = require('./pubsub/PubSub');

const Transform = require('./matrix/Transform');

const uuidV4 = require('uuid/v4');

// A $( document ).ready() block.
$(function () {

    var socket = io.connect();

    const UUID = uuidV4();

    function getRoomName(){
        const pathname = window.location.pathname;
        const fields = pathname.split('/');
        //get the last element
        const room_name = fields[fields.length-1];
        return room_name || 'default';
    }
    //
    socket.on('connect', function (data) {
        socket.emit( 'join', {
            name: 'anonymous',
            room_name: getRoomName(),
            uuid:UUID
        })
    });
    //need to fire that to get the users who joined the room before the user who is joining in now
    //socket.emit('get-users', {room_name: getRoomName()}); 

    socket.on('all-users', function(data){
        pubsub.emit('users_updated', {users: data, selfUUID: UUID});
    });

    var canvas = $('#paper');
    //console.info(canvas.parent().width());
    //console.info(canvas.parent().height());
    //console.info(canvas.parent().outerWidth());
    //console.info(canvas.parent().outerHeight());

    const arbitraryOffsetWidth = 5; //offset from the right edge
    const arbitraryOffsetHeight = 5; // offset from the bottom

    canvas.width(canvas.parent().width()-arbitraryOffsetWidth);
    canvas.height(canvas.parent().height()-arbitraryOffsetHeight);
    //experimenting with canvas width and height ...
    $(document).ready(function(){

        $(window).resize(function(data){
            var canvas = $('#paper');
            canvas.width(canvas.parent().width()-arbitraryOffsetWidth);
            canvas.height(canvas.parent().height()-arbitraryOffsetHeight);
            pubsub.emit('window_resized', {canvas: canvas});
            //console.info(canvas.height());
            //console.info(canvas.parent().width());
            //console.info(canvas.parent().height());
            //canvas.width(canvas.parent().width()-30);
            //canvas.height(canvas.parent().height()-30);
        });

        var breakpointWidth = 768;//this Bootstrap's breakpoint to switch from Desktop to Mobile mode
        //click handler for buttons in the menu
        $("div[data-toggle='buttons'] label").on('click', function(){
            if($(window).width() < breakpointWidth ) {
                $('.navbar-toggle').click(); //collapse the navbar
            }

        });
        //click handler for showing/hiding Preperties Sideber from the menu
        $('#nav-bar-toggle-sidebar').on('click', function(){
            $('#sidebar-wrapper').toggleClass("toggled");
            if($(window).width() < breakpointWidth ) {
                $('.navbar-toggle').click(); //toggle collapse of the navbar
            }
        });
        //make sidebar draggable
        //$(function() {
            $("#sidebar-wrapper").draggable();
        //});
    });
    

    // This demo depends on the canvas element
    if (!('getContext' in document.createElement('canvas'))) {
        alert('Sorry, it looks like your browser does not support canvas!');
        return false;
    }

    const options = {};
    options.canvas = canvas;
    options.socket = socket;
    options.pubsub = pubsub;

    let context = options.canvas[0].getContext('2d');
    //context.transform = new Transform(); // init
    context.transform = new Transform(this.context);
    //console.info(context);
    options.context = context;


    const panZoomModel = new PanZoomModel(options); // just a holder of informaiton
    
    var drawingStateModel = new DrawingStateModel(options);
   
    new InitializationFactory(options); // funny code;; TODO: re-factor , i mean why create new object when a static method will suffice
   
    var mouseHandler = new MouseHandler(canvas);
    
    var lastEmit = $.now();
    
	//All of these socket.on events handlers are needed as one doesn't subscribe
	//to "socket" events on the client files. There is a dedicated pub/sub abstraction.
	
    socket.on('socket_panning', function (data) {
        //$.publish("socket_panning", data);
        pubsub.emit('socket_panning', data);
    });
    
    socket.on('zooming', function (data) {
        //$.publish("socket_zooming", data);
        pubsub.emit('socket_zooming', data);
    });
    
    socket.on('shapeDeleted', function (data) {
        //$.publish("shape_deleted", data);
        pubsub.emit('shape_deleted', data);
    });

    socket.on('shapeUpdated', function (data) {
        //$.publish("shape_updated", data);
        pubsub.emit('shape_updated', data);
    });

    socket.on('shapeCreated', function (data) {
        //$.publish("new_shape_created", data);
        pubsub.emit('new_shape_created', data);
    });

    /*
     * Currently drawn shape is sent to connected clients
     */
    socket.on('updateCurrentDrawingModel', function (data) {
        //$.publish("update_current_drawing_model", data);
        pubsub.emit('update_current_drawing_model', data);
    });
    
    /*
     * Mouse wheel zooming events handler
     * Not sure if this event should be here or somewhere else ...
     */
    canvas.on('mousewheel DOMMouseScroll', function (event) {
        const localEvent = window.event || event.originalEvent; // old IE support

        const direction = (localEvent.detail < 0 || localEvent.wheelDelta > 0) ? 1 : -1;
        const zoomLevel = panZoomModel.getScale();

        const transformMatrixObject = EventFactory.getChangeTransformMatrixObject();
        
        transformMatrixObject.panDisplacement = panZoomModel.getPanDisplacement();
        transformMatrixObject.zoomL = direction < 0 ? 0.8 : 1.25;

        transformMatrixObject.zoomLevel = zoomLevel * transformMatrixObject.zoomL;

        const mCoords = mouseHandler.convertToCanvasCoords(localEvent.pageX, localEvent.pageY);
        const panDis = panZoomModel.getPanDisplacement();

        const origin = {};
        origin.x = (mCoords.x - panDis.x) / panZoomModel.getScale();
        origin.y = (mCoords.y - panDis.y) / panZoomModel.getScale();

        transformMatrixObject.origin = origin;
        
		//publish event for local client consumption
		//$.publish("client_zooming", transformMatrixObject);
        pubsub.emit('client_zooming', transformMatrixObject);
		
        //send event to the server
        //if ($.now() - lastEmit > 30) {
        socket.emit('socket_zoom', transformMatrixObject);
            //lastEmit = $.now();
        //}
    });

    /*
     * This object ets created wheneevr user clicks mouse down or touches the screen
     */
    function getMouseDownOrTouchObject(coords){
        const mouseDownEventObject = EventFactory.getMouseDownEventObject();
        const mCoords = mouseHandler.convertToCanvasCoords(coords.pageX, coords.pageY);
        const panDis = panZoomModel.getPanDisplacement();

        const origin = {};
        origin.x = (mCoords.x - panDis.x) / panZoomModel.getScale();
        origin.y = (mCoords.y - panDis.y) / panZoomModel.getScale();

        mouseDownEventObject.mouseCoordsOverCanvasAdjusted = origin;
        //
        if (drawingStateModel.getDrawingMode() !== false && drawingStateModel.getDrawingMode() !== 0) {
            mouseDownEventObject.isDrawing = true;
            mouseDownEventObject.currentDrawingMode = drawingStateModel.getDrawingMode();
        }
        return mouseDownEventObject;

    }

    /*
     * User touched screen on a mobile device
     */
    canvas.on('touchstart', function (evt) {
        const event = evt.originalEvent;
        event.preventDefault();
        const mouseDownEventObject = getMouseDownOrTouchObject(event.targetTouches[0]);
        pubsub.emit('mouse_left_button_down', mouseDownEventObject);
     
    });

    /*
     * Another mouse event.
     * Perhaps it should have its own "repository"
     */
    canvas.on('mousedown', function (e) {
        e.preventDefault();
        switch (e.which) {
            case 1:
                const mouseDownEventObject = getMouseDownOrTouchObject(e);
                pubsub.emit('mouse_left_button_down', mouseDownEventObject);
                break;
            case 2:
                //Middle Mouse button pressed
                break;
            case 3:
                //Right Mouse button pressed
                break;
            default:
                //You have a strange Mouse!
        }
    });

    /*
     *
     */
    function reactToMouseMoveOrTouchMoveEventObject(coords){

        const mouseEventObject = EventFactory.getMouseMoveEventObject();
        mouseEventObject.mouseCoordsOverCanvas = mouseHandler.convertToCanvasCoords(coords.pageX, coords.pageY);

        const panDisplacement = panZoomModel.getPanDisplacement();
        const adjustedMouseCoords = {};

        adjustedMouseCoords.x = (mouseEventObject.mouseCoordsOverCanvas.x - panDisplacement.x) / panZoomModel.getScale();
        adjustedMouseCoords.y = (mouseEventObject.mouseCoordsOverCanvas.y - panDisplacement.y) / panZoomModel.getScale();

        mouseEventObject.mouseCoordsOverCanvasAdjusted = adjustedMouseCoords;
        mouseEventObject.panDisplacement = panDisplacement;
        mouseEventObject.zoomLevel = panZoomModel.getScale();
        //
        if (drawingStateModel.isDrawing() === true) {
            mouseEventObject.isDrawing = true;
            pubsub.emit('mouse_move', mouseEventObject);
        } else if (drawingStateModel.isPanning() === true) {
            mouseEventObject.origin = panZoomModel.getMouseDownOrigin();
            pubsub.emit('client_panning', mouseEventObject);
            socket.emit('socket_pan', mouseEventObject);
        } else {            
            mouseEventObject.isDrawing = false; //it is default value
            mouseEventObject.isPanning = false;
            pubsub.emit('mouse_move', mouseEventObject);
        }
    }
    /*
     * User moves finger over the canvas
     */
    canvas.on('touchmove', function (evt) {
        const event = evt.originalEvent;
        event.preventDefault();
        reactToMouseMoveOrTouchMoveEventObject(event.targetTouches[0]);
    });
    /*
     * Yet another mouse event handler, the more comments I write about these
     * the more I think they should be refactored into its own class
     */
    canvas.on('mousemove', function (e) {
        reactToMouseMoveOrTouchMoveEventObject(e);
    });
    
    /*
     * Another mouse event handler ...
     */
    canvas.bind('mouseleave', function (e) {
        //$.publish("mouse_left_canvas_boundaries", {});
        pubsub.emit('mouse_left_canvas_boundaries');
    });
    /*
     * User lifted the finger off the canvas
     */
    canvas.on('touchend', function (evt) {
        const event = evt.originalEvent;
        event.preventDefault();
        //pubsub.emit('mouse_left_button_up');
		
		//const mouseDownEventObject = getMouseDownOrTouchObject(event.targetTouches[0]);
        pubsub.emit('mouse_left_button_down', null);
    });

    /*
     * Fairly obvious mouse event hadler
     */
    canvas.on('mouseup', function (e) {

        switch (e.which) { //which mouse button
            case 1:
                //broadcast that left mouse was released  
                pubsub.emit('mouse_left_button_up');            
                break;
            case 2:
                //Middle Mouse button released.
                break;
            case 3:
                //Right Mouse button released.
                break;
            default:
                //You have a strange Mouse!
        }
    });

    // Remove inactive clients after 10 seconds of inactivity
    /*
    setInterval(function () {

        for (ident in clients) {
            if ($.now() - clients[ident].updated > 10000) {
                // Last update was more than 10 seconds ago. 
                // This user has probably closed the page
                cursors[ident].remove();
                delete clients[ident];
                delete cursors[ident];
            }
        }

    }, 10000);
    */

});