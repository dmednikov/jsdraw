'use strict';

/*
 * A central factory which produces the data structures for all events
 * 
 * Rather than creating different events all over the code a central factory
 * should allow for better control and change management. In other words
 * in orde to fire an event a structure for the event must be taken from
 * this factory and its fields filled as required.
 */
class EventFactory {
   	
   	/*
   	 * Some consideration for mouseMove event
	    *
   	 * Rather than creating a drawing event with changing mouse coordinates
   	 * for this iteration decided that creatin a generic event with properties such as
   	 * isDrawing' is preferred. Will see if it goes out of control with a number of required
   	 * properties, in this case might revisit the approach.
	    */
   	static getMouseMoveEventObject () {
   		return {
   			mouseCoordsOverCanvas: { //mouse coordinates over canvas (not adjusted to the current pan and zoom)
   				x: "",
   				y: ""
   			},
            mouseCoordsOverCanvasAdjusted: { //mouse coordinates over canvas adjusted to the current pan and zoom
               x: "",
               y: ""
            },
   			isDrawing:false,
   			isPanning:false,
            panDisplacement:0,
            zoomLevel:1,
            origin:{}, // where the move event started
            currentDrawingMode:0 // this is for cases when isDrawing is set to true one needs to know what Shape to create in onMouseDown
   		}
   	}

   	/*
   	 * Some consideration for mouseDown event
	    *
   	 * A couple of options here:
   	 *  - pass origin already adjusted to pan and zoom
   	 *  - pass raw coordinates and adjust on the client
   	 *
	    */
   	static getMouseDownEventObject () {
   		return {
   			mouseCoordsOverCanvasAdjusted: { //mouse coordinates over canvas adjusted to the current pan and zoom
   				x: "",
   				y: ""
   			},
   			button: "left",
            isDrawing:false
   		}
   	}

   	/*
   	 * Event for when client zooms in or out
   	 *
	    */
   	static getZoomEventObject () {
   		return {
   			direction: 0
   		}
   	}
      /*
       * Event for when client zooms in or out, pans ...
       * Any time transform matrix needs to be adjusted on any layer
       */
      static getChangeTransformMatrixObject () {
         return {
            zoomLevel: 1,
            origin:{},
            panDisplacement: { //pan displacement values
               x: "",
               y: ""
            },
            mouseCoordsOverCanvas: { ////mouse coordinates over canvas (not adjusted to the current pan and zoom), temporary for backwards copmatibility
               x: "",
               y: ""
            }
         }
      }
}

module.exports = EventFactory;