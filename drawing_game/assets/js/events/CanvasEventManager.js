'use strict';
//https://socket.io/docs/emit-cheatsheet/
class CanvasEventManager {

    constructor(canvasEventManagerParams) {
        const socket = canvasEventManagerParams.socket;
        const io = canvasEventManagerParams.io;

        /*
        * This event is needed for panning
        */ 
        socket.on('socket_pan', function (data) {
            // This line sends the event (broadcasts it) to everyone except the originating client.
            socket.broadcast.emit('socket_panning', data);
        });    
        
       /*
        * This event is needed to have zoom level synced across all connections
        * zoom in one client propagate to all others
        */ 
       socket.on('socket_zoom', function (data) {
            // This line sends the event (broadcasts it)
            // to everyone except the originating client.
            socket.broadcast.emit('zooming', data);
        });
        
        /*
        * This event is fired when a new shape is deleted
        * In either case BasicModel array needs to be updated 
        */ 
        socket.on('shapeDeleted', function (data) {
            // This line sends the event (broadcasts it)
            // to everyone except the originating client
            socket.broadcast.emit('shapeDeleted', data);
        });

        /*
        * This event is fired when a new shape is updated ("new shape is updated" - what does it mean?)
        * In either case BasicModel shape storage needs to be updated 
        */ 
        socket.on('shapeUpdated', function (data) {
            // This line sends the event (broadcasts it)
            // to everyone except the originating client
            socket.broadcast.emit('shapeUpdated', data);
        });

         /*
        * This event is fired when a new shape is created in CurrentDrawingmodel
        * BasicModel shape storage needs to be updated , and CurrentDrawingModel needs to flush ti buffer
        */ 
        socket.on('shapeCreated', function (data) {
            // This line sends the event (broadcasts it)
            // to everyone except the originating client
            socket.broadcast.emit('shapeCreated', data);
        });

        /*
         * Whenever shape being drawn on one client has changed this event gets emitted
         * All connected sockets can pick it up and update their own screens with
         * being drawn on the originating socket.
         *  
         * param data JSON representation of the shape being drawn
         */
        socket.on('socket_shape_being_drawn_changed', function (data) {
            //TODO: can I really use socket.room_name or there is a need for a check of it being a valid name?
            //sending to all clients in 'socket.room_name' and/or in 'game2' room, except sender
            socket.to(socket.room_name).emit('updateCurrentDrawingModel', data);
        });


    }
}
module.exports = CanvasEventManager;