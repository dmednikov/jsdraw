/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function MouseHandler(canvas) {
    
    /*
     * 
     * @param {type} canvas
     * @returns {undefined}canvas - jQuery object
     */
    var that = this;
    this.init = function (canvas) {

        this.canvas = canvas;
        this.context = this.canvas[0].getContext('2d'); // set up context for render function

    };

    this.init(canvas);
    
    this.convertToCanvasCoords = function(pageX, pageY){
        //in case canvas get moved during the page lifecycle
        var offset = canvas.offset();

        const x = pageX - offset.left;
        const y = pageY - offset.top;

        return {"x":x, "y":y};
    };

}

module.exports = MouseHandler;
